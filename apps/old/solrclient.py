from django.conf import settings

from .. import models
from . import pysolr

client = None

solr = pysolr.Solr(settings.SOLR_URL)


class SearchError(Exception):
    pass


class Filter:
    def __init__(self, q, value, count, state, filters=None, path="/select/"):
        self.name = value
        self.count = count
        self.active = state
        if q is not None:
            href = f"{path}?q={q}"
            if filters:
                filters = "&f=".join([x for x in filters])
                self.href = f"{href}&f={filters}"
            else:
                self.href = href
        elif filters:
            filters = "&f=".join([x for x in filters])
            self.href = f"{path}?f={filters}"
        else:
            self.href = ""


def setup_filters(request, results):
    active_filters = set(request.filters)
    f = results.facets["facet_fields"]["ar"]
    author_facets = zip(*[f[i::2] for i in range(2)])
    authors = []
    for au, count in author_facets:
        v = f'ar:"{au}"'
        if v in active_filters:
            this_filters = active_filters.copy()
            this_filters.remove(v)
            authors.append(Filter(request.q, au, count, "active", this_filters, request.path))
        else:
            this_filters = active_filters.copy()
            this_filters.add(v)
            authors.append(Filter(request.q, au, count, "not-active", this_filters, request.path))
    f = results.facets["facet_fields"]["year"]
    year_facets = zip(*[f[i::2] for i in range(2)])
    years = []
    for py, count in sorted(year_facets, key=lambda x: x[0], reverse=True):
        v = f"year:{py}"
        if v in active_filters:
            this_filters = active_filters.copy()
            this_filters.remove(v)
            years.append(Filter(request.q, py, count, "active", this_filters, request.path))
        else:
            this_filters = active_filters.copy()
            this_filters.add(v)
            years.append(Filter(request.q, py, count, "not-active", this_filters, request.path))
    f = results.facets["facet_fields"]["dt"]
    dt_facets = zip(*[f[i::2] for i in range(2)])
    dts = []
    for dt, count in dt_facets:
        v = f'dt:"{dt}"'
        if v in active_filters:
            this_filters = active_filters.copy()
            this_filters.remove(v)
            dts.append(Filter(request.q, dt, count, "active", this_filters, request.path))
        else:
            this_filters = active_filters.copy()
            this_filters.add(v)
            dts.append(Filter(request.q, dt, count, "not-active", this_filters, request.path))
    f = results.facets["facet_fields"]["msc"]
    msc_facets = zip(*[f[i::2] for i in range(2)])
    msc = []
    for code, count in sorted(msc_facets, key=lambda x: x[0]):
        v = f"msc:{code}"
        if v in active_filters:
            this_filters = active_filters.copy()
            this_filters.remove(v)
            msc.append(
                Filter(request.q, code.upper(), count, "active", this_filters, request.path)
            )
        else:
            this_filters = active_filters.copy()
            this_filters.add(v)
            msc.append(
                Filter(request.q, code.upper(), count, "not-active", this_filters, request.path)
            )

    f = results.facets["facet_fields"]["serial_id"]
    jid_facets = zip(*[f[i::2] for i in range(2)])
    journals = []
    jids = [x[0] for x in jid_facets]
    jobjects = models.Collection.objects.filter(id__in=jids)
    jdict = {}
    for j in jobjects:
        jdict[str(j.id)] = j
    for jid, count in jid_facets:
        # try:
        #     journal = models.Collection.objects.get(id=jid)
        # except:
        #     continue
        journal = jdict[jid]
        v = f"serial_id:{jid}"
        if v in active_filters:
            this_filters = active_filters.copy()
            this_filters.remove(v)
            journals.append(
                Filter(request.q, journal.title, count, "active", this_filters, request.path)
            )
        else:
            this_filters = active_filters.copy()
            this_filters.add(v)
            journals.append(
                Filter(request.q, journal.title, count, "not-active", this_filters, request.path)
            )

    f = results.facets["facet_fields"]["colid"]
    colid_facets = zip(*[f[i::2] for i in range(2)])
    collections = []
    for colid, count in colid_facets:
        try:
            collection = models.Collection.objects.get(id=colid)
        except BaseException:
            continue
        if collection.coltype == "mbook":
            continue
        v = f"colid:{colid}"
        if v in active_filters:
            this_filters = active_filters.copy()
            this_filters.remove(v)
            collections.append(
                Filter(request.q, collection.title, count, "active", this_filters, request.path)
            )
        else:
            this_filters = active_filters.copy()
            this_filters.add(v)
            collections.append(
                Filter(
                    request.q, collection.title, count, "not-active", this_filters, request.path
                )
            )

    return {
        "author_facets": authors,
        "msc_facets": msc,
        "year_facets": years,
        "dt_facets": dts,
        "jid_facets": journals,
        "colid_facets": collections,
    }


class BrowseResults:
    select_related = []
    prefetch_related = []
    klass = models.Collection

    def __init__(self, results):
        ids = [int(x["id"]) for x in results]
        self.hits = results.hits
        if self.select_related:
            data = self.klass.objects.select_related(*self.select_related).in_bulk(ids)
        else:
            data = self.klass.objects.in_bulk(ids)
        # if self.prefetch_related:
        #     data = self.klass.objects.prefetch_related(*self.prefetch_related).in_bulk(ids)
        # else:
        #     data = self.klass.objects.in_bulk(ids)

        objects = [data.get(x, None) for x in ids]
        self.objects = [x for x in objects if x is not None]
        try:
            self.facets = results.facets
        except AttributeError:
            self.facets = {}

    def __iter__(self):
        return iter(self.objects)

    def __len__(self):
        return len(self.objects)


class JournalList(BrowseResults):
    pass
    # prefetch_related = ['extlink']


class BookList(BrowseResults):
    klass = models.Container


class BookSeriesBooks(BrowseResults):
    klass = models.Container

    def __init__(self, results, colid):
        BrowseResults.__init__(self, results)
        for obj in self.objects:
            m = obj.membership_set.get(collection__id=colid, resource=obj)
            obj.volume = m.volume
            obj.vseries = m.vseries


class IssueList(BrowseResults):
    klass = models.Container


class SearchResults:
    """
    09/06/2021: class unused. search_helpers.SearchResults is used instead
    """

    def __init__(self, request, results, params):
        self.q = request.q
        self.facet_info = setup_filters(request, results)
        self.hits = results.hits
        ids = [int(x["id"]) for x in results]
        # data = models.Resource.objects.in_bulk(ids)
        # objects = [data.get(x, None) for x in ids]
        # self.objects = [x.cast() for x in objects if x is not None]
        self.filters = "&f=".join(request.filters)
        self.params = params
        self.ids = ids

    def __iter__(self):
        ids = self.ids
        for id in ids:
            obj = models.Resource.objects.get(id=id)
            the_attr = getattr(models, obj.classname)

            obj1 = the_attr.objects.select_related().get(id=obj.id)
            yield obj1

    def __len__(self):
        return len(self.ids)


class FacetSearchResults:
    def __init__(self, facet, results, params):
        self.facet = facet
        f = results.facets["facet_fields"][facet]
        self.facets = zip(*[f[i::2] for i in range(2)])


class SolrClient:
    def __init__(self, solr=solr):
        self.solr = solr

    def search(self, request):
        """
        09/06/2021: this function is unused.
        solrSearchCmd calls directly pysolr.Solr.search

        :param request:
        :return:
        """
        if request.filters:
            fq = request.filters[:]
        else:
            fq = []
        if request.ifilters:
            fq.extend(request.ifilters)
        if request.site:
            fq.append(f"sites:{request.site}")
        p = request.params
        params = {
            "q.op": "AND",
            "facet.field": ["ar", "year", "serial_id", "colid", "msc", "dt"],
            "facet.mincount": 1,
            "facet.limit": 100,
            "facet.sort": "count",
            "start": p["start"],
            "rows": p["rows"],
            "sort": p["sort"],
        }
        if fq:
            params.update({"fq": fq})
        q = request.q or "*:*"
        try:
            results = self.solr.search(q, facet="true", **params)
        except pysolr.SolrError as e:
            raise SearchError(str(e))
        return SearchResults(request, results, params)

    def facet_search(self, facet, prefix=None, offset=0, limit=100, site=None):
        q = f"{facet}:*"
        params = {
            "rows": 0,
            "start": 0,
            "facet.field": facet,
            "facet.mincount": 1,
            "facet.offset": offset,
            "facet.limit": limit,
            "facet.sort": "name",
        }
        if prefix:
            params.update({"facet.prefix": prefix})
        if site:
            filters = [f"sites:{site}"]
            params.update({"fq": filters})
        results = self.solr.search(q, facet="true", **params)
        return FacetSearchResults(facet, results, params)

    def class_search(self, classname, start=0, rows=20, site=None, sort=""):
        q = f"classname:{classname}"
        params = {"start": start, "rows": rows, "fl": ["id"]}
        if sort:
            params.update({"sort": sort})
        if site:
            params.update({"fq": f"sites:{site}"})
        results = self.solr.search(q, **params)
        return results

    def stype_search(self, coltype, start=0, rows=20, site=None, sort=""):
        q = f"coltype:{coltype}"
        params = {"start": start, "rows": rows, "fl": ["id"]}
        if sort:
            params.update({"sort": sort})
        if site:
            params.update({"fq": f"sites:{site}"})
        results = self.solr.search(q, **params)
        return results

    def journals(self, start=0, rows=200, site=None, sort="journal_sort_key asc"):
        results = self.stype_search("journal", start=start, rows=rows, site=site, sort=sort)
        return JournalList(results)

    def acta(self, start=0, rows=200, site=None, sort="journal_sort_key asc"):
        results = self.stype_search("acta", start=start, rows=rows, site=site, sort=sort)
        return JournalList(results)

    def books(self, start=0, rows=20, site=None, sort="title_sort_key asc", page=1):
        results = self.class_search("Book", start=start, rows=rows, site=site, sort=sort)
        return BookList(results)

    def issues(self, jid, site=None, sort="vseries desc, year desc, volume desc, number desc"):
        params = {"sort": sort, "rows": 1000, "fl": ["id"]}
        q = f"ctype:issue AND serial_id:{jid}"
        if site:
            params["fq"] = f"sites:{site}"
        results = self.solr.search(q, **params)
        return IssueList(results)

    def issues2(self, jid, site=None, sort="vseries desc, year desc, volume desc, number desc"):
        params = {"sort": sort, "rows": 1000}
        q = f"ctype:issue AND serial_id:{jid}"
        params["fl"] = ["id", "vseries", "year", "volume", "number"]
        if site:
            params["fq"] = f"sites:{site}"
        results = self.solr.search(q, **params)
        return results

    def book_series_books(self, sid, site=None, params={}):
        if "sort" in params:
            pass
        else:
            params["sort"] = "vseries desc, year desc, volume desc"
        q = f"classname:Book AND colid:{sid}"
        if site:
            if "fq" in params:
                params["fq"].append(f"sites:{site}")
            else:
                params["fq"] = f"sites:{site}"
        params["facet.field"] = "year"
        params["facet.mincount"] = 1
        params["facet.limit"] = 1000
        params["facet.sort"] = "index"

        results = self.solr.search(q, facet="true", **params)
        return BookSeriesBooks(results, sid)

    def book_series_books_by_author(self, sid, author, site=None, params={}):
        q = f"classname:Book AND colid:{sid}"
        # q = 'colid:{}'.format(sid
        params["fq"] = [f"{{!tag=au}}ar:{author}"]
        if site:
            params["fq"].append(f"sites:{site}")
        params["facet.field"] = "{!ex=au}year"
        params["facet.mincount"] = 1
        params["facet.limit"] = 1000
        params["facet.sort"] = "index"
        results = self.solr.search(q, facet="true", **params)
        return BookSeriesBooks(results, sid)

    def book_series_books_by_year(self, sid, year, site=None, params={}):
        q = f"classname:Book AND colid:{sid}"
        # q = 'colid:{}'.format(sid
        params["fq"] = [f"{{!tag=y}}year:{year}"]
        if site:
            params["fq"].append(f"sites:{site}")
        params["facet.field"] = "{!ex=y}year"
        params["facet.mincount"] = 1
        params["facet.limit"] = 1000
        params["facet.sort"] = "index"
        results = self.solr.search(q, facet="true", **params)
        return BookSeriesBooks(results, sid)

    def book_series_index(self, sid, site=None, params={}):
        q = f"classname:Book AND colid:{sid}"
        # q = 'colid:{}'.format(sid
        if site:
            params["fq"] = f"sites:{site}"

        params["facet.field"] = ["ar", "year"]
        params["facet.mincount"] = 1
        params["facet.limit"] = 1000
        params["facet.sort"] = "index"
        params["rows"] = 0
        try:
            results = self.solr.search(q, facet="true", **params)
        except pysolr.SolrError as e:
            raise SearchError(str(e))
        return results

    def raw_select(self, request, site=None):
        params = request.GET.copy()
        if site:
            params["fq"] = f"sites:{site}"
        return self.solr._select(params)

    def terms(self, request):
        params = request.GET.copy()
        return self.solr._suggest_terms(params)

    def year_range(self, serial_id, site, ctype="issue"):
        q = f"+serial_id:{serial_id} +ctype:{ctype} +sites:%d"
        result = self.solr.search(q, fl="year", rows=1, sort="year asc")
        if len(result) > 0:
            first_year = result.docs[0]["year"]
        else:
            return ("", "")
        if result.hits > 1:
            start = result.hits - 1
            result = self.solr.search(q, fl="year", rows=1, start=start, sort="year asc")
            last_year = result.docs[0]["year"]
        else:
            last_year = ""
        return (first_year, last_year)


def get_client():
    global client
    if client is None:
        client = SolrClient()
    return client
