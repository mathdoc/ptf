from ptf.cmds.xml.jats.helper import format_date
from ptf.cmds.xml.jats.xmldata import Article as ArticleBase
from ptf.cmds.xml.jats.xmldata import Book as BookBase
from ptf.cmds.xml.jats.xmldata import BookPart as BookPartBase
from ptf.cmds.xml.jats.xmldata import Journal as JournalBase


class Article(ArticleBase):
    published_path = "front/article-meta/pub-date"

    def get_date_published_iso_8601_date_str(self):
        year = self.tree.xpath(self.published_path + "/year")[0].text
        year = year.split("-")
        sp = year[0].split("/")
        if len(sp) > 1:
            year = sp[0]
        else:
            year = year[0]
        month = "1"
        day = "1"
        if self.tree.xpath(self.published_path + "/month"):
            month = self.tree.xpath(self.published_path + "/month")[0].text

        if self.tree.xpath(self.published_path + "/day"):
            month = self.tree.xpath(self.published_path + "/month")[0].text

        return format_date(day, month, year)


class Journal(JournalBase):
    pass


class Book(BookBase):
    id_type_attr = "pub-id-type"

    def get_ctype(self):
        return self.book_type


class BookPart(BookPartBase):
    id_type_attr = "pub-id-type"

    @staticmethod
    def get_book_part_class():
        return BookPart
