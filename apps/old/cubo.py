from ptf.cmds.xml.jats.helper import format_date
from ptf.cmds.xml.jats.xmldata import Article as ArticleBase
from ptf.cmds.xml.jats.xmldata import Journal as JournalBase


class Article(ArticleBase):
    published_path = "front/article-meta/pub-date"

    def get_date_published_iso_8601_date_str(self):
        year = self.tree.xpath(self.published_path + "/year")[0].text
        month = self.tree.xpath(self.published_path + "/month")[0].text
        return format_date("1", month, year)


class Journal(JournalBase):
    pass
