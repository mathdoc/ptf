from ptf.cmds.xml.jats.helper import format_date
from ptf.cmds.xml.jats.xmldata import Article as ArticleBase
from ptf.cmds.xml.jats.xmldata import Book as BookBase
from ptf.cmds.xml.jats.xmldata import BookPart as BookPartBase
from ptf.cmds.xml.jats.xmldata import Journal as JournalBase
from ptf.cmds.xml.xml_utils import remove_namespace


class Article(ArticleBase):
    published_path = "front/article-meta/pub-date"

    def get_date_published_iso_8601_date_str(self):
        year = self.tree.xpath(self.published_path + "/year")[0].text
        year = year.split("-")
        sp = year[0].split("/")
        if len(sp) > 1:
            year = sp[0]
        else:
            year = year[0]
        return format_date("1", "1", year)


class Book(BookBase):
    id_type_attr = "pub-id-type"

    def __init__(self, tree):
        if tree.tag != "book":
            remove_namespace(tree)
            tree = tree.xpath("metadata/book")[0]
            # print(tree.xpath('front/book-meta')[0].text_content())
        meta = tree.xpath("front/book-meta")[0]
        tree.append(meta)
        super().__init__(tree)

    @staticmethod
    def get_book_part_class():
        return BookPart


class BookPart(BookPartBase):
    id_type_attr = "pub-id-type"

    @staticmethod
    def get_book_part_class():
        return BookPart


class Journal(JournalBase):
    pass
