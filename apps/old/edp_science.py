from ptf.cmds.xml.jats.helper import format_date
from ptf.cmds.xml.jats.xmldata import Article as ArticleBase


class Article(ArticleBase):
    published_path = 'front/article-meta/pub-date[@pub-type="epub"]'

    def get_date_published_iso_8601_date_str(self):
        year = self.tree.xpath(self.published_path + "/year")[0].text
        month = self.tree.xpath(self.published_path + "/month")[0].text
        day = self.tree.xpath(self.published_path + "/day")[0].text
        return format_date(day, month, year)

    def get_history_dates(self):
        dates = []
        nodes = self.tree.findall(self.history_path)
        for node in nodes:
            type = node.attrib["date-type"]
            day = node.find("day")
            day = day.text if day else "1"
            month = node.find("month")
            month = month.text if month else "1"
            year = node.find("year").text
            dates.append({"type": type, "date": format_date(day, month, year)})
        return dates
