# Comments views

This module contains the views related to the `comments_database` application (doc [here](/sites/comments_site/comments_database/doc/comments_database.md)). \
It is split into 2 Django applications.

## I. [Comments views core application](../core/doc/comments_views_core.md)

It contains all the code shared between the `comments_views.journal` and the `comments_moderation` (doc [here](/sites/ptf_tools/comments_moderation/doc/comments_moderation.md)) applications.


## II. [Comments views journal application](../journal/doc/comments_views_journal.md)

The application used to display comments and interact with the comments database for prod journal websites (CR, PCJ, ...).

