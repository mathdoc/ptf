from json import JSONDecodeError
from urllib.parse import urlencode

import requests
from requests import RequestException
from requests import Response

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.http import HttpRequest
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from comments_api.constants import API_MESSAGE_KEY
from comments_api.utils import parse_date

from .app_settings import app_settings
from .rights import AbstractUserRights

# Default timeout (seconds) to be used when performing API requests.
DEFAULT_TIMEOUT = 4
DEFAULT_API_ERROR_MESSAGE = _("Error: Something went wrong. Please try again.")


def comments_server_base_url() -> str:
    """
    Returns the base URL of the comment server as defined in settings.py.
    Fallbacks to https://comments.centre-mersenne.org
    """
    comment_base_url = app_settings.API_BASE_URL

    # Remove trailing slash
    if comment_base_url[-1] == "/":
        comment_base_url = comment_base_url[:-1]
    return comment_base_url


def comments_server_url(
    query_params: dict[str, str] = {}, url_prefix="comments", item_id=None
) -> str:
    """
    Returns a formatted URL to query the comment server (API).
    The default URL corresponds to the list of all available comments.

    Parameters:
        - `query_params`:   A dictionnary of query parameters to be appended to the URL.
                            They are used to precisely filter the comment dataset.
        - `comment_id`:     Filters on the given comment ID
    """
    base_url = f"{comments_server_base_url()}/api/{url_prefix}/"
    if item_id:
        base_url += f"{item_id}/"
    if query_params:
        base_url += f"?{urlencode(query_params)}"
    return base_url


def comments_credentials() -> tuple[str, str]:
    """
    Returns the current server credentials (login, password) required to authenticate
    on the comment server.
    """
    credentials = app_settings.API_CREDENTIALS
    if credentials:
        assert isinstance(credentials, tuple) and len(credentials) == 2, (
            "The server has malformed comments server credentials."
            "Please provide them as `(login, password)`"
        )

        return credentials

    raise AttributeError("The server is missing the comments server credentials")


def add_api_error_message(initial_message: str, api_json_data: dict) -> str:
    """Adds the API error description to the given string."""
    formatted_message = initial_message
    api_error = api_json_data.get(API_MESSAGE_KEY)
    if api_error and isinstance(api_error, str):
        formatted_message = f"{formatted_message}<br><br>" if formatted_message else ""
        formatted_message += f'"{api_error}"'

    return formatted_message


def json_from_response(
    response: Response, request_for_message: HttpRequest | None = None
) -> tuple[bool, dict]:
    """
    Returns the JSON content of a HTTP response and whether an exception was raised
    while doing so.
    We consider that there's an exception if the response does not contain correctly
    formed JSON (except HTTP 204) or if its status code >= 300.

    Params:
        -   `response`              The HTTP response to parse.
        -   `request_for_message`   The current HTTP request handled by Django. Used
                                    to add a message (Django) when there's an exception.
                                    Don't provide it if you don't want any messages.

    Returns:
        -   `error`                 Whether an exception was raised.
        -   `content`               The JSON content of the HTTP response.
    """
    try:
        data = response.json()
    except JSONDecodeError:
        # 204 No Content
        if response.status_code == 204:
            return (False, {})
        if request_for_message is not None:
            messages.error(request_for_message, DEFAULT_API_ERROR_MESSAGE)
        return (True, {})

    error = False
    if response.status_code >= 300:
        if request_for_message is not None:
            error_message = add_api_error_message(DEFAULT_API_ERROR_MESSAGE, data)
            messages.error(request_for_message, error_message)
        error = True
    return (error, data)


def format_comment_author_name(comment: dict) -> str:
    """
    Formats an author name according to the comments properties.
    """
    author_name = comment.get("author_name", "")

    hide_name = comment.get("hide_author_name")
    article_author = comment.get("article_author_comment")
    editorial_team = comment.get("editorial_team_comment")
    extra_text = ""

    if article_author:
        if hide_name:
            return _("Auteur(s) de l'article").capitalize()
        extra_text = _("Auteur de l'article")
    elif editorial_team:
        extra_text = _("Équipe éditoriale")
        if hide_name:
            return extra_text.capitalize()

    if not extra_text:
        return author_name

    return f"{author_name} ({extra_text.lower()})"


def format_comment(
    comment: dict, rights: AbstractUserRights | None = None, users: dict = {}
) -> None:
    """
    Format a comment data for display.
    Formatted data includes:
        - author display name (actual name or function)
        - Moderation data (only if `users is provided`)
        - comment URL

    Params:
        -   `comment`   The comment data (usually a dict constructed from
                        the JSON data).
        -   `rights`    The user rights used to check access over moderation data.
        -   `users`     The dictionnary of users used to map moderator IDs
    """
    # Format author name and parent author name
    comment["author_display_name"] = format_comment_author_name(comment)

    parent = comment.get("parent")
    # The comment data may contain either the parent dictionnary data or
    # just the parent ID
    if parent and isinstance(parent, dict):
        comment["parent"]["author_display_name"] = format_comment_author_name(parent)

    # Format author provider link (only ORCID right now)
    author_provider = comment.get("author_provider")
    author_provider_uid = comment.get("author_provider_uid")
    if author_provider and author_provider.lower() == "orcid" and author_provider_uid:
        comment["author_provider_link"] = f"{app_settings.ORCID_BASE_URL}{author_provider_uid}/"

    # Parse dates for display
    date_submitted = comment.get("date_submitted")
    if date_submitted:
        comment["date_submitted"] = parse_date(date_submitted)

    moderation_data = comment.get("moderation_data")
    if moderation_data:
        if moderation_data.get("date_moderated"):
            comment["moderation_data"]["date_moderated"] = parse_date(
                moderation_data.get("date_moderated")
            )

        moderator_id = moderation_data.get("moderator")
        if (
            moderator_id
            and moderator_id in users
            and rights
            and (
                rights.comment_can_manage_moderators(comment)
                or getattr(rights.user, "is_superuser", False)
            )
        ):
            comment["moderation_data"]["moderated_by"] = users[moderator_id]

    # Format comment link
    base_url = comment.get("base_url")
    if base_url:
        # Remove potential trailing slash
        if base_url[-1] == "/":
            base_url = base_url[:-1]
        article_route = reverse("article", kwargs={"aid": comment["doi"]})
        # If a site has an URL prefix, we need to discard it from the article_route,
        # it should already be present in base_url.
        url_prefix = getattr(settings, "SITE_URL_PREFIX", None)
        if url_prefix:
            url_prefix = f"/{url_prefix}"
            if article_route.startswith(url_prefix):
                article_route = article_route[len(url_prefix) :]
        comment["base_url"] = f"{base_url}{article_route}"

    if rights:
        comment["can_moderate"] = rights.comment_can_moderate(comment)

    # Get the moderators
    if rights and users:
        moderators = comment.get("moderators")
        if moderators and (
            rights.comment_can_manage_moderators(comment)
            or getattr(rights.user, "is_superuser", False)
        ):
            comment["moderators_processed"] = {
                moderator_id: users[moderator_id]
                for moderator_id in moderators
                if moderator_id in users
            }


def get_user_dict() -> dict[int, dict[str, str]]:
    """
    Return a dict of users indexed by the user's pk.
    A value is a dict of the user's `pk`, `first_name`, `last_name` and `email`
    """
    users = get_user_model().objects.all().values("pk", "first_name", "last_name", "email")
    return {u["pk"]: u for u in users}


def api_request_wrapper(http_method: str, url: str, **kwargs) -> Response | None:
    """
    Light wrapper around `get`, `post`, and `put` methods of the `requests` library:
        -   Catch any `RequestException`.
        -   Add a default timeout to the request if no `timeout` was provided.

    Params:
        -   `http_method`   The HTTP method to use for the request
                            (get, post, put, head or delete).

    Returns:
        -   `response`      The `Response` object if no `RequestException` was raised,
                            else `None`.
    """
    if "timeout" not in kwargs:
        # (connect timeout, read timeout)
        kwargs["timeout"] = (4, 7)
    http_method = http_method.upper()
    try:
        if http_method == "GET":
            response = requests.get(url, **kwargs)
        elif http_method == "POST":
            response = requests.post(url, **kwargs)
        elif http_method == "PUT":
            response = requests.put(url, **kwargs)
        # Other HTTP methods are not used. They can be added above
        # The timeout kwarg is only available for HEAD and DELETE
        else:
            raise ValueError(f"Wrong argument for 'http_method': '{http_method}'")
    except RequestException:
        response = None

    return response


def make_api_request(
    http_method: str, url: str, request_for_message: HttpRequest | None = None, **kwargs
) -> tuple[bool, dict]:
    """
    Performs the HTTP request to the provided URL and return whether an exception was
    raised and the JSON content of the response. \\
    Catched exceptions are any `RequestException` or `JSONDecodeError`. \\
    Any additional kwargs are passed directly to the `requests` library.

    Params:
        -   `http_method`           The HTTP method to use for the request
                                    (get, post, put, head or delete).
        -   `url`                   The URL to request.
        -   `request_for_message`   The current HTTP request handled by Django. Used
                                    to add a message (Django) when there's an exception.
                                    Don't provide it if you don't want any messages.

    Returns:
        -   `error`                 Whether an exception was raised.
        -   `content`               The JSON content of the HTTP response.
    """
    response = api_request_wrapper(http_method, url, **kwargs)

    if response is None:
        if request_for_message is not None:
            messages.error(request_for_message, DEFAULT_API_ERROR_MESSAGE)
        return True, {}

    return json_from_response(response, request_for_message)


def get_comment(
    query_params: dict[str, str],
    id: int,
    request_for_message: HttpRequest | None = None,
) -> tuple[bool, dict]:
    """
    Make an HTTP request to retrieve the requested comment. Commodity wrapper
    around `make_api_request`.

    Params:
        -   `query_params`          Query parameters for the API URL.
        -   `id`                    The comment's ID.
        -   `request_for_message`   The current HTTP request handled by Django. Used
                                    to add a message (Django) when there's an exception.
                                    Don't provide it if you don't want any messages.

    Returns:
        -   `error`                 Whether an exception was raised.
        -   `content`               The JSON content of the HTTP response.
    """
    return make_api_request(
        "GET",
        comments_server_url(query_params, item_id=id),
        request_for_message,
        auth=comments_credentials(),
    )
