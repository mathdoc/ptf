from typing import Any

from ckeditor.fields import RichTextFormField

from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from comments_api.constants import COMMENT_STATUS_CHOICES


class CommentForm(forms.Form):
    """
    Form for user comments. The only user input is the content of the comment.
    """

    content = RichTextFormField(required=True, label=False, config_name="comment")
    approval = forms.BooleanField(required=True)

    # Hidden inputs
    doi = forms.CharField(required=True, widget=forms.HiddenInput())
    id = forms.IntegerField(required=False, widget=forms.HiddenInput)
    parent = forms.IntegerField(required=False, widget=forms.HiddenInput())
    parent_author_name = forms.CharField(
        max_length=128, required=False, widget=forms.HiddenInput()
    )
    #### TODO: Is `date_submitted` really used ????
    date_submitted = forms.CharField(required=False, widget=forms.HiddenInput())
    status = forms.ChoiceField(
        choices=COMMENT_STATUS_CHOICES,
        required=False,
        widget=forms.HiddenInput(),
    )

    def clean(self):
        cleaned_data = super().clean()
        parent = cleaned_data.get("parent")
        parent_author_name = cleaned_data.get("parent_author_name")
        null_values = [None, 0, "", ":~:init:~:"]

        if (parent in null_values and parent_author_name not in null_values) or (
            parent not in null_values and parent_author_name in null_values
        ):
            raise forms.ValidationError(
                _("Both Parent ID and Parent author name must be null or non-null."),
                code="invalid",
            )

        if self.prefix == "reply" and (parent in null_values or parent_author_name in null_values):
            raise forms.ValidationError(
                _("Both Parent ID and Parent author name must be non-null for a reply comment."),
                code="invalid",
            )

        return cleaned_data


class CommentFormAutogrow(CommentForm):
    content = RichTextFormField(required=True, label=False, config_name="comment_autogrow")
    approval = None


class BaseCommentStatusForm(forms.Form):
    """
    Base comment status form. Only contains the status field.

    Used for quick actions
    """

    post_url = "comment_status_change_light"
    status = forms.ChoiceField(
        choices=COMMENT_STATUS_CHOICES,
        required=True,
        widget=forms.HiddenInput(),
    )


class DetailedCommentStatusForm(BaseCommentStatusForm):
    """
    Form to change a comment status with additional fields available to moderators.
    """

    post_url = "comment_status_change"
    article_author_comment = forms.BooleanField(
        label="Article's author",
        required=False,
        help_text="Check this box if the comment is from an author of the article.",
    )
    editorial_team_comment = forms.BooleanField(
        label="Editorial team",
        required=False,
        help_text="Check this box if the comment is from someone of the editorial team.",
    )
    hide_author_name = forms.BooleanField(
        label="Hide author name",
        required=False,
        help_text="Check this box to hide the name of the comment's author.",
    )

    def clean(self) -> dict[str, Any]:
        cleaned_data = super().clean()
        if (
            cleaned_data["hide_author_name"]
            and not cleaned_data["article_author_comment"]
            and not cleaned_data["editorial_team_comment"]
        ):
            raise ValidationError(
                "'Article's author' or 'Editorial team' must be checked"
                " when 'Hide author name' is checked."
            )
        if cleaned_data["article_author_comment"] and cleaned_data["editorial_team_comment"]:
            raise ValidationError(
                "'Article's author' and 'Editorial team' can't be checked together."
            )
        return cleaned_data


def format_form_errors(form: forms.Form):
    """
    Return the formatted list of all the form error messages, if any.

    Field error messages are prefixed with the field name.
    """
    errors = []
    for field, error_list in form.errors.items():
        if field == "__all__":
            errors.extend(error_list)
            continue
        errors.extend([f"{field} - {err}" for err in error_list])

    return errors
