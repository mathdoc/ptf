from typing import Any

from django.contrib import messages
from django.http import HttpRequest
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import TemplateView
from django.views.generic import View

from comments_api.constants import PARAM_BOOLEAN_VALUE
from comments_api.constants import PARAM_DASHBOARD
from comments_api.constants import STATUS_REJECTED
from comments_api.constants import STATUS_SOFT_DELETED
from comments_api.constants import STATUS_SUBMITTED
from comments_api.constants import STATUS_VALIDATED
from ptf.model_helpers import get_article_by_doi
from ptf.url_utils import format_url_with_params
from ptf.utils import send_email_from_template

from .app_settings import app_settings
from .forms import BaseCommentStatusForm
from .forms import CommentFormAutogrow
from .forms import DetailedCommentStatusForm
from .forms import format_form_errors
from .mixins import AbstractCommentRightsMixin
from .utils import comments_credentials
from .utils import comments_server_url
from .utils import format_comment
from .utils import get_comment
from .utils import get_user_dict
from .utils import make_api_request


class CommentDashboardListView(AbstractCommentRightsMixin, TemplateView):
    """
    View for displaying the lists of pending and processed comments.
    """

    template_name = "dashboard/comment_list.html"

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        """
        Get all the comments to display to the user.
        Split them by status (submitted vs validated)
        """
        context = super().get_context_data(**kwargs)

        # The request to the comment API is adapted with the user permissions:
        rights = self.get_rights(self.request.user)
        query_params = rights.comment_rights_query_params()
        query_params[PARAM_DASHBOARD] = PARAM_BOOLEAN_VALUE

        error, comments = make_api_request(
            "GET",
            comments_server_url(query_params),
            request_for_message=self.request,
            auth=comments_credentials(),
        )

        if error:
            return context

        submitted_comments = []
        processed_comments = []
        users = get_user_dict()
        for comment in comments:
            format_comment(comment, rights, users)
            status_code = comment.get("status")
            if status_code == STATUS_SUBMITTED:
                submitted_comments.append(comment)
            else:
                processed_comments.append(comment)

        context["display_moderators"] = (
            len(rights.get_user_admin_collections() + rights.get_user_staff_collections()) > 0
        ) or self.request.user.is_superuser

        # Table sorting is purely handled by front end, we don't need to enfore it here.
        context["submitted_comments"] = submitted_comments
        context["processed_comments"] = processed_comments

        # Default forms for quick moderation actions
        context["validate_form"] = BaseCommentStatusForm(initial={"status": STATUS_VALIDATED})
        context["reject_form"] = BaseCommentStatusForm(initial={"status": STATUS_REJECTED})
        return context


class CommentDashboardDetailsView(AbstractCommentRightsMixin, TemplateView):
    template_name = "dashboard/comment_details.html"

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        pk = kwargs["pk"]

        # The request is adapted related to the user permissions:
        rights = self.get_rights(self.request.user)
        query_params = rights.comment_rights_query_params()
        query_params[PARAM_DASHBOARD] = PARAM_BOOLEAN_VALUE
        error, comment = get_comment(query_params, pk, request_for_message=self.request)

        if error:
            return context

        context["display_moderators"] = (
            rights.comment_can_manage_moderators(comment) or self.request.user.is_superuser
        )
        users = get_user_dict()
        format_comment(comment, rights, users)
        context["comment"] = comment

        edit = kwargs.get("edit")
        if edit and rights.comment_can_edit(comment):
            context["edit"] = True
            context["show_edit"] = True
            post_query_params = {
                "redirect_url": self.request.build_absolute_uri(
                    reverse_lazy("comment_details", kwargs={"pk": pk})
                )
            }

            context["post_url"] = format_url_with_params(
                reverse_lazy(rights.COMMENT_POST_URL), post_query_params
            )
            # Deep copy the comment data
            initial_data = dict(comment)
            initial_data["content"] = initial_data["sanitized_html"]
            context["comment_form"] = CommentFormAutogrow(initial=initial_data)

        else:
            moderate_form = DetailedCommentStatusForm(
                initial={
                    "editorial_team_comment": comment["editorial_team_comment"],
                    "article_author_comment": comment["article_author_comment"],
                    "hide_author_name": comment["hide_author_name"],
                }
            )
            context["comment_status_validated"] = STATUS_VALIDATED
            context["comment_status_rejected"] = STATUS_REJECTED
            if rights.comment_can_delete(comment):
                context["delete_form"] = BaseCommentStatusForm(
                    initial={"status": STATUS_SOFT_DELETED}
                )

            comment_status = comment.get("status")
            if comment_status == STATUS_SUBMITTED:
                if rights.comment_can_edit(comment):
                    context["show_edit"] = True
            else:
                context["display_moderated_by"] = True
                if comment_status == STATUS_VALIDATED:
                    context["show_context"] = True

            context["moderate_form"] = moderate_form

        return context


class CommentChangeStatusView(AbstractCommentRightsMixin, View):
    """
    View used to modify the status of an existing comment.

    Used by:
        - a comment's author to delete his/her own comment
        - a comment moderator to validate/reject a comment he/she has access to.
    """

    def post(self, request: HttpRequest, *args, **kwargs) -> HttpResponseRedirect:
        redirect_url = request.headers.get("referer")
        redirect_url = redirect_url if redirect_url else reverse_lazy("comment_list")
        response = HttpResponseRedirect(redirect_url)

        if request.path.endswith("/light/"):
            status_form = BaseCommentStatusForm(request.POST)
        else:
            status_form = DetailedCommentStatusForm(request.POST)

        if not status_form.is_valid():
            for error in format_form_errors(status_form):
                messages.error(request, error)

            return response

        # Get the initial comment and check user rights for the requested action.
        rights = self.get_rights(self.request.user)
        query_params = rights.comment_rights_query_params()
        query_params[PARAM_DASHBOARD] = PARAM_BOOLEAN_VALUE

        comment_id = kwargs["pk"]
        error, initial_comment = get_comment(query_params, comment_id, request_for_message=request)
        if error:
            return response

        status = status_form.cleaned_data["status"]
        if status == STATUS_SOFT_DELETED:
            if not rights.comment_can_delete(initial_comment):
                messages.error(request, "You can't delete the comment.")
                return response
            # Redirect to the comment list in case of a deletion. The current URL will not
            # exist anymore if the deletion succeeds.
            response = HttpResponseRedirect(reverse_lazy("comment_list"))

        # If it's not a deletion, it's a moderation. We can check now if the user
        # can moderate the comment before making the request to the comment server.
        elif not rights.comment_can_moderate(initial_comment):
            messages.error(request, "You can't moderate the comment.")
            return response

        # Update the comment status.
        error, comment = make_api_request(
            "PUT",
            comments_server_url(query_params, item_id=comment_id),
            request_for_message=request,
            auth=comments_credentials(),
            json=status_form.cleaned_data,
        )

        if error:
            return response

        messages.success(request, f"The comment has been successfully {status}.")

        # Post status update processing (e-mails)
        subject_prefix = f"[{comment['site_name'].upper()}]"
        # Send e-mails according to the status change.
        # Only if the status has been changed
        if status in [STATUS_VALIDATED, STATUS_REJECTED] and initial_comment["status"] != status:
            format_comment(comment)
            article = get_article_by_doi(comment["doi"])
            article_name = article.title_tex if article else ""
            context_data = {
                "full_name": comment["author_name"],
                "article_url": comment["base_url"],
                "article_name": article_name,
                "comment_url": f"{comment['base_url']}#article-comment-{comment['id']}",
                "email_signature": "The editorial team",
            }
            # Send an e-mail to the comment's author and the article's authors
            if status == STATUS_VALIDATED:
                subject = f"{subject_prefix} New published comment"
                send_email_from_template(
                    "mail/comment_validated.html",
                    context_data,
                    subject,
                    to=[comment["author_email"]],
                    from_collection=comment["site_name"],
                )

                # Send e-mail to the article's authors if the validation email
                # has not been sent yet.
                if (
                    app_settings.EMAIL_AUTHOR
                    and article
                    and initial_comment["validation_email_sent"] is False
                ):
                    author_data = []
                    author_contributions = article.get_author_contributions()
                    for contribution in author_contributions:
                        if contribution.corresponding and contribution.email:
                            author_data.append(contribution)
                    for author in author_data:
                        context_data["article_author_name"] = author.display_name()
                        subject = f"{subject_prefix} New published comment on your article"
                        send_email_from_template(
                            "mail/comment_new_article_author.html",
                            context_data,
                            subject,
                            to=[author.email],
                            from_collection=comment["site_name"],
                        )
            # Only send an e-mail to the comment's author
            elif status == STATUS_REJECTED:
                subject = f"{subject_prefix} Comment rejected"
                send_email_from_template(
                    "mail/comment_rejected.html",
                    context_data,
                    subject,
                    to=[comment["author_email"]],
                    from_collection=comment["site_name"],
                )
        return response
