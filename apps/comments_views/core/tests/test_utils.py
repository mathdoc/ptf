from copy import deepcopy
from datetime import datetime

import requests
import responses

from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.messages import get_messages
from django.test import TestCase
from django.test import override_settings
from django.urls import clear_script_prefix
from django.urls import reverse
from django.urls import set_script_prefix
from django.utils import translation

from ptf.tests.mixins import MessageDependentTestMixin

from ..utils import DEFAULT_API_ERROR_MESSAGE
from ..utils import add_api_error_message
from ..utils import api_request_wrapper
from ..utils import comments_credentials
from ..utils import comments_server_base_url
from ..utils import comments_server_url
from ..utils import format_comment
from ..utils import get_comment
from ..utils import get_user_dict
from ..utils import json_from_response
from ..utils import make_api_request


class UtilsTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        print("\n**App: 'comments_views.core' - Testing `utils.py`**")
        super().setUpClass()

    @override_settings(COMMENTS_VIEWS_API_BASE_URL="http://test/")
    def test_comments_server_url(self):
        self.assertEqual(comments_server_base_url(), "http://test")

        self.assertEqual(comments_server_url(), "http://test/api/comments/")

        query_params = {"param_1": "value_1", "param_2": "value_2"}

        self.assertEqual(
            comments_server_url(query_params),
            "http://test/api/comments/?param_1=value_1&param_2=value_2",
        )

        item_id = 4

        self.assertEqual(
            comments_server_url(query_params, item_id=item_id),
            "http://test/api/comments/4/?param_1=value_1&param_2=value_2",
        )

        self.assertEqual(
            comments_server_url(query_params, url_prefix="moderators", item_id=item_id),
            "http://test/api/moderators/4/?param_1=value_1&param_2=value_2",
        )

        del settings.COMMENTS_VIEWS_API_BASE_URL
        self.assertEqual(comments_server_base_url(), "https://comments.centre-mersenne.org")

    @override_settings(COMMENTS_VIEWS_API_CREDENTIALS=("login", "password"))
    def test_comments_credentials(self):
        credentials = comments_credentials()
        self.assertTrue(isinstance(credentials, tuple) and len(credentials) == 2)
        self.assertEqual(credentials[0], "login")
        self.assertEqual(credentials[1], "password")

        settings.COMMENTS_VIEWS_API_CREDENTIALS = ("login", "password", "_")
        self.assertRaises(AssertionError, comments_credentials)

        settings.COMMENTS_VIEWS_API_CREDENTIALS = "login"
        self.assertRaises(AssertionError, comments_credentials)

        settings.COMMENTS_VIEWS_API_CREDENTIALS = ["login", "password"]
        self.assertRaises(AssertionError, comments_credentials)

        del settings.COMMENTS_VIEWS_API_CREDENTIALS
        self.assertRaises(AttributeError, comments_credentials)

    def test_api_error_message(self):
        initial_message = "My initial message"
        message = add_api_error_message(initial_message, {})
        self.assertEqual(message, initial_message)

        api_json_data = {"error_message": "My error message"}
        message = add_api_error_message(initial_message, api_json_data)
        self.assertEqual(message, f'{initial_message}<br><br>"My error message"')

        message = add_api_error_message("", api_json_data)
        self.assertEqual(message, '"My error message"')

    def test_format_comment(self):
        comment_base = {
            "id": 1,
            "author_name": "Author 1",
            "date_submitted": "2023-04-01T05:48:32Z",
            "moderation_data": {"date_moderated": "2023-04-04T13:02:13Z", "moderator": 1},
            "base_url": "http://test",
            "moderators": [1, 2],
            "doi": "10.5802/crbiol.107",
        }
        comment = deepcopy(comment_base)
        format_comment(comment)

        date_submitted = datetime(2023, 4, 1, 5, 48, 32)
        self.assertTrue(isinstance(comment["date_submitted"], datetime))
        self.assertEqual(comment["date_submitted"], date_submitted)

        date_moderated = datetime(2023, 4, 4, 13, 2, 13)
        self.assertTrue(isinstance(comment["moderation_data"]["date_moderated"], datetime))
        self.assertEqual(comment["moderation_data"]["date_moderated"], date_moderated)

        base_url = "http://test/articles/10.5802/crbiol.107/"
        self.assertEqual(comment["base_url"], base_url)

        comment = deepcopy(comment_base)
        comment["base_url"] = "http://test/"
        format_comment(comment)
        self.assertEqual(comment["base_url"], base_url)

        # Test the correct link formatting when there's an URL prefix.
        with self.settings(SITE_URL_PREFIX="my_prefix"):
            # Emulate a script prefix
            set_script_prefix("/my_prefix")
            home_url = reverse("home")
            self.assertEqual(home_url, "/my_prefix/")

            comment = deepcopy(comment_base)
            comment["base_url"] = "http://test/my_prefix"
            format_comment(comment)
            self.assertEqual(
                comment["base_url"], "http://test/my_prefix/articles/10.5802/crbiol.107/"
            )
            clear_script_prefix()

    def test_format_comment_author_name(self):
        comment_base = {
            "id": 1,
            "author_name": "Author 1",
            "date_submitted": "2023-04-01T05:48:32Z",
            "moderation_data": {"date_moderated": "2023-04-04T13:02:13Z", "moderator": 1},
            "article_author_comment": False,
            "editorial_team_comment": False,
            "hide_author_name": False,
            "parent": {
                "id": 1,
                "author_name": "Parent author",
                "article_author_comment": False,
                "editorial_team_comment": False,
                "hide_author_name": False,
            },
            "base_url": "http://test",
            "moderators": [1, 2],
            "doi": "10.5802/crbiol.107",
        }

        with translation.override("fr"):
            comment = deepcopy(comment_base)
            format_comment(comment)
            self.assertEqual(comment["author_display_name"], "Author 1")
            self.assertEqual(comment["parent"]["author_display_name"], "Parent author")

            comment = deepcopy(comment_base)
            comment["article_author_comment"] = True
            comment["parent"]["editorial_team_comment"] = True
            format_comment(comment)
            self.assertEqual(comment["author_display_name"], "Author 1 (auteur de l'article)")
            self.assertEqual(
                comment["parent"]["author_display_name"], "Parent author (équipe éditoriale)"
            )

            comment = deepcopy(comment_base)
            comment["editorial_team_comment"] = True
            comment["hide_author_name"] = True
            comment["parent"]["article_author_comment"] = True
            format_comment(comment)
            self.assertEqual(comment["author_display_name"], "Équipe éditoriale")
            self.assertEqual(
                comment["parent"]["author_display_name"], "Parent author (auteur de l'article)"
            )

            comment = deepcopy(comment_base)
            comment["editorial_team_comment"] = True
            comment["hide_author_name"] = True
            comment["parent"]["article_author_comment"] = True
            comment["parent"]["hide_author_name"] = True
            format_comment(comment)
            self.assertEqual(comment["author_display_name"], "Équipe éditoriale")
            self.assertEqual(comment["parent"]["author_display_name"], "Auteur(s) de l'article")

    def test_user_dict(self):
        User.objects.create_user(
            username="u_1", email="u_1@test.test", first_name="User", last_name="1", pk=1
        )
        User.objects.create_user(
            username="u_2", email="u_2@test.test", first_name="User", last_name="2", pk=2
        )

        user_dict = get_user_dict()
        self.assertEqual(len(user_dict.keys()), 2)
        self.assertEqual(
            user_dict[1],
            {"first_name": "User", "last_name": "1", "email": "u_1@test.test", "pk": 1},
        )
        self.assertEqual(
            user_dict[2],
            {"first_name": "User", "last_name": "2", "email": "u_2@test.test", "pk": 2},
        )


class UtilsRequestTestCase(MessageDependentTestMixin, TestCase):
    dummy_url = "http://test"

    @classmethod
    def setUpClass(cls):
        print("\n**App: 'comments_views.core' - Testing `utils.py` with requests**")
        super().setUpClass()

    def setUp(self) -> None:
        self.response_params = {
            "method": "GET",
            "url": self.dummy_url,
            "status": 200,
            "content_type": "application/json",
            "json": {"My data": "Hello"},
        }
        self.setUpMessageData()
        return super().setUp()

    def prepare_response_and_request(self, url: str = "") -> tuple:
        mocked_request = self.request_factory.get("/comments")
        self.prepare_request(mocked_request)

        url = self.dummy_url if not url else url
        responses.add(**self.response_params)
        mocked_response = requests.get(url)
        return mocked_request, mocked_response

    @responses.activate
    def test_json_from_response(self):
        json_data = {"My data": "Hello"}
        self.response_params["json"] = json_data
        # 200 - OK
        mocked_request, mocked_response = self.prepare_response_and_request()
        error, data = json_from_response(mocked_response, mocked_request)
        self.assertFalse(error)
        self.assertEqual(data, json_data)
        # Test message
        messages = list(get_messages(mocked_request))
        self.assertEqual(len(messages), 0)

        # 301 - Redirect
        self.response_params["status"] = 301

        mocked_request, mocked_response = self.prepare_response_and_request()
        error, data = json_from_response(mocked_response, mocked_request)
        self.assertTrue(error)
        self.assertEqual(data, json_data)
        # Test message
        messages = list(get_messages(mocked_request))
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), DEFAULT_API_ERROR_MESSAGE)

        # 400 - Bad Request with error message
        self.response_params["status"] = 400
        json_data = {"My data": "Hello", "error_message": "My error message"}
        self.response_params["json"] = json_data

        mocked_request, mocked_response = self.prepare_response_and_request()
        error, data = json_from_response(mocked_response, mocked_request)
        self.assertTrue(error)
        self.assertEqual(data, json_data)
        # Test message
        messages = list(get_messages(mocked_request))
        self.assertEqual(len(messages), 1)
        self.assertEqual(
            str(messages[0]), f'{DEFAULT_API_ERROR_MESSAGE}<br><br>"My error message"'
        )

        # 200 OK - Without request
        self.response_params["status"] = 200
        mocked_request, mocked_response = self.prepare_response_and_request()
        error, data = json_from_response(mocked_response)
        self.assertFalse(error)
        self.assertEqual(data, json_data)

        # 204 - No Content
        self.response_params["status"] = 204
        del self.response_params["json"]
        mocked_request, mocked_response = self.prepare_response_and_request()
        error, data = json_from_response(mocked_response, mocked_request)
        self.assertFalse(error)
        self.assertEqual(data, {})
        # Test message
        messages = list(get_messages(mocked_request))
        self.assertEqual(len(messages), 0)

        # 200 - OK without JSON payload
        self.response_params["status"] = 200
        mocked_request, mocked_response = self.prepare_response_and_request()
        error, data = json_from_response(mocked_response, mocked_request)
        self.assertTrue(error)
        # Test message
        messages = list(get_messages(mocked_request))
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), DEFAULT_API_ERROR_MESSAGE)

        # 200 - OK without JSON payload without request
        self.response_params["status"] = 200
        mocked_request, mocked_response = self.prepare_response_and_request()
        error, data = json_from_response(mocked_response)
        self.assertTrue(error)

    @responses.activate
    def test_api_request_wrapper(self):
        """Only tests the case of RequestException."""
        # Request error - No response are registered for the url
        response = api_request_wrapper("GET", self.dummy_url)
        self.assertIsNone(response)

        # Test every implemented HTTP method
        self.response_params["method"] = "GET"
        responses.add(**self.response_params)
        response = api_request_wrapper("GET", self.dummy_url)
        self.assertEqual(response.json(), self.response_params["json"])

        self.response_params["method"] = "POST"
        responses.add(**self.response_params)
        response = api_request_wrapper("POST", self.dummy_url)
        self.assertEqual(response.json(), self.response_params["json"])

        self.response_params["method"] = "PUT"
        responses.add(**self.response_params)
        response = api_request_wrapper("PUT", self.dummy_url)
        self.assertEqual(response.json(), self.response_params["json"])

        # HEAD not implemented
        self.response_params["method"] = "HEAD"
        responses.add(**self.response_params)
        self.assertRaises(ValueError, api_request_wrapper, "HEAD", self.dummy_url)

    @responses.activate
    @override_settings(
        COMMENTS_VIEWS_API_BASE_URL="http://test/",
        COMMENTS_VIEWS_API_CREDENTIALS=("login", "password"),
    )
    def test_make_api_request(self):
        """Only tests the full process without that many cases."""
        # Request error
        self.response_params["url"] = self.dummy_url
        error, _ = make_api_request("GET", self.dummy_url, auth=comments_credentials())
        self.assertTrue(error)

        # With message
        request = self.request_factory.get("/")
        self.prepare_request(request)

        error, _ = make_api_request("GET", self.dummy_url, request, auth=comments_credentials())

        self.assertTrue(error)
        messages = list(get_messages(request))
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), DEFAULT_API_ERROR_MESSAGE)

        # JSON decode error
        del self.response_params["json"]
        responses.add(**self.response_params)

        error, _ = make_api_request("GET", self.dummy_url, auth=comments_credentials())
        self.assertTrue(error)

        # With messages
        responses.add(**self.response_params)
        request = self.request_factory.get("/")
        self.prepare_request(request)

        error, _ = make_api_request("GET", self.dummy_url, request, auth=comments_credentials())

        self.assertTrue(error)
        messages = list(get_messages(request))
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), DEFAULT_API_ERROR_MESSAGE)

        # OK
        json_data = {"key": "value"}
        self.response_params["json"] = json_data
        responses.add(**self.response_params)

        error, data = make_api_request("GET", self.dummy_url, auth=comments_credentials())

        self.assertFalse(error)
        self.assertEqual(data, json_data)

        # With messages
        request = self.request_factory.get("/")
        self.prepare_request(request)

        error, _ = make_api_request("GET", self.dummy_url, auth=comments_credentials())

        self.assertFalse(error)
        self.assertEqual(data, json_data)
        messages = list(get_messages(request))
        self.assertEqual(len(messages), 0)

        # Test other HTTP methods
        self.response_params["method"] = "POST"
        responses.add(**self.response_params)
        error, data = make_api_request("POST", self.dummy_url, auth=comments_credentials())
        self.assertFalse(error)
        self.assertEqual(data, json_data)

        self.response_params["method"] = "PUT"
        responses.add(**self.response_params)
        error, data = make_api_request("PUT", self.dummy_url, auth=comments_credentials())
        self.assertFalse(error)
        self.assertEqual(data, json_data)

        self.response_params["method"] = "DELETE"
        responses.add(**self.response_params)
        self.assertRaises(
            ValueError, make_api_request, "DELETE", self.dummy_url, auth=comments_credentials()
        )

    @responses.activate
    @override_settings(
        COMMENTS_VIEWS_API_BASE_URL="http://test/",
        COMMENTS_VIEWS_API_CREDENTIALS=("login", "password"),
    )
    def test_get_comment(self):
        url = "http://test/api/comments/4/?param_1=value_1"
        self.response_params["url"] = url
        self.response_params["status"] = 200
        mocked_request, _ = self.prepare_response_and_request(url)

        query_params = {"param_1": "value_1"}

        # Request OK
        # Test without request
        error, comment = get_comment(query_params, 4)
        self.assertFalse(error)
        self.assertEqual(comment, self.response_params["json"])

        # Test with request
        error, comment = get_comment(query_params, 4, mocked_request)
        self.assertFalse(error)
        self.assertEqual(comment, self.response_params["json"])
        messages = list(get_messages(mocked_request))
        self.assertEqual(len(messages), 0)

        # Request failed
        # Test without request
        self.response_params["status"] = 400
        mocked_request, _ = self.prepare_response_and_request(url)
        error, comment = get_comment(query_params, 4)
        self.assertTrue(error)
        self.assertEqual(comment, self.response_params["json"])

        # Test with request
        error, comment = get_comment(query_params, 4, mocked_request)
        self.assertTrue(error)
        self.assertEqual(comment, self.response_params["json"])
        messages = list(get_messages(mocked_request))
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), DEFAULT_API_ERROR_MESSAGE)
