from django.apps import AppConfig


class CommentsFrontCoreConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "comments_views.core"
