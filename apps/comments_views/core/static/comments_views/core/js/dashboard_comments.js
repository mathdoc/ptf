/** Stores the initial data of each table */
const tableRawData = new Map()
/** Stores the filtered data of each table */
const tableFilteredData = new Map()
/** Stores the selected sorting config of each table */
const tableSortConfig = new Map()
/** Stores the pagination config of each table */
const tablePaginationConfig = new Map()

/**
 * Returns the value of a table cell for sorting.
 * It is the `data-value` HTML attribute if present, else the text content of
 * the first child with class "cell-content".
 */
function cellValue(cell) {
    let value = cell.dataset.value || ""
    if (!value) {
        let content = cell.getElementsByClassName("cell-content")
        if (content) {
            value = content[0].innerText.trim()
        }
    }
    return value
}

/**
 * Return the HTML string representing a pagination button.
 *
 * @param {any} page
 * @param {boolean} active Whether the page is active
 * @param {boolean} fill   Whether the page is a fill ("...")
 * @returns
 */
function getpaginationButton(page, active=false, fill=false) {
    let buttonClass = "comment-pagination-page"
    buttonClass += active ? " comment-pagination-active" : ""
    buttonClass += fill ? "" : " comment-pagination-link"
    return `<button class="comment-pagination-button ${buttonClass}" data-page="${page}">${page}<button>`
}

/**
 * Returns an HTMLElement from an HTML string.
 *
 * @param {String} HTML representing a single element
 * @return {Element}
 */
function htmlToElement(html) {
    var template = document.createElement('template');
    html = html.trim(); // Never return a text node of whitespace as the result
    template.innerHTML = html;
    return template.content.firstChild;
}

/**
 * Renders a table with the provided rows.
 * Additionally paginate the rows with the current pagination config of the table.
 *
 * @param {HTMLElement} table
 * @param {Array} rows
 * @param {boolean} selectPage
 */
function renderTable(table, rows) {
    let paginationConfig = tablePaginationConfig.get(table)
    // Handle table pagination
    if (paginationConfig) {
        // Filters the row to display according to pagination config
        let selectedPage = paginationConfig.selectedPage
        rows = rows.filter((row, i) => i >= ((selectedPage - 1) * paginationConfig.itemPerPage)
                && i < (selectedPage * paginationConfig.itemPerPage)
        )

        // Deletes all existing pagination buttons
        let paginationElement = table.closest(".comment-list-section").querySelector(".comment-table-pagination")
        Array.from(paginationElement.getElementsByClassName("comment-pagination-page")).forEach((child) => {
            child.remove()
        })

        // Creates necessary pagination buttons
        let pageNumber = paginationConfig.pageNumber
        let buttonsToCreate = []
        switch (selectedPage) {
            case 1:
            case 2:
                buttonsToCreate = [2, 3]
                break
            case pageNumber:
                buttonsToCreate = [pageNumber - 2, pageNumber - 1]
                break
            default:
                buttonsToCreate = [selectedPage - 1, selectedPage, selectedPage + 1]
        }

        let active = selectedPage == 1
        let buttons = [getpaginationButton(1, active)]
        let maxPage = 1
        for (var page of buttonsToCreate) {
            if (page > pageNumber) {
                break
            }
            else if (page <= maxPage) {
                continue
            }

            if (page > maxPage + 1) {
                buttons.push(getpaginationButton("...", active=false, fill=true))
            }
            active = page == selectedPage
            buttons.push(getpaginationButton(page, active=active))
            maxPage = page
        }

        // Adds last button
        if (page < pageNumber) {
            if (page < (pageNumber - 1)) {
                buttons.push(getpaginationButton("...", active=false, fill=true))
            }
            active = selectedPage == pageNumber
            buttons.push(getpaginationButton(pageNumber, active))
        }

        // Adds all pagination buttons to the DOM
        let previousButton = table.closest(".comment-list-section").querySelector(".comment-pagination-previous")
        for (let i = buttons.length - 1; i >=0; i--) {
            previousButton.after(htmlToElement(buttons[i]))
        }
        // Update listeners
        setPaginationLink(paginationElement)
    }

    // Updates the table HTML
    let tbody = table.querySelector("tbody")
    tbody.innerHTML = rows.map((row) => row.outerHTML).join("")
    setTableContentListeners(table)
}


/**
 * Updates the table pagination config.
 *
 * @param {HTMLElement} table   The table
 * @param {*} value             The number of item per page. If "current", it keeps the existing value.
 */
function updatePaginationConfig(table, value) {
    let rows = tableFilteredData.get(table)
    if (value === "current") {
        value = tablePaginationConfig.get(table)?.itemPerPage
    }
    if (!value) {
        return
    }
    let pageNumber = Math.floor(rows.length / value)
    pageNumber += pageNumber * value == rows.length ? 0 : 1
    tablePaginationConfig.set(table, {itemPerPage: value, pageNumber: pageNumber, selectedPage: 1})
}


/**
 * Adds a handler for every pagination buttons click.
 * @param {*} paginationContainer Any element containing the pagination buttons. Used for CSS selection.
 */
function setPaginationLink(paginationContainer) {
    Array.from(paginationContainer.querySelectorAll(".comment-pagination-link:not(.comment-pagination-active)"))
         .forEach((btn) => btn.addEventListener("click", () => {
            let table = btn.closest(".comment-list-section").querySelector("table.comment-table")
            updateSelectedPage(table, parseInt(btn.dataset.page))
        }))
}


/**
 * Updates the selected table page. It also re-renders the table.
 * @param {*} table     The table
 * @param {*} page      The selected page. Can be the page number or "previous" or "next"
 */
function updateSelectedPage(table, page) {
    let paginationConfig = tablePaginationConfig.get(table)
    if (page === "next") {
        page = Math.min(paginationConfig.selectedPage + 1, paginationConfig.pageNumber)
    }
    else if (page === "previous") {
        page = Math.max(paginationConfig.selectedPage - 1, 1)
    }

    paginationConfig.selectedPage = page
    tablePaginationConfig.set(table, paginationConfig)
    renderTable(table, tableFilteredData.get(table))
}


/**
 * Sorts a table (numeric or text values).
 * The cell value used to sort is the HTML attribute `data-value` if present, else the text content
 * of the first child with class "cell-content"
 *
 * @param {Element} table   The table to sort
 * @param {number}  col     The column used to sort the table.
 * @param {boolean} asc     If true, sort ascending else descending
 */
function sortTable(table, col, asc=true) {
    let rows = tableFilteredData.get(table)
    let ordering = asc ? 1 : -1
    rows.sort((a, b) => {
        let valueA = cellValue(a.cells[col])
        let valueB = cellValue(b.cells[col])
        if (valueA != "" && valueB != "" && !isNaN(valueA) && !isNaN(valueB)) {
            return (valueA - valueB) * ordering
        }
        return valueA.localeCompare(valueB) * ordering
    })
    return rows
}


/**
 * Sets the disabled attribute to true for submit buttons of class button-deactivable.
 */
function disableSubmitButtons() {
    Array.from(document.querySelectorAll("button[type='submit'].button-deactivable")).forEach((button) => {
        button.setAttribute("disabled", true)
    })
}


/**
 * Adds relevant event listeners to click actions on the table changing content (=rows).
 *
 * TODO: Instead of re-adding the event listeners on every change, just add them to the table itself (or any
 * static parent element ) and check the target there to decide what to do:
 * @param {HTMLElement} table
 */
function setTableContentListeners(table) {
    Array.from(table.querySelectorAll("tbody tr")).forEach((row) => {
        row.addEventListener("click", () => {
            href = row.dataset.href
            if (href) {
                window.location.href = href
            }
        })
    })
    // Prevent bubbling of click events when clicking a link, otherwise it triggers the click handler on `tr` elements.
    Array.from(table.querySelectorAll(".comment-table-wrapper a")).forEach((link) => {
        link.addEventListener("click", (event) => {
            event.stopPropagation()
        })
    })

    // Show more/show less overlays triggers
    Array.from(table.getElementsByClassName("show-more-overlay")).forEach((element) => {
        let contentElement = element.parentElement.querySelector(".comment-content")
        if (element.classList.contains("more")) {
            contentElement.classList.remove("cell-content-center")
            element.classList.remove("display-none")
            element.addEventListener("click", (event) => {
                event.stopPropagation()
                Array.from(element.parentElement.getElementsByClassName("show-more-overlay")).forEach((el) => {
                    el.classList.toggle("display-none")
                })
                showMoreAnimation[0][0].maxHeight = getComputedStyle(contentElement).getPropertyValue("--comment-list-max-height")
                contentElement.animate(...showMoreAnimation)
            })
        }
        else if (element.classList.contains("less")) {
            element.addEventListener("click", (event) => {
                event.stopPropagation()
                Array.from(element.parentElement.getElementsByClassName("show-more-overlay")).forEach((el) => {
                    el.classList.toggle("display-none")
                })
                showLessAnimation[0][3].maxHeight = getComputedStyle(contentElement).getPropertyValue("--comment-list-max-height")
                contentElement.animate(...showLessAnimation)
            })
        }
    })
}

const showMoreAnimation = [
    [
        { maxHeight: "8em", offset: 0 }, // value to be replaced
        { maxHeight: "800px", offset: 0.6 },
        { maxHeight: "none", offset: 1 },
    ],
    {
        duration: 400,
        iterations: 1,
        fill: "forwards"
    }
]
const showLessAnimation = [
    [
        { maxHeight: "none", offset: 0 },
        { maxHeight: "1600px", offset: 0.1 },
        { maxHeight: "800px", offset: 0.25 },
        { maxHeight: "8em", offset: 1 }, // value to be replaced
    ],
    {
        duration: 300,
        iterations: 1,
        fill: "forwards"
    }
]


/**
 * Init script.
 */
document.addEventListener("DOMContentLoaded", () => {

    // Remove Show more/show less overlays if the content does not overflow
    Array.from(document.getElementsByClassName("show-more-overlay")).forEach((element) => {
        let contentElement = element.parentElement.querySelector(".comment-content")
        if (contentElement.scrollHeight <= contentElement.clientHeight) {
            element.remove()
        }
    })

    // Menu button
    let menu = document.getElementById("comment-menu")
    if (menu) {
        menu.addEventListener("click", () => {
            let menuContent = document.getElementById("comment-menu-content")
            if (menuContent) {
                menuContent.classList.toggle("display-block")
            }
        })
    }

    // Comment edit button
    let commentEditFormSubmit = Array.from(document.getElementsByClassName("comment-content-submit"))
    if (commentEditFormSubmit) {
        commentEditFormSubmit.forEach((btn) => btn.addEventListener("click", () => {
            let commentEditForm = document.querySelector("#comment-details-content form")
            if (commentEditForm){
                disableSubmitButtons()
                commentEditForm.submit()
            }
        }))
    }

    // Disable all submit buttons on form submission
    Array.from(document.getElementsByClassName("form-deactivate-on-submit")).forEach((form) => {
        form.addEventListener("submit", () => {
            disableSubmitButtons()
        })
    })

    Array.from(document.querySelectorAll(".comment-table .comment-content a")).forEach((link) => {
        link.setAttribute("target", "_blank")
    })

    /**
     * Table related code.
     */
    // Init table data structures
    const tablesInit = () => {
        Array.from(document.querySelectorAll("table.comment-table")).forEach((table) => {
            let rows = Array.from(table.querySelectorAll("tbody tr"))

            tableRawData.set(table, rows)
            tableFilteredData.set(table, rows)
            setTableContentListeners(table)
        })

        // Table pagination
        Array.from(document.querySelectorAll("select.comment-table-pagination-count")).forEach((select) => {
            let table = select.closest(".comment-list-section")?.querySelector("table.comment-table")
            let value = parseInt(select.value)
            updatePaginationConfig(table, value)
            renderTable(table, tableFilteredData.get(table))
            select.addEventListener("change", () => {
                let value = parseInt(select.value)
                updatePaginationConfig(table, value)
                renderTable(table, tableFilteredData.get(table))
            })
        })

        Array.from(document.querySelectorAll(".comment-pagination-previous"))
            .forEach((btn) => btn.addEventListener("click", () => {
                let table = btn.closest(".comment-list-section").querySelector("table.comment-table")
                updateSelectedPage(table, "previous")
            }))

        Array.from(document.querySelectorAll(".comment-pagination-next"))
            .forEach((btn) => btn.addEventListener("click", () => {
                let table = btn.closest(".comment-list-section").querySelector("table.comment-table")
                updateSelectedPage(table, "next")
            }))

        // Table search input
        Array.from(document.getElementsByClassName("table-search")).forEach((searchInput) => {
            searchInput.addEventListener("input", () => {
                let searchValue = searchInput.value.trim().toLowerCase()
                let table = searchInput.closest(".comment-list-section").querySelector("table.comment-table")
                let rows = tableRawData.get(table)
                searchInput.classList.remove("active")
                if (searchValue) {
                    searchInput.classList.add("active")
                    rows = rows.filter((row) => {
                        let cellsContent = row.querySelectorAll("td .cell-content")
                        for (let i = 0; i < cellsContent.length; i++) {
                            let text = cellsContent[i].innerText || cellsContent[i].textContent
                            if (text.toLowerCase().indexOf(searchValue) > -1) {
                                return true
                            }
                        }
                        return false
                    })
                }

                // This assignement must be done before the table sort !
                tableFilteredData.set(table, rows)
                // Sort the table again
                let sortConfig = tableSortConfig.get(table)
                if (sortConfig) {
                    rows = sortTable(table, ...Object.values(sortConfig))
                }
                updatePaginationConfig(table, "current")
                renderTable(table, rows)


                // Update the table count
                let section = searchInput.closest(".comment-list-section")
                if (section) {
                    let tableCount = section.querySelector(".table-badge-count")
                    if (tableCount) {
                        tableCount.textContent = rows.length
                    }
                }
            })
        })


        // Table sort
        Array.from(document.querySelectorAll("table.comment-table")).forEach((tableElement) => {
            Array.from(tableElement.getElementsByTagName("th")).forEach((element, i) => {
                if (!element.classList.contains("cell-header-sort")) {
                    return
                }
                element.addEventListener("click", () => {
                    let table = element.closest("table.comment-table")
                    let asc = !element.classList.contains("ascending")
                    tableSortConfig.set(table, {col: i, asc: asc})
                    let rows = sortTable(table, i, asc)
                    updatePaginationConfig(table, "current")
                    renderTable(table, rows)
                    // We don't have to explicitely set tableFilteredData.set(table, rows) because the sorting
                    // is done in place.

                    // Update CSS styling of the table headers (icon, color)
                    Array.from(table.getElementsByClassName("cell-header-sort")).forEach(
                        (el) => el.classList.remove("sorted", "ascending")
                    )
                    if (asc) {
                        element.classList.add("ascending")
                    }
                    element.classList.add("sorted")
                })

                // Initially sort the table if the column has "sorted" class
                if (element.classList.contains("sorted")) {
                    let table = element.closest("table.comment-table")
                    let asc = element.classList.contains("ascending")
                    tableSortConfig.set(table, {col: i, asc: asc})
                    let rows = sortTable(table, i, asc)
                    updatePaginationConfig(table, "current")
                    renderTable(table, rows)
                }
            })
        })
    }

    // Initialize the table data after making sure the math elements are set
    if (window.mathjaxTypeset && typeof window.mathjaxTypeset == "function") {
        window.mathjaxTypeset(["table.comment-table"])
        MathJax.startup.promise.then(tablesInit)
    }
    else {
        tablesInit()
    }

})
