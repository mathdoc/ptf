from abc import ABC
from abc import abstractmethod

from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import AnonymousUser

from .rights import AbstractUserRights


class AbstractCommentRightsMixin(ABC):
    @property
    @abstractmethod
    def rights_class(self) -> type[AbstractUserRights]:
        pass

    def get_rights(self, user: AbstractBaseUser | AnonymousUser) -> AbstractUserRights:
        return self.rights_class(user)
