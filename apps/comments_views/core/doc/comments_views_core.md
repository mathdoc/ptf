# Comments views core application

This application, `comments_views.core`, contains all the code shared between the `comments_views.journal` (doc [here](../../journal/doc/comments_views_journal.md)) and the `comments_moderation` (doc [here](/sites/ptf_tools/comments_moderation/doc/comments_moderation.md)) applications. \
This provides base code for interfacing with the comments database (see [`comments_database`](/sites/comments_site/comments_database/doc/comments_database.md) application)


## I. Comments dashboard templates and views

This application contains the base templates and views for the **Comments dashboard**. \
The comments dashboard is used in the prod journal websites to display the comments of a SSO authenticated user. \
It's used in ptf_tools (Trammel) to display all the comments an authenticated user can moderate.

### I.1 [Views](../views.py)

The core application offers an abstract view for a list of comments and a abstract view for a single comment details. \
These views are "abstract" in the sense that they require their descendent to implement the [`AbstractCommentRightsMixin`](../mixins.py) methods.

### I.2 [Comment CKEditor config](../ckeditor_config.py)

The comment form on an article page expects a CKEditor config associated to the `"comment"` key. A default configuration is available in [`ckeditor_config.py`](../ckeditor_config.py).
Same for the comment in the comments dashboard: it expects a `"comment_autogrow"` CKEditor config.

## II. [Rights objects](../rights.py)

The abstract views are working with a "rights" object which is responsible for assessing a given user rights over a specific comment or a collection. \
The core application implements an abstract [`AbstractUserRights`](../rights.py) object which is extended by both `comments_views.journal` and `comments_moderation.journal` applications. Each "sub-application" handles different user objects, thus the need to separate and delegate the rights handling.


The rights are easily appended to the relevant view via [mixins](../mixins.py).

## III. Settings

The Django site requires the following settings to perform API calls to the comments database server:

  - `COMMENTS_VIEWS_API_BASE_URL` - The base URL of the comments database server.
  - `COMMENTS_VIEWS_API_CREDENTIALS` - The credentials to authenticate against the comments database server in the form `("username", "password")`.
  - `COMMENTS_VIEWS_EMAIL_AUTHOR` (default `True`) - Whether to send an e-mail to the article's authors when a comment is validated by a moderator. TODO: Should be moved to comments_moderation app (cf. [`app_settings.py`](../app_settings.py)).
  - `COMMENTS_VIEWS_ORCID_BASE_URL` (default `https://orcid.org/`) - Base URL used to generate ORCID record links. This should only be used in test to use https://sandbox.orcid.org/ domain instead of https://orcid.org/
