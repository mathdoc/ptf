from abc import ABC
from abc import abstractmethod

from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import AnonymousUser


class AbstractUserRights(ABC):
    COMMENT_POST_URL = ""
    user: AbstractBaseUser | AnonymousUser

    def __init__(self, user: AbstractBaseUser | AnonymousUser):
        self.user = user

    @abstractmethod
    def get_user_admin_collections(self) -> list[str]:
        pass

    @abstractmethod
    def get_user_staff_collections(self) -> list[str]:
        pass

    @abstractmethod
    def comment_rights_query_params(self) -> dict:
        """
        Populates the correct query parameters according to the current user rights.
        The comment server will filter the comments queryset according to these filters.
        """

    @abstractmethod
    def comment_can_delete(self, comment: dict) -> bool:
        """Whether the current user can delete the provided comment."""

    @abstractmethod
    def comment_can_edit(self, comment: dict) -> bool:
        """Whether the current user can edit the provided comment."""

    @abstractmethod
    def comment_can_moderate(self, comment: dict) -> bool:
        """Whether the current user can moderate the provided comment."""

    @abstractmethod
    def comment_can_manage_moderators(self, comment) -> bool:
        """
        Whether the curent user can manage the moderators of the given comment.
        """

    def is_admin_moderator(self) -> bool:
        """Whether the current user can manage all other moderators."""
        return False

    def is_staff_moderator(self) -> bool:
        """Whether the current user can manage base moderators."""
        return False
