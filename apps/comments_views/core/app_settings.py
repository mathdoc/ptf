from django.conf import settings


class AppSettings:
    """Wrapper for django settings related to this application."""

    def __init__(self):
        self.prefix = "COMMENTS_VIEWS_"

    def _setting(self, name, default):
        """
        Gets the provided setting with the app prefix (see `self.prefix`).
        Params:
            - `name`:       The name of the setting.
            - `default`:    Fallback if the settings is not defined.
        """
        return getattr(settings, self.prefix + name, default)

    @property
    def API_BASE_URL(self) -> str:
        """
        The base URL of the comments database server.
        Falls back to https://comments.centre-mersenne.org.
        """
        return self._setting("API_BASE_URL", "https://comments.centre-mersenne.org")

    @property
    def API_CREDENTIALS(self):
        """
        Credentials used to authenticate when performing API calls to the comments
        server.
        """
        return self._setting("API_CREDENTIALS", None)

    @property
    def EMAIL_AUTHOR(self) -> bool:
        """
        Whether to send an e-mail to the article's authors when a comment is validated
        by a moderator.
        Default: True.

        TODO: This should be included in the comments_moderation app instead.
        There is a design error regarding the POST function of
        `CommentDashboardDetailsView`. It's used differently by comments_views/journal
        and comments_moderation apps. The POST logic for the moderation should be
        overwritten in comments_moderation app instead of being located
        in comments_views/core.
        """
        return self._setting("EMAIL_AUTHOR", True)

    @property
    def ORCID_BASE_URL(self) -> str:
        """
        Base URL used to generate ORCID record links.

        This should only be used in test to use https://sandbox.orcid.org/ domain
        instead of https://orcid.org/
        """
        url = self._setting("ORCID_BASE_URL", "https://orcid.org/")
        if url[-1] != "/":
            url += "/"
        return url


app_settings = AppSettings()
