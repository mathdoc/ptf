from django.apps import AppConfig


class CommentsFrontJournalConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "comments_views.journal"
