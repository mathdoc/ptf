from django.http import HttpRequest

SESSION_PENDING_COMMENT = "pending_comment"


def pending_comment_session_key(doi: str) -> str:
    """
    Returns the session key for a pending comment.
    There can be max. one pending comment per session per DOI.
    """
    return f"{SESSION_PENDING_COMMENT}_{doi}"


def add_pending_comment(request: HttpRequest, doi: str, comment: dict):
    """Stores a comment in the current session."""
    request.session[pending_comment_session_key(doi)] = comment


def get_pending_comment(request: HttpRequest, doi: str) -> dict | None:
    """
    Retrieves the pending comment associated to the given DOI from the session, if any.
    """
    return request.session.get(pending_comment_session_key(doi))


def delete_pending_comment(request: HttpRequest, doi: str):
    """Removes the pending comment associated to the given DOI stored in the session."""
    session_key = pending_comment_session_key(doi)
    if session_key in request.session:
        del request.session[session_key]
