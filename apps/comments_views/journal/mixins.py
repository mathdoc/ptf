from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth import logout
from django.http import HttpRequest
from django.http import HttpResponseRedirect
from django.urls import reverse

from ptf.url_utils import format_url_with_params

from ..core.mixins import AbstractCommentRightsMixin
from .models import OIDCUser
from .rights import OIDCUserRights


class OIDCCommentRightsMixin(AbstractCommentRightsMixin):
    @property
    def rights_class(self) -> type[OIDCUserRights]:
        return OIDCUserRights


class SSOLoginRequiredMixin:
    """
    Redirects to the `remote_login` URL if the current user is not a `OIDCUser`.
    """

    def dispatch(self, request: HttpRequest, *args, **kwargs):
        if not isinstance(request.user, OIDCUser):
            redirect_uri = format_url_with_params(
                reverse("remote_login"), {REDIRECT_FIELD_NAME: request.build_absolute_uri()}
            )
            if request.user.is_authenticated:
                logout(request)
            return HttpResponseRedirect(redirect_uri)

        return super().dispatch(request, *args, **kwargs)  # type:ignore
