from comments_api.constants import PARAM_USER
from comments_api.constants import STATUS_CAN_DELETE
from comments_api.constants import STATUS_CAN_EDIT
from comments_api.constants import STATUS_MODERATED

from ..core.rights import AbstractUserRights
from .app_settings import app_settings
from .models import OIDCUser


class OIDCUserRights(AbstractUserRights):
    """
    Comment rights for a comment's author. \\
    A comment's author can only perform actions related to his/her comments.
    """

    COMMENT_POST_URL = "submit_comment"

    def get_user_admin_collections(self) -> list[str]:
        return []

    def get_user_staff_collections(self) -> list[str]:
        return []

    def comment_rights_query_params(self) -> dict:
        """
        The available comments are limited to the user's comment for a regular user.
        """
        query_params = {}
        if isinstance(self.user, OIDCUser):
            query_params[PARAM_USER] = self.user.get_user_id()
        return query_params

    def comment_can_delete(self, comment: dict) -> bool:
        """
        A comment's author can delete his/her comment only if the comment
        has not been moderated yet. \\
        The behavior can be adapted with `DELETE_BEFORE_MODERATION` and
        `DELETE_AFTER_MODERATION` settings.
        DELETE_AFTER_MODERATION UPDATE: Setting disabled on comment server side.
        """
        return (
            isinstance(self.user, OIDCUser)
            and self.user.get_user_id() == comment.get("author_id")
            and (
                (
                    comment.get("status") in STATUS_CAN_DELETE
                    and app_settings.DELETE_BEFORE_MODERATION
                )
                or (
                    comment.get("status") in STATUS_MODERATED
                    and app_settings.DELETE_AFTER_MODERATION
                )
            )
        )

    def comment_can_edit(self, comment: dict) -> bool:
        """
        A comment's author can edit his/her comment only if
                -   it has not been validated yet.
            AND -   no moderator have been assigned to the comment
                    or `EDIT_BEFORE_MODERATION` is True
        """
        return (
            isinstance(self.user, OIDCUser)
            and self.user.get_user_id() == comment.get("author_id")
            and comment.get("status") in STATUS_CAN_EDIT
            and (
                app_settings.EDIT_BEFORE_MODERATION
                or (
                    not isinstance(comment.get("moderators"), list)
                    or len(comment["moderators"]) == 0
                )
            )
        )

    def comment_can_moderate(self, comment: dict) -> bool:
        """
        Base users can't moderate comments.
        """
        return False

    def comment_can_manage_moderators(self, comment) -> bool:
        """Base users can't manage moderators"""
        return False
