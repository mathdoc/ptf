var commentCkeditorWaitTime = 0
const tagRegex = /<.*?>|\n/g

const collapsibleElements = new Map()
/**
 * Dynamically enables/disables the comment form button according to its content.
 */
function ckeditorFormSubmit() {
    if (!window.CKEDITOR || !CKEDITOR.instances) {
        if (commentCkeditorWaitTime > 5000) {
            return
        }
        setTimeout(ckeditorFormSubmit, 100)
        commentCkeditorWaitTime += 100
        return
    }

    // Delay the action to wait for CKEditor to be correctly initialized
    setTimeout(() => {
        for (let instanceName in CKEDITOR.instances) {
            let instance = CKEDITOR.instances[instanceName]
            let instanceForm = instance.element.$?.closest("form.comment-form")
            let submitButton = instanceForm?.querySelector("button[type=\"submit\"]")
            if (submitButton) {
                disableCkeditorForm(instance, submitButton)
                // Listen to the inputChange event with throttling
                var disableCkeditorTimer = false
                instance.on("change", () => {
                    if (disableCkeditorTimer) {
                        return
                    }
                    disableCkeditorTimer = true
                    setTimeout(() => {
                        disableCkeditorForm(instance, submitButton)
                        disableCkeditorTimer = false
                    }, 500)
                })
            }
        }
    }, 800)
}


function disableCkeditorForm(instance, btn) {
    let actualData = instance.getData().replace(tagRegex, "")
    if (actualData.length < 15) {
        btn.setAttribute("disabled", true)
        return
    }
    btn.removeAttribute("disabled")
}

/**
 * Show more/show less overlays.
 * It requires the following settings to work properly:
 *      -   You must include a show more element with ID `show-more-overlay`. It will be cloned everywhere required.
 *      -   The target element (with limited height) must have a `background-color` declared explicitely in a style.
 *          You must add `apply-show-more-overlay` to its class style to trigger the overlay logic.
 *      -   The target element must have a parent with `position: relative`.
 *          The overlay is appended after the target element as absolute.
 *      -   If the target element does not contain `apply-show-less-overlay`, the overlay is destroyed after being
 *          clicked. Otherwise the target element can be collapsed again.
 */
function showMoreOverlay(commentSection) {
    let showMoreElement = document.getElementById("show-more-overlay")
    if (showMoreElement) {
        Array.from(commentSection.getElementsByClassName("apply-show-more-overlay")).forEach((element) => {
            // Add the overlay only if it overflows
            if (element.scrollHeight > element.clientHeight) {
                // Store the element's initial height
                collapsibleElements.set(element, {initialHeight: element.clientHeight})
                // Clone overlay element and sets a nice linear-gradient background-color
                let overlay = showMoreElement.cloneNode(true)
                overlay.classList.remove("display-none")
                overlay.querySelector(".show-less-span").style.display = "none"

                let backgroundColor = window.getComputedStyle(element).getPropertyValue("background-color")
                let backgroundRgb = backgroundColor.match(/[\d.]+/g)
                if (!backgroundRgb.length >= 3) {
                    backgroundRgb = [0, 0, 0]
                }
                backgroundColor = backgroundRgb.slice(0, 3).join(", ")
                let colorString = `linear-gradient(rgba(${backgroundColor}, 0.35), rgba(${backgroundColor}, 1))`
                overlay.style.background = colorString

                // Overlay user action.
                // Collapse/Uncollapse the content according to configuration.
                overlay.addEventListener("click", () => {
                    if (overlay.classList.contains("show-less-overlay")) {
                        let initHeight = collapsibleElements.get(element).initialHeight
                        let showLessAnimation = [
                            { maxHeight: `${element.scrollHeight}px`, offset: 0 },
                            { maxHeight: `${(initHeight + element.scrollHeight) / 2}px`, offset: 0.3 },
                            { maxHeight: `${initHeight}px`, offset: 0.9 },
                        ]
                        overlay.querySelector(".show-more-span").style.display = "initial"
                        overlay.querySelector(".show-less-span").style.display = "none"
                        overlay.classList.remove("show-less-overlay")
                        element.animate(showLessAnimation, {
                            duration: 50,
                            iterations: 1,
                            fill: "forwards"
                        })
                    }
                    else {
                        let showMoreAnimation = [
                                { maxHeight: `${element.clientHeight}px`, offset: 0 },
                                { maxHeight: `${(element.clientHeight + element.scrollHeight) / 2}px`, offset: 0.3 },
                                { maxHeight: `${element.scrollHeight}px`, offset: 1 }
                        ]
                        if (element.classList.contains("apply-show-less-overlay")) {
                            overlay.classList.add("show-less-overlay")
                            overlay.querySelector(".show-more-span").style.display = "none"
                            overlay.querySelector(".show-less-span").style.display = "initial"
                        }
                        element.animate(showMoreAnimation, {
                            duration: 150,
                            iterations: 1,
                            fill: "forwards"
                        })
                        if (!element.classList.contains("apply-show-less-overlay")) {
                            overlay.remove()
                        }
                    }
                })
                element.after(overlay)
            }
        })
    }
}


/**
 * All code related to the comments section.
 * It heavily depends on some function and variables present in `ptf.js`.
 * Be careful when modifying one or the other.
 */
function articleComments() {

    // Check whether the comment section is there
    var commentSection = document.getElementById('article-comments-section')
    if (commentSection == null) {
        return
    }

    // We need to typeset the potential equations loaded in the comments
    // Function defined in `ptf.js`
    requestMathjaxTypesetting(['#article-comments-section'])

    // Add event listener for comment reply button
    Array.from(commentSection.getElementsByClassName('article-comment-reply')).forEach((replyButton) => {
        replyButton.addEventListener('click', function () {
            // Get the reply comment form and move it below the selected comment
            const commentReplyForm = commentSection.querySelector('#add-comment-reply')
            if (commentReplyForm) {
                commentReplyForm.classList.add('add-comment-reply-active')
                const commentId = parseInt(this.dataset.commentId)

                // Get the comment-wrapper element and append the new form as its last child
                const commentWrapper = commentSection.querySelector(`#article-comment-${commentId} > .comment-wrapper`)
                const replyingTo = commentReplyForm?.querySelector('.comment-replying-to > a')

                const authorName = commentWrapper.getElementsByClassName('comment-author')[0]?.textContent.trim()
                // Add author name to reply form and fill hidden form field.
                // If replyingTo is null, that means the user is not authenticated so we don't fill the non-existing form
                if (replyingTo) {
                    replyingTo.setAttribute('href', `#article-comment-${commentId}`)
                    replyingTo.textContent = authorName
                    Array.from(commentReplyForm.getElementsByTagName('input')).forEach(function (input) {
                        if (
                            input.getAttribute('type') == 'hidden'
                            && input.getAttribute('name').endsWith('parent')
                        ) {
                            input.setAttribute('value', commentId)
                        }
                        else if (
                            input.getAttribute('type') == 'hidden'
                            && input.getAttribute('name').endsWith('parent_author_name')
                        ) {
                            input.setAttribute('value', authorName)
                        }
                    })
                }

                if (commentWrapper) {
                    // Updates the redirect URL in the authentication link with information on the clicked comment
                    let linkTag = commentReplyForm.querySelector("a.comment-sign-in-button")
                    if (linkTag) {
                        let link = linkTag.getAttribute("href")
                        let url = new URL(link)
                        let redirectUrl = url.searchParams.get("next")
                        if (redirectUrl) {
                            let redirectUrlObject = new URL(redirectUrl)
                            redirectUrlObject.hash = `#article-comment-${commentId}`
                            redirectUrlObject.searchParams.set("commentReplyTo", `${commentId}:~:${authorName}`)
                            url.searchParams.set("next", redirectUrlObject.toString())
                            linkTag.setAttribute("href", url.toString())
                        }
                    }

                    // We set the hash to the replied comment to activate the `:target` css pseudo class
                    location.hash = `article-comment-${commentId}`
                    commentWrapper.after(commentReplyForm)
                    commentReplyForm.scrollIntoView()
                }
            }

        })
    })

    // Make all comment links `target="_blank"`
    Array.from(commentSection.querySelectorAll('.comment-body a')).forEach(function (link) {
        link.setAttribute('target', '_blank')
        link.setAttribute('rel', 'nofollow')
    })

    Array.from(commentSection.querySelectorAll('.comment-buttons .comment-get-link')).forEach(function (button) {
        button.addEventListener('click', function (event) {
            let currentUrl = window.location.href.replace(/#.*$/, '')
            let commentAnchor = this.getAttribute('href')
            navigator.clipboard.writeText(`${currentUrl}${commentAnchor}`)
            event.preventDefault()
        })
    })

    // Comment form cancel button
    let formCancelButton = commentSection.querySelector('.comment-form .cancel-button')
    if (formCancelButton) {
        formCancelButton.addEventListener('click', function () {
            const commentReplyForm = document.getElementById('add-comment-reply')
            if (commentReplyForm) {
                commentReplyForm.classList.remove('add-comment-reply-active')
            }
            // Remove the commentReplyTo query parameter from the URL
            url = new URL(location.href)
            url.searchParams.delete("commentReplyTo")
            window.history.pushState(null, "", url.toString())
        })
    }

    // Disable all submit buttons on form submission
    Array.from(commentSection.getElementsByClassName("form-deactivate-on-submit")).forEach((form) => {
        form.addEventListener("submit", () => {
            Array.from(commentSection.querySelectorAll("button[type='submit'].button-deactivable")).forEach((button) => {
                button.setAttribute("disabled", true)
            })
        })
    })

    // Set customDropdowns
    if (window.setCustomDropdowns && typeof window.setCustomDropdowns == "function") {
        window.setCustomDropdowns()
    }

    // Show more overlays
    showMoreOverlay(commentSection)

    // Functions located in `ptf.js`
    ckeditorRelated()
    scrollToRequestedAnchor(hard=true)
    ckeditorFormSubmit()
}
