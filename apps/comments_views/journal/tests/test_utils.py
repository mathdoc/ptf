from django.test import TestCase

from ptf.tests.mixins import MessageDependentTestMixin

from ..utils import add_pending_comment
from ..utils import delete_pending_comment
from ..utils import get_pending_comment
from ..utils import pending_comment_session_key


class UtilsTestCase(MessageDependentTestMixin, TestCase):
    @classmethod
    def setUpClass(cls):
        print("\n**App: 'comments_views.journal' - Testing `utils.py`**")
        super().setUpClass()

    def setUp(self) -> None:
        self.setUpMessageData()
        super().setUp()

    def test_pending_comment(self):
        doi = "10.5802/crbiol.107"
        sess_key = pending_comment_session_key(doi)
        self.assertEqual(sess_key, f"pending_comment_{doi}")

        request = self.request_factory.get("/comments")
        self.prepare_request(request)

        self.assertTrue(get_pending_comment(request, doi) is None)
        comment = {
            "id": 1,
            "author": "Author",
            "sanitized_html": "<p>My wonderful comment<p>",
            "doi": doi,
        }

        add_pending_comment(request, doi, comment)
        self.assertEqual(get_pending_comment(request, doi), comment)

        delete_pending_comment(request, doi)
        self.assertTrue(get_pending_comment(request, doi) is None)
