from django.conf import settings
from django.contrib.sessions.middleware import SessionMiddleware
from django.http import HttpRequest
from django.test import TestCase
from django.test import override_settings

from ..auth import customOIDCAuthenticationBackend
from ..models import OIDCUser
from ..rights import OIDCUserRights


class RightsTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        print("\n**App: 'comments_views.journal' - Testing `rights.py`**")
        super().setUpClass()

    def setUp(self) -> None:
        self.backend = customOIDCAuthenticationBackend()
        dummy_func = lambda x: x
        self.session_middleware = SessionMiddleware(dummy_func)
        request = HttpRequest()
        self.session_middleware.process_request(request)
        self.backend.request = request
        return super().setUp()

    def get_user(self) -> OIDCUser:
        claims = {
            "sub": 1,
            "email": "user_oidc_1@test.test",
            "first_name": "User",
            "last_name": "OIDC 1",
            "name": "User OIDC 1",
            "provider": "orcid",
            "provider_uid": "0000-0000-0000-0001",
        }
        return self.backend.create_user(claims)

    def get_comment(self) -> dict:
        return {
            "id": 1,
            "doi": "10.5802/crbiol.107",
            "sanitized_html": "<p>My wonderful comment</p>",
            "status": "validated",
            "date_submitted": "2023-04-01T00:00:00Z",
            "date_last_modified": "2023-04-01T00:00:00Z",
            "article_author_comment": False,
            "editorial_team_comment": False,
            "author_id": 1,
            "author_email": "user_oidc_1@test.test",
            "author_name": "User OIDC 1",
            "author_provider": "ORCID",
            "author_provider_uid": "0000-0000-0000-0001",
            "site": 2,
            "site_name": "crbiol",
            "parent": None,
            "parent_author_name": None,
            "base_url": "http://crbiol.test",
            "moderation_data": {
                "moderator": 10,
                "comment": 1,
                "date_created": "2023-04-01T12:00:00Z",
                "date_moderated": "2023-04-01T12:00:00Z",
            },
            "moderators": [],
        }

    # We override the following settings in order to delete them afterwards
    # to emulate default settings
    @override_settings(
        COMMENTS_VIEWS_EDIT_BEFORE_MODERATION=False,
        COMMENTS_VIEWS_DELETE_BEFORE_MODERATION=True,
        COMMENTS_VIEWS_DELETE_AFTER_MODERATION=False,
    )
    def test_default_rights(self):
        del settings.COMMENTS_VIEWS_EDIT_BEFORE_MODERATION
        del settings.COMMENTS_VIEWS_DELETE_BEFORE_MODERATION
        del settings.COMMENTS_VIEWS_DELETE_AFTER_MODERATION
        user = self.get_user()
        self.assertEqual(user.get_user_id(), 1)

        rights = OIDCUserRights(user)

        self.assertEqual(rights.get_user_admin_collections(), [])
        self.assertEqual(rights.get_user_staff_collections(), [])
        self.assertEqual(rights.comment_rights_query_params(), {"user_id": 1})

        comment = self.get_comment()
        comment["status"] = "validated"

        # Cannot edit validated comment
        self.assertFalse(rights.comment_can_edit(comment))
        self.assertFalse(rights.comment_can_delete(comment))
        self.assertFalse(rights.comment_can_moderate(comment))
        self.assertFalse(rights.comment_can_manage_moderators(comment))

        # Cannot edit deleted comment
        comment["status"] = "deleted"
        self.assertFalse(rights.comment_can_edit(comment))
        self.assertFalse(rights.comment_can_delete(comment))
        self.assertFalse(rights.comment_can_moderate(comment))
        self.assertFalse(rights.comment_can_manage_moderators(comment))

        # Can edit submitted comment
        comment["status"] = "submitted"
        self.assertTrue(rights.comment_can_edit(comment))
        self.assertTrue(rights.comment_can_delete(comment))
        self.assertFalse(rights.comment_can_moderate(comment))
        self.assertFalse(rights.comment_can_manage_moderators(comment))

        # Cannot edit submitted comment with assigned moderators
        comment["status"] = "submitted"
        comment["moderators"] = [10]
        self.assertFalse(rights.comment_can_edit(comment))
        self.assertTrue(rights.comment_can_delete(comment))
        self.assertFalse(rights.comment_can_moderate(comment))
        self.assertFalse(rights.comment_can_manage_moderators(comment))

        # Cannot edit comment from another user
        comment["moderators"] = []
        comment["author_id"] = 2
        self.assertFalse(rights.comment_can_edit(comment))
        self.assertFalse(rights.comment_can_delete(comment))
        self.assertFalse(rights.comment_can_moderate(comment))
        self.assertFalse(rights.comment_can_manage_moderators(comment))

    def test_app_settings(self):
        user = self.get_user()
        rights = OIDCUserRights(user)

        self.assertEqual(rights.get_user_admin_collections(), [])
        self.assertEqual(rights.get_user_staff_collections(), [])
        self.assertEqual(rights.comment_rights_query_params(), {"user_id": 1})

        comment = self.get_comment()
        with self.settings(COMMENTS_VIEWS_EDIT_BEFORE_MODERATION=True):
            comment["status"] = "submitted"
            comment["moderators"] = [1]
            self.assertTrue(rights.comment_can_edit(comment))

        with self.settings(COMMENTS_VIEWS_DELETE_BEFORE_MODERATION=False):
            self.assertFalse(rights.comment_can_delete(comment))

        with self.settings(COMMENTS_VIEWS_DELETE_AFTER_MODERATION=True):
            comment["status"] = "validated"
            self.assertTrue(rights.comment_can_delete(comment))
