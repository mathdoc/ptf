from django.contrib.sessions.middleware import SessionMiddleware
from django.http import HttpRequest
from django.test import TestCase

from ..auth import customOIDCAuthenticationBackend
from ..models import OIDCUser


class AuthTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        print("\n**App: 'comments_views.journal' - Testing `auth.py`**")
        super().setUpClass()

    def get_claims(self) -> dict:
        return {
            "sub": 1,
            "email": "user_oidc_1@test.test",
            "first_name": "User",
            "last_name": "OIDC 1",
            "name": "User OIDC 1",
            "provider": "orcid",
            "provider_uid": "0000-0000-0000-0001",
        }

    def test_auth(self):
        backend = customOIDCAuthenticationBackend()
        dummy_func = lambda x: x
        session_middleware = SessionMiddleware(dummy_func)
        request = HttpRequest()
        session_middleware.process_request(request)
        backend.request = request

        user = backend.get_user(1)
        self.assertIsInstance(user, OIDCUser)

        user = backend.filter_users_by_claims(self.get_claims())
        self.assertEqual(len(user), 0)

        user = backend.create_user(self.get_claims())
        self.assertIsInstance(user, OIDCUser)
        self.assertEqual(user.get_user_id(), 1)
        self.assertEqual(request.session["oidc_user_data"], user.claims)

        claims = self.get_claims()
        claims["email"] = "user_oidc_edit@test.test"

        user = backend.update_user(user, claims)

        self.assertIsInstance(user, OIDCUser)
        self.assertEqual(user.email, "user_oidc_edit@test.test")
