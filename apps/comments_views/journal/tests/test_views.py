from urllib.parse import parse_qs
from urllib.parse import quote_plus
from urllib.parse import urlencode
from urllib.parse import urlparse

import responses
from responses import matchers

from django.conf import settings
from django.contrib.messages import get_messages
from django.core import mail
from django.http import HttpRequest
from django.http import HttpResponse
from django.test import override_settings
from django.utils import translation

from comments_views.core.forms import CommentForm
from comments_views.core.tests.utils import get_comments_list
from ptf.factories import ArticleWithSiteFactory
from ptf.tests.base_test_case import ViewTestCase as BaseViewTestCase

from ..auth import customOIDCAuthenticationBackend
from ..rights import OIDCUserRights
from ..utils import get_pending_comment
from ..views import get_resource_comments

COMMENTS_BASE_URL = "http://comments.test"


# Set the base URL + apply default settings
@override_settings(
    ALLOWED_HOSTS=["test.comments", "testserver"],
    COMMENTS_VIEWS_API_BASE_URL=COMMENTS_BASE_URL,
    COMMENTS_VIEWS_API_CREDENTIALS=("login", "password"),
    COMMENTS_VIEWS_COMMENTS_NESTING=False,
    COMMENTS_VIEWS_EDIT_BEFORE_MODERATION=False,
    COMMENTS_VIEWS_DELETE_BEFORE_MODERATION=True,
    COMMENTS_VIEWS_DELETE_AFTER_MODERATION=False,
)
class ViewTestCase(BaseViewTestCase):
    @classmethod
    def setUpClass(cls):
        print("\n**App: 'comments_views.journal' - Testing `views.py`**")
        super().setUpClass()

    def setUp(self) -> None:
        translation.activate("fr")
        return super().setUp()

    def tearDown(self) -> None:
        translation.activate(settings.LANGUAGE_CODE)
        return super().tearDown()

    def clear_messages(self):
        """
        Deletes all messages of the test client.
        Checks the presence of messages in both cookies and session (depends on the
        configured storage).
        """
        if "messages" in self.client.cookies:
            del self.client.cookies["messages"]
        if "_messages" in self.client.session:
            del self.client.session["_messages"]

    def assertUniqueLevelMessage(self, response: HttpResponse, level: list, clear_messages=True):
        messages = list(get_messages(response.wsgi_request))
        self.assertEqual(len(messages), 1)
        self.assertIn(messages[0].level_tag, level)
        if clear_messages:
            self.clear_messages()

    def assertUniqueSuccessMessage(self, response: HttpResponse, clear_messages=True):
        self.assertUniqueLevelMessage(response, ["success"], clear_messages)

    def assertUniqueErrorMessage(self, response: HttpResponse, clear_messages=True):
        self.assertUniqueLevelMessage(response, ["error", "danger"], clear_messages)

    def assertUniqueWarningMessage(self, response: HttpResponse, clear_messages=True):
        self.assertUniqueLevelMessage(response, ["warning"], clear_messages)

    def get_user_claims(self) -> dict:
        """Returns an user claims in the same form as our OP."""
        return {
            "sub": 1,
            "email": "user_oidc_1@test.test",
            "first_name": "User",
            "last_name": "OIDC 1",
            "name": "User OIDC 1",
            "provider": "orcid",
            "provider_uid": "0000-0000-0000-0001",
        }

    def add_comments_200_response(self, match=[]):
        """Emulate a HTTP 200 OK response for `/api/comments/?(.*)`"""
        responses.add(
            "GET",
            f"{COMMENTS_BASE_URL}/api/comments/",
            status=200,
            json=get_comments_list(),
            match=match,
        )

    def login_oidc_user(self, claims):
        """
        Login an OIDC user in the django test client.
        Most of the code is copied from `Client.login` native function.
        We just adapt the way to retrieve the user with our hacky "authentication"
        backend.
        """
        from django.contrib.auth import login

        oidc_backend = customOIDCAuthenticationBackend()
        # Create a fake request to store login details.
        request = HttpRequest()
        request.session = self.client.session

        oidc_backend.request = request
        user = oidc_backend.create_user(claims)
        backend = "comments_views.journal.auth.customOIDCAuthenticationBackend"

        login(request, user, backend)

        # Copied from Client _login code
        request.session.save()
        # Set the cookie on the client to represent the session.
        session_cookie = settings.SESSION_COOKIE_NAME
        self.client.cookies[session_cookie] = request.session.session_key
        cookie_data = {
            "max-age": None,
            "path": "/",
            "domain": settings.SESSION_COOKIE_DOMAIN,
            "secure": settings.SESSION_COOKIE_SECURE or None,
            "expires": None,
        }
        self.client.cookies[session_cookie].update(cookie_data)

    def test_sso_login_view(self):
        url = "/remote-login/?next=http%3A%2F%2Ftest.comments%2Fcomments%2F"

        # Request with Anonymous user
        response = self.client.get(url, SERVER_NAME="test.comments")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, "dashboard/comment_login.html")
        self.assertEqual(
            response.context["authentication_url"],
            "/oidc/authenticate/?next=http%3A%2F%2Ftest.comments%2Fcomments%2F",
        )

        # Request with authenticated (OIDC) user
        self.login_oidc_user(self.get_user_claims())
        response = self.client.get(url, SERVER_NAME="test.comments")
        self.assertRedirects(
            response, "http://test.comments/comments/", fetch_redirect_response=False
        )
        # Test that customOIDCAuthenticationBackend correctly store the data
        self.assertEqual(response.wsgi_request.session["oidc_user_data"], self.get_user_claims())

    @responses.activate
    def test_sso_login_required(self):
        # Anonymous user should get redirected
        response = self.client.get("/comments/", SERVER_NAME="test.comments")

        self.assertRedirects(
            response,
            "/remote-login/?next=http%3A%2F%2Ftest.comments%2Fcomments%2F",
            fetch_redirect_response=False,
        )

        # Logged in OICD user should get the data
        claims = self.get_user_claims()
        self.login_oidc_user(claims)
        self.add_comments_200_response()
        response = self.client.get("/comments/", SERVER_NAME="test.comments")
        self.assertEqual(response.status_code, 200)

    @responses.activate
    def test_get_resource_comments(self):
        """
        Test the app setting COMMENTS_VIEWS_COMMENTS_NESTING.
        """
        self.add_comments_200_response()
        claims = self.get_user_claims()
        request = HttpRequest()
        request.session = self.client.session
        backend = customOIDCAuthenticationBackend()
        backend.request = request
        user = backend.create_user(claims)
        rights = OIDCUserRights(user)

        doi = get_comments_list()[0]["doi"]
        with self.settings(COMMENTS_VIEWS_COMMENTS_NESTING=False):
            comments = get_resource_comments(doi, rights, None)
            self.assertEqual(len(comments), 3)
            for comment_data in comments:
                comment = comment_data["content"]
                self.assertNotIn("children", comment_data)
                self.assertEqual(comment["doi"], doi)

        with self.settings(COMMENTS_VIEWS_COMMENTS_NESTING=True):
            comments = get_resource_comments(doi, rights, None)
            self.assertEqual(len(comments), 2)
            comment_data = comments[0]
            self.assertEqual(comment_data["content"]["id"], 1)
            self.assertEqual(comment_data["content"]["doi"], doi)
            self.assertNotIn("children", comment_data)

            comment_data = comments[1]
            self.assertEqual(comment_data["content"]["id"], 2)
            self.assertEqual(comment_data["content"]["doi"], doi)
            self.assertEqual(len(comment_data["children"]), 1)
            self.assertEqual(comment_data["children"][0]["id"], 3)
            self.assertEqual(comment_data["children"][0]["doi"], doi)

    @responses.activate
    def test_comment_section(self):
        article = ArticleWithSiteFactory()
        self.add_comments_200_response()
        query_params = {"redirect_url": f"http://test.comments/articles/{article.doi}/"}
        base_url = f"/article-comments/{article.doi}/?{urlencode(query_params)}"

        # Request with Anonymous user
        response = self.client.get(base_url, SERVER_NAME="test.comments")
        self.assertEqual(response.status_code, 200)
        context = response.context

        self.assertEqual(len(context["ordered_comments"]), 3)
        self.assertEqual(context.get("default_comment_form"), None)
        self.assertEqual(context.get("reply_comment_form"), None)
        self.assertFalse(context["is_user_oidc_authenticated"])
        self.assertEqual(
            context["authentication_url"],
            f"http://test.comments/oidc/authenticate/?next=http%3A%2F%2Ftest.comments%2Farticles%2F{quote_plus(article.doi)}%2F%23add-comment-default",
        )

        # Request with authenticated (OIDC) user
        self.login_oidc_user(self.get_user_claims())
        response = self.client.get(base_url, SERVER_NAME="test.comments")
        context = response.context

        default_form = context["default_comment_form"]
        self.assertEqual(default_form["author_name"], "User OIDC 1")
        self.assertEqual(default_form["form_prefix"], "default")
        self.assertTrue(isinstance(default_form["form"], CommentForm))

        url = urlparse(default_form["submit_url"])
        self.assertEqual(url.path, "/submit-article-comment/")
        qs = parse_qs(url.query)
        self.assertIn("redirect_url", qs)
        self.assertEqual(qs["form_prefix"], ["default"])

        reply_form = context["reply_comment_form"]
        self.assertEqual(reply_form["author_name"], "User OIDC 1")
        self.assertEqual(reply_form["form_prefix"], "reply")
        self.assertTrue(isinstance(reply_form["form"], CommentForm))

        url = urlparse(reply_form["submit_url"])
        self.assertEqual(url.path, "/submit-article-comment/")
        qs = parse_qs(url.query)
        self.assertIn("redirect_url", qs)
        self.assertEqual(qs["form_prefix"], ["reply"])

        self.assertTrue(context["is_user_oidc_authenticated"])

        # Test pending comment
        # Add a pending comment by posting to the submit URL logged out
        self.client.logout()
        query_params_loc = {**query_params}
        query_params_loc["form_prefix"] = "reply"
        post_url = f"/submit-article-comment/?{urlencode(query_params_loc)}"
        post_data = {
            "reply-content": "My comment",
            "reply-doi": article.doi,
            "reply-parent": 2,
            "reply-parent_author_name": "OIDC User X",
            "reply-approval": True,
        }
        response = self.client.post(post_url, post_data, SERVER_NAME="test.comments")
        self.assertEqual(response.status_code, 302)
        self.assertUniqueWarningMessage(response, clear_messages=False)
        pending_comment_data = get_pending_comment(response.wsgi_request, article.doi)
        self.assertEqual(
            pending_comment_data, {"comment": response.wsgi_request.POST, "prefix": "reply"}
        )

        # Log in again and get the page
        self.login_oidc_user(self.get_user_claims())
        response = self.client.get(base_url, SERVER_NAME="test.comments")
        context = response.context
        self.assertTrue(context["display_reply_form"])
        self.assertEqual(context["reply_comment_form"]["parent"], 2)
        self.assertEqual(context["reply_comment_form"]["parent_author_name"], "OIDC User X")
        self.assertEqual(context["reply_comment_form"]["form"].data["reply-content"], "My comment")
        self.assertEqual(context["reply_comment_form"]["form"].data["reply-parent"], "2")
        self.assertEqual(
            context["reply_comment_form"]["form"].data["reply-parent_author_name"], "OIDC User X"
        )

    @responses.activate
    def test_submit_comment(self):
        # Base data
        mocked_response_url = f"{COMMENTS_BASE_URL}/api/comments/"
        article = ArticleWithSiteFactory()
        comment_json_data = {"site_name": "crbiol", "doi": article.doi}
        responses.add("POST", mocked_response_url, status=201, json=comment_json_data)
        redirect_url = f"http://test.comments/articles/{article.doi}"
        query_params = {"redirect_url": redirect_url, "form_prefix": "default"}
        url_base = "/submit-article-comment/"
        url = f"{url_base}?{urlencode(query_params)}"
        post_data = {
            "default-content": "<p>My wonderful comment</p>",
            "default-doi": article.doi,
            "default-approval": True,
        }

        # Test correct default behavior
        self.login_oidc_user(self.get_user_claims())
        response = self.client.post(url, post_data, SERVER_NAME="test.comments")
        self.assertRedirects(response, redirect_url, fetch_redirect_response=False)
        self.assertUniqueSuccessMessage(response)
        pending_comment_data = get_pending_comment(response.wsgi_request, article.doi)
        self.assertTrue(pending_comment_data is None)

        # Empty sent e-mails
        mail.outbox = []

        # Missing `redirect_url`
        query_params_loc = {**query_params}
        del query_params_loc["redirect_url"]
        url_loc = f"{url_base}?{urlencode(query_params_loc)}"
        response = self.client.post(url_loc, post_data, SERVER_NAME="test.comments")
        self.assertEqual(response.status_code, 400)

        # Wrong `redirect_url`
        query_params_loc["redirect_url"] = "http:./my_wrong_url"
        url_loc = f"{url_base}?{urlencode(query_params_loc)}"
        response = self.client.post(url_loc, post_data, SERVER_NAME="test.comments")
        self.assertEqual(response.status_code, 400)

        # Missing `form_prefix`
        query_params_loc = {**query_params}
        del query_params_loc["form_prefix"]
        url_loc = f"{url_base}?{urlencode(query_params_loc)}"
        response = self.client.post(url_loc, post_data, SERVER_NAME="test.comments")
        self.assertRedirects(response, redirect_url, fetch_redirect_response=False)
        self.assertUniqueErrorMessage(response)

        # Wrong `form_prefix`
        query_params_loc["form_prefix"] = "wrong"
        url_loc = f"{url_base}?{urlencode(query_params_loc)}"
        response = self.client.post(url_loc, post_data, SERVER_NAME="test.comments")
        self.assertRedirects(response, redirect_url, fetch_redirect_response=False)
        self.assertUniqueErrorMessage(response)

        # Unvalid comment data
        post_data_loc = {**post_data}
        del post_data_loc["default-content"]
        response = self.client.post(url, post_data_loc, SERVER_NAME="test.comments")
        self.assertRedirects(response, redirect_url, fetch_redirect_response=False)
        self.assertUniqueErrorMessage(response)

        post_data_loc = {**post_data}
        del post_data_loc["default-doi"]
        response = self.client.post(url, post_data_loc, SERVER_NAME="test.comments")
        self.assertRedirects(response, redirect_url, fetch_redirect_response=False)
        self.assertUniqueErrorMessage(response)

        # DOI does not exist
        post_data_loc = {**post_data}
        post_data_loc["default-doi"] = "10.207/cr.001"
        response = self.client.post(url, post_data_loc, SERVER_NAME="test.comments")
        self.assertRedirects(response, redirect_url, fetch_redirect_response=False)
        self.assertUniqueErrorMessage(response)

        # Non-authenticated user
        self.client.logout()
        response = self.client.post(url, post_data, SERVER_NAME="test.comments")
        query_params_loc = {"next": f"{redirect_url}#add-comment-default"}
        redirect_url_loc = f"/oidc/authenticate/?{urlencode(query_params_loc)}"
        self.assertRedirects(response, redirect_url_loc, fetch_redirect_response=False)
        self.assertUniqueWarningMessage(response)
        # Test the pending comment was stored in the session
        pending_comment_data = get_pending_comment(response.wsgi_request, article.doi)
        self.assertEqual(pending_comment_data["comment"], response.wsgi_request.POST)
        self.assertEqual(pending_comment_data["prefix"], "default")

        # Comment not enough characters
        self.login_oidc_user(self.get_user_claims())
        post_data_loc = {**post_data}
        content = "<p>My content</p>"
        post_data_loc["default-content"] = content
        response = self.client.post(url, post_data_loc, SERVER_NAME="test.comments")
        self.assertRedirects(response, redirect_url, fetch_redirect_response=False)
        self.assertUniqueErrorMessage(response)
        pending_comment_data = get_pending_comment(response.wsgi_request, article.doi)
        self.assertEqual(pending_comment_data["comment"]["default-content"], content)

        # Make sure no e-mail were sent
        self.assertEqual(len(mail.outbox), 0)

        # Test posting a reply comment
        post_data_loc = {
            "reply-content": "<p>My wonderful comment</p>",
            "reply-doi": article.doi,
            "reply-parent": 2,
            "reply-parent_author_name": "User OIDC X",
            "reply-approval": True,
        }
        post_data_loc.update({})
        query_params_loc = {**query_params}
        query_params_loc["form_prefix"] = "reply"
        url_loc = f"{url_base}?{urlencode(query_params_loc)}"

        response = self.client.post(url_loc, post_data_loc, SERVER_NAME="test.comments")

        self.assertRedirects(response, redirect_url, fetch_redirect_response=False)
        self.assertUniqueSuccessMessage(response)
        # Missing data - parent & parent_author_name are required together
        del post_data_loc["reply-parent"]
        response = self.client.post(url_loc, post_data_loc, SERVER_NAME="test.comments")
        self.assertRedirects(response, redirect_url, fetch_redirect_response=False)
        self.assertUniqueErrorMessage(response)

        del post_data_loc["reply-parent_author_name"]
        post_data_loc["reply-parent"] = 2
        response = self.client.post(url_loc, post_data_loc, SERVER_NAME="test.comments")
        self.assertRedirects(response, redirect_url, fetch_redirect_response=False)
        self.assertUniqueErrorMessage(response)

        mail.outbox = []

        # Test responses to different comment server status
        # Comment server Response 201 - Test e-mail sending
        response = self.client.post(url, post_data, SERVER_NAME="test.comments")

        self.assertRedirects(response, redirect_url, fetch_redirect_response=False)
        self.assertUniqueSuccessMessage(response)
        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        self.assertEqual(email.subject, "[CRBIOL] Confirmation of a new comment")
        self.assertEqual(email.to, [self.get_user_claims()["email"]])
        mail.outbox = []

        # Comment server Response 400
        error_message = "Bad request 400"
        responses.add(
            "POST", mocked_response_url, status=400, json={"error_message": error_message}
        )

        post_data_loc = {**post_data}
        content = "<p>My content Bad Request 400</p>"
        post_data_loc["default-content"] = content

        response = self.client.post(url, post_data_loc, SERVER_NAME="test.comments")

        self.assertRedirects(response, redirect_url, fetch_redirect_response=False)
        message = list(get_messages(response.wsgi_request))[0]
        self.assertTrue(str(message).endswith(f'"{error_message}"'))
        self.assertUniqueErrorMessage(response)
        pending_comment_data = get_pending_comment(response.wsgi_request, article.doi)
        self.assertEqual(pending_comment_data["comment"]["default-content"], content)

        # Comment server Response 500
        error_message = "Bad request 500"
        responses.add(
            "POST", mocked_response_url, status=500, json={"error_message": error_message}
        )

        post_data_loc = {**post_data}
        content = "<p>My content Bad Request 500</p>"
        post_data_loc["default-content"] = content

        response = self.client.post(url, post_data_loc, SERVER_NAME="test.comments")

        self.assertRedirects(response, redirect_url, fetch_redirect_response=False)
        message = list(get_messages(response.wsgi_request))[0]
        self.assertTrue(str(message).endswith(f'"{error_message}"'))
        self.assertUniqueErrorMessage(response)
        pending_comment_data = get_pending_comment(response.wsgi_request, article.doi)
        self.assertEqual(pending_comment_data["comment"]["default-content"], content)

        # Make sure no e-mail were sent
        self.assertEqual(len(mail.outbox), 0)

        # Test the comment update
        # For the edit, the PUT request MUST send the user_id in the query params
        request_query_params = {"user_id": "1"}
        responses.add(
            "PUT",
            f"{mocked_response_url}2/",
            status=201,
            json={},
            match=[matchers.query_param_matcher(request_query_params)],
        )
        initial_comment = {"author_id": 1, "status": "submitted", "moderators": []}

        response_params = {
            "method": "GET",
            "url": f"{mocked_response_url}2/",
            "status": 200,
            "json": initial_comment,
            "match": [matchers.query_param_matcher(request_query_params)],
        }
        responses.add(**response_params)

        post_data_loc = {
            "content": "<p>My edited wonderful comment</p>",
            "doi": article.doi,
            "date_submitted": "2023-04-11T11:00:00Z",
            "status": "submitted",
            "id": 2,
        }
        query_params_loc = {"redirect_url": redirect_url}
        url_loc = f"{url_base}?{urlencode(query_params_loc)}"

        response = self.client.post(url_loc, post_data_loc, SERVER_NAME="test.comments")

        self.assertRedirects(response, redirect_url, fetch_redirect_response=False)
        self.assertUniqueSuccessMessage(response)
        self.assertTrue(get_pending_comment(response.wsgi_request, article.doi) is None)
        self.assertEqual(len(mail.outbox), 0)

        # Test update when the user cannot edit the provided comment
        initial_comment["moderators"] = [1]
        response_params["json"] = initial_comment
        responses.add(**response_params)

        response = self.client.post(url_loc, post_data_loc, SERVER_NAME="test.comments")

        self.assertRedirects(response, redirect_url, fetch_redirect_response=False)
        self.assertUniqueErrorMessage(response)

        # Edit is allowed with this app setting
        with self.settings(COMMENTS_VIEWS_EDIT_BEFORE_MODERATION=True):
            response = self.client.post(url_loc, post_data_loc, SERVER_NAME="test.comments")

            self.assertRedirects(response, redirect_url, fetch_redirect_response=False)
            self.assertUniqueSuccessMessage(response)

        initial_comment["status"] = "validated"
        response_params["json"] = initial_comment
        responses.add(**response_params)

        response = self.client.post(url_loc, post_data_loc, SERVER_NAME="test.comments")

        self.assertRedirects(response, redirect_url, fetch_redirect_response=False)
        self.assertUniqueErrorMessage(response)

    @responses.activate
    def test_comment_dashboard_list(self):
        self.add_comments_200_response(
            [matchers.query_param_matcher({"dashboard": "true", "user_id": "1"})]
        )

        # Anonymous user should get redirected
        response = self.client.get("/comments/", SERVER_NAME="test.comments")

        self.assertRedirects(
            response,
            "/remote-login/?next=http%3A%2F%2Ftest.comments%2Fcomments%2F",
            fetch_redirect_response=False,
        )

        # Authenticated OIDC user gets data
        self.login_oidc_user(self.get_user_claims())
        response = self.client.get("/comments/", SERVER_NAME="test.comments")
        self.assertEqual(response.status_code, 200)
        context = response.context
        self.assertFalse(context["display_moderators"])
        self.assertEqual(len(context["submitted_comments"]), 1)
        self.assertEqual(len(context["processed_comments"]), 2)

        # Check hadnling of bad response
        error_message = "Error: Bad Request 400"
        responses.add(
            "GET",
            f"{COMMENTS_BASE_URL}/api/comments/",
            status=400,
            json={"error_message": error_message},
            match=[matchers.query_param_matcher({"dashboard": "true", "user_id": "1"})],
        )

        response = self.client.get("/comments/", SERVER_NAME="test.comments")

        message = list(get_messages(response.wsgi_request))[0]
        self.assertTrue(str(message).endswith(f'"{error_message}"'))
        self.assertUniqueErrorMessage(response)
        self.assertTrue("submitted_comments" not in response.context)
        self.assertTrue("processed_comments" not in response.context)

    @responses.activate
    def test_comment_dashboard_details(self):
        # Anonymous user should get redirected
        response = self.client.get("/comments/1/", SERVER_NAME="test.comments")

        self.assertRedirects(
            response,
            "/remote-login/?next=http%3A%2F%2Ftest.comments%2Fcomments%2F1%2F",
            fetch_redirect_response=False,
        )

        # Authenticated OIDC user gets data
        # Validated comment
        comment_data = get_comments_list()[0]
        responses.add(
            "GET",
            f"{COMMENTS_BASE_URL}/api/comments/1/",
            status=200,
            json=comment_data,
            match=[matchers.query_param_matcher({"dashboard": "true", "user_id": "1"})],
        )
        self.login_oidc_user(self.get_user_claims())

        response = self.client.get("/comments/1/", SERVER_NAME="test.comments")

        self.assertEqual(response.status_code, 200)
        context = response.context
        self.assertFalse(context["display_moderators"])
        self.assertFalse(context["comment"]["can_moderate"])
        self.assertTrue("edit" not in context)
        self.assertTrue("show_edit" not in context)
        self.assertTrue("comment_form" not in context)
        self.assertTrue("delete_form" not in context)
        self.assertTrue(context["show_context"])
        self.assertEqual(context["comment"]["sanitized_html"], "<p>My wonderful comment</p>")

        # Submitted comment (can edit & can delete)
        comment_data = get_comments_list()[2]
        responses.add(
            "GET",
            f"{COMMENTS_BASE_URL}/api/comments/3/",
            status=200,
            json=comment_data,
            match=[matchers.query_param_matcher({"dashboard": "true", "user_id": "1"})],
        )

        response = self.client.get("/comments/3/", SERVER_NAME="test.comments")

        self.assertEqual(response.status_code, 200)
        context = response.context
        self.assertFalse(context["display_moderators"])
        self.assertFalse(context["comment"]["can_moderate"])
        self.assertTrue("edit" not in context)
        self.assertTrue(context["show_edit"])
        self.assertTrue("comment_form" not in context)
        self.assertTrue(context["delete_form"])
        self.assertTrue("show_context" not in context)
        self.assertEqual(context["comment"]["sanitized_html"], "<p>My wonderful reply</p>")

        # Edit mode of submitted comment
        response = self.client.get("/comments/3/edit/", SERVER_NAME="test.comments")

        self.assertEqual(response.status_code, 200)
        context = response.context
        self.assertFalse(context["display_moderators"])
        self.assertFalse(context["comment"]["can_moderate"])
        self.assertTrue(context["edit"])
        self.assertTrue(context["show_edit"])
        self.assertTrue("comment_form" in context)
        self.assertEqual(context["comment_form"].initial["content"], "<p>My wonderful reply</p>")

        url = urlparse(context["post_url"])
        self.assertEqual(url.path, "/submit-article-comment/")
        qs = parse_qs(url.query)
        self.assertTrue("redirect_url" in qs)

        self.assertTrue("delete_form" not in context)
        self.assertTrue("show_context" not in context)

        # Submitted comment  with assigned moderator (can delete but not edit)
        comment_data = get_comments_list()[2]
        comment_data["moderators"] = [1]
        responses.add(
            "GET",
            f"{COMMENTS_BASE_URL}/api/comments/3/",
            status=200,
            json=comment_data,
            match=[matchers.query_param_matcher({"dashboard": "true", "user_id": "1"})],
        )

        response = self.client.get("/comments/3/", SERVER_NAME="test.comments")

        self.assertEqual(response.status_code, 200)
        context = response.context
        self.assertFalse(context["display_moderators"])
        self.assertFalse(context["comment"]["can_moderate"])
        self.assertTrue("edit" not in context)
        self.assertTrue("show_edit" not in context)
        self.assertTrue("comment_form" not in context)
        self.assertTrue(context["delete_form"])
        self.assertTrue("show_context" not in context)
        self.assertEqual(context["comment"]["sanitized_html"], "<p>My wonderful reply</p>")

        # Check handling of bad response
        error_message = "Error: Bad Request 400"
        responses.add(
            "GET",
            f"{COMMENTS_BASE_URL}/api/comments/3/",
            status=400,
            json={"error_message": error_message},
            match=[matchers.query_param_matcher({"dashboard": "true", "user_id": "1"})],
        )

        response = self.client.get("/comments/3/", SERVER_NAME="test.comments")

        message = list(get_messages(response.wsgi_request))[0]
        self.assertTrue(str(message).endswith(f'"{error_message}"'))
        self.assertUniqueErrorMessage(response)
        context = response.context
        self.assertTrue("display_moderators" not in context)
        self.assertTrue("edit" not in context)
        self.assertTrue("show_edit" not in context)
        self.assertTrue("comment_form" not in context)
        self.assertTrue("delete_form" not in context)
        self.assertTrue("show_context" not in context)
        self.assertTrue("comment" not in context)

    @responses.activate
    def test_dashboard_comment_details_post(self):
        target_url = "/comments/3/status-change/light/"
        # Anonymous user should get redirected
        response = self.client.post(target_url, SERVER_NAME="test.comments")

        self.assertRedirects(
            response,
            "/remote-login/?next=http%3A%2F%2Ftest.comments%2Fcomments%2F3%2Fstatus-change%2Flight%2F",
            fetch_redirect_response=False,
        )

        comment_data = get_comments_list()[2]

        # Authenticated OIDC user can POST
        responses.add(
            "GET",
            f"{COMMENTS_BASE_URL}/api/comments/3/",
            status=200,
            json=comment_data,
            match=[matchers.query_param_matcher({"dashboard": "true", "user_id": "1"})],
        )
        mail.outbox = []
        responses.add(
            "PUT",
            f"{COMMENTS_BASE_URL}/api/comments/3/",
            status=200,
            json=comment_data,
            match=[matchers.query_param_matcher({"dashboard": "true", "user_id": "1"})],
        )
        self.login_oidc_user(self.get_user_claims())
        post_data = {"status": "deleted"}

        response = self.client.post(target_url, post_data, SERVER_NAME="test.comments")
        self.assertRedirects(response, "/comments/", fetch_redirect_response=False)

        self.assertUniqueSuccessMessage(response)

        # No e-mail sent on comment deletion
        self.assertEqual(len(mail.outbox), 0)

        # Check handling of bad response
        error_message = "Error: Bad Request 400"
        responses.add(
            "PUT",
            f"{COMMENTS_BASE_URL}/api/comments/3/",
            status=400,
            json={"error_message": error_message},
            match=[matchers.query_param_matcher({"dashboard": "true", "user_id": "1"})],
        )
        comment_data = get_comments_list()[2]
        responses.add(
            "GET",
            f"{COMMENTS_BASE_URL}/api/comments/3/",
            status=200,
            json=comment_data,
            match=[matchers.query_param_matcher({"dashboard": "true", "user_id": "1"})],
        )

        response = self.client.post(target_url, post_data, SERVER_NAME="test.comments")
        self.assertRedirects(response, "/comments/", fetch_redirect_response=False)

        message = list(get_messages(response.wsgi_request))[0]
        self.assertTrue(str(message).endswith(f'"{error_message}"'))
        self.assertUniqueErrorMessage(response)

        # Both requests bad response
        responses.add(
            "GET",
            f"{COMMENTS_BASE_URL}/api/comments/3/",
            status=400,
            json={},
            match=[matchers.query_param_matcher({"dashboard": "true", "user_id": "1"})],
        )

        response = self.client.post(target_url, post_data, SERVER_NAME="test.comments")
        self.assertRedirects(response, "/comments/", fetch_redirect_response=False)
        self.assertUniqueErrorMessage(response)
