from mozilla_django_oidc import urls as django_oidc_urls

from django.conf import settings
from django.urls import include
from django.urls import path
from django.urls import re_path

from .views import CommentChangeStatusView
from .views import CommentDashboardDetailsView
from .views import CommentDashboardListView
from .views import CommentSectionView
from .views import SSOLoginView
from .views import SubmitCommentView
from .views import logout_view
from .views import reset_session

urlpatterns = [
    # SSO authentication URLs
    path("oidc/", include(django_oidc_urls)),
    path("remote-login/", SSOLoginView.as_view(), name="remote_login"),
    path("remote-logout/", logout_view, name="remote_logout"),
    # Comments section (article page) URLs
    re_path(
        r"^article-comments/(?P<doi>10[.][0-9]{4,}.+)/$",
        CommentSectionView.as_view(),
        name="article_comments"
    ),
    path(
        "submit-article-comment/",
        SubmitCommentView.as_view(),
        name="submit_comment"
    ),
    # Comments dashboard URLs
    path("comments/", CommentDashboardListView.as_view(), name="comment_list"),
    path(
        "comments/<int:pk>/",
        CommentDashboardDetailsView.as_view(),
        name="comment_details"
    ),
    re_path(
        r"^comments/(?P<pk>[0-9]+)/(?P<edit>edit)/$",
        CommentDashboardDetailsView.as_view(),
        name="comment_edit"
    ),
    path(
        "comments/<int:pk>/status-change/light/",
        CommentChangeStatusView.as_view(),
        name="comment_status_change_light"
    ),
    path(
        "comments/<int:pk>/status-change/",
        CommentChangeStatusView.as_view(),
        name="comment_status_change"
    )
]

dev_patterns = [
    path("reset-session/", reset_session, name="reset_session"),
]


if hasattr(settings, "DEBUG") and settings.DEBUG:
    urlpatterns += dev_patterns
