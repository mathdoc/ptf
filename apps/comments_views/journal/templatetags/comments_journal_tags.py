from urllib.parse import urlencode

from django import template
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.http import HttpRequest

register = template.Library()


@register.simple_tag
def login_server_url(request: HttpRequest) -> str:
    """
    Returns the base URL of the login server as defined in the settings.
    """
    if not hasattr(settings, "OIDC_OP_BASE_URL"):
        raise ImproperlyConfigured("You must define the OIDC_OP_BASE_URL settings.")

    login_url = settings.OIDC_OP_BASE_URL
    query_params = {"origin_uri": request.build_absolute_uri()}
    return f"{login_url}?{urlencode(query_params)}"
