from comments_views.core.app_settings import AppSettings as BaseAppSettings


class AppSettings(BaseAppSettings):
    @property
    def ARTICLE_COMMENTS(self) -> bool:
        """Activates the display of the comments section on the article page."""
        return self._setting("ARTICLE_COMMENTS", False)

    @property
    def COMMENTS_NESTING(self) -> bool:
        """Whether to enable nesting for the display of comments on the article page."""
        return self._setting("COMMENTS_NESTING", False)

    @property
    def COMMENTS_POLICY_LINK(self) -> str | None:
        return self._setting("COMMENTS_POLICY_LINK", None)

    ##### BELOW SETTINGS ARE NOT USED YET
    @property
    def EDIT_BEFORE_MODERATION(self) -> bool:
        """
        Whether a comment's author can edit his/her comment while
        it has not yet been moderated, even if a moderator is already assigned
        to the comment.
        """
        return self._setting("EDIT_BEFORE_MODERATION", False)

    @property
    def DELETE_BEFORE_MODERATION(self) -> bool:
        """
        Whether a comment's author can delete his/her comment while
        it has not yet been moderated.
        """
        return self._setting("DELETE_BEFORE_MODERATION", True)

    # DELETE_AFTER_MODERATION UPDATE - Useless right now.
    @property
    def DELETE_AFTER_MODERATION(self) -> bool:
        """
        Whether a comment's author can delete his/her comment after
        it has been moderated.
        """
        return self._setting("DELETE_AFTER_MODERATION", False)

    @property
    def POLICY_LINK(self) -> str:
        """URL of the comments policy. Only used for display in HTML template."""
        return self._setting("POLICY_LINK", "")

    @property
    def CONSENT_TEXT(self) -> list[dict]:
        """
        Text to display next to the rights consent checkbox whent submitting a comment.

        The setting structure must be as the default below. You must provide here the
        translation in each available language of the target website.
        """
        return self._setting(
            "CONSENT_TEXT",
            [
                {
                    "lang": "fr",
                    "content": [
                        "J'accepte que mon commentaire soit publié sous licence <a href='https://creativecommons.org/licenses/by/4.0/deed.fr' target='_blank'>CC BY 4.0.</a>",
                        "J'accepte que mon nom soit affiché comme auteur du commentaire.",
                    ],
                },
                {
                    "lang": "en",
                    "content": [
                        "I agree that my comment may be published under a <a href='https://creativecommons.org/licenses/by/4.0/' target='_blank'>CC BY 4.0</a> license.",
                        "I agree that my name will be displayed as the comment's author.",
                    ],
                },
            ],
        )


app_settings = AppSettings()
