# Comments views journal application

This Django application contains all the templates, views and logic related to the comments for a prod journal website. \
See also the `comments_views.core` [documentation](/apps/comments_views/core/doc/comments_views_core.md) for information related to the comment list and the comment details views.

[[_TOC_]]

## I. Requirements

This application requires the following applications to work:

  - `ptf`
  - `comments_views.core`
  - `comments_api`
  - `mozilla_django_oidc`


## I.1. Activating comments - Quickstart

This application is designed to be used by a ptf site:

  - a Django application in __/sites/__ repository,
  - using `ptf` application
  - having an entry in `site_register.py`

Configuration steps:

1.  Connect to Django admin site of the login server (`sso-oidc`). \
    Add an application entry while following the documentation [`here`](https://gricad-gitlab.univ-grenoble-alpes.fr/mathdoc/sso-oidc), "__I.3. Configuring Relying Parties (RP)__". \
    Add `mozilla_django_oidc` to the Django applications. \
    See the section __IV.2. RP Configuration__ of the current doc. to configure the authentication Django settings.
    Note the all the common SSO settings are located in this app [settings.py](/apps/comments_views/journal/settings.py) file

2.  Connect to Django admin site of the comments database server. \
    Create an user account using as username the site ID in [`site_register.py`](/apps/ptf/site_register.py). \
    Check the Mathdoc site checkbox and potentially add an url (if the site url is different from prod). \
    Detailed doc. [here](/sites/comments_site/comments_database/doc/comments_database.md), "__I.1 Register a mathdoc site user__". \
    Add `comments_api` to the Django applications.

3. Add `comments_views.core` to the Django applications and configure its settings (doc [here](/apps/comments_views/core/doc/comments_views_core.md)).

4. Add `comments_views.journal` to the Django applications and configure its settings, section __VII. Django Settings__.

4. [BIS] Add the `apps/comments_views/journal/requirements.txt` to the site's requirements.

5. Additional Django settings to configure:
    - Add the OIDC middleware in the `MIDDLEWARES` list. If the variable is not present, copy the one from  __ptf__ app and add the OIDC middleware. Cf. __IV.3 User data local storage__.
    - Add the OIDC authentication backend (Cf. same section as above).
    - Add the comments templates directories to the `TEMPLATES` variable. Usually this is the following:
        ```python
        # Warning! The below comments order does matter
        f'{APPS_DIR}/comments_views/journal/templates',
        f'{APPS_DIR}/comments_views/core/templates',
        ```
    - Add the e-mail settings. This app sends e-mail to users when they submit a comment.
    - Add the CKEDITOR config for the comments form, usually as follow. Make sure the config keys don't clash!
        ```python
        CKEDITOR_CONFIGS = {} # some existing config
        from comments_views.core.ckeditor_config import COMMENT_CKEDITOR_CONFIGS
        CKEDITOR_CONFIGS.update(COMMENT_CKEDITOR_CONFIGS)
        ```

6. Add the extra CSS and JS files to the `block/article.html` file. If using the default template from ptf folder, the files are automatically included if `COMMENTS_VIEWS_ARTICLE_COMMENTS=True`.

7. Configure the required root CSS variables. Example [here](/apps/cr/static/cr/css/cr_common.css).

8. Add the urls from `comments_views.journal.urls` to the site's `urls.py`.

9. Add an user menu to the base template if none exists (at least for logging out and see submitted comments). cf. user of `user_menu.html` template.

10. In `site_register.py`, add an `email_from` entry to the collection being edited. This e-mail address is used as the _from_ address in the sent e-mails. Make sure we are authorized to send e-mails with this address.

11.
    Usually, just import everything from [`comments_views.journal.settings`](/apps/comments_views/journal/settings.py).

    [OPTIONAL] If the __ptf__ site is using shared settings, split the settings between `settings_local.py` and `settings_shared.py`. The former should contain site specific settings (ex: comments & SSO credentials). The latter should contain all usual shared settings (generally a symlink to some file in _/var/www/mersenne_shared_). \
    Create and adapt the static files on the target server. \
    Add the `settings_shared.py` file to the static files in the capistrano deploy config:
    ```ruby
    set :linked_files, fetch(:linked_files, []).push("#{fetch(:django_settings_dir)}/settings_shared.py")
    ```
    Ensure all settings are correctly loaded in the `settings.py` file of the site. Example:
    ```python
    # We now use two settings files per PTF site:
    # - `settings_shared.py` for shared settings across sites (ex: database config)
    # - `settings_local.py` for site specific settings (ex: comments & SSO credentials)
    try:
        from .settings_shared import *
    except ImportError:
        raise Exception('You must define a settings_shared.py (empty for local dev).')

    try:
        from .settings_local import *
    except ImportError:
        raise Exception('You must define a settings_local.py')
    ```

12. Review this app settings and configure them if required (Cf. __VII. Django Settings__).

## II. Data storage

This application doesn't ship any model with it. The paradigm behind this is to keep the prod journal websites **read only**. \
Thus the following things must be emphasized:
  - All comment related actions are proxified, _ie._ this application queries, creates and updates comment data via HTTP requests to the comments database server. Hence all comments data can be potentially stored in a distinct website/server.
  - All the user related data is stored only in the user session, not in the standard user database object. More information can be found in the **IV. Authentication (SSO)** section


## III. [Views](../views.py)

All views but the article comments view require the user to be authenticated.
The authentication process is described below.


### III.1 Article comments view (`CommentSectionView`)

This view provides the comments related to a given article. This is a public route.


### III.2 `SSOLoginView` and `SSOLoginRequiredMixin`

All the other views are protected using a custom [`SSOLoginRequiredMixin`](../mixins.py). It is responsible of redirecting to the `SSOLoginView` if the user performing the request is not authenticated.


## IV. Authentication (SSO)

User can authenticate against our custom OpenID Connect SSO Provider (OP), see [`sso-oidc` project](https://gricad-gitlab.univ-grenoble-alpes.fr/mathdoc/sso-oidc). \
The authentication process with the SSO is performed here on the client side using the `mozilla_django_oidc` (light) library.


### IV.1. OpenID Connect authorization flow

See the following figure representing the authentication process.


![OIDC authorization flow](./SSO_flow.jpg)


### IV.2. RP Configuration

One needs to register the website as a Relying Party in the SSO database to be able to perform the authorization flow (see [`sso-oidc` project](https://gricad-gitlab.univ-grenoble-alpes.fr/mathdoc/sso-oidc)).


The website then requires the following settings of the `mozilla_django_oidc` library in order to enable the OIDC authorization process (TO BE UNVERSIONED):

  - `OIDC_RP_CLIENT_ID` - The client ID (username) obtained when registering the website as a RP.

  - `OIDC_RP_CLIENT_SECRET` - The client secret (password) obtained when registering the website as a RP.

The following (required) static settings could be versionned (they're not sensitive):

  - `OIDC_RP_SIGN_ALGO = "RS256"` - The OAuth2 algorithm used to sign JWTs. \
    Using _HS256_ algo means using a shared secret between OP and RP (OIDC_RP_CLIENT_SECRET) to sign & validate the JWT.
    Using RS256 algo means using the OP public key to validate the JWT signature.
    We can't use HS256 algo with django-oauth-toolkit for now (https://github.com/jazzband/django-oauth-toolkit/issues/1239)

  - `OIDC_ALLOW_UNSECURED_JWT = False` - Don't allow unsecured JWTs (tokens without `alg` defined in header).

  - `OIDC_VERIFY_JWT = True` - Verify the JWT signature ie. verify it was produced by the OP.

  - `OIDC_VERIFY_KID = True` - Verify the RSA key to use.

  - `OIDC_OP_AUTHORIZATION_ENDPOINT` - The authorization endpoint. Something like `http://sso_url/op/oidc/authorize/"`.

  - `OIDC_OP_TOKEN_ENDPOINT` - The token endpoint. Something like `http://sso_url/op/oidc/token/"`.

  - `OIDC_OP_USER_ENDPOINT` - The user info endpoint. Something like `http://sso_url/op/oidc/userinfo/"`.

  - `OIDC_OP_BASE_URL` - The OP base URL. Something like `http://sso_url/`.

  - `OIDC_RP_SCOPES = "openid profile email authentication"` - The OIDC scopes. \
    `openid`, `profile` and `email` are standard scoped defined by the OIDC standard. `authentication` is a custom scope to get the data of how the user authenticated on the SSO.

  - `LOGIN_REDIRECT_URL_FAILURE = "/"` -  Redirect in case the OIDC authentication fails.

  - `OIDC_RP_IDP_SIGN_KEY` -  Provided by `/op/oidc/.well-known/jwks.json` endpoint from the OP. Example:

    ```python
    OIDC_RP_IDP_SIGN_KEY = {
        "alg": "RS256",
        "use": "sig",
        "kid": "8cRkI2o6OWtq9XMKeo-RzeY93nlhx43xs6rYa0t6Vww",
        "e": "AQAB",
        "kty": "RSA",
        "n": "a_very_long_base64_encoded_string"
    }
    ```


### IV.3 User data local storage


The default behavior of the `mozilla-django-oidc` library is to create or retrieve an user from the database when the authorization flow succeeds. This behavior has been adapted to stick to our **read only** paradigm.

What we do instead when the authorization flow succeeds is the following:

  - Create and return an [`OIDCUser`](../models.py) object (model not stored in database) populated with the OIDC user claims (it contains the user information shared by the identity provider (SSO)).

  - Store the OIDC user claims in the current session under the name `oidc_user_data`.

The relevant code is located in [`auth.py`](../auth.py)

The previous actions make the user authenticated only for the remote authentication process request. We still need a way to make the authentication persistent for subsequent requests. This is handled by the following code:

  - Our [`customOIDCAuthenticationBackend`](../auth.py) custom authentication backend always return a dummy `OIDCUser` when its `get_user` method is called. \
    This method is actually called when the `AuthenticationMiddleware` is requesting our custom authentication backend to authenticate the given user (session). Our implementation results in a dummy `OIDCUser` to be attached to the request (`request.user`).

  - Our custom middleware [`OIDCMiddleware`](../middleware.py) fills the dummy `OIDCUser` with the user data stored in the session. If this data cannot be found for some reason, the user is replaced with a fresh instance of `AnonymousUser`.

Note that this requires to declare the `customOIDCAuthenticationBackend`  in the `AUTHENTICATION_BACKENDS` and the `OIDCMiddleware` right after the `AuthenticationMiddleware`:

```python
# settings.py
AUTHENTICATION_BACKENDS = [
    # Authentication against our custom OpenID Connect provider (OP)
    'comments_views.journal.auth.customOIDCAuthenticationBackend',
    # Needed to login by username in Django admin, regardless of `allauth`
    'django.contrib.auth.backends.ModelBackend',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'comments_views.journal.middleware.OIDCMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.contrib.sites.middleware.CurrentSiteMiddleware',
]
```


## V. E-mails

This application only sends confirmation e-mail when an user submit a comment. E-mail template: [`comment_new.html`](../templates/mail/comment_new.html).


## VI. [Rights](../rights.py)

The comment rights for a journal user (SSO authenticated) are the following:

  - An user can see only the comments he/she has submitted.

  - An user can edit a comment before its moderation if no moderator are assigned.

  - An user cannot edit a comment before its moderation if a moderator is asigned (can be modified with `EDIT_BEFORE_MODERATION`, default `False`).

  - An user can delete a comment before its moderation (can be modified with `DELETE_BEFORE_MODERATION`, default `True`).

  - An user cannot delete a comment after its moderation (can be modified with `DELETE_AFTER_MODERATION`, default `False`).

  - An user cannot moderate any comment.


## VII. Django Settings

The application use the following site settings:

  - `COMMENTS_VIEWS_ARTICLE_COMMENTS` (default `False`) - Activates the display of the comments section on the article page.

  - `COMMENTS_VIEWS_COMMENTS_NESTING` (default `False`) - If `true`, displays the comments thread with maximum 1 level of nesting. Else no nesting.

  - `COMMENTS_VIEWS_POLICY_LINK` (optional) - A link to the comments' policy/guidelines. It's displayed in the header of the comments section when provided.

  - `COMMENTS_VIEWS_EDIT_BEFORE_MODERATION` (default `False`) - Whether a comment's author can edit his/her comment while it has not yet been moderated even if a moderator is already assigned to the comment.

  - `COMMENTS_VIEWS_DELETE_BEFORE_MODERATION` (default `True`) - Whether a comment's author can delete his/her comment while it has not yet been moderated.

  - `COMMENTS_VIEWS_DELETE_AFTER_MODERATION` (default `False`) - Whether a comment's author can delete his/her comment after it has been moderated. TODO: This setting is useless because it is prevented at the `comments_database` application level. Check the occurences of DELETE_AFTER_MODERATION UPDATE in `comments_database` to enable it.

  - `COMMENTS_VIEWS_CONSENT_TEXT` - Text to display next to the consent checkbox when an user submits a comment. Cf. [code](../app_settings.py) to see the expected object.
