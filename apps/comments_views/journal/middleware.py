from django.contrib.auth.models import AnonymousUser
from django.http import HttpRequest

from .models import OIDCUser


class OIDCMiddleware:
    """
    This middleware copies the user data from the session to the User object
    for OIDC users.
    If the user data is not present in the session, we replace the user object with
    an AnonymousUser instance.
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request: HttpRequest):
        assert hasattr(
            request, "session"
        ), "The custom OIDCMiddleware requires session middleware to be installed."
        assert hasattr(request, "user"), (
            "The custom OIDCMiddleware requires authentication middleware " "to be installed."
        )
        if isinstance(request.user, OIDCUser):
            user_data = request.session.get("oidc_user_data")
            if user_data:
                request.user.populate_fields(user_data)
            else:
                request.user = AnonymousUser()
        response = self.get_response(request)

        return response
