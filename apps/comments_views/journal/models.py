from django.contrib.auth.models import User


class OIDCUser(User):
    """
    Custom User class for OIDC authenticated users.
    This Model is not to be stored in DB.
    The class is used to: \\
            1.  Distinguish OIDC users from "normal" users. \\
            2.  Store the OIDC metadata properly in an user object so that \
                it's accessible just like a regular authenticated User.
    """

    name: str = ""
    provider: str = ""
    provider_uid: str = ""
    claims = {}

    class Meta:
        # Prevent the database table creation
        managed = False

    def save(self, *args, **kwargs):
        """Override default save to do nothing"""

    def populate_fields(self, claims: dict) -> None:
        """Fill the user object with the claims coming from the OP."""
        for key, value in claims.items():
            if hasattr(self, key):
                setattr(self, key, value)
        self.username = claims["email"]
        self.claims = {**claims}

    def get_user_id(self) -> int:
        return int(self.claims["sub"])
