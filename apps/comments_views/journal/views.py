import re
from typing import Any

from django.contrib import messages
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth import logout
from django.http import HttpRequest
from django.http import HttpResponse
from django.http import HttpResponseBadRequest
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import get_language
from django.utils.translation import gettext_lazy as _
from django.views.decorators.http import require_http_methods
from django.views.generic import TemplateView
from django.views.generic import View

from comments_api.constants import PARAM_BOOLEAN_VALUE
from comments_api.constants import PARAM_DOI
from comments_api.constants import PARAM_PREVIEW
from comments_api.constants import PARAM_STATUS
from comments_api.constants import PARAM_WEBSITE
from comments_api.constants import STATUS_SOFT_DELETED
from comments_api.constants import STATUS_SUBMITTED
from comments_api.constants import STATUS_VALIDATED
from comments_api.utils import date_to_str
from ptf.model_helpers import get_article_by_doi
from ptf.url_utils import add_fragment_to_url
from ptf.url_utils import format_url_with_params
from ptf.url_utils import validate_url
from ptf.utils import ckeditor_input_sanitizer
from ptf.utils import send_email_from_template

from ..core.forms import CommentForm
from ..core.forms import CommentFormAutogrow
from ..core.mixins import AbstractCommentRightsMixin
from ..core.rights import AbstractUserRights
from ..core.utils import comments_credentials
from ..core.utils import comments_server_url
from ..core.utils import format_comment
from ..core.utils import get_comment
from ..core.utils import make_api_request
from ..core.views import CommentChangeStatusView as BaseCommentChangeStatusView
from ..core.views import CommentDashboardDetailsView as BaseCommentDashboardDetailsView
from ..core.views import CommentDashboardListView as BaseCommentDashboardListView
from .app_settings import app_settings
from .mixins import OIDCCommentRightsMixin
from .mixins import SSOLoginRequiredMixin
from .models import OIDCUser
from .utils import add_pending_comment
from .utils import delete_pending_comment
from .utils import get_pending_comment


@require_http_methods(["GET"])
def reset_session(request: HttpRequest) -> HttpResponse:
    """
    Temporary function to reset the current user session data.
    """
    request.session.flush()
    return HttpResponse("<p>Current session has been reseted</p>")


@require_http_methods(["POST"])
def logout_view(request: HttpRequest) -> HttpResponseRedirect:
    """
    Base view to logout an OIDC authenticated user.
    We simply call auth.logout method. We could imagine logging out of the SSO
    server too if required.
    """
    logout(request)
    return HttpResponseRedirect(reverse("home"))


class SSOLoginView(TemplateView):
    """
    Base login view when SSO authentication is required.
    If the user is already SSO authenticated, he/she is redirected to the provided
    redirect URL, else home.
    """

    template_name = "dashboard/comment_login.html"

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        auth_url = reverse("oidc_authentication_init")
        next_request = self.request.GET.get("next")
        if next_request:
            auth_url = format_url_with_params(auth_url, {"next": next_request})
        context["authentication_url"] = auth_url

        return context

    def dispatch(self, request: HttpRequest, *args: Any, **kwargs: Any) -> HttpResponse:
        if isinstance(request.user, OIDCUser):
            redirect_uri = request.GET.get(REDIRECT_FIELD_NAME, "/")
            return HttpResponseRedirect(redirect_uri)
        return super().dispatch(request, *args, **kwargs)


class CommentSectionView(OIDCCommentRightsMixin, AbstractCommentRightsMixin, TemplateView):
    template_name = "common/article_comments.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        redirect_url = self.request.GET.get("redirect_url")
        if not redirect_url:
            raise Exception("Bad request: `redirect_url` parameter is missing")
        if not validate_url(redirect_url):
            raise Exception(
                f"Bad request: `redirect_url` parameter is malformed. You passed {redirect_url}"
            )

        doi = kwargs["doi"]
        preview_id = self.request.GET.get(PARAM_PREVIEW)
        rights = self.get_rights(self.request.user)
        context["ordered_comments"] = get_resource_comments(doi, rights, preview_id)

        # Add comment form for SSO authenticated users.
        # The authentication is provided by an external (custom) OIDC server.
        is_user_oidc_authenticated = False

        current_language = get_language()
        consent_text_list = [
            item["content"]
            for item in app_settings.CONSENT_TEXT
            if item["lang"] == current_language
        ]
        consent_text_list = (
            consent_text_list[0] if consent_text_list else app_settings.CONSENT_TEXT[0]["content"]
        )
        context["consent_text_list"] = consent_text_list

        if isinstance(self.request.user, OIDCUser):
            is_user_oidc_authenticated = True
            default_comment_form = CommentForm(prefix="default", initial={"doi": doi})
            reply_comment_form = CommentForm(prefix="reply", initial={"doi": doi})

            default_parent = ":~:init:~:"
            default_parent_author_name = ":~:init:~:"

            # Check if we should display the reply form
            comment_reply_to = self.request.GET.get("commentReplyTo")
            if comment_reply_to:
                try:
                    parent, parent_author_name = comment_reply_to.split(":~:")
                    parent = int(parent)
                    reply_comment_form = CommentForm(
                        prefix="reply",
                        initial={
                            "doi": doi,
                            "parent": parent,
                            "parent_author_name": parent_author_name,
                        },
                    )
                    default_parent = parent
                    default_parent_author_name = parent_author_name
                    context["display_reply_form"] = True
                except Exception:
                    pass

            # Check if there's a pending comment for that resource.
            # In that case we need to prefill it.
            pending_comment = get_pending_comment(self.request, doi)

            if pending_comment:
                prefix = pending_comment["prefix"]
                comment = pending_comment["comment"]
                comment_form = CommentForm(comment, prefix=prefix)
                if not comment_form.is_valid():
                    delete_pending_comment(self.request, doi)
                else:
                    # Replace the initial form with the pending data
                    if prefix == "reply":
                        # For the reply form, we need to retrieve the parent data
                        default_parent = comment_form.cleaned_data["parent"]
                        default_parent_author_name = comment_form.cleaned_data[
                            "parent_author_name"
                        ]
                        context["display_reply_form"] = True
                        reply_comment_form = comment_form
                    else:
                        default_comment_form = comment_form

            submit_base_url = reverse("submit_comment")
            params = {"redirect_url": redirect_url, "doi": doi, "form_prefix": "default"}
            context["default_comment_form"] = {
                "author_name": self.request.user.name,
                "date_submitted": timezone.now(),
                "submit_url": format_url_with_params(submit_base_url, params),
                "form_prefix": "default",
                "form": default_comment_form,
            }
            params["form_prefix"] = "reply"
            context["reply_comment_form"] = {
                "author_name": self.request.user.name,
                "date_submitted": timezone.now(),
                "parent": default_parent,
                "parent_author_name": default_parent_author_name,
                "submit_url": format_url_with_params(submit_base_url, params),
                "form_prefix": "reply",
                "form": reply_comment_form,
            }

        else:
            redirect_url = add_fragment_to_url(redirect_url, "add-comment-default")
            context["authentication_url"] = format_url_with_params(
                self.request.build_absolute_uri(reverse("oidc_authentication_init")),
                {"next": redirect_url},
            )

        context["is_user_oidc_authenticated"] = is_user_oidc_authenticated
        return context


class SubmitCommentView(OIDCCommentRightsMixin, AbstractCommentRightsMixin, View):
    """
    This view does not use SSOLoginRequiredMixin, it's checked manually instead.
    This enables us to cache the comment in the session in case the user was disconnected
    so we can pre-populate the form the next time he/she visits the page.
    """

    def post(
        self, request: HttpRequest, *args, **kwargs
    ) -> HttpResponseRedirect | HttpResponseBadRequest:
        """
        Posts the provided comment data to the comment server if the data is valid
        and the user is authenticated.

        We store the comment data in the session if the form is valid but something
        goes wrong (user not authenticated, HTTP error from comment server, ...).
        The user is redirected to the remote authentication page if he's not logged
        in.
        """
        # Check that the required query parameters are present and valid
        redirect_url = request.GET.get("redirect_url")
        if not redirect_url:
            return HttpResponseBadRequest("Bad request: `redirect_url` parameter is missing")
        if not validate_url(redirect_url):
            return HttpResponseBadRequest(
                f"Bad request: `redirect_url` parameter is malformed. You passed `{redirect_url}`"
            )
        response = HttpResponseRedirect(redirect_url)

        form_prefix = request.GET.get("form_prefix")
        if form_prefix and form_prefix not in ["default", "reply"]:
            messages.error(
                request,
                f"Bad request: `form_prefix` parameter is malformed. You passed `{form_prefix}`",
            )
            return response

        # For an update, id is present and the form is slightly different
        comment_id = request.POST.get("id")
        if comment_id:
            comment_form = CommentFormAutogrow(request.POST, prefix=form_prefix)
        else:
            comment_form = CommentForm(request.POST, prefix=form_prefix)

        if not comment_form.is_valid():
            messages.error(
                request,
                _(
                    "Une erreur s'est glissée dans le formulaire, votre commentaire n'a "
                    "pas été enregistré. Veuillez réessayer."
                ),
            )
            return response

        comment_data = comment_form.cleaned_data
        doi = comment_data["doi"]
        # Check that the DOI is attached to an article of the current site
        article = get_article_by_doi(doi)
        if not article:
            messages.error(request, _("L'article sélectionné n'existe pas."))
            return response

        # Temporary stores the request data in session
        add_pending_comment(request, doi, {"comment": request.POST, "prefix": form_prefix})
        content = comment_data["content"]
        now = date_to_str(timezone.now())
        date_submitted = comment_data.get("date_submitted")
        comment = {
            "doi": doi,
            "date_submitted": date_submitted if date_submitted else now,
            "date_last_modified": now,
            "raw_html": content,
            "sanitized_html": ckeditor_input_sanitizer(content),
            "parent": comment_data.get("parent"),
        }

        redirect_url = add_fragment_to_url(
            redirect_url,
            "add-comment-default" if form_prefix == "default" else "add-comment-reply",
        )

        # Check if the user is authenticated with SSO.
        if not isinstance(request.user, OIDCUser):
            # Redirect to the remote server authentication page.
            authentication_url = format_url_with_params(
                reverse("oidc_authentication_init"), {"next": redirect_url}
            )
            messages.warning(
                request,
                _(
                    "Vous avez été  déconnecté. Votre commentaire a été sauvegardé."
                    "Veuillez le soumettre à nouveau."
                ),
            )
            return HttpResponseRedirect(authentication_url)

        content_no_tag = re.sub(r"<.*?>|\n|\r|\t", "", content)

        if not content or len(content_no_tag) <= 15:
            messages.error(
                request, _("Un commentaire doit avoir au moins 15 caractères pour être valide.")
            )
            return response

        # Add author data
        author_data = request.user.claims
        comment.update(
            {
                "author_id": request.user.get_user_id(),
                "author_email": author_data["email"],
                "author_first_name": author_data.get("first_name"),
                "author_last_name": author_data.get("last_name"),
                "author_provider": author_data.get("provider"),
                "author_provider_uid": author_data.get("provider_uid"),
            }
        )

        # If the ID is present, it's an update
        update = comment_data["id"] is not None
        if update:
            rights = self.get_rights(request.user)
            query_params = rights.comment_rights_query_params()
            error, initial_comment = get_comment(
                query_params, comment_data["id"], request_for_message=request
            )
            if error:
                return response

            # Make sure the user has rights to edit the comment
            if not rights.comment_can_edit(initial_comment):
                messages.error(request, "Error: You can't edit the comment.")
                return response

            error, result_data = make_api_request(
                "PUT",
                comments_server_url(query_params, item_id=comment_data["id"]),
                request_for_message=request,
                auth=comments_credentials(),
                json=comment,
            )
        # Else, post a new comment
        else:
            error, result_data = make_api_request(
                "POST",
                comments_server_url(),
                request_for_message=request,
                auth=comments_credentials(),
                json=comment,
            )

        if error:
            return response

        messages.success(
            request,
            _(
                "Votre commentaire a été enregistré. Il sera publié après validation par un modérateur."
            ),
        )
        delete_pending_comment(request, doi)
        redirect_url = add_fragment_to_url(redirect_url, "")
        # Send a confirmation mail to the author of the comment
        if not update:
            context_data = {
                "full_name": f"{author_data.get('first_name')} {author_data.get('last_name')}",
                "article_url": request.build_absolute_uri(reverse("article", kwargs={"aid": doi})),
                "article_title": article.title_tex,
                "comment_dashboard_url": request.build_absolute_uri(reverse("comment_list")),
                "email_signature": "The editorial team",
            }
            subject = f"[{result_data['site_name'].upper()}] Confirmation of a new comment"
            send_email_from_template(
                "mail/comment_new.html",
                context_data,
                subject,
                to=[author_data["email"]],
                from_collection=result_data["site_name"],
            )

        return HttpResponseRedirect(redirect_url)


def get_resource_comments(doi: str, rights: AbstractUserRights, preview_id: Any) -> list:
    """
    Return all the comments of a resource, ordered by validated date, in a object
    structured as follow.
    A comment's children are added to the `children` list if `COMMENTS_NESTING`
    is set to `True`, otherwise they are appended to the list as regular comments.

    Returns:
    [
        {
            'content': top_level_comment,
            'children': [child_comment_1, child_comment_2, ...]
        }
    ]
    """
    # Retrieve the comments from the remote server
    # We assume this returns the comments ordered by `date_submitted`
    query_params = rights.comment_rights_query_params()
    query_params.update(
        {
            PARAM_WEBSITE: PARAM_BOOLEAN_VALUE,
            PARAM_DOI: doi,
            PARAM_STATUS: f"{STATUS_VALIDATED},{STATUS_SOFT_DELETED}",
        }
    )

    if preview_id:
        try:
            preview_id = int(preview_id)
            query_params[PARAM_PREVIEW] = preview_id
        except ValueError:
            preview_id = None

    url = comments_server_url(query_params)
    error, comments = make_api_request("GET", url, auth=comments_credentials())
    if error:
        raise Exception(f"Something went wrong while performing API call to {url}")

    # Contains the ordered comments, ready for display
    ordered_comments = []

    #### Only used when nesting is True
    # Contains the index of the top level parent in the ordered_comments list
    top_level_comment_order = {}
    # Stores the top level parent for each child comment
    # This enable immediate lookup of the top level parent of a multi-level child comment
    child_top_level_parent = {}
    ####

    for comment in comments:
        # Format data
        format_comment(comment)
        if comment.get("status") != STATUS_SUBMITTED:
            comment["show_buttons"] = True

        if app_settings.COMMENTS_NESTING:
            # Place the comment at the correct position
            id = comment["id"]
            parent = comment.get("parent")
            parent_id = parent.get("id") if parent else None
            # Whether it's a child comment
            if parent_id:
                # Whether the parent comment is a top level comment
                if parent_id in top_level_comment_order:
                    child_top_level_parent[id] = parent_id
                    top_level_parent_index = top_level_comment_order[parent_id]
                    if "children" not in ordered_comments[top_level_parent_index]:
                        ordered_comments[top_level_parent_index]["children"] = []
                    ordered_comments[top_level_parent_index]["children"].append(comment)
                else:
                    top_level_parent_id = child_top_level_parent[parent_id]
                    child_top_level_parent[id] = top_level_parent_id
                    top_level_parent_index = top_level_comment_order[top_level_parent_id]
                    ordered_comments[top_level_parent_index]["children"].append(comment)
            # Top level comment
            else:
                top_level_comment_order[id] = len(ordered_comments)
                ordered_comments.append({"content": comment})
        else:
            ordered_comments.append({"content": comment})

    return ordered_comments


class CommentDashboardListView(
    SSOLoginRequiredMixin, OIDCCommentRightsMixin, BaseCommentDashboardListView
):
    pass


class CommentDashboardDetailsView(
    SSOLoginRequiredMixin, OIDCCommentRightsMixin, BaseCommentDashboardDetailsView
):
    pass


class CommentChangeStatusView(
    SSOLoginRequiredMixin, OIDCCommentRightsMixin, BaseCommentChangeStatusView
):
    pass
