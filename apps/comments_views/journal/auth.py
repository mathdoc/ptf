from mozilla_django_oidc.auth import OIDCAuthenticationBackend

from .models import OIDCUser


class customOIDCAuthenticationBackend(OIDCAuthenticationBackend):
    """
    Override the default OIDC authentication backend provided by mozilla-django-oidc.
    We create an OIDCUser object on the fly, without persisting it in the database.
    """

    def get_user(self, user_id):
        """Returns a dummy OIDCUser object."""
        return OIDCUser(id=-1, pk=-1, username="temp_username", password="***")

    def filter_users_by_claims(self, claims):
        # This method is used to match incoming OIDC claims to the user DB.
        # We force the creation of a new user object when connecting with OIDC.
        return self.UserModel.objects.none()

    def create_user(self, claims):
        # We don't use the `create_user` method of the OIDCAuthenticationBackend
        # because it automatically creates and saves a new user in database.
        # user = super().create_user(claims)
        # Instead we create a custom User object
        user = OIDCUser(id=-1, pk=-1, username="temp_username", password="***")
        user.populate_fields(claims)

        # For user connecting through OIDC, we set a specific (small) session age.
        # We do this because we don't want to check regularly that the delivered
        # ID token is still valid (proper way to do things in OIDC configuration).
        # We will just force the user to authenticate again after the below time
        # in seconds has passed.
        self.request.session.set_expiry(8 * 60 * 60)

        # Store the meaningful user data (claims) directly in the session
        self.request.session["oidc_user_data"] = claims

        return user

    # This should never be called as we don't persist the user in the database.
    # The user object should be created on every request with the above method.
    def update_user(self, user: OIDCUser, claims):
        user.populate_fields(claims)
        # Store the meaningful user data (claims) directly in the session
        if "oidc_user_data" in self.request.session:
            self.request.session["oidc_user_data"].update(claims)
            self.request.session.save()
        # Should not happen ?
        else:
            self.request.session["oidc_user_data"] = claims

        return user
