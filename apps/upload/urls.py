from django.urls import path
from django.urls import re_path

from .views import ArticleInIssueSerializerView
from .views import ArticlePostpublicationModificationView
from .views import ArticlePostPublicationUpdateView
from .views import ArticleSerializerView
from .views import BookSerializerView
from .views import BookStatusView
from .views import CollectionSerializerView
from .views import CollectionStatusView
from .views import DeleteIssueView
from .views import IssueAddPDFView
from .views import IssueSerializerView
from .views import IssueStatusView
from .views import TranslatedArticleSerializerView

urlpatterns = [
    path('collections/', CollectionSerializerView.as_view(), name="upload-serials"),
    path('books/', BookSerializerView.as_view(), name='book_upload'),
    path('issues/', IssueSerializerView.as_view(), name='issue_upload'),
    path('articles/<path:colid>/', ArticleSerializerView.as_view(), name='article_upload'),
    path('article_in_issue/<path:pid>/', ArticleInIssueSerializerView.as_view(), name='article_in_issue_upload'),
    re_path(r'^translated_article/(?P<lang>[a-z]+)/(?P<doi>.+)/$', TranslatedArticleSerializerView.as_view(), name='trans_article_upload'),
    path('issue_pdf/<path:pid>/', IssueAddPDFView.as_view(), name='issue_pdf_upload'),
    path('issues/delete/<path:pid>/', DeleteIssueView.as_view(), name='issue_delete'),

    path('collections/<path:colid>/status/', CollectionStatusView.as_view(), name='collection_status'),
    path('books/<path:jid>/status/', BookStatusView.as_view(), name='book_status'),
    path('issues/<path:iid>/status/', IssueStatusView.as_view(), name='issue_status'),
    re_path(r'PCJ/post-publication-modification/(?P<colid>[A-Z-]+)/(?P<doi>.+)/$', ArticlePostpublicationModificationView.as_view(), name="post-publication-body-html-modification"),
    path('post-publication/update/<path:pid>/', ArticlePostPublicationUpdateView.as_view(), name='article-update')
]
