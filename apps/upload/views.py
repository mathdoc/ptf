import io
import json
import os
import subprocess

import requests

from django.conf import settings
from django.contrib.sites.models import Site
from django.http import Http404
from django.http import HttpResponse
from django.http import HttpResponseBadRequest
from django.http import HttpResponseServerError
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.template import defaultfilters
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View

from ptf import exceptions
from ptf import model_helpers
from ptf import models
from ptf.cmds import ptf_cmds
from ptf.cmds import xml_cmds
from ptf.display import resolver
from upload.utils import compute_article_for_pcj


def check_lock():
    return hasattr(settings, "LOCK_FILE") and os.path.isfile(settings.LOCK_FILE)


@method_decorator([csrf_exempt], name="dispatch")
class ArticleInIssueSerializerView(View):
    """
    Upload an Article in an existing issue
    """

    def post(self, request, *args, **kwargs):
        if check_lock():
            return HttpResponse(
                "The journal website is under maintenance",
                status=503,
                headers={"Retry-After": 180},
            )

        pid = kwargs["pid"]
        issue = model_helpers.get_container(pid)
        if issue is None:
            raise Http404(f"{pid} does not exist")

        if "xml" not in request.FILES:
            return HttpResponseBadRequest("No XML file provided", status=400)

        body = request.FILES["xml"].read().decode("utf-8")

        cmd = xml_cmds.addArticleXmlCmd(
            {
                "body": body,
                "issue": issue,
                "standalone": True,
                "from_folder": settings.RESOURCES_ROOT,
            }
        )
        cmd.set_collection(issue.get_collection())
        cmd.do()

        return JsonResponse({"message": "OK"})


@method_decorator([csrf_exempt], name="dispatch")
class SerializerView(View):
    update_cmd = None
    add_cmd = None

    def __init__(self):
        super().__init__()
        assert self.update_cmd is not None, "update_cmd is not defined"
        assert self.add_cmd is not None, "add_cmd is not defined"

    def post(self, request):
        if check_lock():
            return HttpResponse(
                "The journal website is under maintenance",
                status=503,
                headers={"Retry-After": 180},
            )

        try:
            self.add_cmd(self.get_data_from_request(request)).do()
        except exceptions.ResourceExists as exception:
            return HttpResponse(exception, status=409)
        except exceptions.ResourceDoesNotExist as exception:
            return HttpResponse(exception, status=404)
        except BaseException as exception:
            return HttpResponse(exception, status=400)
        return HttpResponse(status=201)

    def put(self, request):
        self.update_cmd(self.get_data_from_request(request)).do()
        return HttpResponse(status=204)

    def get_data_from_request(self, request, data=None):
        if not data:
            data = dict()
        data["body"] = request.body.decode("utf-8")
        data["from_folder"] = settings.RESOURCES_ROOT
        # Use defaults (keep_metadata=False / keep_translations=False) for issues sent by POST
        # => Everything comes from the XML
        return data


class StatusView(View):
    obj_id = ""
    model_helpers_getter = None

    def get_object(self):
        # utilisation de __func__ afin de récupérer la fonction original
        return self.model_helpers_getter.__func__(self.kwargs[self.obj_id])

    def get(self, request, *args, **kwargs):
        obj = self.get_object()
        if not obj:
            return HttpResponse(exceptions.ResourceDoesNotExist, status=404)

        return HttpResponse(self._format_resource(obj), content_type="application/json")

    def _format_resource(self, resource):
        site = Site.objects.get_current()
        dtp = resource.date_time_deployed(site)
        if dtp is not None:
            value = defaultfilters.date(dtp, "c")
        else:
            value = "Never"
        # ICI un raccourci : on suppose que pour une collection la date de raffinement est la date de deploy
        if isinstance(resource, models.Collection):
            modified = dtp
        else:
            resource = resource.cast()
            modified = resource.last_modified

        modified = defaultfilters.date(modified, "c")
        return json.dumps(
            {
                "status": "200",
                "id": resource.id,
                "pid": resource.pid,
                "sid": resource.sid,
                "date_time_deployed": value,
                "date_time_modified": modified,
            }
        )


@method_decorator([csrf_exempt], name="dispatch")
class ArticleSerializerView(View):
    """
    Upload article for PCJ
    """

    def post(self, request, *args, **kwargs):
        if check_lock():
            return HttpResponse(
                "The journal website is under maintenance",
                status=503,
                headers={"Retry-After": 180},
            )
        article_pid = request.POST.get("pid", None)
        colid = kwargs["colid"]
        collection = model_helpers.get_collection(colid, sites=False)

        if collection is None:
            raise Http404(f"{colid} does not exist")

        qs = collection.content.order_by(
            "-year", "-vseries", "-volume", "-volume_int", "-number_int"
        )
        if qs.count() == 0:
            raise Http404(f"{colid} has no issue")
        issue = qs.first()

        if "xml" not in request.FILES:
            return HttpResponseBadRequest("No XML file provided", status=400)

        body = request.FILES["xml"].read().decode("utf-8")

        params = {}

        with open(os.path.join(settings.LOG_DIR, "cmds_pcj.log"), "w", encoding="utf-8") as file_:
            for key in request.FILES:
                file_.write(request.FILES[key].name + "\n")

        html_file_name = ""
        for key in request.FILES:
            if key != "xml":
                file_obj = request.FILES[key]

                if key == "html":
                    file_obj = request.FILES[key]
                    relative_folder = resolver.get_relative_folder(
                        collection.pid, issue.pid, article_pid
                    )
                    folder = os.path.join(settings.RESOURCES_ROOT, relative_folder)
                    resolver.create_folder(folder)
                    full_file_name = os.path.join(folder, article_pid + ".html")
                    html_file_name = os.path.join(relative_folder, article_pid + ".html")
                    with open(full_file_name, "wb+") as destination:
                        for chunk in file_obj.chunks():
                            destination.write(chunk)
                elif ".png" in key or ".jpg" in key or ".jpeg" in key:
                    if "uploads" in key:
                        path, image = os.path.split(key)
                        relative_file_name = resolver.copy_file_obj_to_article_folder(
                            file_obj, colid, issue.pid, article_pid, is_image=True, path=path
                        )
                    else:
                        relative_file_name = resolver.copy_file_obj_to_article_folder(
                            file_obj, colid, issue.pid, article_pid, is_image=True
                        )
                else:
                    relative_file_name = resolver.copy_file_obj_to_article_folder(
                        file_obj, colid, issue.pid, article_pid
                    )

                if key == "image":
                    params["icon_location"] = relative_file_name

        from_folder = settings.RESOURCES_ROOT

        if html_file_name:
            article, params["body"] = compute_article_for_pcj(
                body,
                collection,
                from_folder,
                issue,
                html_file_name=html_file_name,
                force_execute=True,
            )
        else:
            article, params["body"] = compute_article_for_pcj(
                body, collection, from_folder, issue, force_execute=True
            )

        # Add the image to the article
        cmd = ptf_cmds.updateArticlePtfCmd(params)
        cmd.set_article(article)
        cmd.do()

        # Direct upload to the test_website
        xml = ptf_cmds.exportPtfCmd(
            {
                "pid": article.pid,
                "with_djvu": False,
                "article_standalone": True,
                "collection_pid": collection.pid,
                "export_to_website": False,
                "export_folder": settings.LOG_DIR,
            }
        ).do()

        with open(
            os.path.join(settings.LOG_DIR, "pcj_article.log"), "w", encoding="utf-8"
        ) as file2_:
            file2_.write(xml)

        xml_file = io.StringIO(xml)
        files = {"xml": xml_file}

        server_url = getattr(collection, "test_website")()
        url = server_url + reverse("article_in_issue_upload", kwargs={"pid": issue.pid})
        # verify=False: ignore TLS certificate
        header = {}
        requests.post(url, headers=header, files=files, verify=False)

        if article.subject_of.all().exists():
            relation = article.subject_of.first()
            corrected_article = relation.related

            # Direct upload to the test_website
            xml = ptf_cmds.exportPtfCmd(
                {
                    "pid": corrected_article.pid,
                    "with_djvu": False,
                    "article_standalone": True,
                    "collection_pid": collection.pid,
                    "export_to_website": False,
                    "export_folder": settings.LOG_DIR,
                }
            ).do()

            with open(
                os.path.join(settings.LOG_DIR, "pcj_article.log"), "w", encoding="utf-8"
            ) as file2_:
                file2_.write(xml)

            xml_file = io.StringIO(xml)
            files = {"xml": xml_file}

            server_url = getattr(collection, "test_website")()
            url = server_url + reverse("article_in_issue_upload", kwargs={"pid": issue.pid})
            # verify=False: ignore TLS certificate
            header = {}
            requests.post(url, headers=header, files=files, verify=False)

        return JsonResponse({"message": "OK"})


class BookSerializerView(SerializerView):
    add_cmd = xml_cmds.addOrUpdateBookXmlCmd
    update_cmd = xml_cmds.updateBookXmlCmd


class BookStatusView(StatusView):
    obj_id = "jid"
    model_helpers_getter = model_helpers.get_container


class CollectionSerializerView(SerializerView):
    add_cmd = xml_cmds.addCollectionsXmlCmd
    update_cmd = xml_cmds.updateCollectionsXmlCmd


class CollectionStatusView(StatusView):
    obj_id = "colid"
    model_helpers_getter = model_helpers.get_collection


@method_decorator([csrf_exempt], name="dispatch")
class DeleteView(View):
    add_cmd = None
    model_class = None

    def delete(self, request, pid):
        # key = request.body
        # TODO CHECK KEY
        obj = get_object_or_404(self.model_class, pid=pid, sites=settings.SITE_ID)
        cmd = self.add_cmd({"pid": obj.pid, "ctype": getattr(obj, "ctype", "")})
        cmd.set_provider(models.Provider.objects.get(name="mathdoc"))
        cmd.set_object_to_be_deleted(obj)
        cmd.undo()
        return HttpResponse(status=204)


class DeleteIssueView(DeleteView):
    model_class = models.Container

    add_cmd = ptf_cmds.addContainerPtfCmd


@method_decorator([csrf_exempt], name="dispatch")
class IssueAddPDFView(View):
    def post(self, request, *args, **kwargs):
        if check_lock():
            return HttpResponse(
                "The journal website is under maintenance",
                status=503,
                headers={"Retry-After": 180},
            )

        pid = kwargs["pid"]
        issue = model_helpers.get_container(pid)
        if issue is None:
            raise Http404(f"{pid} does not exist")

        if not issue.datastream_set.filter(mimetype="application/pdf").exists():
            colid = issue.get_top_collection().pid
            pdf_path = resolver.get_disk_location(settings.RESOURCES_ROOT, colid, "pdf", pid)
            if not os.path.isfile(pdf_path):
                raise Http404(f"{pdf_path} does not exist")

            location = os.path.join(issue.get_relative_folder(), pid + ".pdf")
            datastream = models.DataStream(
                resource=issue,
                base=None,
                rel="full-text",
                mimetype="application/pdf",
                location=location,
                text="",
                seq=0,
            )
            datastream.save()

        return HttpResponse(status=201)


class IssueSerializerView(SerializerView):
    add_cmd = xml_cmds.addOrUpdateIssueXmlCmd
    update_cmd = xml_cmds.replaceIssueXmlCmd


@method_decorator([csrf_exempt], name="dispatch")
class IssueStatusView(StatusView):
    obj_id = "iid"
    model_helpers_getter = model_helpers.get_container


@method_decorator([csrf_exempt], name="dispatch")
class TranslatedArticleSerializerView(View):
    """
    Upload a translated article
    """

    def post(self, request, *args, **kwargs):
        doi = kwargs["doi"]
        lang = kwargs["lang"]
        date_published_str = request.GET.get("date_published", "")

        article = model_helpers.get_article_by_doi(doi)
        if article is None:
            raise Http404(f"{doi} does not exist")

        issue = article.my_container
        collection = article.get_collection()

        if "xml" not in request.FILES:
            return HttpResponseBadRequest("No XML file provided", status=400)

        with open(os.path.join(settings.LOG_DIR, "cmds_trad.log"), "w", encoding="utf-8") as file_:
            for key in request.FILES:
                file_.write(request.FILES[key].name + "\n")

        html_file_name = ""
        pdf_file_name = ""
        for key in request.FILES:
            if key != "xml":
                file_obj = request.FILES[key]

                relative_folder = resolver.get_relative_folder(
                    collection.pid, issue.pid, article.pid
                )
                folder = os.path.join(settings.RESOURCES_ROOT, relative_folder)
                resolver.create_folder(folder)
                full_file_name = os.path.join(folder, f"{article.pid}-{lang}.html")
                html_file_name = os.path.join(relative_folder, f"{article.pid}-{lang}.html")
                pdf_file_name = os.path.join(relative_folder, f"{article.pid}-{lang}.pdf")

                with open(full_file_name, "wb+") as destination:
                    for chunk in file_obj.chunks():
                        destination.write(chunk)

        body = request.FILES["xml"].read().decode("utf-8")

        with open(os.path.join(settings.LOG_DIR, "cmds_trad.log"), "a", encoding="utf-8") as file_:
            file_.write("XML read\n")

        # addTranslatedArticleXmlCmd adds 2 DataStreams (html and pdf) to the translated article
        cmd = xml_cmds.addTranslatedArticleXmlCmd(
            {
                "body": body,
                "lang": lang,
                "html_file_name": html_file_name,
                "pdf_file_name": pdf_file_name,
                "from_folder": settings.RESOURCES_ROOT,
                "date_published_str": date_published_str,
            }
        )
        try:
            article = cmd.do()
        except subprocess.CalledProcessError:
            e = exceptions.PDFException(
                "Unable to compile the article PDF. Please contact the centre Mersenne"
            )
            return HttpResponseServerError(e)
        except Exception:
            e = RuntimeError(
                "Unable to prepare the translation. Please contact the centre Mersenne"
            )
            return HttpResponseServerError(e)

        with open(os.path.join(settings.LOG_DIR, "cmds_trad.log"), "a", encoding="utf-8") as file_:
            file_.write("article added\n")

        # Direct upload to the test_website

        xml = ptf_cmds.exportPtfCmd(
            {
                "pid": article.pid,
                "with_djvu": False,
                "article_standalone": True,
                "collection_pid": collection.pid,
                "export_to_website": False,
                "export_folder": settings.LOG_DIR,
            }
        ).do()

        with open(os.path.join(settings.LOG_DIR, "cmds_trad.log"), "a", encoding="utf-8") as file_:
            file_.write("xml for website written in LOG_DIR\n")

        xml_file = io.StringIO(xml)
        files = {"xml": xml_file}

        server_url = getattr(collection, "test_website")()
        url = server_url + reverse("article_in_issue_upload", kwargs={"pid": issue.pid})
        # verify=False: ignore TLS certificate
        header = {}
        try:
            response = requests.post(url, headers=header, files=files, verify=False)
            status = response.status_code
        except requests.exceptions.ConnectionError:
            e = exceptions.ServerUnderMaintenance(
                "The journal test website is under maintenance. Please try again later."
            )
            return HttpResponseServerError(e, status=503)

        with open(os.path.join(settings.LOG_DIR, "cmds_trad.log"), "a", encoding="utf-8") as file_:
            file_.write("test website request sent\n")

        if status == 503:
            e = exceptions.ServerUnderMaintenance(
                "The journal test website is under maintenance. Please try again later."
            )
            return HttpResponseServerError(e, status=503)
        elif not (199 < status < 205):
            e = RuntimeError(
                "Unable to upload the translation to the journal test website. Please contact the centre Mersenne"
            )
            return HttpResponseServerError(e)

        with open(os.path.join(settings.LOG_DIR, "cmds_trad.log"), "a", encoding="utf-8") as file_:
            file_.write("OK. Done\n")

        return JsonResponse({"message": "OK"})


@method_decorator(csrf_exempt, name="dispatch")
class ArticlePostPublicationUpdateView(View):
    def post(self, requests, **kwargs):
        article_pid = kwargs.get("pid", None)
        article = model_helpers.get_article(article_pid)
        relative_folder = requests.POST["relative_folder"]
        html_file_path = os.path.join(
            settings.RESOURCES_ROOT, relative_folder, article_pid + ".html"
        )
        html_obj = requests.FILES["html"]
        with open(html_file_path, "wb+") as new_html:
            for chunk in html_obj.chunks():
                new_html.write(chunk)

        with open(html_file_path, encoding="utf-8") as file_:
            body_html = file_.read()
        article.body_html = body_html
        article.save()
        return JsonResponse({"message": "OK"})


@method_decorator(csrf_exempt, name="dispatch")
class ArticlePostpublicationModificationView(View):
    def post(self, request, **kwargs):
        colid = kwargs.get("colid", None)
        collection = model_helpers.get_collection(colid, sites=False)

        doi = kwargs.get("doi", None)
        article = model_helpers.get_article_by_doi(doi)
        article_pid = article.pid
        issue = article.my_container.pid
        file_obj = request.FILES["html"]
        relative_folder = resolver.get_relative_folder(colid, issue, article_pid)
        folder = os.path.join(settings.RESOURCES_ROOT, relative_folder)
        full_file_name = os.path.join(folder, article_pid + ".html")
        with open(full_file_name, "wb+") as destination:
            for chunk in file_obj.chunks():
                destination.write(chunk)

        new_html = ""
        with open(full_file_name) as html:
            new_html = html.read()
        files = {"html": io.StringIO(new_html)}
        server_url = getattr(collection, "website")()
        if server_url[-1] == "/":
            server_url = server_url[:-1]
        url = server_url + reverse("article-update", kwargs={"pid": article.pid})
        url = url.replace("/submit", "")
        response = requests.post(
            url,
            files=files,
            data={"relative_folder": relative_folder},
            verify=False,
        )
        if response.status_code != 200:
            pass

        return JsonResponse({"message": "ok"})
