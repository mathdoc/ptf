from django.conf import settings

from ptf import tex
from ptf import utils
from ptf.cmds import xml_cmds


def compute_article_for_pcj(
    body, collection, from_folder, issue, html_file_name=None, force_execute=False
):
    cmd = xml_cmds.addPCJArticleXmlCmd(
        {
            "body": body,
            "issue": issue,
            "standalone": True,
            "html_file_name": html_file_name,
            "from_folder": from_folder,
            "collection": collection,
        }
    )

    article = cmd.do()

    # ptf-tools: new articles are not ready for publication
    if article.date_published is None:
        article.do_not_publish = True
    article.save()
    # Create the tex file
    lines = tex.create_tex_for_pcj(article)
    pdf_filename = tex.compile_tex(lines, article)
    # Extract full text from the PDF
    if settings.MERSENNE_CREATE_FRONTPAGE or force_execute:
        body = utils.pdf_to_text(pdf_filename)
        return article, body

    # for debug
    else:
        body = utils.pdf_to_text(pdf_filename)

        return article, body
