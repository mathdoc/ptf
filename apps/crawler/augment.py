import requests

from matching import crossref
from ptf import model_data_converter
from ptf.cmds import xml_cmds

from .zbmath import zbmath_request_article


def augment_article(article, source, what="all", doi_to_fetch=None):
    """
    Fetch metadata from external sources and augment an article metadata.

    what is the database to query: crossref/zbmath/all
    doi_to_fetch let's you specify an alternate DOI to use, for example in the case of a translation

    """

    # An article in Mathnet.ru may have a DOI and a DOI for the translation
    # Metadata in Crossref for the DOI are in Russian: use the translated version.

    databases = ["crossref", "zbmath"]

    if source == "EUDML":
        # Ignore some databases of some sources
        # TODO: Move the check in a concrete crawler ?
        databases = ["zbmath"]

    if what != "all":
        databases = [what]

    for what in databases:
        article_data = fetch_article_data(article, what, doi_to_fetch)
        if article_data is not None:
            update_article(article, source, article_data, what)


def update_article(article, source, article_data, what):
    """
    Update the article with external metadata stored in article_data
    """
    collection = article.get_collection()
    existing_article_data = model_data_converter.db_to_article_data(article)

    model_data_converter.update_data_for_jats(article_data)

    # Protection to make sure the database returned something valid.
    if article_data.title_xml:
        # Preserve existing values not set by the Crossref/zbMATH API
        article_data.pid = article.pid
        article_data.doi = article.doi
        article_data.seq = existing_article_data.seq
        article_data.ids = existing_article_data.ids
        article_data.extids = existing_article_data.extids
        article_data.ext_links = existing_article_data.ext_links
        if not article_data.fpage:
            article_data.fpage = existing_article_data.fpage
        if not article_data.lpage:
            article_data.lpage = existing_article_data.lpage
        article_data.streams = existing_article_data.streams

        # Replace with metadata from the database (if it is found)

        # TITLE
        if not article_data.title_html or (
            what == "crossref" and existing_article_data.title_html
        ):
            # TODO: handle Crossref titles with formulas.
            # The quality depends on the publisher so we might not be able to know in general if we have to replace
            article_data.title_html = existing_article_data.title_html
            article_data.title_tex = existing_article_data.title_tex
            article_data.title_xml = existing_article_data.title_xml

        # AUTHOR
        if not article_data.contributors:
            article_data.contributors = existing_article_data.contributors

        # ABSTRACTS
        if not article_data.abstracts:
            article_data.abstracts = existing_article_data.abstracts

        params = {
            "xarticle": article_data,
            "use_body": False,
            "issue": article.my_container,
            "standalone": True,
        }
        cmd = xml_cmds.addArticleXmlCmd(params)
        cmd.set_collection(collection)
        article = cmd.do()
        if not article_data.title_html:
            print(f"   Warning: {article.pid} has no title_html after XmlCmd")


def fetch_article_data(article, what, doi_to_fetch=None):
    article_data = None

    if what == "crossref":
        doi_to_fetch = doi_to_fetch if doi_to_fetch is not None else article.doi

        # qs = article.extid_set.filter(id_type="doi-translation")
        # if qs:
        #     doi_translation = qs.first().id_value
        #     print(f"    DOI Translation {doi_translation}")

        article_data = crossref.fetch_article(doi_to_fetch)
    elif what == "zbmath":
        qs = article.extid_set.filter(id_type="zbl-item-id")
        if qs:
            zblid = qs.first().id_value
            if "|" not in zblid and "%7" not in zblid:
                attempt = 0
                done = False
                while not done and attempt < 3:
                    try:
                        article_data = zbmath_request_article(zblid)
                        done = True
                    except (
                        requests.exceptions.ConnectionError,
                        requests.exceptions.ReadTimeout,
                    ):
                        attempt += 1

    return article_data
