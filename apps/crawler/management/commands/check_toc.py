from crawler.factory import crawler_factory
from crawler.utils import get_eudml_collections

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Export a container or an article or a collection"

    def add_arguments(self, parser):
        parser.add_argument(
            "-pid",
            action="store",
            dest="pid",
            type=str,
            default=None,
            required=True,
            help="Collection pid",
        )
        parser.add_argument(
            "-source",
            action="store",
            dest="source",
            type=str,
            default=None,
            required=True,
            help="Source. Allowed values: EUDML/AMP",
        )

    def handle(self, *args, **options):
        pid = options["pid"]
        source = options["source"]

        eudml_collections = get_eudml_collections()
        eudml_col = eudml_collections[pid]

        crawler = crawler_factory(source, eudml_col["pid"], eudml_col["url"], "")
        crawler.check_toc()
