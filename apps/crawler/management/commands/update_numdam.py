from crawler.utils import get_numdam_collections

from django.core.management.base import BaseCommand

from gdml.models import Periode
from gdml.models import Source
from ptf.model_helpers import get_collection


class Command(BaseCommand):
    help = "Create source/periode for the Numdam collections present in the database (use after the import command)"

    def get_or_create_source(self):
        # TODO remove when the numdam collections are in all_cols.csv
        # We can then use the NumdamCrawler

        try:
            source = Source.objects.get(name="Numdam")
        except Source.DoesNotExist:
            source = Source(
                name="Numdam",
                domain="NUMDAM",
                website="http://www.numdam.org",
                create_xissue=True,
                periode_href="",
                article_href="",
                pdf_href="",
            )
            source.save()

        return source

    def get_or_create_periode(self, source, collection):
        # TODO remove when the numdam collections are in all_cols.csv
        # We can then use the NumdamCrawler

        if collection is None or source is None:
            return None

        qs = Periode.objects.filter(collection=collection).exclude(source=source)
        if qs.exists():
            print(f"{collection.pid} has a periode which is not Numdam. Delete")
            qs.delete()

        qs = Periode.objects.filter(collection=collection, source=source)
        if qs.exists():
            periode = qs.first()
        else:
            periode = Periode(
                collection=collection,
                source=source,
                title=collection.title_tex,
                issue_href="",
                collection_href="",
                doi_href="",
                published=False,
                begin=0,
                end=0,
                first_issue=None,
                last_issue=None,
            )
            periode.save()

        return periode

    def handle(self, *args, **options):
        numdam_collections = get_numdam_collections()

        for colid in numdam_collections:
            collection = get_collection(colid)

            if collection is not None:
                source = self.get_or_create_source()
                self.get_or_create_periode(source, collection)
