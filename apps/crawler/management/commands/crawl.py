import traceback

from alive_progress import alive_bar
from crawler.types import JSONCol
from crawler.factory import crawler_factory
from crawler.utils import get_all_cols
from crawler.utils import get_cols_by_source
from crawler.utils import insert_crawl_event_in_history

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Crawl collections of a given source"

    def add_arguments(self, parser):
        parser.add_argument(
            "-pid",
            action="store",
            dest="pid",
            type=str,
            default=None,
            required=True,
            help="Collection pid or ALL to crawl all collections of a source",
        )
        parser.add_argument(
            "-source",
            action="store",
            dest="source",
            type=str,
            default=None,
            required=True,
            help="Source. Allowed values: EUDML/AMP",
        )
        parser.add_argument(
            "-u",
            action="store",
            dest="username",
            type=str,
            default=None,
            required=True,
            help="Username. Needed to store the user in an HistoryEvent",
        )
        parser.add_argument(
            "-col_only",
            action="store_true",
            dest="col_only",
            default=False,
            help="Crawl only the collection level, not the issues",
        )
        parser.add_argument(
            "-start_pid",
            action="store",
            dest="start_pid",
            type=str,
            default=None,
            help="Start crawling at a given pid (issue or collection if -pid=ALL)",
        )

        parser.add_argument(
            "-issue",
            action="store_true",
            dest="import_issue",
            default=None,
            help="Issue type",
        )

    def crawl_one_col(self, col, source, col_only, username, progress_bar=None, start_pid=None):
        try:
            url_collection = col["sources"][source]
            crawler = crawler_factory(
                source, col["pid"], url_collection, username, progress_bar, start_pid
            )
            crawler.crawl_collection(col_only=col_only)
            insert_crawl_event_in_history(
                col["pid"], source, username, "OK", 0, "")
        except Exception as e:
            message = getattr(e, "message", "")
            message += "\n" + traceback.format_exc()
            insert_crawl_event_in_history(
                col["pid"], source, username, "ERROR", 0, message)
            print(col["pid"], message)

    def crawl_one_issue(self, pid: str, col: JSONCol, source: str, username: str):
        try:
            crawler = crawler_factory(
                source, col["pid"], col["sources"][source], username)
            merged_xissues = crawler.crawl_collection(col_only=True)
            crawler.crawl_issue(pid, merged_xissues)

            insert_crawl_event_in_history(
                col["pid"], source, username, "OK", 0, pid)
        except Exception as e:
            message = getattr(e, "message", "")
            message += "\n" + traceback.format_exc()
            insert_crawl_event_in_history(
                col["pid"], source, username, "ERROR", 0, message)
            print(col["pid"], message)

    def handle(self, *args, **options):
        pid = colid = options["pid"]
        source = options["source"]
        username = options["username"]
        col_only = options["col_only"]
        start_pid = options["start_pid"]
        issue = options["import_issue"]

        all_cols = get_all_cols()

        if "_" in colid:
            parts = colid.split("_")
            colid = parts[0]

        if colid != "ALL":
            col = all_cols[colid]
            if issue:
                if (source == "" or source is None):
                    raise ValueError(
                        "-issue cannot be used without a specific source")
                self.crawl_one_issue(pid, col, source, username)
            else:
                self.crawl_one_col(col, source, col_only,
                                   username, start_pid=start_pid)
        else:
            cols = get_cols_by_source(source)
            if start_pid is not None:
                new_cols = []
                for col in cols:
                    if start_pid is None or col["pid"] == start_pid:
                        start_pid = None
                        new_cols.append(col)
                cols = new_cols

            with alive_bar(
                len(cols), dual_line=True, title="Crawl XXX", stats="(eta {eta})", force_tty=True
            ) as progress_bar:
                for col in cols:
                    progress_bar.title = f"Crawl {col['pid']}"
                    progress_bar()
                    self.crawl_one_col(col, source, col_only,
                                       username, progress_bar)
