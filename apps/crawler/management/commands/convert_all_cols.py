import csv
import json
import os

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Convert all collections csv to json"

    def add_arguments(self, parser):
        parser.add_argument(
            "-i",
            action="store",
            dest="input",
            type=str,
            default=None,
            required=True,
            help="path for input file",
        )
        parser.add_argument(
            "-o",
            action="store",
            dest="output",
            type=str,
            default=None,
            required=True,
            help="output file destination",
        )

    def numdam_collections(self):
        return [
            "ACIRM",
            "ALCO",
            "AFST",
            "AIHPC",
            "AIHPA",
            "AIHPB",
            "AIF",
            "AIHP",
            "AUG",
            "AMPA",
            "AHL",
            "AMBP",
            "ASENS",
            "ASCFPA",
            "ASCFM",
            "ASNSP",
            "AST",
            "BSMF",
            "BSMA",
            "CTGDC",
            "BURO",
            "CSHM",
            "CG",
            "CM",
            "CRMATH",
            "CML",
            "CJPS",
            "CIF",
            "DIA",
            "COCV",
            "M2AN",
            "PS",
            "GAU",
            "GEA",
            "STS",
            "TAN",
            "JSFS",
            "JEP",
            "JMPA",
            "JTNB",
            "JEDP",
            "CAD",
            "CCIRM",
            "RCP25",
            "MSIA",
            "MRR",
            "MSH",
            "MSMF",
            "MSM",
            "NAM",
            "OJMO",
            "PHSC",
            "PSMIR",
            "PDML",
            "PMB",
            "PMIHES",
            "PMIR",
            "RO",
            "RCP",
            "ITA",
            "RSMUP",
            "RSA",
            "RHM",
            "SG",
            "SB",
            "SBCD",
            "SC",
            "SCC",
            "SAF",
            "SDPP",
            "SMJ",
            "SPHM",
            "SPS",
            "STNB",
            "STNG",
            "TSG",
            "SD",
            "SE",
            "SEDP",
            "SHC",
            "SJ",
            "SJL",
            "SLSEDP",
            "SLDB",
            "SL",
            "SPK",
            "SAC",
            "SMS",
            "SLS",
            "SSL",
            "SENL",
            "SSS",
            "SAD",
            "THESE",
            "SMAI-JCM",
            "WBLN",
        ]

    def convert_all_cols(self, filename, output):
        """
        Reads a CSV file provided by the Documentation team and converts it to a dict with the journal PIDs as keys
        """

        all_collections = {}
        # elibm_collections = get_elibm_collections()
        NUMDAM_COLLECTIONS = self.numdam_collections()
        with open(filename, encoding="utf-8", newline="") as csv_file:
            reader = csv.reader(csv_file, delimiter=",", quotechar='"', quoting=csv.QUOTE_MINIMAL)

            pos_state = 1
            keys = ""
            for index_row, row in enumerate(reader):
                if index_row == 0:
                    keys = row
                else:
                    state = row[pos_state]
                    dict_obj = {}
                    dict_obj["sources"] = {}
                    for index_col, item in enumerate(keys):
                        if item == "source_id":
                            source_name = row[index_col]
                            url = row[index_col + 1]
                            if source_name and url:
                                dict_obj["sources"][source_name] = url
                        elif item not in ["source_id", "Commentaire", "url"]:
                            dict_obj[item] = row[index_col]
                    pid = row[0]

                    if pid not in NUMDAM_COLLECTIONS:
                        if state == "":
                            all_collections[pid] = dict_obj

            print(all_collections)

            if output == "":
                output = os.path.join(
                    os.path.dirname(os.path.abspath(__file__)),
                    "data/all_cols.json",
                )

            with open(output, "w", encoding="utf8") as list_json:
                items = json.dumps(all_collections, ensure_ascii=False)
                list_json.write(
                    items,
                )

        return "success to convert csv file to json"

    def handle(self, *args, **options):
        output = options["output"]
        input = options["input"]

        return self.convert_all_cols(input, output)
