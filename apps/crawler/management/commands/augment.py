import traceback

from alive_progress import alive_bar
from crawler.augment import augment_article
from crawler.utils import insert_crawl_event_in_history

from django.core.management.base import BaseCommand
from django.db.models import Q

from ptf.models import Article


class Command(BaseCommand):
    help = "Augment the article metadata of a collection/source using all/some databases (crossref, zbmath)"

    def add_arguments(self, parser):
        parser.add_argument(
            "-pid",
            action="store",
            dest="pid",
            type=str,
            default=None,
            required=True,
            help="Collection pid",
        )
        parser.add_argument(
            "-source",
            action="store",
            dest="source",
            type=str,
            default=None,
            required=True,
            help="Source (EUDML/AMP/TAC...)",
        )
        parser.add_argument(
            "-u",
            action="store",
            dest="username",
            type=str,
            default=None,
            required=True,
            help="Username. Needed to store the user in an HistoryEvent",
        )
        parser.add_argument(
            "-what",
            action="store",
            dest="what",
            type=str,
            default=None,
            required=True,
            choices=["crossref", "zbmath", "all"],
            help="Database used to augment metadata.",
        )

    def handle(self, *args, **options):
        colid = pid = options["pid"]
        source = options["source"]
        username = options["username"]
        what = options["what"]

        if "_" in pid:
            parts = colid.split("_")
            colid = parts[0]

            qs = Article.objects.filter(my_container__pid=pid).order_by("pid")
        else:
            qs = Article.objects.filter(
                Q(my_container__my_collection__pid=pid)
                | Q(my_container__my_collection__parent__pid=pid)
            ).order_by("pid")

        try:
            with alive_bar(
                qs.count(),
                dual_line=True,
                title=f"Augment {pid}",
                stats="(eta {eta})",
            ) as progress_bar:
                for article in qs:
                    progress_bar.text = article.pid
                    augment_article(article, source, what)
                    progress_bar()

            insert_crawl_event_in_history(colid, source, username, "OK", 0, pid, "augment")

        except Exception as e:
            message = getattr(e, "message", "")
            message += "\n" + traceback.format_exc()
            insert_crawl_event_in_history(colid, source, username, "ERROR", 0, message, "augment")
            print(message)
