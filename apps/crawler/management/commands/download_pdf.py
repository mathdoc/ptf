import os
import traceback

import pypdf
import requests
from alive_progress import alive_bar
from crawler.utils import insert_crawl_event_in_history

from django.conf import settings
from django.core.management.base import BaseCommand
from django.db.models import Q

from gdml.models import Periode
from ptf.display import resolver
from ptf.models import Article


class Command(BaseCommand):
    help = "Download the PDF of a collection/source"

    def add_arguments(self, parser):
        parser.add_argument(
            "-pid",
            action="store",
            dest="pid",
            type=str,
            default=None,
            required=True,
            help="Collection pid",
        )
        parser.add_argument(
            "-source",
            action="store",
            dest="source",
            type=str,
            default=None,
            required=True,
            help="Source (EUDML/AMP/TAC...)",
        )
        parser.add_argument(
            "-u",
            action="store",
            dest="username",
            type=str,
            default=None,
            required=True,
            help="Username. Needed to store the user in an HistoryEvent",
        )
        parser.add_argument(
            "-remove_first_page",
            action="store_true",
            dest="remove_first_page",
            default=False,
            help="Remove the PDF first page",
        )

    def download_pdf(self, obj, remove_first_page):
        collection = obj.get_collection()

        qs = obj.datastream_set.filter(mimetype="application/pdf")
        if qs:
            datastream = qs.first()
            href = datastream.location
            embargo = False

            if hasattr(obj, "my_container"):
                container_id = obj.my_container.pid
                obj_id = obj.pid
            else:
                # Download a PDF of a book
                container_id = obj.pid
                obj_id = None

            if href.find("http") == 0:
                disk_location = resolver.get_disk_location(
                    settings.RESOURCES_ROOT,
                    collection.pid,
                    "pdf",
                    container_id=container_id,
                    article_id=obj_id,
                    do_create_folder=True,
                )

                if not os.path.isfile(disk_location):
                    # Download only if not already present

                    attempt = 0
                    done = False
                    while not done and attempt < 3:
                        try:
                            r = requests.get(href, stream=True, timeout=10.0)
                            if len(r.text) > 10 and r.text[0:21] == "<!DOCTYPE html PUBLIC":
                                print(f"{obj.doi} has an embargo, no PDF")
                                done = True
                                embargo = True
                            else:
                                done = True

                                # Remove front page
                                if remove_first_page:
                                    temp_location = os.path.join(settings.TEMP_FOLDER, "file.pdf")
                                    with open(temp_location, "wb") as f_:
                                        f_.write(r.content)

                                    pdf_reader = pypdf.PdfReader(temp_location)
                                    pdf_writer = pypdf.PdfWriter()
                                    for page in range(len(pdf_reader.pages)):
                                        current_page = pdf_reader.pages[page]
                                        if page > 0:
                                            pdf_writer.add_page(current_page)

                                    with open(disk_location, "wb") as f_:
                                        pdf_writer.write(f_)
                                else:
                                    with open(disk_location, "wb") as f_:
                                        f_.write(r.content)

                        except (
                            requests.exceptions.ConnectionError,
                            requests.exceptions.ReadTimeout,
                            pypdf.errors.PdfReadError,
                        ):
                            attempt += 1

                    # if not embargo:
                    #     time.sleep(1)
            else:
                disk_location = resolver.get_disk_location(
                    settings.RESOURCES_ROOT,
                    collection.pid,
                    "pdf",
                    container_id=container_id,
                    article_id=obj_id,
                    do_create_folder=False,
                )

            if os.path.isfile(disk_location):
                new_location = resolver.get_relative_folder(
                    collection.pid, container_id=container_id, article_id=obj_id
                )
                pdf_filename = os.path.join(new_location, obj.pid + ".pdf")

                if datastream.location != pdf_filename:
                    datastream.location = pdf_filename
                    datastream.save()
            elif not embargo:
                print(f"Error: unable to download {href}")

        else:
            print(f"No PDF link for {obj.pid}")

    def handle(self, *args, **options):
        colid = pid = options["pid"]
        source_id = options["source"]
        username = options["username"]
        remove_first_page = options["remove_first_page"]

        if "_" in pid:
            parts = colid.split("_")
            colid = parts[0]

            qs = Article.objects.filter(my_container__pid=pid).order_by("pid")
        else:
            qs = Article.objects.filter(
                Q(my_container__my_collection__pid=pid)
                | Q(my_container__my_collection__parent__pid=pid)
            )

            periode = Periode.objects.get(source__domain=source_id, collection__pid=colid)

            if periode.begin and periode.end:
                begin = periode.begin if periode.begin is not None else 0
                end = periode.end if periode.end is not None else 0
                periode_range = list(range(abs(begin), abs(end)))

                qs = qs.filter(my_container__year__in=periode_range)

            qs = qs.order_by("pid")

        try:
            with alive_bar(
                qs.count(),
                dual_line=True,
                title=f"Download PDF for {pid}",
                stats="(eta {eta})",
            ) as progress_bar:
                for article in qs:
                    progress_bar.text = article.pid
                    self.download_pdf(article, remove_first_page)
                    progress_bar()

            insert_crawl_event_in_history(colid, source_id, username, "OK", 0, pid, "download_pdf")

        except Exception as e:
            message = getattr(e, "message", "")
            message += "\n" + traceback.format_exc()
            insert_crawl_event_in_history(
                colid, source_id, username, "ERROR", 0, message, "download_pdf"
            )
            print(message)
