import asyncio
import csv
import shutil
from tempfile import NamedTemporaryFile

import aiohttp

from django.core.management.base import BaseCommand

"""
    Command to get ISSN and replace alls inside CSV files
"""


class Command(BaseCommand):
    help = "Get journals ISSN from CFP API"

    def add_arguments(self, parser):
        parser.add_argument(
            "-i",
            action="store",
            dest="input",
            type=str,
            default=None,
            required=True,
            help="path for input file",
        )
        parser.add_argument(
            "-o",
            action="store",
            dest="output",
            type=str,
            default=None,
            required=True,
            help="path for output file",
        )

    PERIODIQUES_SEARCH_URL = "https://cfp.mathdoc.fr/api/periodiques/?search="
    all_data_collections = {}

    def get_item(self, item, param):
        """
        function to get item in list of tuples
        @param item:
        @param param:
        @return:
        """
        elems = item[1]
        for index, elem in enumerate(elems):
            if param in elem:
                titre = elem[1]
                return (titre, index)

    # Asynchronous function to fetch data from a given URL using aiohttp
    async def fetch_data(self, session, url, data_collection):
        async with session.get(url) as response:
            # Return the JSON content of the response using 'response.json()'
            json_data = await response.json()
            if json_data:
                if "results" in json_data:
                    if len(json_data["results"]) > 0:
                        results = json_data["results"]
                        title_collection = data_collection[0]

                        if len(json_data["results"]) == 1:
                            index = 0

                        """
                            iterate results to find position of matched item
                        """
                        if len(json_data["results"]) > 1:
                            for index, item in enumerate(results):
                                title = results[index]["titre"]
                                if title == title_collection:
                                    break
                                else:
                                    continue
                        result_data = json_data["results"][index]
                        issn = result_data["edition_set"][0]["issn"]

                        pos_issn = self.get_item(data_collection, "ISSN_papier")[1]
                        data_collection[1].pop(pos_issn)
                        issn_elem = ("ISSN_papier", issn)
                        data_collection[1].append(issn_elem)

                        edition_len = len(result_data["edition_set"])
                        if edition_len > 1:
                            eissn = result_data["edition_set"][1]["issn"]
                            pos_eissn = self.get_item(data_collection, "ISSN_électronique")[1]
                            data_collection[1].pop(pos_eissn)
                            issn_elem = ("ISSN_électronique", eissn)
                            data_collection[1].append(issn_elem)

                        self.all_data_collections[title_collection] = data_collection[1]

            return json_data

    def add_all_issn(self, filename, output):
        tempfile = NamedTemporaryFile(mode="w", delete=False)
        with open(filename) as csvfile, tempfile:
            reader = csv.reader(csvfile)
            fieldnames = [
                "pid",
                "state",
                "ISSN_papier",
                "ISSN_électronique",
                "title",
                "type",
                "source_id",
                "url",
            ]
            writer = csv.DictWriter(tempfile, fieldnames=fieldnames)
            writer.writeheader()

            index_issn = 2
            index_eissn = 3
            for index, row in enumerate(reader):
                pid = row[0]
                if index > 0:
                    # iterate tuples parameters and get matched row
                    matched_col = [
                        col for col in self.all_data_collections.items() if col[1][0][1] == pid
                    ]
                    collection_item = dict()

                    if len(matched_col) > 0:
                        matched_col = matched_col[0]

                        pos_issn = self.get_item(matched_col, "ISSN_papier")
                        row[index_issn] = pos_issn[0]
                        pos_eissn = self.get_item(matched_col, "ISSN_électronique")
                        row[index_eissn] = pos_eissn[0]

                        for index, item in enumerate(row):
                            if index < 8:
                                collection_item[fieldnames[index]] = item

                    writer.writerow(collection_item)

        shutil.move(tempfile.name, output)

    async def main(self, filename, output):
        headers = {"accept": "application/json"}
        items = self.all_data_collections.items()

        async with aiohttp.ClientSession(headers=headers) as session:
            # Create a list of tasks, where each task is a call to 'fetch_data' with a specific URL
            tasks = [
                self.fetch_data(
                    session, self.PERIODIQUES_SEARCH_URL + self.get_item(item, "title")[0], item
                )
                for item in items
            ]

            results = await asyncio.gather(*tasks)
            if results:
                self.add_all_issn(filename, output)

    def get_all_issn(self, filename, output):
        """
        Reads a CSV file provided by the Documentation team and converts it to a dict with the journal PIDs as keys
        """

        with open(filename, encoding="utf-8", newline="") as csv_file:
            reader = csv.reader(csv_file, delimiter=",", quotechar='"', quoting=csv.QUOTE_MINIMAL)

            for index, row in enumerate(reader):
                if index == 0:
                    keys = row
                if index > 0:
                    title = row[4]
                    dict_obj = []
                    for index, elem in enumerate(keys):
                        if index < 8:
                            param = (elem, row[index])
                            dict_obj.append(param)

                    self.all_data_collections[title] = dict_obj

        asyncio.run(self.main(filename, output))

        return "success to fetch all ISSN"

    def handle(self, *args, **options):
        input = options["input"]
        output = options["output"]
        return self.get_all_issn(input, output)
