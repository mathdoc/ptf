from bs4 import BeautifulSoup
from crawler.base_crawler import BaseCollectionCrawler, add_pdf_link_to_xarticle

from ptf.model_data import create_articledata
from ptf.model_data import create_contributor
from ptf.model_data import create_issuedata
from ptf.model_data import create_subj


class ElibmCrawler(BaseCollectionCrawler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # TODO: creates a cols.csv that supersedes cols_eudml.csv with the entire collection catalogue.

        self.source_name = "Electronic Library of Mathematics"
        self.source_domain = "ELIBM"
        self.source_website = "https://www.elibm.org"
        self.source = self.get_or_create_source()

        if self.collection_id == "DOCMA":
            self.delimiter_inline_formula = "\\("
            self.delimiter_disp_formula = "\\["

    def parse_collection_content(self, content):
        """
        Parse the HTML page of Annals of Math and returns a list of xissue.
        Each xissue has its pid/volume/number/year metadata + its url

        self.periode is set at the end based on the xissue years of the HTML page
        """
        soup = BeautifulSoup(content, "html.parser")
        xissues = []

        # Extract the list of issues
        link_nodes = soup.find_all("a")

        # eLibM puts special issue titles as volume number
        # to create a issue pid, we use S1, S2...
        last_special_issue_number = 0

        for link_node in link_nodes:
            url = link_node.get("href")
            text = link_node.get_text()
            if url.startswith("/issue"):
                xissue, last_special_issue_number = self.create_xissue(
                    url, text, last_special_issue_number
                )

                # eLibM lists the special issues at the end.
                # set the periode_begin if we find a special issue
                if last_special_issue_number == 1:
                    self.periode_begin = self.get_first_year(xissues[-1].year)

                if xissue:
                    xissues.append(xissue)

        self.periode_end = self.get_first_year(xissues[0].year)

        if last_special_issue_number == 0:
            self.periode_begin = self.get_first_year(xissues[-1].year)

        self.periode = self.get_or_create_periode()

        return xissues

    def get_first_year(self, year):
        if "/" in year:
            year = year.split("/")[0]

        return year

    def create_xissue(self, url, text, last_special_issue_number):
        if "(" not in text or ")" not in text:
            return None

        parts = text.split("(")

        year = parts[1].split(")")[0]
        year = year.replace("/", "-")

        # volume might not be an integer. eLibM puts special issue titles as volume number.
        volume = parts[0].strip()

        number = ""
        if "No. " in volume:
            parts = volume.split("No. ")
            volume = parts[0].strip()
            number = parts[1].strip()

        try:
            volume_for_pid = int(volume)
        except ValueError:
            last_special_issue_number += 1
            volume_for_pid = f"S{last_special_issue_number}"

        xissue = create_issuedata()
        xissue.pid = f"{self.collection_id}_{year}__{volume_for_pid}_{number}"
        xissue.year = year
        xissue.volume = volume
        xissue.number = number
        xissue.url = self.source_website + url

        return xissue, last_special_issue_number

    def parse_issue_content(self, content, xissue):
        soup = BeautifulSoup(content, "html.parser")
        article_nodes = soup.find_all("div", {"class": "title"})

        for index_article, article_node in enumerate(article_nodes):
            article_link_node = article_node.find("a")
            if article_link_node:
                url = article_link_node.get("href")
                xarticle = create_articledata()
                xarticle.pid = "a" + str(index_article)
                xarticle.url = self.source_website + url

                # eLibM lists the articles in the reverse order, except for one special issue
                if xissue.volume == "Mahler Selecta":
                    xissue.articles.append(xarticle)
                else:
                    xissue.articles.insert(0, xarticle)

        # if the issue has only 1 article, eLibM skip the issue page and directly display the article page
        if len(xissue.articles) == 0:
            title_node = soup.find("h2", {"class": "document_title"})
            if title_node is not None:
                xarticle = create_articledata()
                xarticle.pid = "a0"
                xarticle.url = xissue.url

                xissue.articles.append(xarticle)

    def parse_article_content(self, content, xissue, xarticle, url, pid):
        """
        Parse the content with Beautifulsoup and returns an ArticleData
        """
        xarticle = create_articledata()
        xarticle.pid = pid
        xarticle.lang = "en"

        soup = BeautifulSoup(content, "html.parser")

        # TITLE
        title_node = soup.find("h2", {"class": "document_title"})
        if title_node:
            xarticle.title_tex = title_node.get_text()

        # AUTHORS
        citation_author_node = soup.find("h3", {"class": "document_author"})
        if citation_author_node:
            text = citation_author_node.get_text()
            if text:
                parts = text.split(";")
                for part in parts:
                    text_author = part.strip()

                    role = "author"
                    if "(ed.)" in text_author:
                        role = "editor"
                        text_author = text_author.split("(ed.)")[0].strip()

                    author = create_contributor()
                    author["role"] = role
                    author["string_name"] = text_author

                    xarticle.contributors.append(author)

        # PDF
        link_nodes = soup.find_all("a")
        for link_node in link_nodes:
            url = link_node.get("href")
            if url.startswith("/ft/"):
                pdf_url = self.source_website + url
                add_pdf_link_to_xarticle(xarticle, pdf_url)

        panel_nodes = soup.find_all("h3", {"class": "panel-title"})
        for panel_node in panel_nodes:
            text = panel_node.get_text()
            content_node = panel_node.parent.parent.find("div", {"class": "panel-body"})

            if text == "Summary":
                # ABSTRACT
                abstract = content_node.get_text()
                xabstract = {
                    "tag": "abstract",
                    "value_html": "",
                    "value_tex": abstract,
                    "value_xml": "",
                    "lang": "en",
                }
                xarticle.abstracts.append(xabstract)

            elif text == "Mathematics Subject Classification":
                # MSC
                subjs = content_node.get_text().split(", ")
                for subj in subjs:
                    subject = create_subj()
                    subject["value"] = subj
                    subject["type"] = "msc"
                    subject["lang"] = "en"
                    xarticle.kwds.append(subject)

            elif text == "Keywords/Phrases":
                # Keywords
                subjs = content_node.get_text().split(", ")
                for subj in subjs:
                    subject = create_subj()
                    subject["value"] = subj
                    subject["lang"] = "en"
                    xarticle.kwds.append(subject)

        # PAGES
        citation_node = soup.find("h5", {"class": "document_source"})
        if citation_node:
            text = citation_node.get_text()
            year = f"({xissue.year})"
            if year in text:
                text = text.split(year)[0]

                if "p." in text:
                    text = text.split("p.")[0].split(",")[-1].strip()
                    xarticle.size = text

                elif "-" in text:
                    parts = text.split("-")
                    first_page = parts[-2].split(" ")[-1]
                    last_page = parts[-1].split(",")[0].split(" ")[0]

                    xarticle.fpage = first_page
                    xarticle.lpage = last_page

        # DOI
        doi_node = citation_node.next_sibling
        if doi_node.name == "div":
            text = doi_node.get_text()
            if text.startswith("DOI: "):
                doi = text[5:]

                xarticle.doi = doi
                xarticle.pid = doi.replace("/", "_").replace(".", "_").replace("-", "_")

        return xarticle
