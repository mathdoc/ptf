import re

from bs4 import BeautifulSoup
from bs4 import Tag
from crawler.base_crawler import BaseCollectionCrawler
from crawler.base_crawler import add_pdf_link_to_xarticle
from crawler.types import CitationLiteral

from ptf.model_data import create_articledata
from ptf.model_data import create_issuedata
from ptf.model_data import create_subj


class DmlczCrawler(BaseCollectionCrawler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # TODO: creates a cols.csv that supersedes cols_eudml.csv with the entire collection catalogue.

        self.source_name = "Czech Digital Mathematics Library"
        self.source_domain = "DMLCZ"
        self.source_website = "https://dml.cz"
        self.source = self.get_or_create_source()

        self.issue_href = r"/handle/\d+.dmlcz/\d+"

    def parse_collection_content(self, content):
        """
        Parse the HTML page of Annals of Math and returns a list of xissue.
        Each xissue has its pid/volume/number/year metadata + its url

        self.periode is set at the end based on the xissue years of the HTML page
        """
        soup = BeautifulSoup(content, "html.parser")
        xissues = []

        issue_nodes = [volume for volume in soup.find_all("td", {"class": "volume"})]

        for issue_node in issue_nodes:
            reg_year = re.compile(r"\d{4}")
            reg_volume = re.compile(r"Volume \d+")
            issue_text = issue_node.get_text()
            if re.compile(r"\d+").search(issue_text):
                elem = issue_node.find("a")
                dates = reg_year.search(issue_text)
                volume = reg_volume.search(elem.get_text())
                issues = issue_node.findNext("td")
                issues = issues.findAll("a")
                if volume:
                    volume = volume[0].replace("Volume ", "")
                if dates:
                    search = reg_year.search(issue_text)
                    if search is not None:
                        dates = search[0]
                for issue in issues:
                    link = issue.get("href")
                    number = issue.get_text()
                    xissue = self.create_xissue(link, volume, number, dates)
                    if xissue:
                        xissues.append(xissue)

        self.periode_begin = self.get_year(xissues[0].year)
        self.periode_end = self.get_year(xissues[-1].year)
        self.periode = self.get_or_create_periode()

        return xissues

    def get_year(self, year):
        if "/" in year:
            year = year.split("/")[0]

        return year

    def create_xissue(self, url, volume, number, dates):
        year = dates.replace("/", "-")

        # volume might not be an integer. eLibM puts special issue titles as volume number.

        try:
            volume_for_pid = int(volume)
        except ValueError:
            print("error parsing volume")

        xissue = create_issuedata()
        number = number.replace(",", "-")
        xissue.pid = f"{self.collection_id}_{year}__{volume_for_pid}_{number}"
        xissue.year = year
        xissue.volume = volume
        xissue.number = number
        xissue.url = self.source_website + url

        return xissue

    def parse_issue_content(self, content, xissue):
        soup = BeautifulSoup(content, "html.parser")
        article_nodes = soup.find_all("td", {"class": "article"})

        # DML-CZ may list the same article multiple times (ex: https://dml.cz/handle/10338.dmlcz/149887)
        # We need to ignore the articles already crawled
        article_urls = []

        for index_article, article_node in enumerate(article_nodes):
            article_link_node = article_node.find("a")
            if article_link_node:
                url = article_link_node.get("href")
                if url not in article_urls:
                    article_urls.append(url)

                    xarticle = create_articledata()
                    xarticle.pid = "a" + str(index_article)
                    xarticle.url = self.source_website + url

                    xissue.articles.append(xarticle)

    def parse_article_content(self, content, xissue, xarticle, url, pid):
        """
        Parse the content with Beautifulsoup and returns an ArticleData
        """
        xarticle = create_articledata()
        xarticle.pid = pid
        xarticle.lang = "en"

        soup = BeautifulSoup(content, "html.parser")
        bloc_ref_ids = soup.find("div", {"class": "item-refids"})
        # TITLE
        title_node = soup.find("span", {"class": "item-title"})
        if title_node:
            xarticle.title_tex = title_node.get_text()

        # ABSTRACT
        abstract_section_node = soup.find("dim:field")
        if abstract_section_node:
            abstract = str(abstract_section_node.get_text())
            xabstract = {
                "tag": "abstract",
                "value_html": "",
                "value_tex": abstract,
                "value_xml": "",
                "lang": "en",
            }
            xarticle.abstracts.append(xabstract)

        # PDF
        link_nodes = soup.find_all("a")
        for link_node in link_nodes:
            pdf_url = link_node.get("href")
            if pdf_url.startswith("/bitstream/"):
                add_pdf_link_to_xarticle(xarticle, pdf_url)
        reg_msc = re.compile("/browse-subject")
        subjs_nodes = [a.get_text() for a in soup.find_all("a") if reg_msc.search(a.get("href"))]

        # MSC
        for subj in subjs_nodes:
            subject = create_subj()
            subject["value"] = subj
            subject["type"] = "msc"
            subject["lang"] = "en"
            xarticle.kwds.append(subject)

        # PAGES
        pages = soup.find("span", {"class": "item-pp"})
        if pages:
            pages_to = re.compile(r"(\(?\d+\)?)?-?(\(?\d+\)?)").search(pages.get_text())
            if pages_to:
                parts = pages_to[0].split("-")
                first_page = parts[0].replace("(", "").replace(")", "")
                if len(parts) > 1:
                    last_page = parts[1].replace("(", "").replace(")", "")
                    xarticle.lpage = last_page

                xarticle.fpage = first_page

        # Biblio
        # bibitems_tags = soup.select("div.references-inside div.reference")
        # bibitems = [self.parse_bibitem_tag(item) for item in bibitems_tags]
        # if len(bibitems) > 0:
        #     xarticle.abstracts.append(self.create_bibliography(bibitems))

        # DOI
        reg_doi = re.compile("dx.doi.org")

        what: list[CitationLiteral] = [
            "lang",
            "title",
            "author",
            "pdf",
            "abstract",
            "page",
            "mr",
            "zbl",
            "publisher",
            "keywords",
        ]
        self.get_metadata_using_citation_meta(xarticle, xissue, soup, what)

        if bloc_ref_ids and isinstance(bloc_ref_ids, Tag):
            doi_node = [a for a in bloc_ref_ids.find_all("a") if reg_doi.search(a.get("href"))]
            if len(doi_node) > 0:
                doi = doi_node[0].get_text()
                pos = doi.find("10.")
                if pos > 0:
                    doi = doi[pos:]
                xarticle.doi = doi

                # fix wrong doi attribution for article a14 of volume 62 number 1
                # 10.1007/s10587-012-0005-x:
                if xarticle.pid in ["CMJ_2012__62_1_a14", "ZCSUT_2012__22_3_a3"]:
                    xarticle.doi = None
                else:
                    xarticle.pid = (
                        doi.replace("/", "_").replace(".", "_").replace("-", "_").replace(":", "_")
                    )

        # Hack to handle articles with no titles
        if not xarticle.title_tex:
            xarticle.title_tex = " "

        return xarticle
