import re

from bs4 import BeautifulSoup
from crawler.base_crawler import BaseCollectionCrawler
from crawler.base_crawler import add_pdf_link_to_xarticle

from ptf.cmds.xml.jats.builder.issue import get_issue_title_xml
from ptf.model_data import AbstractDict
from ptf.model_data import create_articledata
from ptf.model_data import create_contributor
from ptf.model_data import create_issuedata
from ptf.model_data import create_subj


class TacCrawler(BaseCollectionCrawler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.source_name = "Theory and Applications of Categories website"
        self.source_domain = "TAC"
        self.source_website = "http://www.tac.mta.ca/tac"
        self.source = self.get_or_create_source()

        self.periode_begin = 1995
        self.periode_end = 2024
        self.periode = self.get_or_create_periode()

    def parse_collection_content(self, content):
        """
        Parse the HTML page of Annals of Math and returns a list of xissue.
        Each xissue has its volume/number/year metadata + its url

        self.periode is set during the parsing with the <meta name="citation_year"> of the HTML page
        """
        soup = BeautifulSoup(content, "html5lib")
        xissues = []

        issue_nodes = soup.find_all("h3")
        previous_year = 0

        for issue_node in issue_nodes:
            xissue, previous_year = self.create_xissue(issue_node, previous_year)
            xissues.append(xissue)

        # TAC has multiple links towards the same page (ie: title + abstract)
        # We want to add only 1 article so we keep track of the urls handled.
        urls = []

        # The TAC web page is badly formatted. <dt> have no closing </dt> tags.
        # To get the list of articles, we rely on the links and parse the URLs to find the corresponding volume
        link_nodes = soup.find_all("a")
        for link_node in link_nodes:
            url = link_node.get("href")
            if (
                url is not None
                and url.startswith("volumes/")
                and url.endswith(".html")
                and url not in urls
            ):
                urls.append(url)

                article_url = self.source_website + "/" + url
                url = url[8:]
                parts = url.split("/")
                volume = parts[0]

                if len(volume) == 4:
                    # The first volumes do have a url in /volumes/@vid/
                    # The url is /volumes/@year/@article_number/@volume-*.html
                    parts = parts[2].split("-")
                    if len(parts) > 1:
                        volume = parts[0]
                    else:
                        volume = ""
                elif len(parts) != 3:
                    # Ignore URLs that do not respect /volumes/@year/@article_number/@volume-*.html
                    volume = ""

                if volume:
                    xissue = [xissue for xissue in xissues if xissue.volume == volume][0]
                    article_index = len(xissue.articles)

                    xarticle = create_articledata()
                    xarticle.pid = "a" + str(article_index)
                    xarticle.url = article_url
                    xissue.articles.append(xarticle)

        return xissues

    def create_xissue(self, issue_node, previous_year):
        text = issue_node.get_text().strip()
        text = text[7:]  # Remove "Volume "
        parts = text.split(" - ")
        volume = parts[0]
        year = parts[1]
        title = ""

        # TAC has some special issues: the title is specified instead of the year
        try:
            year_int = int(year)
            previous_year = year_int
        except Exception:
            if year[-1] == "*":
                year = year[:-1]

            title = year
            if "Festschrift" in title:
                if title == "Bunge Festschrift":
                    year = "2024"
                else:
                    year = str(previous_year - 1)
            elif volume == "17":
                title = "Chu spaces"
                year = "2006"
            else:
                year = title[2:]

        xissue = create_issuedata()
        xissue.pid = self.collection_id + "_" + year + "__" + volume
        xissue.year = year
        xissue.volume = volume

        xissue.title_tex = title
        xissue.title_html = title
        xissue.title_xml = get_issue_title_xml(title, "en")

        return xissue, previous_year

    def parse_article_content(self, content, xissue, xarticle, url, pid):
        """
        Parse the content with Beautifulsoup and returns an ArticleData
        """
        xarticle = create_articledata()
        xarticle.pid = pid
        xarticle.lang = "en"

        soup = BeautifulSoup(content, "html5lib")

        # TITLE
        title_node = soup.find("h1")
        if title_node is not None:
            xarticle.title_tex = title_node.get_text().strip()

        # AUTHORS
        author_node = soup.find("h2")
        if author_node is not None:
            text = author_node.get_text().strip()
            parts = re.split(r",\s+and\s+|\s+and\s+|,\s+", text)

            if pid == "TAC_2018__33_a30":
                parts = ["J. Bhowmick", "S. Ghosh", "N. Rakshit", "M. Yamashita"]

            for text_author in parts:
                author = create_contributor()
                author["role"] = "author"
                author["string_name"] = text_author.replace("\n", " ")

                xarticle.contributors.append(author)

            # The first paragraphs (there can be many) before other metadata are part of the abstracts
            parsed_p_besides_abstract = False

            for node in author_node.find_next_siblings():
                if node.name == "p":
                    text = node.get_text().strip()

                    # KEYWORDS
                    parsed_text = self.insert_kwd(xarticle, "", text, "Keywords:")
                    parsed_text = parsed_text or self.insert_kwd(
                        xarticle, "msc", text, "2020 MSC:"
                    )
                    parsed_text = parsed_text or self.insert_kwd(
                        xarticle, "msc", text, "2010 MSC:"
                    )
                    parsed_text = parsed_text or self.insert_kwd(
                        xarticle, "msc", text, "2000 MSC:"
                    )
                    parsed_text = parsed_text or self.insert_kwd(
                        xarticle, "msc", text, "1991 MSC:"
                    )
                    parsed_text = parsed_text or self.insert_kwd(
                        xarticle, "msc", text, "AMS Classification (1991):"
                    )

                    # PAGES
                    title = "Theory and Applications of Categories"
                    if not parsed_text and text.startswith(title) and not xarticle.fpage:
                        parsed_text = True
                        pages = text[len(title) :].split("pp")[1][:-1].strip()
                        pages = pages.replace("--", "-")
                        parts = pages.split("-")
                        xarticle.fpage = parts[0]
                        xarticle.lpage = parts[1].split(".")[0]

                    # PUBLICATION DATE  (note: revised dates are ignored)
                    if not parsed_text and text.startswith("Published"):
                        parsed_text = True
                        date_str = text[10:].split(".")[0]
                        xarticle.date_published_iso_8601_date_str = date_str

                    parsed_p_besides_abstract = parsed_text or parsed_p_besides_abstract

                    # ABSTRACT
                    if not parsed_p_besides_abstract:
                        abstract = str(node)
                        if len(xarticle.abstracts) > 0:
                            xarticle.abstracts[0]["value_tex"] += abstract
                        else:
                            xabstract: AbstractDict = {
                                "tag": "abstract",
                                "value_html": "",
                                "value_tex": abstract,
                                "value_xml": "",
                                "lang": "en",
                            }
                            xarticle.abstracts.append(xabstract)

        # PDF
        # We need to find the last PDF link because TAC can have revised version of an article.
        # Ex: http://www.tac.mta.ca/tac/volumes/38/31/38-31abs.html
        pdf_url = ""
        link_nodes = soup.find_all("a")
        for link_node in link_nodes:
            url = link_node.get("href")
            if url is not None and url.endswith(".pdf"):
                pdf_url = url
        if pdf_url:
            add_pdf_link_to_xarticle(xarticle, pdf_url)

        return xarticle

    def insert_kwd(self, xarticle, content_type, text, prefix):
        if text.startswith(prefix):
            text = text[len(prefix) + 1 :]
            for kwd in re.split(",|;", text):
                subject = create_subj()
                subject["value"] = kwd.strip().replace("\n", " ")
                subject["type"] = content_type
                subject["lang"] = "en"
                xarticle.kwds.append(subject)
            return True
        return False
