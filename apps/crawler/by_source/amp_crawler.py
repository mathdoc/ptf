from bs4 import BeautifulSoup
from crawler.types import CitationLiteral
from crawler.base_crawler import BaseCollectionCrawler

from ptf.model_data import create_articledata
from ptf.model_data import create_issuedata


class AmpCrawler(BaseCollectionCrawler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # TODO: creates a cols.csv that supersedes cols_eudml.csv with the entire collection catalogue.
        # self.collection_id = "AM"
        # self.collection_url = "https://annals.math.princeton.edu"

        self.source_name = "Annals of Mathematics Princeton University"
        self.source_domain = "AMP"
        self.source_website = "https://annals.math.princeton.edu"
        self.source = self.get_or_create_source()

        self.periode_begin = 2003
        self.periode_end = 2017
        self.periode = self.get_or_create_periode()

    def parse_collection_content(self, content):
        """
        Parse the HTML page of Annals of Math and returns a list of xissue.
        Each xissue has its volume/number/year metadata + its url
        """
        soup = BeautifulSoup(content, "html.parser")
        xissues = []

        # Extract the list of issues
        issue_nodes = soup.find_all("div", {"class": "cat-item-2"})

        for issue_node in issue_nodes:
            issue_link_node = issue_node.find("a")
            if issue_link_node:
                url = issue_link_node.get("href")
                xissue = self.create_xissue(url)
                if xissue:
                    xissues.append(xissue)

        return xissues

    def create_xissue(self, url):
        if url.endswith("/"):
            url = url[:-1]
        parts = url.split("/")

        last_part = parts[-1]
        exceptions = last_part.split("-")
        if len(exceptions) > 2:
            year = exceptions[0]
            volume = exceptions[0]
            number = exceptions[1]
        else:
            year = parts[-2]
            if len(year) < 4:
                # The links are different with volumes before 2015
                year = parts[-3]

            volume_number = parts[-1]
            volume_number_parts = volume_number.split("-")
            volume = volume_number_parts[0]
            number = volume_number_parts[1]

        year_int = int(year)
        if self.periode_begin <= year_int <= self.periode_end:
            xissue = create_issuedata()
            xissue.pid = f"{self.collection_id}_{year}__{volume}_{number}"
            xissue.year = year
            xissue.volume = volume
            xissue.number = number
            xissue.url = url
        else:
            xissue = None

        return xissue

    def parse_issue_content(self, content, xissue):
        soup = BeautifulSoup(content, "html.parser")
        article_nodes = soup.find_all("h2", {"class": "entry-title"})

        for index_article, article_node in enumerate(article_nodes):
            article_link_node = article_node.find("a")
            if article_link_node:
                url = article_link_node.get("href")
                xarticle = create_articledata()
                xarticle.pid = "a" + str(index_article)
                xarticle.url = url
                xissue.articles.append(xarticle)

    def parse_article_content(self, content, xissue, xarticle, url, pid):
        """
        Parse the content with Beautifulsoup and returns an ArticleData
        """
        xarticle = create_articledata()
        xarticle.pid = pid
        xarticle.lang = "en"

        soup = BeautifulSoup(content, "html.parser")

        what: list[CitationLiteral] = ["author", "pdf", "abstract", "page"]

        title_node = soup.find("h1", {"class": "entry-title"})
        if title_node:
            what.append("title")

        if url != "https://annals.math.princeton.edu/2010/172-3/p06":
            # Exception with Annals of Math: 2 articles have the same DOI !
            # https://annals.math.princeton.edu/2010/172-3/p06 and https://annals.math.princeton.edu/2011/173-1/p14
            # we ignore DOI/ZBMATH/MR for the first one
            what.append("doi")

        self.get_metadata_using_citation_meta(xarticle, xissue, soup, what)

        # ZBMATH
        metadata_header_nodes = soup.find_all("div", {"class": "metadata-headers"})
        for metadata_header_node in metadata_header_nodes:
            text = metadata_header_node.get_text()

            if text == "zbMATH":
                link_node = metadata_header_node.parent.find("a")
                if link_node:
                    zblid = link_node.get("href")
                    pos = zblid.find("?q=an:")
                    if pos > 0:
                        zblid = zblid[pos + 6:]
                    xarticle.extids.append(("zbl-item-id", zblid))
            elif text == "MR":
                link_node = metadata_header_node.parent.find("a")
                if link_node:
                    mrid = link_node.get("href")
                    pos = mrid.find("?mr=")
                    if pos > 0:
                        mrid = mrid[pos + 4:]
                    xarticle.extids.append(("mr-item-id", mrid))

        return xarticle
