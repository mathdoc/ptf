from bs4 import BeautifulSoup
from crawler.base_crawler import BaseCollectionCrawler, add_pdf_link_to_xarticle

from ptf.external.arxiv import get_arxiv_article
from ptf.external.crossref import get_crossref_articles_in_journal
from ptf.model_data import create_issuedata


class DaCrawler(BaseCollectionCrawler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.source_name = "Discrete Analysis website"
        self.source_domain = "DA"
        self.source_website = "https://discreteanalysisjournal.com"
        self.source = self.get_or_create_source()

        self.periode_begin = 2016
        self.periode_end = 2024
        self.periode = self.get_or_create_periode()

    def parse_collection_content(self, content):
        """
        Discrete Analysis.
        We ignore the journal web page and query Crossref to get the list of articles.
        We query crossref for each article to get the list of xissues based on the publication date.
        Each xissue has its year + list of articles with their URLs
        """

        what = ["published", "year", "primary_url"]
        xarticles = get_crossref_articles_in_journal("2397-3129", what)

        xissues = []
        years = {}

        for xarticle in xarticles:
            year = str(xarticle.year)
            if year not in years:
                xissue = create_issuedata()
                xissue.pid = self.collection_id + "_" + year + "__"
                xissue.year = year

                years[year] = xissue
                xissues.append(xissue)
            else:
                xissue = years[year]

            xissue.articles.append(xarticle)

        return xissues

    def parse_article_content(self, content, xissue, xarticle, url, pid):
        """
        Parse the content with Beautifulsoup and returns an ArticleData
        """

        # We only parse the arXiv id in the Discrete Analysis article page
        soup = BeautifulSoup(content, "html.parser")

        metadata_node = soup.find("div", {"class": "article-metadata"})
        if metadata_node is None:
            return None

        a_node = metadata_node.find("a", {"class": "outline-alt button"})
        if a_node is None:
            return None

        href = a_node.get("href")
        id = href.split("/")[-1]

        new_xarticle = get_arxiv_article(id)
        if new_xarticle is None:
            return None

        new_xarticle.pid = xarticle.pid
        new_xarticle.doi = xarticle.doi
        new_xarticle.lang = "en"
        new_xarticle.date_published_iso_8601_date_str = xarticle.date_published_iso_8601_date_str

        add_pdf_link_to_xarticle(new_xarticle, new_xarticle.pdf_url)

        return new_xarticle
