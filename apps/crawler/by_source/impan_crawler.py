from bs4 import BeautifulSoup
from crawler.base_crawler import BaseCollectionCrawler
from crawler.base_crawler import add_pdf_link_to_xarticle
from crawler.types import CitationLiteral

from ptf.model_data import AbstractDict
from ptf.model_data import IssueData
from ptf.model_data import create_articledata
from ptf.model_data import create_issuedata


class ImpanCrawler(BaseCollectionCrawler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.source_name = "Institute of Mathematics Polish Academy of Sciences"
        self.source_domain = "IMPAN"
        self.source_website = "https://www.impan.pl"
        self.source = self.get_or_create_source()

        self.periode = self.get_or_create_periode()
        self.periode_end = 2016
        self.periode_begin = 0

    def parse_collection_content(self, content):
        """
        Discrete Analysis.
        We ignore the journal web page and query Crossref to get the list of articles.
        We query crossref for each article to get the list of xissues based on the publication date.
        Each xissue has its year + list of articles with their URLs
        """
        if self.periode.collection.pid == "APM":
            self.periode_begin = 1955

        if self.periode.collection.pid == "DIM":
            self.periode_begin = 2000

        soup = BeautifulSoup(content, "html.parser")
        xissues_dict: dict[str, IssueData] = {}

        # Extract the list of issues
        volume_nodes = soup.select("div.year")

        for volume_node in volume_nodes:
            year = volume_node.get_text()
            year_int = int(year)
            if self.periode_begin > year_int or year_int > self.periode_end:
                continue
            issues_nodes = volume_node.parent
            if issues_nodes is None:
                continue
            issues_nodes = issues_nodes.select("div.issues")

            for issue_node in issues_nodes:
                issues_link_node = issue_node.select("a")
                for issue_link_node in issues_link_node:
                    href = issue_link_node.get("href")
                    if href is None:
                        raise ValueError(
                            f"[{self.source_domain}] {self.collection_id} : Collection href is None"
                        )
                    if isinstance(href, list):
                        raise ValueError(
                            f"[{self.source_domain}] {self.collection_id} : Collection href is an array"
                        )
                    url = self.source_website + href

                    xissue = self.create_xissue(url, year)
                    # Prevent duplicate issues
                    # NOTE : is this needed ?
                    pid = xissue.pid
                    if pid is None:
                        continue
                    if pid in xissues_dict:
                        print(
                            f"[{self.source_domain}] {self.collection_id} : Duplicate issue in connection : {pid}"
                        )
                        continue
                    xissues_dict[pid] = xissue

        return xissues_dict.values()

    def create_xissue(self, url: str, year: str):
        if url.endswith("/"):
            url = url[:-1]
        parts = url.split("/")
        issue_number = parts[-1].replace(",", "-")
        volume_number = parts[-2]

        xissue = create_issuedata()
        if volume_number == "all":
            xissue.pid = f"{self.collection_id}_{year}__{issue_number}"
            xissue.volume = issue_number

        else:
            xissue.pid = f"{self.collection_id}_{year}__{volume_number}_{issue_number}"
            xissue.volume = volume_number

        xissue.year = year
        xissue.number = issue_number.replace(",", "-")
        xissue.url = url

        return xissue

    def parse_issue_content(self, content, xissue: IssueData):
        soup = BeautifulSoup(content, "html.parser")
        article_nodes = soup.select("div.info")
        for index_article, article_node in enumerate(article_nodes):
            xarticle = create_articledata()
            xarticle.pid = "a" + str(index_article)

            article_link_node = article_node.select_one("a")
            if article_link_node is None:
                raise ValueError(f"[{self.source_domain}] {xarticle.pid} : Issue link is None")
            href = article_link_node.get("href")
            if href is None:
                raise ValueError(f"[{self.source_domain}] {xarticle.pid} : Issue href is None")
            if isinstance(href, list):
                raise ValueError(f"[{self.source_domain}] {xarticle.pid} : Issue href is a list")
            xissue_url = xissue.url
            if xissue_url is None:
                raise ValueError(f"[{self.source_domain}] {xarticle.pid} : Issue url is None")
            xarticle.url = xissue_url + href

            xissue.articles.append(xarticle)

    def parse_article_content(self, content, xissue, xarticle, url, pid):
        """
        Parse the content with Beautifulsoup and returns an ArticleData
        """

        # We only parse the arXiv id in the Discrete Analysis article page
        soup = BeautifulSoup(content, "html.parser")

        title_info = soup.select_one("div.info")
        if title_info is None:
            raise ValueError(
                f"[{self.source_domain}] {self.collection_id} {xarticle.pid} : Title not found"
            )
        title_node = title_info.select_one("a")
        if title_node is None:
            title_node = soup.select_one("h2.product-title")
        if title_node is None:
            raise ValueError(
                f"[{self.source_domain}] {self.collection_id}  {xarticle.pid} : Title not found"
            )

        title_tex = title_node.get_text()
        xarticle.title_tex = title_tex

        what: list[CitationLiteral] = [
            "author",
            "pdf",
            "page",
            "doi",
            "issn",
            "publisher",
            "page",
            "keywords",
        ]
        self.get_metadata_using_citation_meta(xarticle, xissue, soup, what)
        # abstract

        abstract_mml_node = soup.select_one("div.details.abstract p")
        if abstract_mml_node is None:
            print(f"[{self.source_domain}] {xarticle.pid} : Abstract not found")
        else:
            abstract_tex = abstract_mml_node.get_text()
            xabstract: AbstractDict = {
                "tag": "abstract",
                "value_html": "",
                "value_tex": abstract_tex,
                "value_xml": "",
                "lang": "en",
            }
            xarticle.abstracts.append(xabstract)

        href_attrib = soup.select_one("div.order a")

        if href_attrib is not None:
            href = href_attrib.get("href")
            if isinstance(href, list):
                raise ValueError(f"[{self.source_domain}] {xarticle.pid} : Article href is a list")
            if href is None:
                raise ValueError(f"[{self.source_domain}] {xarticle.pid} : Article href not found")
            pdf_url = self.source_website + href
            add_pdf_link_to_xarticle(xarticle, pdf_url)
        if xarticle.title_tex is None or xarticle.title_tex == "":
            print(xarticle)
        return xarticle
