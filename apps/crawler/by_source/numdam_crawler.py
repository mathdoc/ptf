from crawler.base_crawler import BaseCollectionCrawler


class NumdamCrawler(BaseCollectionCrawler):
    """
    We do not crawl Numdam collections, but the NumdamCrawler is used to have access to
    get_or_create_source and get_or_create_period
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.source_name = "Numdam"
        self.source_domain = "NUMDAM"
        self.source_website = "http://www.numdam.org"
        self.source = self.get_or_create_source()

        self.periode_begin = 0
        self.periode_end = 0
        self.periode = self.get_or_create_periode()

    def crawl_collection(self, col_only=False):
        return []
