import re

from bs4 import BeautifulSoup
from bs4 import Tag
from crawler.base_crawler import BaseCollectionCrawler
from crawler.base_crawler import add_pdf_link_to_xarticle
from requests import Response

from ptf.model_data import AbstractDict
from ptf.model_data import ArticleData
from ptf.model_data import IssueData
from ptf.model_data import create_articledata
from ptf.model_data import create_contributor
from ptf.model_data import create_extlink
from ptf.model_data import create_issuedata
from ptf.model_data import create_subj


class SasaCrawler(BaseCollectionCrawler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.source_name = (
            "eLibrary of Mathematical Institute of the Serbian Academy of Sciences and Arts"
        )
        self.source_domain = "SASA"
        self.source_website = "http://elib.mi.sanu.ac.rs"
        self.source = self.get_or_create_source()

        self.periode = self.get_or_create_periode()
        self.periode_end = float("inf")
        self.periode_begin = 0

    def parse_collection_content(self, content):
        soup = BeautifulSoup(content, "html.parser")
        xissues: list[IssueData] = []

        # Extract the list of issues
        # Filter out empty table cells
        volume_nodes = [
            node for node in soup.select("td.issue_cell a.main_link") if node.text != "\xa0"
        ]
        for vol_node in volume_nodes:
            # NOTE : should we parse year here or in the issue itself ?
            href = vol_node.get("href")
            if isinstance(href, list):
                raise ValueError(
                    f"[{self.source_domain}] {self.collection_id} : Collection href is an array."
                )
            if href is None:
                raise ValueError(
                    f"[{self.source_domain}] {self.collection_id} : Collection href cannot be found"
                )

            # Parse Volume and Issue numbers
            url = self.source_website + "/pages/" + href
            # Formats like 44_1 / 2024 | Tom XIV / 2024 | Knj. 8 / 1960 | LXIX_1-2 / 2024
            volume_re = list(
                re.finditer(
                    r"(?P<volume>[a-zA-Z0-9 .-]+)(?:_(?P<issue>[\w-]+))? \/ (?P<year>\d+)",
                    vol_node.text,
                )
            )
            if len(volume_re) == 0:
                # Formats like 20(28) / 2022 | 44 (1) / 2024 | (N.S.) 115 (129) / 2024 |
                volume_re = list(
                    re.finditer(
                        r"(?P<volume>[\.\( \)\w]+)\((?P<issue>\d+)\) \/ (?P<year>\d+)",
                        vol_node.text,
                    )
                )
            if len(volume_re) == 0:
                raise IndexError(
                    f"[{self.source_domain}] {self.collection_id} : Volume cannot be parsed"
                )
            volume_metadata = volume_re[0].groupdict()
            year = int(volume_metadata["year"])
            if self.periode_begin <= year and year <= self.periode_end:
                xissues.append(
                    self.create_xissue(
                        url,
                        volume_metadata["year"],
                        volume_metadata["volume"].strip(),
                        volume_metadata.get("issue", None),
                    )
                )

        # Handle pagination
        pages_node = soup.select_one(".page_selector")
        if pages_node is None:
            return xissues
        next_page_node = pages_node.select_one(".page_selector_dead_link+a.page_selector_link")
        if next_page_node is None:
            return xissues
        next_page_href = next_page_node.get("href")
        if isinstance(next_page_href, list):
            raise ValueError(
                f"[{self.source_domain}] {self.collection_id} : Collection next page href is an array."
            )
        if next_page_href is None:
            raise ValueError(
                f"[{self.source_domain}] {self.collection_id} : Collection next page href cannot be found"
            )

        content = self.get_page_content(
            self.source_website + "/" + next_page_href,
            self.collection_id + "_" + next_page_node.text,
        )
        return xissues + self.parse_collection_content(content)

    def create_xissue(self, url: str, year: str, volume_number: str, issue_number="1"):
        if url.endswith("/"):
            url = url[:-1]
        xissue = create_issuedata()
        xissue.url = url

        # Replace any non-word character with an underscore
        xissue.pid = re.sub(
            r"[^a-zA-Z0-9-]+", "_", f"{self.collection_id}_{year}__{volume_number}"
        )
        xissue.volume = volume_number

        xissue.year = year
        if issue_number is not None:
            xissue.pid += f"_{issue_number}"
            xissue.number = issue_number.replace(",", "-")
        return xissue

    def parse_issue_content(self, content, xissue: IssueData, index: int = 0):
        soup = BeautifulSoup(content, "html.parser")
        article_nodes = soup.select(".content .result")
        if xissue.pid is None:
            raise ValueError(
                f"Error in crawler : {self.source_domain} : you must set an issue PID before parsing it"
            )

        # NOTE : publishers aren't implemented yet in base_crawler, but this should work for SASA.
        # issue_publisher_node = soup.select_one(".content>table td.text_cell span.data_text")
        # if (issue_publisher_node is not None):
        #     publisher = issue_publisher_node.text
        #     xpub = create_publisherdata()
        #     xpub.name = publisher.removeprefix("Publisher ")
        #     xissue.publisher = xpub

        for i, art_node in enumerate(article_nodes):
            xarticle = self.parse_sasa_article(i + index, art_node, xissue)
            xissue.articles.append(xarticle)

        index = index + len(article_nodes)
        # Handle pagination
        pages_node = soup.select_one(".page_selector")
        if pages_node is None:
            return
        next_page_node = pages_node.select_one(".page_selector_dead_link+a.page_selector_link")
        if next_page_node is None:
            return
        next_page_href = next_page_node.get("href")
        if isinstance(next_page_href, list):
            raise ValueError(
                f"[{self.source_domain}] {self.collection_id} : Issue next page href is an array."
            )
        if next_page_href is None:
            raise ValueError(
                f"[{self.source_domain}] {self.collection_id} : Issue next page href cannot be found"
            )

        content = self.get_page_content(
            self.source_website + "/" + next_page_href, xissue.pid + "_" + next_page_node.text
        )
        self.parse_issue_content(content, xissue, index)

    def parse_sasa_article(
        self, article_index: int, article_node: Tag, xissue: IssueData
    ) -> ArticleData:
        """
        Since Sasa doesn't have pages per articles, we parse the article data from the issue page instead
        """
        title_node = article_node.select_one(".main_link")
        if title_node is None:
            raise ValueError(f"[{self.source_domain}] {xissue.pid} : Title not found")
        href = title_node.get("href")
        if href is None or isinstance(href, list):
            raise ValueError(f"[{self.source_domain}] {xissue.pid} : Article href not found")

        xarticle = create_articledata()

        pages_node = article_node.select_one(".pages")
        if pages_node is not None:
            xarticle.page_range = pages_node.text
        xarticle.title_tex = title_node.text
        xarticle.title_html = title_node.text
        xarticle.pid = f"{xissue.pid}_a{article_index}"

        if xissue.url is not None:
            ext_link = create_extlink(
                rel="source", location=xissue.url, metadata=self.source_domain
            )
            xarticle.ext_links.append(ext_link)
            # xarticle.url = xissue.url

        author_node = article_node.select_one(".secondary_text")
        if author_node is not None:
            authors = re.findall(
                r"(?: and )?((?:(?<!,)(?<! and)[\w. -](?!and ))+)", author_node.text
            )
            for a in authors:
                author = create_contributor()
                author["role"] = "author"
                author["string_name"] = a
                xarticle.contributors.append(author)
        else:
            print(f"[{self.source_domain}] {xarticle.pid} : Author not found")

        abstract_node = article_node.select_one(".secondary_link")
        if abstract_node is None:
            print(f"[{self.source_domain}] {xarticle.pid} : Abstract not found")
        else:
            abstract_href = abstract_node.get("href")
            if abstract_href is None or isinstance(abstract_href, list):
                raise ValueError(
                    f"[{self.source_domain}] {xarticle.pid} : Abstract href not found"
                )

            abstract = self.fetch_sasa_abstract(
                self.source_website + "/" + abstract_href, xarticle.pid
            )
            if abstract is not None:
                xarticle.abstracts.append(abstract)

        secondary_nodes = article_node.select(".secondary_info_text")
        subjects = []
        keywords = []
        doi = None
        for node in secondary_nodes:
            text = node.text
            if text.startswith("Keywords"):
                keywords = text.removeprefix("Keywords:\xa0").split("; ")
            elif text.startswith("MSC"):
                subjects = text.removeprefix("MSC:\xa0").split("; ")
            elif text.startswith("DOI"):
                doi = text.removeprefix("DOI:\xa0")

                for kwd in keywords:
                    subject = create_subj()
                    subject["value"] = kwd
                    subject["lang"] = "en"
                    xarticle.kwds.append(subject)
            elif text.startswith("MSC"):
                subjects = text.removeprefix("MSC:\xa0").split("; ")
                for subj in subjects:
                    subject = create_subj()
                    subject["value"] = subj
                    subject["type"] = "msc"
                    subject["lang"] = "en"
                    xarticle.kwds.append(subject)
            elif text.startswith("DOI"):
                doi = text.removeprefix("DOI:\xa0")
                if doi is not None:
                    xarticle.doi = doi
                    xarticle.pid = doi.replace("/", "_").replace(".", "_").replace("-", "_")
            elif text.startswith("Zbl:"):
                zbl_link = node.select_one(".secondary_link")
                if zbl_link is not None:
                    xarticle.extids.append(("zbl-item-id", zbl_link.text))

        if href.startswith("http"):
            pdf_url = href
        else:
            pdf_url = self.source_website + "/files/" + href

        # Fix for Filomat
        if self.collection_id == "FLMT" and "www.pmf.ni.ac.rs" in pdf_url:
            pdf_url = pdf_url.replace("www.pmf.ni.ac.rs", "www1.pmf.ni.ac.rs")

        add_pdf_link_to_xarticle(xarticle, pdf_url)
        return xarticle

    def fetch_sasa_abstract(self, abstract_url: str, pid: str):
        content = self.get_page_content(abstract_url, pid)
        soup = BeautifulSoup(content, "html.parser")
        text_node = soup.select_one("p")
        if text_node is not None:
            text = text_node.text.replace("$$", "$")
            abstract: AbstractDict = {
                "tag": "abstract",
                "value_tex": text,
                "lang": "eng",
            }
            return abstract
        print(f"[{self.source_domain}] {pid} : Abstract page exists, but text not found")

    # NOTE : SASA abstracts are encoded in windows-1250 despite the header and meta tag advertising otherwise. Is it possible to handle this more elegantly ?
    # example : http://elib.mi.sanu.ac.rs/files/journals/bltn/26/1e.htm
    def decode_response(self, response: Response, encoding: str = "utf-8"):
        """Force windows-1250 encoding if we cannot cannot read the abstract"""
        try:
            return super().decode_response(response, encoding)
        except UnicodeDecodeError:
            print(
                f"[{self.source_domain}] cannot parse resource using {encoding} : {response.url}. Attempting windows-1250"
            )
        try:
            return super().decode_response(response, "windows-1250")
        except UnicodeDecodeError:
            raise BufferError(
                f"[{self.source_domain}] cannot parse resource using windows-1250 : {response.url}. Cannot read"
            )
