import re

import regex
from bs4 import BeautifulSoup
from bs4 import Tag
from crawler.base_crawler import BaseCollectionCrawler
from crawler.base_crawler import add_pdf_link_to_xarticle

from ptf.cmds.xml.jats.builder.citation import ContribAuthor
from ptf.cmds.xml.jats.builder.citation import get_all_authors_xml
from ptf.cmds.xml.jats.builder.citation import get_ext_link_xml
from ptf.cmds.xml.jats.builder.citation import get_publisher_xml
from ptf.cmds.xml.jats.builder.citation import get_source_xml
from ptf.cmds.xml.jats.builder.citation import get_volume_xml
from ptf.cmds.xml.jats.builder.citation import get_year_xml
from ptf.cmds.xml.jats.builder.issue import get_title_xml
from ptf.cmds.xml.xml_utils import escape
from ptf.model_data import AbstractDict
from ptf.model_data import create_articledata
from ptf.model_data import create_contributor
from ptf.model_data import create_issuedata


class BdimCrawler(BaseCollectionCrawler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # TODO: creates a cols.csv that supersedes cols_eudml.csv with the entire collection catalogue.

        self.source_name = "Biblioteca Digitale Italiana di Matematica"
        self.source_domain = "BDIM"
        self.source_website = "http://www.bdim.eu"
        self.source = self.get_or_create_source()

        self.issue_href = r"\?id=(?P<col>\w+)(?P<issue>_\d{1,4})"

    def parse_collection_content(self, content):
        """
        Parse the HTML page of Annals of Math and returns a list of xissue.
        Each xissue has its pid/volume/number/year metadata + its url

        self.periode is set at the end based on the xissue years of the HTML page
        """
        soup = BeautifulSoup(content, "html.parser")
        xissues = []

        reg_issue = regex.compile(self.issue_href)

        issue_nodes = [
            issue for issue in soup.find_all("a") if reg_issue.search(issue.get("href"))
        ]

        for issue_node in issue_nodes:
            # issue_text = issue_node.get_text()

            part_issue = issue_node.get("href").split("_")
            volume = part_issue[-2]
            number = part_issue[-1]
            year = part_issue[1]
            serie = part_issue[2]
            link = "/item" + issue_node.get("href")
            xissue = self.create_xissue(link, serie, volume, number, year)
            if xissue:
                xissues.append(xissue)

        self.periode_begin = self.get_year(xissues[0].year)
        self.periode_end = self.get_year(xissues[-1].year)

        self.periode = self.get_or_create_periode()

        return xissues

    def get_year(self, year):
        if "/" in year:
            year = year.split("/")[0]

        return year

    def create_xissue(self, url, serie, volume, number, dates):
        year = dates.replace("/", "-")

        xissue = create_issuedata()
        xissue.pid = f"{self.collection_id}_{year}_{serie}_{volume}_{number}"
        xissue.year = year
        xissue.volume = volume
        xissue.number = number
        xissue.vseries = serie
        xissue.url = self.source_website + url

        return xissue

    def parse_issue_content(self, content, xissue):
        soup = BeautifulSoup(content, "html.parser")
        article_nodes = soup.find_all("div", {"class": "referenza"})

        for index_article, article_node in enumerate(article_nodes):
            article_link_node = article_node.find("a", text="referenza completa")
            if article_link_node:
                url = article_link_node.get("href")
                xarticle = create_articledata()
                xarticle.pid = "a" + str(index_article)
                xarticle.url = self.source_website + url

                xissue.articles.append(xarticle)

    def parse_article_content(self, content, xissue, xarticle, url, pid):
        """
        Parse the content with Beautifulsoup and returns an ArticleData
        """
        xarticle = create_articledata()
        xarticle.pid = pid
        xarticle.lang = "it"

        soup = BeautifulSoup(content, "html.parser")
        # TITLE
        title_node = soup.find("span", {"class": "titolo"})
        if title_node:
            xarticle.title_tex = title_node.get_text()
            if xarticle.title_tex == "":
                xarticle.title_tex = " "

        reg_author_link = regex.compile(r"\?testo=\w+")
        text_author_bloc = soup.select_one("div.referenza p")
        if text_author_bloc:
            authors = [
                link
                for link in text_author_bloc.select("a")
                if reg_author_link.search(link.get("href"))
            ]
        if authors:
            for contrib in authors:
                role = "author"
                contrib_node = contrib.find("span", {"class": "autore"})
                if contrib_node is not None:
                    surname_node = contrib.find("span", {"class": "cognome"})
                    firstname_node = contrib.find("span", {"class": "nome"})
                    surname = ""
                    firstname = ""
                    author = create_contributor()
                    author["role"] = role

                    if surname_node is not None:
                        surname = surname_node.get_text()
                        author["last_name"] = surname

                    if firstname_node is not None:
                        firstname = firstname_node.get_text()
                        author["first_name"] = firstname

                    string_name = surname + ", " + firstname

                    if not string_name:
                        string_name = contrib_node.get_text()

                    author["string_name"] = string_name

                    xarticle.contributors.append(author)

        # ABSTRACT
        abstract_section_node = soup.find("div", {"class": "sunto"})
        if abstract_section_node:
            abstract = str(abstract_section_node.get_text())
            xabstract: AbstractDict = {
                "tag": "abstract",
                "value_html": "",
                "value_tex": abstract,
                "value_xml": "",
                "lang": "en",
            }
            xarticle.abstracts.append(xabstract)

        # PDF
        pdf_url = soup.find_all("a", text="pdf")
        if len(pdf_url) > 0:
            pdf_url = self.source_website + pdf_url[0].get("href")
            add_pdf_link_to_xarticle(xarticle, pdf_url)

        # PAGES
        pages = soup.find("span", {"class": "pagine"})
        if pages:
            pages_to = re.compile(r"(\(?\d+\)?)?-?(\(?\d+\)?)").search(pages.get_text())
            if pages_to:
                parts = pages_to[0].split("-")
                first_page = parts[0].replace("(", "").replace(")", "")
                if len(parts) > 1:
                    last_page = parts[1].replace("(", "").replace(")", "")
                    xarticle.lpage = last_page
                xarticle.fpage = first_page

        # Biblio
        bibitems_tags = soup.select("div.biblio div.bibitem")
        bibitems = [self.parse_bibitem_tag(item) for item in bibitems_tags]
        if len(bibitems) > 0:
            xarticle.abstracts.append(self.create_bibliography(bibitems))

        # metadata
        reg_zbl_id = re.compile(r"Zbl \w+")
        reg_mr_id = re.compile(r"MR \d+")

        medata_bloc = soup.select_one("div.referenza")
        if not medata_bloc:
            raise ValueError("metadata_bloc cannot be found")
        mr_id = [link for link in medata_bloc.find_all("a") if reg_mr_id.search(link.get_text())]
        zbl_id = [link for link in medata_bloc.find_all("a") if reg_zbl_id.search(link.get_text())]

        if len(zbl_id) > 0:
            zblid = zbl_id[0].get("href")
            pos = zblid.find("?q=an:")
            if pos > 0:
                zblid = zblid[pos + 6 :]
            xarticle.extids.append(("zbl-item-id", zblid))
        if len(mr_id) > 0:
            mr_id = mr_id[0].get_text()
            mr_id = mr_id.split("MR ")
            mr_id = mr_id[1]
            xarticle.extids.append(("mr-item-id", mr_id))

        if xarticle.title_tex == " ":
            title = " "
            if xarticle.pid == "AANL_1965_8_39_5_a17":
                title = "Eventi fasici nel midollo spinale quali prove di inibizione presinaptica durante il sonno desincronizzato"
            if xarticle.pid == "AANL_1973_8_55_6_a0":
                title = "Complementarity between nilpotent selfmappings and periodic autohomeomorphisms."
            if xarticle.pid == "AANL_1973_8_55_6_a2":
                title = "Sur une extension du lemme de Green."
            if xarticle.pid == "AANL_1979_8_67_1-2_a6":
                title = "On the existence o f an unbounded connected set of solutions fo r nonlinear equations in Banach spaces."
            if xarticle.pid == "AANL_1979_8_69_1-2_a6":
                title = "A note on a variational formulation of the Einstein equations for thermo-elastic materials."
            if xarticle.pid == "AANL_1972_8_52_2_a5":
                title = "Sul carattere proiettivo del rapporto plurisezionale."
            if xarticle.pid == "AANL_1980_8_69_1-2_a6":
                title = "A note on a variational formulation of the Einstein equations fo r thermo-elastic materials."
            xarticle.title_tex = title

        return xarticle

    def parse_bibitem_tag(self, item: Tag):
        value_xml = ""
        # First pass : we create an semi-complete XML Jats string, except for the authors
        # that we store inside authors_list to be serialized at the end
        authors_list: list[ContribAuthor] = []
        for c in item.children:
            c_text = escape(c.text)
            if isinstance(c, str):
                value_xml += c_text
                continue

            if not isinstance(c, Tag):
                raise NotImplementedError("bibitem_tag is not a Tag or a string")

            if c.name == "a":
                a_xml, is_badge = self.parse_a_tag(c)
                if is_badge:
                    value_xml = regex.sub(r" \| $", "", value_xml)
                value_xml += a_xml
                continue

            child_class = c.get("class")
            if not child_class:
                value_xml += c_text
            elif "bautore" in child_class:
                # TODO : parse firstname and lastname
                author_data, author_xml = self.parse_biblio_author_tag(c, len(authors_list))
                authors_list.append(author_data)
                value_xml += author_xml

            elif "titolo" in child_class:
                value_xml += get_title_xml(c_text)
            elif "rivista" in child_class:
                value_xml += get_source_xml(c_text)
            elif "anno" in child_class:
                value_xml += get_year_xml(c_text)
            elif "volume" in child_class:
                value_xml += get_volume_xml(c_text)
            elif "publisher" in child_class:
                value_xml += get_publisher_xml(c_text)
            else:
                # booktitle
                value_xml += c_text

        # In order to have a valid Jats xml, we have to group all authors into the person-group xml tag.
        authors_occurence = regex.compile(r"{author_\d}").findall(value_xml)
        if len(authors_occurence) > 0:
            first_author = value_xml.index(authors_occurence[0])
            last_author = value_xml.index(authors_occurence[-1]) + len(authors_occurence[-1])
            value_xml = (
                value_xml[:first_author]
                + get_all_authors_xml(value_xml[first_author:last_author], authors_list)
                + value_xml[last_author:]
            )

        return self.create_crawled_bibitem(value_xml, str(item))
        # return self.create_crawled_bibitem([*bib_elements, *bib_link_elements])

    def parse_a_tag(self, a_tag: Tag):
        a_text = escape(a_tag.text)
        href = a_tag.get("href")
        if not href:
            return a_text, False
        elif isinstance(href, list):
            raise ValueError("a tag has multiple href values !")
        else:
            a_type = "uri"
            if a_text.startswith("MR "):
                a_type = "mr-item-id"
                a_text = a_text.removeprefix("MR ")
            elif a_text.startswith("Zbl "):
                a_type = "zbl-item-id"
                a_text = a_text.removeprefix("Zbl ")
            return get_ext_link_xml(escape(href), a_text, a_type), a_type != "uri"

    def parse_biblio_author_tag(self, author_tag: Tag, index: int = 0):
        value_xml = ""
        author_data: ContribAuthor = {"template_str": ""}
        for c in author_tag.children:
            c_text = escape(c.text)
            if isinstance(c, str):
                author_data["template_str"] += c_text
                continue

            if not isinstance(c, Tag):
                raise NotImplementedError("author_tag is not a Tag or a string")
            # given name = cognome = prénom
            # surname = nome = nom de famille
            child_class = c.get("class")
            if not child_class:
                value_xml += c_text
            elif "cognome" in child_class:
                c.replace_with("{given_names}")
                author_data["given_names"] = c_text
                author_data["template_str"] += "{given_names}"
            elif "nome" in child_class:
                c.replace_with("{surname}")
                author_data["surname"] = c_text
                author_data["template_str"] += "{surname}"
        value_xml += "{author_" + str(index) + "}"

        return author_data, value_xml
