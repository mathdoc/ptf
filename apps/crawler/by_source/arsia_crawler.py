from crawler.by_source.da_crawler import DaCrawler

from ptf.external.datacite import get_datacite_articles_in_journal
from ptf.model_data import create_issuedata


class ArsiaCrawler(DaCrawler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.source_name = " Ars Inveniendi Analytica  website"
        self.source_domain = "ARSIA"
        self.source_website = "https://ars-inveniendi-analytica.com/"
        self.source = self.get_or_create_source()

        self.periode_begin = 2021
        self.periode_end = 2024
        self.periode = self.get_or_create_periode()

        # self.has_dynamic_article_pages = True

    def parse_collection_content(self, content):
        """
        Discrete Analysis.
        We ignore the journal web page and query Crossref to get the list of articles.
        We query crossref for each article to get the list of xissues based on the publication date.
        Each xissue has its year + list of articles with their URLs
        """

        what = ["published", "year", "primary_url"]
        xarticles = get_datacite_articles_in_journal("Ars Inveniendi Analytica", what)

        xissues = []
        years = {}

        for xarticle in xarticles:
            year = str(xarticle.year)
            if year not in years:
                xissue = create_issuedata()
                xissue.pid = self.collection_id + "_" + year + "__"
                xissue.year = year

                years[year] = xissue
                xissues.append(xissue)
            else:
                xissue = years[year]

            xissue.articles.append(xarticle)

        return xissues
