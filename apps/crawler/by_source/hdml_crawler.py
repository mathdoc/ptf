import re
from urllib.parse import unquote

import regex
from bs4 import BeautifulSoup
from crawler.base_crawler import BaseCollectionCrawler, add_pdf_link_to_xarticle

from ptf.model_data import create_articledata
from ptf.model_data import create_contributor
from ptf.model_data import create_issuedata


class HdmlCrawler(BaseCollectionCrawler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.source_name = "Hellenic Digital Mathematics Library"
        self.source_domain = "HDML"
        self.source_website = "https://hdml.di.ionio.gr"
        self.article_href = (
            r"(?P<base>en/item/Journals)/(\p{Greek}+|s|\s)+/(?P<volume>\d+)/(?P<num>\d+)"
        )
        self.pdf_href = "pdfs/journals"
        self.source = self.get_or_create_source()
        self.issue_href = r"(?P<number>((\d+)-?)(\d+)?)"
        # initialisation periode
        self.periode_begin = 0
        self.periode_end = 0
        self.periode = self.get_or_create_periode()

    def parse_collection_content(self, content):
        """
        Parse the HTML page of Annals of Math and returns a list of xissue.
        Each xissue has its volume/number/year metadata + its url

        self.periode is set during the parsing with the <meta name="citation_year"> of the HTML page
        """
        soup = BeautifulSoup(content, "html5lib")
        xissues = []

        # Extract the list of issues
        base_url_collection = self.collection_url.replace(self.source_website, "")
        base_url_collection = unquote(base_url_collection[1:])
        reg_issue = re.compile(base_url_collection + self.issue_href)

        issue_nodes = [a for a in soup.find_all("a") if reg_issue.search(str(a.get("href")))]

        for issue_node in issue_nodes:
            issue_node_link = self.source_website + "/" + issue_node.get("href")
            dates = issue_node.find_all("strong")[1].get_text()
            xissue = self.create_xissue(issue_node_link, dates)
            if xissue:
                xissues.append(xissue)

        return xissues

    def crawl_one_issue_url(self, xissue):
        xissue = super().crawl_one_issue_url(xissue)

        xissue.articles = sorted(
            xissue.articles, key=lambda x: (int(-1 if x.fpage == "" else x.fpage), int(x.lpage))
        )

        return xissue

    def create_xissue(self, url, dates):
        if url.endswith("/"):
            url = url[:-1]
        parts = url.split("/")

        volume = parts[-1]
        year = dates

        year_int = int(year.split("-")[:1][0])
        if self.periode_begin <= year_int:
            if self.periode_end == 0 or self.periode_begin <= self.periode_end:
                xissue = create_issuedata()
                xissue.pid = f"{self.collection_id}_{year}__{volume}"
                xissue.year = year
                xissue.volume = volume
                xissue.url = url
        else:
            xissue = None

        return xissue

    def parse_issue_content(self, content, xissue):
        # xissue = self.create_xissue(url)

        soup = BeautifulSoup(content, "html.parser")
        article_nodes = soup.find("div", {"id": "collectionResults"})
        for index_article, article_node in enumerate(article_nodes.find_all("a")):
            article_link_node = article_node.get("href")
            if article_link_node:
                url = article_node.get("href")
                xarticle = create_articledata()
                xarticle.pid = "a" + str(index_article)
                xarticle.url = self.source_website + "/" + url

                xissue.articles.append(xarticle)

        xissue.articles = sorted(
            xissue.articles, key=lambda x: int(-1 if x.fpage == "" else x.fpage)
        )

        return xissue

    def parse_article_content(self, content, xissue, xarticle, url, pid):
        """
        Parse the content with Beautifulsoup and returns an ArticleData
        """
        xarticle = create_articledata()
        xarticle.pid = pid
        xarticle.lang = "en"
        soup = BeautifulSoup(content, "html.parser")
        node_infos_em = soup.find_all("em")

        try:
            if node_infos_em:
                # TITLE
                title = node_infos_em[0].get_text()
                xarticle.title_tex = title
                xarticle.lang = "gr"

                # PAGES
                pages = node_infos_em[4].get_text()
                xarticle.page_range = pages
                pages_infos = pages.split("-")

                if len(pages_infos) > 1:
                    xarticle.fpage = pages_infos[0]
                else:
                    xarticle.fpage = pages
                if len(pages_infos) > 1:
                    xarticle.lpage = pages_infos[1]

        except Exception:
            pass

        # AUTHORS
        authors = soup.find("strong", text="Authors")
        if not isinstance(authors, type(None)):
            contribs = authors.find_next("em").get_text()
            contribs = contribs.split(",")

        else:
            author = soup.find("strong", text="Author")
            if not isinstance(author, type(None)):
                contribs = author.find_next("em").get_text()
                contribs = contribs.split(",")

        for contrib in contribs:
            author = create_contributor()
            author["role"] = "author"
            author["string_name"] = contrib.replace("\xa0", "")
            author["string_name"] = author["string_name"].replace(",", "").replace("by", "")
            xarticle.contributors.append(author)

        # PDF
        reg_pdf = regex.compile(self.source.pdf_href)
        pdf_link = [a.get("href") for a in soup.find_all("a") if reg_pdf.search(a.get("href"))][0]
        pdf_link = self.source_website + "/" + pdf_link
        add_pdf_link_to_xarticle(xarticle, pdf_link)

        return xarticle
