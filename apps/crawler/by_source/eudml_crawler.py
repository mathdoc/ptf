import re
from urllib.parse import urljoin

import regex
from bs4 import BeautifulSoup
from crawler.base_crawler import BaseCollectionCrawler, add_pdf_link_to_xarticle

from django.conf import settings

from ptf.model_data import create_articledata
from ptf.model_data import create_contributor
from ptf.model_data import create_extlink
from ptf.model_data import create_issuedata
from ptf.model_data import create_subj


class EudmlCrawler(BaseCollectionCrawler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.source_name = "European Digital Mathematics Library"
        self.source_domain = "EUDML"
        self.source_website = "https://eudml.org"
        self.source = self.get_or_create_source()

        self.has_dynamic_collection_pages = True

    def parse_collection_content(self, content):
        """
        Parse the HTML page of a EuDml journal and returns a list of xissue.
        Each xissue has a list of articles with just an url.

        self.periode is set during the parsing with the <meta name="citation_year"> of the HTML page
        """
        soup = BeautifulSoup(content, "html.parser")
        xissues = []

        citation_year_node = soup.find("meta", {"name": "citation_year"})
        if citation_year_node:
            value = citation_year_node.get("content")
            values = value.split("-")
            try:
                self.periode_begin = int(values[0])
                if len(values) > 1:
                    self.periode_end = int(values[1])
            except ValueError:
                pass
            self.periode = self.get_or_create_periode()

        # Extract the list of volumes
        volume_nodes = soup.find_all("li", {"class": "toggleable vol"})
        issue_count = 0
        volume_count = len(volume_nodes)

        for volume_node in volume_nodes:
            # missing_issues = []  # TODO. Unused ?
            span_volume = volume_node.find("span")

            volume_text = span_volume.findChildren("strong", recursive=False)

            # Extract the volume number
            volume_number = volume_text[0].get_text()
            regex_volume_prefix = regex.compile(r"vol\. \d+")
            if regex_volume_prefix.search(volume_number):
                volume_number = regex_volume_prefix.search(volume_number)[0]
                volume_number = volume_number.split(" ")
                volume_number = volume_number[1]

            if volume_number == "000":
                volume_number = "0"
            if volume_number[:2] == "00":
                volume_number = volume_number[2:]
            if volume_number[:1] == "0":
                volume_number = volume_number[1:]
            if re.compile("Special Series").search(volume_number):
                volume_number = "00"
            if re.compile(r"\d+-\d+").search(volume_number):
                volume_number = volume_number.split("-")
                volume_number = volume_number[0]
            if re.compile("Extra Vol").search(volume_number):
                volume_number = "00"

            # Extract the year
            year = None
            reg_year = re.compile(r"\d{4}")
            if len(volume_text) > 1:
                if reg_year.search(volume_text[1].get_text()):
                    year = reg_year.search(volume_text[1].get_text())[0]
            else:
                if reg_year.search(volume_text[0].get_text()):
                    year = reg_year.search(volume_text[0].get_text())[0]
            reg_strip_chars = re.compile(r"\/")

            if isinstance(year, str):
                if reg_strip_chars.search(year):
                    year = year.split("/")
                    year = year[0]

            year_str = year if year is not None else ""

            if re.compile("year").search(volume_number):
                regexp_year = regex.compile(r"\d{4}")
                year = regexp_year.search(volume_number)[0]
                year = "00"
            if year_str == "":
                year = "0"

            if re.compile(r"\d+\/\d+").match(year):
                year = year.split("/")
                year = year[0]
                year_str = str(year)

            issue_nodes = volume_node.find_all("li", {"class": "toggleable iss"})

            if len(issue_nodes) > 0 and year_str != "":
                # Extract all the issues
                for index_issue, issue_node in enumerate(issue_nodes):
                    xissue = self.create_xissue(issue_node, year_str, volume_number)
                    self.set_issue_number(issue_node, xissue, index_issue)
                    xissues.append(xissue)
                    issue_count += 1
            else:
                # No issues, articles are directly in the volumeF
                xissue = self.create_xissue(volume_node, year_str, volume_number)
                xissues.append(xissue)

        # EuDML stores the total of issues and articles in the <ul class="article-details unit unit-list">
        # This info is used to check the number of articles/issues parsed in the page
        volumes_to_find = 0
        issues_to_find = 0
        articles_to_find = 0
        article_details_nodes = soup.find_all("ul", {"class": "article-details unit unit-list"})
        for article_detail_node in article_details_nodes:
            unit_nodes = article_detail_node.find_all("li")
            for unit_node in unit_nodes:
                strong_node = unit_node.find("strong")
                if strong_node is not None:
                    text = strong_node.get_text()
                    if text == "Issue count:":
                        value = unit_node.get_text()[13:]
                        issues_to_find += int(value)
                    elif text == "Volume count:":
                        value = unit_node.get_text()[14:]
                        volumes_to_find += int(value)
                    elif text == "Number of articles:":
                        value = unit_node.get_text()[20:]
                        articles_to_find += int(value)

        if volume_count != volumes_to_find:
            txt = f"EuDML declares {volumes_to_find} volumes for {self.collection_id}. We parsed {volume_count}"
            print(txt)
            if settings.CRAWLER_LOG_FILE:
                with open(settings.CRAWLER_LOG_FILE, "a") as f_:
                    f_.write(txt + "\n")

        if issue_count != issues_to_find:
            txt = f"EuDML declares {issues_to_find} issues for {self.collection_id}. We parsed {issue_count}"
            print(txt)
            if settings.CRAWLER_LOG_FILE:
                with open(settings.CRAWLER_LOG_FILE, "a") as f_:
                    f_.write(txt + "\n")

        article_count = sum([len(xissue.articles) for xissue in xissues])
        if article_count != articles_to_find:
            txt = f"EuDML declares {articles_to_find} articles for {self.collection_id}. We parsed {article_count}"
            print(txt)
            if settings.CRAWLER_LOG_FILE:
                with open(settings.CRAWLER_LOG_FILE, "a") as f_:
                    f_.write(txt + "\n")

        return xissues

    def create_xissue(self, issue_node, year_str, volume_number):
        """
        EuDML does not have a separate HTML page for an issue.
        The list of issues/articles is directly found in the collection page.

        create_xissue creates an IssueData (see ptf/model_data.py) and sets its year/volume
        The PID is temporary and will be updated with the issue number (if any)
        create_xissue directly creates articles, but with just a pid and an url.
        """
        xissue = create_issuedata()
        xissue.pid = self.collection_id + "_" + year_str + "__" + volume_number
        xissue.year = year_str
        xissue.volume = volume_number

        # Extract the article urls
        article_nodes = issue_node.find_all("li", {"class": "art"})
        for index_article, article_node in enumerate(article_nodes):
            xarticle = create_articledata()
            xarticle.pid = "a" + str(index_article)
            article_link_node = article_node.find("a")
            xarticle.url = urljoin(self.source_website, article_link_node.get("href"))
            xissue.articles.append(xarticle)

        return xissue

    def set_issue_number(self, issue_node, xissue, index_issue):
        """
        EuDML does not have a separate HTML page for an issue.
        The list of issues/articles is directly found in the collection page.

        set_issue_number extracts the issue_number and updates xissue.
        The xissue pid is modified
        """

        issue_span = issue_node.find("span")

        # Extract the issue number
        issue_number = issue_span.find_next("strong").get_text()

        issue_number = issue_number.replace(",", "-").replace("/", "-")

        xissue.pid += "_" + issue_number
        xissue.number = issue_number

        return xissue

    def parse_article_content(self, content, xissue, xarticle, url, pid):
        """
        Parse the content with Beautifulsoup and returns an ArticleData
        """
        xarticle = create_articledata()
        xarticle.pid = pid
        soup = BeautifulSoup(content, "html.parser")

        what = [
            "lang",
            "title",
            "author",
            "pdf",
            "abstract",
            "page",
            "doi",
            "mr",
            "zbl",
            "publisher",
            "keywords",
        ]
        self.get_metadata_using_citation_meta(xarticle, xissue, soup, what)

        # LINK to SOURCE
        url_full_text_node = soup.find("a", text="Access to full text")
        if url_full_text_node is not None:
            url_full_text = url_full_text_node.get("href")
            ext_link = create_extlink()
            ext_link["rel"] = "primary-source"
            ext_link["location"] = url_full_text
            xarticle.ext_links.append(ext_link)

        # MSC KEYWORDS
        subj_part = soup.find("article", {"id": "unit-subject-areas"})
        if subj_part is not None:
            reg_msc = re.compile("/subject/MSC/[a-zA-Z0-9.]+")
            subjs = [a for a in subj_part.find_all("a") if reg_msc.search(a.get("href"))]
            for subj in subjs:
                type_class = subj.get("href").split("/")
                subject = create_subj()
                subject["value"] = type_class[3]
                subject["type"] = "msc"
                subject["lang"] = "en"
                xarticle.kwds.append(subject)

        # FALLBACK
        if not xarticle.title_tex:
            try:
                title = soup.find("h1").get_text(strip=True).replace("\xa0", " ")
                txt = f"{url} Fallback for title"
                print(txt)
                if settings.CRAWLER_LOG_FILE:
                    with open(settings.CRAWLER_LOG_FILE, "a") as f_:
                        f_.write(txt + "\n")
                xarticle.title_tex = title.replace("\xa0", " ").replace("\n", "")
            except:
                pass

        if len(xarticle.contributors) == 0:
            # AUTHORS
            authors_bloc = soup.find("p", {"class": "sub-title-1"})
            if authors_bloc:
                authors_node = authors_bloc.find_all("a")
                if len(authors_node) > 0:
                    txt = f"{url} Fallback for authors"
                    print(txt)
                    if settings.CRAWLER_LOG_FILE:
                        with open(settings.CRAWLER_LOG_FILE, "a") as f_:
                            f_.write(txt + "\n")
                for author_node in authors_node:
                    text_author = author_node.get_text()
                    text_author = text_author.replace(",", "")

                    author = create_contributor()
                    author["role"] = "author"
                    author["string_name"] = text_author

                    xarticle.contributors.append(author)

        if len(xarticle.streams) == 0:
            # PDF
            pdf_node = soup.find("a", text="Full (PDF)")
            if pdf_node is not None:
                pdf_url = pdf_node.get("href")
                if pdf_url:
                    add_pdf_link_to_xarticle(xarticle, pdf_url)

        if len(xarticle.abstracts) == 0:
            # ABSTRACT
            abstract_node = soup.find("article", {"id": "unit-article-abstract"})
            if abstract_node is not None:
                abstract_section_node = abstract_node.find("section")
                if abstract_section_node:
                    abstract = str(abstract_section_node)
                    xabstract = {
                        "tag": "abstract",
                        "value_html": "",
                        "value_tex": abstract,
                        "value_xml": "",
                        "lang": "en",
                    }
                    xarticle.abstracts.append(xabstract)

        if len(xarticle.contributors) == 0 or not xarticle.fpage:
            # LANG, PAGES, (AUTHORS)
            # EuDML has an export BibTex section with some information (lang, pages, authors)
            self.parse_bibtex(soup, xarticle, url)

        if xarticle.doi is None:
            # DOI
            doi_link = soup.find("article", {"id": "unit-other-ids"})
            if doi_link is not None:
                # Simplify ?
                # See https://eudml.org/doc/54683 with http://dx.doi.org/10.1155/2007/10368%E2%80%89
                try:
                    reg_doi = re.compile("doi.org")
                    doi_array = [
                        d.get("href")
                        for d in doi_link.find_all("a")
                        if reg_doi.search(str(d.get("href")))
                    ]
                    if doi_array:
                        if len(doi_array) > 1:
                            start_dois = len(doi_array) - 1
                            doi = doi_array[start_dois:][0]
                        else:
                            doi = doi_array[0]

                        doi_array = doi.split("doi.org/")
                        # strip unwanted chars present
                        if len(doi_array) > 1:
                            doi = doi_array[1].encode("ascii", "ignore")
                            doi = str(doi.decode())
                            doi_array = doi.split("\\u")
                            doi = str(doi_array[0])

                            doi = re.sub("}", "", doi)
                            doi = re.sub("\t", "", doi)
                            doi = doi.encode("ascii", "ignore")
                            doi = doi.decode()

                        doi = bytes(r"{}".format(r"" + doi + ""), "utf-8")
                        doi = doi.decode()
                        doi_array = doi.split("\\u")
                        doi = str(doi_array[0]).strip()
                        doi = doi.replace(" ", "")

                        xarticle.doi = doi
                except TypeError as e:
                    print(e)

        # You can't get the first link to zbmath.org: it could be in the list of references !

        # has_zblid = len([extid for extid in xarticle.extids if extid[0] == "zbl-item-id"]) == 1
        # if not has_zblid:
        #     # ZBL
        #     zblid_link = soup.find(
        #         "a", {"href": re.compile(r"http:\/\/www.zentralblatt-math.org\/zmath\/")}
        #     )
        #     if zblid_link is not None:
        #         zblid = zblid_link.get("href").split("?q=")[1]
        #         if zblid:
        #             print(f"{url} Fallback for zbl-id: {zblid}")
        #             xarticle.extids.append(("zbl-item-id", zblid))

        # In Other Databases is not (always ?) the publisher
        # if not xissue.publisher:
        #     # PUBLISHER
        #     section_oai = soup.find("h3", text="In Other Databases")
        #     if section_oai is not None:
        #         section_oai_array = section_oai.parent.find_all("dd")
        #         if section_oai is not None:
        #             pub = [
        #                 d.text
        #                 for d in section_oai_array
        #                 if d.text.strip() not in ["DOI", "ZBMath", "MathSciNet", "PUBLISHER"]
        #             ]
        #             if pub != "":
        #                 print(f"{url} Fallback for publisher")
        #                 xpub = create_publisherdata()
        #                 xpub.name = pub[0].strip()
        #                 xissue.publisher = xpub

        # ARTICLE PID
        if xarticle.doi is not None:
            xarticle.pid = doi.replace("/", "_").replace(".", "_").replace("-", "_")
            xarticle.pid = xarticle.pid.replace("pid", "").replace(":", "_")
        else:
            reg_article = regex.compile(r"\d+")
            if not isinstance(xarticle.pid, type(None)):
                pid_array = reg_article.findall(url)
                if len(pid_array) > 0:
                    id_article = pid_array[0]
                    xarticle.pid = xissue.pid + "_" + id_article

        return xarticle

    def parse_bibtex(self, soup, xarticle, url):
        """
        Parse the BibTeX section of a EuDML article page.
        Extract
        - the authors (if no author was already found in the page)
        - the article language
        - the article pages
        """
        bib_div = [p for p in soup.find_all("p") if "@article" in p.text]

        if len(bib_div) > 0:
            bib_tex = bib_div[0].get_text()
            text = bib_tex.split("\t")

            for text_part in text:
                # AUTHORS (only if no authors were already found in the page)
                if len(xarticle.contributors) == 0:
                    reg_author = re.compile("author =")
                    if reg_author.search(text_part):
                        txt = f"{url} Fallback for authors with the bibtex"
                        print(txt)
                        if settings.CRAWLER_LOG_FILE:
                            with open(settings.CRAWLER_LOG_FILE, "a") as f_:
                                f_.write(txt + "\n")

                        authors_text = (
                            text_part.replace("{", "").replace("}", "").replace("author = ", "")
                        )
                        authors_bib = authors_text.split(",")
                        for index, name in enumerate(authors_bib):
                            if index % 2 == 1:
                                author_name = authors_bib[index - 1] + " " + authors_bib[index]
                                author_name = self.latext_parser.latex_to_text(author_name)
                                author_name = author_name.replace("\xa0", "")

                                author = create_contributor()
                                author["role"] = "author"
                                author["string_name"] = author_name
                                xarticle.contributors.append(author)

                # LANG
                reg_lang = re.compile("language = ")
                if reg_lang.search(text_part):
                    xarticle.lang = (
                        text_part.replace("{", "")
                        .replace("}", "")
                        .replace("language = ", "")
                        .replace(",", "")
                    )
                    if len(xarticle.lang) >= 3:
                        xarticle.lang = xarticle.lang[:-1]

                    if len(xarticle.lang) > 0 and len(xarticle.abstracts) > 0:
                        xarticle.abstracts[0]["lang"] = xarticle.lang

                if not xarticle.fpage:
                    # PAGES
                    reg_pages = re.compile("pages =")
                    if reg_pages.search(text_part):
                        pages = (
                            text_part.replace("{", "")
                            .replace("}", "")
                            .replace("(", "")
                            .replace(")", "")
                            .replace("[", "")
                            .replace("]", "")
                            .replace("pages = ", "")
                        )
                        if len(pages) > 0 and pages != "null":
                            pages = pages.split(",")
                            if re.compile(r"\d+-\d+").search(pages[0]):
                                txt = f"{url} Fallback for pages with the bibtex"
                                print(txt)
                                if settings.CRAWLER_LOG_FILE:
                                    with open(settings.CRAWLER_LOG_FILE, "a") as f_:
                                        f_.write(txt + "\n")

                                pages = pages[0].split("-")
                                xarticle.fpage = pages[0]
                                if len(pages) > 1:
                                    reg_digit = re.compile(r"\d+")
                                    if re.search(reg_digit, str(pages[1])):
                                        pages[1] = re.search(reg_digit, str(pages[1]))[0]
                                    xarticle.lpage = pages[1]
                                xarticle.page_range = pages[0] + "-" + pages[1]

                # reg_title = re.compile("title")
                # if reg_title.search(text_part):
                #     if (
                #         xarticle.title_html is None
                #         or xarticle.title_html == ""
                #         or xarticle.title_html == "Contents"
                #     ):
                #         xarticle.title_html = (
                #             text_part.replace("{", "")
                #             .replace("}", "")
                #             .replace("title = ", "")
                #             .replace(",", "")
                #         )
                #         xarticle.title_tex = xarticle.title_html
                #         xarticle.title_xml = f"<title-group><article-title>{xarticle.title_html}</article-title></title-group>"
