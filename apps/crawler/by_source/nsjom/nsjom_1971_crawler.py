import re
import typing
from datetime import datetime

from bs4 import BeautifulSoup
from bs4 import Tag
from crawler.base_crawler import add_pdf_link_to_xarticle
from crawler.utils import cleanup_str

from ptf.model_data import IssueData
from ptf.model_data import create_articledata
from ptf.model_data import create_contributor
from ptf.model_data import create_extlink
from ptf.model_data import create_issuedata

if typing.TYPE_CHECKING:
    from ..nsjom_crawler import NsjomCrawler


source_domain = "NSJOM"


def parse_collection_content(
    self: "NsjomCrawler",
    content: str,
    periode_start: int = 0,
    periode_end: int = datetime.now().year,
    source_domain: str = "NSJOM",
):
    """
    Parses all articles from year-specific webpages : https://sites.dmi.uns.ac.rs/nsjom/ns1971.html
    From 1971 to 2009 (included)
    """
    xissues: list[IssueData] = []
    year_start = max(1971, periode_start)
    year_end = min(2009, periode_end)

    for year in range(year_start, year_end + 1):
        url = f"https://sites.dmi.uns.ac.rs/nsjom/ns{year}.html"
        year_content = self.get_page_content(url, f"NSJOM_{year}")
        try:
            xissues = xissues + parse_year(self, year_content, year, url)
        except ValueError as e:
            # Adds message to printed error
            e.add_note(f"[{source_domain}]: {year}")
            raise
    return xissues


def parse_issue_content(self, content: str, xissue: IssueData):
    parse_year(self, content, int(xissue.year), xissue.url, xissue.pid)


def parse_year(
    self: "NsjomCrawler",
    content: str,
    year: int,
    url: str | None = None,
    pid_to_parse: str | None = None,
):
    """Parses one page.
    eg : https://sites.dmi.uns.ac.rs/nsjom/ns2009.html"""
    soup = BeautifulSoup(content, "html.parser")
    xissues: list[IssueData] = []
    issues_tags = soup.select("body>table")
    for issue_tag in issues_tags:
        xissue = parse_issue_tag(self, issue_tag, year)
        if not xissue:
            continue
        xissue.url = url
        xissues.append(xissue)
    return xissues


def parse_issue_tag(
    self: "NsjomCrawler", issue_tag: Tag, year: int, pid_to_parse: str | None = None
):
    """Parses one issue tag.
    eg: `document.querySelector('body>table')` in https://sites.dmi.uns.ac.rs/nsjom/ns2009.html
    """
    xissue = create_issuedata()
    xissue.year = str(year)
    ext_link = create_extlink(
        rel="source",
        location=f"https://sites.dmi.uns.ac.rs/nsjom/issue.html?year={year}",
        metadata=source_domain,
    )
    xissue.ext_links.append(ext_link)

    table_lines = issue_tag.select("tr")
    issue_title_tag = table_lines.pop(0)
    match = re.search(
        r"[\n\r ]*NSJOM[\n\r ]*Vol (?P<Volume>\d+)\.(?: No\. (?P<Issue>\d+))?",
        issue_title_tag.text,
    )
    # Issue Summary ?
    if match is None:
        raise ValueError("Cannot find volume number")
    volume_number = match.group("Volume")
    issue_number = match.group("Issue")
    if volume_number:
        xissue.volume = volume_number
    else:
        raise ValueError("Cannot read volume number")
    if issue_number:
        xissue.number = issue_number
    xissue.pid = f"{source_domain}_{year}_{volume_number}_{issue_number}"

    if pid_to_parse and xissue.pid != pid_to_parse:
        return
    table_lines.pop(0)  # table header
    for index, table_line_tag in enumerate(table_lines):
        try:
            xarticle = parse_article_tag(
                self,
                table_line_tag,
                xissue.pid,
                index,
                f"https://sites.dmi.uns.ac.rs/nsjom/issue.html?year={year}",
            )
        except ValueError as e:
            e.add_note(f"{volume_number}_{issue_number}")
            raise
        if xarticle:
            xissue.articles.append(xarticle)
    return xissue


def parse_article_tag(
    self: "NsjomCrawler", article_tag: Tag, issue_pid: str, index: int, source: str | None = None
):
    """Parses one article tag.
    eg: `document.querySelector("body>table tr:nth-child(3)")` in https://sites.dmi.uns.ac.rs/nsjom/ns2009.html
    """
    xarticle = create_articledata()
    if source:
        ext_link = create_extlink(rel="source", location=source, metadata=self.source_domain)
        xarticle.ext_links.append(ext_link)

    article_data = article_tag.select("td")
    if len(article_data) != 3:
        raise ValueError("Issue table doesn't have three columns")
    author_tag, title_tag, pages_tag = article_data

    authors = [
        BeautifulSoup(_, "html.parser").text.strip() for _ in str(author_tag).split("<br/>")
    ]
    for author_name in authors:
        if author_name == "":
            continue

        author_name = cleanup_str(author_name)

        author = create_contributor(role="author", string_name=author_name)
        xarticle.contributors.append(author)

    title = cleanup_str(title_tag.text)
    xarticle.title_tex = title
    xarticle.pid = f"{issue_pid}a_{index}"

    title_link_tag = title_tag.select_one("a")
    if title_link_tag is None:
        print(f"[{source_domain}] {issue_pid}_{index} : Cannot find article pdf link")
        return None
    pdf_link = title_link_tag.get("href")
    if pdf_link is None:
        raise ValueError("Article pdf link is None")
    if isinstance(pdf_link, list):
        raise ValueError("Article has multiple pdf hrefs")
    pdf_link = self.source_website + pdf_link
    add_pdf_link_to_xarticle(xarticle, pdf_link)

    xarticle.page_range = cleanup_str(pages_tag.text)
    return xarticle
