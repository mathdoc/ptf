import re
import typing

from bs4 import BeautifulSoup
from bs4 import Tag
from crawler.base_crawler import add_pdf_link_to_xarticle

from ptf.model_data import IssueData
from ptf.model_data import create_articledata
from ptf.model_data import create_contributor
from ptf.model_data import create_extlink
from ptf.model_data import create_issuedata
from ptf.model_data import create_publisherdata
from ptf.model_data import create_subj

if typing.TYPE_CHECKING:
    from ..nsjom_crawler import NsjomCrawler

source_domain = "NSJOM"


def parse_collection_content(
    self: "NsjomCrawler",
    _: str,
    periode_start: int = 0,
    periode_end: float = float("inf"),
    source_domain: str = "NSJOM",
    xissue_pid_to_parse: str | None = None,
):
    """
    Parses all articles from one xml file : https://sites.dmi.uns.ac.rs/nsjom/NSJOM.xml
    From 2015 to today
    """
    xissues: dict[tuple[str, str], IssueData] = {}
    url = "https://sites.dmi.uns.ac.rs/nsjom/NSJOM.xml"
    content = self.get_page_content(url, "NSJOM_xml")
    soup = BeautifulSoup(content, "lxml-xml")
    record_container_element = soup.select_one("records")
    if record_container_element is None:
        raise ValueError(f"[{source_domain}] Cannot parse source")
    for record_element in record_container_element.select("record"):
        publication_type_tag = record_element.select_one("publicationType")
        if publication_type_tag is None:
            raise ValueError(f"[{source_domain}] Cannot determine article publicationType")
        if publication_type_tag.text != "published":
            continue
        year_tag = record_element.select_one("year")
        if year_tag is None or year_tag.text == "":
            raise ValueError(f"[{source_domain}] Cannot parse year from article")
        year = int(year_tag.text)
        if periode_start > year or year > periode_end:
            continue
        xarticle, volume_number, issue_number = parse_article(
            record_element, source_domain=source_domain
        )
        if (volume_number, issue_number) not in xissues:
            pid = f"{source_domain}_{year}__{volume_number}_{issue_number}"
            if xissue_pid_to_parse and xissue_pid_to_parse != pid:
                continue
            xissue = create_issuedata()
            parse_issue_tag(xissue, record_element, year)
            xissue.year = year_tag.text
            xissue.volume = volume_number
            xissue.number = issue_number
            xissue.pid = pid
            xissues[(volume_number, issue_number)] = xissue
        xissues[(volume_number, issue_number)].articles.append(xarticle)

    return list(xissues.values())


def parse_issue_content(self, content: str, xissue: IssueData):
    return parse_collection_content(
        self, content, int(xissue.year), int(xissue.year), source_domain, xissue.pid
    )


def parse_issue_tag(xissue: IssueData, article_tag: Tag, year: int) -> IssueData:
    publisher_tag = article_tag.select_one("publisher")
    if publisher_tag:
        xpub = create_publisherdata()
        xpub.name = publisher_tag.text
        xissue.publisher = xpub

    ext_link = create_extlink(
        rel="source",
        location=f"https://sites.dmi.uns.ac.rs/nsjom/issue.html?year={year}",
        metadata=source_domain,
    )
    xissue.ext_links.append(ext_link)
    return xissue


def parse_article(article_tag: Tag, source_domain: str = "NSJOM"):
    xarticle = create_articledata()

    doi_tag = article_tag.select_one("doi")
    if doi_tag is None:
        raise ValueError(f"[{source_domain}] : Article doi not found")
    xarticle.doi = doi_tag.text
    xarticle.pid = re.sub("\\/\\.-", "_", doi_tag.text)

    page_start_tag = article_tag.select_one("startPage")
    page_end_tag = article_tag.select_one("endPage")
    if page_start_tag and page_end_tag:
        xarticle.page_range = page_start_tag.text + " - " + page_end_tag.text

    date_published_tag = article_tag.select_one("publicationDate")
    if date_published_tag:
        xarticle.date_published_iso_8601_date_str = date_published_tag.text

    url_tag = article_tag.select_one("publisherRecordId")
    if url_tag:
        ext_link = create_extlink(
            rel="source",
            location=f"https://sites.dmi.uns.ac.rs/nsjom/paper.html?noid={url_tag.text}",
            metadata=source_domain,
        )
        xarticle.ext_links.append(ext_link)

    title_tag = article_tag.select_one("title")
    if title_tag:
        xarticle.title_tex = title_tag.text

    # TODO : Affiliations ?

    authors_container = article_tag.select_one("authors")
    if authors_container:
        for author_tag in authors_container.select("author"):
            author = create_contributor(role="author")
            author_name_tag = author_tag.select_one("name")
            if author_name_tag:
                author["string_name"] = author_name_tag.text
            corresponding = author_tag.get("corresponding")
            if corresponding == "1":
                author["corresponding"] = True
            email_tag = author_tag.select_one("email")
            if email_tag:
                author["email"] = email_tag.text
            xarticle.contributors.append(author)

    abstract_tag = article_tag.select_one("abstract")
    if abstract_tag:
        abstract_language = abstract_tag.get("langauge", "eng")
        if abstract_language is None or isinstance(abstract_language, list):
            abstract_language = "eng"
        xarticle.abstracts.append(
            {"tag": "abstract", "value_tex": abstract_tag.text, "lang": abstract_language}
        )

    keywords_tag = article_tag.select_one("keywords")
    if keywords_tag:
        keywords_language = keywords_tag.get("language", "eng")
        if keywords_language is None or isinstance(keywords_language, list):
            keywords_language = "eng"
        for kwd_tag in keywords_tag.select("keyword"):
            subject = create_subj()
            subject["value"] = kwd_tag.text
            subject["lang"] = "en"
            xarticle.kwds.append(subject)

    msc_tag = article_tag.select_one("MSCs")
    if msc_tag:
        for msc_subj in msc_tag.select("MSC"):
            subject = create_subj()
            subject["value"] = msc_subj.text
            subject["type"] = "msc"
            subject["lang"] = "en"
            xarticle.kwds.append(subject)

    pdf_location_tag = article_tag.select_one("filelocation")
    pdf_name_tag = article_tag.select_one("file")
    if pdf_location_tag and pdf_name_tag:
        pdf_url = "https://sites.dmi.uns.ac.rs/nsjom/" + pdf_location_tag.text + pdf_name_tag.text
        add_pdf_link_to_xarticle(xarticle, pdf_url)

    volume_tag = article_tag.select_one("volume")
    issue_tag = article_tag.select_one("issue")
    if volume_tag is None or issue_tag is None:
        raise ValueError(
            f"[{source_domain}] {xarticle.doi} Cannot parse volume or issue from article"
        )

    # Citations ?

    return xarticle, volume_tag.text, issue_tag.text
