import re
import typing
from datetime import datetime

from bs4 import BeautifulSoup
from bs4 import Tag
from crawler.base_crawler import add_pdf_link_to_xarticle
from crawler.utils import cleanup_str

from ptf.model_data import IssueData
from ptf.model_data import create_articledata
from ptf.model_data import create_contributor
from ptf.model_data import create_extlink
from ptf.model_data import create_issuedata

if typing.TYPE_CHECKING:
    from ..nsjom_crawler import NsjomCrawler

source_domain = "NSJOM"


def parse_collection_content(
    self: "NsjomCrawler",
    content: str,
    periode_start: int = 0,
    periode_end: int = datetime.now().year,
    source_domain: str = "NSJOM",
):
    """
    Parses all articles from year-specific webpages : https://sites.dmi.uns.ac.rs/nsjom/ns2010.html
    From 2010 to 2014 (included)
    """
    xissues: list[IssueData] = []
    year_start = max(2010, periode_start)
    year_end = min(2014, periode_end)

    for year in range(year_start, year_end + 1):
        url = f"https://sites.dmi.uns.ac.rs/nsjom/ns{year}.html"
        year_content = self.get_page_content(url, f"NSJOM_{year}")
        xissues = xissues + parse_year(self, year_content, year, url)

    return xissues


def is_heading(element: Tag):
    if element.select_one(".HeadingNSJOM"):
        return True
    classname = element.get("class")
    if not classname:
        return False

    if isinstance(classname, str):
        if classname == "HeadingNSJOM":
            return True
        return False

    if "HeadingNSJOM" in classname:
        return True
    return False


def parse_issue_content(self, content: str, xissue: IssueData):
    parse_year(self, content, int(xissue.year), xissue.url, xissue.pid)


def parse_year(
    self: "NsjomCrawler",
    content: str,
    year: int,
    url: str | None = None,
    pid_to_crawl: str | None = None,
):
    soup = BeautifulSoup(content, "html.parser")
    page_elements = soup.select("p.HeadingNSJOM, p.style1, p.style1+blockquote a")
    issues: list[list[Tag]] = []
    # Sort tags into issues
    for current_element in page_elements:
        if is_heading(current_element):
            issues.append([current_element])
            continue
        if current_element.text == "\xa0":
            continue
        issues[-1].append(current_element)

    xissues: list[IssueData] = []
    for issue_elements in issues:
        xissue = parse_issue_tags(self, issue_elements, year, pid_to_crawl)
        if not xissue:
            continue
        xissue.url = url
        xissues.append(xissue)

    return xissues


def parse_issue_tags(
    self: "NsjomCrawler", tags: list[Tag], year: int, pid_to_crawl: str | None = None
):
    xissue = create_issuedata()
    xissue.year = str(year)
    ext_link = create_extlink(
        rel="source",
        location=f"https://sites.dmi.uns.ac.rs/nsjom/issue.html?year={year}",
        metadata=source_domain,
    )
    xissue.ext_links.append(ext_link)
    issue_title_tag = tags.pop(0)
    match = re.search(
        r"NSJOM Vol\. (?P<Volume>\d+)(?:, No. (?P<Issue>\d+))?", issue_title_tag.text
    )
    if match is None:
        raise ValueError("Cannot find volume number")
    volume_number = match.group("Volume")
    issue_number = match.group("Issue")
    if volume_number:
        xissue.volume = volume_number
    else:
        raise ValueError("Cannot read volume number")
    if issue_number:
        xissue.number = issue_number
    xissue.pid = f"{source_domain}_{year}_{volume_number}_{issue_number}"

    if pid_to_crawl and xissue.pid != pid_to_crawl:
        return

    while len(tags) > 0:
        article_meta = tags.pop(0)
        article_meta_text = cleanup_str(article_meta.text)
        if article_meta_text == "":
            continue

        article_title = tags.pop(0)
        article = parse_article(
            self,
            article_meta_text,
            article_title,
            xissue.pid,
            len(xissue.articles),
            f"https://sites.dmi.uns.ac.rs/nsjom/issue.html?year={year}",
        )
        if article is None:
            continue
        xissue.articles.append(article)
    return xissue


def parse_article(
    self: "NsjomCrawler",
    meta_text: str,
    title_tag: Tag,
    issue_pid: str,
    index: int,
    source: str | None = None,
):
    xarticle = create_articledata()
    if source:
        ext_link = create_extlink(rel="source", location=source, metadata=self.source_domain)
        xarticle.ext_links.append(ext_link)
    match = re.search(r"\d+ \/ \d+ \/ \d+ \/ (?P<Pages>\d+(?:-\d+)?): (?P<Authors>.+)", meta_text)
    if match is None:
        raise ValueError("Cannot parse authors or page number")
    xarticle.page_range = match.group("Pages")
    authors = re.findall(r"(?: and )?((?:(?<!,)(?<! and).(?!and ))+)", match.group("Authors"))
    for a in authors:
        author = create_contributor(role="author", string_name=a)
        xarticle.contributors.append(author)

    article_pdf_link = title_tag.get("href")
    if article_pdf_link is None:
        print("[NSJOM] article does not have a pdf")
        return None
    if isinstance(article_pdf_link, list):
        raise ValueError("Article link is a list")
    pdf_link = self.source_website + article_pdf_link
    add_pdf_link_to_xarticle(xarticle, pdf_link)

    title = cleanup_str(title_tag.text)
    xarticle.title_tex = title
    xarticle.pid = f"{issue_pid}a_{index}"

    return xarticle
