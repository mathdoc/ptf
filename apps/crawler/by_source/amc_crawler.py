from bs4 import BeautifulSoup, Tag
from crawler.base_crawler import BaseCollectionCrawler, add_pdf_link_to_xarticle

from ptf.model_data import create_articledata
from ptf.model_data import create_contributor
from ptf.model_data import create_issuedata
from ptf.model_data import create_subj


class AmcCrawler(BaseCollectionCrawler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # TODO: creates a cols.csv that supersedes cols_eudml.csv with the entire collection catalogue.
        # self.collection_id = "AM"
        # self.collection_url = "https://annals.math.princeton.edu"

        self.source_name = "Ars Mathematica Contemporanea website"
        self.source_domain = "AMC"
        self.source_website = "https://amc-journal.eu"
        self.source = self.get_or_create_source()

        self.periode_begin = 2009
        self.periode_end = 2024
        self.periode = self.get_or_create_periode()

    def parse_collection_content(self, content):
        """
        Parse the HTML page of Ars Mathematica Contemporanea and returns a list of xissue.
        Each xissue has its volume/number/year metadata + its url
        This web site has multiple pages for its issues. so we need to crawl all of them
        """
        xissues = []
        self.parse_one_issues_page(content, xissues)

        url = self.collection_url + "/2"
        content = self.download_file(url, has_dynamic_pages=False)
        self.parse_one_issues_page(content, xissues)

        return xissues

    def parse_one_issues_page(self, content, xissues):
        soup = BeautifulSoup(content, "html.parser")

        # Extract the list of issues
        issue_nodes = soup.find_all("h2")

        for issue_node in issue_nodes:
            issue_link_node = issue_node.find("a")
            if issue_link_node:
                url = issue_link_node.get("href")
                text = issue_link_node.get_text().strip()
                if text.find("Vol.") == 0:
                    text = text[5:]
                    parts = text.split("No.")
                    volume = parts[0].strip()
                    parts = parts[1].split("(")
                    number = parts[0].strip()
                    year = parts[1][0:4]

                    xissue = create_issuedata()
                    xissue.pid = f"{self.collection_id}_{year}__{volume}_{number}"
                    xissue.year = year
                    xissue.volume = volume
                    xissue.number = number
                    xissue.url = url

                    xissues.append(xissue)

    def parse_issue_content(self, content, xissue):
        soup = BeautifulSoup(content, "html.parser")
        article_nodes = soup.find_all("h3", {"class": "title"})

        for index_article, article_node in enumerate(article_nodes):
            article_link_node = article_node.find("a")
            if article_link_node:
                url = article_link_node.get("href")
                xarticle = create_articledata()
                xarticle.pid = "a" + str(index_article)
                xarticle.url = url

                meta_node = article_node.find_next_sibling("div")
                if meta_node:
                    pages_node = meta_node.find("div", {"class": "pages"})
                    if pages_node is not None:
                        text = pages_node.get_text()

                        if "," in text and "pp" in text:
                            parts = text.split(",")
                            number_parts = parts[0].split(".")
                            if len(number_parts) == 2:
                                xarticle.article_number = number_parts[1].strip()

                            text = parts[1].split("pp")[0].strip()
                            xarticle.counts.append(("page-count", text))
                        elif "-" in text:
                            parts = text.split("-")
                            xarticle.fpage = parts[0].strip()
                            xarticle.lpage = parts[1].strip()

                xissue.articles.append(xarticle)

    def parse_article_content(self, content, xissue, xarticle, url, pid):
        """
        Parse the content with Beautifulsoup and returns an ArticleData
        """
        xarticle.pid = pid
        xarticle.lang = "en"

        soup = BeautifulSoup(content, "html.parser")

        # TITLE
        title_node = soup.find("h1", {"class": "page_title"})
        if title_node:
            xarticle.title_tex = title_node.get_text()

        # AUTHORS
        authors_node = soup.find("ul", {"class": "authors"})
        if authors_node and isinstance(authors_node, Tag):
            span_nodes = authors_node.find_all("span", {"class": "name"})
            for span_node in span_nodes:
                text = span_node.get_text().strip()

                author = create_contributor(role="author", string_name=text)

                xarticle.contributors.append(author)

        # DOI
        doi_node = soup.find("section", {"class": "item doi"})
        if doi_node:
            doi_node = doi_node.find("a")
            if doi_node and isinstance(doi_node, Tag):
                url = doi_node.get("href")
                if (isinstance(url, str)):
                    pos = url.find("10.")
                    if pos > 0:
                        doi = url[pos:]
                        xarticle.doi = doi
                        xarticle.pid = doi.replace("/", "_").replace(".", "_").replace("-", "_")

        # KEYWORDS
        kwds_node = soup.find("section", {"class": "item keywords"})
        if kwds_node:
            span_node = kwds_node.find("span", {"class": "value"})
            if span_node and not isinstance(span_node, int):
                text = span_node.get_text().strip()
                for kwd in text.split(", "):
                    subject = create_subj()
                    subject["value"] = kwd
                    subject["lang"] = "en"
                    xarticle.kwds.append(subject)

        # ABSTRACT
        abstract_node = soup.find("section", {"class": "item abstract"})
        if abstract_node:
            text = abstract_node.get_text().strip()
            if text.find("Abstract") == 0:
                text = text[9:]
                xarticle.abstracts.append(
                    {
                        "tag": "abstract",
                        "value_html": "",
                        "value_tex": text,
                        "value_xml": "",
                        "lang": "en",
                    })

        # PDF
        pdf_node = soup.find("a", {"class": "obj_galley_link pdf"})
        if pdf_node and isinstance(pdf_node, Tag):
            pdf_url = pdf_node.get("href")
            if (isinstance(pdf_url, list)):
                raise ValueError("pdf_url is a list")
            if pdf_url is None:
                raise ValueError("pdf_url not found")
            add_pdf_link_to_xarticle(xarticle, pdf_url)

        return xarticle
