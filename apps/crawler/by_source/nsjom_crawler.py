from datetime import datetime

from crawler.base_crawler import BaseCollectionCrawler
from requests import Response

from ptf.model_data import IssueData

from .nsjom.nsjom_1971_crawler import parse_collection_content as parse_1971
from .nsjom.nsjom_1971_crawler import parse_issue_content as parse_issue_1971
from .nsjom.nsjom_2010_crawler import parse_collection_content as parse_2010
from .nsjom.nsjom_2010_crawler import parse_issue_content as parse_issue_2010
from .nsjom.nsjom_xml_crawler import parse_collection_content as parse_xml
from .nsjom.nsjom_xml_crawler import parse_issue_content as parse_issue_xml

collection_crawlers = (
    parse_1971,
    parse_2010,
    parse_xml,
)


class NsjomCrawler(BaseCollectionCrawler):
    """NSJOM has multiple source layouts for articles depending of the publication year.
    - 1971 to 2009: one HTML page per year https://sites.dmi.uns.ac.rs/nsjom/ns1971.html
    - 2010 to 2014: one HTML page per year https://sites.dmi.uns.ac.rs/nsjom/ns2010.html
    - 2015 to today : an XML file: https://sites.dmi.uns.ac.rs/nsjom/NSJOM.xml

    We have to implement different crawlers for each layout.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.source_name = "Novi sad journal of mathematics (NSJOM)"
        self.source_domain = "NSJOM"
        self.source_website = "https://sites.dmi.uns.ac.rs/nsjom/"
        self.source = self.get_or_create_source()

        self.periode = self.get_or_create_periode()
        self.periode_begin = 0
        self.periode_end = datetime.now().year

    def parse_collection_content(self, content):
        """
        Parses all articles from one xml file : https://sites.dmi.uns.ac.rs/nsjom/NSJOM.xml
        """
        xissues: list[IssueData] = []
        for crwlr in collection_crawlers:
            xissues = xissues + crwlr(
                self, content, self.periode_begin, self.periode_end, self.source_domain
            )
        return xissues

    def parse_issue_content(self, content: str, xissue: IssueData):
        if int(xissue.year) >= 2015:
            parse_issue_xml(self, content, xissue)
        elif int(xissue.year) >= 2010:
            parse_issue_2010(self, content, xissue)
        else:
            parse_issue_1971(self, content, xissue)

    def decode_response(self, response: Response, encoding: str = "utf-8"):
        """Force windows-1250 encoding if we cannot cannot read the abstract"""
        try:
            return super().decode_response(response, encoding)
        except UnicodeDecodeError:
            print(
                f"[{self.source_domain}] cannot parse resource using {encoding} : {response.url}. Attempting windows-1252"
            )
        try:
            return super().decode_response(response, "windows-1252")
        except UnicodeDecodeError:
            print(
                f"[{self.source_domain}] cannot parse resource using windows-1252 : {response.url}. Attempting windows-1250"
            )
        try:
            return super().decode_response(response, "windows-1250")
        except UnicodeDecodeError:
            raise BufferError(
                f"[{self.source_domain}] cannot parse resource using windows-1250 : {response.url}. Cannot read"
            )
