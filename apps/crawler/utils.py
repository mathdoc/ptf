import csv
import json
import os
import re
import unicodedata

import requests
from crawler.types import JSONCol

from django.conf import settings
from django.contrib.auth.models import User

from history.views import insert_history_event
from ptf import model_helpers
from ptf.cmds import ptf_cmds
from ptf.display import resolver
from ptf.exceptions import ResourceDoesNotExist
from ptf.model_data import create_publicationdata
from ptf.models import Collection

# from ptf.models import ResourceId


def insert_crawl_event_in_history(
    colid, source_name, username, status, tasks_count, message, event_type="import"
):
    collection = model_helpers.get_collection(colid)
    if collection is None:
        UserWarning(f"Collection {colid} cannot be found inside model_helpers")
        return
    user = User.objects.get(username=username)

    event_data = {
        "type": event_type,
        "pid": f"{colid}-{source_name}",
        "col": colid,
        "source": source_name,
        "status": status,
        "title": collection.title_html,
        "userid": user.id,
        "type_error": "",
        "data": {
            "ids_count": tasks_count,
            "message": message,
            "target": "",
        },
    }

    insert_history_event(event_data)


def get_cached_html_folder(collection_id, source_id, container_id=None, article_id=None):
    """
    Web crawling (collections, issues, articles) save the downloaded files on disk in settings.HTML_ROOT_FOLDER
    The cache is used in case of a second attempt to get the HTML content.
    """

    folder = resolver.get_relative_folder(collection_id, container_id, article_id)

    if container_id is None and article_id is None:
        folder = os.path.join(settings.HTML_ROOT_FOLDER, folder, "html", source_id)
    else:
        folder = os.path.join(settings.HTML_ROOT_FOLDER, folder, "html")

    return folder


def get_cached_html_file_name(collection_id, source_id, container_id=None, article_id=None):
    """
    Web crawling (collections, issues, articles) save the downloaded files on disk in settings.HTML_ROOT_FOLDER
    The cache is used in case of a second attempt to get the HTML content.
    """

    folder = get_cached_html_folder(collection_id, source_id, container_id, article_id)

    if article_id is not None:
        basename = f"{article_id}.html"
    elif container_id is not None:
        basename = f"{container_id}.html"
    else:
        basename = f"{collection_id}.html"

    filename = os.path.join(folder, basename)

    return filename


def col_has_source(col: JSONCol, filter: str):
    return any(source for source in col["sources"] if source == filter)


def get_cols_by_source(source: str) -> list[JSONCol]:
    """
    Get all cols by source
    @param source: str
    @return: list of collections
    """
    data = get_all_cols()

    return [col for col in data.values() if col_has_source(col, source)]


def get_all_cols() -> dict[str, JSONCol]:
    with open(
        os.path.dirname(os.path.abspath(__file__)) + "/data/all_cols.json", encoding="utf8"
    ) as data_collections:
        return json.load(data_collections)


def read_json(path):
    with open(path, encoding="utf8") as data_collections:
        return json.load(data_collections)


def get_eudml_collections():
    """
    Reads a CSV file provided by the Documentation team and converts it to a dict with the journal PIDs as keys
    """

    file_name = os.path.join(
        os.path.dirname(os.path.dirname(os.path.abspath(__file__))),
        "crawl/mathcollector/cols_eudml.csv",
    )

    eudml_collections = {}
    with open(file_name, encoding="utf8") as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=",")

        for row in csv_reader:
            if row["pid"] not in NUMDAM_COLLECTIONS:
                eudml_collections[row["pid"]] = row

    return eudml_collections


def get_elibm_collections():
    """
    TODO: remove this function when we have all_cols.csv
    """

    return {
        "DOCMA": {
            "pid": "DOCMA",
            "title": "Documenta Mathematica",
            "url": "https://www.elibm.org/DM",
        },
        "EEJLA": {
            "pid": "EEJLA",
            "title": "ELA. The Electronic Journal of Linear Algebra",
            "url": "https://www.elibm.org/series?q=se:2229",
        },
        "EETNA": {
            "pid": "EETNA",
            "title": "ETNA. Electronic Transactions on Numerical Analysis",
            "url": "https://www.elibm.org/series?q=se:2063",
        },
        "EJDE": {
            "pid": "EJDE",
            "title": "Electronic Journal of Differential Equations",
            "url": "https://www.elibm.org/series?q=se:1957",
        },
        "JAC": {
            "pid": "JAC",
            "title": "Journal of Algebraic Combinatorics",
            "url": "https://www.elibm.org/series?q=se:1828",
        },
        "JGAA": {
            "pid": "JGAA",
            "title": "Journal of Graph Algorithms and Applications",
            "url": "https://www.elibm.org/series?q=se:2402",
        },
        "JIS": {
            "pid": "JIS",
            "title": "Journal of Integer Sequences",
            "url": "https://www.elibm.org/series?q=se:2512",
        },
        "SSIG": {
            "pid": "SSIG",
            "title": "SIGMA. Symmetry, Integrability and Geometry: Methods and Applications",
            "url": "https://www.elibm.org/series?q=se:3352",
        },
    }


def get_numdam_collections():
    """
    Returns a list of Numdam collection pids
    """

    url = "http://www.numdam.org/api-all-collections/"

    response = requests.get(url)
    if response.status_code != 200:
        return []

    data = response.json()
    if "collections" not in data:
        return []

    return data["collections"]


def get_or_create_collection(pid: str):
    """
    Creates a Collection based on its pid.
    The pid has to be in the list of collections given by the Documentation team (CSV then transformed in JSON)
    """

    all_collections = get_all_cols()

    if pid == "DA2":
        col_data = {
            "pid": "DA2",
            "url": "https://discreteanalysisjournal.com",
            "title": "Discrete Analysis",
        }
    elif pid == "ARSIA":
        col_data = {
            "pid": "ARSIA",
            "url": "https://ars-inveniendi-analytica.com",
            "title": "Ars Inveniendi Analytica",
        }
    elif pid == "AMC":
        col_data = {
            "pid": "AMC",
            "url": "https://amc-journal.eu/index.php/amc/issue/archive",
            "title": "Ars Mathematica Contemporanea",
        }
    else:
        if pid not in all_collections:
            raise ValueError(f"{pid} is not listed in the Eudml collections")

        col_data = [item for item in all_collections.items() if item[0] == pid][0][1]

    collection: Collection | None = model_helpers.get_collection(pid)

    if not collection:
        p = model_helpers.get_provider("mathdoc-id")

        xcol = create_publicationdata()
        xcol.coltype = "journal"
        xcol.pid = pid
        xcol.title_tex = col_data["title"]
        # Mis en commentaire car trop tôt, Taban n'a pas encore validé les ISSNs
        # xcol.e_issn = col_data["ISSN_électronique"]
        # xcol.issn = col_data["ISSN_papier"]
        xcol.title_html = col_data["title"]
        xcol.title_xml = f"<title-group><title>{col_data['title']}</title></title-group>"
        xcol.lang = "en"

        cmd = ptf_cmds.addCollectionPtfCmd({"xobj": xcol})
        cmd.set_provider(p)
        collection = cmd.do()

        # Mis en commentaire car trop tôt, Taban n'a pas encore validé les ISSNs
        # if col_data["ISSN_électronique"] != "":
        #     e_issn = {
        #         "resource_id": collection.resource_ptr_id,
        #         "id_type": "e_issn",
        #         "id_value": col_data["ISSN_électronique"],
        #     }
        #     ResourceId.objects.create(**e_issn)
        #
        # if col_data["ISSN_papier"] != "":
        #     issn = {
        #         "resource_id": collection.resource_ptr_id,
        #         "id_type": "issn",
        #         "id_value": col_data["ISSN_papier"],
        #     }
        #     ResourceId.objects.create(**issn)

    if not collection:
        raise ResourceDoesNotExist(f"Resource {pid} does not exist")

    return collection


NUMDAM_COLLECTIONS = [
    "ACIRM",
    "ALCO",
    "AFST",
    "AIHPC",
    "AIHPA",
    "AIHPB",
    "AIF",
    "AIHP",
    "AUG",
    "AMPA",
    "AHL",
    "AMBP",
    "ASENS",
    "ASCFPA",
    "ASCFM",
    "ASNSP",
    "AST",
    "BSMF",
    "BSMA",
    "CTGDC",
    "BURO",
    "CSHM",
    "CG",
    "CM",
    "CRMATH",
    "CML",
    "CJPS",
    "CIF",
    "DIA",
    "COCV",
    "M2AN",
    "PS",
    "GAU",
    "GEA",
    "STS",
    "TAN",
    "JSFS",
    "JEP",
    "JMPA",
    "JTNB",
    "JEDP",
    "CAD",
    "CCIRM",
    "RCP25",
    "MSIA",
    "MRR",
    "MSH",
    "MSMF",
    "MSM",
    "NAM",
    "OJMO",
    "PHSC",
    "PSMIR",
    "PDML",
    "PMB",
    "PMIHES",
    "PMIR",
    "RO",
    "RCP",
    "ITA",
    "RSMUP",
    "RSA",
    "RHM",
    "SG",
    "SB",
    "SBCD",
    "SC",
    "SCC",
    "SAF",
    "SDPP",
    "SMJ",
    "SPHM",
    "SPS",
    "STNB",
    "STNG",
    "TSG",
    "SD",
    "SE",
    "SEDP",
    "SHC",
    "SJ",
    "SJL",
    "SLSEDP",
    "SLDB",
    "SL",
    "SPK",
    "SAC",
    "SMS",
    "SLS",
    "SSL",
    "SENL",
    "SSS",
    "SAD",
    "THESE",
    "SMAI-JCM",
    "WBLN",
]


def cleanup_str(input: str):
    # some white spaces aren't actual space characters, like \xa0
    input = unicodedata.normalize("NFKD", input)
    # remove useless continuous \n and spaces from the string
    return re.sub(r"[\n\t ]+", " ", input).strip()
