from typing import Literal, TypedDict

CitationLiteral = Literal["lang", "title", "author", "pdf",
                          "abstract", "page", "doi", "mr", "zbl", "publisher", "keywords"]


class JSONCol(TypedDict):
    """Element of the JSON outputted by the CSV file"""
    sources: dict[str, str]
    pid: str
    state: str
    pISSN: str
    eISSN: str
    title: str
    published: str
    type: str
