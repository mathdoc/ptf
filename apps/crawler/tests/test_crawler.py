import os
import re

# from ..utils import read_json
from .test import BaseTestCase


class CrawlerTestCase(BaseTestCase):
    @classmethod
    def setUpClass(cls):
        print("\n**App: 'Crawler ' - Testing `crawl_annals_of_maths.py`**")
        super().setUpClass()

    def test_crawler_instance_response(self, match=[]):
        """Emulate a HTTP 200 OK response for `/api/comments/?(.*)`"""

        def walk_collections(begin=None, source=None, iter_level=None, current_pos=None):
            """

            @param begin:
            @param source:
            @param iter_level: level of walk tree
            @param current_pos:
            @return:
            """
            dir_start = self.dir_html
            # toc_list = ""
            if begin is not None:
                dir_start = dir_start + "/" + begin
            if iter_level is None:
                iter_level = 0
            if current_pos is None:
                current_pos = 0
            for subdir, dirs, files in os.walk(dir_start):
                for index, file in enumerate(files):
                    if file == "toc.json":
                        print("read collections by source....")
                        # toc_list = read_json(dir_start + "/" + file)
                # init list of collections
                if iter_level == 0 and begin is None:
                    self.list_cols = dirs
                if iter_level <= 1:
                    for index, dir_col in enumerate(dirs):
                        # première iteration, récuperation collection
                        if index == 0 and iter_level == 0:
                            if self.list_cols[current_pos] != begin:
                                walk_collections(
                                    begin=dir_col, iter_level=iter_level, current_pos=current_pos
                                )
                            else:
                                walk_collections(
                                    begin=dir_col, iter_level=iter_level, current_pos=current_pos
                                )

                        if dir_col == "html" and source is None:
                            source = next(os.walk(dir_start + "/html"))[1]
                            if re.compile("^[a-zA-Z]+(?:(?!_))$").search(source[0]):
                                source = source[0]
                                break

                iter_level += 1
                if iter_level == 1:
                    current_pos += 1
                    begin = self.list_cols[current_pos]

                    walk_collections(begin=begin, iter_level=iter_level, current_pos=current_pos)

        walk_collections()
        self.assertEqual(True, True)
