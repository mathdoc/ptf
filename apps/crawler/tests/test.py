import os
from os.path import dirname

from django.contrib.auth.models import Group
from django.contrib.auth.models import User
from django.contrib.messages import get_messages
from django.http import HttpResponse
from django.test import TestCase

"""
from gdml import settings
from gdml.models import Periode
from gdml.models import Source
from ptf.models import Collection
from ptf.models import Provider
from ptf.models import Publisher
"""


class BaseTestCase(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()

    def setUp(self):
        # init collection
        """
        xcollection = ""
        xcollection = collections_to_crawl["AM"].copy()

        xcollection["title_html"] = xcollection["title"]
        xcollection["title_tex"] = xcollection["title"]
        xcollection[
            "title_xml"
        ] = f"<title-group><title>{xcollection['title_html']}</title></title-group>"
        provider = Provider.objects.filter(name="mathdoc")
        xcollection["provider"] = provider[0]
        """
        self.group_manage_import = Group(name="can_manage_import")
        self.group_manage_resources = Group(name="can_manage_resources")
        self.group_manage_import.save()
        self.group_manage_resources.save()

        self.user = User.objects.create_user(
            username="test", email="test@test.com", password="test"
        )
        self.user.groups.add(self.group_manage_import)
        self.user.groups.add(self.group_manage_resources)
        self.user.save()
        """
        source_data = {
            "name": "Princeton University",
            "website": xcollection["website"],
            "domain": xcollection["domain"],
            "create_xissue": False,
            "article_href": xcollection["article_href"],
            "pdf_href": xcollection["pdf_href"],
            "periode_href": xcollection["periode_href"],
        }
        source = Source.objects.create(**source_data)

        issue_href = xcollection["issue_href"]
        collection_href = xcollection["collection_href"]
        doi_href = xcollection["doi_href"]

        xcollection.pop("create_xissue")
        xcollection.pop("pdf_href")
        xcollection.pop("article_href")
        xcollection.pop("periode_href")
        xcollection.pop("domain")
        xcollection.pop("issue_href")
        xcollection.pop("id")
        xcollection.pop("title")
        xcollection.pop("collection_href")
        xcollection.pop("publisher")
        xcollection.pop("website")
        xcollection.pop("doi_href")
        xcollection.pop("periode_range")

        collection = Collection.objects.create(**xcollection)
        collection.sites.set([settings.SITE_ID])
        collection.provider = provider[0]
        collection.save()
        # create default periode

        pub = Publisher.objects.create(**{"pub_name": "mathdoc", "pub_loc": ""})
        print(pub)
        periode_data = {
            "source": source,
            "begin": 2003,
            "end": 2017,
            "collection": collection,
            "collection_href": collection_href,
            "title": collection.title_html,
            "published": False,
            "wall": 0,
            "publisher": pub,
            "doi_href": doi_href,
            "issue_href": issue_href,
        }
        periode = Periode.objects.create(**periode_data)

        print("collection reference added :" + str(periode.pk))

        # init collections
        self.collection = collection
        self.source = source
        self.periode = periode
        xcollection = collections_to_crawl["AM"]
        xcollection["id"] = self.periode.pk
        """
        self.dir_html = dirname(dirname(os.path.abspath(__file__))) + "/html"
        self.list_cols = []

    def clear_messages(self):
        """
        Deletes all messages of the test client.
        Checks the presence of messages in both cookies and session (depends on the
        configured storage).
        """
        if "messages" in self.client.cookies:
            del self.client.cookies["messages"]
        if "_messages" in self.client.session:
            del self.client.session["_messages"]

    def assertUniqueLevelMessage(self, response: HttpResponse, level: list, clear_messages=True):
        messages = list(get_messages(response.wsgi_request))
        self.assertEqual(len(messages), 1)
        self.assertIn(messages[0].level_tag, level)
        if clear_messages:
            self.clear_messages()

    def assertUniqueSuccessMessage(self, response: HttpResponse, clear_messages=True):
        """
        Asserts the given HttpResponse has an unique attached message with
        a success level_tag.
        If `clear_messages is True`, it deletes all the messages stored in the
        test client.
        """
        self.assertUniqueLevelMessage(response, ["success"], clear_messages)

    def assertUniqueErrorMessage(self, response: HttpResponse, clear_messages=True):
        """
        Asserts the given HttpResponse has an unique attached message with
        an error level_tag.
        If `clear_messages is True`, it deletes all the messages stored in the
        test client.
        """
        self.assertUniqueLevelMessage(response, ["error", "danger"], clear_messages)

    def assertUniqueWarningMessage(self, response: HttpResponse, clear_messages=True):
        """
        Asserts the given HttpResponse has an unique attached message with
        a warning level_tag.
        If `clear_messages is True`, it deletes all the messages stored in the
        test client.
        """
        self.assertUniqueLevelMessage(response, ["warning"], clear_messages)
