# import shutil
# import tempfile
# import urllib
# import xml.etree.ElementTree as ET

import requests
from bs4 import BeautifulSoup

from ptf import model_data
from ptf.cmds.xml.ckeditor.utils import get_html_and_xml_from_text_with_formulas
from ptf.cmds.xml.xml_utils import get_contrib_xml

ZBMATH_URL = "https://zbmath.org"
ZBMATH_API_URL = "https://api.zbmath.org/v1/document/"


def zbmath_request_article(zblid):
    params = {"q": "an:" + zblid}
    headers = {"Content-Type": "text/html"}
    response = requests.get(ZBMATH_URL, params=params, headers=headers, timeout=2.0)
    if response.status_code != 200:
        return None

    soup = BeautifulSoup(response.text, "html.parser")

    # TODO:
    #  1. call cited_by.get_zbmath_bibtex(params)
    #  2. extract the zbMATH number
    #  3. call the zbMATH OAI to get the article XML
    #  4. parse the XML to create the article_data
    #  The XML does not always have metadata so the following is the fallback method

    article_data = model_data.create_articledata()

    for div_author in soup.find_all("div", {"class": "author"}):
        for link in div_author.find_all("a"):
            author = model_data.create_contributor()
            author["role"] = "author"
            author["string_name"] = link.get_text()
            author["contrib_xml"] = get_contrib_xml(author)
            article_data.contributors.append(author)

    title_h2 = soup.find("h2", {"class": "title"})
    if title_h2:
        title_h2 = title_h2.find("strong")
        if title_h2:
            value_tex = str(title_h2)[
                8:-9
            ]  # Get the zbmath text keeping the tags, except the surrounding <strong>
            if value_tex[-1] == ".":  # Remove a trailing "."
                value_tex = value_tex[:-1]
            value_html, value_xml = get_html_and_xml_from_text_with_formulas(
                value_tex, delimiter_inline="\\(", delimiter_disp="\\["
            )

            article_data.title_tex = value_tex
            article_data.title_html = value_html
            article_data.title_xml = (
                f"<title-group><article-title>{value_xml}</article-title></title-group>"
            )

    abstract_elt = soup.find("div", {"class": "abstract"})
    if abstract_elt:
        value_tex = str(abstract_elt)
        value_tex = value_tex.replace('a href="/', 'a href="https://zbmath.org/')
        value_html, value_xml = get_html_and_xml_from_text_with_formulas(
            value_tex, delimiter_inline="\\(", delimiter_disp="\\["
        )

        value_xml = f'<abstract xml:lang="en">{value_xml}</abstract>'

        abstract_data = {
            "tag": "abstract",
            "lang": "en",
            "value_xml": value_xml,
            "value_html": value_html,
            "value_tex": value_tex,
        }

        article_data.abstracts.append(abstract_data)

    # Use api.zbmath.org to find more info

    # First find the zbMATH internal id.
    oai_node = soup.find("a", {"class": "btn btn-default btn-xs xml"})
    if oai_node:
        oai_url = oai_node.get("href")
        if "%3A" in oai_url:
            url = oai_url.split("%3A")[-1]
            url = ZBMATH_API_URL + url

            headers = {"Content-Type": "application.json"}
            response = requests.get(url, headers=headers, timeout=2.0)
            data = response.json()
            try:
                reviewer = data["result"]["editorial_contributions"][0]["reviewer"]["name"]
                if reviewer is not None:
                    # The abstract is in fact a review, remove it
                    article_data.abstracts = []
            except Exception:
                pass

    # OLD CODE to request and parse the zbmath OAI
    #     with urllib.request.urlopen(OAI_URL) as response:
    #         with tempfile.NamedTemporaryFile(delete=False, suffix=".xml") as tmp_file:
    #             shutil.copyfileobj(response, tmp_file)
    #     with open(tmp_file.name) as article_oai:
    #         body = f"""{article_oai.read()}"""
    #
    #         tree = ET.fromstring(body)
    #         records = tree[2]
    #         for record in records:
    #             # tag = record.tag
    #             metadata = record[1]
    #             """for node in metadata[0]:
    #                 text = node.text
    #             """
    #
    #             xarticle = jats_parser.JatsArticle(tree=metadata[0])

    return article_data
