# Crawl


#### [Installation](https://pptr.dev/category/guides) | [Fonctionnement](https://pptr.dev/api) | [Exemple](https://pptr.dev/faq) |

> Application crawler
>
> données initiales: data/all_cols.csv
>
> Démarrage avec CLI


## Demarrage

### Installation

depuis l'application gdml:


```bash

pip3.11 install -r requirements.txt --upgrade

```
installation de puppeteer


#### Données initiales

Les données de références se trouvent dans **/data/all_cols.csv**
le fichier contient l'ensemble des collections et des sources

Pour une source unique à moissonner, l'identifiant est le PID source.

> Exemple avec la source **TAC**

    PID: "TAC"
    title: "Theory and Applications of Categories"
    type: "journal"
    source_id: "TAC"
    url : "http://www.tac.mta.ca/tac"

Au préalable générer ce fichier avec des noms de colonne unique

> Cas avec plusieurs sources pour une collection

pour la collection **Annals Of Mathematics**

- colonne **source** : remplir la matrice correspondante avec "EUDML,AMP"

- colonne **url** : "https://eudml.org/journal/10098,https://annals.math.princeton.edu/"

l'ordre est à respecter.



##### Conversion du fichier en json

Lancer la commande
```commandline
convert_all_cols -i "all_cols.csv" -o "data/all_cols.json"
```

## Fonctionnement

Une fois le fichier de référentiel généré, une commande permet de moissoner :

- un ensemble de collections / source
- une source directement avec ses collections

1. récuperation des données


récuperation des données d'entrée en fonction des paramètres (Collections, Source)
retourne une liste de collections

2. processus de crawl


<img align="center" style="margin-left: 100px;" src="doc/crawl_process.png">


### Exemple

```bash
python3 manage.py crawl -pid ALL -source ELIBM -username username
```

```bash
python3 manage.py crawl -pid PID -source ELIBM -username username
```
