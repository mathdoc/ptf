from .base_crawler import BaseCollectionCrawler
from .by_source.amc_crawler import AmcCrawler  # noqa: F401 (class name dynamically set)
from .by_source.amp_crawler import AmpCrawler  # noqa: F401 (class name dynamically set)
from .by_source.arsia_crawler import ArsiaCrawler  # noqa: F401 (class name dynamically set)
from .by_source.bdim_crawler import BdimCrawler  # noqa: F401 (class name dynamically set)
from .by_source.da_crawler import DaCrawler  # noqa: F401 (class name dynamically set)
from .by_source.dmlcz_crawler import DmlczCrawler  # noqa: F401 (class name dynamically set)
from .by_source.elibm_crawler import ElibmCrawler  # noqa: F401 (class name dynamically set)
from .by_source.eudml_crawler import EudmlCrawler  # noqa: F401 (class name dynamically set)
from .by_source.hdml_crawler import HdmlCrawler  # noqa: F401 (class name dynamically set)
from .by_source.tac_crawler import TacCrawler  # noqa: F401 (class name dynamically set)
from .by_source.sasa_crawler import SasaCrawler  # noqa : F401 (class name dynamically set)
from .by_source.nsjom_crawler import NsjomCrawler  # noqa: F401 (class name dynamically set)
from .by_source.impan_crawler import ImpanCrawler  # noqa: F401 (class name dynamically set)


def crawler_factory(
    source, colid, col_url, username, progress_bar=None, start_pid=None, test_mode=None
) -> BaseCollectionCrawler:
    """
    Factory for the crawlers

    :param source: "Eudml"
    :param colid: collection pid
    :param col_url: url of the collection web page
    :param username:
    :param progress_bar:  alive_bar progress_bar if you already have one (default: None)
    :return: a crawler derived from base_crawler
    """
    class_name = source.capitalize() + "Crawler"
    klass = globals()[class_name]
    crawler: BaseCollectionCrawler = klass(
        collection_id=colid,
        collection_url=col_url,
        username=username,
        progress_bar=progress_bar,
        start_pid=start_pid,
        test_mode=test_mode,
    )

    return crawler
