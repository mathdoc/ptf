import os
import subprocess
import time
from collections import OrderedDict
from collections.abc import Sequence
from datetime import timedelta

import requests
from alive_progress import alive_bar
from bs4 import BeautifulSoup
from crawler.utils import get_or_create_collection
from pylatexenc.latex2text import LatexNodes2Text
from pysolr import SolrError
from requests_cache import CachedSession
from requests_cache import FileCache

from django.conf import settings
from django.contrib.auth.models import User
from django.utils import timezone

# TODO: pass a class factory instead of a dependency to a site
# TODO: pass a class factory instead of a dependency to a site
from gdml.models import Periode
from gdml.models import Source
from ptf.cmds import xml_cmds
from ptf.cmds.xml.ckeditor.utils import get_html_and_xml_from_text_with_formulas
from ptf.cmds.xml.jats.builder.issue import get_title_xml
from ptf.cmds.xml.jats.jats_parser import check_bibitem_xml
from ptf.display.resolver import extids_formats
from ptf.display.resolver import resolve_id
from ptf.model_data import AbstractDict
from ptf.model_data import ArticleData
from ptf.model_data import IssueData
from ptf.model_data import RefData
from ptf.model_data import create_articledata
from ptf.model_data import create_contributor
from ptf.model_data import create_extlink
from ptf.model_data import create_publisherdata
from ptf.model_data_converter import update_data_for_jats
from ptf.utils import execute_cmd

from .types import CitationLiteral


class BaseCollectionCrawler:
    """
    Base collection for the crawlers.
    To create a crawler:
    1) derive a class from BaseCollectionCrawler and name it XXXCrawler
    2) override the functions parse_collection_content, parse_issue_content and parse_article_content
    3) update factory.py so that crawler_factory can return your new crawler
    """

    session: requests.Session | CachedSession

    def __init__(self, *args, **kwargs):
        self.username = kwargs["username"]
        self.user = User.objects.get(username=self.username)

        self.collection_id = kwargs["collection_id"]
        self.collection_url = kwargs[
            "collection_url"
        ]  # url of the collection. Ex: https://eudml.org/journal/10098
        self.collection = get_or_create_collection(self.collection_id)

        self.source_name = ""
        self.source_domain = ""
        self.source_website = ""
        self.source = None

        self.issue_href = ""
        self.test_mode = kwargs.get("test_mode", False)
        self.publisher = kwargs.get("publisher", "mathdoc")

        # progress_bar can be set externally, for example if you want to crawl all the collections of a given source.
        self.progress_bar = kwargs.get("progress_bar", None)

        # EuDML uses javascript to fill the journal page with the issues list.
        # We need to use a headless browser (NodeJs/Puppeteer) that can handle dynamic content with EuDML.
        # Set has_dynamic* to True if the Source uses dynamic content in its web pages.
        self.has_dynamic_collection_pages = False
        self.has_dynamic_issue_pages = False
        self.has_dynamic_article_pages = False

        # EUDML sets or creates the Periode based on the <meta name="citation_year"> found in the journal page
        # AMP sets or creates the Periode during the __init__
        # TODO: see with other sources when to create the Periode
        self.periode = None
        self.periode_begin = None
        self.periode_end = None
        self.periode_first_issue = None
        self.periode_last_issue = None

        self.start_pid = kwargs.get("start_pid", None)

        # Some source have multiple pages for 1 issue. We need to merge the content

        self.latext_parser = LatexNodes2Text()

        # Override the values in your concrete crawler if the formulas in text (titles, abstracts)
        # do not use the "$" to surround tex formulas
        self.delimiter_inline_formula = "$"
        self.delimiter_disp_formula = "$"

        self.session = CachedSession(
            backend=FileCache(
                getattr(settings, "REQUESTS_CACHE_LOCATION", None) or "/tmp/ptf_requests_cache",
                decode_content=False,
            ),
            headers={
                "User-Agent": getattr(settings, "REQUESTS_USER_AGENT", None) or "Mathdoc/1.0.0",
                "From": getattr(settings, "REQUESTS_EMAIL", None) or "accueil@listes.mathdoc.fr",
            },
            expire_after=timedelta(days=30),
        )
        # self.session = requests.Session()

    def parse_collection_content(self, content: str) -> list[IssueData]:
        """
        Parse the HTML content with BeautifulSoup
        returns a list of xissue.
        Override this function in a derived class
        """
        return []

    def parse_issue_content(self, content: str, xissue: IssueData):
        """
        Parse the HTML content with BeautifulSoup
        Fills the xissue.articles
        Override this function in a derived class.
        """

    def parse_article_content(
        self, content: str, xissue: IssueData, xarticle: ArticleData, url: str, pid: str
    ):
        """
        Parse the HTML content with BeautifulSoup
        returns the xarticle.
        Override this function in a derived class.
        The xissue is passed to the function in case the article page has issue information (ex: publisher)
        The article url is also passed as a parameter
        """
        return create_articledata()

    def crawl_collection(self, col_only=False):
        """
        Crawl an entire collection. ptf.models.Container objects are created.
        - get the HTML content of the collection_url
        - parse the HTML content with beautifulsoup to extract the list of issues
        - merge the xissues (some Source can have multiple pages for 1 volume/issue. We create only 1 container)
        - crawl each issue if col_only is False
        - Returns the list of merged issues.
              It is an OrderedDict {pid: {"issues": xissues}}
              The key is the pid of the merged issues.
              Ex: The source may have Ex: Volume 6 (2000) and Volume 6 (1999)
              the pid is then made with 1999-2000__6_
        """

        if self.source is None:
            raise RuntimeError("ERROR: the source is not set")

        content = self.get_page_content(self.collection_url)
        xissues = self.parse_collection_content(content)

        """
        Some collections split the same volumes in different pages
        Ex: Volume 6 (2000) and Volume 6 (1999)
        We merge the 2 xissues with the same volume number => Volume 6 (1999-2000)
        """
        merged_xissues = self.merge_xissues(xissues)

        if col_only:
            # TODO: update Celery tasks
            return merged_xissues

        filtered_xissues = OrderedDict()

        # Filter the issues to crawl if start_pid was set in the constructor
        start = False
        for pid in merged_xissues:
            if self.start_pid is None or start or pid == self.start_pid:
                start = True
                filtered_xissues[pid] = merged_xissues[pid]

        def iterate_xissues(filtered_xissues, progress_bar_for_issues):
            """
            internal function to be used by alive_bar (see below)
            to iterate on the issues to crawl.
            crawl_issue calls addOrUpdateIssueXmlCmd but returns the xissue computed by the crawl
            """
            crawled_xissues = []
            for pid in filtered_xissues:
                if self.progress_bar is not None and progress_bar_for_issues:
                    self.progress_bar()
                crawled_xissue = self.crawl_issue(pid, filtered_xissues)
                crawled_xissues.append(crawled_xissue)
            return crawled_xissues

        if self.progress_bar is None:
            with alive_bar(
                len(filtered_xissues),
                dual_line=True,
                title=f"Crawl {self.collection_id} - {self.collection_url}",
                stats="(eta {eta})",
                force_tty=True,
            ) as self.progress_bar:
                crawled_xissues = iterate_xissues(filtered_xissues, progress_bar_for_issues=True)
        else:
            crawled_xissues = iterate_xissues(filtered_xissues, progress_bar_for_issues=False)

        return crawled_xissues

    def crawl_issue(self, pid, merged_xissues):
        """
        Wrapper around crawl_one_issue_url, to handle issues declared in multiple web pages.
        If you want to crawl only 1 issue and not the entire collection,
           you need to call crawl_collection(col_only=True) before to get the merged_xissues
        A ptf.models.Container object is created with its Articles.
        Returns the full xissue (with its articles) used to call addOrUpdateIssueXmlCmd
        """

        if pid not in merged_xissues:
            raise ValueError(f"Error {pid} is not found in the collection")

        xissues_to_crawl = merged_xissues[pid]["issues"]

        merged_xissue = self.crawl_one_issue_url(xissues_to_crawl[0])

        if len(xissues_to_crawl) > 1:
            do_append = merged_xissues[pid]["do_append"]
            for index, raw_xissue in enumerate(xissues_to_crawl[1:]):
                crawled_xissue = self.crawl_one_issue_url(raw_xissue)

                if do_append:
                    merged_xissue.articles.extend(crawled_xissue.articles)
                else:
                    merged_xissue.articles[:0] = crawled_xissue.articles

                # Updates the article pid
                for article_index, xarticle in enumerate(merged_xissue):
                    if raw_xissue.pid in xarticle.pid:
                        xarticle.pid = f"{pid}_a{str(article_index)}"

        # Now that the issue pages have been downloaded/read, we can set the merged pid
        # The merged_year was set in self.merge_xissues
        merged_xissue.pid = pid
        merged_xissue.year = merged_xissue.merged_year
        if self.test_mode is False or self.test_mode is None:
            if len(merged_xissue.articles) > 0:
                self.add_xissue_into_database(merged_xissue)

        return merged_xissue

    def crawl_one_issue_url(self, xissue: IssueData):
        """
        Crawl 1 wag page of an issue.
        - get the HTML content of the issue
        - parse the HTML content with beautifulsoup to extract the list of articles and/or the issue metadata
        - crawl each article
        """

        # Some source, like EuDML do not have a separate HTML pages for an issue's table of content.
        # The list of articles directly come from the collection HTML page: the xissue has no url attribute
        if hasattr(xissue, "url") and xissue.url:
            content = self.get_page_content(xissue.url, xissue.pid)
            self.parse_issue_content(content, xissue)

        xarticles = xissue.articles

        if self.progress_bar:
            self.progress_bar.title = (
                f"Crawl {self.collection_id} - {xissue.year} {xissue.volume} {xissue.number}"
            )

        parsed_xarticles = []

        for xarticle in xarticles:
            parsed_xarticle = self.crawl_article(xarticle, xissue)
            if parsed_xarticle is not None:
                parsed_xarticles.append(parsed_xarticle)

        xissue.articles = parsed_xarticles

        return xissue

    def crawl_article(self, xarticle: ArticleData, xissue: IssueData):
        if hasattr(xarticle, "url") and xarticle.url:
            if self.progress_bar:
                self.progress_bar.text(f"{xarticle.pid} - {xarticle.url}")

            url = xarticle.url

            content = self.get_page_content(xarticle.url, xissue.pid, xarticle.pid)
            pid = f"{xissue.pid}_{xarticle.pid}"
            xarticle = self.parse_article_content(content, xissue, xarticle, xarticle.url, pid)
            if xarticle is None:
                return None

            # ARTICLE URL as en ExtLink (to display the link in the article page)
            ext_link = create_extlink()
            ext_link["rel"] = "source"
            ext_link["location"] = url
            ext_link["metadata"] = self.source_domain
            xarticle.ext_links.append(ext_link)

        # The article title may have formulas surrounded with '$'
        return self.process_article_metadata(xarticle)

    def process_article_metadata(self, xarticle: ArticleData):
        html, xml = get_html_and_xml_from_text_with_formulas(
            xarticle.title_tex,
            delimiter_inline=self.delimiter_inline_formula,
            delimiter_disp=self.delimiter_disp_formula,
        )
        xml = get_title_xml(xml, with_tex_values=False)
        xarticle.title_html = html
        xarticle.title_xml = xml

        abstracts_to_parse = [
            xabstract for xabstract in xarticle.abstracts if xabstract["tag"] == "abstract"
        ]
        # abstract may have formulas surrounded with '$'
        if len(abstracts_to_parse) > 0:
            for xabstract in abstracts_to_parse:
                html, xml = get_html_and_xml_from_text_with_formulas(
                    xabstract["value_tex"],
                    delimiter_inline=self.delimiter_inline_formula,
                    delimiter_disp=self.delimiter_disp_formula,
                )
                xabstract["value_html"] = html
                lang = xabstract["lang"]
                if lang == xarticle.lang:
                    xabstract["value_xml"] = f'<abstract xml:lang="{lang}">{xml}</abstract>'
                else:
                    xabstract[
                        "value_xml"
                    ] = f'<trans-abstract xml:lang="{lang}">{xml}</trans-abstract>'

        update_data_for_jats(xarticle)

        return xarticle

    def download_file(self, url: str, has_dynamic_pages: bool, filename: str | None = None):
        """
        Downloads a URL, saves its content on disk in filename and returns its content.
        If has_dynamic_pages is False, the Python requests library is used to get the URL content
        Otherwise, a nodejs/puppeteer script is called.
        """
        if has_dynamic_pages and filename is None:
            raise ValueError("ERROR. Crawling dynamic web pages need a filename")

        txt = f"Download {url} in {filename}"
        if settings.CRAWLER_LOG_FILE:
            with open(settings.CRAWLER_LOG_FILE, "a") as f_:
                f_.write(txt + "\n")

        content = ""
        attempt = 0
        while not content and attempt < 3:
            if has_dynamic_pages and filename is not None:
                attempt += 1
                try:
                    cmd = f"cd {settings.NODE_CRAWLER_FOLDER}; {settings.NODE_CRAWLER_BIN} -u {url} -o {filename}"
                    execute_cmd(cmd)

                    if os.path.isfile(filename):
                        with open(filename) as file_:
                            content = file_.read()
                except subprocess.CalledProcessError:
                    pass
            else:
                try:
                    headers = {"accept_encoding": "utf-8"}
                    # For SSL Errors, use verify=False kwarg
                    response = self.session.get(url, headers=headers)
                    content = self.decode_response(response)

                    if filename is not None:
                        with open(filename, "w") as file_:
                            file_.write(content)

                except (requests.ConnectionError, requests.ConnectTimeout):
                    attempt += 1

            if not content:
                raise requests.exceptions.HTTPError(f"Unable to download {url}")

        return content

    def decode_response(self, response: requests.Response, encoding: str = "utf-8"):
        """Override this if the content-type headers from the sources are advertising something else than the actual content
        SASA needs this"""
        response.encoding = encoding
        return response.text

    def get_page_content(
        self, url: str, container_id: str | None = None, article_id=None, force_download=False
    ):
        """
        Get the HTML content of a given url.
        A cache is used to back up the HTML content on disk. By default, the cache is used to read the HTML content.
        """

        # Even if a file is present on disk at the give container_id/article_id, there might be a mismatch with the url
        # This could happen in case of a code change in the parsing function (ie: bug fix)
        content = ""

        if article_id is not None:
            has_dynamic_pages = self.has_dynamic_article_pages
        elif container_id is not None:
            has_dynamic_pages = self.has_dynamic_issue_pages
        else:
            has_dynamic_pages = self.has_dynamic_collection_pages

        def set_progress_bar_title():
            if not self.progress_bar:
                return
            if isinstance(self.session, CachedSession):
                if self.session.cache.contains(
                    url=url,
                ):
                    self.progress_bar.text(f"Get Cached {url}")
                    return
            self.progress_bar.text(f"Download {url}")

        set_progress_bar_title()
        content = self.download_file(url, has_dynamic_pages)

        return content

    def get_or_create_source(self):
        try:
            source = Source.objects.get(name=self.source_name)
        except Source.DoesNotExist:
            source = Source(
                name=self.source_name,
                domain=self.source_domain,
                website=self.source_website,
                create_xissue=True,
                periode_href="",
                article_href="",
                pdf_href="",
            )
            source.save()

        return source

    def get_or_create_periode(self):
        if self.periode is not None:
            return self.periode

        if self.collection is None or self.source is None:
            raise ValueError("You need to set a collection or a source before creating a periode")

        qs = Periode.objects.filter(collection=self.collection, source=self.source)
        if qs.exists():
            periode = qs.first()
        else:
            periode = Periode(
                collection=self.collection,
                source=self.source,
                title=self.collection.title_tex,
                issue_href=self.issue_href,
                collection_href=self.collection_url,
                doi_href="",
                published=False,
                begin=self.periode_begin,
                end=self.periode_end,
                first_issue=self.periode_first_issue,
                last_issue=self.periode_last_issue,
            )
            periode.save()

        return periode

    def merge_xissues(self, xissues: list[IssueData]):
        """
        Some collections split the same volumes in different pages
        Ex: Volume 6 (2000) and Volume 6 (1999)
        We merge the 2 xissues with the same volume number => Volume 6 (1999-2000)
        """

        merged_xissues = OrderedDict()

        for xissue in xissues:
            xissues_with_same_volume = [
                item
                for item in xissues
                if xissue.volume == item.volume
                and xissue.number == item.number
                and xissue.vseries == item.vseries
                and (item.volume or item.number)
            ]

            do_append = False

            if len(xissues_with_same_volume) < 2:
                if xissue.pid is None:
                    raise ValueError("Issue does not have a PID")
                merged_xissues[xissue.pid] = {"issues": [xissue], "do_append": True}
                first_issue = xissue
                year = xissue.year
            else:
                first_issue = xissues_with_same_volume[0]
                volume = xissues_with_same_volume[0].volume
                number = xissues_with_same_volume[0].number
                vseries = xissues_with_same_volume[0].vseries

                # Compute the year based on all issues with the same volume/number
                begin = end = year = xissues_with_same_volume[0].year
                if not year:
                    raise ValueError("year is not defined")

                if "-" in year:
                    parts = year.split("-")
                    begin = parts[0]
                    end = parts[1]

                for xissue_with_same_volume in xissues_with_same_volume[1:]:
                    new_begin = new_end = xissue_with_same_volume.year

                    if not xissue_with_same_volume.year:
                        raise ValueError("xissue year is not defined")

                    if "-" in xissue_with_same_volume.year:
                        parts = year.split("-")
                        new_begin = parts[0]
                        new_end = parts[1]

                    if begin is None or end is None or new_begin is None or new_end is None:
                        continue
                    begin_int = int(begin)
                    end_int = int(end)
                    new_begin_int = int(new_begin)
                    new_end_int = int(new_end)

                    if new_begin_int < begin_int:
                        begin = new_begin
                    if new_end_int > end_int:
                        end = new_end
                        do_append = True

                    if begin != end:
                        year = f"{begin}-{end}"
                    else:
                        year = begin

                # We can now set the real pid
                # Note: We cannot update the pid of each xissue of xissues_with_same_volume
                #       because the HTML cache relies on the original id
                pid = f"{self.collection_id}_{year}_{vseries}_{volume}_{number}"
                if pid not in merged_xissues:
                    merged_xissues[pid] = {
                        "issues": xissues_with_same_volume,
                        "do_append": do_append,
                    }

            # We can set the year only for the first xissue because it is the one used to collect
            # all the articles.
            # See crawl_issue with merged_xissue = self.crawl_one_issue_url(xissues_to_crawl[0])
            # But we need to use a separate variable (merged_year) because parse_article_content may rely on the year
            first_issue.merged_year = year

        return merged_xissues

    def add_xissue_into_database(self, xissue: IssueData):
        xissue.journal = self.collection

        xpub = create_publisherdata()
        xpub.name = self.publisher
        xissue.publisher = xpub
        xissue.last_modified_iso_8601_date_str = timezone.now().isoformat()

        attempt = 1
        success = False

        while not success and attempt < 4:
            try:
                params = {"xissue": xissue, "use_body": False}
                cmd = xml_cmds.addOrUpdateIssueXmlCmd(params)
                cmd.do()
                success = True
            except SolrError:
                attempt += 1
                time.sleep(10)

    def get_metadata_using_citation_meta(
        self,
        xarticle: ArticleData,
        xissue: IssueData,
        soup: BeautifulSoup,
        what: list[CitationLiteral] = [],
    ):
        """
        :param xarticle: the xarticle that will collect the metadata
        :param xissue: the xissue that will collect the publisher
        :param soup: the BeautifulSoup object of tha article page
        :param what: list of citation_ items to collect.
        :return: None. The given article is modified
        """

        if "title" in what:
            # TITLE
            citation_title_node = soup.select_one("meta[name='citation_title']")
            if citation_title_node:
                title = citation_title_node.get("content")
                if isinstance(title, str):
                    xarticle.title_tex = title

        if "author" in what:
            # AUTHORS
            citation_author_nodes = soup.find_all("meta", {"name": "citation_author"})
            for citation_author_node in citation_author_nodes:
                text_author = citation_author_node.get("content")

                author = create_contributor()
                author["role"] = "author"
                author["string_name"] = text_author

                xarticle.contributors.append(author)

        if "pdf" in what:
            # PDF
            citation_pdf_node = soup.select_one('meta[name="citation_pdf_url"]')
            if citation_pdf_node:
                pdf_url = citation_pdf_node.get("content")
                if isinstance(pdf_url, str):
                    add_pdf_link_to_xarticle(xarticle, pdf_url)

        lang = "en"
        if "lang" in what:
            # LANG
            citation_lang_node = soup.find("meta", {"name": "citation_language"})
            if citation_lang_node:
                # TODO: check other language code
                lang = xarticle.lang = citation_lang_node.get("content").strip()[0:2]

        if "abstract" in what:
            # ABSTRACT
            abstract_node = soup.find("div", {"class": "entry-content"})
            if abstract_node is not None:
                abstract_section_node = abstract_node.find("p")
                if abstract_section_node:
                    abstract = str(abstract_section_node)
                    xarticle.abstracts.append(
                        {
                            "tag": "abstract",
                            "value_html": "",
                            "value_tex": abstract,
                            "value_xml": "",
                            "lang": lang,
                        }
                    )

        if "page" in what:
            # PAGES
            citation_fpage_node = soup.find("meta", {"name": "citation_firstpage"})
            if citation_fpage_node:
                page = citation_fpage_node.get("content")
                page = page.split("(")[0]
                if len(page) < 32:
                    xarticle.fpage = page

            citation_lpage_node = soup.find("meta", {"name": "citation_lastpage"})
            if citation_lpage_node:
                page = citation_fpage_node.get("content")
                page = page.split("(")[0]
                if len(page) < 32:
                    xarticle.fpage = page

        if "doi" in what:
            # DOI
            citation_doi_node = soup.find("meta", {"name": "citation_doi"})
            if citation_doi_node:
                doi = citation_doi_node.get("content").strip()
                pos = doi.find("10.")
                if pos > 0:
                    doi = doi[pos:]
                xarticle.doi = doi
                xarticle.pid = doi.replace("/", "_").replace(".", "_").replace("-", "_")

        if "mr" in what:
            # MR
            citation_mr_node = soup.find("meta", {"name": "citation_mr"})
            if citation_mr_node:
                mr = citation_mr_node.get("content").strip()
                if mr.find("MR") == 0:
                    mr = mr[2:]
                    xarticle.extids.append(("mr-item-id", mr))

        if "zbl" in what:
            # ZBL
            citation_zbl_node = soup.find("meta", {"name": "citation_zbl"})
            if citation_zbl_node:
                zbl = citation_zbl_node.get("content").strip()
                if zbl.find("Zbl") == 0:
                    zbl = zbl[3:].strip()
                    xarticle.extids.append(("zbl-item-id", zbl))

        if "publisher" in what:
            # PUBLISHER
            citation_publisher_node = soup.find("meta", {"name": "citation_publisher"})
            if citation_publisher_node:
                pub = citation_publisher_node.get("content").strip()
                if pub != "":
                    xpub = create_publisherdata()
                    xpub.name = pub
                    xissue.publisher = xpub

        if "keywords" in what:
            # KEYWORDS
            citation_kwd_node = soup.find("meta", {"name": "citation_keywords"})
            if citation_kwd_node:
                kwds = citation_kwd_node.get("content").split(",")
                for kwd in kwds:
                    if kwd == "":
                        continue
                    kwd = kwd.strip()
                    xarticle.kwds.append({"type": "", "lang": lang, "value": kwd})

    def create_crawled_bibitem(self, value_xml: str, value_tex: str):
        xref = RefData(lang="en")
        # xref.citation_tex = "".join([e["value_tex"] for e in elements])

        value_xml = f'<mixed-citation xml:space="preserve">{value_xml}</mixed-citation>'
        xref.citation_xml = value_xml
        xref = check_bibitem_xml(xref)

        # Bakes extlink badges into the bibliography html
        # Maybe we should put this into another file (jats_parser ?)
        for extid in xref.extids:
            href = resolve_id(extid[0], extid[1])
            if (not href) or (not xref.citation_html):
                continue
            str_format = extid[0]
            if str_format in extids_formats:
                str_format = extids_formats[str_format]
            xref.citation_html += f" | <a href={href} class='badge bg-secondary rounded-pill ref-badge extid-badge'>{str_format}</a>"

        return xref

    def create_bibliography(self, bibitems: Sequence[RefData]):
        xml_str = "<ref-list>\n"
        html_str = "<div>\n"

        for item in bibitems:
            xml_str += f"\t{item.citation_xml}\n"
            html_str += f"\t<p>{item.citation_html}</p>\n"
        xml_str += "</ref-list>"

        # for item in bibitems:
        #     html_str =
        #     html_str += f"\t<p>{item.citation_html}</p>\n"
        html_str += "</div>"

        tex_str = "<div>\n"
        for item in bibitems:
            tex_str += f"\t<p>{item.citation_tex}</p>\n"
        tex_str += "</div>"

        biblio_dict: AbstractDict = {
            "tag": "biblio",
            "value_html": html_str,
            "value_tex": tex_str,
            "value_xml": xml_str,
            "lang": "en",
        }

        return biblio_dict


def add_pdf_link_to_xarticle(xarticle: ArticleData, pdf_url: str):
    data = {
        "rel": "full-text",
        "mimetype": "application/pdf",
        "location": pdf_url,
        "base": "",
        "text": "Full Text",
    }
    xarticle.streams.append(data)

    # The pdf url is already added as a stream (just above) but might be replaced by a file later on.
    # Keep the pdf url as an Extlink if we want to propose both option:
    # - direct download of a local PDF
    # - URL to the remote PDF
    ext_link = create_extlink(rel="article-pdf", location=pdf_url)
    xarticle.ext_links.append(ext_link)
