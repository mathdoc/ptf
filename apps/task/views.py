import json
from importlib import import_module

from celery import Celery
from celery import current_app
from django_celery_results.models import TaskResult

from django.http import HttpResponse
from django.http import JsonResponse
from django.urls import reverse
from django.views.generic import ListView
from django.views.generic import RedirectView
from django.views.generic import TemplateView
from django.views.generic import View

from history.models import HistoryEvent

#
# base classes / util functions to handle tasks used in Celery
#
# To display a progress bar:
# - an HistoryEvent is needed to store the tasks count.
# - TaskResult are then used to get the status of each tasks
#
# This solution allows you to create a Celery chain:
#    the tasks are created one by one (counting all the TaskResults does not give the total count).
#
# Different task (names) can be used in 1 progress bar.
# For example a task can 1 used at the collection level which creates many issue tasks.
# To monitor the progress, we need to know the names of the TaskResult to track.
# You need to write a get_XXX_task_names() function to return the list of task names

# See gdml/backend/tasks for an example of a chain


def get_messages_in_task_queue():
    app = Celery("task")
    # tasks = list(current_app.tasks)
    tasks = list(sorted(name for name in current_app.tasks if name.startswith("celery")))
    print(tasks)
    # i = app.control.inspect()

    with app.connection_or_acquire() as conn:
        remaining = conn.default_channel.queue_declare(queue="celery", passive=True).message_count
        return remaining


class TaskProgressAPIView(View):
    def get(self, request, *args, **kwargs):
        """
        Returns a JSON object with the progress of the Celery tasks, based on the task name.
        - task_mod is the python module where the get_XXX_task_names function is defined.
          Ex: task_mod="backend.tasks"
        - task_name is the XXX of the get_XXX_task_names function you have to write.
          Ex: task_name="crawl_collection"
        - pid is the id of the HistoryEvent that stores the total number of tasks to perform

        get_XXX_task_names has to return a list of Celery task names to track.
        Ex: ["backend.tasks.crawl_collection_task", "backend.tasks.crawl_issue_task"]
        """
        pid = self.kwargs.get("pid", "")

        task_module_name = self.kwargs.get("task_mod", "")
        module = import_module(task_module_name)
        task_name = self.kwargs.get("task_name", "")
        function_name = f"get_{task_name}_task_names"
        ftor = getattr(module, function_name)
        task_names = ftor()

        def get_event_data():
            # Tasks are typically in the CREATED then SUCCESS or FAILURE state

            # Some messages (in case of many call to <task>.delay) have not been converted to TaskResult yet
            # remaining_messages = get_messages_in_task_queue()

            events = HistoryEvent.objects.filter(pid=pid, status="PENDING")
            all_tasks_count = events.first().data["ids_count"] if events.exists() else 0

            all_tasks = TaskResult.objects.filter(task_name__in=task_names)
            successed_tasks = all_tasks.filter(status="SUCCESS").order_by("-date_done")
            failed_tasks = all_tasks.filter(status="FAILURE")

            # all_tasks_count = all_tasks.count()
            success_count = successed_tasks.count()
            fail_count = failed_tasks.count()

            all_count = all_tasks_count  # + remaining_messages
            remaining_count = all_count - success_count - fail_count

            success_rate = int(success_count * 100 / all_count) if all_count else 0
            error_rate = int(fail_count * 100 / all_count) if all_count else 0
            status = "consuming_queue" if all_count > 0 and remaining_count != 0 else "polling"

            last_task = successed_tasks.first()
            last_task = (
                " : ".join([last_task.date_done.strftime("%Y-%m-%d"), last_task.task_args])
                if last_task
                else ""
            )

            # SSE event format
            event_data = {
                "status": status,
                "success_rate": success_rate,
                "error_rate": error_rate,
                "all_count": all_count,
                "remaining_count": remaining_count,
                "success_count": success_count,
                "fail_count": fail_count,
                "last_task": last_task,
            }

            return event_data

        def stream_response(data):
            # Send initial response headers
            yield f"data: {json.dumps(data)}\n\n"

        data = get_event_data()
        response_format = request.GET.get("format", "stream")
        if response_format == "json":
            response = JsonResponse(data)
        else:
            response = HttpResponse(stream_response(data), content_type="text/event-stream")
        return response


class TasksView(TemplateView):
    template_name = "tasks.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["tasks"] = list(TaskResult.objects.all())
        return context


class TaskFailedListView(ListView):
    model = TaskResult

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.filter(
            status="FAILURE",
            task_name=self.kwargs["task_name"],
        )
        return qs


class TasksDeleteView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        TaskResult.objects.all().delete()

        return reverse("tasks")
