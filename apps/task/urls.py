from task.views import TaskFailedListView
from task.views import TaskProgressAPIView
from task.views import TasksDeleteView
from task.views import TasksView

from django.urls import path

urlpatterns = [
    path("tasks/progress/<path:task_mod>/<path:task_name>/<path:pid>", TaskProgressAPIView.as_view(), name="progress-bar-api"),
    path("tasks/fails/<path:task_name>", TaskFailedListView.as_view(), name="tasks-failed"),
    path("tasks/", TasksView.as_view(), name="tasks"),
    path("tasks/delete/", TasksDeleteView.as_view(), name="tasks-delete"),
]

    #
    # path(
    #     "celery-progress/", include("celery_progress.urls")
    # ),
