CELERY_RESULT_EXTENDED = True
CELERY_CACHE_BACKEND = "default"
CELERY_BROKER_URL = "amqp://guest:guest@localhost:5672//"  # TODO: change in your settings_local.py
CELERY_RESULT_BACKEND = "django-db"
CELERY_TASK_SERIALIZER = "pickle"  # pickle allows to pass Python object
CELERY_RESULT_SERIALIZER = "pickle"
CELERY_ACCEPT_CONTENT = ["pickle", "json"]  # pickle and json have to be set for the broker to work
CELERY_TIMEZONE = "Europe/Paris"
CELERY_ENABLE_UTC = True

CELERY_TASK_TRACK_STARTED = True
CELERY_RESULT_EXTENDED = True
CELERY_CREATE_MISSING_QUEUES = True
CELERY_RESULT_EXPIRES = 24 * 60 * 60  # 1 day in seconds
CELERY_TASK_TRACK_STARTED = True
# CELERY_BROKER_OPTIONS = {
#     "fanout_prefix": True,
#     "fanout_patterns": True,
#     "fanout_domains": ["*"],  # Allow SSE connections from any origin
# }
