function displayProgressBar(success_rate, error_rate) {
    // Show progress bar
    $("#progress-bar").removeClass("hidden");
    $("#task-results").removeClass("hidden");

    // Handle buttons
    $("#btn-task").addClass("disabled");  // Prevent other clicks on the button

    // Update progress bar
    $(".progress-bar-success")
        .css("width", success_rate + "%")
        .text(success_rate + "%");
    $(".progress-bar-warning")
        .css("width", error_rate + "%")
        .text(error_rate + "%");
}

var eventSource;

function connectTaskSSE() {
    // Initiate the SSE connection
    // Note: that even if the server does not send update messages
    // The client will post regular requests to the server to see if there is something to do !?!
    console.log("Inside connectTaskSSE");

    let url_progress = $("#progress-bar").attr("data-url-progress");
    eventSource = new EventSource(url_progress);
    eventSource.addEventListener('message', handleTaskSSEMessage);
    eventSource.addEventListener('error', handleTaskSSEConnectionError);
    console.log("Connection opened.");
}

function disconnectTaskSSE() {
    if (eventSource) {
        eventSource.close();
        eventSource = null;
    }
    console.log("Connection closed.");
}

function handleTaskSSEConnectionError(event) {
  console.log("handleTaskSSEConnectionError", event);

  if (eventSource.readyState === EventSource.CLOSED) {
    // The connection has been closed, reopen it.
    connectTaskSSE();
  }
}

function handleTaskSSEMessage(event) {
    console.log("Connection message received.");
    var result = JSON.parse(event.data);
    updateProgressBar(result);
}

function checkProgress() {
    // If you reload the web page, we need to check if tasks are still being processed
    let url_progress = $("#progress-bar").attr("data-url-progress") + "?format=json";

    console.log("Inside checkProgress");

    $.ajax({
        url: url_progress,
        success: function (result) {
            if (result.status == 'consuming_queue') {
                updateProgressBar(result);
                connectTaskSSE();
            }
        }
    });
}

function updateProgressBar(result) {
    let report = "Archive status<br>Errors : " + result.fail_count;
    report += "<br>Successes : " + result.success_count;
    report += "<br>Total : " + result.all_count;

    if (result.status == "consuming_queue") {
        report += "<br>Remaining tasks : " + result.remaining_count;
        if (result.last_task != '') {
            report += "<br>Last Task : " + result.last_task;
        }
    }
    $("#results").html(report);

    if (result.status == "consuming_queue") {
        // Archiving in progress
        displayProgressBar(result.success_rate, result.error_rate);
    } else {
        $("#progress-bar").addClass("hidden");
        if (result.fail_count != 0) {
        }
        $("#btn-task").removeClass("disabled");
        disconnectTaskSSE();
    }
}

$(document).ready(function () {
    $("#btn-task").click(function () {
        displayProgressBar(0, 0);
        connectTaskSSE();
    });

    checkProgress();
});
