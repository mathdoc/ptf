from django.core.management.base import BaseCommand

from history import models


class Command(BaseCommand):
    help = "Import sample history events"

    def handle(self, *args, **options):
        models.test_create()
