import json
from argparse import RawTextHelpFormatter

from django.core.management.base import BaseCommand

from history import models


class Command(BaseCommand):
    help = (
        "Save a record in history (geodesic)\n"
        "format JSON : {\n"
        '"type": " - string -",\n'
        '"pid": pid,\n'
        '"col": colid,\n'
        '"status": "ERROR"||"WARNING"||"OK",\n'
        '"data": {\n'
        '    "ids_count": 0,\n'
        '    "message": message\n'
        "    }}"
    )

    def create_parser(self, *args, **kwargs):
        parser = super().create_parser(*args, **kwargs)
        parser.formatter_class = RawTextHelpFormatter
        return parser

    def add_arguments(self, parser):
        parser.add_argument(
            "--file",
            dest="file",
            default=False,
            help="Save from a JSON file",
            type=str,
            required=True,
        )

    def handle(self, *args, **options):
        if options["file"]:
            file = options["file"]
            with open(file) as f:
                json_data = json.load(f)
                models.insert_history_event(
                    json_data,
                )
