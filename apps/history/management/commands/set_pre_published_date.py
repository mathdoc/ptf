from django.core.management.base import BaseCommand

from history.models import HistoryEvent
from ptf import model_helpers


class Command(BaseCommand):
    help = "Import sample history events"

    def handle(self, *args, **options):
        for colid in ["CRMATH", "CRCHIM", "CRMECA", "CRPHYS", "CRBIOL", "CRGEOS"]:
            collection = model_helpers.get_collection(colid)
            for issue in collection.content.all():
                qs = HistoryEvent.objects.filter(
                    type="deploy", pid=issue.pid, status="OK"
                ).order_by("created_on")
                events = [h for h in qs if h.data["message"] == "test_website"]

                if len(events) > 0:
                    h = events[0]

                    for article in issue.article_set.all():
                        if article.date_pre_published is None:
                            print(article.pid)
                            article.date_pre_published = h.created_on
                            article.save()
