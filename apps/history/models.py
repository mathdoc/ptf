import datetime
from datetime import timedelta
from itertools import tee

from django.contrib.auth.models import User
from django.db import models
from django.db.models import JSONField
from django.db.models import Q
from django.db.models.functions import TruncMonth
from django.utils import timezone

from ptf.models import Collection


class HistoryEventQuerySet(models.QuerySet):
    def get_stale_events(self):
        distinct_pids = self.order_by("pid", "type").distinct("pid", "type")
        latest_events = [
            self.filter(pid=event.pid, type=event.type).latest("created_on").pk
            for event in distinct_pids
        ]
        return self.exclude(pk__in=latest_events)

    def get_last_unsolved_error(self, pid, strict):
        if strict:
            result = self.filter(status="ERROR", pid=pid).latest("created_on")
        else:
            result = self.filter(status="ERROR", pid__startswith=pid).latest("created_on")
        if "obsolete" in result.data:
            raise HistoryEvent.DoesNotExist
        return result


class HistoryEvent(models.Model):
    created_on = models.DateTimeField(db_index=True, default=timezone.now)
    type = models.CharField(max_length=200, db_index=True)
    pid = models.CharField(max_length=1024, db_index=True, default="")
    col = models.CharField(max_length=1024, db_index=True, default="")
    source = models.CharField(max_length=1024, db_index=True, default="", null=True, blank=True)
    status = models.CharField(max_length=10, db_index=True, default="OK")
    title = models.CharField(max_length=1024, db_index=True, default="")
    type_error = models.CharField(max_length=1024, db_index=True, default="")
    user = models.ForeignKey(User, models.SET_NULL, blank=True, null=True)
    data = JSONField(default=dict)
    objects = HistoryEventQuerySet.as_manager()
    unique_id = models.CharField(
        max_length=64, verbose_name="Reference unique id parent", null=True, blank=True
    )
    parent = models.ForeignKey(
        "self", on_delete=models.CASCADE, null=True, blank=True, related_name="ancestors"
    )

    def __str__(self):
        return f"{self.pid}:{self.type} - {self.created_on}"

    def is_expandable(self):
        result = (
            ("ids_count" in self.data and self.data["ids_count"] > 1)
            or ("message" in self.data and len(self.data["message"]) > 0)
            or ("issues" in self.data and len(self.data["issues"]) > 0)
        )

        return result


def insert_history_event(new_event):
    """
    :param new_event:
    :return:
    """
    if "type" not in new_event or "pid" not in new_event or "col" not in new_event:
        raise ValueError("type, pid or col is nor set")
    do_insert = True

    # has_child = False

    last_events = HistoryEvent.objects.all().order_by("-pk")[:1]
    last_event = None
    if new_event["pid"] != "":
        last_event = last_events[0] if len(last_events) > 0 else None
        events_pid = HistoryEvent.objects.all().filter(Q(pid=new_event["pid"])).order_by("-pk")[:1]
        if events_pid:
            last_event = events_pid[0]

    if "source" in new_event:
        if new_event["source"] != "":
            last_event = last_events[0] if len(last_events) > 0 else None
            events_pid = (
                HistoryEvent.objects.all()
                .filter(Q(source=new_event["source"]))
                .order_by("-pk")[:1]
            )
            if events_pid:
                last_event = events_pid[0]

    if "unique_id" in new_event:
        if not isinstance(new_event["unique_id"], type(None)):
            evt = HistoryEvent.objects.filter(unique_id=new_event["unique_id"])

            if len(evt) > 0:
                last_event = evt[0]

    if "parent_id" in new_event:
        parent_evt = HistoryEvent.objects.filter(pk=new_event["parent_id"])

        if len(parent_evt) > 0:
            prev_events = HistoryEvent.objects.filter(
                parent=new_event["parent_id"],
                pid=new_event["pid"],
            )
            if len(prev_events) > 0:
                last_event = prev_events.first()
            else:
                last_event = None

    if not isinstance(last_event, type(None)):
        time_delta = timezone.now() - last_event.created_on
        delta = timedelta(seconds=5)
        #  Check if we have to merge with the last event in the history
        # Match 1 article generates many events per bibitemid
        if (
            (
                last_event.type == new_event["type"]
                and last_event.pid == new_event["pid"]
                # and time_delta < timedelta(seconds=20)
            )
            or
            # Deploy, Archive, Import a collection
            (
                last_event.type == new_event["type"]
                and last_event.pid == new_event["col"]
                and time_delta < delta
            )
            or
            # Typically used when Edit one article (mark matched ids as valid)
            (
                last_event.type == new_event["type"]
                and last_event.pid == new_event["pid"]
                and last_event.status == new_event["status"]
            )
        ):
            do_insert = False

            new_fields = {}

            # Downgrade the status
            if "status" in new_event and (
                last_event.status == "OK"
                or (last_event.status == "WARNING" and new_event["status"] == "ERROR")
                or new_event["status"] == "ERROR"
            ):
                last_event.status = new_event["status"]
            if "status" in new_event and (new_event["type"] == "import") and not do_insert:
                last_event.status = new_event["status"]

            last_event_data = last_event.data
            new_event_data = new_event["data"] if "data" in new_event else None

            if last_event.pid == new_event["pid"]:
                # Merge ids
                if new_event_data and "ids" in new_event["data"]:
                    if "ids" in last_event_data:
                        new_fields["ids"] = last_event_data["ids"] + new_event_data["ids"]
                    else:
                        new_fields["ids"] = new_event_data["ids"]

            elif new_event_data:
                if "articles" in last_event_data and "ids" in new_event_data:
                    # Article inside issue
                    # The last event was for an issue (ex: Matching) and
                    # The new event is for an article.
                    # last_event_data has 'articles' and new_even_data has 'ids'
                    found_articles = [
                        item
                        for item in last_event_data["articles"]
                        if item["pid"] == new_event["pid"]
                    ]
                    if len(found_articles) > 0:
                        article = found_articles[0]
                        article["ids"] = article["ids"] + new_event_data["ids"]
                    else:
                        last_event_data["articles"].append(
                            {"pid": new_event["pid"], "ids": new_event_data["ids"]}
                        )
                elif "ids" in new_event_data:
                    last_event_data["articles"] = [
                        {"pid": new_event["pid"], "ids": new_event_data["ids"]}
                    ]
                if "articles" in last_event_data:
                    new_fields["articles"] = last_event_data["articles"]

            # Merge *data['articles']
            if new_event_data and "articles" in new_event_data:
                if "articles" in last_event_data:
                    articles = last_event_data["articles"]
                    for new_article in new_event_data["articles"]:
                        found_articles = [
                            item for item in articles if item["pid"] == new_article["pid"]
                        ]
                        if len(found_articles) > 0:
                            article = found_articles[0]
                            article["ids"] = article["ids"] + new_article["ids"]
                        else:
                            articles.append(new_article)
                else:
                    last_event_data["articles"] = new_event_data["articles"]
                new_fields["articles"] = last_event_data["articles"]

            # Update ids_count
            # TODO remove update
            if new_event_data and "ids_count" in new_event_data:
                new_count = 0
                if "ids_count" in last_event_data:
                    new_count = last_event_data["ids_count"]
                new_count = new_event_data["ids_count"]
                new_fields["ids_count"] = new_count

            # Deploy, Archive, Import a collection
            # Add 'issues' to the collection event
            if last_event.type == new_event["type"] and last_event.pid == new_event["col"]:
                if "issues" in last_event_data:
                    if new_event["pid"] not in last_event_data["issues"]:
                        last_event_data["issues"].append(new_event["pid"])
                else:
                    last_event_data["issues"] = [new_event["pid"]]
                new_fields["issues"] = last_event_data["issues"]

            if (
                new_event_data
                and "message" in new_event_data
                and len(new_event_data["message"]) > 0
            ):
                if (
                    False
                    and "message" in last_event_data
                    and new_event["type"] == "edit"
                    and last_event_data["message"] != ""
                ):
                    new_fields["message"] = (
                        new_event_data["message"] + "<br/>" + last_event_data["message"]
                    )
                else:
                    new_fields["message"] = new_event_data["message"]

            for key, value in new_fields.items():
                last_event.data[key] = value

            if "created_on" in new_event:
                last_event.created_on = new_event["created_on"]
            else:
                last_event.created_on = timezone.now()

            if "type_error" in new_event:
                last_event.type_error = new_event["type_error"]

            if "unique_id" in new_event:
                last_event.unique_id = new_event["unique_id"]

            if "status" in new_event:
                last_event.status = new_event["status"]

            last_event.save()

    if do_insert:
        event = HistoryEvent(type=new_event["type"], pid=new_event["pid"], col=new_event["col"])

        if "unique_id" in new_event:
            event.unique_id = new_event["unique_id"]

        if "parent_id" in new_event:
            parent_evt = HistoryEvent.objects.filter(pk=new_event["parent_id"])

            if len(parent_evt) > 0:
                event.parent = parent_evt[0]

        if "message" in new_event:
            event.message = new_event["message"]
        if "created_on" in new_event:
            event.created_on = new_event["created_on"]
        if "status" in new_event:
            event.status = new_event["status"]
        if "data" in new_event:
            event.data = new_event["data"]
        if "source" in new_event:
            event.source = new_event["source"]
        if "userid" in new_event:
            if new_event["userid"] is not None:
                event.user = User.objects.get(pk=new_event["userid"])
        if "title" in new_event:
            event.title = new_event["title"]
        if "type" in new_event:
            event.type = new_event["type"]
        if "type_error" in new_event:
            event.type_error = new_event["type_error"]
        if "created_on" in new_event:
            event.type_error = new_event["created_on"]

        if new_event["status"] != "OK":
            event.save()
        if "parent_id" in new_event:
            if new_event["parent_id"] == "":
                event.save()

        if "parent_id" not in new_event:
            event.save()


# Attempt to create a generator to group events.
# See https://docs.python.org/2/library/itertools.html#itertools.groupby
# It works fine in the model, the view (and the tests)
# Unfortunately, it does not work with the template.
# Django's render function converts the iterator into a list:
# See django/template/defaulttags.py, line 172:
#    if not hasattr(values, '__len__'):
#        values = list(values)
#    len_values = len(values)
# The generator is completely traversed in list(values) and is not reset
# When the render arrives in the for loop, there is nothing left to iterate
class GroupEventsBy:
    # [k for k, g in groupby('AAAABBBCCDAABBB')] --> A B C D A B
    # [list(g) for k, g in groupby('AAAABBBCCD')] --> AAAA BBB CC D
    def __init__(self, iterable, key=None):
        if key is None:
            key = lambda x: x
        self.keyfunc = key
        self.it = iter(iterable)
        self.tgtkey = self.currkey = self.currvalue = object()
        self.error_count = 0

    def __iter__(self):
        return self

    def __next__(self):
        while self.currkey == self.tgtkey:
            self.currvalue = next(self.it)  # Exit on StopIteration
            self.currkey = self.keyfunc(self.currvalue)
        self.tgtkey = self.currkey

        # make a copy of the iterator to fetch counts
        self.it, second_it = tee(self.it)
        value_in_gp = self.currvalue
        count = error_count = warning_count = 0
        gp_key = self.tgtkey
        while self.tgtkey == gp_key:
            count += 1
            if value_in_gp.status == "ERROR":
                error_count += 1
            if value_in_gp.status == "WARNING":
                warning_count += 1
            try:
                value_in_gp = next(second_it)
                gp_key = self.keyfunc(value_in_gp)
            except StopIteration:
                gp_key = object()

        return (self.currkey, count, error_count, warning_count, self._grouper(self.tgtkey))

    def _grouper(self, tgtkey):
        while self.currkey == tgtkey:
            yield self.currvalue
            self.currvalue = next(self.it)  # Exit on StopIteration
            self.currkey = self.keyfunc(self.currvalue)


def get_history_events(filters):
    data = HistoryEvent.objects.all()

    filter_by_month = True
    kwargs = {}
    for key, value in filters.items():
        if key == "status":
            if value == "error":
                kwargs["status"] = "ERROR"
            if value == "success":
                kwargs["status"] = "OK"
            else:
                data = data.filter(Q(status="ERROR") | Q(status="WARNING"))
        elif key == "month":
            if value == "all":
                filter_by_month = False
        else:
            kwargs[key] = value

    if filter_by_month:
        today = datetime.datetime.today()
        kwargs["created_on__year"] = today.year
        kwargs["created_on__month"] = today.month

    data = data.filter(**kwargs).order_by("-pk").annotate(month=TruncMonth("created_on"))
    # grouped_events = GroupEventsBy(data, lambda t: t.month)
    grouped_events = []
    events_in_month = []
    curmonth = None
    error_count = warning_count = 0
    for event in data:
        month = event.month
        if month != curmonth and len(events_in_month) > 0:
            grouped_events.append(
                {
                    "month": curmonth,
                    "error_count": error_count,
                    "warning_count": warning_count,
                    "events_in_month": events_in_month,
                }
            )
            events_in_month = []
            curmonth = month
            error_count = warning_count = 0
        elif month != curmonth:
            curmonth = month
        events_in_month.append(event)
        is_obsolete = "obsolete" in event.data and event.data["obsolete"]
        if event.status == "ERROR" and not is_obsolete:
            error_count += 1
        if event.status == "WARNING" and not is_obsolete:
            warning_count += 1
    if len(events_in_month) > 0:
        grouped_events.append(
            {
                "month": curmonth,
                "error_count": error_count,
                "warning_count": warning_count,
                "events_in_month": events_in_month,
            }
        )

    return grouped_events

    # .values('month') #.annotate(count=Count('id'))

    # https://stackoverflow.com/questions/8746014/django-group-by-date-day-month-year
    # data = test1.objects.annotate(month=TruncMonth('cdate')).values('month').annotate(c=Count('id')).order_by()

    # https://docs.djangoproject.com/en/2.1/topics/db/aggregation/#values


def get_history_error_warning_counts():
    # .exclude(data__obsolete=True) or ~Q(data__obsolete=True) do not work in Django 1.11
    # Need to count manually

    error_count = warning_count = 0

    events = HistoryEvent.objects.filter(status="ERROR")
    for event in events:
        if "obsolete" not in event.data or not event.data["obsolete"]:
            error_count += 1

    events = HistoryEvent.objects.filter(status="WARNING")
    for event in events:
        if "obsolete" not in event.data or not event.data["obsolete"]:
            warning_count += 1

    return error_count, warning_count


def delete_history_event(pk):
    HistoryEvent.objects.get(pk=pk).delete()


def get_history_last_event_by(type, pid=""):
    last_events = HistoryEvent.objects.filter(type=type)
    if len(pid) > 0:
        last_events = last_events.filter(pid__startswith=pid)
    last_events = last_events.order_by("-pk")[:1]

    last_event = last_events[0] if len(last_events) > 0 else None
    return last_event


def get_last_event_by_error(type, pid=""):
    last_events = HistoryEvent.objects.filter(type=type, status="ERROR")
    if len(pid) > 0:
        last_events = last_events.filter(pid__startswith=pid)
    last_events = last_events.order_by("-pk")[:1]

    last_event = last_events[0] if len(last_events) > 0 else None
    return last_event


def get_gap(now, event):
    gap = ""
    if event:
        timelapse = now - event.created_on
        if timelapse.days > 0:
            gap = str(timelapse.days) + " days ago"
        elif timelapse.seconds > 3600:
            gap = str(int(timelapse.seconds // 3600)) + " hours ago"
        else:
            gap = str(int(timelapse.seconds // 60)) + " minutes ago"
    return gap


def get_last_error_of_colid(colid):
    last_event = HistoryEvent.objects.filter(col=colid, status="ERROR").last()
    return last_event


def get_last_error_of_collection(col):
    return get_last_error_of_colid(col.pid)


Collection.get_last_history_events = get_last_error_of_collection


def test_create():
    """HistoryEvent.objects.all().delete()"""
    insert_history_event(
        {
            "type": "import",
            "unique_id": "43542532-3e3d-4366-9200-0dfc77233084",
            "pid": "",
            "col": "MR",
            "status": "ERROR",
            "title": "Μαθηματική Επιθεώρηση",
            "type_error": "",
            "userid": 1,
            "data": {
                "ids_count": 0,
                "message": "",
                "target": "",
            },
        }
    )

    """
    insert_history_event(
        {
            "type": "matching",
            "pid": "AIF_2015__65_6_2331_0",
            "col": "AIF",
            "status": "WARNING",
            "created_on": datetime.datetime(2018, 12, 10, tzinfo=datetime.UTC),
            "data": {
                "message": "MR ne répond pas",
                "ids_count": 2,
                "ids": [
                    {"type": "zbl", "id": "zbl1", "seq": 15},
                    {"type": "zbl", "id": "zbl2", "seq": 22},
                ],
            },
        }
    )

    insert_history_event(
        {
            "type": "matching",
            "pid": "AIF_2015__65_6_2331_0",
            "col": "AIF",
            "status": "OK",
            "created_on": datetime.datetime(2018, 12, 11, 8, tzinfo=datetime.UTC),
            "data": {"ids_count": 1, "ids": [{"type": "mr", "id": "mr1", "seq": 7}]},
        }
    )

    insert_history_event(
        {
            "type": "edit",
            "pid": "AIF_2015__65_6_2331_0",
            "col": "AIF",
            "status": "OK",
            "created_on": datetime.datetime(2018, 12, 11, 17, tzinfo=datetime.UTC),
            "data": {"message": "MR:mr12 checked"},
        }
    )

    insert_history_event(
        {
            "type": "edit",
            "pid": "AIF_2009__59_7_2593_0",
            "col": "AIF",
            "status": "OK",
            "created_on": datetime.datetime(2018, 12, 12, 10, tzinfo=datetime.UTC),
            "data": {"message": "[2] zbl:zbl4 false_positive"},
        }
    )

    insert_history_event(
        {
            "type": "edit",
            "pid": "AIF_2009__59_7_2593_0",
            "col": "AIF",
            "status": "OK",
            "created_on": datetime.datetime(2018, 12, 12, 10, tzinfo=datetime.UTC),
            "data": {"message": "[7] zbl:zbl8 checked"},
        }
    )

    insert_history_event(
        {
            "type": "edit",
            "pid": "AIF_2009__59_7_2700_0",
            "col": "AIF",
            "status": "OK",
            "created_on": datetime.datetime(2018, 12, 12, 11, tzinfo=datetime.UTC),
            "data": {"message": "[3] zbl:zbl5 checked"},
        }
    )

    insert_history_event(
        {
            "type": "edit",
            "pid": "AIF_2009__59_7_2611_0",
            "col": "AIF",
            "status": "OK",
            "created_on": datetime.datetime(2018, 12, 12, 15, tzinfo=datetime.UTC),
            "data": {"message": "All ids checked"},
        }
    )

    insert_history_event(
        {
            "type": "matching",
            "pid": "AIF_2007__57_7",
            "col": "AIF",
            "status": "ERROR",
            "created_on": datetime.datetime(2019, 1, 4, tzinfo=datetime.UTC),
            "data": {
                "message": "Internal Error",
                "ids_count": 3,
                "articles": [
                    {
                        "pid": "AIF_2007__57_7_2143_0",
                        "ids": [{"type": "zbl", "id": "zbl3", "seq": 5}],
                    },
                    {
                        "pid": "AIF_2007__57_7_2143_0",
                        "ids": [
                            {"type": "zbl", "id": "zbl4", "seq": 8},
                            {"type": "mr", "id": "mr2", "seq": 12},
                        ],
                    },
                ],
            },
        }
    )

    insert_history_event(
        {
            "type": "edit",
            "pid": "AIF_2007__57_7",
            "col": "AIF",
            "status": "OK",
            "created_on": datetime.datetime(2019, 1, 5, tzinfo=datetime.UTC),
            "data": {"message": "All ids checked"},
        }
    )

    insert_history_event(
        {
            "type": "edit",
            "pid": "JEP_2014__1_7_352_0",
            "col": "JEP",
            "status": "OK",
            "data": {"message": "Zbl:zbl8 checked"},
        }
    )

    insert_history_event({"type": "deploy", "col": "ALCO", "pid": "ALCO"})
    insert_history_event({"type": "deploy", "col": "ALCO", "pid": "ALCO_2018__1_1"})
    insert_history_event({"type": "deploy", "col": "ALCO", "pid": "ALCO_2018__1_2"})
    insert_history_event({"type": "deploy", "col": "ALCO", "pid": "ALCO_2018__1_3"})
    insert_history_event({"type": "deploy", "col": "ALCO", "pid": "ALCO_2018__1_4"})
    insert_history_event({"type": "clockss", "pid": "ALL", "col": "ALL"})
"""
