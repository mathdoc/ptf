# Generated by Django 3.2.20 on 2023-09-14 14:43

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('history', '0009_historyevent_type_erreur'),
    ]

    operations = [
        migrations.RenameField(
            model_name='historyevent',
            old_name='titre_collection',
            new_name='title_collection',
        ),
        migrations.RenameField(
            model_name='historyevent',
            old_name='type_erreur',
            new_name='type_error',
        ),
    ]
