function displayProgressBar() {
    $('#importing-progress').removeClass('hidden')
}

function updateProgressBar(progress, error_rate) {
    $('.progress-bar-success').css('width', progress+'%').text(progress+'%');
    $('.progress-bar-warning').css('width', error_rate+'%').text(error_rate+'%');
}

function updateReport(result) {
    remaining = "Remaining tasks : " + result.remaining
    fails = "<br>Errors : " + result.fails
    successes = "<br>Successes : " + result.successes
    last_task = "<br>Last Task : " + result.last_task
    return remaining + fails + successes + last_task
}

progressbar=setInterval(GetProgress, 1000);


function GetProgress() {
console.log("progress");

    $.ajax({
        url: '/dashboard/import-issues/progress/',
        success: function (result) {
            progress = result.progress
            error_rate = result.error_rate
            total = result.total
            $('#results').html(updateReport(result))
            /* Archiving in progress */
            if (result.status == 'consuming_queue') {
                $('#polling').addClass('hidden')
                $('#results').removeClass('hidden')
                if (progress != 100) {
                    displayProgressBar()
                    $('.progress').removeClass('hidden')
                    if(result.fails > 0) {
                      $('.progress-error').removeClass('hidden')
                    }
                    updateProgressBar(progress, error_rate)
                }
                if(total == 0) {
                    $('#importing-progress').addClass('hidden')
                    $('#results').addClass('hidden')

                }
            }
            /* Archiving complete */
            if (progress == 100) {
                updateProgressBar(progress, error_rate)
                clearInterval(progressbar);
                displayProgressBar()
                $('#results').removeClass('hidden')
                $('.progress').removeClass('hidden')

                /* celery archiving button is usable again */
                if (result.fails != 0) {
                    $('#import-errors-btn').removeClass('hidden')
                }
                $('#celery-importing').removeClass('disabled')
            /* Polling all issues to archive */
            } else if (result.status == 'polling') {
                if (total == 0) {
                    $('#celery-importing').removeClass('disabled')
                } else {
                    displayProgressBar()
                    $('#polling').removeClass('hidden')
                }
                $('#results').addClass('hidden')
            }
        },
        error: function (err) {
        console.log(err);
        }
    });
}

