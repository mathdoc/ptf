from django.urls import path

from history.views import HistoryClearView
from history.views import HistoryEventDeleteView
from history.views import HistoryEventUpdateView
from history.views import HistoryView

urlpatterns = [
    path(r'^history/$', HistoryView.as_view(), name='history'),
    path('history/clear/', HistoryClearView.as_view(), name='history-clear'),
    path('ĥistory/<int:pk>/delete/', HistoryEventDeleteView.as_view(), name='history-delete'),
]
