from django.conf.urls.i18n import i18n_patterns
from django.urls import include
from django.urls import path
from django.urls import re_path
from django.views.decorators.cache import cache_page
from django.views.generic import TemplateView

from .views import AllIssuesAPIView
from .views import APIFetchAllView
from .views import APIFetchId
from .views import APIFetchView
from .views import APILookupView
from .views import APIMatchAllView
from .views import APIMatchOneView
from .views import ArticleCitedByView
from .views import ArticleDumpAPIView
from .views import ArticlesAPIView
from .views import ArticleView
from .views import BookDumpAPIView
from .views import CollectionExportCSV
from .views import CollectionIssnAPIView
from .views import CollectionsAPIView
from .views import ContainerView
from .views import ExportArticleHtml
from .views import HelpView
from .views import IssueListAPIView
from .views import IssuesAPIView
from .views import IssuesView
from .views import ItemFileListAPIView
from .views import ItemView
from .views import ItemXMLView
from .views import LatestArticlesFeed
from .views import LatestIssue
from .views import MoreLikeThisView
from .views import RecentArticlesPublished
from .views import RSSTemplate
from .views import SearchView
from .views import UpdateBibItemIdView
from .views import UpdateExtIdView
from .views import UpdateMatchingView
from .views import VolumeDetailView
from .views import VolumeGeneralDetailView
from .views import actas
from .views import authors
from .views import books
from .views import citations
from .views import export_citation
from .views import get_binary_file
from .views import graphical_abstract
from .views import home
from .views import journals
from .views import proceedings
from .views import set_formula_display
from .views import sorted_books
from .views import sorted_lectures
from .views import thesis
from .views import update_suggest

urlpatterns_protectable_by_account = [
    path("", home, name="home"),
    path("i18n/", include("django.conf.urls.i18n")),
    path("help/", HelpView.as_view(), name="help"),
    path("set-formula-display/", set_formula_display, name="set_formula_display"),
    re_path(
        r"^robots\.txt$",
        TemplateView.as_view(template_name="robots.txt", content_type="text/plain"),
    ),
    path("rss/", RSSTemplate.as_view(), name="syndication-feed"),
    path("actas/", actas, name="actas"),
    path("actas/<path:jid>/", IssuesView.as_view(), name="acta-issues"),
    re_path(r"^authors/*$", authors, name="authors"),
    path("authors/<path:query>/", authors, name="pretty_authors"),
    path("articles/<path:aid>/citations/", citations, name="article-citations"),  # TODO XSL
    re_path(
        r"^articles/(?P<lang>[a-z]{2})/(?P<aid>.+)/$", ArticleView.as_view(), name="article-lang"
    ),
    path("articles/<path:aid>/", ArticleView.as_view(), name="article"),
    path("books/", books, name="books"),
    path("books/<path:aid>/citations/", citations, name="book-citations"),  # TODO XSL
    path("books/<path:pid>/", ContainerView.as_view(), name="book-toc"),
    path("book-part/<path:aid>/", ArticleView.as_view(), name="book-part"),
    path("issues/<path:pid>/", ContainerView.as_view(), name="issue-items"),
    path("item/", ItemView.as_view(), name="item"),  # Compatibility with Numdam v1
    path("item/<path:pid>/latest_issue/", LatestIssue.as_view(), name="latest_issue"),
    path("item/<path:pid>/", ItemView.as_view(), name="item_id"),
    path("journals/", journals, name="journals"),
    path("journals/<path:pid>/latest_issue/", LatestIssue.as_view(), name="journal_latest_issue"),
    path("journals/<path:jid>/", IssuesView.as_view(), name="journal-issues"),
    path("search/<path:query>/", SearchView.as_view(), {"path": "/search"}, name="pretty_search"),
    re_path(r"^search/?$", SearchView.as_view(), {"path": "search", "query": ""}, name="search"),
    re_path(r"^series/*$", sorted_books, name="book-series"),
    path("series/<path:query>/", sorted_books, name="pretty-series"),
    re_path(r"^lectures/*$", sorted_lectures, name="lectures"),
    # path("lectures/<path:pid>/", ContainerView.as_view(), name="lectures-issues"),
    re_path(r"^thesis/*$", thesis, name="thesis"),
    # path("thesis/<path:pid>/", ContainerView.as_view(), name="thesis-issues"),
    path("volume/<path:vid>/", VolumeDetailView.as_view(), name="volume-items"),
    path(
        "volume_general/<path:vid>/",
        VolumeGeneralDetailView.as_view(),
        name="volume-general-items",
    ),
    path("proceedings/", proceedings, name="proceedings"),
    path("proceedings/<path:jid>/", IssuesView.as_view(), name="proceeding-issues"),
    #################################################
    #
    # File server
    #
    #################################################
    re_path(
        r"^issue/(?P<binary_file_type>.+)/(?P<pid>[:\(\)\./\w-]+|/{0,1}?)\.(?P<extension>pdf|djvu)$",
        get_binary_file,
        {"relative_path": ""},
        name="issue-relatedobject-pdf",
    ),
    re_path(
        r"^article/(?P<binary_file_type>[a-zA-Z]+)/(?P<pid>[:\(\)\./\w-]+|/{0,1}?)\.(?P<extension>pdf|djvu)$",
        get_binary_file,
        {"relative_path": ""},
        name="article-translated-pdf",
    ),
    re_path(
        r"^article/(?P<pid>[:\(\)\./\w%-]+|/{0,1}?)/file/(?P<relative_path>.+)$",
        get_binary_file,
        {"binary_file_type": None, "extension": ""},
        name="article-binary-files",
    ),
    re_path(
        r"^item/(?P<pid>[:\(\)\./\w%-]+|/{0,1}?)\.(?P<extension>pdf|djvu)$",
        get_binary_file,
        {"binary_file_type": None, "relative_path": ""},
        name="item-pdf",
    ),
    # Compatibility with urls inside SolR
    re_path(
        r"^article/(?P<pid>[:\(\)\./\w-]+|/{0,1}?)\.(?P<extension>pdf|djvu)$",
        get_binary_file,
        {"binary_file_type": None, "relative_path": ""},
        name="item-pdf-solr",
    ),
    re_path(
        r"^issue/(?P<pid>[:\(\)\./\w-]+|/{0,1}?)\.(?P<extension>pdf|djvu)$",
        get_binary_file,
        {"binary_file_type": None, "relative_path": ""},
        name="issue-pdf-solr",
    ),
    re_path(
        r"^article/(?P<pid>[\w-]+)/tex/(?P<relative_path>.+)$",
        get_binary_file,
        {"binary_file_type": None, "extension": ""},
        name="article-binary-files-solr",
    ),
    # Compatibility with urls to images inside FullText
    re_path(
        r"^article/(?P<pid>[\w-]+)/(?P<extension>png|jpg)/(?P<relative_path>.+)$",
        get_binary_file,
        {"binary_file_type": None},
        name="article-image-files",
    ),
    re_path(
        r"^citedby/(?P<doi>10[.][0-9]{4,}.+)$",
        cache_page(60 * 60 * 24 * 7)(ArticleCitedByView.as_view()),
        name="api_citedby",
    ),
    re_path(
        r"^export_citation/(?P<pid>[:\(\)\./\w-]+|/{0,1}?)/(?P<ext>.+)/$",
        export_citation,
        name="export_citation",
    ),
    re_path(r"^mlt/(?P<doi>10[.][0-9]{4,}.+)/$", MoreLikeThisView.as_view(), name="mlt_article"),
    re_path(
        r"^export_article_html/(?P<doi>.+)/tokenize=(?P<tokenize>[0-1]+)",
        ExportArticleHtml.as_view(),
        name="export_article_token",
    ),
    re_path(
        r"^export_article_html/(?P<doi>.+)",
        ExportArticleHtml.as_view(),
        name="export_article_html",
    ),
]

urlpatterns_protectable_by_ip = [
    path("api/lookup/", APILookupView.as_view(), name="api-lookup"),
    path("api/fetch/", APIFetchView.as_view(), name="api-fetch"),
    path(
        "api/collection/issn/<str:issn>/",
        CollectionIssnAPIView.as_view(),
        name="api_collection_issn",
    ),
    path(
        "api-collection-dump/<path:colid>/",
        CollectionExportCSV.as_view(),
        name="api_collection_export_csv",
    ),
    path("api-all-issues/", AllIssuesAPIView.as_view(), name="api-all-issues"),
    path("api-all-collections/", CollectionsAPIView.as_view(), name="api-all-collections"),
    path("api-articles/", ArticlesAPIView.as_view(), name="api-articles"),
    path("api-article-dump/<path:pid>/", ArticleDumpAPIView.as_view(), name="api-article-dump"),
    path("api-book-dump/<path:pid>/", BookDumpAPIView.as_view(), name="api-book-dump"),
    path("api-issues/<path:colid>/", IssuesAPIView.as_view(), name="api-issues"),
    path("api-issue-list/<path:pid>/", IssueListAPIView.as_view(), name="api-issue-list"),
    path("api-item-xml/<path:pid>/", ItemXMLView.as_view(), name="api-item-xml"),
    path(
        "api-item-file-list/<path:pid>/", ItemFileListAPIView.as_view(), name="api-item-file-list"
    ),
    path("api-articles-recents/", RecentArticlesPublished.as_view(), name="api-articles-recents"),
    re_path(
        r"^api-update-suggest/(?P<doi>10[.][0-9]{4,}.+)/$",
        update_suggest,
        name="api-update-suggest",
    ),
    re_path(
        r"^api-graphical-abstract/(?P<doi>10[.][0-9]{4,}.+)/$",
        graphical_abstract,
        name="api-graphical-abstract",
    ),
    path("upload/", include("upload.urls")),
]

ptf_i18n_patterns = i18n_patterns(
    re_path(r'^latest/feed/(?P<name>[\w]+)/$', LatestArticlesFeed(), name='latest-articles-feed'),
)

urlpatterns = []
urlpatterns += urlpatterns_protectable_by_account
urlpatterns += urlpatterns_protectable_by_ip

admin_urlpatterns = [
    path(
        "match-one/<path:pid>/<int:seq>/<path:what>/",
        APIMatchOneView.as_view(),
        name="api-match-one",
    ),
    re_path(
        # if what contains several targets, they are separated with an underscore
        r"^match-all/(?P<pid>.+)/(?P<what>[a-z-_]+)/(?P<force>[0-9])/$",
        APIMatchAllView.as_view(),
        name="api-match-all",
    ),
    path(
        "update-extid/<path:pk>/<path:action>/",
        UpdateExtIdView.as_view(),
        name="update-extid",
    ),
    re_path(
        r"^update-bibitemid/(?P<pk>[0-9]+)/(?P<action>[a-z-]+)/$",
        UpdateBibItemIdView.as_view(),
        name="update-bibitemid",
    ),
    re_path(
        r"^update-matching/(?P<pid>.+)/(?P<action>[a-z-]+)/$",
        UpdateMatchingView.as_view(),
        name="update-matching",
    ),
    re_path(
        r"^fetch-id/(?P<pk>[0-9]+)/(?P<id>.+)/(?P<what>.+)/(?P<resource>[a-z]+)/$",
        APIFetchId.as_view(),
        name="api-fetch-id",
    ),
    path("fetch-all/<path:pid>/", APIFetchAllView.as_view(), name="api-fetch-all"),
    #    re_path(r'^api-article-edit/(?P<colid>[A-Z-]+)/(?P<doi>.+)/$', ArticleEditAPIView.as_view(),
    #            name='api-edit-article'),
]
