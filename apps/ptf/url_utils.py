import re
from base64 import urlsafe_b64decode
from base64 import urlsafe_b64encode
from urllib.parse import parse_qs
from urllib.parse import urlencode
from urllib.parse import urlparse

from django.core.exceptions import ValidationError
from django.core.validators import URLValidator


def validate_url(url: str) -> bool:
    """
    Returns whether an absolute URL string is correctly formed.
    """
    url_validator = URLValidator(schemes=["http", "https"])
    url_valid = True

    try:
        url_validator(url)
    except ValidationError:
        url_valid = False

    return url_valid


def format_url_with_params(base_url: str, query_params: dict = {}) -> str:
    """
    Replace the querystring of the base URL with the given query parameters.
    """
    if not query_params:
        return base_url

    url = urlparse(str(base_url))
    url = url._replace(query=urlencode(query_params, doseq=True))
    return url.geturl()


def add_fragment_to_url(base_url: str, fragment: str) -> str:
    """Adds a fragment to an URL. Overwrite existing one, if any."""
    url = re.sub(r"#.*$", "", base_url)
    return f"{url}#{fragment}"


def add_query_parameters_to_url(base_url: str, query_dict: dict[str, list[str]]) -> str:
    """
    Add the given query parameters to the given URL.
    It keeps the existing query parameters & values.
    """
    url = urlparse(str(base_url))
    qs = parse_qs(url.query)

    for name, values in query_dict.items():
        if name not in qs:
            qs[name] = values
            continue
        values_to_add = [v for v in values if v not in qs[name]]
        if values_to_add:
            qs[name] = [*qs[name], *values_to_add]

    url = url._replace(query=urlencode(qs, doseq=True))
    return url.geturl()


def encode_for_url(string: str) -> str:
    """
    Encodes the given string for a safe use in URL.
    """
    return urlsafe_b64encode(string.encode()).decode()


def decode_from_url(encoded_string: str) -> str:
    """
    Decodes the given string previously encoded with `encode_for_url`.
    """
    return urlsafe_b64decode(encoded_string).decode()
