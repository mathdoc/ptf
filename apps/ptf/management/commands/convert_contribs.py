from django.core.management.base import BaseCommand

from ptf.cmds.database_cmds import convert_contribs
from ptf.models import *


class Command(BaseCommand):
    help = "convert ContribGroup/Contrib into Person/PersonInfo/Contribution"

    def add_arguments(self, parser):
        parser.add_argument(
            "-pid",
            action="store",
            dest="pid",
            type=str,
            default=None,
            required=False,
            help="Collection pid",
        )
        parser.add_argument(
            "-delete",
            action="store_true",
            dest="delete",
            default=False,
            help="Delete existing COntributions",
        )

    def handle(self, *args, **options):
        pid = options["pid"]
        delete = options["delete"]

        convert_contribs(pid, delete)
