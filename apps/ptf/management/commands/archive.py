import os

import pypdf as pypdf
from lxml import etree

from django.core.management.base import BaseCommand

from ptf.cmds.ptf_cmds import archiveIssuePtfCmd
from ptf.exceptions import ResourceDoesNotExist
from ptf.model_helpers import get_resource

OK = "\033[92m"  # GREEN
WARNING = "\033[93m"  # YELLOW
FAIL = "\033[91m"  # RED
RESET = "\033[0m"  # RESET COLOR


class Command(BaseCommand):
    help = "Export a container or an article or a collection"

    def add_arguments(self, parser):
        parser.add_argument(
            "-pid",
            action="store",
            dest="pid",
            type=str,
            default=None,
            required=True,
            help="Resource pid",
        )
        parser.add_argument(
            "-folder", dest="folder", type=str, default=None, required=True, help="Output folder"
        )
        parser.add_argument(
            "-binary-folder",
            dest="binary_folder",
            type=str,
            default=None,
            required=True,
            help="input folder where binary files are stored",
        )
        parser.add_argument(
            "-with_toc",
            dest="with_toc",
            type=bool,
            default=False,
            required=False,
            help="to write the toc.xml file",
        )
        parser.add_argument(
            "-with_pdfa",
            dest="with_pdfa",
            type=bool,
            default=False,
            required=False,
            help="to write a pdfa file",
        )
        parser.add_argument(
            "-xml_only",
            dest="xml_only",
            type=bool,
            default=False,
            required=False,
            help="only write the JATS file, the source files are ignored",
        )

    def create_toc(self, collection, pid):
        if os.access(f"/mathdoc_archive/{pid}", os.R_OK):
            os.chdir(f"/mathdoc_archive/{pid}")
        else:
            print(
                FAIL
                + f"ERREUR ! Vous n'avez pas accès au répertoire : /mathdoc_archive/{pid}"
                + RESET
            )
            return

        if os.access("toc.xml", os.R_OK):
            os.remove("toc.xml")

        new_fichier = open("toc.xml", "w+")  # Création du fichier toc.xml
        journal = etree.Element("journal")
        for issue in collection.content.all():
            new_node = etree.Element("journal-meta")

            issue_id = etree.Element("journal-id")  # Par exemple : JEP
            issue_id.text = pid
            new_node.append(issue_id)

            year = etree.Element("year")  # Par exemple : 2019
            year.text = issue.year
            new_node.append(year)

            if len(issue.vseries) != 0:
                series = etree.Element("series")  # Peu être null
                series.text = issue.vseries
                new_node.append(series)

            if len(issue.volume) != 0:
                volume = etree.Element("volume")  # Par exemple : 1
                volume.text = issue.volume
                new_node.append(volume)

            if len(issue.number) != 0:
                number = etree.Element("number")  # Peu être null
                number.text = issue.number
                new_node.append(number)

            folder = etree.Element("folder")
            folder.text = issue.pid
            new_node.append(folder)

            journal.append(new_node)
        node_str = etree.tostring(journal, pretty_print=True, encoding="unicode")
        new_fichier.write(node_str)
        new_fichier.close()
        print(
            OK
            + f"Le fichier toc.xml a été créé dans le répertoire : /mathdoc_archive/{pid}"
            + RESET
        )

    def create_pdfa(self, pid):
        lien_mathdoc_archive = "/mathdoc_archive"
        if os.access(f"{lien_mathdoc_archive}/{pid}", os.R_OK):
            os.chdir(f"{lien_mathdoc_archive}/{pid}")
            dossiers_collections = os.listdir()
            for dossier_collection_cours in dossiers_collections:
                if os.path.isdir(f"{lien_mathdoc_archive}/{pid}/{dossier_collection_cours}"):
                    os.chdir(f"{lien_mathdoc_archive}/{pid}/{dossier_collection_cours}")
                    dossiers_articles = os.listdir()
                    for dossier_article_cours in dossiers_articles:
                        if os.path.isdir(
                            f"{lien_mathdoc_archive}/{pid}/{dossier_collection_cours}/{dossier_article_cours}"
                        ):
                            os.chdir(
                                f"{lien_mathdoc_archive}/{pid}/{dossier_collection_cours}/{dossier_article_cours}"
                            )
                            fichiers_articles = os.listdir()
                            for fichier_article_cours in fichiers_articles:
                                if fichier_article_cours.endswith(
                                    ".pdf"
                                ) and not fichier_article_cours.endswith("PDFA.pdf"):
                                    fichier_pdf = fichier_article_cours.strip(".pdf")
                                    code_return = os.system(
                                        f"gs -dSAFER -dBATCH -DNOPAUSE -sPAPERSIZE=halfletter -dPDFFitPage -dFIXEDMEDIA -dEmbedAllFonts=true -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPrinted=false -dPDFSETTINGS=/printer -q -o {fichier_pdf}_PDFA.pdf {fichier_pdf}.pdf"
                                    )
                                    if (
                                        code_return != 0
                                    ):  # Si le code retour de la commande n'est pas 0, il y a eu une erreur
                                        print(
                                            FAIL
                                            + f"ERREUR ! Le fichier {lien_mathdoc_archive}/{pid}/{dossier_collection_cours}/{dossier_article_cours}/{fichier_pdf}.pdf n'a pas pu être converti en PDFA"
                                            + RESET
                                        )
                                    else:
                                        print(
                                            OK
                                            + f"Ecriture du fichier PDFA pour le fichier {lien_mathdoc_archive}/{pid}/{dossier_collection_cours}/{dossier_article_cours}/{fichier_pdf}.pdf"
                                            + RESET
                                        )

    def handle(self, *args, **options):
        pid = options["pid"]
        folder = options["folder"]
        binary_folder = options["binary_folder"]
        with_toc = options["with_toc"]
        with_pdfa = options["with_pdfa"]
        xml_only = options["xml_only"]

        resource = get_resource(pid)
        if not resource:
            raise ResourceDoesNotExist(f"Resource {pid} does not exist")
        obj = resource.cast()
        if obj.classname == "Container" or obj.classname == "Article":
            cmd = archiveIssuePtfCmd(
                {
                    "pid": pid,
                    "export_folder": folder,
                    "binary_files_folder": binary_folder,
                    "xml_only": xml_only,
                }
            )

            if obj.classname == "Article":
                cmd.set_article(obj)

            cmd.do()
        else:
            collection = obj

            if with_toc:
                self.create_toc(collection, pid)

            if with_pdfa:
                self.create_pdfa(pid)
