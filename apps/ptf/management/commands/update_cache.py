from django.core.management.base import BaseCommand

from ptf.cmds.xml_cmds import updateCacheXmlCmd


class Command(BaseCommand):
    help = "recreate the citation_html field of the bibitems"

    def add_arguments(self, parser):
        parser.add_argument(
            "-pid",
            action="store",
            dest="pid",
            type=str,
            default=None,
            required=True,
            help="Collection pid",
        )
        parser.add_argument(
            "-start_id",
            action="store",
            dest="start_id",
            type=str,
            default=None,
            help="Issues are sorted before update. start_id is the issue pid to start the update",
        )

    def handle(self, *args, **options):
        pid = options["pid"]
        start_id = options["start_id"]

        updateCacheXmlCmd({"colid": pid, "start_id": start_id}).do()
