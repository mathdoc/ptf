import json
import os

from django.core.management.base import BaseCommand

from ptf.cmds.ptf_cmds import exportPtfCmd
from ptf.exceptions import ResourceDoesNotExist
from ptf.model_helpers import get_resource
from ptf.models import Article


class Command(BaseCommand):
    help = "Export a container or an article or a collection"

    def add_arguments(self, parser):
        parser.add_argument(
            "-pid",
            action="store",
            dest="pid",
            type=str,
            default=None,
            required=True,
            help="Resource pid",
        )
        parser.add_argument(
            "-folder", dest="folder", type=str, default=None, help="Output folder for collections"
        )
        parser.add_argument(
            "-with-internal-data",
            action="store_true",
            dest="with_internal_data",
            default=False,
            help="Export internal date (ex extid:false_positive) in a json file",
        )
        parser.add_argument(
            "-for-archive",
            action="store_true",
            dest="for_archive",
            default=False,
            help="Store prod_deployed_date in the XML (default: False)",
        )
        parser.add_argument(
            "-with-binary-files",
            action="store_true",
            dest="with_binary_files",
            default=False,
            help="Export JPG/PNG/PDF/DjVu/...",
        )
        parser.add_argument(
            "-date_for_pii",
            action="store_true",
            dest="date_for_pii",
            default=False,
            help="export publication date for Elsevier articles. Export in @folder/elsevier.json",
        )

    def export_elsevier(self, colid, folder):
        articles = {}
        qs = Article.objects.filter(my_container__my_collection__pid=colid).order_by("pid")
        for article in qs:
            if article.date_published is not None:
                date_published_str = article.date_published.strftime("%Y-%m-%d")
                articles[article.doi] = {"date_published": date_published_str}

        json_object = json.dumps(articles, indent=4)
        to_file = os.path.join(folder, "elsevier.json")
        with open(to_file, "w") as f_:
            f_.write(json_object)

    def handle(self, *args, **options):
        pid = options["pid"]
        root_folder = options["folder"]

        if options["date_for_pii"]:
            self.export_elsevier(pid, root_folder)
        else:
            with_internal_data = options["with_internal_data"]
            with_binary_files = options["with_binary_files"]
            for_archive = options["for_archive"]
            resource = get_resource(pid)
            if not resource:
                raise ResourceDoesNotExist(f"Resource {pid} does not exist")
            obj = resource.cast()
            if obj.classname == "Collection":
                if root_folder is None:
                    raise ValueError("-folder is required for collections")
                exportPtfCmd(
                    {
                        "pid": pid,
                        "with_internal_data": with_internal_data,
                        "with_binary_files": with_binary_files,
                        "for_archive": for_archive,
                        "export_folder": root_folder,
                    }
                ).do()

                for issue in obj.content.all():
                    exportPtfCmd(
                        {
                            "pid": issue.pid,
                            "with_internal_data": with_internal_data,
                            "with_binary_files": with_binary_files,
                            "for_archive": for_archive,
                            "export_folder": root_folder,
                        }
                    ).do()

                for ancestor in obj.ancestors.all():
                    for issue in ancestor.content.all():
                        exportPtfCmd(
                            {
                                "pid": issue.pid,
                                "with_internal_data": with_internal_data,
                                "with_binary_files": with_binary_files,
                                "for_archive": for_archive,
                                "export_folder": root_folder,
                            }
                        ).do()

            else:
                xml_body = exportPtfCmd(
                    {
                        "pid": pid,
                        "with_internal_data": with_internal_data,
                        "with_binary_files": with_binary_files,
                        "for_archive": for_archive,
                        "export_folder": root_folder,
                    }
                ).do()
                self.stdout.write(xml_body)
