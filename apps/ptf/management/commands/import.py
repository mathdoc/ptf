import argparse
import json
import os

from lxml import ElementInclude
from lxml import etree

from django.conf import settings
from django.core.management.base import BaseCommand
from django.core.management.base import DjangoHelpFormatter

from matching import scrapping
from ptf.cmds.ptf_cmds import addDjvuPtfCmd
from ptf.cmds.xml.jats import jats_parser
from ptf.cmds.xml_cmds import addArticleXmlCmd
from ptf.cmds.xml_cmds import addBookXmlCmd
from ptf.cmds.xml_cmds import addCollectionsXmlCmd
from ptf.cmds.xml_cmds import addIssueXmlCmd
from ptf.cmds.xml_cmds import addOrUpdateBookXmlCmd
from ptf.cmds.xml_cmds import addorUpdateCedricsArticleXmlCmd
from ptf.cmds.xml_cmds import addOrUpdateIssueXmlCmd
from ptf.cmds.xml_cmds import importEntireCollectionXmlCmd
from ptf.cmds.xml_cmds import updateCollectionsXmlCmd
from ptf.display import resolver
from ptf.model_helpers import get_article
from ptf.model_helpers import get_article_by_doi
from ptf.model_helpers import get_collection
from ptf.model_helpers import get_container
from ptf.model_helpers import parse_date_str


class CustomHelpFormatter(DjangoHelpFormatter):
    pass


# Use RawDescriptionHelpFormatter to allow '\n' in the help
CustomHelpFormatter._fill_text = argparse.RawDescriptionHelpFormatter._fill_text


class Command(BaseCommand):
    help = """******************************************************************
Import Resources (Collection/Containers/Articles)
Typical use cases:
import -pid @col -folder /mathdoc_archive -to_folder /mersenne_test_data: Import a collection from mathdoc_archive (metadata, files)
import -pid @col -folder /mathdoc_archive: Import a collection from mathdoc_archive, only the metadata
import -issue|-book @full_path.xml:  Import an issue/a book
import -pid @colid -article @full_path.xml:  Import an article

Advanced use cases:
import -pid @col -keep_metadata ...: used with ptf_tools to preserve existing metadata (matching...)
import -pid @col -first_issue @pid ...: import an entire collection starting from a specific issue (used to continue a previous task)
import -pid @col -col_only...: import only the collection XML
import -article @pid -folder @issue_pid: import a Cedrics Article

Other options:
-fake: Parse XML but do not import
-no_bib: Ignore the references during the import (for Geodesic)
-embargo: Only import the opened articles (for Geodesic)
******************************************************************
"""

    def add_arguments(self, parser):
        parser.formatter_class = CustomHelpFormatter

        parser.add_argument(
            "-sample",
            action="store_true",
            dest="sample",
            default=False,
            help="Import a few journals",
        )
        parser.add_argument(
            "-issue",
            action="store",
            dest="issue",
            type=str,
            default=None,
            help="Issue filename if you want to import only 1 issue",
        )
        parser.add_argument(
            "-book",
            action="store",
            dest="book",
            type=str,
            default=None,
            help="Book filename if you want to import only 1 book",
        )
        parser.add_argument(
            "-article",
            action="store",
            dest="article",
            type=str,
            default=None,
            help="Article filename if you want to import only 1 article",
        )
        parser.add_argument(
            "-pid",
            action="store",
            dest="pid",
            type=str,
            default=None,
            help="Resource pid",
        )
        parser.add_argument(
            "-folder",
            dest="folder",
            type=str,
            default=None,
            help="Input folder for collections : The folder must look like @COL/@ISSUE/@ISSUE.XML - Use a path at the root (1 slash only)",
        )
        parser.add_argument(
            "-to_folder",
            dest="to_folder",
            type=str,
            default=None,
            help="Output folder for collections : Folder where binary files are copied",
        )
        parser.add_argument(
            "-fake",
            action="store_true",
            dest="fake",
            default=False,
            help="Parse XML but do not import",
        )
        parser.add_argument(
            "-keep_metadata",
            action="store_true",
            dest="keep_metadata",
            default=False,
            help="Preserve existing metadata (matching, dates)",
        )
        parser.add_argument(
            "-with_cedrics",
            action="store_true",
            dest="with_cedrics",
            default=False,
            help="If the pid is a collection, you can import Cedrics XML after mathdoc_archive (to be used in ptf_tools only)",
        )
        parser.add_argument(
            "-from_cedrics",
            action="store_true",
            dest="from_cedrics",
            default=False,
            help="Set to True if the pid is a collection with only Cedrics XML issues)",
        )
        parser.add_argument(
            "-col_only",
            action="store_true",
            dest="col_only",
            default=False,
            help="update only the collection",
        )
        parser.add_argument(
            "-date_for_pii",
            action="store_true",
            dest="date_for_pii",
            default=False,
            help="fetch publication date for Elsevier articles. Export in @to_folder/elsevier.json ",
        )
        parser.add_argument(
            "-first_issue",
            dest="first_issue",
            type=str,
            default="",
            help="used with -date_for_pii. First issue to handle",
        )
        parser.add_argument(
            "-json",
            action="store",
            dest="json_file",
            type=str,
            default=None,
            help="JSON filename of article metadata",
        )
        parser.add_argument(
            "-djvu",
            action="store_true",
            dest="djvu",
            default=False,
            help="Restore Djvu datastream objects",
        )
        parser.add_argument(
            "-no_bib",
            action="store_true",
            dest="no_bib",
            default=False,
            help="Ignore the references during the import",
        )
        parser.add_argument(
            "-embargo",
            action="store_true",
            dest="embargo",
            default=False,
            help="Only import the open articles",
        )

    @staticmethod
    def get_body(filename):
        with open(filename, encoding="utf-8") as file_:
            body = file_.read()
        return body

    def import_sample(self):
        data_folder = f"{settings.PTF_DIR}/sites/numdam/tests/data/"

        journal = get_collection("AIF")

        if not journal:
            body = self.get_body(data_folder + "collection-aif.xml")
            cmd = addCollectionsXmlCmd({"body": body})
            journals = cmd.do()
            journal = journals[0]

        journal = get_collection("JEDP")

        if not journal:
            body = self.get_body(data_folder + "collection-jedp.xml")
            cmd = addCollectionsXmlCmd({"body": body})
            journals = cmd.do()
            journal = journals[0]

        journal = get_collection("AFST")

        if not journal:
            body = self.get_body(data_folder + "journal-formula.xml")
            cmd = addCollectionsXmlCmd({"body": body})
            journals = cmd.do()
            journal = journals[0]

        issue = get_container("AIF_2015__65_6")
        if not issue:
            body = self.get_body(data_folder + "issue-aif.xml")
            cmd = addIssueXmlCmd({"body": body})
            issue = cmd.do()

        issue = get_container("JEDP_2003___")
        if not issue:
            body = self.get_body(data_folder + "issue-jedp.xml")
            cmd = addIssueXmlCmd({"body": body})
            issue = cmd.do()

        journal = get_collection("SMS")

        if not journal:
            body = self.get_body(data_folder + "journal-sms.xml")
            cmd = addCollectionsXmlCmd({"body": body})
            journals = cmd.do()
            journal = journals[0]

        issue = get_container("SMS_1969-1970__1_")
        if not issue:
            body = self.get_body(data_folder + "issue.xml")
            cmd = addIssueXmlCmd({"body": body})
            issue = cmd.do()

        journal = get_collection("MSH")

        if not journal:
            body = self.get_body(data_folder + "collection-msh.xml")
            cmd = addCollectionsXmlCmd({"body": body})
            journals = cmd.do()
            journal = journals[0]

        issue = get_container("MSH_1968__21_")
        if not issue:
            body = self.get_body(data_folder + "issue5.xml")
            cmd = addIssueXmlCmd({"body": body})
            issue = cmd.do()

        collection = get_collection("CIF")

        if not collection:
            body = self.get_body(data_folder + "collection.xml")
            cmd = addCollectionsXmlCmd({"body": body})
            collections = cmd.do()
            collection = collections[0]

        book = get_container("CIF_1995__23_")
        if not book:
            body = self.get_body(data_folder + "book-cif.xml")
            cmd = addBookXmlCmd({"body": body})
            book = cmd.do()

        collection = get_collection("THESE")
        if not collection:
            body = self.get_body(data_folder + "collection-these.xml")
            cmd = addCollectionsXmlCmd({"body": body})
            collections = cmd.do()
            collection = collections[0]

        book = get_container("THESE_1934__156__1_0")
        if not book:
            body = self.get_body(data_folder + "book-these.xml")
            cmd = addBookXmlCmd({"body": body})
            book = cmd.do()

        journal = get_collection("BSMF")

        if not journal:
            body = self.get_body(data_folder + "collection-bsmf.xml")
            cmd = addCollectionsXmlCmd({"body": body})
            journals = cmd.do()
            journal = journals[0]

        # issue = get_container("BSMF_1984__112__3_0")
        # if not issue:
        #     body = self.get_body(data_folder + 'issue-bsmf.xml')
        #     cmd = addOrUpdateIssueXmlCmd({"body": body})
        #     issue = cmd.do()

        journal = get_collection("MSMF")

        if not journal:
            body = self.get_body(data_folder + "collection-msmf.xml")
            cmd = addCollectionsXmlCmd({"body": body})
            journals = cmd.do()
            journal = journals[0]

        # book = get_container("MSMF_2014_2_138-139__1_0")
        # if not book:
        body = self.get_body(data_folder + "book.xml")
        cmd = addOrUpdateBookXmlCmd({"body": body})
        book = cmd.do()

        book = get_container("MSMF_1993_2_52_")
        if not book:
            body = self.get_body(data_folder + "book-with-part.xml")
            cmd = addBookXmlCmd({"body": body})
            book = cmd.do()

        book = get_container("MSMF_1992_2_49__1_0")
        if not book:
            body = self.get_body(data_folder + "book-msmf2.xml")
            cmd = addBookXmlCmd({"body": body})
            book = cmd.do()

        journal = get_collection("AST")

        if not journal:
            body = self.get_body(data_folder + "collection-ast.xml")
            cmd = addCollectionsXmlCmd({"body": body})
            journals = cmd.do()
            journal = journals[0]

        book = get_container("AST_1984__119-120_")
        if not book:
            body = self.get_body(data_folder + "book-ast.xml")
            cmd = addBookXmlCmd({"body": body})
            book = cmd.do()

        journal = get_collection("SB")

        if not journal:
            body = self.get_body(data_folder + "collection-sb.xml")
            cmd = addCollectionsXmlCmd({"body": body})
            journals = cmd.do()
            journal = journals[0]

        book = get_container("SB_1987-1988__30_")
        if not book:
            body = self.get_body(data_folder + "book-ast-sb.xml")
            cmd = addOrUpdateBookXmlCmd({"body": body})
            book = cmd.do()

        self.stdout.write(self.style.SUCCESS("OK"))

    def import_json(self, json_file):
        with open(json_file) as f_:
            json_object = json.load(f_)

        for doi, items in json_object.items():
            article = get_article_by_doi(doi)
            if article is not None:
                if "date_published" in items:
                    date_published = parse_date_str(items["date_published"])
                    if article.date_published != date_published:
                        article.date_published = date_published
                        article.save()

    def import_djvu(self, colid):
        """
        Can be used with ptf_tools only (rely on ResourceInNumdam)
        """

        from ptf_tools.models import ResourceInNumdam

        qs = ResourceInNumdam.objects.filter(pid__startswith=f"{colid}_")

        for item in qs:
            container = get_container(item.pid)
            if container is not None:
                if int(container.year) < 2020:
                    print(container.pid)
                    for article in container.article_set.all():
                        try:
                            cmd = addDjvuPtfCmd()
                            cmd.set_resource(article)
                            cmd.do()
                        except Exception as e:
                            print(f"Djvu error for {article.pid}, {e}")

    def import_elsevier(self, params):
        colid = params["pid"]
        from_folder = params["import_folder"]
        to_folder = params["to_folder"]
        first_issue = params["first_issue"]

        collection = get_collection(colid)
        if collection is None:
            raise ValueError(f"{colid} does not exists")

        articles = {}

        for pid, file_ in resolver.iterate_collection_folder(from_folder, colid, first_issue):
            print(pid)
            body = resolver.get_body(file_)
            xml_file_folder = os.path.dirname(file_)

            parser = etree.XMLParser(
                huge_tree=True,
                recover=True,
                remove_blank_text=True,
                remove_comments=True,
                resolve_entities=True,
            )
            if xml_file_folder[-1] != "/":
                xml_file_folder += "/"
            # For ElementInclude to find the href
            body = body.replace('xmlns:xlink="http://www.w3.org/1999/xlink"', "").replace(
                "xlink:href", "href"
            )
            tree = etree.fromstring(body.encode("utf-8"), parser=parser)
            if xml_file_folder is not None:
                ElementInclude.include(tree, base_url=xml_file_folder)
            if tree is None:
                raise ValueError("tree est vide")

            xissue = jats_parser.JatsIssue(tree=tree)
            for xarticle in xissue:
                if hasattr(xarticle, "pii"):
                    date_published_str = ""
                    doi = xarticle.doi
                    article = get_article(xarticle.pid)
                    if article is not None and article.date_published is not None:
                        date_published_str = article.date_published.strftime("%Y-%m-%d")
                    elif xarticle.date_published_iso_8601_date_str is not None:
                        date_published_str = xarticle.date_published_iso_8601_date_str
                    elif article is not None:
                        article_data = scrapping.fetch_article(xarticle.doi, xarticle.pii)
                        date_published_str = article_data.date_published_iso_8601_date_str

                        article.date_published = parse_date_str(date_published_str)
                        article.save()

                    if len(date_published_str) > 0:
                        articles[doi] = {"date_published": date_published_str}

        json_object = json.dumps(articles, indent=4)
        to_file = os.path.join(to_folder, "elsevier.json")
        with open(to_file, "w") as f_:
            f_.write(json_object)

    def handle(self, *args, **options):
        # self.stdout.write(self.style.ERROR('error - A major error.'))

        if options["sample"]:
            self.import_sample()
        elif options["issue"]:
            body = self.get_body(options["issue"])
            xml_file_folder = os.path.dirname(options["issue"])
            params = {
                "body": body,
                "backup_folder": options["folder"],
                "from_folder": options["folder"],
                "to_folder": options["to_folder"],
                "xml_file_folder": xml_file_folder,
                "fake": options["fake"],
                "no_bib": options["no_bib"],
                "embargo": options["embargo"],
            }
            if options["keep_metadata"]:
                params["keep_metadata"] = True
                params["backup_folder"] = settings.MERSENNE_TMP_FOLDER
            cmd = addOrUpdateIssueXmlCmd(params)
            cmd.do()
            self.stdout.write(self.style.SUCCESS("OK"))
        elif options["article"]:
            if options["folder"]:
                cmd = addorUpdateCedricsArticleXmlCmd(
                    {"container_pid": options["folder"], "article_folder_name": options["article"]}
                )
                cmd.do()
                self.stdout.write(self.style.SUCCESS("OK"))
            else:
                body = self.get_body(options["article"])
                collection = get_collection(options["pid"])
                issue = collection.content.order_by(
                    "-year", "-vseries", "-volume", "-volume_int", "-number_int"
                ).first()

                cmd = addArticleXmlCmd({"body": body, "issue": issue, "standalone": True})
                cmd.set_collection(collection)

                # body = json.loads(body)
                #
                # if options['article'].rfind('.json') > 0:
                #     xarticle = Munch(body)
                #     new_bibitems = []
                #     for bib in xarticle.bibitems:
                #         new_bibitems.append(Munch(bib))
                #     xarticle.bibitems = new_bibitems
                #
                #     collection = get_collection(options['pid'])
                #
                #     cmd = addArticleXmlCmd({'xarticle': xarticle, 'use_body': False, 'standalone': True})
                #     cmd.set_collection(collection)
                # else:
                #     cmd = addBodyInHtmlXmlCmd(
                #         {"body": body, 'pid': options['pid'], 'import_folder': options['folder']})
                cmd.do()
                self.stdout.write(self.style.SUCCESS("OK"))
        elif options["book"]:
            body = self.get_body(options["book"])
            cmd = addOrUpdateBookXmlCmd({"body": body, "import_folder": options["folder"]})
            cmd.do()
            self.stdout.write(self.style.SUCCESS("OK"))
        elif options["json_file"]:
            self.import_json(options["json_file"])
        elif options["djvu"]:
            self.import_djvu(options["pid"])
        else:
            params = {
                "pid": options["pid"],
                "import_folder": options["folder"],
                "to_folder": options["to_folder"],
                "fake": options["fake"],
            }
            if options["keep_metadata"]:
                params["keep_metadata"] = True
                params["temp_folder"] = settings.MERSENNE_TMP_FOLDER
            params["with_cedrics"] = options.get("with_cedrics", False)
            params["from_cedrics"] = options.get("from_cedrics", False)
            params["date_for_pii"] = options.get("date_for_pii", False)
            params["first_issue"] = options.get("first_issue", "")
            col_only = options.get("col_only", False)
            if params["date_for_pii"]:
                self.import_elsevier(params)
            else:
                if col_only:
                    body = resolver.get_archive_body(options["folder"], options["pid"], None)
                    params["body"] = body
                    params["from_folder"] = options["folder"]
                    cmd = updateCollectionsXmlCmd(params)
                else:
                    if (
                        options["folder"]
                        and options["to_folder"]
                        and options["folder"] != settings.MATHDOC_ARCHIVE_FOLDER
                    ):
                        print(
                            f"WARNING: -folder is different from settings.MATHDOC_ARCHIVE_FOLDER ({settings.MATHDOC_ARCHIVE_FOLDER})."
                        )
                        print("Files are not copied to the to_folder")
                    params["from_folder"] = options["folder"]
                    params["no_bib"] = options["no_bib"]
                    params["embargo"] = options["embargo"]
                    cmd = importEntireCollectionXmlCmd(params)
                cmd.do()
                self.stdout.write(self.style.SUCCESS("OK"))
