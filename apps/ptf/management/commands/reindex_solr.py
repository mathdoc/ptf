from django.conf import settings
from django.core.management.base import BaseCommand
from django.db.models import Q

from ptf.cmds.solr_cmds import addArticleSolrCmd
from ptf.cmds.solr_cmds import addBookPartSolrCmd
from ptf.cmds.solr_cmds import addContainerSolrCmd
from ptf.cmds.xml import xml_utils
from ptf.model_data_converter import db_to_article_data
from ptf.model_data_converter import db_to_book_data
from ptf.model_data_converter import db_to_bookpart_data
from ptf.models import Article
from ptf.models import Container


class Command(BaseCommand):
    help = "Reindex your Solr core from PostgreSQL"

    def add_arguments(self, parser):
        parser.add_argument(
            "-pid",
            action="store",
            dest="pid",
            type=str,
            default=None,
            help="Collection pid if you want to reindex only 1 collection",
        )
        parser.add_argument(
            "-start_id",
            action="store",
            dest="start_id",
            type=str,
            default=None,
            help="In case SolR crashes, start_id allows you to start the reindexing from a given pid",
        )
        parser.add_argument(
            "--use_settings_site",
            action="store_true",
            dest="use_settings_site",
            default=False,
            help="""By default, the command indexes the resources with the site of the corresponding collection in site_register.py.
            Using this option will index the resources using the site where the command has been executed.""",
        )

    def get_body(self, xobj):
        # Get body_xml from PostgreSQL
        # regenerate body (XML -> TXT)
        body = ""
        body_xml = xobj.body_xml
        if body_xml:
            body = xml_utils.get_text_from_xml_with_mathml(body_xml)
        return body

    def handle(self, *args, **options):
        qs = Article.objects.all()
        if options["pid"]:
            qs = qs.filter(my_container__my_collection__pid=options["pid"])

        start = options["start_id"] is None

        for article in qs.order_by("pid"):
            if options["start_id"] and article.pid == options["start_id"]:
                start = True

            if start:
                site_id = None
                if options["use_settings_site"]:
                    site_id = settings.SITE_ID
                # The Solr search is by default filtered on the site, which is by default equivalent to the article's collection
                # Since the reindex_solr is run from any given site, force the 'sites' param
                else:
                    site_id = settings.SITE_REGISTER[article.get_top_collection().pid.lower()][
                        "site_id"
                    ]
                params = {
                    "id": article.id,
                    "pid": article.pid,
                    "db_obj": article,
                    "sites": [site_id],
                }

                if (
                    article.my_container.ctype.startswith("book")
                    or article.my_container.ctype == "lecture-notes"
                ):
                    xobj = db_to_bookpart_data(article)
                    params["xobj"] = xobj
                    cmd = addBookPartSolrCmd(params)
                else:
                    xobj = db_to_article_data(article)
                    params["xobj"] = xobj
                    cmd = addArticleSolrCmd(params)

                xobj.body = self.get_body(xobj)
                if xobj.body:
                    print(article.pid)
                else:
                    print(article.pid, "No body")

                cmd.add_collection(article.get_collection())
                cmd.set_container(article.my_container)
                cmd.do()  # pre_do starts by deleting the existing Solr document

                for trans_article in article.translations.all():
                    print(trans_article.pid)
                    xtrans_obj = db_to_article_data(trans_article)
                    if xtrans_obj.body_html:
                        xtrans_obj.body = xml_utils.get_text_from_xml_with_mathml(
                            xtrans_obj.body_html
                        )
                    xtrans_obj.trans_title_tex = xobj.title_tex
                    xtrans_obj.trans_title_html = xobj.title_html

                    if article.trans_lang == trans_article.lang:
                        if article.trans_title_tex:
                            xtrans_obj.title_tex = article.trans_title_tex
                            xtrans_obj.title_html = article.trans_title_html
                        for abstract in xobj.abstracts:
                            if (
                                abstract["tag"] == "abstract"
                                and abstract["lang"] == trans_article.lang
                            ):
                                xtrans_obj.abstracts = [abstract]

                    params = {
                        "id": trans_article.id,
                        "pid": trans_article.pid,
                        "db_obj": trans_article,
                        "sites": [site_id],
                        "xobj": xtrans_obj,
                    }
                    cmd = addArticleSolrCmd(params)
                    cmd.add_collection(article.get_collection())
                    cmd.set_container(article.my_container)
                    cmd.do()  # pre_do starts by deleting the existing Solr document

        qs = Container.objects.filter(Q(ctype__startswith="book") | Q(ctype="lecture-notes"))
        if options["pid"]:
            qs = qs.filter(my_collection__pid=options["pid"])

        for book in qs.order_by("pid"):
            if options["start_id"] and article.pid == options["start_id"]:
                start = True

            if start:
                xobj = db_to_book_data(book)
                xobj.body = self.get_body(xobj)

                if xobj.body:
                    print(book.pid)
                else:
                    print(book.pid, "No body")

                cmd = addContainerSolrCmd(
                    {"xobj": xobj, "id": book.id, "pid": book.pid, "db_obj": book}
                )
                cmd.add_collection(book.get_collection())
                cmd.do()
