from django.core.management.base import BaseCommand

from ptf.cmds.ptf_cmds import addContainerPtfCmd
from ptf.model_helpers import get_collection


class Command(BaseCommand):
    help = (
        "Remove all contents of a collection (articles/issues/collection)"
        "in PTF. Does not remove binary files on disk"
    )

    def add_arguments(self, parser):
        parser.add_argument(
            "-pid",
            action="store",
            dest="pid",
            type=str,
            default=None,
            required=True,
            help="Resource pid",
        )

    def handle(self, *args, **options):
        pid = options["pid"]
        collection = get_collection(pid)
        if not collection:
            raise ValueError(pid + " does not exist")
        print(f"Deleting the collection '{pid}' ...\n")
        for issue in collection.content.all():
            cmd = addContainerPtfCmd()
            cmd.set_object_to_be_deleted(issue)
            cmd.undo()
        collection.delete()
