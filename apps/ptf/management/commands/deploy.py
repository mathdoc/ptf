import json
from datetime import date

import dateutil.parser
import requests

from django.conf import settings
from django.core.management.base import BaseCommand
from django.core.serializers.json import DjangoJSONEncoder

from ptf.cmds import ptf_cmds
from ptf.models import Container
from ptf.models import Site
from ptf.models import SiteMembership


class DeployFailed(Exception):
    """
    Probleme lors du déploiement : sans doute PROD plus à jour que PRE!
    """


class Command(BaseCommand):
    help = "Export deploy date for Container - for OAI !!! ATTENTION, si on veut une date niveau article c est à MODIFIER"

    def add_arguments(self, parser):
        parser.add_argument(
            "--export",
            action="store_true",
            dest="export",
            default=False,
            help="Export Resources with deploy date",
        )
        parser.add_argument(
            "--import",
            action="store",
            dest="importation",
            type=str,
            default=None,
            help="Import JSON file of Resources with deploy date",
        )
        parser.add_argument(
            "--pid",
            action="store",
            dest="pid",
            type=str,
            default=None,
            help="Deploy container manually (needs site as well)",
        )
        parser.add_argument(
            "--site",
            action="store",
            dest="site",
            type=str,
            default=None,
            help="website/test_website to deploy the --pid to",
        )

    def export(self):
        data = {}
        site = Site.objects.get_current()
        for container in Container.objects.filter(sitemembership__site=site):
            # test = SiteMembership.objects.filter(resource=container).filter(site__id=settings.SITE_ID)
            data[container.pid] = {
                "deployed": container.deployed_date().isoformat(),
                "last_modified": container.last_modified.isoformat(),
            }
        json_data = json.dumps(data, cls=DjangoJSONEncoder)
        self.stdout.write(json_data)

    def importation(self, **options):
        filename = options["importation"]
        try:
            if isinstance(filename, str):
                data_file = open(filename, encoding="utf-8")
                data = data_file.read()
            else:  # pour les tests
                data = filename.getvalue()
            data = json.loads(data)
            # print data
            containers = Container.objects.filter(sitemembership__site__id=settings.SITE_ID)
            for container in containers:
                # check if the last_modified date is the same as the JSON
                # file
                if container.pid in data:
                    last_modified_date = dateutil.parser.parse(
                        data[container.pid]["last_modified"]
                    )
                    # si la date de raffinement de la prod est supérieure à
                    # celle de la preprod, exception
                    if last_modified_date < container.last_modified:
                        sitemembership = (
                            SiteMembership.objects.filter(resource=container)
                            .filter(site__id=settings.SITE_ID)
                            .first()
                        )
                        sitemembership.deployed = date.today()
                        sitemembership.save()
                        # il faut aussi modifier les articles/book part
                        articles = container.article_set.all()
                        if articles:
                            SiteMembership.objects.filter(resource__in=articles).filter(
                                site__id=settings.SITE_ID
                            ).update(deployed=date.today())

                        self.stdout.write(f"update deploy date for {container.pid} to NOW")
                    elif (last_modified_date - container.last_modified).total_seconds() > -1 and (
                        last_modified_date - container.last_modified
                    ).total_seconds() < 1:
                        # print "on met à jour la date de pub du container"
                        deployed_date = dateutil.parser.parse(data[container.pid]["deployed"])
                        sitemembership = (
                            SiteMembership.objects.filter(resource=container)
                            .filter(site__id=settings.SITE_ID)
                            .first()
                        )
                        sitemembership.deployed = deployed_date
                        sitemembership.save()
                        # il faut aussi modifier les articles/book part
                        articles = container.article_set.all()
                        if articles:
                            SiteMembership.objects.filter(resource__in=articles).update(
                                deployed=deployed_date
                            )

                        self.stdout.write(
                            f"update deploy date for {container.pid} to date of prod"
                        )
                    elif last_modified_date > container.last_modified:
                        raise DeployFailed(
                            f"La Prod a une ressource de version plus récente que PRE : PID : {container.pid}"
                        )
                    else:
                        raise DeployFailed(f"Cas non pensé : PID : {container.pid}")

                else:
                    # le container n'était pas publié auparavant, on met à jour la date de pub
                    # à maintenant +1h
                    sitemembership = (
                        SiteMembership.objects.filter(resource=container)
                        .filter(site__id=settings.SITE_ID)
                        .first()
                    )
                    sitemembership.deployed = date.today()
                    sitemembership.save()
                    # il faut aussi modifier les articles/book part
                    articles = container.article_set.all()
                    if articles:
                        SiteMembership.objects.filter(resource__in=articles).update(
                            deployed=date.today()
                        )
                    self.stdout.write(f"set deploy date to today for {container.pid}")
        except OSError:
            self.stderr.write("Bad name for JSON file : python manage.py --import file.json")
        except ValueError:
            self.stderr.write("Bad file format")

    def deploy(self, pid, site):
        xml = ptf_cmds.exportPtfCmd({"pid": pid}).do()
        body = xml.encode("utf8")

        # if container = issue
        if container.ctype == "issue":
            url = server_url + reverse("issue_upload")
        else:
            url = server_url + reverse("book_upload")

        response = requests.post(url, data=body, verify=False)
        status = response.status_code

        if status < 200 or status > 204:
            print("Error, status code", status)

    def handle(self, *args, **options):
        if options["export"]:
            self.export()
        if options["importation"]:
            self.importation(**options)
