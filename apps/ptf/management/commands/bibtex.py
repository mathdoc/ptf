from django.core.management.base import BaseCommand

from ...models import Article


class Command(BaseCommand):
    help = "Export unknown bibtex characters"

    def handle(self, *args, **options):
        self.stdout.write("Caractères des articles inconnus par latexcodec")

        all_unknowns = set()
        for article in Article.objects.all():
            try:
                article.get_bibtex("")
            except Exception as e:
                new_unknowns = set(e.unknowns)
                in_new_but_not_in_all = new_unknowns - all_unknowns

                for c in in_new_but_not_in_all:
                    self.stdout.write("".join([repr(c), " : ", c, "    ", article.pid]))

                all_unknowns = all_unknowns | in_new_but_not_in_all
