import datetime
import itertools
import random
import re
import unicodedata

import dateutil.parser
from unidecode import unidecode

from django.conf import settings
from django.http import Http404
from django.shortcuts import get_list_or_404
from django.utils import timezone

# from .models import ResourceInSpecialIssue
from .models import Article
from .models import Author
from .models import BibItem
from .models import BibItemId
from .models import Collection
from .models import Container
from .models import ExtId
from .models import ExtLink
from .models import Provider
from .models import PtfSite
from .models import Publisher
from .models import RelatedObject
from .models import RelationName
from .models import Relationship
from .models import Resource
from .models import ResourceInSpecialIssue
from .models import SiteMembership
from .models import XmlBase
from .site_register import SITE_REGISTER

##########################################################################
#
# Get functions
#
##########################################################################


def get_first_last_years(year):
    the_array = year.split("-")

    fyear = the_array[0]
    lyear = the_array[1] if len(the_array) > 1 else fyear

    return fyear, lyear


def get_provider_by_name(name):
    if name == "numdam":
        name = "mathdoc"

    provider = Provider.objects.get(name=name)
    return provider


def get_provider(pid_type):
    provider = Provider.objects.get(pid_type=pid_type)
    return provider


def get_publisher(name):
    try:
        key = make_key(name)
        publisher = Publisher.objects.get(pub_key=key)
    except Publisher.DoesNotExist:
        publisher = None
    return publisher


def get_or_create_site(site_id, acro=None):
    try:
        site = PtfSite.objects.get(id=site_id)
    except PtfSite.DoesNotExist:
        site = PtfSite.objects.create(id=site_id, name=acro, domain=acro, acro=acro)
    return site


def get_journals():
    journals = Collection.objects.filter(sites__id=settings.SITE_ID, coltype="journal")
    return journals.all()


def get_actas():
    actas = Collection.objects.filter(sites__id=settings.SITE_ID, coltype="acta")
    return actas.all()


def get_lectures():
    lectures = Collection.objects.filter(sites__id=settings.SITE_ID, coltype="lecture-notes")
    return lectures.all()


def get_proceedings():
    proceedings = Collection.objects.filter(sites__id=settings.SITE_ID, coltype="proceeding")
    return proceedings.all()


def get_collection_of_books(with_lectures=False):
    filters = ["book-series"]
    if with_lectures:
        filters.append("lecture-notes")

    book_series = Collection.objects.filter(sites__id=settings.SITE_ID, coltype__in=filters)
    return book_series.all()


def get_books():
    return Container.objects.filter(
        sites__id=settings.SITE_ID,
        my_collection__coltype__in=["book-series", "lecture-notes", "book-edited-book"],
    )


def get_collection_of_thesis():
    theses = Collection.objects.filter(sites__id=settings.SITE_ID, coltype="thesis")
    return theses.all()


def get_theses():
    theses = Container.objects.filter(sites__id=settings.SITE_ID, my_collection__coltype="thesis")
    return theses.all()


# TODO require the provider in the get_ functions as it serves as a namespace


def get_collection(pid, sites=True, prefetch=False):
    try:
        if sites:
            col = Collection.objects.get(pid=pid, sites__id=settings.SITE_ID)
        else:
            col = Collection.objects.get(pid=pid)
    except Collection.DoesNotExist:
        col = None
    return col


def get_volumes_in_collection(collection):
    """
    used by issue-list.html and oai gallica
    return list of issues by volume if volume_int exist or by date
    return list of publishers GROUP BY name with date
    @param collection:
    @return:

    """

    items = (
        collection.content.filter(sites__id=settings.SITE_ID)
        .exclude(ctype="issue_special")
        .order_by("-vseries_int", "-year", "-volume_int", "number_int")
        .all()
    )

    if collection.collectionmembership_set.count() > 0:
        col_membsership_qs = (
            collection.collectionmembership_set.filter(container__sites__id=settings.SITE_ID)
            .exclude(ctype="issue_special")
            .order_by("-vseries_int", "-volume_int", "number_int")
            .all()
        )
        joined = itertools.chain(items, col_membsership_qs)

        def sorter(dict_):
            return (
                dict_.vseries_int,
                dict_.year if hasattr(dict_, "year") else dict_.container.year,
                dict_.volume_int,
                dict_.number_int,
            )

        items = sorted(joined, key=sorter, reverse=True)

    # items is now a collection of Container and/or CollectionMembership

    issue_to_appear_pid = ""
    if hasattr(settings, "ISSUE_TO_APPEAR_PID"):
        issue_to_appear_pid = settings.ISSUE_TO_APPEAR_PID

    issues_in_vseries = []
    volumes_in_vseries = []
    issues_in_volume = []
    publishers = []
    results_vseries = "-1"
    results_volume = "-1"
    results_year = "-1"
    results_fyear = results_lyear = -1
    volume_count = 0
    max_volume_count = 0
    max_width = 0
    width = 0
    for item in items:
        width += len(item.number)
        if hasattr(item, "container"):
            issue = item.container
            # The issue-list template use issue properties to display the list of issues
            # Replace the values with those of the CollectionMembership for the display
            issue.veries = item.vseries
            issue.volume = item.volume
            issue.number = item.number
            issue.vseries_int = item.vseries_int
            issue.volume_int = item.volume_int
            issue.number_int = item.number_int

            year = issue.year
            vseries = item.vseries
            volume = item.volume
        else:
            issue = item
            year = issue.year
            vseries = issue.vseries
            volume = issue.volume

        if issue.pid != issue_to_appear_pid:
            fyear, lyear = get_first_last_years(year)
            fyear = int(fyear)
            lyear = int(lyear)

            # new volume found, we need to add issues_in_volume in the current volumes_in_veries
            if (
                results_volume != volume
                or (results_volume == "" and results_year != year)
                # or (results_volume == volume and results_year != year)
            ):
                # The first time, we simply add the issue in issues_in_volume (see below)
                # But we do not append in volumes_in_vseries
                if results_volume != "-1":
                    volumes_in_vseries.append(
                        {
                            "volume": results_volume,
                            "fyear": results_fyear,
                            "lyear": results_lyear,
                            "issues": issues_in_volume,
                        }
                    )
                    # Clear issues_in_volume to prepare a new volume
                    issues_in_volume = []

                # Set the current volume info
                results_volume = volume
                results_year = year
                results_fyear = fyear
                results_lyear = lyear
                volume_count += 1
                if width > max_width:
                    max_width = width
                width = 0

            # New vseries found, we need to add volumes_in_vseries to the current vseries
            if results_vseries != vseries:
                # The first time, we do not append in issues_in_vseries.
                # We simply set the vseries info and add the issues in issues_in_volume below
                if results_vseries != "-1":
                    issues_in_vseries.append(
                        {"vseries": results_vseries, "volumes": volumes_in_vseries}
                    )
                    volumes_in_vseries = []
                results_vseries = vseries
                max_volume_count = max(0, volume_count)
                volume_count = 0

            issues_in_volume.append(issue)

            # we have to create a list of publishers with date - for Gallica OAI
            if issue.my_publisher:
                if publishers:
                    item = publishers[-1]
                    if issue.my_publisher.pub_name != item["name"]:
                        item = {
                            "name": issue.my_publisher.pub_name,
                            "fyear": fyear,
                            "lyear": lyear,
                        }
                        publishers.append(item)
                    else:
                        if fyear < item["fyear"]:
                            item["fyear"] = fyear
                        if lyear > item["lyear"]:
                            item["lyear"] = lyear
                else:
                    publishers.insert(
                        0, {"name": issue.my_publisher.pub_name, "fyear": fyear, "lyear": lyear}
                    )

    # At the end of the loop, we need to append the remaining issues_in_volume
    # then volumes_in_vseries
    if results_volume != "-1" and issues_in_volume:
        volumes_in_vseries.append(
            {
                "volume": results_volume,
                "fyear": results_fyear,
                "lyear": results_lyear,
                "issues": issues_in_volume,
            }
        )
        # volume_count += 1

    if results_vseries != "-1" and volumes_in_vseries:
        issues_in_vseries.append({"vseries": results_vseries, "volumes": volumes_in_vseries})
        max_volume_count = max(0, volume_count)

    context = {
        "sorted_issues": issues_in_vseries,
        "volume_count": max_volume_count,
        "publishers": publishers,
        "max_width": max_width,
    }

    return context


def get_container(pid, prefetch=False, sites=True):
    try:
        if prefetch:
            container = (
                Container.objects.filter(sites__id=settings.SITE_ID, pid=pid)
                .prefetch_for_toc()
                .first()
            )
        else:
            if sites:
                container = Container.objects.get(sites__id=settings.SITE_ID, pid=pid)
            else:
                container = Container.objects.get(pid=pid)

    except Container.DoesNotExist:
        container = None
    return container


def get_book_serie(pid):
    try:
        book_serie = Collection.objects.get(
            pid=pid, coltype="book-series", sites__id=settings.SITE_ID
        )
    except Collection.DoesNotExist:
        book_serie = None
    return book_serie


def get_issues_count_in_collection(pid):
    try:
        collection = Collection.objects.get(pid=pid)
        issues = Container.objects.filter(my_collection=collection).count()
        return issues
    except Exception:
        pass


def get_issues_in_volume(vid, is_cr=False, general_articles=False):
    # 08/09/2022: vid is no longer built (see get_vid in models.py)
    # It is now the pid of the first issue of the volume
    first_issue = get_container(vid)
    if first_issue is None:
        raise Http404

    collection = first_issue.get_collection()
    year = first_issue.year
    vseries = first_issue.vseries
    volume = first_issue.volume

    if is_cr:
        year_int = int(year)
        if year_int > 2020 and collection.pid not in ["CRMATH", "CRBIOL"]:
            # CRAS: Les thématiques à partir de 2021 ont un number en "T1", "T2",...
            # On trie par number pour avoir les thématiques isolés des autres
            queryset = Container.objects.order_by("number")
        elif year_int > 2022 and collection.pid == "CRBIOL":
            queryset = Container.objects.order_by("number")
        else:
            queryset = Container.objects.order_by("number_int")
        if general_articles:
            queryset = queryset.filter(title_html="")
    else:
        queryset = Container.objects.order_by("number_int")
    queryset = queryset.filter(
        my_collection__pid=collection.pid, year=year, vseries=vseries, volume=volume
    ).prefetch_for_toc()
    issues = get_list_or_404(queryset)

    if is_cr and (
        (year_int > 2020 and collection.pid != "CRBIOL")
        or (year_int > 2022 and collection.pid == "CRBIOL")
    ):
        issues_articles = []
        grouped_articles = []
        grouped_issue_articles = {"issue": None, "articles": grouped_articles}
        for issue in issues:
            if len(issue.title_html) == 0:
                grouped_articles.extend(issue.article_set.all().order_by_sequence())
                grouped_issue_articles["issue"] = issue
            else:
                issues_articles.append(
                    {
                        "issue": issue,
                        "articles": issue.article_set.all().order_by_published_date()
                        if settings.SORT_ARTICLES_BY_DATE
                        else issue.article_set.all().order_by_sequence(),
                    }
                )
        if grouped_issue_articles["issue"] is not None:
            issues_articles.insert(0, grouped_issue_articles)
    else:
        issues_articles = [
            {
                "issue": issue,
                "articles": issue.article_set.all().order_by_published_date()
                if settings.SORT_ARTICLES_BY_DATE
                else issue.article_set.all().order_by_sequence(),
            }
            for issue in issues
        ]

    return issues_articles, collection


def get_resource_in_special_issue_by_doi(doi):
    try:
        resource_in_special_issue = ResourceInSpecialIssue.objects.get(resource_doi=doi)
    except ResourceInSpecialIssue.DoesNotExist:
        resource_in_special_issue = None
    return resource_in_special_issue


def get_article(pid: str, prefetch=False, sites=True) -> Article | None:
    try:
        if prefetch:
            article = (
                Article.objects.filter(sites__id=settings.SITE_ID, pid=pid).prefetch_all().first()
            )
        else:
            if sites:
                article = Article.objects.get(sites__id=settings.SITE_ID, pid=pid)
            else:
                article = Article.objects.get(pid=pid)

    except Article.DoesNotExist:
        article = None
    return article


def get_article_by_doi(doi, prefetch=False):
    try:
        if prefetch:
            article = (
                Article.objects.filter(sites__id=settings.SITE_ID, doi=doi).prefetch_all().first()
            )
        else:
            article = Article.objects.get(sites__id=settings.SITE_ID, doi=doi)

    except Article.DoesNotExist:
        article = None
    return article


def get_articles():
    article_qs = Article.objects.filter(sites__id=settings.SITE_ID).exclude(
        classname="TranslatedArticle"
    )
    return article_qs


def get_articles_by_deployed_date():
    sitemembership_qs = SiteMembership.objects.filter(
        site__id=settings.SITE_ID, resource__classname="Article"
    ).order_by("-deployed", "-seq")

    articles = [sm.resource.cast() for sm in sitemembership_qs]
    return articles


def get_articles_by_published_date():
    article_qs = Article.objects.filter(sites__id=settings.SITE_ID).exclude(
        classname="TranslatedArticle"
    )
    if hasattr(settings, "ISSUE_TO_APPEAR_PID"):
        article_qs = article_qs.exclude(my_container__pid=settings.ISSUE_TO_APPEAR_PID)
    article_qs = article_qs.order_by("-date_published", "-seq")

    return article_qs


def get_xmlbase(base):
    try:
        xmlbase = XmlBase.objects.get(base=base)
    except XmlBase.DoesNotExist:
        xmlbase = None
    return xmlbase


# RelationName are created with a fixture (see app/ptf/apps/ptf/fixtures/initial_data.json
# Example { "left" : "follows", "right" : "followed-by" }
# A related-article of an article has 1 relation name (ex "follows" or "followed-by")
# You need to know if the relation was stored in the left or right attribute of a RelationName,
# so that you can create/search the Relationship with the correct object/subject.
# Ex: with A "follows" B, A is the subject and B the object because "follows" is a RelationName.left attribute
# with A "followed-by" B, A is the object the B the subject because
# "followed-by" is a RelationName.right attribute
def get_relationname_left(left_name):
    try:
        relationname = RelationName.objects.get(left=left_name)
    except RelationName.DoesNotExist:
        relationname = None

    return relationname


def get_relationname_right(right_name):
    try:
        relationname = RelationName.objects.get(right=right_name)
    except RelationName.DoesNotExist:
        relationname = None

    return relationname


# See comments about RelationName above
def get_relationship(subject_pid, object_pid, relationname):
    try:
        relationship = Relationship.objects.get(
            subject_pid=subject_pid, object_pid=object_pid, rel_info=relationname
        )
    except Relationship.DoesNotExist:
        relationship = None

    return relationship


def get_extid(resource, id_type):
    extid = None
    extids = ExtId.objects.filter(resource=resource, id_type=id_type)
    if extids.count() > 0:
        extid = extids.first()

    return extid


def get_bibitemid(bibitem, id_type):
    bibitemid = None
    bibitemids = BibItemId.objects.filter(bibitem=bibitem, id_type=id_type)
    if bibitemids.count() > 0:
        bibitemid = bibitemids.first()

    return bibitemid


def get_bibitem_by_seq(article, seq):
    try:
        bibitem = article.bibitem_set.get(sequence=seq)
    except BibItem.DoesNotExist:
        bibitem = None
    return bibitem


def get_extlink(**filters):
    try:
        extlink = ExtLink.objects.get(**filters)
    except ExtLink.DoesNotExist:
        extlink = None
    return extlink


def get_related_object(**filters):
    """
    Return RelatedObject with filters pass by params (all are optionals)
    resource, base, rel, mimetype, location, metadata, seq
    Check models.py for the params of a RelatedObject
    """
    try:
        related_object = RelatedObject.objects.get(**filters)
        related_object.select_related()
    except RelatedObject.DoesNotExist:
        related_object = None
    return related_object


def get_authors_by_letter(first_letter):
    try:
        authors = Author.objects.filter(first_letter=first_letter).order_by("name")
    except Author.DoesNotExist:
        authors = None
    return authors


def make_key(string):
    n_string = unicodedata.normalize("NFKD", string).encode("ascii", "ignore").decode("ascii")
    n_string = re.sub(r"[^\w\s-]", "", n_string).strip().lower()
    n_string = re.sub(r"[-\s]+", "-", n_string)
    if len(n_string) > 64:
        n_string = n_string[:64]

    return n_string if n_string else unidecode(string)


def get_resource(pid: str, prefetch=False) -> Resource | None:
    try:
        if prefetch:
            resource = (
                Resource.objects.filter(sites__id=settings.SITE_ID, pid=pid).prefetch_all().first()
            )
        else:
            resource = Resource.objects.get(pid=pid, sites__id=settings.SITE_ID)
    except Resource.DoesNotExist:
        resource = None
    return resource


def get_resource_by_doi(doi, prefetch=False):
    try:
        if prefetch:
            resource = (
                Resource.objects.filter(sites__id=settings.SITE_ID, doi=doi).prefetch_all().first()
            )
        else:
            resource = Resource.objects.get(sites__id=settings.SITE_ID, doi=doi)

    except Resource.DoesNotExist:
        resource = None
    return resource


def get_random_containers():
    # TODO get the newest containers only

    containers = Container.objects.all()
    random_list = random.sample(containers, 6)

    return random_list


def parse_date_str(date_str):
    """
    @param date_str:a string representing a date. Ex: "2017-01-10T18:24:58.202+01:00", "2017-05-03"
    @return:a localized datetime object (localized means that the date has a timezone)
    """
    the_date = dateutil.parser.parse(date_str)
    if not timezone.is_aware(the_date):
        the_date = timezone.make_aware(the_date, datetime.UTC)
    return the_date


def get_issue_to_appear(colid):
    """
    Some journals want to display "articles to appear" with articles that have been accepted but are not yet
    finalized (pages start at 1). ex: AIF
    :param pid:
    :return: The container object of articles to appear
    """
    pid = ""

    if hasattr(settings, "ISSUE_TO_APPEAR_PIDS"):
        if colid in settings.ISSUE_TO_APPEAR_PIDS:
            pid = settings.ISSUE_TO_APPEAR_PIDS[colid]

    container = get_container(pid=pid, prefetch=True)
    return container


def get_number_from_doi(doi):
    value = 0

    try:
        index = doi.rfind(".")
        index += 1
        if index > 0:
            str_value = doi[index:]
            value = int(str_value)
    except BaseException:
        pass

    return value


def get_site_mersenne(collection_pid):
    key = collection_pid.lower()

    # TODO refactor smai-jcm, centre_mersenne to have collection_pid == key
    # Do not use a find here, we want an access time in O(1)
    if key == "smai-jcm":
        key = "smai"
    elif key == "mersenne":
        key = "centre_mersenne"
    elif key == "malsm":
        key = "mbk"

    try:
        site_item = settings.SITE_REGISTER[key]
    except KeyError:
        return None
    site_id = site_item["site_id"]
    site_acro = key

    return get_or_create_site(site_id, site_acro)


##########################################################################
#
# Update functions
#
##########################################################################
def post_resource_updated(resource):
    """
    A resource is modified (ex: matching), the last_modified date of its container has to be updated.
    :param resource:
    :return:
    """
    obj = resource.cast()
    container = obj.get_container()
    if container:
        container.last_modified = timezone.now()
        container.save()


def update_deployed_date(resource, site, deployed_date_in_prod_to_restore=None, file_=None):
    """
    Used by ptf_tools during DeployJatsIssue

    Update the SiteMembership deployed_date of container/site based on the production website.
      - If there is no deployed_date in ptf_tools (not yet in prod), we create one.
            - with deployed_date_in_prod if it's not None (case when we restore data), or
            - with now if deployed_date_in_prod is None (first deploy in prod)
      - If the last_modified date of the container in ptf_tools is > deployed_date_in_prod
        (we have a new version in ptf_tools), we update deployed_date_in_prod with now(),
      - else we update deployed_date with deployed_date_in_prod
         (Normally, they should be equal)

    :param resource:
    :param site:
    :param deployed_date_in_prod_to_restore:
    :param file_ file object to log info
    :return:
    """

    def get_deployed_date_in_prod(resource_, site_):
        deployed_date_in_prod = None
        try:
            membership = SiteMembership.objects.get(resource=resource_, site=site_)
            deployed_date_in_prod = membership.deployed
        except SiteMembership.DoesNotExist:
            pass

        return deployed_date_in_prod

    def update_or_create(resource_, site_, deployed):
        try:
            membership = SiteMembership.objects.get(resource=resource_, site=site_)
            membership.deployed = deployed
        except SiteMembership.DoesNotExist:
            membership = SiteMembership(resource=resource_, site=site_, deployed=deployed)
        membership.save()

    container = article = None

    if resource.classname == "Article":
        article = resource
        container = article.my_container
    else:
        container = resource

    existing_deployed_date = get_deployed_date_in_prod(container, site)

    # If we restore deployed_date, force the new value to the restored value
    if deployed_date_in_prod_to_restore:
        new_deployed_date = deployed_date_in_prod_to_restore
    else:
        # Get the existing deployed_date_in_prod (from SiteMembership)
        new_deployed_date = existing_deployed_date

        # If there is no value of if the current version (last_modified) is newer, update the date
        if new_deployed_date is None or container.last_modified > new_deployed_date:
            new_deployed_date = timezone.now()

    # Set the new value to the entire container/articles (+ collection)

    if file_:
        file_.write(
            "{}. Date to restore: {}. Container.last_modified: {}, Existing date {}. New date {}\n".format(
                container.pid,
                deployed_date_in_prod_to_restore,
                container.last_modified,
                existing_deployed_date,
                new_deployed_date,
            )
        )

    update_or_create(container, site, new_deployed_date)
    update_or_create(container.get_collection(), site, new_deployed_date)

    if article is not None:
        update_or_create(article, site, new_deployed_date)
    else:
        for article in container.article_set.all():
            update_or_create(article, site, new_deployed_date)


def assign_doi(pid):
    """
    In the Mersenne process, articles imported for the first time receive a DOI.
    Thus function creates a new DOI, based on the last doi stored in the Collection object.
    :param pid:
    :return: A new DOI, equal to 10.5802/@pid.(last_doi + 1)
    """
    collection = get_collection(pid)

    if collection is None:
        return None

    last_doi = collection.last_doi + 1
    collection.last_doi = last_doi
    collection.save()

    doi = "10.5802/" + pid.lower() + "." + str(last_doi)
    return doi


# TODO make a command ?


def add_or_update_extid(
    resource, id_type, id_value, checked, false_positive, update_last_modified=True
):
    from ptf.cmds import database_cmds

    if id_value:
        extid = get_extid(resource, id_type)
        if extid:
            if not extid.checked:
                extid.id_value = id_value
                extid.checked = checked
                extid.false_positive = false_positive
                extid.save()
        else:
            cmd = database_cmds.addExtIdDatabaseCmd(
                {
                    "id_type": id_type,
                    "id_value": id_value,
                    "checked": checked,
                    "false_positive": false_positive,
                }
            )
            cmd.set_resource(resource)
            cmd.do()

        # last_modified is not modified during data restoration (importExtraDataPtfCmd)
        if update_last_modified:
            post_resource_updated(resource)


def add_or_update_bibitemid(
    bibitem, id_type, id_value, checked, false_positive, update_last_modified=True
):
    from ptf.cmds import database_cmds
    from ptf.cmds import xml_cmds

    if id_value:
        bibitemid = get_bibitemid(bibitem, id_type)
        if bibitemid:
            bibitemid.id_value = id_value
            bibitemid.checked = checked
            bibitemid.false_positive = false_positive
            bibitemid.save()
        else:
            cmd = database_cmds.addBibItemIdDatabaseCmd(
                {
                    "id_type": id_type,
                    "id_value": id_value,
                    "checked": checked,
                    "false_positive": false_positive,
                }
            )
            cmd.set_bibitem(bibitem)
            cmd.do()

        # Update citation_xml|html|tex
        cmd = xml_cmds.updateBibitemCitationXmlCmd()
        cmd.set_bibitem(bibitem)
        cmd.do()

        # last_modified is not modified during data restoration (importExtraDataPtfCmd)
        if update_last_modified:
            post_resource_updated(bibitem.resource)


def get_site_id(collection_id):
    result = [v for k, v in SITE_REGISTER.items() if v["collection_pid"] == collection_id]
    return result[0]["site_id"] if result else ""


def get_collection_id(site_id):
    result = [v for k, v in SITE_REGISTER.items() if v["site_id"] == site_id]
    return result[0]["collection_pid"] if result else ""


def get_site_default_language(site_id):
    result = [v for k, v in SITE_REGISTER.items() if v["site_id"] == site_id]
    if result and "default_lang" in result[0]:
        return result[0]["default_lang"]
    return None


def is_site_en_only(site_id):
    result = [v for k, v in SITE_REGISTER.items() if v["site_id"] == site_id]
    return result[0]["en_only"] if result else False


def is_site_fr_only(site_id):
    result = [v for k, v in SITE_REGISTER.items() if v["site_id"] == site_id]
    return result[0]["fr_only"] if result and "fr_only" in result[0] else False
