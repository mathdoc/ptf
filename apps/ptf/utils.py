import difflib
import html
import os
import re
import subprocess
import unicodedata

from bleach.css_sanitizer import ALLOWED_CSS_PROPERTIES
from bleach.css_sanitizer import CSSSanitizer
from bleach.sanitizer import Cleaner
from PIL import Image
from PIL import ImageFile

ImageFile.LOAD_TRUNCATED_IMAGES = True

from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.core.mail import EmailMultiAlternatives
from django.template import Template
from django.template import TemplateSyntaxError
from django.template import engines
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.utils.translation import gettext_lazy as _

from ptf.site_register import SITE_REGISTER


def strip_markup(string):
    """
    Strip string from :
    - xml markkup (mathml, html, etc..)
    - html entities (&nbsp, etc...)
    """
    cleanr = re.compile("<.*?>|&([a-z0-9]+|#[0-9]{1,6}|#x[0-9a-f]{1,6});")
    return re.sub(cleanr, "", string)


def highlight_diff(ours, theirs):
    matcher = difflib.SequenceMatcher(
        None, strip_markup(ours.lower()), strip_markup(theirs.lower())
    )

    def process_tag(tag, i1, i2, j1, j2):
        if tag == "equal":
            return f"<span class='bg-success'>{matcher.b[j1:j2]}</span>"
        elif tag == "replace":
            return f"<span class='bg-dark'>{matcher.b[j1:j2]}</span>"
        else:
            return matcher.b[j1:j2]

    return "".join(process_tag(*t) for t in matcher.get_opcodes())


def volume_display():
    if settings.VOLUME_STRING:
        return "Volume"
    else:
        return _("Tome")


def execute_cmd(cmd, force_execute=False):
    """
    excute shell command
    @param cmd: str which represents shell command
    @return: output of the command
    """
    if settings.MERSENNE_CREATE_FRONTPAGE or force_execute:
        result = subprocess.check_output(cmd, shell=True)
        return result

    else:
        # pour debug
        returnStatus = 0
        output = cmd
        with open(os.path.join(settings.LOG_DIR, "cmd.log"), "a", encoding="utf-8") as file_:
            file_.write(f"cmd : {cmd}\n")
        return returnStatus, output


def get_file_content_in_utf8(filename):
    """

    :param filename:
    :return: the body of a utf-8 file
    """
    with open(filename, encoding="utf-8") as f:
        body = f.read()
    return body


def pdf_to_text(pdf_filename):
    # Extract full text from the PDF
    if not settings.MERSENNE_CREATE_FRONTPAGE:
        return ""
    else:
        os.makedirs(settings.MERSENNE_TMP_FOLDER, exist_ok=True)

        txt_filename = os.path.join(settings.MERSENNE_TMP_FOLDER, "fulltext.txt")
        cmd_str = "pdftotext -raw -nopgbrk -enc UTF-8 " + pdf_filename + " " + txt_filename

        execute_cmd(cmd_str)
        # Check if the output file has been created
        if not os.path.isfile(txt_filename):
            raise RuntimeError("The PDF file was not converted by pdftotext")

        body = get_file_content_in_utf8(txt_filename)
        # strip control characters
        body = "".join(ch for ch in body if unicodedata.category(ch)[0] != "C")

        return body


def linearize_pdf(from_path, to_path):
    # Linearize the PDF

    cmd_str = "qpdf --linearize " + from_path + " " + to_path

    try:
        subprocess.check_output(cmd_str, shell=True)
    except Exception as e:
        if not os.path.isfile(to_path):
            raise e

    do_copy = False
    return do_copy


def get_display_name(prefix, first_name, last_name, suffix, string_name):
    display_first_name_first = getattr(settings, "DISPLAY_FIRST_NAME_FIRST", False)

    list_name = [x for x in [last_name, first_name] if x.strip()]
    if display_first_name_first and (last_name or first_name):
        string_name = " ".join(list_name[::-1])
    elif last_name or first_name:
        string_name = f"{prefix} " if prefix else ""
        string_name += ", ".join(list_name)
        string_name += f" {suffix}" if suffix else ""

    return string_name


def ckeditor_input_sanitizer(html: str, allow_img: bool = False) -> str:
    """
    Sanitizes HTML input from the CKEditor.
    It uses bleach library (https://bleach.readthedocs.io/en/latest/index.html), an allowed-list-based sanitizer.
    JavaScript is removed by allowing only a subset of HTML tags and attributes.
    It does not make use of `lxml.html.Cleaner` because the documentation clearly says that this is not a secure
    approach.

    html: str
        The HTML string to sanitize.
    allow_img : bool
        Whether to preserve img related tags
    """
    allowed_tags = [
        "a",
        "abbr",
        "acronym",
        "address",
        "aside",
        "b",
        "bdi",
        "bdo",
        "blockquote",
        "br",
        "caption",
        "cite",
        "code",
        "dd",
        "del",
        "dfn",
        "div",
        "dl",
        "dt",
        "em",
        "h1",
        "h2",
        "h3",
        "h4",
        "h5",
        "h6",
        "hgroup",
        "hr",
        "i",
        "ins",
        "kbd",
        "li",
        "mark",
        "ol",
        "p",
        "pre",
        "q",
        "s",
        "samp",
        "small",
        "span",
        "strike",
        "strong",
        "sub",
        "table",
        "tbody",
        "td",
        "th",
        "thead",
        "tr",
        "u",
        "ul",
        "var",
    ]

    allowed_attributes = {
        "*": ["class", "dir", "style", "id", "name"],
        "a": ["href", "target"],
        "img": ["alt", "height", "src", "width"],
        "source": ["type", "src"],
        # Those table attributes are deprecated but they are still used by CKEditor 4
        # We might consider upgrading/migrating to CKEditor 5 at some point
        "table": ["align", "border", "align", "cellspacing", "cellpadding"],
        "th": ["scope"],
    }

    additional_css_properties = [
        "border",
        "margin",
        "margin-left",
        "margin-right",
        "margin-top",
        "margin-bottom",
        "padding",
        "padding-left",
        "padding-right",
        "padding-top",
        "padding-bottom",
    ]

    allowed_css_properties = ALLOWED_CSS_PROPERTIES | set(additional_css_properties)

    image_allowed_tags = ["figcaption", "figure", "img", "picture", "source"]

    if allow_img:
        allowed_tags += image_allowed_tags

    css_sanitizer = CSSSanitizer(allowed_css_properties=allowed_css_properties)
    cleaner = Cleaner(
        tags=allowed_tags, attributes=allowed_attributes, css_sanitizer=css_sanitizer, strip=True
    )
    return cleaner.clean(html)


def send_email(
    html_content: str,
    subject: str,
    to: list[str] | tuple[str],
    from_email: str | None = None,
    cc: list[str] | tuple[str] = [],
    from_collection: str = "",
    reply_to: list[str] | tuple[str] = [],
) -> None:
    """
    Sends an e-mail to the provided recipients and copy recipients with the provided html content.
    It sends the e-mail with both a text and a HTML alternative.
    If not provided, the sender's e-mail address default to `settings.DEFAULT_FROM_EMAIL`
    Params:
        - html_content      The HTML content of the e-mail
        - subject           The e-mail's subject
        - from_email        The sender's e-mail address
        - to                The list or tuple of the e-mail recipients
        - cc                The list or tuple of the e-mail CC
        - from_collection:  The collection to send the mail for. If from_email is None
                            it will get the email from site_register.py (`email_from`).
    """
    if from_email == "":
        try:
            from_email = SITE_REGISTER[from_collection.lower()]["email_from"]
        except (KeyError, ValueError):
            if from_collection:
                raise ImproperlyConfigured(
                    f"The collection {from_collection.lower()} is missing the "
                    "email_from property in site_register.py"
                )

    # We additionally unescape HTML characters here to avoid having stuff like
    # &nbsp; &gt; etc. in the output text.
    text_content = html.unescape(strip_tags(html_content))
    # Create the email, and attach the HTML version as well.
    return_path = getattr(settings, "RETURN_PATH", "no-reply@listes.mathdoc.fr")
    msg = EmailMultiAlternatives(
        subject=subject,
        body=text_content,
        from_email=from_email,
        to=to,
        cc=cc,
        headers={"Return-path": return_path},
        reply_to=reply_to,
    )
    msg.attach_alternative(html_content, "text/html")
    msg.send(fail_silently=False)


def send_email_from_template(
    template: str,
    context_data: dict,
    subject: str,
    to: list[str] | tuple[str],
    from_email: str = "",
    cc: list[str] | tuple[str] = [],
    from_collection: str = "",
) -> None:
    """
    Renders the provided template and sends it as an e-mail to the
    provided recipients and copy recipients.
    It sends the e-mail with both a text and a HTML alternative.
    If not provided, the sender's e-mail address default to `settings.DEFAULT_FROM_EMAIL`
    Params:
        - template          The HTML template of the e-mail
        - context_data      The context data used to render the template
        - subject           The e-mail's subject
        - from_email        The sender's e-mail address
        - to                The list or tuple of the e-mail recipients
        - cc                The list or tuple of the e-mail CC
        - from_collection:  The collection to send the mail for. If from_email is None
                            it will get the email from site_register.py (`email_from`).
    """
    if from_email == "":
        try:
            from_email = SITE_REGISTER[from_collection.lower()]["email_from"]
        except (KeyError, ValueError):
            if from_collection:
                raise ImproperlyConfigured(
                    f"The collection {from_collection.lower()} is missing the "
                    "email_from property in site_register.py"
                )

    html_content = render_to_string(template, context_data)
    send_email(
        html_content, subject, to=to, from_email=from_email, cc=cc, from_collection=from_collection
    )


def template_from_string(template_string, using=None) -> Template:
    """
    Convert a string into a template object using a given template engine
    or using the default backends from `settings.TEMPLATES` if no engine was specified.
    """
    # This function is based on django.template.loader.get_template,
    # but uses Engine.from_string instead of Engine.get_template.
    engine_list = engines.all() if using is None else [engines[using]]
    for engine in engine_list:
        try:
            return engine.from_string(template_string)
        except TemplateSyntaxError:
            continue
    raise TemplateSyntaxError(template_string)


def resize_image(img, max_size=1600):
    """Take an image in argument and resize it to a {max_size} width with the same ratio"""

    if img.width > max_size:
        ratio = img.width / img.height
        new_width = max_size
        new__height = int(max_size / ratio)
        img = img.resize((new_width, new__height))

    return img


def convert_tiff_to_jpg(img):
    """Take the path of a '.tiff' image and convert the image to a '.jpg' one"""
    image_file = os.path.basename(img.filename)
    image_name = os.path.splitext(image_file)[0]
    image_directory = os.path.dirname(img.filename)
    if img.mode == "RGBA":
        img = img.convert("RGB")
    img.thumbnail(img.size)
    img.save(os.path.join(image_directory, image_name + ".jpg"), "JPEG", quality=100)


Image.Image.resize_image = resize_image
Image.Image.convert_tiff_to_jpg = convert_tiff_to_jpg


def convert_tiff_to_jpg_from_path(image_path):
    """Take the path of a '.tiff' image and convert the image to a '.jpg' one"""

    path = os.path.split(image_path)[0]
    image_file = os.path.basename(image_path)
    name = os.path.splitext(image_file)[0]

    img = Image.open(image_path)

    img.convert_tiff_to_jpg()

    final_path = os.path.join(path, name + ".jpg")
    img.save(final_path, "JPEG", quality=100)
    img.close()


def resize_image_from_path(image_path):
    img = Image.open(image_path)

    img = img.resize_image()

    img.save(os.path.join(image_path), quality=100)
    img.close()


def convert_image_for_web(image_path):
    image_file = os.path.basename(image_path)
    extension = os.path.splitext(image_file)[1]

    img = Image.open(image_path)
    if extension in [".tiff", ".tif"]:
        img.convert_tiff_to_jpg()
    elif extension in [".wmf", ".emf"]:
        return

    img = img.resize_image()
    img.close()


def create_citation_link_and_new_html(key, label, tooltip_html=""):
    highlight_id = f"'r{label}'"
    citation_link = f'<a id="{label}" href="#r{label}" onclick="highlightReference({highlight_id}, 3000, 500)" >{key}</a>'
    new_html = f'<span class="tooltipPCJ">{citation_link}{tooltip_html}</span>'
    return new_html


def create_innerlink_for_citation(html_text, biblio):
    html_text = html_text.replace("\n", " ")
    for key, value in biblio.items():
        label = value["label"].replace("[", "").replace("]", "")
        # highlight_id = f"'r{label}'"
        reference = value["reference"]
        tooltip_html = (
            '<span style="position: absolute; visibility: hidden" class="tooltip tooltiptexthidden">'
            + f"{reference}"
            + "</span>"
        )
        new_html = create_citation_link_and_new_html(key, label, tooltip_html=tooltip_html)
        html_text = html_text.replace(f"{key}", new_html)
        if key in html_text:
            continue

        key2 = key.replace("&", "&amp;")
        new_html = create_citation_link_and_new_html(key2, label, tooltip_html=tooltip_html)
        html_text = html_text.replace(f"{key2}", new_html)
        if key2 in html_text:
            continue

        key3 = key.replace("&", "and")
        new_html = create_citation_link_and_new_html(key3, label, tooltip_html=tooltip_html)
        html_text = html_text.replace(f"{key3}", new_html)
        if key3 in html_text:
            continue

        key4 = key3.replace(",", "")
        new_html = create_citation_link_and_new_html(key4, label, tooltip_html=tooltip_html)
        html_text = html_text.replace(f"{key4}", new_html)
        if key4 in html_text:
            continue

        key5 = key.replace(",", "")
        new_html = create_citation_link_and_new_html(key5, label, tooltip_html=tooltip_html)
        html_text = html_text.replace(f"{key5}", new_html)
        if key5 in html_text:
            continue

        key6 = key2.replace(",", "")
        new_html = create_citation_link_and_new_html(key6, label, tooltip_html=tooltip_html)
        html_text = html_text.replace(f"{key6}", new_html)
        if key6 in html_text:
            continue
        pass

    return html_text
