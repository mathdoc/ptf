from ptf.site_register import SITE_REGISTER

DOI_BASE_URL = "https://doi.org/"
ARTICLE_BASE_URL = "/article/"
ISSUE_BASE_URL = "/issue/"
ICON_BASE_URL = "/icon/"
SOLR_URL = "http://127.0.0.1:8983/solr/core0"

MIGRATION_MODULES = {
    "sites": "ptf.fixtures.sites_migrations",
}

DEFAULT_AUTO_FIELD = "django.db.models.AutoField"

SENDFILE_BACKEND = "django_sendfile.backends.xsendfile"

DATA_UPLOAD_MAX_MEMORY_SIZE = 40000000
MATHDOC_ARCHIVE_FOLDER = "/mathdoc_archive"
MERSENNE_PROD_DATA_FOLDER = "/mersenne_prod_data"
CEDRAM_XML_FOLDER = "/cedram_dev/exploitation/cedram"
CEDRAM_TEX_FOLDER = "/cedram_dev/production_tex/CEDRAM"
CEDRAM_DISTRIB_FOLDER = "/cedram_dev/production/distrib"
NUMDAM_ISSUE_SRC_FOLDER = "/numdam_dev/numerisation/donnees_validees"
NUMDAM_ARTICLE_SRC_FOLDER = "/numdam_dev/raffinement"
NUMDAM_DATA_ROOT = "/numdam_data"

MERSENNE_COLLECTIONS = [
    "ACIRM",
    "AFST",
    "AHL",
    "AIF",
    "ALCO",
    "AMBP",
    "ART",
    "CCIRM",
    "CML",
    "CR",
    "CRBIOL",
    "CRGEOS",
    "CRMATH",
    "CRCHIM",
    "CRMECA",
    "CRPHYS",
    "IGT",
    "JEDP",
    "JEP",
    "JTNB",
    "MRR",
    "MSIA",
    "OGEO",
    "OJMO",
    "PCJ",
    "PMB",
    "ROIA",
    "SMAI-JCM",
    "TSG",
    "SLSEDP",
    "WBLN",
    "XUPS",
    "MALSM",
]
NUMDAM_COLLECTIONS = ["CG"]

MAX_RESULT_SIZE = 100

# The volume number can be used for Next/previous breadcrumb buttons
# CONTAINER_SEQUENCED = False
CONTAINER_SEQUENCED_BY_YEAR = (
    False  # The sequence comes from the year and not from the volume number
)
COLLECTIONS_SEQUENCED = ["AHL", "JEP", "SLSEDP", "XUPS"]
CRAS_COLLECTIONS = ["CRMATH", "CRMECA", "CRPHYS", "CRGEOS", "CRCHIM", "CRBIOL"]

# Display TOC by dates instead of sequence
SORT_ARTICLES_BY_DATE = False
DISPLAY_LATEST_ARTICLES = False

NUMDAM_MATCHING_URL = "http://www.numdam.org"
CROSSREF_BASEURL = "https://test.crossref.org/"
CROSSREF_CHECKDOI_URL = "https://api.crossref.org/works/{}"
TRANSLATION_URL = "https://translation.centre-mersenne.org"

VOLUME_STRING = False

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    # 'ptf.middleware.ForceDefaultLanguageMiddleware',
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "django.contrib.sites.middleware.CurrentSiteMiddleware",
]

CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.filebased.FileBasedCache",
        "LOCATION": "/var/log/mersenne/django_cache",
        "OPTIONS": {"MAX_ENTRIES": 60000},
    }
}

LOG_DIR = "/var/tmp/mersenne"
LOCK_FILE = "/var/www/mersenne_shared/lock.txt"

LOGOUT_REDIRECT_URL = "/"

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "filters": {"require_debug_false": {"()": "django.utils.log.RequireDebugFalse"}},
    "handlers": {
        "mail_admins": {
            "level": "ERROR",
            "filters": ["require_debug_false"],
            # 'class': 'django.utils.log.AdminEmailHandler'
            "class": "ptf.log_utils.ThrottledAdminEmailHandler",
        }
    },
    "loggers": {
        "django.request": {
            "handlers": ["mail_admins"],
            "level": "ERROR",
            "propagate": False,
        },
    },
}

# The cms_tags templatetags is defined in ptf and overridden in mersenne_cms
# (the pytests test some Views that load some HTML pages: the get_all_pages cms_tags gets called by top.html
#  We could/should merge mersenne_cms in ptf ? Instead, an empty get_all_pages is defined in ptf)
# Django gives a warning that we silence
SILENCED_SYSTEM_CHECKS = ["templates.E003"]
