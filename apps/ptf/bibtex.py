import re

from pylatexenc.latex2text import LatexNodes2Text
from pylatexenc.latexencode import unicode_to_latex
from unidecode import unidecode

from ptf import models


def protect_uppercase(line):
    l = []

    found_closing_brace = False
    i = line.rfind("},")
    if i == len(line) - 2:
        found_closing_brace = True
        line = line[: len(line) - 2]

    i = line.find("{")
    if i > 0:
        i += 1
        l.append(line[:i])
    else:
        i = 0

    first_word = True

    while i < len(line):
        while i < len(line) and line[i] in [" ", "\t"]:
            l.append(line[i])
            i += 1

        if i < len(line):
            j = i

            found_uppercase = False

            if line[j] == "$":
                # Skip formulas
                j += 1
                while j < len(line) and line[j] != "$":
                    j += 1
                if j < len(line) and line[j] == "$":
                    j += 1
            else:
                while j < len(line) and line[j] not in [" ", "\t", "$"]:
                    if "A" <= line[j] <= "Z":
                        found_uppercase = True
                    j += 1

            if found_uppercase and not first_word:
                l.append("{" + line[i:j] + "}")
            else:
                l.append(line[i:j])

            first_word = False
            i = j

    if found_closing_brace:
        l.append("},")
    new_line = "".join(l)

    return new_line


def append_in_latex(array, unicode_line, is_title=False):
    if is_title:
        line = unicode_line.replace("<i>", "|||i|||").replace("</i>", "|||/i|||")
        line = line.replace("<sup>", "|||sup|||").replace("</sup>", "|||/sup|||")
        line = line.replace("<sub>", "|||sub|||").replace("</sub>", "|||/sub|||")
        line = unicode_to_latex(line, non_ascii_only=True)
        line = line.replace("|||i|||", "\\protect\\emph{").replace("|||/i|||", "}")
        line = line.replace("|||sup|||", "\\protect\\textsuperscript{").replace("|||/sup|||", "}")
        line = line.replace("|||sub|||", "\\protect\\textsubscript{").replace("|||/sub|||", "}")
    else:
        line = unicode_to_latex(unicode_line, non_ascii_only=True)

    if is_title:
        line = protect_uppercase(line)

    array.append(line)


def get_bibtex_names(resource, role):
    contribs = " and ".join(
        [
            str(contribution)
            for contribution in resource.contributions.all()
            if contribution.role == role
        ]
    )
    line = f"{role} = {{{contribs}}}," if contribs else ""
    return line


def get_bibtex_id(resource, year):
    # Set the id to the last name of first author + year
    # The last name needs to be converted to ASCII (with unidecode) and put back in unicode (get_bibtex returns unicode)
    authors = models.get_names(resource, "author")
    if not authors:
        return ""

    first_author = authors[0]
    ascii_name = unidecode(re.split(", .", first_author)[0]).lower()
    regex = re.compile(r"[^a-zA-Z]")
    ascii_name = regex.sub("", ascii_name)
    return ascii_name + year.split("-")[0]


def parse_bibtex(text, math_mode="verbatim"):
    entries = []
    parser = LatexNodes2Text(math_mode=math_mode)
    if text and "@" in text:
        bibitems = re.split(r"\s*\n*@", text)
        for item in bibitems:
            result = re.match(r"(.*){.*,(\s*)", item)
            if result:
                entry = {"doctype": result.group(1).strip().lower()}  # article, inbook...
                for line in item.splitlines():
                    field = line.split(" = ")
                    if field and len(field) == 2:
                        key = field[0].strip().lower()
                        val = field[1].strip(" ,")
                        entry[key] = parser.latex_to_text(val)
                entries.append(entry)
    return entries
