"""
See :
    - https://docs.djangoproject.com/en/2.2/topics/http/middleware/
    - https://gist.github.com/vstoykov/1366794
"""


class ForceDefaultLanguageMiddleware:
    """
    Ignore Accept-Language HTTP headers

    This will force the I18N machinery to always choose settings.LANGUAGE_CODE
    as the default initial language, unless another one is set via sessions or cookies

    Should be installed *before* any middleware that checks request.META['HTTP_ACCEPT_LANGUAGE'],
    namely django.middleware.locale.LocaleMiddleware
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if "accept-language" in request.headers:
            del request.META["HTTP_ACCEPT_LANGUAGE"]

        response = self.get_response(request)
        return response
