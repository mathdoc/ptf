from django.conf import settings
from django.http import Http404

from .models import Article
from .models import Collection
from .models import Container
from .models import Resource

try:
    USE_PID = settings.USE_NATURAL_ID
except AttributeError:
    USE_PID = False


def get_article(aid):
    manager = Article.objects
    if USE_PID:
        try:
            article = (
                manager.select_related()
                .prefetch_related(
                    "bibitem_set__bibitemid_set",
                    "resourceid_set",
                    "extid_set",
                    "extlink_set",
                    "datastream_set",
                    "contribgroup_set__contrib_set",
                    "subject_of",
                    "abstract_set",
                )
                .get(pid=aid, sites__id=settings.SITE_ID)
            )
        except Article.DoesNotExist:
            raise Http404
    else:
        try:
            article = (
                manager.select_related()
                .prefetch_related(
                    "bibitem_set__bibitemid_set",
                    "resourceid_set",
                    "extid_set",
                    "extlink_set",
                    "datastream_set",
                    "contribgroup_set__contrib_set",
                    "subject_of",
                    "abstract_set",
                )
                .get(id=aid, sites__id=settings.SITE_ID)
            )
        except Article.DoesNotExist:
            raise Http404
    return article


def get_issue(iid):
    qs = Container.objects.select_related("my_collection")
    if USE_PID:
        try:
            issue = qs.prefetch_related(
                "article_set__contribgroup_set__contrib_set", "article_set__subject_of"
            ).get(pid=iid, ctype="issue", sites__id=settings.SITE_ID)
        except Container.DoesNotExist:
            raise Http404
    else:
        try:
            issue = qs.prefetch_related(
                "article_set__contribgroup_set__contrib_set", "article_set__subject_of"
            ).get(id=iid, ctype="issue", sites__id=settings.SITE_ID)
        except Container.DoesNotExist:
            raise Http404
    return issue


# def get_issue(iid):
#     qs = Container.objects.select_related('serial')
#     if USE_PID:
#         try:
#             issue = qs.get(pid=iid, ctype='issue',
#                             sites__id=settings.SITE_ID)
#         except Container.DoesNotExist:
#             raise Http404
#     else:
#         try:
#             issue = qs.get(id=iid, ctype='issue',
#                            sites__id=settings.SITE_ID)
#         except Container.DoesNotExist:
#             raise Http404
#     return issue


def get_journal(jid):
    if USE_PID:
        try:
            journal = Collection.objects.get(pid=jid, sites__id=settings.SITE_ID)
        except Collection.DoesNotExist:
            raise Http404
    else:
        try:
            journal = Collection.objects.get(id=jid, sites__id=settings.SITE_ID)
        except Collection.DoesNotExist:
            raise Http404
    return journal


def get_book(bid):
    if USE_PID:
        try:
            book = Container.objects.get(
                pid=bid, ctype__startswith="book", sites__id=settings.SITE_ID
            )
        except Container.DoesNotExist:
            raise Http404
    else:
        try:
            book = Container.objects.get(
                id=bid, ctype__startswith="book", sites__id=settings.SITE_ID
            )
        except Container.DoesNotExist:
            raise Http404
    return book


# def get_book_serie(sid):
#     """
#     09/06/2021: Unused. Remove ? Miss lecture-notes
#     :param sid:
#     :return:
#     """
#     if USE_PID:
#         try:
#             series = Collection.objects.get(pid=sid, coltype="book-series", sites__id=settings.SITE_ID)
#         except Collection.DoesNotExist:
#             raise Http404
#     else:
#         try:
#             series = Collection.objects.get(id=sid, coltype="book-series", sites__id=settings.SITE_ID)
#         except Collection.DoesNotExist:
#             raise Http404
#     return series


def get_collection(sid):
    if USE_PID:
        try:
            col = Collection.objects.get(pid=sid, sites__id=settings.SITE_ID)
        except Collection.DoesNotExist:
            raise Http404
    else:
        try:
            col = Collection.objects.get(id=sid, sites__id=settings.SITE_ID)
        except Collection.DoesNotExist:
            raise Http404
    return col


def get_resource(rid):
    if USE_PID:
        try:
            ressource = Resource.objects.get(pid=rid, sites__id=settings.SITE_ID)
        except Resource.DoesNotExist:
            raise Http404
    else:
        try:
            ressource = Resource.objects.get(id=rid, sites__id=settings.SITE_ID)
        except Resource.DoesNotExist:
            raise Http404
    return ressource


# def get_serial(sid):
#     """
#     09/06/2021: Unused. Remove ? Miss lecture-notes
#     :param sid:
#     :return:
#     """
#     try:
#         serial = get_journal(sid)
#     except Http404:
#         serial = get_book_serie(sid)
#     return serial


def get_all_journals():
    qs = Collection.objects.filter(sites__id=settings.SITE_ID)
    return qs
