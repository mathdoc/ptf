import json

from django.test import TestCase

from ptf.bibtex import append_in_latex
from ptf.bibtex import get_bibtex_id
from ptf.bibtex import get_bibtex_names
from ptf.bibtex import parse_bibtex
from ptf.models import Contribution
from ptf.models import Resource


class BibtexTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        with open("tests/data/bibtex/biblio.tex") as f:
            cls.bibtex = f.read()
        with open("tests/data/bibtex/biblio.json") as f:
            cls.result = json.load(f)

        resource = Resource.objects.create(doi="10.5b")
        Contribution.objects.create(
            resource=resource,
            seq=0,
            first_name="John",
            last_name="Smith",
            role="author",
        )

    def setUp(self):
        self.array = []
        self.line = "<i>aa</i> <sup>éé</sup> $x^{2y}$ Adam <sub>cc#</sub>"

    def test_01_append_in_latex(self):
        """Append in latex list with some html tag replacements. Used for BibTeX export"""

        result_1 = r"\protect\emph{aa} \protect\textsuperscript{\'e\'e} $x^{2y}$ {Adam} \protect\textsubscript{cc#}"
        result_2 = r"<i>aa</i> <sup>\'e\'e</sup> $x^{2y}$ Adam <sub>cc#</sub>"
        append_in_latex(self.array, self.line, is_title=True)
        append_in_latex(self.array, self.line, is_title=False)
        self.assertEqual(self.array[0], result_1)
        self.assertEqual(self.array[1], result_2)

    def test_02_get_bibtex_names_alco_style(self):
        resource = Resource.objects.get(doi="10.5b")
        self.assertEqual(get_bibtex_names(resource, "author"), "author = {Smith, John},")

    def test_03_get_bibtex_id_alco_style(self):
        resource = Resource.objects.get(doi="10.5b")
        self.assertEqual(get_bibtex_id(resource, "2023"), "smith2023")

    def test_04_parse_bibtex(self):
        self.assertEqual(parse_bibtex(self.bibtex), self.result)
