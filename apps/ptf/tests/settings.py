import os
import socket

from ptf.settings import *


INSTALLED_APPS = [
    "tests",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sites",
    "ptf",
]

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
APPS_DIR = os.path.dirname(BASE_DIR)

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [
            "%s/ptf/templates/ptf/bs5" % APPS_DIR,
            "%s/oai/templates" % APPS_DIR,
            BASE_DIR,
            "%s/" % APPS_DIR,
        ],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "ptf.context_processors.ptf",
            ],
        },
    },
]

DATABASES = {}
DATABASES["default"] = {"ENGINE": "django.db.backends.sqlite3", "NAME": "db_name.sqlite3"}

ROOT_URLCONF = "ptf.tests.urls"
SOLR_URL = "http://127.0.0.1:8983/solr/core0-test"
# SOLR_URL = os.environ.get("SOLR_URL",
#                           'http://gricad-registry.univ-grenoble-alpes.fr-tienyoun-ptf-tests-solr:8983/solr/core0-test')

RESOURCES_ROOT = "tests/data"
TEMP_FOLDER = "/tmp"
MERSENNE_TMP_FOLDER = "/tmp"
MERSENNE_LOG_FILE = MERSENNE_TMP_FOLDER + "/transformations.log"
MERSENNE_TEST_DATA_FOLDER = "/tmp"
LOG_DIR = "/tmp"
SENDFILE_ROOT = ""

ALLOW_ADMIN = True
MAX_RESULT_SIZE = 7
REPOSITORIES = {"http://127.0.0.1/oai/": "PtfRepository"}

SECRET_KEY = "dsfqsdlkfjlkqjsdhokqdfhgkdqfhgildfhgidfhgiqdfhghdfghsfiughel"

TIME_ZONE = "UTC"
USE_I18N = True
USE_TZ = True
LANGUAGE_CODE = "en"

SITE_NAME = "test"
SITE_ID = 777
SITE_DOMAIN = "www.test.mathdoc"
COLLECTION_PID = "ALL"

HOST_FQDN = socket.getfqdn()
# without / at the end
BASE_URL = "http://%s" % HOST_FQDN


def get_virtualenv_dir(base_dir, ptf_dir):
    return os.environ["VIRTUAL_ENV"]
