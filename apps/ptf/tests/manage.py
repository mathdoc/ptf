#!/usr/bin/env python
import os
import sys

import django
from django.conf import settings
from django.test.utils import get_runner


if __name__ == "__main__":

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "tests.test_settings")
    # os.environ.setdefault("DJANGO_CONFIGURATION", "Test")

    sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

    curr_dir = os.path.join(os.path.dirname(__file__))

    print(curr_dir)
    os.chdir(os.path.join(os.path.dirname(__file__)))

    print(os.listdir("tests/data"))

    try:
        from django.core.management import execute_from_command_line
    except ImportError:
        try:
            import django
        except ImportError:
            raise ImportError(
                "Couldn't import Django. Are you sure it's installed and "
                "available on your PYTHONPATH environment variable? Did you "
                "forget to activate a virtual environment?"
            )
        raise

    execute_from_command_line(["runtests", "migrate"])

    django.setup()
    TestRunner = get_runner(settings)
    test_runner = TestRunner()
    failures = test_runner.run_tests(["tests"])
    sys.exit(bool(failures))

