import re

from ptf.utils import ckeditor_input_sanitizer


def test_ckeditor_input_sanitier():
    # Safe data coming from the CKEditor that should be left untouched
    # Inline style and tex formula
    safe_data = """
    <p id="my-paragraph" name="my-paragraph" style="text-align: center;">
        Test all inline styles
        <b>Word</b>
        <cite>Word</cite>
        <code>Word</code>
        <del>Word</del>
        <em>Word</em>
        <i>Word</i>
        <ins>Word</ins>
        <kbd>Word</kbd>
        <mark>Word</mark>
        <q>Word</q>
        <s>Word</s>
        <samp>Word</samp>
        <strike>Word</strike>
        <strong>Word</strong>:
        <sub>Word</sub>
        <u>Word</u>
        <small>Word</small>
        <var>Word</var>
        <span>Word</span>
    </p>

    <p>HTML encoded characters: &iquest; &nbsp; &lt; &gt; &amp;.</p>

    <h1>Header</h1>
    <h2>Header</h2>
    <h3>Header</h3>
    <h4>Header</h4>
    <h5>Header</h5>
    <h6>Header</h6>

    <p>Test block styles</p>
    <address>Block</address>
    <aside>Block</aside>
    <blockquote>Block</blockquote>
    <pre>Block</pre>

    <p><span class="mathjax-formula">\\(x = {-b \\pm \\sqrt{b^2-4ac} \\over 2a}\\)</span></p>

    <ul style="background-color: yellow; margin: 5px 10px;">My list
        <li><i>First Element</i></li>
        <li><b>Second Element</b></li>
    </ul><br><br><hr>
    <ol>My ordered list
        <li><h1>One</h1></li>
        <li><h2>Two</h2></li>
    </ol>
    <a href="https://somehost.org" target="_blank">Some link</a>
    <a href="#SomeAnchorLink">Anchor link</a>
    <table align="right" border="1" cellpadding="1" cellspacing="1">
        <caption>My table</caption>
        <thead>
            <tr><th scope="col">Column 1 title</th><th scope="col">Column 2 title</th></tr>
        </thead>
        <tbody>
            <tr><th scope="row">Row 1 title</th><td>Cell 1</td><td>Cell 2</td></tr>
        </tbody>
    </table>
    """
    assert ckeditor_input_sanitizer(safe_data) == safe_data

    # Test img tag is left untouched when accepted
    safe_data = '<img src="/media/uploads/uploaded_img.png"><img src="https://someimagesite.org">'
    assert ckeditor_input_sanitizer(safe_data, allow_img=True) == safe_data

    # Unsafe data that should be sanitized
    # Following tags should be completely removed
    tags_to_be_removed = [
        '<form action="https://evilformsubmission"></form>',
        '<iframe src="https://eviliframe.org">My embedded frame</iframe>',
        '<img src="https://evilimage.org>',
        "<input hidden=true></input>",
        "<html><script>My evil script</script></html>",
        '<link rel="stylesheet" href="http://evilstylesheet.org">',
        '<meta content="Redefine document meta">',
        "<script>My evil script</scrip>",
        "<style>body { display: none; }</style>",
        "<textarea>Evil input</textarea>",
        '<video src="https://youtube.com/my_video"></video>' "<wrongtag>",
        "<wrongclosingtag></wrongclosingtag>",
    ]
    for unsafe_data in tags_to_be_removed:
        assert re.sub(r"<.*?>", "", unsafe_data) == ckeditor_input_sanitizer(unsafe_data)

    # Test unallowed tag within allowed tagbackground:url('')</script></span>'
    unsafe_data = "<span><script>My evil script</script></span>"
    assert ckeditor_input_sanitizer(unsafe_data) == "<span>My evil script</span>"

    # Test unallowed attributes
    unsafe_data = """<div style="background:url('javascript:alert(\'XSS\')')"></div>"""
    assert ckeditor_input_sanitizer(unsafe_data) == '<div style=""></div>'

    unsafe_data = """<div onclick="javascript:alert('XSS')"></div>"""
    assert ckeditor_input_sanitizer(unsafe_data) == "<div></div>"

    # Test unallowed protocol
    unsafe_data = """<a href="javascript:alert('XSS')"></a>"""
    assert ckeditor_input_sanitizer(unsafe_data) == "<a></a>"
