from ..templatetags.helpers import get_doi_url
from ..templatetags.helpers import get_flag_icon
from ..templatetags.helpers import pretty_search
from ..templatetags.helpers import short_doi
from ..templatetags.partition import columns
from ..templatetags.partition import rows
from ..templatetags.stringsfilter import eudml_filter
from ..templatetags.stringsfilter import remove_email


def test_rows():
    num_range = range(4)

    assert rows(num_range, 2) == [[0, 1], [2, 3]]
    assert rows(num_range, 3) == [[0, 1], [2, 3], []]
    assert rows(num_range, 5) == [[0], [1], [2], [3], []]
    assert rows(num_range, "") == [num_range]


def test_columns():
    num_range = range(7)

    assert columns(num_range, 3) == [[0, 3, 6], [1, 4], [2, 5]]
    assert columns(num_range, 5) == [[0, 2, 4, 6], [1, 3, 5]]
    assert rows(num_range, "") == [num_range]


def test_pretty_search():
    assert pretty_search("search", "c", '"Sullivan, Dennis"') == '/search?"Sullivan, Dennis"-c'


def test_get_flag_icon():
    assert get_flag_icon("fr") == "flag-icon-fr"
    assert get_flag_icon("es") == "flag-icon-es"
    assert get_flag_icon("en") == "flag-icon-gb"


def test_short_doi():
    doi = "10.5802/alco.134"
    short = "alco.134"

    assert short_doi(doi) == short


def test_get_doi_url():
    doi = "10.5802/alco.134"

    assert get_doi_url(doi) == "https://doi.org/10.5802/alco.134"
    assert get_doi_url(doi[2:]) == ""


# @pytest.mark.django_db
# def test_get_addresses():
#     a = ArticleFactory()


def test_eudml_filter():
    values = "urn:eudml:doc:110093"

    assert eudml_filter(values) == "110093"


def test_remove_email():
    contrib = (
        "<contrib><name>"
        "<surname>Attouch</surname>"
        "<given-names>Hedy</given-names>"
        "</name><name-alternatives>"
        "<string-name specific-use='index'>Attouch, Hédy</string-name>"
        "</name-alternatives><email>attouch@math.univ-montp2.fr</email></contrib>"
    )
    value = (
        "<contrib><name>"
        "<surname>Attouch</surname>"
        "<given-names>Hedy</given-names>"
        "</name><name-alternatives>"
        "<string-name specific-use='index'>Attouch, Hédy</string-name>"
        "</name-alternatives></contrib>"
    )

    assert remove_email(contrib) == value
