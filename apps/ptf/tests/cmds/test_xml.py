import io
import os
from datetime import datetime

import dateutil.parser
from lxml import etree

from django.conf import settings
from django.core.management import call_command
from django.test import TestCase
from django.test.client import RequestFactory
from django.utils import timezone

from ptf import exceptions
from ptf import model_helpers
from ptf.cmds import database_cmds
from ptf.cmds import ptf_cmds
from ptf.cmds import solr_cmds
from ptf.cmds import xml_cmds
from ptf.models import Abstract
from ptf.models import Article
from ptf.models import BibItem
from ptf.models import BibItemId
from ptf.models import Collection
from ptf.models import Container
from ptf.models import Contribution
from ptf.models import DataStream
from ptf.models import ExtId
from ptf.models import ExtLink
from ptf.models import Kwd
from ptf.models import PtfSite
from ptf.models import Publisher
from ptf.models import RelatedObject
from ptf.models import Relationship
from ptf.models import Resource
from ptf.models import ResourceId
from ptf.models import Stats


class XmlCmdTestCase(TestCase):
    def get_solr(self):
        return solr_cmds.solrFactory.get_solr()

    def setUp(self):
        solr_cmds.solrFactory.solr_url = settings.SOLR_URL
        self.get_solr().delete(q="*:*")
        self.factory = RequestFactory()

        settings.SITE_NAME = "test"  # Make sure it is not crXXX

    def tearDown(self):
        self.get_solr().delete(q="*:*")

    def solrSearch(self, searchStr):
        return self.get_solr().search(searchStr)

    def check_empty_database(self):
        # self.assertEqual(Collection.objects.count(), 0)
        self.assertEqual(Container.objects.count(), 0)
        self.assertEqual(Article.objects.count(), 0)
        self.assertEqual(BibItem.objects.count(), 0)
        self.assertEqual(Abstract.objects.count(), 0)
        self.assertEqual(Contribution.objects.count(), 0)
        self.assertEqual(Kwd.objects.count(), 0)
        self.assertEqual(Publisher.objects.count(), 0)

    def test_01_serial(self):
        print("** test_01_serial")

        self.check_empty_database()

        self.assertRaises(ValueError, xml_cmds.addCollectionsXmlCmd().do)

        # cmd0 = ptf_cmds.addProviderPtfCmd( { "name":"numdam", "pid_type": "mathdoc-id" } )
        # provider = cmd0.do()
        provider = model_helpers.get_provider("mathdoc-id")

        f = open("tests/data/xml/journal.xml")
        body = f.read()
        f.close()

        cmd = xml_cmds.addCollectionsXmlCmd({"body": body})
        journals = cmd.do()

        self.assertEqual(len(journals), 1)
        journal = journals[0]

        self.assertEqual(Collection.objects.count(), 1)
        self.assertEqual(journal.pid, "AFST")
        # self.assertIsNone( journal.sid )
        self.assertEqual(journal.title_tex, "Annales de la faculté des sciences de Toulouse")
        self.assertEqual(journal.provider, provider)

        self.assertEqual(Publisher.objects.count(), 0)
        # publisher = Publisher.objects.first()
        # self.assertEqual( publisher.pub_name, "Université Paul Sabatier" )
        # self.assertEqual( publisher.pub_loc, "Toulouse (France)" )

        self.assertEqual(ExtLink.objects.count(), 4)
        e = ExtLink.objects.first()
        self.assertEqual(e.rel, "website")
        self.assertEqual(e.location, "http://afst.cedram.org")
        website = journal.website()
        self.assertEqual(website, "http://afst.cedram.org")
        test_website = journal.test_website()
        self.assertEqual(test_website, "http://afst-test.cedram.org")
        icon = journal.icon()
        self.assertEqual(icon, "/icon/AFST/AFST.png")
        icon = journal.small_icon()
        self.assertEqual(icon, "/icon/AFST/AFST_small.png")

        # 1 issn
        self.assertEqual(ResourceId.objects.count(), 1)

        f = open("tests/data/xml/journal2.xml")
        body = f.read()
        f.close()

        cmd_a = xml_cmds.addCollectionsXmlCmd({"body": body})
        journals = cmd_a.do()

        self.assertEqual(len(journals), 1)
        journal = journals[0]

        self.assertEqual(Collection.objects.count(), 2)
        self.assertEqual(journal.pid, "AFST2")

        cmd_a.undo()

        cmd.undo()
        self.assertRaises(
            Resource.DoesNotExist, Collection.objects.get, pid="AFST", provider=provider
        )
        self.assertEqual(Collection.objects.count(), 0)
        self.assertEqual(Publisher.objects.count(), 0)
        self.assertEqual(ExtLink.objects.count(), 0)
        self.assertEqual(ResourceId.objects.count(), 0)

        self.assertRaises(exceptions.ResourceDoesNotExist, cmd.undo)

        cmd.do()
        self.assertEqual(Collection.objects.count(), 1)

        f = open("tests/data/xml/journal.xml")
        body = f.read()
        f.close()

        cmd_a = xml_cmds.addCollectionsXmlCmd({"body": body})

        # Assertion if try to add an existing serial
        self.assertRaises(exceptions.ResourceExists, cmd_a.do)
        self.assertEqual(Collection.objects.count(), 1)
        cmd.undo()

        self.check_empty_database()

    def test_02_issue(self):
        print("** test_02_issue")

        self.check_empty_database()

        self.assertRaises(ValueError, xml_cmds.addCollectionsXmlCmd().do)

        provider = model_helpers.get_provider("mathdoc-id")

        f = open("tests/data/xml/journal-sms.xml")
        body = f.read()
        f.close()

        cmd1 = xml_cmds.addCollectionsXmlCmd({"body": body})
        journals = cmd1.do()

        self.assertEqual(len(journals), 1)

        f = open("tests/data/xml/issue.xml")
        body = f.read()
        f.close()

        cmd = xml_cmds.addIssueXmlCmd({"body": body})
        issue = cmd.do()

        self.assertEqual(Container.objects.count(), 1)
        self.assertEqual(issue.pid, "SMS_1969-1970__1_")
        self.assertEqual(
            issue.title_xml,
            "<issue-title>Problèmes mathématiques de la théorie des automates</issue-title>",
        )
        # self.assertIsNone( issue.sid )
        self.assertEqual(issue.provider, provider)

        the_issue = Container.objects.first()

        self.assertEqual(issue, the_issue)

        self.assertEqual(issue.year, "1969-1970")
        self.assertEqual(issue.volume, "")
        self.assertEqual(issue.volume_int, 0)
        self.assertEqual(issue.vseries, "")
        self.assertEqual(issue.vseries_int, 0)
        self.assertEqual(issue.number, "")
        self.assertEqual(issue.number_int, 0)

        self.assertEqual(Abstract.objects.count(), 2)

        # 36 contributions, 6 homonyms
        self.assertEqual(Contribution.objects.count(), 36)
        contribution = issue.contributions.first()
        self.assertEqual(contribution.role, "director")

        contribs_iter = issue.contributions.iterator()

        contribution = next(contribs_iter)
        self.assertEqual(contribution.role, "director")
        self.assertEqual(contribution.string_name, "M. P. Schützenberger")

        contribution = next(contribs_iter)
        self.assertEqual(contribution.role, "director")
        self.assertEqual(contribution.string_name, "A. Lentin")

        contribution = next(contribs_iter)
        self.assertEqual(contribution.role, "director")
        self.assertEqual(contribution.string_name, "M. Nivat")

        # self.assertEqual(KwdGroup.objects.count(), 4)
        # kwd_group = issue.kwdgroup_set.first()
        # self.assertEqual(kwd_group.content_type, "msc")
        # self.assertEqual(kwd_group.lang, "it")

        self.assertEqual(Kwd.objects.count(), 13)
        self.assertEqual(issue.kwd_set.count(), 3)

        kwds_iter = issue.kwd_set.iterator()
        kwd = next(kwds_iter)
        self.assertEqual(kwd.value, "53C55")
        self.assertEqual(kwd.type, "msc")
        self.assertEqual(kwd.lang, "it")
        kwd = next(kwds_iter)
        self.assertEqual(kwd.value, "53C15")
        kwd = next(kwds_iter)
        self.assertEqual(kwd.value, "53d20")

        # TODO check related objects
        self.assertEqual(RelatedObject.objects.count(), 2)
        self.assertEqual(ExtId.objects.count(), 4)

        self.assertEqual(Article.objects.count(), 2)
        self.assertEqual(BibItem.objects.count(), 22)

        # Issues are no longer stored in SolR
        result = self.get_solr().search(f"id:{issue.id}")

        self.assertEqual(result.hits, 0)

        i = 1
        for article in issue.article_set.all():
            if i == 1:
                body = article.get_body()
                self.assertEqual(
                    body,
                    "iv& < >LA METHODE DES MARTINGALES APPLIQUEE A L'ETUDE DE LA CONVERGENCE EN LOI DE PROCESSUS (*)par Rolando REBOLLEDO (**)INTRODUCTIONDepuis de nombreuses années, plusieurs chercheurs ont étudié \"la convergence en loi des processus par des méthodes \"fonctionnelles\", c'est-à-dire en utilisant les méthodes élaborées par PROKHOROV, SKOROKHOD et autres dans les années 50.D'un autre côté, ces dernières années ont connu l'essor de la Théorie des Martingales et de la Théorie Générale des Processus.La convergence en loi des processus est d'une utilité très grande dans de nombreuses branches des Probabilités Appliquées. La Théorie des Martingales à son tour s'est révélée être un auxiliaire très efficace dans de nombreux problèmes théoriques concernant la structure des processus. Cette théorie est certainement aujourd'hui l'unedes plus riches parmi les domaines de recherche en Probabilités.Le but de cet article est de montrer comment la Théorie des Martingales peut servir à analyser la convergence en loi des processus.Le point de départ de cette recherche a été le Théorème de DONSKER ouPrincipe d'Invariance.",
                )
                xml_body = article.body_xml
                expected_body = """<body specific-use="index">iv&amp; &lt; &gt;LA METHODE DES MARTINGALES APPLIQUEE A L'ETUDE DE LA CONVERGENCE EN LOI DE PROCESSUS (*)par Rolando REBOLLEDO (**)INTRODUCTIONDepuis de nombreuses années, plusieurs chercheurs ont étudié "la convergence en loi des processus par des méthodes "fonctionnelles", c'est-à-dire en utilisant les méthodes élaborées par PROKHOROV, SKOROKHOD et autres dans les années 50.D'un autre côté, ces dernières années ont connu l'essor de la Théorie des Martingales et de la Théorie Générale des Processus.La convergence en loi des processus est d'une utilité très grande dans de nombreuses branches des Probabilités Appliquées. La Théorie des Martingales à son tour s'est révélée être un auxiliaire très efficace dans de nombreux problèmes théoriques concernant la structure des processus. Cette théorie est certainement aujourd'hui l'unedes plus riches parmi les domaines de recherche en Probabilités.Le but de cet article est de montrer comment la Théorie des Martingales peut servir à analyser la convergence en loi des processus.Le point de départ de cette recherche a été le Théorème de DONSKER ouPrincipe d'Invariance.</body>"""
                self.assertEqual(xml_body, expected_body)

                self.assertEqual(
                    article.title_tex,
                    "Un langage quasi-rationnel lié aux graphes planaires test &",
                )
                self.assertEqual(article.lang, "de")
                self.assertEqual(article.trans_lang, "en")
                self.assertEqual(article.trans_title_tex, "In English")
                self.assertEqual(article.page_range, "8-11, 14-19, 40")

                contribs_iter = article.contributions.iterator()

                contribution = next(contribs_iter)
                self.assertEqual(contribution.role, "author")
                self.assertEqual(contribution.last_name, "Cori")
                self.assertEqual(contribution.first_name, "Robert")
                self.assertEqual(contribution.string_name, "")

                self.assertEqual(article.kwd_set.count(), 10)

                result = self.get_solr().search(f"id:{article.id}")

                self.assertEqual(result.hits, 1)
                solr_article = result.docs[0]
                self.assertEqual(solr_article["classname"], "Article de recherche")

                facets = solr_article["ar"]
                self.assertEqual(len(facets), 1)
                if getattr(settings, "DISPLAY_FIRST_NAME_FIRST", False):
                    self.assertEqual(facets[0], "Robert Cori")
                else:
                    self.assertEqual(facets[0], "Cori, Robert")

                name = solr_article["au"]
                if getattr(settings, "DISPLAY_FIRST_NAME_FIRST", False):
                    self.assertEqual(facets[0], "Robert Cori")
                else:
                    self.assertEqual(name, "Cori, Robert")

                lastnames = solr_article["aul"]
                self.assertEqual(len(lastnames), 1)
                self.assertEqual(lastnames[0], "Cori")

                self.assertIsNotNone(solr_article["body"])

                the_bibitems = result.docs[0]["bibitem"]
                self.assertEqual(len(the_bibitems), 15)
                self.assertEqual(article.date_accepted.strftime("%Y-%m-%d"), "2017-12-21")
                self.assertEqual(article.date_received.strftime("%Y-%m-%d"), "2017-12-21")
                self.assertEqual(article.date_published.strftime("%Y-%m-%d"), "2018-10-04")
                self.assertIsNone(article.date_revised)

                msc_values = solr_article["msc"]
                self.assertEqual(len(msc_values), 3)
                kwd = solr_article["kwd"]
                self.assertEqual(
                    kwd,
                    "Surfaces affines, champs de vecteurs complets, fibrés algébriques, chocolat",
                )
                kwd = solr_article["trans_kwd"]
                self.assertEqual(
                    kwd, "affine surfaces, complete vector fields, algebraic fibrations"
                )

                self.assertEqual(article.datastream_set.count(), 3)
                self.assertIsNotNone(article.datastream_set.get(mimetype="application/x-tex"))
                # check bibtex
                request = self.factory.get("/item/" + article.pid)
                bibtex = article.get_bibtex(request)
                expected_bibtex = "@article{SMS_1969-1970__1__A1_0,\n     author = {Cori, Robert},\n     title = {Un langage quasi-rationnel li\\'e aux graphes planaires test &},\n     journal = {S\\'eminaire Sch\\\"utzenberger},\n     note = {talk:1},\n     pages = {8-11, 14-19, 40},\n     publisher = {Secr\\'etariat math\\'ematique},\n     year = {1969-1970},\n     mrnumber = {289322},\n     zbl = {0216.23901},\n     language = {de},\n     url = {http://testserver/item/SMS_1969-1970__1__A1_0/}\n}"
                self.assertEqual(bibtex, expected_bibtex)
            else:
                self.assertEqual(article.page_type, "volant")

            i += 1

        f = open("tests/data/xml/issue.xml")
        body = f.read()
        f.close()

        cmd_a = xml_cmds.addIssueXmlCmd({"body": body})
        self.assertRaises(exceptions.ResourceExists, cmd_a.do)

        cmd.undo()
        cmd1.undo()
        # cmd0.undo()

        self.check_empty_database()

    def test_03_commit(self):
        print("** test_03_commit")

        self.check_empty_database()

        # cmd0 = ptf_cmds.addProviderPtfCmd( { "name": "numdam", "pid_type": "mathdoc-id" } )
        # provider = cmd0.do()
        # provider = model_helpers.get_provider( "mathdoc-id" )

        f = open("tests/data/xml/journal-sms.xml")
        body = f.read()
        f.close()

        cmd1 = xml_cmds.addCollectionsXmlCmd({"body": body})
        journals = cmd1.do()

        self.assertEqual(len(journals), 1)

        # The purpose of this decorator is to check that SolrCmds are committed
        # only at the end of the XmlCmd
        def check_commit_decorator(testcase, func):
            def inner(obj, *args, **kwargs):
                # print "Inside " + type(obj).__name__ + "." + func.__name__ + ". Checking solr commit"

                result = self.get_solr().search("classname:Livre")
                self.assertEqual(result.hits, 0)

                result = self.get_solr().search('classname:"Article de recherche"')
                self.assertEqual(result.hits, 0)

                return func(obj, *args, **kwargs)

            return inner

        old_func = ptf_cmds.addBibItemPtfCmd.do

        # decorate addBibItemPtfCmd.do that is used inside the addIssueXmlCmd
        # during addBibItemPtfCmd.do, no issue and article have to exist inside
        # Solr
        ptf_cmds.addBibItemPtfCmd.do = check_commit_decorator(self, ptf_cmds.addBibItemPtfCmd.do)

        f = open("tests/data/xml/issue3.xml")
        body = f.read()
        f.close()

        cmd = xml_cmds.addIssueXmlCmd({"body": body})
        issue = cmd.do()

        ptf_cmds.addBibItemPtfCmd.do = old_func

        # print "Checking solr commit after addIssueXmlCmd"
        result = self.get_solr().search(f"id:{issue.id}")
        self.assertEqual(result.hits, 0)

        cmd.undo()

        result = self.get_solr().search('classname:"Article de recherche"')
        self.assertEqual(result.hits, 0)
        result = self.get_solr().search("classname:Livre")
        self.assertEqual(result.hits, 0)

        cmd1.undo()
        # cmd0.undo()

        self.check_empty_database()

    def test_04_exception(self):
        print("** test_04_exception")

        self.check_empty_database()

        # cmd0 = ptf_cmds.addProviderPtfCmd( { "name": "numdam", "pid_type": "mathdoc-id" } )
        # provider = cmd0.do()

        f = open("tests/data/xml/journal-sms.xml")
        body = f.read()
        f.close()

        cmd1 = xml_cmds.addCollectionsXmlCmd({"body": body})
        journals = cmd1.do()

        self.assertEqual(len(journals), 1)

        # The purpose of this decorator is to check that SolrCmds are committed
        # only at the end of the XmlCmd
        def check_commit_decorator(testcase, func):
            def inner(obj, *args, **kwargs):
                # print "Inside " + type(obj).__name__ + "." + func.__name__ + ". Raising an exception"

                raise RuntimeError(
                    "Exception raised on purpose to check Django transaction.atomic and solr rollback"
                )
                return func(obj, *args, **kwargs)

            return inner

        old_func = database_cmds.add_biblio

        # decorate addBibItemPtfCmd.do that is used inside the addIssueXmlCmd
        database_cmds.add_biblio = check_commit_decorator(self, database_cmds.add_biblio)

        f = open("tests/data/xml/issue3.xml")
        body = f.read()
        f.close()

        cmd = xml_cmds.addIssueXmlCmd({"body": body})

        try:
            cmd.do()
            raise RuntimeError("addIssueXmlCmd.do should have raised a RuntimeError exception")
        except RuntimeError:
            pass

        # print "Checking Django transaction.atomic and solr rollback"
        # An exception has occured, there should not be any
        # journal/article/bibitem... inside the database
        self.assertEqual(Container.objects.count(), 0)
        self.assertEqual(Article.objects.count(), 0)
        self.assertEqual(DataStream.objects.count(), 0)
        # print "   1. No issue, article or lower objects (such as DataStream) inside the database."

        # The journal is still there
        self.assertEqual(Collection.objects.count(), 1)
        # print "   2. The journal is still inside the database."

        result = self.get_solr().search('classname:"Article de recherche"')
        # do_solr_commit()

        self.assertEqual(result.hits, 0)
        result = self.get_solr().search("classname:Livre")
        self.assertEqual(result.hits, 0)
        # print "   3. No issue or article inside Solr."

        database_cmds.add_biblio = old_func

        f = open("tests/data/xml/journal-sms2.xml")
        body = f.read()
        f.close()

        cmd2 = xml_cmds.addCollectionsXmlCmd({"body": body})

        journals = cmd2.do()

        self.assertEqual(Collection.objects.count(), 2)
        self.assertEqual(Container.objects.count(), 0)
        self.assertEqual(Article.objects.count(), 0)

        result = self.get_solr().search("classname:Journal")
        self.assertEqual(result.hits, 0)
        result = self.get_solr().search('classname:"Article de recherche"')
        self.assertEqual(result.hits, 0)
        result = self.get_solr().search("classname:Livre")
        self.assertEqual(result.hits, 0)

        cmd2.undo()
        # cmd.undo()
        cmd1.undo()
        # cmd0.undo()

        self.check_empty_database()

    def test_05_delete(self):
        print("** test_05_delete")

        self.check_empty_database()

        model_helpers.get_provider("mathdoc-id")

        f = open("tests/data/xml/journal-sms.xml")
        body = f.read()
        f.close()

        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": body})
        cmd0.do()

        f = open("tests/data/xml/issue.xml")
        body = f.read()
        f.close()

        cmd1 = xml_cmds.addIssueXmlCmd({"body": body})
        issue = cmd1.do()
        publisher = issue.my_publisher

        self.assertEqual(publisher.publishes.count(), 1)

        self.assertEqual(Article.objects.count(), 2)
        if getattr(settings, "DISPLAY_FIRST_NAME_FIRST", False):
            result = self.solrSearch('fau:"Robert Cori"')
        else:
            result = self.solrSearch('fau:"Cori, Robert"')
        self.assertEqual(result.hits, 1)

        self.assertEqual(Kwd.objects.count(), 13)

        cmd = ptf_cmds.addContainerPtfCmd()
        cmd.set_object_to_be_deleted(issue)
        # cmd.set_provider(p)
        # cmd.add_collection(journal)
        cmd.undo()

        self.assertEqual(Article.objects.count(), 0)
        # Removing a container removes its articles and all related objects
        # (Abstracts, Contribution, Kwds,...)
        self.assertEqual(Kwd.objects.count(), 0)

        result = self.solrSearch('fau:"Cori, Robert"')
        self.assertEqual(result.hits, 0)

        self.assertEqual(Publisher.objects.count(), 1)
        self.assertEqual(publisher.publishes.count(), 0)

        cmd = ptf_cmds.addPublisherPtfCmd({"pid": publisher.pid})
        cmd.set_object_to_be_deleted(publisher)
        cmd.undo()

        self.assertEqual(Publisher.objects.count(), 0)
        # self.assertEqual(Collection.objects.count(), 1)
        self.assertEqual(Container.objects.count(), 0)
        self.assertEqual(Article.objects.count(), 0)
        self.assertEqual(Contribution.objects.count(), 0)

        cmd0.undo()

        self.check_empty_database()

    def test_06_replace_issue(self):
        print("** test_06_replace_issue")

        self.check_empty_database()

        f = open("tests/data/xml/journal-sms.xml")
        body = f.read()
        f.close()

        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": body})
        cmd0.do()

        f = open("tests/data/xml/issue.xml")
        body = f.read()
        f.close()

        cmd1 = xml_cmds.addIssueXmlCmd({"body": body})
        cmd1.do()

        self.assertEqual(Article.objects.count(), 2)

        if getattr(settings, "DISPLAY_FIRST_NAME_FIRST", False):
            result = self.solrSearch('fau:"Robert Cori"')
        else:
            result = self.solrSearch('fau:"Cori, Robert"')
        self.assertEqual(result.hits, 1)

        self.assertEqual(Kwd.objects.count(), 13)

        self.assertEqual(Publisher.objects.count(), 1)
        publisher = Publisher.objects.first()
        self.assertEqual(publisher.pub_name, "Secrétariat mathématique")

        f = open("tests/data/xml/issue3.xml")
        body = f.read()
        f.close()

        cmd = xml_cmds.replaceIssueXmlCmd({"body": body})
        issue = cmd.do()

        self.assertEqual(
            issue.title_tex, "New Problèmes mathématiques de la théorie des automates"
        )
        self.assertEqual(Article.objects.count(), 1)
        if getattr(settings, "DISPLAY_FIRST_NAME_FIRST", False):
            result = self.solrSearch('fau:"Robert Cori"')
        else:
            result = self.solrSearch('fau:"Cori, Robert"')
        self.assertEqual(result.hits, 1)

        self.assertEqual(Kwd.objects.count(), 3)

        the_kwd = issue.kwd_set.first()
        self.assertEqual(the_kwd.value, "53C50")

        # The first publisher should have been deleted
        self.assertEqual(Publisher.objects.count(), 1)
        publisher = Publisher.objects.first()
        self.assertEqual(publisher.pub_name, "New publisher")

        # no undo for updateCmds (as they first delete the resource before
        # adding the new one. We need to delete issue)
        cmd1 = ptf_cmds.addContainerPtfCmd()
        cmd1.set_object_to_be_deleted(issue)
        cmd1.undo()

        cmd1 = ptf_cmds.addPublisherPtfCmd({"pid": publisher.pid})
        cmd1.set_object_to_be_deleted(publisher)
        cmd1.undo()

        cmd0.undo()

        self.check_empty_database()

    def test_07_update_journal(self):
        print("** test_07_update_journal")

        self.check_empty_database()

        f = open("tests/data/xml/journal.xml")
        body = f.read()
        f.close()

        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": body})
        journals = cmd0.do()

        f = open("tests/data/xml/issue4.xml")
        body = f.read()
        f.close()

        cmd1 = xml_cmds.addIssueXmlCmd({"body": body})
        cmd1.do()

        f = open("tests/data/xml/journal-update1.xml")
        body = f.read()
        f.close()

        cmd = xml_cmds.updateCollectionsXmlCmd({"body": body})
        journals = cmd.do()
        journal_update1 = journals[0]

        self.assertEqual(Collection.objects.count(), 1)
        self.assertEqual(Container.objects.count(), 1)
        self.assertEqual(Article.objects.count(), 1)

        issue = Container.objects.first()
        publisher = issue.my_publisher

        # self.assertEqual(the_journal.id, journal_update1.id)
        self.assertEqual(journal_update1.title_tex, "Journal TOTO")

        # Publisher are ignored in serials
        self.assertEqual(Publisher.objects.count(), 1)
        # publisher = Publisher.objects.first()
        self.assertEqual(publisher.pub_name, "UNIVERSITE PAUL SABATIER")
        #
        # existing_id = publisher.id

        f = open("tests/data/xml/journal-update2.xml")
        body = f.read()
        f.close()

        cmd = xml_cmds.updateCollectionsXmlCmd({"body": body})
        journals = cmd.do()
        journal_update2 = journals[0]

        # self.assertEqual( Publisher.objects.count(), 1 )
        # publisher = Publisher.objects.first()
        # self.assertEqual( publisher.pub_name, "TITI" )
        #
        # self.assertEqual( publisher.id, existing_id )

        f = open("tests/data/xml/journal-sms.xml")
        body = f.read()
        f.close()

        cmd = xml_cmds.updateCollectionsXmlCmd({"body": body})
        # Cannot update a journal that does not exist
        self.assertRaises(exceptions.ResourceDoesNotExist, cmd.do)

        p = model_helpers.get_provider("mathdoc-id")
        cmd1 = ptf_cmds.addContainerPtfCmd({"pid": "AFST_1999__1_", "ctype": "issue"})
        cmd1.set_provider(p)
        cmd1.add_collection(journal_update2)
        cmd1.set_object_to_be_deleted(issue)
        cmd1.undo()

        cmd1 = ptf_cmds.addPublisherPtfCmd({"pid": publisher.pid})
        cmd1.set_object_to_be_deleted(publisher)
        cmd1.undo()

        cmd0 = ptf_cmds.addCollectionPtfCmd({"pid": "AFST"})
        cmd0.set_object_to_be_deleted(journal_update2)
        cmd0.set_provider(p)
        cmd0.undo()

        # Since we're deleting a journal directly (addCollectionPtfCmd.undo),
        # we need to delete the publisher manually
        # mathdoc_provider = model_helpers.get_provider("mathdoc-id")
        # cmd = ptf_cmds.addPublisherPtfCmd( {"name": "TITI",
        #                                    "location": publisher.pub_loc,
        #                                    "pid": publisher.pid} )
        # cmd.set_provider(mathdoc_provider)
        # cmd.undo()

        self.check_empty_database()

    def test_08_book(self):
        print("** test_08_book")

        self.check_empty_database()

        self.assertRaises(ValueError, xml_cmds.addBookXmlCmd().do)

        provider = model_helpers.get_provider("mathdoc-id")

        f = open("tests/data/xml/collection-msmf.xml")
        body = f.read()
        f.close()

        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": body})
        cmd0.do()

        f = open("tests/data/xml/book.xml")
        body = f.read()
        f.close()

        cmd = xml_cmds.addBookXmlCmd({"body": body})
        book = cmd.do()

        self.assertEqual(Container.objects.count(), 1)
        self.assertEqual(book.pid, "MSMF_2014_2_138-139__1_0")
        expected_title = r'<book-title-group><book-title xml:space="preserve">Holonomic <inline-formula><alternatives><tex-math>$\mathcal{D}$</tex-math><math xmlns="http://www.w3.org/1998/Math/MathML"><mi>𝒟</mi></math></alternatives></inline-formula>-modules with Betti structure</book-title><trans-title-group xml:lang="fr"><trans-title xml:space="preserve"><inline-formula><alternatives><tex-math>$\mathcal{D}$</tex-math><math xmlns="http://www.w3.org/1998/Math/MathML"><mi>𝒟</mi></math></alternatives></inline-formula>-modules holonomes munis d’une structure de Betti</trans-title></trans-title-group></book-title-group>'
        self.assertEqual(book.title_xml, expected_title)

        # self.assertIsNone( book.sid )
        self.assertEqual(book.provider, provider)

        self.assertEqual(Publisher.objects.count(), 1)

        the_book = Container.objects.first()

        self.assertEqual(book, the_book)

        self.assertEqual(book.year, "2014")
        self.assertEqual(book.number, "138-139")
        self.assertEqual(book.number_int, 138)
        self.assertEqual(book.vseries, "2")
        self.assertEqual(book.vseries_int, 2)

        self.assertEqual(Abstract.objects.count(), 2)

        self.assertEqual(Contribution.objects.exclude(resource__isnull=True).count(), 1)
        contribs_iter = book.contributions.iterator()

        contributor = next(contribs_iter)
        self.assertEqual(contributor.role, "author")
        self.assertEqual(contributor.last_name, "Mochizuki")
        self.assertEqual(contributor.first_name, "Takuro")

        self.assertEqual(Kwd.objects.count(), 8)
        kwds_iter = book.kwd_set.iterator()

        kwd = next(kwds_iter)
        self.assertEqual(kwd.value, "14F10")
        self.assertEqual(kwd.type, "msc")

        kwd = next(kwds_iter)
        self.assertEqual(kwd.value, "32C38")

        kwd = next(kwds_iter)
        self.assertEqual(kwd.value, "holonomic D-modules")
        self.assertEqual(kwd.type, "")
        self.assertEqual(kwd.lang, "en")

        self.assertEqual(RelatedObject.objects.count(), 0)
        self.assertEqual(ExtId.objects.count(), 2)
        self.assertEqual(Article.objects.count(), 0)
        self.assertEqual(BibItem.objects.count(), 59)

        self.assertEqual(Collection.objects.count(), 1)
        col = Collection.objects.first()
        self.assertEqual(col.coltype, "book-series")
        self.assertEqual(col.pid, "MSMF")

        self.assertEqual(book.my_collection, col)
        self.assertEqual(book.datastream_set.count(), 2)
        # result = self.get_solr().search('*:*' )
        result = self.get_solr().search(f"id:{book.id}")

        # book-series are not stored in SolR, only its book parts
        self.assertEqual(result.docs[0]["classname"], "Livre")
        self.assertEqual(result.docs[0]["year"], "2014")

        # volume/vseries/number should be converted to int
        self.assertNotEqual(result.docs[0]["number"], "138")
        self.assertEqual(result.docs[0]["number"], 138)

        f = open("tests/data/xml/book.xml")
        body = f.read()
        f.close()

        cmd_a = xml_cmds.addBookXmlCmd({"body": body})
        self.assertRaises(exceptions.ResourceExists, cmd_a.do)

        cmd.undo()
        cmd0.undo()

        self.check_empty_database()

    def test_09_book_part(self):
        print("** test_09_book_part and no contrib group for book : collectif*****")

        self.check_empty_database()

        self.assertRaises(ValueError, xml_cmds.addBookXmlCmd().do)

        provider = model_helpers.get_provider("mathdoc-id")

        f = open("tests/data/xml/collection-msmf.xml")
        body = f.read()
        f.close()
        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": body})
        cmd0.do()

        f = open("tests/data/xml/book-with-part.xml")
        body = f.read()
        f.close()

        cmd = xml_cmds.addBookXmlCmd({"body": body})
        book = cmd.do()

        self.assertEqual(Container.objects.count(), 1)
        self.assertEqual(book.pid, "MSMF_1993_2_52_")
        # self.assertIsNone( book.sid )
        self.assertEqual(book.provider, provider)

        self.assertEqual(Publisher.objects.count(), 1)

        the_book = Container.objects.first()

        self.assertEqual(book, the_book)

        self.assertEqual(book.year, "1993")
        self.assertEqual(book.number, "52")
        self.assertEqual(book.vseries, "2")
        self.assertEqual(book.vseries_int, 2)
        book_contribution = book.contributions.first()
        self.assertEqual(book_contribution.string_name, "Collectif")
        self.assertEqual(Abstract.objects.count(), 0)

        # Book Parts are stored as Article objects
        self.assertEqual(Article.objects.count(), 3)
        the_book_parts = Article.objects.filter(my_container=book).order_by_sequence()
        self.assertEqual(the_book_parts.count(), 3)

        the_part = the_book_parts.first()

        self.assertEqual(the_part.fpage, "1")
        self.assertEqual(the_part.lpage, "73")
        self.assertEqual(the_part.seq, 1)
        self.assertEqual(the_part.pseq, 0)
        self.assertEqual(the_part.datastream_set.count(), 2)

        self.assertEqual(Contribution.objects.exclude(resource__isnull=True).count(), 4)

        contribution = the_part.contributions.first()
        self.assertEqual(contribution.role, "author")
        self.assertEqual(contribution.first_name, "Laure")

        self.assertEqual(RelatedObject.objects.count(), 0)
        self.assertEqual(ExtId.objects.count(), 4)
        self.assertEqual(BibItem.objects.count(), 42)

        self.assertEqual(Collection.objects.count(), 1)
        col = Collection.objects.first()

        self.assertEqual(col.coltype, "book-series")
        self.assertEqual(col.pid, "MSMF")

        self.assertEqual(book.my_collection, col)

        # result = self.get_solr().search('*:*' )
        result = self.get_solr().search(f"id:{book.id}")

        self.assertEqual(result.hits, 1)
        # self.assertEqual( result.docs[0]['classname'], 'Book')
        # self.assertEqual( result.docs[0]['year'], '1993')

        result = self.get_solr().search('classname:"Chapitre de livre"')
        self.assertEqual(result.hits, 3)
        self.assertEqual(result.docs[0]["id"], str(the_part.id))

        f = open("tests/data/xml/book-with-part.xml")
        body = f.read()
        f.close()

        cmd_a = xml_cmds.addBookXmlCmd({"body": body})
        self.assertRaises(exceptions.ResourceExists, cmd_a.do)

        cmd.undo()
        cmd0.undo()

        self.check_empty_database()

    def test_10_delete_book(self):
        print("** test_10_delete_book")

        self.check_empty_database()

        f = open("tests/data/xml/collection-msmf.xml")
        body = f.read()
        f.close()
        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": body})
        cmd0.do()

        f = open("tests/data/xml/book-with-part.xml")
        body = f.read()
        f.close()

        cmd1 = xml_cmds.addBookXmlCmd({"body": body})
        book = cmd1.do()

        provider = model_helpers.get_provider("mathdoc-id")

        cmd = ptf_cmds.addContainerPtfCmd(
            {"pid": book.pid, "solr_commit": False, "ctype": "book-edited-book"}
        )
        cmd.set_provider(provider)
        cmd.set_object_to_be_deleted(book)
        cmd.undo()

        publisher = Publisher.objects.first()
        cmd3 = ptf_cmds.addPublisherPtfCmd({"pid": publisher.pid})
        cmd3.set_object_to_be_deleted(publisher)
        cmd3.undo()

        cmd0.undo()

        self.check_empty_database()

    def test_11_update_book(self):
        print("** test_11_update_book")

        self.check_empty_database()

        f = open("tests/data/xml/collection-msmf.xml")
        body = f.read()
        f.close()

        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": body})
        cmd0.do()

        f = open("tests/data/xml/book.xml")
        body = f.read()
        f.close()

        cmd1 = xml_cmds.addBookXmlCmd({"body": body})
        book = cmd1.do()

        self.assertEqual(Publisher.objects.count(), 1)
        publisher = Publisher.objects.first()
        publisher_book = book.my_publisher
        self.assertEqual(publisher_book, publisher)

        f = open("tests/data/xml/book2.xml")
        body = f.read()
        f.close()

        cmd2 = xml_cmds.updateBookXmlCmd({"body": body})
        book2 = cmd2.do()

        book = Container.objects.get(pid="MSMF_2014_2_138-139__1_0")
        uris = book.datastream_set.all()

        self.assertEqual(len(uris), 2)
        self.assertEqual(Container.objects.count(), 1)
        self.assertEqual(book.pid, "MSMF_2014_2_138-139__1_0")
        self.assertEqual(book.year, "2014")

        self.assertEqual(Publisher.objects.count(), 1)
        publisher = Publisher.objects.first()
        publisher_book = book2.my_publisher
        self.assertEqual(publisher_book, publisher)

        self.assertEqual(Abstract.objects.count(), 2)
        self.assertEqual(Contribution.objects.exclude(resource__isnull=True).count(), 1)

        self.assertEqual(Kwd.objects.count(), 8)
        self.assertEqual(RelatedObject.objects.count(), 0)
        self.assertEqual(ExtId.objects.count(), 0)
        self.assertEqual(Article.objects.count(), 0)
        self.assertEqual(BibItem.objects.count(), 59)

        col = book.my_collection
        self.assertEqual(col.coltype, "book-series")
        self.assertEqual(col.pid, "MSMF")

        # result = self.get_solr().search('*:*' )
        result = self.get_solr().search(f"id:{book.id}")

        self.assertEqual(result.docs[0]["classname"], "Livre")
        self.assertEqual(result.docs[0]["year"], "2014")

        cmd3 = ptf_cmds.addContainerPtfCmd(
            {"pid": book.pid, "solr_commit": False, "ctype": "book-edited-book"}
        )
        cmd3.set_provider(book.provider)
        cmd3.set_object_to_be_deleted(book)
        cmd3.undo()

        publisher = Publisher.objects.first()
        cmd3 = ptf_cmds.addPublisherPtfCmd({"pid": publisher.pid})
        cmd3.set_object_to_be_deleted(publisher)
        cmd3.undo()

        cmd0.undo()

        self.check_empty_database()

    def test_12a_relationship_reverse(self):
        print("** test_12a_relationship_reverse")

        self.check_empty_database()

        self.assertRaises(ValueError, xml_cmds.addCollectionsXmlCmd().do)

        f = open("tests/data/xml/journal-sms.xml")
        body = f.read()
        f.close()

        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": body})
        journals = cmd0.do()

        self.assertEqual(len(journals), 1)

        f = open("tests/data/xml/issue2.xml")
        body = f.read()
        f.close()

        cmd2 = xml_cmds.addIssueXmlCmd({"body": body})
        cmd2.do()

        article2 = model_helpers.get_article("SMS2_1971__1__A1_0")

        self.assertEqual(Relationship.objects.count(), 1)
        # We've only imported issue.xml the related article is not yet in the
        # database
        self.assertEqual(article2.left_resources_related_to_me.count(), 0)
        self.assertEqual(article2.right_resources_related_to_me.count(), 0)

        relationship = Relationship.objects.first()
        relationname = relationship.rel_info

        self.assertEqual(relationship.subject_pid, "SMS2_1971__1__A1_0")
        self.assertEqual(relationship.object_pid, "SMS_1969-1970__1__A1_0")
        self.assertEqual(relationname.left, "follows")
        self.assertEqual(relationname.right, "followed-by")
        # relationship.related is a Resource, we need to downcast
        self.assertEqual(relationship.resource.article, article2)
        self.assertIsNone(relationship.related)

        f = open("tests/data/xml/issue.xml")
        body = f.read()
        f.close()

        cmd1 = xml_cmds.addIssueXmlCmd({"body": body})
        cmd1.do()

        article1 = model_helpers.get_article("SMS_1969-1970__1__A1_0")

        # Still 1 relationship
        self.assertEqual(Relationship.objects.count(), 1)
        relationship = Relationship.objects.first()
        self.assertEqual(relationship.subject_pid, "SMS2_1971__1__A1_0")
        self.assertEqual(relationship.object_pid, "SMS_1969-1970__1__A1_0")
        self.assertEqual(relationname.left, "follows")
        self.assertEqual(relationname.right, "followed-by")
        # relationship.related is a Resource, we need to downcast
        self.assertEqual(relationship.related.article, article1)
        self.assertEqual(relationship.resource.article, article2)

        self.assertEqual(article1.subject_of.count(), 0)
        self.assertEqual(article1.object_of.count(), 1)
        rel = article1.object_of.first()
        self.assertEqual(rel, relationship)

        self.assertEqual(article2.subject_of.count(), 1)
        self.assertEqual(article2.object_of.count(), 0)
        rel = article2.subject_of.first()
        self.assertEqual(rel, relationship)

        self.assertEqual(article1.left_resources_related_to_me.count(), 1)
        self.assertEqual(article1.right_resources_related_to_me.count(), 0)

        related_resource = article1.left_resources_related_to_me.first()
        # downcast
        related_article = related_resource.article
        self.assertEqual(related_article, article2)

        self.assertEqual(article2.left_resources_related_to_me.count(), 0)
        self.assertEqual(article2.right_resources_related_to_me.count(), 1)

        related_resource = article2.right_resources_related_to_me.first()
        # downcast
        related_article = related_resource.article
        self.assertEqual(related_article, article1)

        cmd1.undo()

        # self.assertEqual(Collection.objects.count(), 1)
        self.assertEqual(Container.objects.count(), 1)

        # The relationship is still there after the 1st undo
        self.assertEqual(Relationship.objects.count(), 1)
        relationship = Relationship.objects.first()
        self.assertEqual(relationship.subject_pid, "SMS2_1971__1__A1_0")
        self.assertEqual(relationship.object_pid, "SMS_1969-1970__1__A1_0")
        # relationship.related is a Resource, we need to downcast
        self.assertEqual(relationship.resource.article, article2)
        self.assertIsNone(relationship.related)

        cmd2.undo()
        cmd0.undo()

        self.check_empty_database()

    def test_12b_relationship(self):
        print("** test_12b_relationship")

        self.check_empty_database()

        self.assertRaises(ValueError, xml_cmds.addCollectionsXmlCmd().do)

        f = open("tests/data/xml/journal-sms.xml")
        body = f.read()
        f.close()

        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": body})
        journals = cmd0.do()

        self.assertEqual(len(journals), 1)

        f = open("tests/data/xml/issue.xml")
        body = f.read()
        f.close()

        cmd1 = xml_cmds.addIssueXmlCmd({"body": body})
        cmd1.do()

        article1 = model_helpers.get_article("SMS_1969-1970__1__A1_0")

        self.assertEqual(Relationship.objects.count(), 1)
        # We've only imported issue.xml the related article is not yet in the
        # database
        self.assertEqual(article1.left_resources_related_to_me.count(), 0)
        self.assertEqual(article1.right_resources_related_to_me.count(), 0)

        relationship = Relationship.objects.first()
        relationname = relationship.rel_info

        self.assertEqual(relationship.subject_pid, "SMS2_1971__1__A1_0")
        self.assertEqual(relationship.object_pid, "SMS_1969-1970__1__A1_0")
        self.assertEqual(relationname.left, "follows")
        self.assertEqual(relationname.right, "followed-by")
        # relationship.related is a Resource, we need to downcast
        self.assertEqual(relationship.related.article, article1)
        self.assertIsNone(relationship.resource)

        f = open("tests/data/xml/issue2.xml")
        body = f.read()
        f.close()

        cmd2 = xml_cmds.addIssueXmlCmd({"body": body})
        cmd2.do()

        article2 = model_helpers.get_article("SMS2_1971__1__A1_0")

        # Still 1 relationship
        self.assertEqual(Relationship.objects.count(), 1)
        relationship = Relationship.objects.first()
        self.assertEqual(relationship.subject_pid, "SMS2_1971__1__A1_0")
        self.assertEqual(relationship.object_pid, "SMS_1969-1970__1__A1_0")
        self.assertEqual(relationname.left, "follows")
        self.assertEqual(relationname.right, "followed-by")
        # relationship.related is a Resource, we need to downcast
        self.assertEqual(relationship.related.article, article1)
        self.assertEqual(relationship.resource.article, article2)

        self.assertEqual(article1.subject_of.count(), 0)
        self.assertEqual(article1.object_of.count(), 1)
        rel = article1.object_of.first()
        self.assertEqual(rel, relationship)

        self.assertEqual(article2.subject_of.count(), 1)
        self.assertEqual(article2.object_of.count(), 0)
        rel = article2.subject_of.first()
        self.assertEqual(rel, relationship)

        self.assertEqual(article1.left_resources_related_to_me.count(), 1)
        self.assertEqual(article1.right_resources_related_to_me.count(), 0)

        related_resource = article1.left_resources_related_to_me.first()
        # downcast
        related_article = related_resource.article
        self.assertEqual(related_article, article2)

        self.assertEqual(article2.left_resources_related_to_me.count(), 0)
        self.assertEqual(article2.right_resources_related_to_me.count(), 1)

        related_resource = article2.right_resources_related_to_me.first()
        # downcast
        related_article = related_resource.article
        self.assertEqual(related_article, article1)

        cmd2.undo()

        # self.assertEqual(Collection.objects.count(), 1)
        self.assertEqual(Container.objects.count(), 1)

        # The relationship is still there after the 1st undo
        self.assertEqual(Relationship.objects.count(), 1)
        relationship = Relationship.objects.first()
        self.assertEqual(relationship.subject_pid, "SMS2_1971__1__A1_0")
        self.assertEqual(relationship.object_pid, "SMS_1969-1970__1__A1_0")
        # relationship.related is a Resource, we need to downcast
        self.assertEqual(relationship.related.article, article1)
        self.assertIsNone(relationship.resource)

        cmd1.undo()
        cmd0.undo()

        self.check_empty_database()

    def test_13_formula(self):
        print("** test_13_formula")

        self.check_empty_database()

        f = open("tests/data/xml/journal-formula.xml")
        body = f.read()
        f.close()

        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": body})
        journals = cmd0.do()

        self.assertEqual(len(journals), 1)
        journal = journals[0]

        title_xml = journal.title_xml
        title_tex = journal.title_tex
        title_html = journal.title_html

        self.assertEqual(
            title_tex, "Remarks on gauge theory, complex geometry and $4$-manifold topology"
        )
        self.assertEqual(
            title_xml,
            '<title-group><title>Remarks on gauge theory, complex geometry and <inline-formula><alternatives><math xmlns="http://www.w3.org/1998/Math/MathML"><mn>4</mn></math><tex-math>$4$</tex-math></alternatives></inline-formula>-manifold topology</title><abbrev-title>Remarks</abbrev-title></title-group>',
        )
        self.assertEqual(
            title_html,
            'Remarks on gauge theory, complex geometry and <span class="mathjax-formula" data-tex="$4$"><math xmlns="http://www.w3.org/1998/Math/MathML"><mn>4</mn></math></span>-manifold topology',
        )

        cmd0.undo()

        self.check_empty_database()

    def test_14_collection(self):
        print("** test_14_collection")

        self.check_empty_database()

        f = open("tests/data/xml/collection.xml")
        body = f.read()
        f.close()

        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": body})
        collections = cmd0.do()

        self.assertEqual(len(collections), 1)
        collection = collections[0]

        self.assertEqual(collection.title_tex, "Cours de l'institut Fourier")
        self.assertEqual(collection.wall, 2)
        self.assertEqual(collection.pid, "CIF")
        self.assertEqual(collection.fyear, 0)

        f = open("tests/data/xml/book-cif.xml")
        body = f.read()
        f.close()

        cmd = xml_cmds.addBookXmlCmd({"body": body})
        cmd.do()

        collection = Collection.objects.first()
        self.assertEqual(collection.fyear, 1995)

        cmd.undo()
        cmd0.undo()

        self.check_empty_database()

    def test_15_book2(self):
        print("** test_15_book2")

        self.check_empty_database()

        f = open("tests/data/xml/collection-msmf.xml")
        body = f.read()
        f.close()

        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": body})
        collections = cmd0.do()

        self.assertEqual(len(collections), 1)
        collection = collections[0]

        self.assertEqual(collection.pid, "MSMF")

        f = open("tests/data/xml/book-msmf.xml")
        body = f.read()
        f.close()

        cmd = xml_cmds.addBookXmlCmd({"body": body})
        book = cmd.do()

        # test prise en compte last_modified date
        my_book = Container.objects.get(id=book.id)
        self.assertTrue(isinstance(my_book.last_modified, datetime))

        self.assertEqual(book.last_modified, my_book.last_modified)

        my_last_modified_date = dateutil.parser.parse("2017-01-10T18:24:58.202+01:00")
        self.assertEqual(my_book.last_modified, my_last_modified_date)

        self.assertEqual(Container.objects.count(), 1)

        cmd.undo()
        cmd0.undo()

        self.check_empty_database()

    def test_15_monograph_with_toc(self):
        print("** test_15_monograph_with_toc : addContainerXmlCmd ET toc**")

        self.check_empty_database()

        f = open("tests/data/xml/collection.xml")
        body = f.read()
        f.close()

        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": body})
        collections = cmd0.do()

        self.assertEqual(len(collections), 1)
        collection = collections[0]

        self.assertEqual(collection.title_tex, "Cours de l'institut Fourier")
        self.assertEqual(collection.wall, 2)
        self.assertEqual(collection.pid, "CIF")
        self.assertEqual(collection.fyear, 0)

        f = open("tests/data/xml/book-monograph-with-toc.xml")
        body = f.read()
        f.close()

        cmd = xml_cmds.addOrUpdateBookXmlCmd({"body": body})
        book = cmd.do()

        # test prise en compte toc
        my_book = Container.objects.get(id=book.id)

        self.assertEqual(Container.objects.count(), 1)
        self.assertIsNotNone(my_book.frontmatter)
        frontmatter = my_book.frontmatter
        # self.maxDiff = None
        # print(toc.value_html)
        # print(len(toc.value_html))
        self.assertEqual(len(frontmatter.value_html), 6006)

        # check citation_bibtex
        request = self.factory.get("/item/CIF_1970-1971__3__1_0")
        bibtex = my_book.get_bibtex(request)
        self.maxDiff = None
        self.assertEqual(
            bibtex,
            "@book{CIF_1970-1971__3__1_0,\n     author = {Malgrange, B.},\n     title = {Op\\'erateurs pseudo-diff\\'erentiels et op\\'erateurs de {Fourier}},\n     series = {Cours de l'institut Fourier},\n     publisher = {Institut des Math\\'ematiques Pures - Universit\\'e Scientifique et M\\'edicale de Grenoble},\n     number = {3},\n     year = {1970-1971},\n     language = {fr},\n     url = {http://testserver/item/CIF_1970-1971__3__1_0/}\n}",
        )
        cmd.undo()
        cmd0.undo()

        self.assertEqual(Publisher.objects.count(), 1)

        publisher = Publisher.objects.first()
        cmd1 = ptf_cmds.addPublisherPtfCmd({"pid": publisher.pid})
        cmd1.set_object_to_be_deleted(publisher)
        cmd1.undo()

        self.check_empty_database()

    def test_16_monograph_with_parts(self):
        print("** test_16_monograph_with_parts book_contribgroup = first part contribgroup*****")

        self.check_empty_database()

        f = open("tests/data/xml/collection.xml")
        body = f.read()
        f.close()

        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": body})
        collections = cmd0.do()

        self.assertEqual(len(collections), 1)
        collection = collections[0]

        self.assertEqual(collection.title_tex, "Cours de l'institut Fourier")
        self.assertEqual(collection.wall, 2)
        self.assertEqual(collection.pid, "CIF")
        self.assertEqual(collection.fyear, 0)

        f = open("tests/data/xml/book-cif.xml")
        body = f.read()
        f.close()

        cmd = xml_cmds.addBookXmlCmd({"body": body})
        cmd.do()

        collection = Collection.objects.first()
        self.assertEqual(collection.fyear, 1995)
        book = Container.objects.get(pid="CIF_1995__23_")
        self.assertEqual(
            book.contributions.first().last_name,
            book.article_set.first().contributions.first().last_name,
        )

        result = self.solrSearch(f"id:{book.id}")
        self.assertEqual(len(result.docs), 1)
        self.assertTrue(result.docs[0]["title_tex"] == book.title_tex)

        cmd.undo()
        cmd0.undo()

        self.check_empty_database()

    def test_17_article_with_citations(self):
        print("** test_17 article avec citations")

        self.check_empty_database()

        f = open("tests/data/xml/journal-sms.xml")
        body = f.read()
        f.close()

        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": body})
        cmd0.do()

        f = open("tests/data/xml/issue2.xml")
        body = f.read()
        f.close()

        cmd = xml_cmds.addIssueXmlCmd({"body": body})
        cmd.do()

        f = open("tests/data/xml/collection-aif.xml")
        body = f.read()
        f.close()

        cmd1 = xml_cmds.addCollectionsXmlCmd({"body": body})
        cmd1.do()

        f = open("tests/data/xml/issue-for-citations.xml")
        body = f.read()
        f.close()

        cmd2 = xml_cmds.addIssueXmlCmd({"body": body})
        cmd2.do()

        article = Article.objects.get(pid="AIF_1968__18_1_339_0")
        citings = article.citations()
        self.assertEqual(len(citings), 1)

        cmd2.undo()
        cmd.undo()
        cmd0.undo()
        cmd1.undo()

        self.check_empty_database()

    def test_18_update_issue2(self):
        print("** test_18_update_issue2")

        self.check_empty_database()

        f = open("tests/data/xml/journal-sms.xml")
        body = f.read()
        f.close()

        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": body})
        journals = cmd0.do()
        journal = journals[0]

        f = open("tests/data/xml/issue.xml")
        body = f.read()
        f.close()

        cmd1 = xml_cmds.addIssueXmlCmd({"body": body})
        issue1 = cmd1.do()

        f = open("tests/data/xml/issue2.xml")
        body = f.read()
        f.close()

        cmd1 = xml_cmds.addIssueXmlCmd({"body": body})
        issue2 = cmd1.do()

        f = open("tests/data/xml/journal-sms.xml")
        body = f.read()
        f.close()

        cmd1 = xml_cmds.updateCollectionsXmlCmd({"body": body})
        journals = cmd1.do()
        journal = journals[0]

        # self.assertEqual(Collection.objects.count(), 1)

        f = open("tests/data/xml/issue.xml")
        body = f.read()
        f.close()

        cmd1 = xml_cmds.replaceIssueXmlCmd({"body": body})
        issue1 = cmd1.do()

        journal = Collection.objects.first()
        self.assertEqual(journal.fyear, 1921)
        self.assertEqual(journal.lyear, 1970)

        self.assertEqual(Container.objects.count(), 2)

        # no undo for updateCmds (as they first delete the resource before
        # adding the new one. We need to delete issue)
        cmd1 = ptf_cmds.addContainerPtfCmd()
        cmd1.set_object_to_be_deleted(issue2)
        cmd1.undo()

        self.assertEqual(Container.objects.count(), 1)

        cmd1 = ptf_cmds.addContainerPtfCmd()
        cmd1.set_object_to_be_deleted(issue1)
        cmd1.undo()

        self.assertEqual(Container.objects.count(), 0)
        self.assertEqual(Publisher.objects.count(), 1)

        publisher = Publisher.objects.first()
        cmd1 = ptf_cmds.addPublisherPtfCmd({"pid": publisher.pid})
        cmd1.set_object_to_be_deleted(publisher)
        cmd1.undo()

        cmd0.undo()

        self.check_empty_database()

    def test_19_book_with_collection_DoesNotExist(self):
        print("** test_19_test-book_with_collection_DoesNotExist")

        self.check_empty_database()

        f = open("tests/data/xml/book-cif.xml")
        body = f.read()
        f.close()
        cmd = xml_cmds.addBookXmlCmd({"body": body})
        self.assertRaises(exceptions.ResourceDoesNotExist, cmd.do)

        self.check_empty_database()

    def test_20_test_bibitems_citation(self):
        print("** test_20_test-citation2")

        self.check_empty_database()

        f = open("tests/data/xml/collection-aif.xml")
        body = f.read()
        f.close()

        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": body})
        journals = cmd0.do()

        self.assertEqual(len(journals), 1)

        f = open("tests/data/xml/issue-aif2.xml")
        body = f.read()
        f.close()

        cmd = xml_cmds.addIssueXmlCmd({"body": body})
        cmd.do()

        bib = BibItem.objects.get(resource__pid="AIF_2015__65_6_2437_0", sequence=1)
        self.assertEqual(bib.volume, "2")
        self.assertEqual(bib.year, "1970")

        ##############################################
        #
        # element-citations
        #
        ##############################################

        # 1 generic ext-link, 1 eid
        bib = BibItem.objects.get(resource__pid="AIF_2015__65_6_2331_0", sequence=1)
        self.assertEqual(
            bib.citation_html,
            ' (2009), Research Paper 21 <a href="http://www.toto.org" target="_blank">toto</a>',
        )

        # 1 generic ext-link, 1 ext-link with a specific type (filtered), 1 pub-id (filtered)
        bib = BibItem.objects.get(resource__pid="AIF_2015__65_6_2331_0", sequence=2)
        self.assertEqual(
            bib.citation_html,
            ' (2009) <a href="http://www.titi.org" target="_blank">http://www.titi.org</a>',
        )

        # 1 pub-id (doi), 1 generic ext-link to doi.org (filtered)
        bib = BibItem.objects.get(resource__pid="AIF_2015__65_6_2331_0", sequence=3)
        self.assertEqual(bib.citation_html, " (2009)")

        # 1 generic ext-link to doi.org (filtered)
        bib = BibItem.objects.get(resource__pid="AIF_2015__65_6_2331_0", sequence=4)
        self.assertEqual(bib.citation_html, " (2009)")
        # check that the doi has been created in a bibitemid
        bibitemid = BibItemId.objects.get(bibitem=bib)
        self.assertEqual(bibitemid.id_value, "10.1016/S1")

        # generic ext-link in comments, kept
        bib = BibItem.objects.get(resource__pid="AIF_2015__65_6_2331_0", sequence=5)
        self.assertEqual(
            bib.citation_html,
            ' (2009) (bla <a href="http://www.toto.org" target="_blank">toto</a> bla)',
        )

        # ext-link to doi.org in comments, kept
        bib = BibItem.objects.get(resource__pid="AIF_2015__65_6_2331_0", sequence=6)
        self.assertEqual(
            bib.citation_html,
            ' (2009) (bla <a href="http://dx.doi.org/10.1016/S1" target="_blank">http://dx.doi.org/10.1016/S1</a> bla)',
        )
        # no bibitemid has been created for the doi
        self.assertEqual(bib.bibitemid_set.count(), 0)

        #  ext-link in source, kept
        bib = BibItem.objects.get(resource__pid="AIF_2015__65_6_2331_0", sequence=7)
        self.assertEqual(
            bib.citation_html,
            '<span class="citation-document-title"> bla <a href="http://dx.doi.org/10.1016/S1" target="_blank">http://dx.doi.org/10.1016/S1</a> bla</span> (2009)',
        )

        # 1 pub-id with pub-id-type="eid"
        bib = BibItem.objects.get(resource__pid="AIF_2015__65_6_2331_0", sequence=12)
        self.assertEqual(bib.citation_html, " (2009), Research Paper 21")
        # check that the eid has been created in a bibitemid
        bibitemid = BibItemId.objects.get(bibitem=bib)
        self.assertEqual(bibitemid.id_value, "Research Paper 21")

        # 1 ext-link with ext-link-type="eid": the eid is added to the citation
        bib = BibItem.objects.get(resource__pid="AIF_2015__65_6_2331_0", sequence=13)
        self.assertEqual(bib.citation_html, " (2009), Research Paper 21")
        # check that the eid has been created in a bibitemid
        bibitemid = BibItemId.objects.get(bibitem=bib)
        self.assertEqual(bibitemid.id_value, "Research Paper 21")

        # 1 generic ext-link, 1 eid
        bib = BibItem.objects.get(resource__pid="AIF_2015__65_6_2331_0", sequence=14)
        self.assertEqual(
            bib.citation_html, ' (2009) <a href="http://www.toto.org" target="_blank">toto</a>'
        )

        ##############################################
        #
        # mixed-citations
        #
        ##############################################
        # generic ext-link, kept
        bib = BibItem.objects.get(resource__pid="AIF_2015__65_6_2331_0", sequence=8)
        self.assertEqual(
            bib.citation_html, 'bla <a href="http://www.toto.org" target="_blank">toto</a> bla'
        )

        # ext-link to numdam-org, kept
        bib = BibItem.objects.get(resource__pid="AIF_2015__65_6_2331_0", sequence=9)
        self.assertEqual(
            bib.citation_html,
            'bla <a href="http://www.numdam.org/item/foo" target="_blank">foo</a> bla',
        )

        # ext-link to doi.org, removed
        bib = BibItem.objects.get(resource__pid="AIF_2015__65_6_2331_0", sequence=10)
        self.assertEqual(bib.citation_html, "bla  bla")

        # uri, kept
        bib = BibItem.objects.get(resource__pid="AIF_2015__65_6_2331_0", sequence=11)
        self.assertEqual(
            bib.citation_html,
            'bla <a href="http://www.toto.org" target="_blank">http://www.toto.org</a> bla',
        )

        cmd.undo()
        cmd0.undo()

        self.check_empty_database()

    def test_21_test_bibitems_citation_bibtex(self):
        print("** test_21_test-citation_bibtex")

        self.check_empty_database()

        f = open("tests/data/xml/journal-afst.xml")
        body = f.read()
        f.close()

        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": body})
        journals = cmd0.do()

        self.assertEqual(len(journals), 1)

        f = open("tests/data/xml/issue-afst.xml")
        body = f.read()
        f.close()

        cmd = xml_cmds.addIssueXmlCmd({"body": body})
        cmd.do()

        bib = BibItem.objects.get(resource__pid="AFST_2007_6_16_4_719_0", sequence=1)

        self.assertEqual(bib.publisher_name, "Hermann")
        self.assertEqual(bib.type, "book")
        self.assertEqual(bib.user_id, "toto")
        self.assertEqual(bib.article_title_tex, "")
        source = "Éléments de mathématique. Fasc. XXXI. Algèbre commutative. Chapitre 7: Diviseurs"
        self.assertEqual(bib.source_tex, source)
        self.assertEqual(bib.publisher_loc, "Paris")
        self.assertEqual(bib.institution, "Princeton University")
        self.assertEqual(bib.series, "Actualités Scientifiques et Industrielles, No. 1314")
        self.assertEqual(bib.volume, "33")
        self.assertEqual(bib.issue, "77")
        self.assertEqual(bib.year, "1965")
        self.assertEqual(bib.comment, "blabla")
        self.assertEqual(bib.fpage, "100")
        self.assertEqual(bib.lpage, "246")
        self.assertEqual(bib.page_range, "iii+146 pp. (1 foldout)")

        bibitemids = BibItemId.objects.filter(bibitem=bib)

        self.assertEqual(bib.bibitemid_set.count(), 2)
        for id in bib.bibitemid_set.all():
            if id.id_type == "zbl-item-id":
                self.assertEqual(id.id_value, "0141.03501")
            else:
                self.assertEqual(id.id_type, "doi")
                self.assertEqual(id.id_value, "10.1023/A:1025962825241")

        self.assertEqual(bib.contributions.count(), 1)
        contribution = bib.contributions.first()
        self.assertEqual(contribution.last_name, "Bourbaki")
        self.assertEqual(contribution.first_name, "N.")
        self.assertEqual(contribution.string_name, "")

        bib = BibItem.objects.get(resource__pid="AFST_2007_6_16_4_719_0", sequence=2)
        self.assertEqual(bib.type, "misc")
        self.assertEqual(bib.user_id, "")
        self.assertEqual(
            bib.article_title_tex,
            "The algebraic theory of context free langages in computer programming and formal systems",
        )
        self.assertEqual(bib.source_tex, "")
        self.assertEqual(bib.publisher_name, "North-Holland publishing Company")
        self.assertEqual(bib.publisher_loc, "Amsterdam")
        self.assertEqual(bib.institution, "")
        self.assertEqual(bib.series, "")
        self.assertEqual(bib.volume, "")
        self.assertEqual(bib.issue, "")
        self.assertEqual(bib.year, "1963")
        self.assertEqual(bib.comment, "")
        self.assertEqual(bib.fpage, "")
        self.assertEqual(bib.lpage, "")
        self.assertEqual(bib.page_range, "")

        # This bibitem has 2 authors
        self.assertEqual(bib.contributions.count(), 2)
        contribs_iter = bib.contributions.iterator()

        contribution = next(contribs_iter)
        self.assertEqual(contribution.last_name, "Chomsky")
        self.assertEqual(contribution.first_name, "N.")
        self.assertEqual(contribution.string_name, "")

        contribution = next(contribs_iter)
        self.assertEqual(contribution.last_name, "Schutzenberger")
        self.assertEqual(contribution.first_name, "M.P.")
        self.assertEqual(contribution.string_name, "")

        bibitemids = BibItemId.objects.filter(bibitem=bib)
        self.assertEqual(bibitemids.count(), 3)
        for id in bib.bibitemid_set.all():
            if id.id_type == "doi":
                self.assertEqual(id.id_value, "10.1016/j.jalgebra.2013.02.034")

        bib = BibItem.objects.get(resource__pid="AFST_2007_6_16_4_719_0", sequence=3)
        self.assertEqual(
            bib.article_title_tex,
            'Hecke operators on <span class="mathjax-formula" data-tex="$\\Gamma _o(m)$"><math xmlns="http://www.w3.org/1998/Math/MathML"><mrow><msub><mi>Γ</mi> <mi>o</mi> </msub><mrow><mo>(</mo><mi>m</mi><mo>)</mo></mrow></mrow></math></span>',
        )

        bib = BibItem.objects.get(resource__pid="AFST_2007_6_16_4_719_0", sequence=4)

        bibitemids = BibItemId.objects.filter(bibitem=bib)
        self.assertEqual(bibitemids.count(), 3)
        for id in bib.bibitemid_set.all():
            if id.id_type == "doi":
                self.assertEqual(id.id_value, "10.1016/j.aim.2011.05.006")

        bib = BibItem.objects.get(resource__pid="AFST_2007_6_16_4_719_0", sequence=5)
        # This bibitem has 3 authors
        self.assertEqual(bib.contributions.count(), 3)
        contribution = bib.contributions.first()
        self.assertEqual(contribution.last_name, "")
        self.assertEqual(contribution.first_name, "")
        self.assertEqual(contribution.string_name, "N. Burq")

        # FIXME : unused bibtex ?
        # from ptf import bibtex
        # bibtex_array = bib.get_bibtex()

        # bib = BibItem.objects.get(resource__pid='AFST_2007_6_16_4_719_0', sequence=2)
        # self.assertEqual(bib.type, '')
        # self.assertEqual(bib.article_title_tex, '')
        # self.assertEqual(bib.source_tex, '')
        # self.assertEqual(bib.publisher_name, '')
        # self.assertEqual(bib.publisher_loc, '')
        # self.assertEqual(bib.institution, '')
        # self.assertEqual(bib.series, '')
        # self.assertEqual(bib.volume, '')
        # self.assertEqual(bib.issue, '')
        # self.assertEqual(bib.year, '')
        # self.assertEqual(bib.comment, '')
        # self.assertEqual(bib.fpage, '')
        # self.assertEqual(bib.lpage, '')
        # self.assertEqual(bib.page_range, '')

        cmd.undo()
        cmd0.undo()

        self.check_empty_database()

    def test_22_update_issue(self):
        print("** test_22_update_issue")

        self.check_empty_database()

        f = open("tests/data/xml/journal-sms.xml")
        body = f.read()
        f.close()

        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": body})
        journals = cmd0.do()
        journal = journals[0]

        # issue-0 is an issue to be published, it does not have any other ids
        # (Zbm, MR)
        f = open("tests/data/xml/issue-0.xml")
        body = f.read()
        f.close()

        cmd1 = xml_cmds.addOrUpdateIssueXmlCmd({"body": body})
        issue0 = cmd1.do()

        self.assertEqual(issue0.my_collection.last_doi, 3002)

        article = Article.objects.get(doi="10.5802/aif.3000")
        self.assertIsNotNone(article)

        # Do some matching manually
        ext_id = ExtId(resource=article, id_type="zbl-item-id", id_value="0061.99999")
        ext_id.save()

        # issue-1 is a volume with final numdam ids, but as it "comes" from copy editing,
        # It does not have other ids in it (note: not realistic, new articles do not have other ids.
        # But this is a quick way to test data preservation)
        f = open("tests/data/xml/issue-1.xml")
        body = f.read()
        f.close()

        settings.ISSUE_TO_APPEAR_PIDS = {"SMS": "SMS_0__0_0"}

        cmd = xml_cmds.addOrUpdateIssueXmlCmd(
            {"body": body, "keep_metadata": True, "backup_folder": settings.TEMP_FOLDER}
        )
        issue = cmd.do()

        self.assertEqual(issue.my_collection.last_doi, 3002)

        self.assertEqual(issue.title_tex, "Problèmes mathématiques de la théorie des automates")
        self.assertEqual(Article.objects.count(), 3)
        if getattr(settings, "DISPLAY_FIRST_NAME_FIRST", False):
            result = self.solrSearch('fau:"Robert Cori"')
        else:
            result = self.solrSearch('fau:"Cori, Robert"')
        self.assertEqual(result.hits, 1)

        # Check that some metadata has been preserved (matching...)
        article = Article.objects.get(doi="10.5802/aif.3000")
        self.assertIsNotNone(article)
        ext_id = ExtId.objects.get(resource=article, id_type="zbl-item-id")
        self.assertEqual(ext_id.id_value, "0061.99999")

        # Test import from archive folder
        # We first need to create the archive (in the temp folder) with extra data
        # We mark the manually added extid as false_positive so that it appears
        # in the json

        ext_id.false_positive = True
        ext_id.save()

        # Manually add an icon
        cmd = ptf_cmds.addorUpdateExtLinkPtfCmd({"rel": "icon", "location": "foo.jpg"})
        cmd.set_resource(article)
        cmd.do()

        # We backup the json in the archive
        cmd = ptf_cmds.exportExtraDataPtfCmd(
            {"pid": "SMS_1969-1970__1_", "export_folder": settings.TEMP_FOLDER}
        )
        cmd.do()

        # We re-import the XML with import_folder as the archive folder
        # extra data should be restored
        cmd = xml_cmds.addOrUpdateIssueXmlCmd(
            {"body": body, "backup_folder": settings.TEMP_FOLDER}
        )
        issue = cmd.do()

        # Check that some metadata has been preserved (matching...)
        article = Article.objects.get(doi="10.5802/aif.3000")
        self.assertIsNotNone(article)
        ext_id = ExtId.objects.get(resource=article, id_type="zbl-item-id")
        self.assertEqual(ext_id.id_value, "0061.99999")
        self.assertEqual(ext_id.false_positive, True)
        ext_link = ExtLink.objects.get(resource=article, rel="icon")
        self.assertEqual(ext_link.location, "foo.jpg")

        f = open("tests/data/xml/issue-2.xml")
        body = f.read()
        f.close()

        cmd = xml_cmds.addOrUpdateIssueXmlCmd({"body": body})
        issue = cmd.do()

        self.assertEqual(issue.title_tex, "Problèmes mathématiques de la théorie des automates")
        self.assertEqual(Article.objects.count(), 3)
        if getattr(settings, "DISPLAY_FIRST_NAME_FIRST", False):
            result = self.solrSearch('fau:"Robert Cori"')
        else:
            result = self.solrSearch('fau:"Cori, Robert"')
        self.assertEqual(result.hits, 1)

        # Check that metadata have not been preserved (matching...)
        self.assertRaises(Resource.DoesNotExist, Article.objects.get, doi="10.5802/aif.3000")
        article = Article.objects.first()
        # No DOI has been assigned during import (assign_doi has not be set in
        # the addOrUpdateIssueXmlCmd params)
        self.assertIsNone(article.doi)
        self.assertRaises(
            ExtId.DoesNotExist, ExtId.objects.get, resource=article, id_type="zbl-item-id"
        )

        cmd = xml_cmds.addOrUpdateIssueXmlCmd({"body": body, "assign_doi": True})
        issue = cmd.do()
        self.assertEqual(issue.my_collection.last_doi, 3004)

        self.assertEqual(issue.title_tex, "Problèmes mathématiques de la théorie des automates")
        self.assertEqual(Article.objects.count(), 3)
        if getattr(settings, "DISPLAY_FIRST_NAME_FIRST", False):
            result = self.solrSearch('fau:"Robert Cori"')
        else:
            result = self.solrSearch('fau:"Cori, Robert"')
        col = Collection.objects.get(pid="SMS")
        self.assertEqual(result.hits, 1)

        # Check that metadata have not been preserved (matching...)
        self.assertRaises(Resource.DoesNotExist, Article.objects.get, doi="10.5802/aif.3000")
        article = Article.objects.get(pid="SMS_1969-1970__1__A1_0")
        col = Collection.objects.get(pid="SMS")
        self.assertEqual(article.doi, "10.5802/sms.3003")
        # article = Article.objects.last()
        # self.assertEqual(article.doi, "10.5802/sms.3005")

        col = article.my_container.my_collection
        self.assertEqual(col.last_doi, 3004)

        # on vérifie qu'il reste un article dans SMS_0__0_0 : il y a 3 articles
        # et seuls 2 sont repris dans le published
        self.assertEqual(issue0.article_set.count(), 1)
        # on doit effacer les 2 issues car la première est gardée étant donné que c'est le volume a paraitre et
        # qu'il peut rester des articles dedans : cas des AIF
        p = model_helpers.get_provider("mathdoc-id")
        cmd1 = ptf_cmds.addContainerPtfCmd({"pid": "SMS_1969-1970__1_", "ctype": "issue"})
        cmd1.set_provider(p)
        cmd1.add_collection(journal)
        cmd1.set_object_to_be_deleted(issue0)
        cmd1.undo()

        cmd1 = ptf_cmds.addContainerPtfCmd({"pid": "SMS_0__0_0", "ctype": "issue"})
        cmd1.set_provider(p)
        cmd1.add_collection(journal)
        cmd1.set_object_to_be_deleted(issue)
        cmd1.undo()

        self.assertEqual(Publisher.objects.count(), 1)
        publisher = Publisher.objects.first()
        cmd1 = ptf_cmds.addPublisherPtfCmd({"pid": publisher.pid})
        cmd1.set_object_to_be_deleted(publisher)
        cmd1.undo()

        cmd0.undo()

        self.check_empty_database()

    def test_23_export_issue(self):
        print("** test_23_export_issue")

        self.check_empty_database()

        f = open("tests/data/xml/collection-ampa.xml")
        body = f.read()
        f.close()

        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": body})
        journals = cmd0.do()
        journal = journals[0]

        f = open("tests/data/xml/issue-ampa.xml")
        body = f.read()
        f.close()

        cmd1 = xml_cmds.addOrUpdateIssueXmlCmd({"body": body})
        cmd1.do()

        body = ptf_cmds.exportPtfCmd({"pid": "AMPA_1810-1811__1_", "with_body": False}).do()
        str_body = body

        cmd2 = xml_cmds.addOrUpdateIssueXmlCmd({"body": str_body})
        issue = cmd2.do()

        # no undo for updateCmds (as they first delete the resource before
        # adding the new one. We need to delete issue)
        p = model_helpers.get_provider("mathdoc-id")
        cmd1 = ptf_cmds.addContainerPtfCmd({"pid": "AMPA_1810-1811__1_", "ctype": "issue"})
        cmd1.set_provider(p)
        cmd1.add_collection(journal)
        cmd1.set_object_to_be_deleted(issue)
        cmd1.undo()

        self.assertEqual(Publisher.objects.count(), 1)
        publisher = Publisher.objects.first()
        cmd1 = ptf_cmds.addPublisherPtfCmd({"pid": publisher.pid})
        cmd1.set_object_to_be_deleted(publisher)
        cmd1.undo()

        cmd0.undo()

        self.assertEqual(Collection.objects.count(), 0)
        self.assertEqual(Container.objects.count(), 0)
        self.assertEqual(Article.objects.count(), 0)
        self.assertEqual(Publisher.objects.count(), 0)

        f = open("tests/data/xml/collection-aif.xml")
        body = f.read()
        f.close()

        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": body})
        journals = cmd0.do()
        journal = journals[0]

        # issue-0 is an issue to be published, it does not have any other ids
        # (Zbm, MR)
        f = open("tests/data/xml/issue-for-matching.xml")
        body = f.read()
        f.close()

        cmd1 = xml_cmds.addOrUpdateIssueXmlCmd({"body": body})
        cmd1.do()

        body = ptf_cmds.exportPtfCmd({"pid": "AIF_2007__57_3"}).do()
        str_body = body

        cmd2 = xml_cmds.addOrUpdateIssueXmlCmd({"body": str_body})
        issue = cmd2.do()

        # no undo for updateCmds (as they first delete the resource before
        # adding the new one. We need to delete issue)
        p = model_helpers.get_provider("mathdoc-id")
        cmd1 = ptf_cmds.addContainerPtfCmd({"pid": "AIF_2007__57_3", "ctype": "issue"})
        cmd1.set_provider(p)
        cmd1.add_collection(journal)
        cmd1.set_object_to_be_deleted(issue)
        cmd1.undo()

        self.assertEqual(Publisher.objects.count(), 1)
        publisher = Publisher.objects.first()
        cmd1 = ptf_cmds.addPublisherPtfCmd({"pid": publisher.pid})
        cmd1.set_object_to_be_deleted(publisher)
        cmd1.undo()

        cmd0.undo()

        self.check_empty_database()

    def test_24_upload_failed(self):
        print("** test_24_upload_failed")

        self.check_empty_database()

        f = open("tests/data/xml/collection-msh.xml")
        body = f.read()
        f.close()

        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": body})
        cmd0.do()

        # issue-0 is an issue to be published, it does not have any other ids
        # (Zbm, MR)
        f = open("tests/data/xml/issue5.xml")
        body = f.read()
        f.close()

        cmd1 = xml_cmds.addIssueXmlCmd({"body": body})
        cmd1.do()

        article = Article.objects.get(pid="MSH_1968__21__5_0")
        self.assertEqual(article.article_number, "17")

        cmd1.undo()
        cmd0.undo()

        self.check_empty_database()

    def test_24a_test_blank(self):
        print("** test_24a_test-blank")

        self.check_empty_database()

        f = open("tests/data/xml/journal-blank.xml")
        body = f.read()
        f.close()

        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": body})
        journals = cmd0.do()

        self.assertEqual(len(journals), 1)

        f = open("tests/data/xml/issue-blank.xml")
        body = f.read()
        f.close()

        cmd = xml_cmds.addIssueXmlCmd({"body": body})
        cmd.do()

        stat = Stats.objects.first()
        self.assertIsNotNone(stat)
        self.assertEqual(stat.value, 70)

        cmd.undo()
        cmd0.undo()

        stat = Stats.objects.first()
        self.assertIsNotNone(stat)
        self.assertEqual(stat.value, 0)

        self.check_empty_database()

        # f = open('tests/data/xml/book-blank.xml')
        # body = f.read()
        # f.close()
        # cmd = xml_cmds.addBookXmlCmd( { "body":body } )
        # book = cmd.do()

        # cmd.undo()
        # cmd1.undo()
        # publisher = book.my_publisher
        # cmd = xml_cmds.updateBookXmlCmd( { "body":body } )
        # book = cmd.do()
        # publisher = book.my_publisher
        # p = model_helpers.get_provider( "mathdoc-id" )
        # cmd = ptf_cmds.addContainerPtfCmd( { "pid": "THESE_1916__11__1_0" , 'ctype': "monograph"} )
        # cmd.set_provider( p )
        # cmd.undo()
        # Collection.objects.all().delete()
        # Publisher.objects.all().delete()

    def test_25_deployed_date(self):
        settings.DEBUG = True
        print("** test_25_deployed_date")

        self.check_empty_database()

        # ----------------------------------------------------------------------------------------
        # 1. Import d'un XML sur un site de revue
        #    - avec last-modified
        #    - avec prod-deployed-date sur des resources, mais pas partout
        #      (Note: ce test ne reflète pas la réalité de dec 2018: tous les articles ont
        #       les mêmes dates last-modified et prod-deployed-date que leur issue.
        #       On garde les test car cela permet de tester le cas avec ou sans prod-deployed-date)
        #    Les dates du XML doivent se retrouver dans les objets.
        #    S'il n'y a pas de prod-deployed-date, un SiteMembership est créé avec now()

        f = open("tests/data/xml/collection-aif.xml")
        body = f.read()
        f.close()
        cmdCol = xml_cmds.addCollectionsXmlCmd({"body": body})
        journals = cmdCol.do()

        self.assertEqual(len(journals), 1)
        journal = journals[0]

        f = open("tests/data/xml/issue-with-last-modified-and-published.xml")
        body = f.read()
        f.close()

        cmd = xml_cmds.addIssueXmlCmd({"body": body})
        issue = cmd.do()
        self.assertEqual(issue.last_modified.strftime("%Y-%m-%d"), "2017-06-28")
        self.assertEqual(issue.deployed_date().strftime("%Y-%m-%d"), "2017-10-28")

        # on teste si les articles ont bien ces dates de deploiement
        article = model_helpers.get_article("AIF_2007__57_3_693_0")
        self.assertEqual(article.deployed_date().strftime("%Y-%m-%d"), "2012-02-15")
        # cet article est "nouveau" il n'a pas de date de deployement en prod,
        # il prend la date du jour
        article = model_helpers.get_article("AIF_2007__57_3_703_0")
        self.assertEqual(
            article.deployed_date().strftime("%Y-%m-%d"), timezone.now().strftime("%Y-%m-%d")
        )
        cmd.undo()
        cmdCol.undo()

        # ----------------------------------------------------------------------------------------
        # 2. Import d'un XML sur ptf-tools, avec last-modified et prod-deployed-date
        #    C'est le cas d'un (re)import depuis /mathdoc_archive
        #    On vérifie que les dates se retrouvent dans les objets.

        aif_site = PtfSite(id=1, domain="aif", name="aif", acro="aif")
        aif_site.save()

        temp_settingsSITE_ID = settings.SITE_ID
        temp_settingsSITE_NAME = settings.SITE_NAME
        settings.SITE_ID = "2"
        settings.SITE_NAME = "ptf_tools"
        site = PtfSite(id=settings.SITE_ID, domain="ptf-tools", name="ptf_tools", acro="ptf-tools")
        site.save()

        f = open("tests/data/xml/collection-aif.xml")
        body = f.read()
        f.close()
        cmd2Col = xml_cmds.addCollectionsXmlCmd({"body": body})
        journals = cmd2Col.do()

        self.assertEqual(len(journals), 1)

        f = open("tests/data/xml/issue-with-last-modified-and-published.xml")
        body = f.read()
        f.close()

        cmd1 = xml_cmds.addIssueXmlCmd({"body": body})
        issue1 = cmd1.do()

        self.assertEqual(issue.last_modified.strftime("%Y-%m-%d"), "2017-06-28")
        self.assertEqual(issue1.deployed_date().strftime("%Y-%m-%d"), "2017-10-28")
        self.assertEqual(issue1.deployed_date(aif_site).strftime("%Y-%m-%d"), "2017-10-28")
        # on test si l'export en xml integre bien la date prod-deployed-date du
        # site lié à la collection
        out = io.StringIO()

        call_command("export", "-pid=AIF_2007__57_3", stdout=out)
        dataXml = out.getvalue()

        tree = etree.XML(dataXml)

        # on teste la date au niveau de l'issue
        date = tree.xpath("//issue-meta/history/date[@date-type='prod-deployed-date']")[0].get(
            "iso-8601-date"
        )
        date = model_helpers.parse_date_str(date)
        self.assertEqual(date.strftime("%Y-%m-%d"), "2017-10-28")

        # on teste la date au niveau du 1er article
        # En Dec 2018, la prod_deployed_date d'un article est égale à celle de son issue.
        date = tree.xpath("//article-meta/history/date[@date-type='prod-deployed-date']")[0].get(
            "iso-8601-date"
        )
        date = model_helpers.parse_date_str(date)
        self.assertEqual(date.strftime("%Y-%m-%d"), "2017-10-28")

        # on verifie que dans nouvel import sur PTF TOOLS la date article est
        # bonne
        article1 = model_helpers.get_article("AIF_2007__57_3_693_0")
        self.assertEqual(article1.deployed_date().strftime("%Y-%m-%d"), "2017-10-28")
        article1 = model_helpers.get_article("AIF_2007__57_3_703_0")
        self.assertEqual(article1.deployed_date().strftime("%Y-%m-%d"), "2017-10-28")
        self.assertIsNone(article1.date_published)

        cmd1.undo()

        # ----------------------------------------------------------------------------------------
        # 3. Import dans ptf-tools d'un XML sans date (last-modified ou prod-deployed-date)
        #    C'est le cas d'un 1er import Cedrics.
        #    On vérifie que last-modified est bien now() (au jour près)
        #    et que prod-deploye-date est None (article pas encore en prod)

        f = open("tests/data/xml/issue-without-history.xml")
        body = f.read()
        f.close()

        cmd2 = xml_cmds.addIssueXmlCmd({"body": body})
        issue2 = cmd2.do()

        self.assertEqual(
            issue2.deployed_date(site).strftime("%Y-%m-%d"), timezone.now().strftime("%Y-%m-%d")
        )
        self.assertEqual(
            issue2.last_modified.strftime("%Y-%m-%d"), timezone.now().strftime("%Y-%m-%d")
        )
        self.assertIsNone(issue2.deployed_date(aif_site))

        # Déployement. On vérifie que le date_published est créé
        cmd = ptf_cmds.publishResourcePtfCmd()
        cmd.set_resource(issue2)
        cmd.do()
        article1 = model_helpers.get_article("AIF_2015__65_6_2331_0")
        self.assertIsNone(article1.date_published)

        issue2.year = "2019"
        issue2.save()
        issue2 = model_helpers.get_container(issue2.pid)
        cmd = ptf_cmds.publishResourcePtfCmd()
        cmd.set_resource(issue2)
        cmd.do()

        article1 = model_helpers.get_article("AIF_2015__65_6_2331_0")
        self.assertEqual(
            article1.date_published.strftime("%Y-%m-%d"), timezone.now().strftime("%Y-%m-%d")
        )

        cmd2.undo()
        cmd2Col.undo()

        # ----------------------------------------------------------------------------------------
        # 4. Re-import dans ptf-tools avec keep_metadata=True. On vérifie que
        # - last_modified devient celui du XML
        # - prod_deployed_date est préservée
        f = open("tests/data/xml/collection-aif.xml")
        body = f.read()
        f.close()
        cmd2Col = xml_cmds.addCollectionsXmlCmd({"body": body})
        journals = cmd2Col.do()

        self.assertEqual(len(journals), 1)

        f = open("tests/data/xml/issue-with-last-modified-and-published.xml")
        body = f.read()
        f.close()

        cmd1 = xml_cmds.addIssueXmlCmd({"body": body})
        cmd1.do()

        # On modifie qqch et on vérifie que last_modified devient now(),
        # et que prod_deployed_date est inchangée
        bibitemid = BibItemId.objects.first()
        model_helpers.add_or_update_bibitemid(
            bibitemid.bibitem, bibitemid.id_type, bibitemid.id_value, True, True
        )

        issue = Container.objects.first()
        self.assertEqual(
            issue.last_modified.strftime("%Y-%m-%d"), timezone.now().strftime("%Y-%m-%d")
        )
        self.assertEqual(issue.deployed_date(aif_site).strftime("%Y-%m-%d"), "2017-10-28")

        # On déploie et on vérifie que prod_deployed_date devient now()
        model_helpers.update_deployed_date(issue, aif_site)
        issue = Container.objects.first()
        self.assertEqual(
            issue.deployed_date(aif_site).strftime("%Y-%m-%d"), timezone.now().strftime("%Y-%m-%d")
        )

        # On re-importe avec keep_metadata=True et on vérifie que
        # - last_modified devient now()
        # - prod_deployed_date est préservée (donc égale à now)

        f = open("tests/data/xml/issue-without-last-modified-and-published.xml")
        body = f.read()
        f.close()

        cmd2 = xml_cmds.addOrUpdateIssueXmlCmd(
            {"body": body, "keep_metadata": True, "backup_folder": settings.TEMP_FOLDER}
        )
        issue = cmd2.do()

        self.assertEqual(
            issue.last_modified.strftime("%Y-%m-%d"), timezone.now().strftime("%Y-%m-%d")
        )
        self.assertEqual(
            issue.deployed_date(aif_site).strftime("%Y-%m-%d"), timezone.now().strftime("%Y-%m-%d")
        )

        # TODO. Si le XML a un last_modified. Il est utilisé.
        # Or avec keep_metadata=True (ex: on a fait du matching), le last_modified du XML n'est pas
        # forcément bon. Ce cas ne se produit pas en dec 2018, car le XML de Cedrics n'a pas de
        # las_modified

        # ----------------------------------------------------------------------------------------
        # 5. Re-import dans ptf-tools avec keep_metadata=False et d'un XML qui n'a pas d'info.
        # last_modified doit être à now() et prod_deployed_date à None
        cmd2 = xml_cmds.addOrUpdateIssueXmlCmd({"body": body})
        issue = cmd2.do()

        self.assertEqual(
            issue.last_modified.strftime("%Y-%m-%d"), timezone.now().strftime("%Y-%m-%d")
        )
        self.assertIsNone(issue.deployed_date(aif_site))

        p = model_helpers.get_provider("mathdoc-id")
        cmd1 = ptf_cmds.addContainerPtfCmd({"pid": "AIF_2007__57_3", "ctype": "issue"})
        cmd1.set_provider(p)
        cmd1.add_collection(journal)
        cmd1.set_object_to_be_deleted(issue)
        cmd1.undo()

        cmd2Col.undo()

        self.assertEqual(Publisher.objects.count(), 1)
        publisher = Publisher.objects.first()
        cmd1 = ptf_cmds.addPublisherPtfCmd({"pid": publisher.pid})
        cmd1.set_object_to_be_deleted(publisher)
        cmd1.undo()

        settings.SITE_NAME = temp_settingsSITE_NAME
        settings.SITE_ID = temp_settingsSITE_ID

        self.check_empty_database()

    def test_26_test_these(self):
        print("** test_26_test_these")

        self.check_empty_database()

        f = open("tests/data/xml/collection-these.xml")
        body = f.read()
        f.close()

        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": body})
        journals = cmd0.do()

        self.assertEqual(len(journals), 1)
        journal = journals[0]
        self.assertEqual(journal.coltype, "these")

        f = open("tests/data/xml/book-these.xml")
        body = f.read()
        f.close()

        cmd = xml_cmds.addBookXmlCmd({"body": body})
        cmd.do()

        cmd.undo()
        cmd0.undo()

        self.check_empty_database()

    def test_27_test_multi_col(self):
        print("** test_27_test_multi-col")

        self.check_empty_database()

        f = open("tests/data/xml/collection-ast.xml")
        body = f.read()
        f.close()

        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": body})
        ast = cmd0.do()[0]

        f = open("tests/data/xml/collection-sb.xml")
        body = f.read()
        f.close()

        cmd1 = xml_cmds.addCollectionsXmlCmd({"body": body})
        sb = cmd1.do()[0]

        f = open("tests/data/xml/book-ast-sb.xml")
        body = f.read()
        f.close()

        cmd = xml_cmds.addBookXmlCmd({"body": body})
        book = cmd.do()

        self.assertEqual(book.my_collection, ast)
        self.assertEqual(book.number, "134")
        self.assertEqual(book.title_html, "Séminaire Bourbaki. Volume 1987/88 - Exposés 686-699")
        other_cols = book.my_other_collections.all()
        self.assertEqual(other_cols.count(), 1)
        self.assertEqual(other_cols.first(), sb)
        col_membership_qs = book.collectionmembership_set.all()
        self.assertEqual(col_membership_qs.count(), 1)
        col_mb = col_membership_qs.first()
        self.assertEqual(col_mb.collection, sb)
        self.assertEqual(col_mb.container, book)
        self.assertEqual(col_mb.number, "30")

        cmd.undo()
        cmd1.undo()
        cmd0.undo()

        self.check_empty_database()

    def test_28_test_ref_doi(self):
        print("** test_28_test_ref_doi")

        self.check_empty_database()

        f = open("tests/data/xml/collection-aif.xml")
        body = f.read()
        f.close()

        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": body})
        journals = cmd0.do()

        self.assertEqual(len(journals), 1)

        f = open("tests/data/xml/issue-aif.xml")
        body = f.read()
        f.close()

        cmd = xml_cmds.addIssueXmlCmd({"body": body})
        issue = cmd.do()

        bib = BibItem.objects.get(resource__pid="AIF_2015__65_6_2331_0", sequence=1)

        self.assertEqual(bib.bibitemid_set.count(), 3)

        article = issue.article_set.first()
        contribs_iter = article.contributions.iterator()

        contribution = next(contribs_iter)
        self.assertEqual(contribution.deceased_before_publication, True)

        contribution = next(contribs_iter)
        self.assertEqual(contribution.deceased_before_publication, False)

        cmd.undo()
        cmd0.undo()

        self.check_empty_database()

    def test_28_test_trans_title_only(self):
        print("** test_28_test_trans_title_only")
        self.check_empty_database()
        with open("tests/data/xml/collection-ast.xml") as f:
            body = f.read()

        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": body})
        journals = cmd0.do()

        self.assertEqual(len(journals), 1)

        with open("tests/data/xml/book-ast.xml") as f:
            body = f.read()

        cmd = xml_cmds.addBookXmlCmd({"body": body})
        cmd.do()
        cmd.undo()
        cmd0.undo()
        self.check_empty_database()

    def test_29_fetch_tex(self):
        print("** test_29 fetch_tex")

        self.check_empty_database()

        f = open("tests/data/xml/collection-aif.xml")
        body = f.read()
        f.close()

        cmd1 = xml_cmds.addCollectionsXmlCmd({"body": body})
        cmd1.do()

        f = open("tests/data/xml/issue-aif.xml")
        body = f.read()
        f.close()

        old_folder = settings.CEDRAM_TEX_FOLDER
        settings.CEDRAM_TEX_FOLDER = "tests/data"

        cmd2 = xml_cmds.addIssueXmlCmd({"body": body})
        cmd2.do()

        settings.CEDRAM_TEX_FOLDER = old_folder
        cmd2.undo()
        cmd1.undo()

        self.check_empty_database()

    def test_30_abstract(self):
        print("** test_30_abstract")

        self.check_empty_database()

        f = open("tests/data/xml/collection-msh.xml")
        body = f.read()
        f.close()

        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": body})
        cmd0.do()

        # issue-0 is an issue to be published, it does not have any other ids
        # (Zbm, MR)
        f = open("tests/data/xml/issue5.xml")
        body = f.read()
        f.close()

        cmd1 = xml_cmds.addIssueXmlCmd({"body": body})
        cmd1.do()

        article = Article.objects.get(pid="MSH_1968__21__5_0")
        abstract = article.abstract_set.first()

        self.assertEqual(
            abstract.value_html,
            '<p>At the <strong>1912</strong> Congress</p><ol type="i"><li>Are there infinitely many primes</li><li>The (Binary) Goldbach Conjecture</li><li>The Twin Prime Conjecture.</li></ol><p>All these problems are still <span class="italique">open</span>.</p><p class="align-right">Me</p>',
        )
        cmd1.undo()
        cmd0.undo()

        self.check_empty_database()

    def test_31_ref_with_editors(self):
        print("** test_31_ref_with_editors")

        settings.REF_JEP_STYLE = True
        settings.DISPLAY_FIRST_NAME_FIRST = True

        self.check_empty_database()

        f = open("tests/data/xml/journal-sms.xml")
        body = f.read()
        f.close()

        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": body})
        cmd0.do()

        f = open("tests/data/xml/issue3.xml")
        body = f.read()
        f.close()

        cmd1 = xml_cmds.addIssueXmlCmd({"body": body})
        cmd1.do()

        bib = BibItem.objects.get(resource__pid="SMS_1969-1970__1__A1_0", sequence=2)
        self.assertEqual(
            bib.citation_html,
            '[Tro90] <span class="citation-author">M. Troyanov</span><span class="citation-document-title"> - &ldquo;Espaces à courbure négative et groupes hyperboliques&rdquo;</span> (E. Ghys & P. de la Harpe, eds.)<span class="citation-series">, Progress in Math.</span><span class="citation-volume-incollection">, vol. 83</span>, Birkhäuser, 1990, p. 47-66',
        )

        settings.REF_JEP_STYLE = False
        settings.DISPLAY_FIRST_NAME_FIRST = False

        cmd1.undo()
        cmd0.undo()

        self.check_empty_database()

    def test_32_monograph_with_warnings(self):
        print("** test_32_monograph_with_warnings **")

        self.check_empty_database()

        f = open("tests/data/xml/collection.xml")
        body = f.read()
        f.close()

        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": body})
        collections = cmd0.do()

        self.assertEqual(len(collections), 1)
        collection = collections[0]

        self.assertEqual(collection.title_tex, "Cours de l'institut Fourier")
        self.assertEqual(collection.wall, 2)
        self.assertEqual(collection.pid, "CIF")
        self.assertEqual(collection.fyear, 0)

        f = open("tests/data/xml/book-monograph-with-3warnings.xml")
        body = f.read()
        f.close()

        cmd = xml_cmds.addOrUpdateBookXmlCmd({"body": body})
        book = cmd.do()
        warnings = cmd.warnings
        self.assertEqual(len(warnings), 3)
        # test prise en compte toc
        my_book = Container.objects.get(id=book.id)

        self.assertEqual(Container.objects.count(), 1)

        # check citation_bibtex
        request = self.factory.get("/item/CIF_1970-1971__3__1_0")
        bibtex = my_book.get_bibtex(request)
        self.maxDiff = None
        self.assertEqual(
            bibtex,
            "@book{CIF_1970-1971__3__1_0,\n     author = {Malgrange, B.},\n     title = {Op\\'erateurs pseudo-diff\\'erentiels et op\\'erateurs de {Fourier}},\n     series = {Cours de l'institut Fourier},\n     publisher = {Institut des Math\\'ematiques Pures - Universit\\'e Scientifique et M\\'edicale de Grenoble},\n     number = {3},\n     year = {1970-1971},\n     language = {fr},\n     url = {http://testserver/item/CIF_1970-1971__3__1_0/}\n}",
        )
        cmd.undo()
        cmd0.undo()

        self.assertEqual(Publisher.objects.count(), 1)

        publisher = Publisher.objects.first()
        cmd1 = ptf_cmds.addPublisherPtfCmd()
        cmd1.set_object_to_be_deleted(publisher)
        cmd1.undo()

        self.check_empty_database()

    def test_33_body(self):
        print("** test_33_body **")

        self.check_empty_database()

        f = open("tests/data/xml/collection.xml")
        body = f.read()
        f.close()

        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": body})
        cmd0.do()

    def test_34_add_pcj_article(self):
        """
        Test the publication of an article in PCJ with the conversion from docx to html
        Input: an article xml and a html file
        Output: an article with a body_html from the html file
        """
        print("** test_34_add_pcj_article **")
        self.check_empty_database()

        self.assertRaises(ValueError, xml_cmds.addPCJArticleXmlCmd().do)
        # We first need to create a collection for pcj journal
        with open("tests/data/xml/collection-pcj.xml") as col:
            body_col = col.read()

        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": body_col})
        collections = cmd0.do()
        self.assertEqual(len(collections), 1)

        collection = collections[0]
        self.assertEqual(collection.pid, "PCJ")

        # we also need to create the issue the article be part of
        with open("tests/data/xml/issue-pcj.xml") as issue_xml:
            body = issue_xml.read()

        cmd1 = xml_cmds.addIssueXmlCmd({"body": body})
        issue = cmd1.do()

        self.assertEqual(Container.objects.count(), 1)
        self.assertEqual(issue.pid, "PCJ_2024__4__")
        issue = Container.objects.first()

        # Now we can add the article
        with open("tests/data/xml/article.xml") as article_xml:
            body = article_xml.read()

        loc = os.path.dirname(os.path.abspath(__file__)).split("/")
        html_file_name = "/".join(loc[:-2]) + "/tests/data/html/10_24072_pcjournal_408.html"
        from_folder = "/".join(loc[:-2]) + "/tests/data"
        collection = Collection.objects.get(pid="PCJ")
        self.assertEqual(os.path.isfile(html_file_name), True)
        self.assertEqual(os.path.isdir(from_folder), True)
        self.assertIsInstance(collection, Collection)
        self.assertEqual(collection.pid, "PCJ")

        cmd = xml_cmds.addPCJArticleXmlCmd(
            {
                "body": body,
                "issue": issue,
                "standalone": True,
                "html_file_name": html_file_name,
                "from_folder": from_folder,
                "collection": collection,
            }
        )
        article = cmd.do()
        # we check the presence of the html version. It implies that the datastream has been created
        # on the xml file in addPCJArticleXmlCmd
        self.assertNotEqual(article.body_html, "")
        self.assertEqual(article.pid, "10_24072_pcjournal_408")
        self.assertEqual(article.title_html, "Test addPCJArticleXmlCmd\n")
