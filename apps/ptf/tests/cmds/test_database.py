from django.conf import settings
from django.test import TestCase

from ... import exceptions
from ... import model_helpers
from ...cmds.database_cmds import addArticleDatabaseCmd
from ...cmds.database_cmds import addCollectionDatabaseCmd
from ...cmds.database_cmds import addContainerDatabaseCmd
from ...cmds.database_cmds import addExtLinkDatabaseCmd
from ...cmds.database_cmds import addProviderDatabaseCmd
from ...cmds.database_cmds import addPublisherDatabaseCmd
from ...cmds.database_cmds import addSiteDatabaseCmd
from ...cmds.database_cmds import addXmlBaseDatabaseCmd
from ...cmds.database_cmds import updateCollectionDatabaseCmd
from ...model_data import create_articledata
from ...model_data import create_bookdata
from ...model_data import create_collectiondata
from ...model_data import create_contributor
from ...model_data import create_issuedata
from ...model_data import create_publicationdata
from ...model_data import create_publisherdata
from ...models import Abstract
from ...models import Article
from ...models import Author
from ...models import Collection
from ...models import CollectionMembership
from ...models import Container
from ...models import Contribution
from ...models import ExtLink
from ...models import Provider
from ...models import PtfSite
from ...models import Publisher
from ...models import Resource
from ...models import ResourceId
from ...models import XmlBase


class ImportTestCase(TestCase):
    def setUp(self):
        pass

    def test_01_provider(self):
        print("** Test: test_database provider")

        self.assertRaises(ValueError, addProviderDatabaseCmd(None).do)

        cmd = addProviderDatabaseCmd({"name": "foo-numdam", "pid_type": "foo-mathdoc-id"})
        p = cmd.do()

        # the provider "mathdoc" is added by default with
        # fixtures/initial_data.json
        self.assertEqual(Provider.objects.count(), 2)
        self.assertEqual(p.name, "foo-numdam")
        self.assertIsNone(p.sid_type)

        try:
            cmd.do()
            self.fail(
                "addProviderDatabaseCmd should raise ResourceExist if the provider already exists"
            )
        except exceptions.ResourceExists:
            pass

        self.assertEqual(Provider.objects.count(), 2)

        cmd.undo()
        self.assertRaises(Provider.DoesNotExist, Provider.objects.get, name="foo-numdam")

        # Attempting to remove a 2nd time should raise
        # exceptions.ResourceDoesNotExist
        self.assertRaises(exceptions.ResourceDoesNotExist, cmd.undo)

    def test_02_collection(self):
        print("** Test: test_database collection")

        self.assertRaises(ValueError, addCollectionDatabaseCmd().do)

        # a Collection needs a Provider
        # numdam is added by initial_data.json
        # cmd0 = addProviderDatabaseCmd( { "name":"numdam", "pid_type":"mathdoc-id" } )
        # p = cmd0.do()
        p = model_helpers.get_provider("mathdoc-id")

        xcol = create_publicationdata()
        xcol.coltype = "journal"
        xcol.pid = "AFST"
        xcol.title_tex = "Mon Journal"
        xcol.ids = [("cedram-id", "AFST"), ("issn", "0240-2963")]
        cmd = addCollectionDatabaseCmd({"xobj": xcol})
        cmd.set_provider(p)
        collection = cmd.do()

        self.assertEqual(Collection.objects.count(), 1)
        self.assertEqual(ResourceId.objects.count(), 2)
        self.assertEqual(collection.pid, "AFST")
        self.assertIsNone(collection.sid)
        self.assertEqual(collection.title_tex, "Mon Journal")
        self.assertEqual(collection.provider, p)

        # Adding a new resource should mark it as 'deployed' to the current
        # site
        site = model_helpers.get_or_create_site(settings.SITE_ID)
        self.assertEqual(collection.is_deployed(site), True)

        cmd.undo()
        self.assertRaises(Resource.DoesNotExist, Collection.objects.get, pid="AFST", provider=p)
        self.assertEqual(ResourceId.objects.count(), 0)

        # Attempting to remove a 2nd time should raise
        # exceptions.ResourceDoesNotExist
        self.assertRaises(exceptions.ResourceDoesNotExist, cmd.undo)

        # cmd0.undo()

    def test_03_site(self):
        print("** Test: test_database site")
        # self.assertFalse( PtfSite.objects.all().exists() )

        # If no init params are passed, the cmd uses global settings SITE_NAME
        # & SITE_DOMAIN
        try:
            cmd = addSiteDatabaseCmd(
                {"site_name": "toto", "site_domain": "www.toto.com", "site_id": 10}
            )
        except ValueError:
            self.fail("addSiteDatabaseCmd constructor should not fail")

        cmd.do()

        # numdam is added by default wit initial_data.json
        self.assertEqual(PtfSite.objects.count(), 2)

        # Check that Add again has no effect
        try:
            cmd.do()
            self.fail("addSiteDatabaseCmd should raise ResourceExist if the site already exists")
        except exceptions.ResourceExists:
            pass

        self.assertEqual(PtfSite.objects.count(), 2)

        site = None
        try:
            site = PtfSite.objects.get(acro="toto")
        except PtfSite.DoesNotExist:
            self.fail("toto does not exist")

        # Add and deploy a resource to check that undo fails
        # cmd1 = addProviderDatabaseCmd( { "name":"numdam", "pid_type":"mathdoc-id" } )
        # p = cmd1.do()
        p = model_helpers.get_provider("mathdoc-id")

        xcol = create_publicationdata()
        xcol.coltype = "journal"
        xcol.pid = "AFST"
        xcol.title_tex = "Mon Journal"
        cmd2 = addCollectionDatabaseCmd({"xobj": xcol})
        cmd2.set_provider(p)
        s = cmd2.do()

        s.deploy(site)

        # Shouldn't be able to remove the site: some resources are still
        # deployed
        self.assertRaises(RuntimeError, cmd.undo)

        s.undeploy(site)

        try:
            cmd.undo()
        except RuntimeError:
            self.fail("Shouldn't raise an exception since no resources are deployed")

        # Attempting to remove a 2nd time should raise
        # exceptions.ResourceDoesNotExist
        self.assertRaises(exceptions.ResourceDoesNotExist, cmd.undo)

        cmd2.undo()
        # cmd1.undo()

    def test_04_container(self):
        print("** Test: test_database container")
        self.assertRaises(ValueError, addContainerDatabaseCmd().do)

        # a Collection needs a Provider
        # cmd0 = addProviderDatabaseCmd( { "name":"numdam", "pid_type":"mathdoc-id" } )
        # p = cmd0.do()
        p = model_helpers.get_provider("mathdoc-id")

        xcol = create_publicationdata()
        xcol.coltype = "journal"
        xcol.pid = "AFST"
        xcol.title_tex = "Mon Journal"
        cmd1 = addCollectionDatabaseCmd({"xobj": xcol})
        cmd1.set_provider(p)
        s = cmd1.do()

        xissue = create_issuedata()
        xissue.ctype = "issue"
        xissue.pid = "ASENS_2013_4_46_6"
        xissue.year = 2013
        xissue.vseries = 4
        xissue.volume = 46
        xissue.number = 6
        xissue.last_modified_iso_8601_date_str = "2012-06-01T00:00:00"
        text = "This is an abstract"
        xissue.abstracts = [
            {
                "tag": "trans-abstract",
                "lang": "fr",
                "value_html": text,
                "value_xml": text,
                "value_tex": text,
            }
        ]
        cmd = addContainerDatabaseCmd({"xobj": xissue})

        cmd.add_collection(s)
        cmd.set_provider(p)
        container = cmd.do()

        self.assertEqual(Container.objects.count(), 1)
        self.assertEqual(container.pid, "ASENS_2013_4_46_6")
        self.assertIsNone(container.sid)
        self.assertEqual(container.my_collection, s)
        self.assertEqual(container.provider, p)

        self.assertEqual(Abstract.objects.count(), 1)
        self.assertEqual(container.abstract_set.count(), 1)
        self.assertEqual(container.abstract_set.first().value_html, text)

        cmd.undo()
        self.assertRaises(
            Resource.DoesNotExist,
            Container.objects.get,
            pid="ASENS_2013_4_46_6",
            provider=p,
            my_collection=s,
        )
        self.assertEqual(Abstract.objects.count(), 0)

        # Attempting to remove a 2nd time should raise
        # exceptions.ResourceDoesNotExist
        self.assertRaises(exceptions.ResourceDoesNotExist, cmd.undo)

        cmd1.undo()
        # cmd0.undo()

    def test_05_extlink(self):
        print("** Test: test_database extlink")
        self.assertRaises(ValueError, addExtLinkDatabaseCmd().do)

        # An extlink needs a resource. Even if in real life, an extlink is attached to an container,
        # we use a collection here, as it is simpler to create

        # cmd0 = addProviderDatabaseCmd( { "name":"numdam", "pid_type":"mathdoc-id" } )
        # p = cmd0.do()
        p = model_helpers.get_provider("mathdoc-id")

        xcol = create_publicationdata()
        xcol.coltype = "journal"
        xcol.pid = "AFST"
        xcol.title_tex = "Mon Journal"
        cmd1 = addCollectionDatabaseCmd({"xobj": xcol})
        cmd1.set_provider(p)
        s = cmd1.do()

        cmd2 = addXmlBaseDatabaseCmd({"base": "http://archive.numdam.org/article"})
        b = cmd2.do()

        cmd = addExtLinkDatabaseCmd(
            {
                "rel": "website",
                "mimetype": "",
                "location": "http://jtnb.cedram.org",
                "metadata": "website",
                "seq": 1,
            }
        )

        self.assertRaises(ValueError, cmd.do)  # cmd needs a base
        cmd.set_base(b)
        self.assertRaises(ValueError, cmd.do)  # cmd needs a resource
        cmd.set_resource(s)

        e = cmd.do()
        self.assertEqual(ExtLink.objects.count(), 1)
        self.assertEqual(e.rel, "website")
        self.assertEqual(e.mimetype, "")
        self.assertEqual(e.location, "http://jtnb.cedram.org")
        self.assertEqual(e.metadata, "website")
        self.assertEqual(e.resource, s)
        self.assertEqual(e.base, b)

        cmd1.undo()
        # Django automatically delete related objects
        self.assertEqual(ExtLink.objects.count(), 0)

        # XmlBase is not directly related to the collection, so Django does not
        # remove it
        self.assertEqual(XmlBase.objects.count(), 1)

        # Attempting to remove a 2nd time should raise
        # exceptions.ResourceDoesNotExist
        self.assertRaises(exceptions.ResourceDoesNotExist, cmd.undo)

        cmd2.undo()
        # cmd0.undo()

    def test_05_publisher(self):
        print("** Test: test_database publisher")
        self.assertRaises(ValueError, addPublisherDatabaseCmd().do)

        p = model_helpers.get_provider("mathdoc-id")

        # , "location": 'Grenoble (France)' } )
        xpub = create_publisherdata()
        xpub.name = "Institut Fourier"
        cmd = addPublisherDatabaseCmd({"xobj": xpub})
        publisher = cmd.do()

        self.assertEqual(Publisher.objects.count(), 1)
        self.assertEqual(publisher.pub_name, "Institut Fourier")
        self.assertEqual(publisher.pub_loc, "")
        self.assertEqual(publisher.pub_key, "institut-fourier")

        self.assertEqual(Provider.objects.count(), 1)
        try:
            Provider.objects.get(pid_type="mathdoc-id")
        except Provider.DoesNotExist:
            self.fail("The mathdoc provider does not exist")

        try:
            cmd.do()
            self.fail(
                "addPublisherDatabaseCmd should raise ResourceExists if the publisher already exists"
            )
        except exceptions.ResourceExists:
            pass

        self.assertEqual(Publisher.objects.count(), 1)

        cmd.undo()
        self.assertRaises(
            Publisher.DoesNotExist, Publisher.objects.get, pub_key="institut-fourier"
        )
        # Removing a publisher should remove the corresponding resource
        self.assertEqual(Resource.objects.count(), 0)

        # Attempting to remove a 2nd time should raise
        # exceptions.ResourceDoesNotExist
        self.assertRaises(exceptions.ResourceDoesNotExist, cmd.undo)

        publisher = cmd.do()

        # a Collection that needs a Provider
        # cmd0 = addProviderDatabaseCmd( { "name":"numdam", "pid_type":"mathdoc-id" } )
        # p = cmd0.do()
        p = model_helpers.get_provider("mathdoc-id")

        xcol = create_publicationdata()
        xcol.coltype = "journal"
        xcol.pid = "AFST"
        xcol.title_tex = "Mon Journal"
        cmd1 = addCollectionDatabaseCmd({"xobj": xcol})
        cmd1.set_provider(p)
        s = cmd1.do()

        xissue = create_issuedata()
        xissue.ctype = "issue"
        xissue.pid = "ASENS_2013_4_46_6"
        xissue.year = 2013
        xissue.vseries = 4
        xissue.volume = 46
        xissue.number = 6
        xissue.last_modified_iso_8601_date_str = "2012-06-01T00:00:00"
        xissue.publisher = xpub
        cmd2 = addContainerDatabaseCmd({"xobj": xissue})
        cmd2.add_collection(s)
        cmd2.set_provider(p)
        cmd2.do()

        # The publisher can't be removed as we declared the collection to its
        # publisher
        self.assertRaises(RuntimeError, cmd.undo)

        cmd2.undo()
        cmd1.undo()
        # cmd0.undo()

        # Auto removal of the publisher if it no longer publishes anything
        self.assertEqual(Publisher.objects.count(), 0)

    def test_06_article(self):
        print("** Test: test_database article")
        self.assertRaises(ValueError, addArticleDatabaseCmd().do)

        # an Article needs a Container that needs a Collection that needs a Provider
        # cmd0 = addProviderDatabaseCmd( { "name":"numdam", "pid_type":"mathdoc-id" } )
        # p = cmd0.do()
        p = model_helpers.get_provider("mathdoc-id")

        xcol = create_publicationdata()
        xcol.coltype = "journal"
        xcol.pid = "AFST"
        xcol.title_tex = "Mon Journal"
        cmd1 = addCollectionDatabaseCmd({"xobj": xcol})
        cmd1.set_provider(p)
        s = cmd1.do()

        xissue = create_issuedata()
        xissue.ctype = "issue"
        xissue.pid = "ASENS_2013_4_46_6"
        xissue.year = 2013
        xissue.vseries = 4
        xissue.volume = 46
        xissue.number = 6
        xissue.last_modified_iso_8601_date_str = "2012-06-01T00:00:00"
        cmd2 = addContainerDatabaseCmd({"xobj": xissue})
        cmd2.add_collection(s)
        cmd2.set_provider(p)
        container = cmd2.do()

        contributor = create_contributor()
        contributor["role"] = "author"
        contributor["orcid"] = "1234"
        contributor["first_name"] = "Robert"
        contributor["last_name"] = "Cori"

        xarticle = create_articledata()
        xarticle.pid = "ASENS_2013_4_46_6_857_0"
        xarticle.fpage = 857
        xarticle.lpage = 878
        xarticle.seq = 1
        xarticle.title_tex = "Mon article"
        xarticle.contributors.append(contributor)
        cmd = addArticleDatabaseCmd({"xobj": xarticle})

        # cmd.container is not set
        self.assertRaises(ValueError, cmd.do)

        cmd.set_container(container)
        article = cmd.do()

        self.assertEqual(Article.objects.count(), 1)
        self.assertEqual(article.pid, "ASENS_2013_4_46_6_857_0")
        self.assertIsNone(article.sid)
        self.assertEqual(article.my_container, container)
        self.assertEqual(article.provider, p)

        self.assertEqual(Contribution.objects.count(), 1)
        author = Contribution.objects.first()
        self.assertEqual(author.orcid, "1234")
        self.assertEqual(Author.objects.count(), 1)
        author = Author.objects.first()
        self.assertEqual(author.count, 1)

        xarticle = create_articledata()
        xarticle.pid = "ASENS_2013_4_46_6_707_0"
        xarticle.fpage = 707
        xarticle.lpage = 877
        xarticle.seq = 2
        xarticle.title_tex = "My best article"
        xarticle.doi = "doi1"
        xarticle.ids = [("doi", "doi1"), ("eid", "e1")]
        xarticle.contributors.append(contributor)
        cmd1 = addArticleDatabaseCmd({"xobj": xarticle})
        cmd1.set_container(container)
        article1 = cmd1.do()

        self.assertEqual(Contribution.objects.count(), 2)
        self.assertEqual(Author.objects.count(), 1)
        author = Author.objects.first()
        self.assertEqual(author.count, 2)

        id_ = article1.get_id_value("doi")
        self.assertEqual(id_, "doi1")
        id_ = article1.get_id_value("foo")
        self.assertIsNone(id_)
        href = article1.get_doi_href()
        self.assertEqual(href, "https://doi.org/doi1")

        cmd1.undo()

        self.assertEqual(Contribution.objects.count(), 1)
        self.assertEqual(Author.objects.count(), 1)
        author = Author.objects.first()
        self.assertEqual(author.count, 1)

        cmd.undo()
        self.assertRaises(
            Resource.DoesNotExist,
            Article.objects.get,
            pid="ASENS_2013_4_46_6_857_0",
            provider=p,
            my_container=container,
        )
        self.assertEqual(Article.objects.count(), 0)

        self.assertEqual(Author.objects.count(), 0)
        self.assertEqual(Contribution.objects.count(), 0)

        # Attempting to remove a 2nd time should raise
        # exceptions.ResourceDoesNotExist
        self.assertRaises(exceptions.ResourceDoesNotExist, cmd.undo)

    def test_07_delete(self):
        print("** Test: test_database delete")

        p = model_helpers.get_provider("mathdoc-id")

        xcol = create_publicationdata()
        xcol.coltype = "journal"
        xcol.pid = "AFST"
        xcol.title_tex = "Mon Journal"
        cmd1 = addCollectionDatabaseCmd({"xobj": xcol})
        cmd1.set_provider(p)
        s = cmd1.do()

        xissue = create_issuedata()
        xissue.ctype = "issue"
        xissue.pid = "ASENS_2013_4_46_6"
        xissue.year = 2013
        xissue.vseries = 4
        xissue.volume = 46
        xissue.number = 6
        xissue.last_modified_iso_8601_date_str = "2012-06-01T00:00:00"
        cmd2 = addContainerDatabaseCmd({"xobj": xissue})
        cmd2.add_collection(s)
        cmd2.set_provider(p)
        container = cmd2.do()

        xarticle = create_articledata()
        xarticle.pid = "ASENS_2013_4_46_6_857_0"
        xarticle.fpage = 857
        xarticle.lpage = 878
        xarticle.seq = 1
        xarticle.title_tex = "Mon article"
        cmd = addArticleDatabaseCmd({"xobj": xarticle})
        cmd.set_container(container)
        cmd.do()

        cmd1.undo()
        # Deleting a collection deletes all its articles/containers
        self.assertEqual(Collection.objects.count(), 0)
        self.assertEqual(Container.objects.count(), 0)
        self.assertEqual(Article.objects.count(), 0)

        # Adding back the journal/container/article
        s = cmd1.do()

        xissue = create_issuedata()
        xissue.ctype = "issue"
        xissue.pid = "ASENS_2013_4_46_6"
        xissue.year = 2013
        xissue.vseries = 4
        xissue.volume = 46
        xissue.number = 6
        xissue.last_modified_iso_8601_date_str = "2012-06-01T00:00:00"
        cmd2 = addContainerDatabaseCmd({"xobj": xissue})
        cmd2.add_collection(s)
        cmd2.set_provider(p)
        container = cmd2.do()
        cmd.set_container(container)
        cmd.do()

        self.assertEqual(Collection.objects.count(), 1)
        self.assertEqual(Container.objects.count(), 1)
        self.assertEqual(Article.objects.count(), 1)

        cmd2.undo()
        # Deleting a collection deletes all its articles
        self.assertEqual(Collection.objects.count(), 1)
        self.assertEqual(Container.objects.count(), 0)
        self.assertEqual(Article.objects.count(), 0)

        # Attempting to remove a 2nd time should raise
        # exceptions.ResourceDoesNotExist
        self.assertRaises(exceptions.ResourceDoesNotExist, cmd2.undo)

        cmd1.undo()

    def test_08_update(self):
        print("** Test: test_database update")
        p = model_helpers.get_provider("mathdoc-id")

        xcol = create_publicationdata()
        xcol.coltype = "journal"
        xcol.pid = "AFST"
        xcol.title_tex = "Mon Journal"
        cmd0 = addCollectionDatabaseCmd({"xobj": xcol})
        cmd0.set_provider(p)
        s = cmd0.do()

        xissue = create_issuedata()
        xissue.ctype = "issue"
        xissue.pid = "ASENS_2013_4_46_6"
        xissue.year = 2013
        xissue.vseries = 4
        xissue.volume = 46
        xissue.number = 6
        xissue.last_modified_iso_8601_date_str = "2012-06-01T00:00:00"
        cmd1 = addContainerDatabaseCmd({"xobj": xissue})
        cmd1.add_collection(s)
        cmd1.set_provider(p)
        container = cmd1.do()

        xarticle = create_articledata()
        xarticle.pid = "ASENS_2013_4_46_6_857_0"
        xarticle.fpage = 857
        xarticle.lpage = 878
        xarticle.seq = 1
        xarticle.title_tex = "Mon article"
        cmd2 = addArticleDatabaseCmd({"xobj": xarticle})
        cmd2.set_container(container)
        cmd2.do()

        the_collection_id = s.id

        xcol = create_publicationdata()
        xcol.coltype = "journal"
        xcol.pid = "AFST"
        xcol.title_tex = "Mon Journal2"
        cmd = updateCollectionDatabaseCmd({"xobj": xcol})

        # cmd.provider is not set
        self.assertRaises(ValueError, cmd.do)

        cmd.set_provider(p)
        new_collection = cmd.do()

        self.assertEqual(the_collection_id, the_collection_id)
        self.assertEqual(Collection.objects.count(), 1)
        self.assertEqual(Container.objects.count(), 1)
        self.assertEqual(Article.objects.count(), 1)

        self.assertEqual(new_collection.pid, "AFST")
        self.assertEqual(new_collection.title_tex, "Mon Journal2")

    def test_09_book(self):
        print("** Test: test_database book")
        self.assertRaises(ValueError, addContainerDatabaseCmd().do)

        p = model_helpers.get_provider("mathdoc-id")

        xcol = create_publicationdata()
        xcol.coltype = "book-series"
        xcol.pid = "MyCol"
        xcol.title_tex = "MyCol"
        cmd0 = addCollectionDatabaseCmd({"xobj": xcol})
        cmd0.set_provider(p)
        collection = cmd0.do()

        xbook = create_bookdata()
        xbook.ctype = "book"
        xbook.pid = "MyBook"
        xbook.year = 2013
        xbook.volume = 1
        xbook.vseries = 0
        xbook.number = 3
        xbook.last_modified_iso_8601_date_str = "2012-06-01T00:00:00"
        text = "This is an abstract"
        xbook.abstracts = [
            {
                "tag": "trans-abstract",
                "lang": "fr",
                "value_html": text,
                "value_xml": text,
                "value_tex": text,
            }
        ]
        cmd = addContainerDatabaseCmd({"xobj": xbook})
        cmd.add_collection(collection)
        cmd.set_provider(p)
        book = cmd.do()

        self.assertEqual(Container.objects.count(), 1)
        self.assertEqual(book.pid, "MyBook")
        self.assertIsNone(book.sid)
        self.assertEqual(book.my_collection, collection)
        self.assertEqual(book.provider, p)

        self.assertEqual(Abstract.objects.count(), 1)
        self.assertEqual(book.abstract_set.count(), 1)
        self.assertEqual(book.abstract_set.first().value_html, text)

        cmd.undo()
        self.assertRaises(Resource.DoesNotExist, Container.objects.get, pid="MyBook", provider=p)
        self.assertEqual(Abstract.objects.count(), 0)

        # Attempting to remove a 2nd time should raise
        # exceptions.ResourceDoesNotExist
        self.assertRaises(exceptions.ResourceDoesNotExist, cmd.undo)

        cmd0.undo()

        self.assertEqual(Collection.objects.count(), 0)
        self.assertEqual(Container.objects.count(), 0)
        self.assertEqual(Article.objects.count(), 0)

    def test_10_multiple_collection(self):
        print("** Test: test_database multiple collection")

        # a Collection needs a Provider
        # cmd0 = addProviderDatabaseCmd( { "name":"numdam", "pid_type":"mathdoc-id" } )
        # p = cmd0.do()
        p = model_helpers.get_provider("mathdoc-id")

        xcol = create_publicationdata()
        xcol.coltype = "book-series"
        xcol.pid = "AST"
        xcol.title_tex = "Asterisque"
        cmd1 = addCollectionDatabaseCmd({"xobj": xcol})
        cmd1.set_provider(p)
        col1 = cmd1.do()

        xcol.pid = "PB"
        xcol.title_tex = "Bourbaki"
        cmd2 = addCollectionDatabaseCmd({"xobj": xcol})
        cmd2.set_provider(p)
        col2 = cmd2.do()

        xbook = create_bookdata()
        xbook.ctype = "book-edited-book"
        xbook.pid = "AST_2013_4_46_6"
        xbook.year = 2013
        xbook.last_modified_iso_8601_date_str = "2012-06-01T00:00:00"
        incol = create_collectiondata()
        incol.pid = "AST"
        incol.seq = 2
        incol.volume = 23
        xbook.incollection.append(incol)
        incol = create_collectiondata()
        incol.pid = "PB"
        incol.seq = 1
        incol.volume = 5
        xbook.incollection.append(incol)

        cmd = addContainerDatabaseCmd({"xobj": xbook})
        cmd.set_provider(p)
        cmd.add_collection(col1)
        container = cmd.do()

        self.assertEqual(Collection.objects.count(), 2)
        self.assertEqual(Container.objects.count(), 1)
        self.assertEqual(container.my_collection, col1)
        self.assertEqual(container.provider, p)

        other_collections = container.my_other_collections
        self.assertEqual(other_collections.count(), 1)
        self.assertEqual(CollectionMembership.objects.count(), 1)
        self.assertEqual(other_collections.first(), col2)

        col_membership = container.collectionmembership_set.first()
        self.assertEqual(col_membership.collection, col2)
        self.assertEqual(col_membership.container, container)
        self.assertEqual(col_membership.number, "5")
        self.assertEqual(col_membership.number_int, 5)
        self.assertEqual(col_membership.seq, 1)

        col_membership = col2.collectionmembership_set.first()
        self.assertEqual(col_membership.collection, col2)
        self.assertEqual(col_membership.container, container)
        self.assertEqual(col_membership.number, "5")
        self.assertEqual(col_membership.number_int, 5)
        self.assertEqual(col_membership.seq, 1)

        cmd.undo()
        cmd2.undo()
        cmd1.undo()

        self.assertEqual(Collection.objects.count(), 0)
        self.assertEqual(Container.objects.count(), 0)
        self.assertEqual(CollectionMembership.objects.count(), 0)
