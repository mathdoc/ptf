from lxml import etree

from django.conf import settings
from django.test import TestCase

from ptf import model_data_comparator
from ptf import model_helpers
from ptf.cmds import database_cmds
from ptf.cmds import solr_cmds
from ptf.cmds import xml_cmds
from ptf.cmds.xml.jats import jats_parser
from ptf.model_data_comparator import IssueDataComparator
from ptf.model_data_comparator import prepare_issue_for_comparison
from ptf.model_data_converter import db_to_issue_data
from ptf.model_data_converter import db_to_journal_data
from ptf.models import Abstract
from ptf.models import Article
from ptf.models import BibItem
from ptf.models import Collection
from ptf.models import Container
from ptf.models import Contribution


# Unused anymore. see model_data_comparator.py
def deep_compare(a, b):
    if isinstance(a, jats_parser.JatsJournal):
        # xml_cmds does not use the jats_parser collection but retrieve it from the database
        collection = model_helpers.get_collection(a.pid)
        a = db_to_journal_data(collection)
    if hasattr(a, "__dict__"):
        a = a.__dict__
    if hasattr(b, "__dict__"):
        b = b.__dict__
    if type(a) != type(b):
        return False
    if type(a) != list and type(a) != dict:
        if type(a) == str and type(b) == str:
            try:
                a_date = model_helpers.parse_date_str(a)
                b_date = model_helpers.parse_date_str(b)
                a = a_date
                b = b_date
            except:
                pass
        return a == b
    if len(a) != len(b):
        return False
    if type(a) == list:
        for a_, b_ in zip(a, b):
            if not deep_compare(a_, b_):
                return False
    if type(a) == dict:
        for a_key, a_value in a.items():
            if a_key not in b:
                return False
            b_value = b[a_key]
            if not deep_compare(a_value, b_value):
                return False

    return True


class XmlCmdTest(TestCase):
    def get_solr(self):
        return solr_cmds.solrFactory.get_solr()

    def setUp(self):
        solr_cmds.solrFactory.solr_url = settings.SOLR_URL
        self.get_solr().delete(q="*:*")

    def tearDown(self):
        self.get_solr().delete(q="*:*")

    def solrSearch(self, searchStr):
        return self.get_solr().search(searchStr)

    def check_empty_database(self):
        self.assertEqual(Collection.objects.count(), 0)
        self.assertEqual(Container.objects.count(), 0)
        self.assertEqual(Article.objects.count(), 0)
        self.assertEqual(BibItem.objects.count(), 0)
        self.assertEqual(Abstract.objects.count(), 0)
        self.assertEqual(Contribution.objects.count(), 0)

    def test_01_issue(self):
        print("** test_01_issue")

        self.check_empty_database()

        f = open("tests/data/xml/journal-sms.xml")
        body = f.read()
        f.close()

        cmd1 = xml_cmds.addCollectionsXmlCmd({"body": body})
        cmd1.do()

        f = open("tests/data/xml/issue.xml")
        body = f.read()
        f.close()

        cmd = xml_cmds.addIssueXmlCmd({"body": body})
        issue = cmd.do()
        data_issue = db_to_issue_data(issue)

        # The above function deept_compare is not used anymore
        # data_issue_dict = data_issue.__dict__

        parser = etree.XMLParser(
            huge_tree=True, recover=True, remove_blank_text=False, remove_comments=True
        )
        tree = etree.fromstring(body.encode("utf-8"), parser=parser)
        xissue = jats_parser.JatsIssue(tree=tree)

        model_data_comparator.TEST_STRICT = True

        # Handle xml cmds side effects (ex: "numdam" changed into "mathdoc", ...)
        prepare_issue_for_comparison(xissue)

        issue_comparator = IssueDataComparator()
        issues_diff = {}
        result = issue_comparator.compare(data_issue, xissue, issues_diff)

        self.assertEqual(result, True)

        # Change the Django DB
        article = issue.article_set.first()
        article.coi_statement = "Article bidon"
        article.date_received = model_helpers.parse_date_str("2001-04-01")
        article.save()

        data_issue = db_to_issue_data(issue)
        issues_diff = {}
        result = issue_comparator.compare(data_issue, xissue, issues_diff)

        self.assertEqual(result, False)
        self.assertEqual("articles" in issues_diff, True)
        self.assertEqual(len(issues_diff["articles"]), 1)
        self.assertEqual("coi_statement" in issues_diff["articles"][0]["diff"], True)
        self.assertEqual("received" in issues_diff["articles"][0]["diff"], True)

        # Mimic the matching
        bibitem = article.bibitem_set.first()
        subcmd = database_cmds.addBibItemIdDatabaseCmd(
            {
                "id_type": "zbl-item-id",
                "id_value": "newid",
                "checked": True,
                "false_positive": False,
            }
        )
        subcmd.set_bibitem(bibitem)
        subcmd.do()

        data_issue = db_to_issue_data(issue)
        issues_diff = {}
        result = issue_comparator.compare(data_issue, xissue, issues_diff)

        self.assertEqual(result, False)
        self.assertEqual("articles" in issues_diff, True)
        self.assertEqual(len(issues_diff["articles"]), 1)
        self.assertEqual("coi_statement" in issues_diff["articles"][0]["diff"], True)
        self.assertEqual("bibitems" in issues_diff["articles"][0]["diff"], True)
        bibitems = issues_diff["articles"][0]["diff"]["bibitems"]
        self.assertEqual(len(bibitems), 1)
        self.assertEqual("extids" in bibitems[0]["diff"], True)
        diff_extids = bibitems[0]["diff"]["extids"][0]
        self.assertEqual(diff_extids, ["len", 3, 2])

        self.assertEqual(Container.objects.count(), 1)

        cmd.undo()
        cmd1.undo()
        # cmd0.undo()

        self.check_empty_database()
