import requests

from django.conf import settings
from django.test import TestCase

from ptf import model_helpers
from ptf.cmds import solr_cmds
from ptf.cmds import xml_cmds
from ptf.models import Abstract
from ptf.models import Article
from ptf.models import BibItem
from ptf.models import Collection
from ptf.models import Container
from ptf.models import Contrib
from ptf.models import ContribGroup
from ptf.models import Kwd
from ptf.models import KwdGroup
from ptf.models import PtfSite
from ptf.models import Publisher


class XmlCmdTestCase(TestCase):
    def get_solr(self):
        return solr_cmds.solrFactory.get_solr()

    def setUp(self):
        solr_cmds.solrFactory.solr_url = settings.SOLR_URL
        self.get_solr().delete(q="*:*")

    def tearDown(self):
        self.get_solr().delete(q="*:*")

    def solrSearch(self, searchStr):
        return self.get_solr().search(searchStr)

    def check_empty_database(self):
        self.assertEqual(Collection.objects.count(), 0)
        self.assertEqual(Container.objects.count(), 0)
        self.assertEqual(Article.objects.count(), 0)
        self.assertEqual(BibItem.objects.count(), 0)
        self.assertEqual(Abstract.objects.count(), 0)
        self.assertEqual(ContribGroup.objects.count(), 0)
        self.assertEqual(Contrib.objects.count(), 0)
        self.assertEqual(KwdGroup.objects.count(), 0)
        self.assertEqual(Kwd.objects.count(), 0)
        self.assertEqual(Publisher.objects.count(), 0)

    def check_resource(self, resource, data, is_book=False):
        self.assertEqual(resource.title_tex, data["title_tex"])
        self.assertEqual(resource.title_html, data["title_html"])
        self.assertEqual(resource.doi or "", data["doi"])

        if not is_book:
            date_accepted = date_published = ""
            if resource.date_accepted:
                date_accepted = resource.date_accepted.strftime("%Y-%m-%d")
            if resource.date_published:
                date_published = resource.date_published.strftime("%Y-%m-%d")
            self.assertEqual(date_accepted, data["date_accepted"])
            self.assertEqual(date_published, data["date_published"])

        extids = []
        for extid in resource.extid_set.all():
            extids.append([extid.id_type, extid.id_value])
        self.assertEqual(extids, data["extids"])

        kwd_gps = []
        for kwd_gp in resource.kwdgroup_set.all():
            group = {
                "content_type": kwd_gp.content_type,
                "lang": kwd_gp.lang,
                "value_html": kwd_gp.value_html or "",
            }
            kwds = []
            for kwd in kwd_gp.kwd_set.all():
                kwds.append(kwd.value)
            group["kwds"] = kwds

            kwd_gps.append(group)
        self.assertEqual(kwd_gps, data["kwd_gps"])

        if not is_book:
            awards = []
            for award in resource.award_set.all():
                awards.append([award.abbrev, award.award_id])
            self.assertEqual(awards, data["awards"])

        for abstract1, abstract2 in zip(resource.abstract_set.all(), data["abstracts"]):
            self.assertEqual(abstract1.lang, abstract2["lang"])
            # TODO: in xmldata, value_tex is a simple text without HTML tags.
            # Remove the post processing of abstract
            value = abstract1.value_tex.replace("<strong>", "").replace("</strong>", "")
            self.assertEqual(value, abstract2["value_tex"])
            self.assertEqual(abstract1.value_html, abstract2["value_html"])

        citation = resource.get_citation()
        data_citation = data["citation"]

        # TODO: Remove the test. On 02/05/2020, JEP is not consistent in production.
        # The "How to cite" string does not follow the same naming convention (First then Last name)
        # As the other displays (references, Article's author names
        if settings.SITE_NAME == "jep":
            authors = resource.get_authors()
            authors = "; ".join([author.string_name for author in authors])
            citation = citation[len(authors) + 2 :]
            pos = data["citation"].find(citation[0:10])
            data_citation = data["citation"][pos:]

        if citation != data_citation:
            print(resource.pid)
            print(citation)
            print(data["citation"])
        self.assertEqual(citation, data_citation)

        for bibitem, y in zip(resource.bibitem_set.all(), data["bibitems"]):
            x = bibitem.citation_html
            x = x.replace("\n", "")
            while x.find("  ") > 0:
                x = x.replace("  ", " ")
            y = y.replace("\n", "")
            while y.find("  ") > 0:
                y = y.replace("  ", " ")
            if x != y:
                print(resource.pid)
                print(x)
                print(y)
            self.assertEqual(x, y)

    def test_01_jep(self):
        print("** test_01_jep")

        self.check_empty_database()

        jep_site = PtfSite(id=8, domain="jep.centre-mersenne.org", name="jep", acro="jep")
        jep_site.save()

        temp_settingsSITE_ID = settings.SITE_ID
        temp_settingsSITE_NAME = settings.SITE_NAME
        temp_settingsTIME_ZONE = settings.TIME_ZONE
        temp_settingsUSE_TZ = settings.USE_TZ

        settings.SITE_ID = "8"
        settings.SITE_NAME = "jep"

        settings.DISPLAY_FIRST_NAME_FIRST = True
        settings.REF_JEP_STYLE = True

        settings.TIME_ZONE = "UTC"
        settings.USE_TZ = True

        f = open("tests/data/journal-jep.xml")
        body = f.read()
        f.close()

        cmd1 = xml_cmds.addCollectionsXmlCmd({"body": body})
        cmd1.do()

        site = settings.SITE_REGISTER["jep"]["site_domain"]

        url = "https://" + site + "/api-issues/JEP/"
        response = requests.get(url)
        self.assertEqual(response.status_code, 200)
        issues = response.json()["issues"]

        for issue_pid in issues:
            print("    " + issue_pid)
            url = "https://" + site + "/api-item-xml/" + issue_pid
            response = requests.get(url)
            self.assertEqual(response.status_code, 200)
            body = response.content.decode("utf-8")

            cmd = xml_cmds.addIssueXmlCmd({"body": body})
            cmd.do()

            url = "https://" + site + "/api-issue-list/" + issue_pid
            response = requests.get(url)
            self.assertEqual(response.status_code, 200)
            articles = response.json()["articles"]

            for article_pid in articles:
                print("        " + article_pid)
                url = "https://" + site + "/api-article-dump/" + article_pid
                response = requests.get(url)
                self.assertEqual(response.status_code, 200)
                data = response.json()

                article = model_helpers.get_article(article_pid)
                self.assertIsNotNone(article)

                self.check_resource(article, data)

            cmd.undo()

        cmd1.undo()

        settings.SITE_NAME = temp_settingsSITE_NAME
        settings.SITE_ID = temp_settingsSITE_ID
        settings.TIME_ZONE = temp_settingsTIME_ZONE
        settings.USE_TZ = temp_settingsUSE_TZ

        self.check_empty_database()

    def test_02_book(self):
        print("** test_02_book")

        self.check_empty_database()

        settings.DISPLAY_FIRST_NAME_FIRST = False
        settings.REF_JEP_STYLE = False

        f = open("tests/data/collection-msmf.xml")
        body = f.read()
        f.close()

        cmd1 = xml_cmds.addCollectionsXmlCmd({"body": body})
        cmd1.do()

        site = "http://numdam-pre.u-ga.fr"

        book_pid = "MSMF_2014_2_138-139__1_0"

        url = site + "/api-item-xml/" + book_pid
        response = requests.get(url)
        self.assertEqual(response.status_code, 200)
        body = response.content.decode("utf-8")

        cmd = xml_cmds.addBookXmlCmd({"body": body})
        book = cmd.do()

        print("        " + book_pid)
        url = site + "/api-book-dump/" + book_pid
        response = requests.get(url)
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.check_resource(book, data, is_book=True)

        cmd.undo()
        cmd1.undo()

        self.check_empty_database()

    def test_03_aif(self):
        print("** test_03_aif")

        self.check_empty_database()

        aif_site = PtfSite(id=1, domain="aif.centre-mersenne.org", name="aif", acro="aif")
        aif_site.save()

        temp_settingsSITE_ID = settings.SITE_ID
        temp_settingsSITE_NAME = settings.SITE_NAME
        temp_settingsTIME_ZONE = settings.TIME_ZONE
        temp_settingsUSE_TZ = settings.USE_TZ

        settings.SITE_ID = "1"
        settings.SITE_NAME = "aif"

        settings.DISPLAY_FIRST_NAME_FIRST = False
        settings.REF_JEP_STYLE = False

        settings.TIME_ZONE = "UTC"
        settings.USE_TZ = True

        f = open("tests/data/collection-aif.xml")
        body = f.read()
        f.close()

        cmd1 = xml_cmds.addCollectionsXmlCmd({"body": body})
        cmd1.do()

        site = settings.SITE_REGISTER["aif"]["site_domain"]

        url = "https://" + site + "/api-issues/AIF"
        response = requests.get(url)
        self.assertEqual(response.status_code, 200)
        issues = response.json()["issues"]

        for issue_pid in issues:
            if issue_pid != "AIF_0__0_0":
                print("    " + issue_pid)
                url = "https://" + site + "/api-item-xml/" + issue_pid
                response = requests.get(url)
                self.assertEqual(response.status_code, 200)
                body = response.content.decode("utf-8")

                cmd = xml_cmds.addIssueXmlCmd({"body": body})
                cmd.do()

                url = "https://" + site + "/api-issue-list/" + issue_pid
                response = requests.get(url)
                self.assertEqual(response.status_code, 200)
                articles = response.json()["articles"]

                for article_pid in articles:
                    print("        " + article_pid)
                    url = "https://" + site + "/api-article-dump/" + article_pid
                    response = requests.get(url)
                    self.assertEqual(response.status_code, 200)
                    data = response.json()
                    # Ignore some articles
                    #  AIF_2002__52_6_1681_0. Only 1 ref has square braquets which causes problems the sort order.
                    #  AIF_2005__55_2_673_0: a ref has a comment that start with a space
                    #  AIF_2005__55_4_1195_0: a ref has a comment that start with a space
                    #  AIF_2008__58_2_689_0: a ref has a comment that start with a space
                    #  AIF_2008__58_5_1733_0 has an abstract with a list <ul> (removed in prod)
                    #  AIF_2009__59_3_977_0 has an abstract with a list <ul> (removed in prod)
                    #  AIF_2010__60_5_1787_0 has [5] <surname>Tschinkel, Yuri; Zarhin, Yuri G.</surname>
                    #  AIF_2012__62_5_1627_0 has a ref with "http ://mahery.math.u-psud.fr/frances" => No link
                    #  AIF_2014__64_6_2527_0 has an abstract with a list <ul> (removed in prod)
                    #  AIF_2019__69_1_303_0 has an abstract with a list <ul> (removed in prod)
                    #  AIF_2019__69_5_2291_0 says [19] 175014 pages in prod !!
                    if (
                        article_pid != "AIF_2002__52_6_1681_0"
                        and article_pid != "AIF_2005__55_2_673_0"
                        and article_pid != "AIF_2005__55_4_1195_0"
                        and article_pid != "AIF_2008__58_2_689_0"
                        and article_pid != "AIF_2008__58_5_1733_0"
                        and article_pid != "AIF_2009__59_3_977_0"
                        and article_pid != "AIF_2010__60_5_1787_0"
                        and article_pid != "AIF_2012__62_5_1627_0"
                        and article_pid != "AIF_2014__64_6_2527_0"
                        and article_pid != "AIF_2019__69_1_303_0"
                        and article_pid != "AIF_2019__69_5_2291_0"
                    ):
                        print("        " + article_pid)
                        url = "https://" + site + "/api-article-dump/" + article_pid
                        response = requests.get(url)
                        self.assertEqual(response.status_code, 200)
                        data = response.json()

                        article = model_helpers.get_article(article_pid)
                        self.assertIsNotNone(article)

                        self.check_resource(article, data)

                cmd.undo()

        cmd1.undo()

        settings.SITE_NAME = temp_settingsSITE_NAME
        settings.SITE_ID = temp_settingsSITE_ID
        settings.TIME_ZONE = temp_settingsTIME_ZONE
        settings.USE_TZ = temp_settingsUSE_TZ

        self.check_empty_database()
