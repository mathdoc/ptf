from django.conf import settings
from django.test import TestCase
from django.test import override_settings
from django.test.client import RequestFactory

from ptf.cmds import solr_cmds
from ptf.cmds import xml_cmds
from ptf.models import BibItem
from ptf.models import Collection


class CollectionTestCase(TestCase):
    def get_solr(self):
        return solr_cmds.solrFactory.get_solr()

    def setUp(self):
        solr_cmds.solrFactory.solr_url = settings.SOLR_URL
        self.get_solr().delete(q="*:*")
        self.factory = RequestFactory()

    def tearDown(self):
        self.get_solr().delete(q="*:*")

    def solrSearch(self, searchStr):
        return self.get_solr().search(searchStr)

    @override_settings(
        # TODO: add a testcase with USE_META_COLLECTIONS=False
        USE_META_COLLECTIONS=True
    )
    def test_01_issue(self):
        self.assertRaises(ValueError, xml_cmds.addCollectionsXmlCmd().do)

        # journal with issn 0241-2963
        f = open("tests/data/xml/journal-afst.xml")
        body = f.read()
        f.close()
        cmd1 = xml_cmds.addCollectionsXmlCmd({"body": body})
        top_collection = cmd1.do()[0]

        self.assertEqual(len(list(top_collection.bloodline())), 0)
        self.assertEqual(len(list(top_collection.preceding_journal())), 0)

        # issue with journal-meta / issn 0240-2963  => Should create a child collection on the fly
        f = open("tests/data/xml/issue-afst.xml")
        body = f.read()
        f.close()
        cmd = xml_cmds.addIssueXmlCmd({"body": body})
        _ = cmd.do()

        self.assertEqual(Collection.objects.count(), 2)
        c = Collection.objects.get(pid="AFST-0240-2963")
        self.assertEqual(c.parent.pid, "AFST")

        bloodline = list(c.bloodline())
        self.assertEqual(len(bloodline), 2)
        self.assertEqual(bloodline[0], top_collection)
        self.assertEqual(bloodline[1], c)
        self.assertEqual(c.preceding_journal(), top_collection)

        self.assertEqual(len(list(top_collection.bloodline())), 2)
        self.assertEqual(len(list(top_collection.preceding_journal())), 0)

        cmd.undo()
        # The issue has been removed. The "AFST-0240-2963" collection should have been removed as well
        self.assertEqual(Collection.objects.count(), 1)
        col = Collection.objects.first()
        self.assertEqual(col.pid, "AFST")

        self.assertEqual(BibItem.objects.count(), 0)
