from django.conf import settings
from django.test import TestCase
from django.utils import translation

from ptf.cmds.database_cmds import addArticleDatabaseCmd
from ptf.cmds.database_cmds import addCollectionDatabaseCmd
from ptf.cmds.database_cmds import addContainerDatabaseCmd
from ptf.cmds.solr_cmds import addArticleSolrCmd
from ptf.cmds.solr_cmds import addContainerSolrCmd
from ptf.cmds.solr_cmds import solrFactory
from ptf.cmds.solr_cmds import solrGetDocumentByPidCmd
from ptf.cmds.solr_cmds import solrInternalSearchCmd
from ptf.cmds.solr_cmds import solrSearchCmd
from ptf.cmds.xml_cmds import addBookXmlCmd
from ptf.cmds.xml_cmds import addCollectionsXmlCmd
from ptf.cmds.xml_cmds import addIssueXmlCmd
from ptf.model_data import create_articledata
from ptf.model_data import create_bookdata
from ptf.model_data import create_issuedata
from ptf.model_data import create_publicationdata
from ptf.model_helpers import get_provider
from ptf.models import Article
from ptf.models import Collection
from ptf.models import Container
from ptf.models import PtfSite


class SolrTestCase(TestCase):
    def get_solr(self):
        return solrFactory.get_solr()

    def solrSearch(self, searchStr):
        return self.get_solr().search(searchStr)

    def setUp(self):
        solrFactory.solr_url = settings.SOLR_URL
        self.get_solr().delete(q="*:*")

    def tearDown(self):
        self.get_solr().delete(q="*:*")

    # remove solr test_03 because we never record Issue in Solr / see book for container level
    # def test_03_container(self):
    #     print("** test_03_container")
    #     self.assertRaises(ValueError, addContainerSolrCmd(None).do)
    #
    #     # a Collection needs a Provider
    #     # cmd0 = addProviderDatabaseCmd( { "name":"numdam", "pid_type":"mathdoc-id" } )
    #     # p = cmd0.do()
    #     p = get_provider("mathdoc-id")
    #
    #     cmd1 = addCollectionDatabaseCmd(
    #         {"coltype": "journal", "pid": "AFST", "title_xml": 'Mon Journal', "title_tex": "Mon Journal"})
    #     cmd1.set_provider(p)
    #     s = cmd1.do()
    #
    #     cmd1a = addPublisherDatabaseCmd({"name": 'Institut Fourier'})
    #     publisher = cmd1a.do()
    #
    #     cmd2 = addContainerDatabaseCmd(
    #         {"pid": "ASENS_2013_4_46_6", "ctype": "issue", "year": 2013, "vseries": 4, "volume": 46, "number": 6, "last_modified_iso_8601_date_str": "2012-06-01T00:00:00"})
    #     cmd2.add_collection(s)
    #     cmd2.set_provider(p)
    #     container = cmd2.do()
    #
    #     cmd = addContainerSolrCmd({"id": container.id,
    #                                "pid": container.pid,
    #                                "ctype": 'issue',
    #                                "collection_id": s.id,
    #                                "publisher_id": publisher.id,
    #                                'year': "1969-1970",
    #                                'volume': '  46',
    #                                'vseries': '4 ',
    #                                'number': ' 6 ',
    #                                'lang': 'de',
    #                                'last_modified_iso_8601_date_str': "2012-06-01T00:00:00",
    #                                'kwd_groups': [{'content_type': 'msc', 'kwds': ['32M25', '14r25']}]
    #                                })
    #
    #     cmd.do()
    #
    #     # result = self.solrSearch('*:*' )
    #     result = self.solrSearch('id:{}'.format(container.id))
    #
    #     self.assertEqual(result.hits, 1)
    #     self.assertEqual(result.docs[0]['classname'], 'Issue')
    #     self.assertEqual(result.docs[0]['year'], '1969-1970')
    #     self.assertEqual(result.docs[0]['publisher_id'], publisher.id)
    #
    #     # volume/vseries/number should be converted to int
    #     self.assertNotEqual(result.docs[0]['volume'], '46')
    #     self.assertEqual(result.docs[0]['volume'], 46)
    #     self.assertNotEqual(result.docs[0]['vseries'], '4')
    #     self.assertEqual(result.docs[0]['vseries'], 4)
    #     self.assertNotEqual(result.docs[0]['number'], '6')
    #     self.assertEqual(result.docs[0]['number'], 6)
    #
    #     self.assertIsNotNone(result.docs[0]['msc'])
    #     msc_codes = result.docs[0]['msc']
    #     self.assertEqual(len(msc_codes), 2)
    #     self.assertEqual(msc_codes[0], '32M25')
    #     # Msc codes are converted in uppercase in SolR
    #     self.assertEqual(msc_codes[1], '14R25')
    #
    #     cmd.undo()
    #
    #     result = self.solrSearch('id:{}'.format(container.id))
    #     self.assertEqual(result.hits, 0)
    #
    #     cmd2.undo()
    #     cmd1a.undo()
    #     cmd1.undo()
    #     # cmd0.undo()
    #
    #     self.assertEqual(Collection.objects.count(), 0)
    #     self.assertEqual(Container.objects.count(), 0)
    #     self.assertEqual(Article.objects.count(), 0)
    #     self.assertEqual(Publisher.objects.count(), 0)

    def test_04_article(self):
        print("** test_04_article")
        self.assertRaises(ValueError, addArticleSolrCmd(None).do)

        # a Collection needs a Provider
        # cmd0 = addProviderDatabaseCmd( { "name":"numdam", "pid_type":"mathdoc-id" } )
        # p = cmd0.do()
        p = get_provider("mathdoc-id")

        xcol = create_publicationdata()
        xcol.coltype = "journal"
        xcol.pid = "AFST"
        xcol.title_tex = "Mon Journal"
        cmd1 = addCollectionDatabaseCmd({"xobj": xcol})
        cmd1.set_provider(p)
        s = cmd1.do()

        xissue = create_issuedata()
        xissue.ctype = "issue"
        xissue.pid = "ASENS_2013_4_46_6"
        xissue.year = 2013
        xissue.vseries = 4
        xissue.volume = 46
        xissue.number = 6
        xissue.last_modified_iso_8601_date_str = "2012-06-01T00:00:00"
        cmd2 = addContainerDatabaseCmd({"xobj": xissue})
        cmd2.add_collection(s)
        cmd2.set_provider(p)
        container = cmd2.do()

        xarticle = create_articledata()
        xarticle.pid = "ASENS_2013_4_46_6_857_0"
        xarticle.fpage = 857
        xarticle.lpage = 878
        xarticle.seq = 1
        xarticle.title_tex = "Mon article"
        cmd3 = addArticleDatabaseCmd({"xobj": xarticle})
        cmd3.set_container(container)
        article = cmd3.do()

        xarticle = create_articledata()
        xarticle.title_tex = "Mon Article"
        xarticle.pid = article.pid
        xarticle.fpage = "857"
        xarticle.lpage = "878"
        xarticle.lang = "fr"
        xarticle.kwds = [
            {"type": "msc", "lang": "und", "value": "37D40"},
            {"type": "msc", "lang": "und", "value": "37D20"},
        ]

        cmd = addArticleSolrCmd({"xobj": xarticle, "id": article.id, "pid": article.pid})
        cmd.add_collection(s)
        cmd.set_container(container)
        cmd.do()

        # result = self.solrSearch('*:*' )
        result = self.solrSearch(f"id:{article.id}")

        self.assertEqual(result.hits, 1)
        self.assertEqual(result.docs[0]["classname"], "Article de recherche")
        self.assertEqual(result.docs[0]["page_range"], "p. 857-878")

        cmd4 = solrGetDocumentByPidCmd({"pid": "ASENS_2013_4_46_6_857_0"})
        result = cmd4.do()
        self.assertEqual(result["pid"], "ASENS_2013_4_46_6_857_0")

        cmd.undo()

        result = self.solrSearch(f"id:{article.id}")
        self.assertEqual(result.hits, 0)

        cmd3.undo()
        cmd2.undo()
        cmd1.undo()
        # cmd0.undo()

        self.assertEqual(Collection.objects.count(), 0)
        self.assertEqual(Container.objects.count(), 0)
        self.assertEqual(Article.objects.count(), 0)

    def test_06_book(self):
        print("** test_06_book")
        self.assertRaises(ValueError, addContainerSolrCmd(None).do)

        p = get_provider("mathdoc-id")

        xcol = create_publicationdata()
        xcol.coltype = "book-series"
        xcol.pid = "MyCol"
        xcol.title_tex = "MyCol"
        cmd0 = addCollectionDatabaseCmd({"xobj": xcol})
        cmd0.set_provider(p)
        collection = cmd0.do()

        xbook = create_bookdata()
        xbook.ctype = "book-monograph"
        xbook.pid = "MyBook"
        xbook.year = 2013
        xbook.volume = 1
        xbook.vseries = 0
        xbook.number = 3
        xbook.last_modified_iso_8601_date_str = "2012-06-01T00:00:00"
        text = "This is an abstract"
        xbook.abstracts = [
            {
                "tag": "trans-abstract",
                "lang": "fr",
                "value_html": text,
                "value_xml": text,
                "value_tex": text,
            }
        ]
        cmd0a = addContainerDatabaseCmd({"xobj": xbook})
        cmd0a.add_collection(collection)
        cmd0a.set_provider(p)
        book = cmd0a.do()

        # cmd0b = addPublisherDatabaseCmd({"name": 'Institut Fourier'})
        # publisher = cmd0b.do()

        xbook = create_bookdata()
        xbook.pid = book.pid
        xbook.ctype = "book-monograph"
        xbook.year = "2013"
        xbook.volume = "  46"
        xbook.lang = "en"

        cmd = addContainerSolrCmd({"id": book.id, "pid": book.pid, "xobj": xbook})
        cmd.add_collection(collection)
        cmd.do()

        # result = self.solrSearch('*:*' )
        result = self.solrSearch(f"id:{book.id}")

        self.assertEqual(result.hits, 1)
        self.assertEqual(result.docs[0]["classname"], "Livre")
        self.assertEqual(result.docs[0]["year"], "2013")

        # volume/vseries/number should be converted to int
        self.assertNotEqual(result.docs[0]["volume"], "46")
        self.assertEqual(result.docs[0]["volume"], 46)

        cmd.undo()

        result = self.solrSearch(f"id:{book.id}")
        self.assertEqual(result.hits, 0)

        cmd0a.undo()
        cmd0.undo()

        self.assertEqual(Collection.objects.count(), 0)
        self.assertEqual(Container.objects.count(), 0)
        self.assertEqual(Article.objects.count(), 0)

    def test_07_search_results(self):
        print("** test_07_search")

        f = open("tests/data/xml/journal-sms.xml")
        body = f.read()
        f.close()

        cmd1 = addCollectionsXmlCmd({"body": body})
        cmd1.do()

        f = open("tests/data/xml/issue.xml")
        body = f.read()
        f.close()

        cmd2 = addIssueXmlCmd({"body": body})
        cmd2.do()

        # in issue/xml, year is "1969-1970"
        # in SolR, years are split for search/facets purposes
        # Searching "1969" returns the issue/articles of year "1969-1970"
        # params = {'q': 'year:1969' }

        # cmd = solrSearchCmd( params )
        # results = cmd.do()
        #
        # # 2 articles
        # self.assertEqual( results.hits, 2 )
        #
        # for result in results.docs:
        #     title = result['title_tex']
        #     i = 0

        #### Test search filtering
        # Search for 'chocolat' in all data
        # The search should match the 2 articles from `issue.xml`
        params = {
            "qs": [{"name": "all", "value": "chocolat", "not": False, "first": None, "last": None}]
        }
        cmd = solrSearchCmd(params)
        results = cmd.do()
        self.assertEqual(results.hits, 2)

        # Search for 'MARTINGALES' in all data
        # The search should only match the first article from `issue.xml`
        params = {
            "qs": [
                {"name": "all", "value": "MARTINGALES", "not": False, "first": None, "last": None}
            ]
        }
        cmd = solrSearchCmd(params)
        results = cmd.do()
        self.assertEqual(results.hits, 1)

        #### Test the highlighting
        # Highlight of author
        params = {
            "qs": [{"name": "author", "value": "Cori", "not": False, "first": None, "last": None}]
        }
        cmd = solrSearchCmd(params)
        results = cmd.do()
        self.assertEqual(results.hits, 1)
        self.assertTrue("<strong>Cori</strong>" in results.docs[0]["au"])

        # TODO: Highlight of year

        # Highlight of collection_title_tex / collection_title_html
        params = {
            "qs": [
                {"name": "all", "value": "Séminaire", "not": False, "first": None, "last": None}
            ]
        }
        cmd = solrSearchCmd(params)
        results = cmd.do()
        self.assertEqual(results.hits, 2)
        self.assertTrue("<strong>Séminaire</strong>" in results.docs[0]["collection_title_tex"])
        self.assertTrue("<strong>Séminaire</strong>" in results.docs[0]["collection_title_html"])

        # Highlight of abstract_tex for 'en' and trans_abstract_text for 'fr'
        params = {
            "qs": [
                {
                    "name": "abstract_tex",
                    "value": "compact",
                    "not": False,
                    "first": None,
                    "last": None,
                }
            ]
        }
        with translation.override("en"):
            cmd = solrSearchCmd(params)
            results = cmd.do()
            self.assertEqual(results.hits, 1)
            self.assertTrue("<strong>compact</strong>" in results.docs[0]["highlighting"]["value"])
            # Check that it's the English abstract
            self.assertTrue("boundary" in results.docs[0]["highlighting"]["value"])
        with translation.override("fr"):
            cmd = solrSearchCmd(params)
            results = cmd.do()
            self.assertEqual(results.hits, 1)
            self.assertTrue("<strong>compact</strong>" in results.docs[0]["highlighting"]["value"])
            # Check that it's the French abstract
            self.assertTrue("un espace" in results.docs[0]["highlighting"]["value"])

        # Highlight of kwd
        params = {
            "qs": [
                {
                    "name": "kwd",
                    "value": '"fibrés algébriques"',
                    "not": False,
                    "first": None,
                    "last": None,
                }
            ]
        }
        cmd = solrSearchCmd(params)
        results = cmd.do()
        self.assertEqual(results.hits, 1)
        self.assertTrue(
            "<strong>fibrés algébriques</strong>" in results.docs[0]["highlighting"]["value"]
        )

        # Highlight of body
        params = {
            "qs": [
                {"name": "body", "value": "MARTINGALES", "not": False, "first": None, "last": None}
            ]
        }
        cmd = solrSearchCmd(params)
        results = cmd.do()
        self.assertEqual(results.hits, 1)
        self.assertTrue("<strong>Martingales</strong>" in results.docs[0]["highlighting"]["value"])

        # Highlight of bibitem + test SITE_URL_PREFIX setting
        with self.settings(SITE_URL_PREFIX="my-prefix"):
            params = {
                "qs": [
                    {
                        "name": "references",
                        "value": '"gauge theory"',
                        "not": False,
                        "first": None,
                        "last": None,
                    }
                ]
            }
            cmd = solrSearchCmd(params)
            results = cmd.do()
            self.assertEqual(results.hits, 1)
            doc = results.docs[0]
            self.assertTrue("<strong>gauge theory</strong>" in doc["highlighting"]["value"])
            # Test SITE_URL_PREFIX
            self.assertTrue(doc["pdf"].startswith("/my-prefix"))
            self.assertTrue(doc["tex"].startswith("/my-prefix"))
            self.assertTrue(len(results.facets["author_facets"]) > 0)
            for _, facets in results.facets.items():
                self.assertTrue(all([facet.href.startswith("/my-prefix") for facet in facets]))

        cmd2.undo()
        cmd1.undo()

    def test_07_bis_search_results_cr(self):
        """Tests the search cmd impersonating CR website."""
        print("** test_07_bis_search_cr")
        cr_site = PtfSite(
            id=34, domain="comptes-rendus.academie-sciences.fr", name="cr", acro="cr"
        )
        cr_site.save()
        crmath = PtfSite(
            id=26,
            domain="comptes-rendus.academie-sciences.fr/mathematique",
            name="crmath",
            acro="crmath",
        )
        crmath.save()

        # Impersonate crmath site to import the documents
        with self.settings(SITE_NAME="crmath", SITE_ID=26, COLLECTION_PID="CRMATH"):
            f = open("tests/data/xml/journal-sms.xml")
            body = f.read()
            f.close()

            cmd1 = addCollectionsXmlCmd({"body": body})
            cmd1.do()

            f = open("tests/data/xml/issue.xml")
            body = f.read()
            f.close()

            cmd2 = addIssueXmlCmd({"body": body})
            cmd2.do()

            # Test Ptfsite filtering for sites other than Numdam
            # sites_facets should be empty
            params = {
                "qs": [
                    {"name": "all", "value": "chocolat", "not": False, "first": None, "last": None}
                ]
            }
            cmd = solrSearchCmd(params)
            results = cmd.do()
            self.assertTrue("sites:26" in cmd.filters)
            self.assertEqual(results.hits, 2)
            self.assertTrue(len(results.facets["sites_facets"]) == 0)

        # Impersonate cr site to execute the search command
        # sites_facets should be filled + prefix should be appended to urls
        with self.settings(SITE_NAME="cr", SITE_ID=34, COLLECTION_PID="CR"):
            params = {
                "qs": [
                    {"name": "all", "value": "chocolat", "not": False, "first": None, "last": None}
                ]
            }
            cmd = solrSearchCmd(params)
            results = cmd.do()
            self.assertTrue("sites:[26 TO 31]" in cmd.filters)
            self.assertTrue(len(results.facets["sites_facets"]) > 0)
            self.assertEqual(results.hits, 2)
            for doc in results.docs:
                self.assertTrue(doc["item_url"].startswith("/mathematique"))

        cmd2.undo()
        cmd1.undo()

        sites = PtfSite.objects.filter(id__in=[26, 34])
        for site in sites:
            site.delete()

    def test_00_search_these(self):
        print("** test_08_search_these")

        with open("tests/data/xml/collection-these.xml") as f:
            body = f.read()

        cmd1 = addCollectionsXmlCmd({"body": body})
        cmd1.do()

        with open("tests/data/xml/these.xml") as f:
            body = f.read()

        cmd2 = addBookXmlCmd({"body": body})
        cmd2.do()

        params = {
            "qs": [
                {"name": "all", "value": "originelles", "not": False, "first": None, "last": None}
            ]
        }

        cmd = solrSearchCmd(params)
        results = cmd.do()

        self.assertEqual(results.hits, 1)
        cmd2.undo()
        cmd1.undo()

    def test_09_book_volume(self):
        print("** test_09_book_volume")

        with open("tests/data/xml/collection-these.xml") as f:
            body = f.read()

        cmd1 = addCollectionsXmlCmd({"body": body})
        cmd1.do()

        with open("tests/data/xml/book-these.xml") as f:
            body = f.read()

        cmd2 = addBookXmlCmd({"body": body})
        book = cmd2.do()

        results = self.solrSearch(f"id:{book.id}")
        solrFactory.reset()

        self.assertEqual(results.hits, 1)
        doc = results.docs[0]
        self.assertEqual(doc["number"], 156)

        with open("tests/data/xml/collection-msmf.xml") as f:
            body = f.read()

        cmd3 = addCollectionsXmlCmd({"body": body})
        cmd3.do()

        with open("tests/data/xml/book.xml") as f:
            body = f.read()

        cmd4 = addBookXmlCmd({"body": body})
        cmd4.do()

        # Test the search on books (solrInternalSearch)
        path = "/books"
        params = {
            "q": '(classname:"Livre")',
            "sort": "fau asc,title_sort_key asc",
            "page": 1,
            "filters": "",
            "path": path,
            "search_path": f"{path}?",
            "facet_fields": [
                "author_facet",
                "firstLetter",
                "year_facet",
                "collection_title_facet",
            ],
            "query": "",
            "start": 0,
            "rows": 20,
        }
        cmd = solrInternalSearchCmd(params)
        results = cmd.do()

        self.assertEqual(results.hits, 2)
        facets = results.facets
        self.assertEqual(len(facets["author_facets"]), 2)
        self.assertEqual(len(facets["year_range_facets"]), 2)
        self.assertEqual(len(facets["letter_facets"]), 27)
        self.assertEqual(len(facets["collection_title_facets"]), 2)

        # Test the filtering on author first letter
        params["filters"] = ["{!tag=firstletter}firstNameFacetLetter:A"]
        cmd = solrInternalSearchCmd(params)
        results = cmd.do()

        self.assertEqual(results.hits, 1)
        self.assertTrue(results.docs[0]["au"].startswith("A"))
        facets = results.facets
        self.assertEqual(len(facets["author_facets"]), 1)
        self.assertEqual(len(facets["year_range_facets"]), 1)
        self.assertEqual(len(facets["letter_facets"]), 27)
        self.assertEqual(len(facets["collection_title_facets"]), 1)
        for letter_facet in facets["letter_facets"]:
            if letter_facet.name == "A":
                self.assertEqual(letter_facet.active, "active")
            # The other book has an author starting with M
            elif letter_facet.name in ["All", "M"]:
                self.assertEqual(letter_facet.active, "not-active")
            else:
                self.assertEqual(letter_facet.active, "disabled")

        cmd4.undo()
        cmd3.undo()
        cmd2.undo()
        cmd1.undo()
