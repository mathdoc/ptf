import os
import dateutil.parser

from datetime import datetime
from lxml import etree

from django.conf import settings
from django.core.management import call_command
from django.test import TestCase
from django.utils import timezone
from django.test.client import RequestFactory

from ptf import exceptions, model_helpers
from ptf.cmds import ptf_cmds, solr_cmds, xml_cmds
from ptf.models import *
from ptf import model_data_comparator
from ptf import model_data_converter


class XmlCmdTestCase(TestCase):
    def setUp(self):
        solr_cmds.solrFactory.solr_url = settings.SOLR_URL
        self._solr = solr_cmds.solrFactory.get_solr()
        self._solr.delete(q='*:*')
        self.factory = RequestFactory()

    def tearDown(self):
        self._solr.delete(q='*:*')

    def solrSearch(self, searchStr):
        return self._solr.search(searchStr)

    def check_empty_database(self):
        self.assertEqual(Collection.objects.count(), 0)
        self.assertEqual(Container.objects.count(), 0)
        self.assertEqual(Article.objects.count(), 0)
        self.assertEqual(BibItem.objects.count(), 0)
        self.assertEqual(Abstract.objects.count(), 0)
        self.assertEqual(ContribGroup.objects.count(), 0)
        self.assertEqual(Contrib.objects.count(), 0)
        self.assertEqual(KwdGroup.objects.count(), 0)
        self.assertEqual(Kwd.objects.count(), 0)
        self.assertEqual(Publisher.objects.count(), 0)

    def compare_issue(self, colid, filename, file_in_test_data=False):

        settings.MERSENNE_TEST_DATA_FOLDER = ''

        self.check_empty_database()

        if file_in_test_data:
            f = open('tests/data/collection-' + colid + '.xml')
        else:
            f = open('/mathdoc_archive/' + colid + '/' + colid + '.xml')
        body = f.read()
        f.close()

        cmd1 = xml_cmds.addCollectionsXmlCmd({"body": body})
        journals = cmd1.do()
        collection = journals[0]

        if file_in_test_data:
            this_folder = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
            filename = os.path.join(this_folder, filename)

        f = open(filename)
        body = f.read()
        f.close()

        cmd = xml_cmds.addCedricsIssueXmlCmd({"body": body,
                                              "is_seminar": collection.coltype == "acta"})
        issue1 = cmd.do()
        xissue1 = cmd.xissue

        cmd.undo()

        cmd = xml_cmds.importCedricsIssueXmlCmd({
            "colid": collection.pid,
            "input_file": filename,
            "remove_email": False,
            "remove_date_prod": True,
            "copy_files": False
        })
        issue2 = cmd.do()
        publisher = issue2.my_publisher
        xissue2 = cmd.xissue

        model_data_comparator.TEST_STRICT = True
        issue_comparator = model_data_comparator.IssueDataComparator()

        issues_diff = {}
        result = issue_comparator.compare(xissue1, xissue2, issues_diff)

        self.assertEqual(result, True)

        cmd.undo()

        cmd1.undo()

        cmd = ptf_cmds.addPublisherPtfCmd({"pid": publisher.pid})
        cmd.set_object_to_be_deleted(publisher)
        cmd.undo()
        # cmd0.undo()

        # settings.SITE_NAME = temp_settingsSITE_NAME
        # settings.SITE_ID = temp_settingsSITE_ID

        self.check_empty_database()

    def compare_issue_no_xsl(self, colid, exploitation_filename, filename):

        settings.MERSENNE_TEST_DATA_FOLDER = '/tmp/mersenne'

        self.check_empty_database()

        f = open('/mathdoc_archive/' + colid + '/' + colid + '.xml')
        body = f.read()
        f.close()

        cmd1 = xml_cmds.addCollectionsXmlCmd({"body": body})
        journals = cmd1.do()
        collection = journals[0]

        cmd = xml_cmds.importCedricsIssueDirectlyXmlCmd({
            "colid": collection.pid,
            "input_file": filename,
            "remove_email": False,
            "remove_date_prod": True,
            "copy_files": True
        })
        issue1 = cmd.do()
        xissue1 = cmd.xissue

        cmd = xml_cmds.importCedricsIssueXmlCmd({
            "colid": collection.pid,
            "input_file": exploitation_filename,
            "remove_email": False,
            "remove_date_prod": True,
            "copy_files": True
        })
        issue2 = cmd.do()
        xissue2 = cmd.xissue

        model_data_comparator.TEST_STRICT = True
        issue_comparator = model_data_comparator.IssueDataComparator()

        issues_diff = {}
        result = issue_comparator.compare(xissue1, xissue2, issues_diff)

        self.assertEqual(result, True)

        # importCedricsIssue* use addOrUpdateIssueXmlCmd
        # The update commands have no undo: we need to call addContainerPtfCmd.undo()
        provider = model_helpers.get_provider_by_name(issue1.provider)
        cmd = ptf_cmds.addContainerPtfCmd(
            {"pid": issue1.pid, "ctype": issue1.ctype,
             'to_folder': settings.MERSENNE_TEST_DATA_FOLDER})
        cmd.set_provider(provider)
        cmd.add_collection(collection)
        cmd.set_object_to_be_deleted(issue1)
        cmd.undo()

        cmd1.undo()

        self.assertEqual(Publisher.objects.count(), 1)
        publisher = Publisher.objects.first()
        cmd = ptf_cmds.addPublisherPtfCmd({"pid": publisher.pid})
        cmd.set_object_to_be_deleted(publisher)
        cmd.undo()

        self.check_empty_database()

    def test_01_all_cedramdev_production(self):
        print("** test_01_all_cedramdev_production")

        settings.REF_JEP_STYLE = False

        src_folder = "/cedram_dev/production_tex/CEDRAM"
        src_folder2 = "/cedram_dev/exploitation/cedram"
        collections = [d for d in os.listdir(src_folder) if os.path.isdir(os.path.join(src_folder, d))
                       # and d not in ['ACIRM', 'AIF', 'AFST', 'AHL', 'ALCO', 'AMBP', 'CCIRM', 'CML',
                       # 'CRBIOL', 'CRCHIM', 'CRGEOS', 'CRMATH', 'CRPHYS', 'CRMECA', 'JEDP', 'JEP', 'JTNB', 'MRR',
                       # 'MSIA', 'OGEO', 'OJMO', 'PMB', 'SMAI-JCM', 'SLSEDP', 'TSG', 'WBLN']
                       ]
        collections = sorted(collections)

        for col in collections:
            print("***", col, '***')
            settings.REF_JEP_STYLE = (col == "JEP")

            issue_folder = os.path.join(src_folder, col)
            issues = [d for d in os.listdir(issue_folder) if os.path.isdir(os.path.join(issue_folder, d))
                      and d not in ["AIF_2014__64_3", "AIF_2014__64_5", "AIF_2015__65_5", "AIF_2015__65_6",
                                    "AIF_2016__66_3", "AIF_2017__67_1", "AIF_2017__67_6", "AIF_2019__69_2",
                                    "AIF_2019__69_3", "AIF_2020__70_3",
                                    "AFST_2019_6_28_5",
                                    "ALCO_2018__1_5", "ALCO_2020__3_3", "ALCO_2020__3_6",
                                    "CRBIOL_0__0_0", "CRBIOL_2020__343_2",
                                    "CRCHIM_2020__23_1", "CRCHIM_2020__23_2",
                                    "CRGEOS_2020__352_1", "CRGEOS_2020__352_2", "CRGEOS_2020__352_3",
                                    "CRMATH_2020__358_11-12", "CRMATH_2020__358_2", "CRMATH_2020__358_4",
                                    "CRMATH_2020__358_5",
                                    "CRPHYS_0__0_0", "CRPHYS_2020__21_1", "CRPHYS_2020__21_2",
                                    "CRMECA_0__0_0",
                                    "CML_2013__5_1",
                                    "JEP_2015__2_", "JEP_2020__7_",
                                    "PMB_2014___1", "PMB_2014___2", "PMB_2015___.xml", "PMB_2017___.xml",
                                    "PMB_2018___.xml"]
                      ]
            """
            ACIRM: pas de doi, pas de version tex pour les références (texdata)
            AIF_2014__64_3: problème d'encodage sur le titre AIF_2014__64_3_1291_0 (cf site web des AIF)
            AIF_2014__64_5: 2 articles ont le même pid
            AIF_2015__65_5, AIF_2015__65_6: Aucun fichier de /cedram_dev/production_tex n'a des doi.
            AIF_2016__66_3: doi avec un espace
            AIF_2017__67_1_237_0 [16], CML_2013__5_1: <chapter> can be used only for an inbook
            AIF_2017__67_6: doi avec "DOI:"
            AIF_2019__69_2: une <note> a été ajouté à la main dans AIF_2019__69_2_515_0 [8] d'exploitation
            AIF_2019__69_3_955_0: abstract modifié dans exploitation
            AIF_2020__70_3: exploitation modifié à la main ($\protect ajouté dans des keywords...)
            AFST < AFST_2019_6_28_3: pas de doi dans production_tex (except: AFST_2018_6_27_2)
            AFST_2018_6_27_2: abstract modifié dans exploitation
            AFST_2019_6_28_5: abstract modifié dans exploitation
            AFST_2020_6_29_1: la <notice> d'exploitation n'a pas de <langue>
            AFST_2020_6_29_2: abstract modifié dans exploitation
            ALCO_2018__1_5: doi avec https
            ALCO_2020__3_3_667_0: abstract modifié dans exploitation
            ALCO_2020__3_6_1231_0 [11]: auteurs modifié dans exploitation
            AFST < AFST_2019: pas de doi dans production_tex
            AMBP < AMBP_2019__26_2: pas de doi dans production_tex
            CCIRM: pas de doi dans production_tex
            CML < CML_2019__11_2: pas de doi dans production_tex
            CML_2019__11_2_25_0 [3], CML_2020__12_1: pas de texmath dans production
            CRBIOL_2020__343_2: doi avec un espace
            CRCHIM_2020__23_1_47_0 [16]: title modifié dans exploitation
            CRCHIM_2020__23_2_143_0 [12]: title modifié dans exploitation
            CRCHIM_2020__23_3: doi avec https
            CRGEOS_2020__352_1_19_0: abstract modifié dans exploitation
            CRGEOS_2020__352_2_139_0: abstract modifié dans exploitation
            CRGEOS_2020__352_3_199_0: abstract modifié dans exploitation
            CRMATH_2020__358_2: la <notice> d'exploitation n'a pas de <langue>
            CRMATH_2020__358_4: fichier modifié dans exploitation
            CRMATH_2020__358_5_523_0: fichier modifié dans exploitation (title: On the canonical solution of <span title="$\protect \,\protect \,\protect \overline{\protect \!\partial }$"... !
            CRMATH_2020__358_11-12: (temporary) &amp; added by deplace_fasc
            CRPHYS_0__0_0: date non valide (crphys20201304-cdrxml.xml:<date_online>dateprod </date_online>)
            CRPHYS_2020__21_1: abstract modifié dans exploitation
            CRPHYS_2020__21_2_185_0: lpage: 199 vs 198 !!!
            CRMECA_0__0_0: date non valide
            JEDP: pas de doi dans production_tex
            JEP_2015__2_: doi avec https
            JEP_2020__7_: <chapter> can be used only for an inbook
            JEP_2021__8_: doi avec espace
            JTNB < JTNB_2019__31_2: pas de doi dans production_tex
            JTNB/plaintext/JTNB_2020__32_2_665_0.xml is missing
            MRR_2020__1_: abstract modifié dans exploitation (espace en moins)
            MSIA < MSIA_2017__8_1: pas de doi dans production_tex
            OGEO_2020__2_: doi avec http
            PMB 2014/2015/2017/2018: pas de doi dans production_tex
            PMB 2016: pas de tex dans une ref de production_tex
            PMB_2019___1.xml: pas de doi dans exploitation !
            SMAI-JCM_2015__1_, SMAI-JCM_2016__2_: doi avec http
            SMAI-JCM < 2019: pas de doi dans production
            SMAI-JCM_2020__6_: abstract modifié dans exploitation (espace en moins)
            SLSEDP < 2019-2020: pas de doi dans production_tex
            SLSEDP_2019-2020___: pas de tex dans une ref de production_tex
            TSG: pas de tex dans une ref de production_tex
            WBLN < WBLN_2018__5_: pas de doi dans production_tex
            """
            issues = sorted(issues)

            start = False
            for pid in issues:
                if True or pid == 'WBLN_2018__5_':
                    start = True

                # Already done
                if start:
                    exploitation_filename = os.path.join(src_folder2, col, "metadata", pid + ".xml")
                    production_filename = os.path.join(issue_folder, pid, pid + "-cdrxml.xml")
                    if os.path.isfile(exploitation_filename) and os.path.isfile(production_filename):
                        print(pid)
                        self.compare_issue_no_xsl(col, exploitation_filename, production_filename)
                    else:
                        text = "skipped "
                        if not os.path.isfile(production_filename):
                            text += "(no production file)"
                        else:
                            text += "(no exploitation file)"
                        print(pid, text)

    def test_10_all_cedramdev(self):
        print("** test_10_all_cedram_dev")

        settings.REF_JEP_STYLE = False

        src_folder = "/cedram_dev/exploitation/cedram"
        collections = [d for d in os.listdir(src_folder) if os.path.isdir(os.path.join(src_folder, d))]
        # and d not in ['AIF', 'ALCO', 'PMB', 'CCIRM', 'SMAI-JCM', 'CML', 'AHL', 'WBLN', 'ACIRM', 'CRBIOL', 'CRCHIM', 'SLSEDP', 'JEP', 'AFST', 'JTNB', 'AMBP', 'CRMECA', 'MSIA', 'JEDP', 'CRMATH', 'OGEO', 'CRPHYS', 'MRR', 'TSG', 'OJMO']]

        for col in collections:
            col = "AIF"
            settings.REF_JEP_STYLE = (col == "JEP")

            issue_folder = os.path.join(src_folder, col, "metadata")
            files = [f for f in os.listdir(issue_folder) if
                     os.path.isfile(os.path.join(issue_folder, f)) and ".swp" not in f and f not in (
                     "CRCHIM_2020__23_1.xml", "CRCHIM_2020__23_2.xml",
                     "CRGEOS_2020__352_2.xml", "CRPHYS_0__0_0.xml", "CRGEOS_0__0_0.xml")]
            # CRGEOS_2020__352_2: a tex-math node is swallowed by ptf-xsl: the alternatives become mixed up

            files = sorted(files)

            start = False
            for f in files:
                if f == "AIF_2020__70_3.xml":
                    start = True

                # Already done
                if start:
                    print(f)
                    self.compare_issue(col, os.path.join(issue_folder, f))

    def test_02_issue(self):
        print("** test_02_issue: JEP_2019__6_")

        settings.REF_JEP_STYLE = True
        self.compare_issue("jep", 'data/cedrics/JEP/metadata/JEP_2019__6_.xml', file_in_test_data=True)

    def test_03_issue(self):
        print("** test_03_issue: AIF_2017__67_2")

        settings.REF_JEP_STYLE = False
        self.compare_issue("aif", 'data/cedrics/AIF/metadata/AIF_2017__67_2.xml', file_in_test_data=True)

    def test_04_escape(self):
        print("** test_04_escape")
        self.check_empty_database()

        f = open('tests/data/collection-aif.xml')
        body = f.read()
        f.close()
        cmdCol = xml_cmds.addCollectionsXmlCmd({"body": body})
        journals = cmdCol.do()

        f = open('tests/data/issue-aif-escape.xml')
        body = f.read()
        f.close()

        cmd = xml_cmds.addIssueXmlCmd({"body": body})
        issue = cmd.do()

        article = model_helpers.get_article('AIF_2015__65_6_2331_0')
        self.assertEqual(article.title_html, "Toto &amp; &lt;Titi&gt;")

        cmd.undo()
        cmdCol.undo()

        self.check_empty_database()
