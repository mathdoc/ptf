from django.conf import settings
from django.test import TestCase

from ptf import exceptions
from ptf import model_helpers
from ptf.cmds.ptf_cmds import addCollectionPtfCmd
from ptf.cmds.ptf_cmds import addContainerPtfCmd
from ptf.cmds.ptf_cmds import addProviderPtfCmd
from ptf.cmds.ptf_cmds import addPublisherPtfCmd
from ptf.cmds.solr_cmds import solrFactory
from ptf.model_data import create_issuedata
from ptf.model_data import create_publicationdata
from ptf.model_data import create_publisherdata
from ptf.models import Abstract
from ptf.models import Collection
from ptf.models import Container
from ptf.models import Provider
from ptf.models import Resource
from ptf.models import ResourceId


class PtfCmdTestCase(TestCase):
    def get_solr(self):
        return solrFactory.get_solr()

    def setUp(self):
        solrFactory.solr_url = settings.SOLR_URL
        self.get_solr().delete(q="*:*")

    def tearDown(self):
        self.get_solr().delete(q="*:*")

    def test_01_provider(self):
        print("** test_ptf 01_provider")

        # self.assertRaises( ValueError, addProviderPtfCmd(None).do )

        cmd = addProviderPtfCmd({"name": "foo-numdam", "pid_type": "foo-mathdoc-id"})
        p = cmd.do()

        self.assertEqual(Provider.objects.count(), 2)
        self.assertEqual(p.name, "foo-numdam")
        self.assertIsNone(p.sid_type)

        try:
            cmd.do()
            self.fail(
                "addProviderPtfCmd should raise ResourceExist if the provider already exists"
            )
        except exceptions.ResourceExists:
            pass

        self.assertEqual(Provider.objects.count(), 2)

        cmd.undo()
        self.assertRaises(Provider.DoesNotExist, Provider.objects.get, name="foo-numdam")

        # Attempting to remove a 2nd time should raise
        # exceptions.ResourceDoesNotExist
        self.assertRaises(exceptions.ResourceDoesNotExist, cmd.undo)

    def test_02_collection(self):
        print("** test_ptf 02_collection")
        self.assertRaises(ValueError, addCollectionPtfCmd().do)

        # a Collection needs a Provider
        # cmd0 = addProviderPtfCmd( { "name":"numdam", "pid_type":"mathdoc-id" } )
        # p = cmd0.do()
        p = model_helpers.get_provider("mathdoc-id")

        xcol = create_publicationdata()
        xcol.coltype = "journal"
        xcol.pid = "AFST"
        xcol.title_tex = "Mon Collection"
        xcol.ids = [("cedram-id", "AFST"), ("issn", "0240-2963")]
        cmd = addCollectionPtfCmd({"xobj": xcol})
        cmd.set_provider(p)
        collection = cmd.do()

        self.assertEqual(Collection.objects.count(), 1)
        self.assertEqual(collection.pid, "AFST")
        self.assertIsNone(collection.sid)
        self.assertEqual(collection.title_tex, "Mon Collection")
        self.assertEqual(collection.provider, p)

        result = self.get_solr().search("".join(["id:", str(collection.id)]))

        self.assertEqual(result.hits, 0)

        # we do not index collection
        # self.assertEqual(result.hits, 1)
        # self.assertEqual(result.docs[0]['classname'], 'Journal')
        # self.assertEqual(result.docs[0]['collection_title'], 'Mon Collection')
        # self.assertEqual(result.docs[0]['coltype'], 'journal')

        # cur_site = Site.objects.get_current().ptfsite
        # sites = [Site.objects.get_current().ptfsite.name]
        #
        # self.assertIsNotNone(result.docs[0]['sites'])
        # self.assertEqual(len(result.docs[0]['sites']), 1)
        # self.assertEqual(result.docs[0]['sites'][0], settings.SITE_ID)

        cmd.undo()
        self.assertRaises(Resource.DoesNotExist, Collection.objects.get, pid="AFST", provider=p)
        self.assertEqual(ResourceId.objects.count(), 0)

        result = self.get_solr().search("".join(["id:", str(collection.id)]))
        self.assertEqual(result.hits, 0)

        # Attempting to remove a 2nd time should raise
        # exceptions.ResourceDoesNotExist
        self.assertRaises(exceptions.ResourceDoesNotExist, cmd.undo)

    def test_03_container(self):
        print("** test_ptf 03_container")
        self.assertRaises(ValueError, addContainerPtfCmd().do)

        # a Collection needs a Provider
        # cmd0 = addProviderPtfCmd( { "name":"numdam", "pid_type":"mathdoc-id" } )
        # p = cmd0.do()
        p = model_helpers.get_provider("mathdoc-id")

        xpub = create_publisherdata()
        xpub.name = "Institut Fourier"
        cmd0 = addPublisherPtfCmd({"xobj": xpub})
        publisher = cmd0.do()

        xcol = create_publicationdata()
        xcol.coltype = "journal"
        xcol.pid = "AFST"
        xcol.title_tex = "Mon Collection"
        xcol.ids = [("cedram-id", "AFST"), ("issn", "0240-2963")]
        cmd1 = addCollectionPtfCmd({"xobj": xcol})
        cmd1.set_provider(p)
        collection = cmd1.do()

        xissue = create_issuedata()
        xissue.pid = "ASENS_2013_4_46_6"
        xissue.ctype = "issue"
        xissue.abstracts = [
            {"tag": "trans-abstract", "lang": "fr", "value_xml": "This is an abstract"}
        ]
        xissue.year = "2013"
        xissue.volume = "  46"
        xissue.number = "6 "
        xissue.last_modified_iso_8601_date_str = "2012-06-01T00:00:00"

        cmd = addContainerPtfCmd({"xobj": xissue})
        cmd.add_collection(collection)
        cmd.set_publisher(publisher)
        cmd.set_provider(p)

        container = cmd.do()

        self.assertEqual(Container.objects.count(), 1)
        self.assertEqual(container.pid, "ASENS_2013_4_46_6")
        self.assertIsNone(container.sid)
        self.assertEqual(container.my_collection, collection)
        self.assertEqual(container.provider, p)

        self.assertEqual(Abstract.objects.count(), 1)
        self.assertEqual(container.abstract_set.count(), 1)
        self.assertEqual(container.abstract_set.first().value_xml, "This is an abstract")

        # result = self.get_solr().search('*:*' )
        result = self.get_solr().search("".join(["id:", str(container.id)]))
        self.assertEqual(result.hits, 0)

        # we do not index container except ctype=book-monograph
        # self.assertEqual(result.hits, 1)
        # self.assertEqual(result.docs[0]['classname'], 'Issue')
        # self.assertEqual(result.docs[0]['year'], '2013')
        # self.assertEqual(result.docs[0]['publisher_id'], publisher.id)
        #
        # # volume/vseries/number should be converted to int
        # self.assertNotEqual(result.docs[0]['volume'], '46')
        # self.assertEqual(result.docs[0]['volume'], 46)
        # self.assertNotEqual(result.docs[0]['vseries'], '4')
        # self.assertEqual(result.docs[0]['vseries'], 4)
        # self.assertNotEqual(result.docs[0]['number'], '6')
        # self.assertEqual(result.docs[0]['number'], 6)

        cmd.undo()
        self.assertRaises(
            Resource.DoesNotExist,
            Container.objects.get,
            pid="ASENS_2013_4_46_6",
            provider=p,
            my_collection=collection,
        )
        self.assertEqual(Abstract.objects.count(), 0)
        # Attempting to remove a 2nd time should raise
        # exceptions.ResourceDoesNotExist
        # Ce test ne passe plus avec Django 4.2
        # self.assertRaises(exceptions.ResourceDoesNotExist, cmd.undo)

        cmd1.undo()
        cmd0.undo()

    def test_04_delete(self):
        print("** test_ptf 04_delete")
        self.assertRaises(ValueError, addCollectionPtfCmd().do)

        p = model_helpers.get_provider("mathdoc-id")

        xcol = create_publicationdata()
        xcol.coltype = "journal"
        xcol.pid = "AFST"
        xcol.title_tex = "Mon Collection"
        xcol.ids = [("cedram-id", "AFST"), ("issn", "0240-2963")]
        cmd0 = addCollectionPtfCmd({"xobj": xcol})
        cmd0.set_provider(p)
        collection = cmd0.do()

        cmd = addCollectionPtfCmd()
        cmd.set_object_to_be_deleted(collection)
        cmd.undo()

        self.assertRaises(Resource.DoesNotExist, Collection.objects.get, pid="AFST", provider=p)
        self.assertEqual(ResourceId.objects.count(), 0)

        result = self.get_solr().search("".join(["id:", str(collection.id)]))
        self.assertEqual(result.hits, 0)
