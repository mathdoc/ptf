from django.contrib.messages import get_messages
from django.http import HttpResponse
from django.test import TestCase


class ViewTestCase(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()

    def clear_messages(self):
        """
        Deletes all messages of the test client.
        Checks the presence of messages in both cookies and session (depends on the
        configured storage).
        """
        if "messages" in self.client.cookies:
            del self.client.cookies["messages"]
        if "_messages" in self.client.session:
            del self.client.session["_messages"]

    def assertUniqueLevelMessage(self, response: HttpResponse, level: list, clear_messages=True):
        messages = list(get_messages(response.wsgi_request))
        self.assertEqual(len(messages), 1)
        self.assertIn(messages[0].level_tag, level)
        if clear_messages:
            self.clear_messages()

    def assertUniqueSuccessMessage(self, response: HttpResponse, clear_messages=True):
        """
        Asserts the given HttpResponse has an unique attached message with
        a success level_tag.
        If `clear_messages is True`, it deletes all the messages stored in the
        test client.
        """
        self.assertUniqueLevelMessage(response, ["success"], clear_messages)

    def assertUniqueErrorMessage(self, response: HttpResponse, clear_messages=True):
        """
        Asserts the given HttpResponse has an unique attached message with
        an error level_tag.
        If `clear_messages is True`, it deletes all the messages stored in the
        test client.
        """
        self.assertUniqueLevelMessage(response, ["error", "danger"], clear_messages)

    def assertUniqueWarningMessage(self, response: HttpResponse, clear_messages=True):
        """
        Asserts the given HttpResponse has an unique attached message with
        a warning level_tag.
        If `clear_messages is True`, it deletes all the messages stored in the
        test client.
        """
        self.assertUniqueLevelMessage(response, ["warning"], clear_messages)
