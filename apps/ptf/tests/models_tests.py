import pytest

# from ..models import Person
# from ..models import PersonInfo
from ..cmds.xml.xml_utils import split_kwds
from ..factories import ArticleFactory
from ..factories import ArticleWithSiteFactory
from ..factories import BibItemFactory
from ..factories import CollectionFactory
from ..factories import ContainerFactory
from ..factories import ContributionFactory
from ..factories import ExtIdFactory
from ..factories import KwdFactory
from ..factories import PublisherFactory
from ..factories import ResourceCountFactory
from ..factories import ResourceIdFactory
from ..factories import SubjFactory
from ..model_helpers import get_article
from ..model_helpers import get_article_by_doi
from ..models import Article
from ..models import Container


def test_kwds():
    def check_items(actual, expected):
        assert len(actual) == len(expected)
        assert all([a == b for a, b in zip(actual, expected)])

    check_items(split_kwds("abc"), ["abc"])
    check_items(split_kwds("a,b,c"), ["a", "b", "c"])
    check_items(split_kwds("a, b ,c"), ["a", "b", "c"])
    check_items(split_kwds("a,b,"), ["a", "b"])
    check_items(split_kwds("abc $formula$ with"), ["abc $formula$ with"])
    check_items(split_kwds("abc $formula$"), ["abc $formula$"])
    check_items(split_kwds("abc $for,mula$ with"), ["abc $for,mula$ with"])
    check_items(split_kwds("abc $formula$, with"), ["abc $formula$", "with"])
    check_items(split_kwds("abc $formula$ wi, th $tex$"), ["abc $formula$ wi", "th $tex$"])
    check_items(split_kwds("abc $formula$, with$"), ["abc $formula$, with$"])


@pytest.mark.django_db
def test_article_prefetch():
    article = ArticleWithSiteFactory()

    a = get_article(pid=article.pid, prefetch=True)
    assert a.doi == article.doi

    a = get_article_by_doi(doi=article.doi, prefetch=True)
    assert a.pid == article.pid


@pytest.mark.django_db
def test_article():
    a = ArticleFactory(lang="en")
    assert not a.is_edited_book()

    SubjFactory(resource=a, lang="en", type="type")
    SubjFactory(resource=a, lang="fr", type="type", value="subj1")
    SubjFactory(resource=a, lang="fr", type="type", value="subj2")
    subjs = a.get_subjs_by_type_and_lang()
    assert len(subjs) == 1
    assert "type" in subjs
    assert len(subjs["type"]) == 2
    assert len(subjs["type"]["fr"]) == 2
    assert len(subjs["type"]["en"]) == 1

    assert a.get_volume() == ""
    assert a.get_number() == "3"
    assert not a.embargo()
    assert a.get_wall() == 5

    c = CollectionFactory(pid="ALCO")
    p = a.my_container.my_publisher
    i = ContainerFactory(my_collection=c, my_publisher=p, seq=0, pid="PID1", vseries="10")

    a = ArticleFactory(lang="en", my_container=i, doi="d1")
    assert a.volume_series() == "Ser. 10, "

    a = ArticleFactory(lang="fr", my_container=i, doi="d2", title_tex="Title")
    assert a.volume_series() == "10e s{\\'e}rie, "

    i.vseries = "3"
    i.volume = "352"
    i.number = "12"

    assert a.pages() is None
    assert a.get_article_page_count() == 0
    assert a.get_page_text() == ""

    a.fpage = "2"
    a.page_type = "volant"
    assert a.pages() == "2"
    assert a.get_article_page_count() == 0
    assert a.get_page_text() == "p. 2"
    assert a.get_breadcrumb_page_text() == "p. 2"
    assert a.get_citation_base() == ", Serie 3, Volume 352 (2014) no. 12, p. 2. (Loose sheets)"

    i.volume = ""
    a.lpage = "2"
    a.page_type = "supplement"
    ContributionFactory(resource=a, last_name="A1", role="editor")
    assert a.get_page_text() == "p. 2"
    assert a.get_breadcrumb_page_text() == "p. 2"
    assert a.get_citation_base() == ", Serie 3, no. 12 (2014), p. 2. (Additional pages)"
    assert (
        a.get_citation()
        == "A1, John (ed.). Title. Collection1, Serie 3, no. 12 (2014), p. 2. (Additional pages) doi : d2."
    )

    a.lpage = "5"
    a.page_type = "preliminaire"
    i.ctype = "book"
    i.title_tex = "Great book"
    assert a.pages() == "2-5"
    assert a.get_article_page_count() == 4
    assert a.get_page_text() == "p. 2-5"
    assert a.get_breadcrumb_page_text() == "p. 2-5"
    assert a.get_citation_base() == ", Serie 3, no. 12 (2014), pp. 2-5. (Front Matter)"
    assert (
        a.get_citation(with_formatting=True)
        == "A1, John (ed.). <strong>Title</strong>, in <em>Great book</em>, Collection1, Serie 3, no. 12 (2014), pp. 2-5. (Front Matter) doi : d2."
    )

    a.page_range = "i-x"
    assert a.pages() == "i-x"
    a.page_type = "special"
    assert a.get_article_page_count() == 4
    assert a.get_page_text() == "p. i-x"
    assert a.get_breadcrumb_page_text() == "p. i-x"
    assert a.get_citation_base() == ", Serie 3, no. 12 (2014), p. i-x. (Special pages)"

    ResourceCountFactory(resource=a)
    a.fpage = a.lpage = ""
    a.article_number = "e1"
    assert a.pages() == "i-x"
    assert a.get_article_page_count() == 10
    assert a.get_page_text() == "10 p."
    assert a.get_summary_page_text() == "article no. e1, 10 p."
    assert a.get_breadcrumb_page_text() == "article no. e1"
    a.article_number = ""
    a.talk_number = "t1"
    assert a.get_summary_page_text() == "Talk no. t1, 10 p."
    assert a.get_breadcrumb_page_text() == "Talk no. t1"

    i.with_online_first = True
    assert a.get_breadcrumb_page_text() == a.doi
    a.doi = None
    assert a.get_breadcrumb_page_text() == "Online First"
    assert a.get_citation_base() == ", Online first (2014), 10 p."
    i.year = "0"
    assert a.get_citation_base() == ", Online first, 10 p."

    assert Article.objects.filter(doi="d2").order_by_published_date().first() == a
    assert Article.objects.filter(doi="d2").prefetch_for_toc().first() == a
    assert a.get_base_url() == "/article/"

    assert a.has_detailed_info()
    a.doi = None
    a.date_accepted = None
    a.date_online_first = None
    a.date_published = None
    r = ResourceIdFactory(resource=a)
    assert r.get_href() == "https://doi.org/10.5802/foo.1"
    assert a.has_detailed_info()
    KwdFactory(resource=a)
    assert a.has_detailed_info()
    ExtIdFactory(resource=a)
    assert a.has_detailed_info()

    SubjFactory(resource=a, lang="en", type="type", value="type1")
    SubjFactory(resource=a, lang="en", type="type", value="type2")
    SubjFactory(resource=a, lang="fr", type="subject", value="subject1")
    SubjFactory(resource=a, lang="en", type="pci", value="neuro")
    SubjFactory(resource=a, lang="en", type="conference", value="conf1")
    SubjFactory(resource=a, lang="en", type="heading", value="head1")
    assert a.get_subj_text() == "subject1 - type1, type2 - Neuroscience - head1 - conf1"
    assert a.get_conference() == "conf1"
    assert a.get_pci_section() == "neuro"
    assert a.get_pci_value() == "Neuroscience"
    c.pid = "CRMATH"
    assert a.get_subj_text() == "subject1 - head1"

    a = ArticleFactory(lang="fr", my_container=i, doi=None, title_tex="Title")
    assert not a.allow_crossref()
    a.doi = "doi1"
    c.doi = "doi_col"
    assert a.allow_crossref()


@pytest.mark.django_db
def test_contribution():
    a = ArticleFactory()

    contrib1 = ContributionFactory(resource=a, first_name="John", last_name="Smith", role="author")
    assert len(a.get_author_contributions()) == 1
    ContributionFactory(resource=a, first_name="John", last_name="Doe", role="editor")
    assert len(a.get_author_contributions()) == 1
    assert len(a.get_author_contributions(strict=False)) == 1
    contrib1.delete()
    assert len(a.get_author_contributions()) == 0
    assert len(a.get_author_contributions(strict=False)) == 1

    ContributionFactory(resource=a, first_name="John", last_name="Smith", role="author")
    ContributionFactory(resource=a, first_name="John", last_name="Doe", role="author")
    assert a.get_authors_short() == "Smith, John; Doe, John"
    ContributionFactory(resource=a, first_name="Toto", last_name="Titi", role="author")
    assert a.get_authors_short() == "Smith, John; Doe, John <i>et al.</i>"

    for role in ["contributor", "redaktor", "organizer", "presenter"]:
        c = ContributionFactory(resource=a, last_name="C1", role=role)
        fct_name = f"get_{role}s"
        ftor = getattr(a, fct_name, None)
        contribs = ftor()
        assert len(contribs) == 1
        assert contribs[0] == c


@pytest.mark.django_db
def test_collection():
    c = CollectionFactory.build(pid="ALCO")
    assert c.get_absolute_url() == f"/item/{c.pid}/"

    c = CollectionFactory.build(coltype="thesis")
    assert c.get_absolute_url() == f"/item/{c.pid}/"
    # assert c.get_absolute_url() == f"/thesis/%22{c.title_html}%22-p/"

    c = CollectionFactory.build(coltype="lectures", title_html="LN1")
    assert c.get_absolute_url() == f"/item/{c.pid}/"
    # assert c.get_absolute_url() == f"/lectures/%22{c.title_html}%22-p/"

    c = CollectionFactory.build(coltype="book-series", title_html="BS1")
    assert c.get_absolute_url() == f"/item/{c.pid}/"
    # assert c.get_absolute_url() == f"/series/%22{c.title_html}%22-p/"

    assert c.get_wall() == 5
    assert c.get_collection() == c
    assert c.get_relative_folder() == c.pid


@pytest.mark.django_db
def test_container():
    c = CollectionFactory(pid="ALCO")
    p = PublisherFactory()

    assert str(p) == p.pub_key
    assert p.get_collection() is None
    assert p.get_top_collection() is None
    assert p.get_container() is None

    i1 = ContainerFactory(my_collection=c, my_publisher=p, seq=0, pid="PID1")

    assert i1.get_other_collections().count() == 0
    assert i1.get_volume() == ""
    assert i1.get_number() == "3"
    assert i1.get_year() == "2014"
    assert i1.is_edited_book() is False

    t = i1.get_citation(None)
    assert t == "Container1. Collection1, no. 3 (2014),  /item/PID1/"

    i3 = ContainerFactory(
        my_collection=c, my_publisher=p, seq=2, pid="PID3", vseries="1", volume="15"
    )
    ContributionFactory(resource=i3, last_name="A1", role="author")
    ContributionFactory(resource=i3, last_name="R1", role="redaktor")
    ResourceIdFactory(resource=i3)
    t = i3.get_citation(None)
    assert (
        t
        == "A1, John. Container1. Collection1, Serie 1, Volume 15 (2014) no. 3, R1, John (red.),  doi : 10.5802/foo.1. /item/PID3/"
    )

    i4 = ContainerFactory(my_collection=c, my_publisher=p, seq=2, pid="PID4", number="")
    ContributionFactory(resource=i4, last_name="E1", role="editor")
    ResourceCountFactory(resource=i4)
    t = i4.get_citation(None)
    assert t == "E1, John (ed.). Container1. Collection1 (2014), 10 p. /item/PID4/"

    assert not i1.has_detailed_info()
    assert i3.has_detailed_info()

    assert Container.objects.filter(pid="PID4").prefetch_all().first() == i4
    assert not i4.allow_crossref()

    ArticleFactory(lang="en", my_container=i4, doi="d1")
    c.doi = "col_doi"
    assert i4.allow_crossref()
    assert i4.are_all_articles_published()
    i4.year = "2019"
    assert i4.are_all_articles_published()
    i4.year = "0"
    assert not i4.are_all_articles_published()

    assert i4.get_base_url() == "/issue/"


@pytest.mark.django_db
def test_bibitem():
    b = BibItemFactory(size="10", chapter_title_tex="Chapter")
    ContributionFactory(resource=None, bibitem=b, last_name="A1", role="author")
    ContributionFactory(resource=None, bibitem=b, last_name="E1", role="editor")

    text = "|".join(b.get_bibtex())
    assert (
        text
        == "@article{bib1,|     author = {A1, John},|     editor = {E1, John},|     title = {Article title},|     chapter = {Chapter},|     journal = {Journal},|     publisher = {Publisher},|     address = {Grenoble},|     institution = {CNRS},|     series = {3},|     volume = {252},|     number = {12},|     year = {2012},|     pages = {2--5},|     pagetotal = {10},|     note = {Comment},|}"
    )

    assert str(b) == " - label"
    assert b.get_ref_id() == "bib1"


# @pytest.mark.django_db
# def test_person_fields():
#     person = PersonFactory.create(orcid="orcid1", idref="idref1", mid="ref1")
#     assert person.orcid == "orcid1"
#     assert person.idref == "idref1"
#     assert person.mid == "ref1"
#
#     person = PersonFactory.create(orcid="orcid2")
#     assert person.idref is None
#     assert person.mid is None
#
#     person = PersonFactory.create(idref="idref2")
#     assert person.orcid is None
#     assert person.mid is None
#
#     person = PersonFactory.create()
#     assert person.orcid is None
#     assert person.idref is None
#     assert person.mid is None
#
#
# @pytest.mark.django_db
# def test_person_unique_orcid():
#     person = PersonFactory.create(orcid="orcid1")
#
#     # orcid has to be unique
#     with pytest.raises(IntegrityError) as exc_info:
#         person = PersonFactory.create(orcid="orcid1")
#
#
# @pytest.mark.django_db
# def test_person_unique_idref():
#     person = PersonFactory.create(idref="idref1")
#
#     # idref has to be unique
#     with pytest.raises(IntegrityError) as exc_info:
#         person = PersonFactory.create(idref="idref1")
#
#
# @pytest.mark.django_db
# def test_person_unique_mid():
#     person = PersonFactory.create(mid="ref1")
#
#     # mid has to be unique
#     with pytest.raises(IntegrityError) as exc_info:
#         person = PersonFactory.create(mid="ref1")
