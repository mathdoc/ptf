from ptf.cmds.xml.ckeditor.ckeditor_parser import CkeditorParser


def test_ckeditor_parser():
    html_value = r'<p>Te&lt;st&nbsp;<span class="mathjax-formula">\(x = {-b \pm \sqrt{b^2-4ac} \over 2a}\)</span> done</p><ul><li>Item</li></ul><ol><li>Item 1<br />New line</li><li>&nbsp;</li></ol>'
    parser = CkeditorParser(html_value=html_value, mml_formulas="", ignore_p=True)
    result = parser.value_xml
    assert (
        result
        == 'Te&lt;st\xa0<inline-formula><alternatives><tex-math>$x = {-b \\pm \\sqrt{b^2-4ac} \\over 2a}$</tex-math></alternatives></inline-formula> done<list list-type="simple"><list-item><p>Item</p></list-item></list><list list-type="number"><list-item><p>Item 1<break/>New line</p></list-item><list-item><p>\xa0</p></list-item></list>'
    )

    html_value = r"<strong>Item</strong><em>text</em><sub>i</sub><sup>2</sup><a>title</a>"
    parser = CkeditorParser(html_value=html_value, mml_formulas="", ignore_p=True)
    result = parser.value_xml
    assert (
        result
        == r'<bold>Item</bold><italic>text</italic><sub>i</sub><sup>2</sup><ext-link ext-link-type="uri" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="title">title</ext-link>'
    )

    html_value = r'<strong></strong><em></em><a href="my_url"></a><a tag=""></a>'
    parser = CkeditorParser(html_value=html_value, mml_formulas="", ignore_p=True)
    result = parser.value_xml
    assert (
        result
        == r'<bold/><italic/><ext-link ext-link-type="uri" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="my_url"></ext-link><ext-link ext-link-type="uri" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href=""></ext-link>'
    )

    html_value = r'<span class="other">title</span>'
    parser = CkeditorParser(html_value=html_value, mml_formulas="", ignore_p=True)
    result = parser.value_xml
    assert result == "title"

    html_value = r"<span>title</span>"
    parser = CkeditorParser(html_value=html_value, mml_formulas="", ignore_p=True)
    result = parser.value_xml
    assert result == "title"

    html_value = r'<p><span class="mathjax-formula">x^2</span></p>'
    parser = CkeditorParser(html_value=html_value, mml_formulas="", ignore_p=False)
    result = parser.value_xml
    assert (
        result
        == """<p xml:space="preserve"><disp-formula xml:space="preserve">\n<alternatives><tex-math>$x^2$</tex-math></alternatives></disp-formula></p>"""
    )

    html_value = r'<p><span class="mathjax-formula">x^2</span></p>'
    mml_formulas = [r"<math><msup><mi>y</mi><mn>2</mn></msup><mo>+</mo><mi>c</mi></math>"]
    parser = CkeditorParser(html_value=html_value, mml_formulas=mml_formulas, ignore_p=True)
    result = parser.value_xml
    assert (
        result
        == """<disp-formula xml:space="preserve">\n<alternatives><math><msup><mi>y</mi><mn>2</mn></msup><mo>+</mo><mi>c</mi></math><tex-math>$x^2$</tex-math></alternatives></disp-formula>"""
    )

    html_value = r'<p>TEXT<span class="mathjax-formula">x^2</span></p>'
    mml_formulas = [
        r"<math><msup><mi>y</mi><mn>2</mn></msup><mo>+</mo><mi>c</mi></math>",
        r"<math><mi>a</mi><mo>+</mo><mi>b</mi></math>",
    ]
    parser = CkeditorParser(html_value=html_value, mml_formulas=mml_formulas, ignore_p=True)
    result = parser.value_xml
    assert (
        result
        == """TEXT<inline-formula><alternatives><math><msup><mi>y</mi><mn>2</mn></msup><mo>+</mo><mi>c</mi></math><tex-math>$x^2$</tex-math></alternatives></inline-formula>"""
    )
