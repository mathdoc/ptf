import datetime

from django.conf import settings
from django.test import TestCase
from django.test import override_settings
from django.test.client import RequestFactory

from ptf.model_helpers import get_article
from ptf.model_helpers import get_collection
from ptf.model_helpers import get_container


class RequestTestCase(TestCase):
    def setUp(self):
        settings.DEBUG = True
        self.factory = RequestFactory()

    @override_settings(RESOURCES_ROOT="tests/data/archive")
    def test_urls_pdf(self):
        print("** Test: test_urls_pdf **")

        # init de la base
        f = open("tests/data/xml/journal-sms.xml")
        body = f.read()
        f.close()

        # Ajout d'un journal

        response = self.client.post(
            "/upload/collections/", data=body, content_type="application/xml"
        )
        self.assertEqual(response.status_code, 201)

        f = open("tests/data/xml/issue.xml")
        body = f.read()
        f.close()

        # Ajout d'un issue

        response = self.client.post("/upload/issues/", data=body, content_type="application/xml")
        self.assertEqual(response.status_code, 201)

        ###################
        # test des URI
        ###################
        # pour une 'toc' en pdf
        issue = get_container("SMS_1969-1970__1_")
        uri = issue.get_binary_file_href_full_path("toc", "application/pdf", "")
        self.assertEqual(uri, settings.ISSUE_BASE_URL + "toc/SMS_1969-1970__1_.pdf")

        # pour un article
        article = get_article("SMS_1969-1970__1__A1_0")
        uri = article.get_binary_file_href_full_path("self", "image/x.djvu", "")
        self.assertEqual(uri, "/item/" + "SMS_1969-1970__1__A1_0.djvu")

        ###################
        # test de protection d'un fichier d'article via Moving Wall
        ###################

        # test d'un pdf autorisé (data/xml/issue.xml)
        response = self.client.get("/item/SMS_1969-1970__1__A1_0.pdf")
        self.assertEqual(response.status_code, 200)

        # test d'une mauvaise url
        response = self.client.get("/item/SMS_1969-1970_TOTO_1__A1_0.pRdf/")
        self.assertEqual(response.status_code, 404)

        f = open("tests/data/xml/journal.xml")
        body = f.read()
        f.close()

        # Ajout d'un journal pour le test suivant

        response = self.client.post(
            "/upload/collections/", data=body, content_type="application/xml"
        )
        self.assertEqual(response.status_code, 201)

        # test d'un djvu non autorisé
        f = open("tests/data/xml/issue_test_movingwall.xml")
        body = f.read()
        f.close()
        response = self.client.post("/upload/issues/", data=body, content_type="application/xml")
        self.assertEqual(response.status_code, 201)

        # on force l'embargo à (Année courante - 2015) + 1 car 2015 année de l'issue
        col_afst = get_collection("AFST")
        now = datetime.datetime.now()
        new_wall = (now.year - 2015) + 1
        col_afst.wall = new_wall
        col_afst.save()
        response = self.client.get("/item/AFST_2015_6_24_2_227_0.djvu")
        self.assertEqual(response.status_code, 403)
        self.assertInHTML(
            '<a href="/item/AFST_2015_6_24_2_227_0/">lien</a>', response.content.decode("utf-8")
        )
        ###################
        # test d'accès aux fichiers d'un issue
        ###################
        # test archive.numdam.org / issue / @ issue_id.pdf

        response = self.client.get("/item/SMS_1969-1970__1_.pdf")
        self.assertEqual(response.status_code, 200)
        # test archive.numdam.org/issue/toc/@issue_id.pdf

        response = self.client.get("/issue/toc/SMS_1969-1970__1_.djvu")
        self.assertEqual(response.status_code, 200)

        # TODO test   archive.numdam.org/issue/frontmatter/@issue_id.pdf
        # TODO test   archive.numdam.org/issue/backmatter/@issue_id.pdf
        # test Moving Wall
        response = self.client.get("/item/AFST_2015_6_24_2.pdf")
        self.assertEqual(response.status_code, 403)
        self.assertInHTML(
            '<a href="/item/AFST_2015_6_24_2/">lien</a>', response.content.decode("utf-8")
        )
