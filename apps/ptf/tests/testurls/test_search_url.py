from urllib.parse import urlencode

from django.conf import settings
from django.http import QueryDict
from django.test import TestCase
from django.test.client import RequestFactory

from ptf.solr.search_helpers import CleanSearchURL


class RequestTestCase(TestCase):
    def setUp(self):
        settings.DEBUG = True
        self.factory = RequestFactory()

        f = open("tests/data/xml/journal-sms.xml")
        body = f.read()
        f.close()
        # Ajout d'un journal
        self.client.post("/upload/collections/", data=body, content_type="application/xml")

        f = open("tests/data/xml/issue.xml")
        body = f.read()
        f.close()
        # Ajout d'un issue
        self.client.post("/upload/issues/", data=body, content_type="application/xml")

    def test_01_emptyform(self):
        print("** Test: test_empty search form **")
        response = self.client.post("/search?")
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, "/help/")

    def test_02_onlysearch_get(self):
        print("** Test: /search renvoie 400 **")

        ###########################################################
        #
        # si /search est appelé en get on renvoie 400 vers Bad request
        #
        ############################################################

        response = self.client.get("/search?")

        self.assertEqual(response.status_code, 400)

    def test_03_onlysearch_post(self):
        print("** Test: /search renvoie 302 **")

        ###########################################################
        #
        # si /search est appelé en post on renvoie 302 vers help
        #
        ############################################################

        response = self.client.post("/search?")

        self.assertEqual(response.status_code, 302)

    def test_04_decode_date_search(self):
        print("** test search between date1 et date2 **")
        _, querydict = CleanSearchURL.decode("[1045 TO 2017]-d", "/search")
        result = querydict.dict()
        self.assertEqual(result, {"q-l-0": "2017", "q-f-0": "1045", "qt0": "date"})

        response = self.client.get("/search?[1045 TO 2017]-d/")
        self.assertEqual(response.status_code, 200)

    def test_05_date_facet(self):
        print("** test date facet [1940 TO 2000] **")
        response = self.client.get("/search?*-[1940 TO 2000]-qo/")
        self.assertEqual(response.status_code, 200)
        response = self.client.get("/search?[TEST TO 2000]-o/")
        self.assertEqual(response.status_code, 400)

    def test_06_decode_complex_string(self):
        print("** test /search?*-'Séminaire Schützenberger'-qp")

        _, querydict = CleanSearchURL.decode("*-%22Séminaire Schützenberger%22-qp", "/search")
        self.assertEqual(
            querydict.dict(),
            {
                "q0": "*",
                "qt0": "all",
                "f": "collection_title_facet:%22Séminaire Schützenberger%22",
            },
        )

        response = self.client.get(r"/search?*-%22Séminaire Schützenberger%22-qp/")
        self.assertEqual(response.status_code, 200)
        response = self.client.get(
            r"/search?%22Sunyach, Christian%22-%221970%22-%22Séminaire"
            r" Analyse fonctionnelle (dit \%22Maurey-Schwartz\%22)%22-cyp/"
        )
        self.assertEqual(response.status_code, 200)
        response = self.client.get(r"/search?%22%22\%22-q/")
        self.assertEqual(response.status_code, 200)
        response = self.client.get(r"/search?%22-%22-q/")
        self.assertEqual(response.status_code, 200)

    def test_07_wrong_url_get(self):
        print("** test wrong url on get query: must return error 404")

        response = self.client.get(r"/search?Godement-\"1994\"-\"Ast/\?risque\"-qyp")
        self.assertEqual(response.status_code, 404)

    def test_08_wrong_url_post(self):
        print("** test wrong url on post query: must return error 302")

        response = self.client.post(r"/search?Godement-%221994%22-%22Ast/?_risque%22-qyp")
        self.assertEqual(response.status_code, 302)

    def test_09_decode_not_url(self):
        print("** test NOT url")
        search_qs = "groupe-anneau-qQ"
        _, queryDict = CleanSearchURL.decode(search_qs, "/search")
        self.assertEqual(
            queryDict.dict(),
            {"q0": "groupe", "qt0": "all", "q1": "anneau", "qt1": "all", "not1": "on"},
        )
        response = self.client.get(f"/search?{search_qs}/")
        self.assertEqual(response.status_code, 200)


class SearchUrlEncodingTestCase(TestCase):
    """
    The only test in this class does not require to be a Django test and could
    be moved to pure pytest test.
    However it tests the class CleanSearchURL so it makes sense to keep it here...
    """

    def test_01_encode_search_dict(self):
        """
        Tests the mapping of every query type (author, all, ...) to the correct
        "encoding".
        Tests the NOT search feature.
        Tests the filters (facets).
        """
        ## Query type mapping
        params_dict = {"qt0": "all", "q0": "complex string"}
        query_dict = QueryDict(urlencode(params_dict))
        query_string = CleanSearchURL.encode(query_dict)
        # `complex string-q`
        self.assertEqual(query_string, "complex%20string-q")

        params_dict = {"qt0": "author", "q0": "My author"}
        query_dict = QueryDict(urlencode(params_dict))
        query_string = CleanSearchURL.encode(query_dict)
        # `My author-a`
        self.assertEqual(query_string, "My%20author-a")

        params_dict = {"qt0": "author_ref", "q0": "My author"}
        query_dict = QueryDict(urlencode(params_dict))
        query_string = CleanSearchURL.encode(query_dict)
        # `My author-c`
        self.assertEqual(query_string, "My%20author-c")

        params_dict = {"qt0": "title", "q0": "My title"}
        query_dict = QueryDict(urlencode(params_dict))
        query_string = CleanSearchURL.encode(query_dict)
        # `My title-b`
        self.assertEqual(query_string, "My%20title-b")

        params_dict = {"qt0": "date", "q-f-0": "1980", "q-l-0": "2000"}
        query_dict = QueryDict(urlencode(params_dict))
        query_string = CleanSearchURL.encode(query_dict)
        # `[1980 TO 2000]-d``
        self.assertEqual(query_string, "%5B1980%20TO%202000%5D-d")

        params_dict = {"qt0": "date"}
        query_dict = QueryDict(urlencode(params_dict))
        query_string = CleanSearchURL.encode(query_dict)
        # `[* TO *]-d``
        self.assertEqual(query_string, "%5B%2A%20TO%20%2A%5D-d")

        params_dict = {"qt0": "references", "q0": "complex string"}
        query_dict = QueryDict(urlencode(params_dict))
        query_string = CleanSearchURL.encode(query_dict)
        # `complex string-f`
        self.assertEqual(query_string, "complex%20string-f")

        params_dict = {"qt0": "body", "q0": "complex string"}
        query_dict = QueryDict(urlencode(params_dict))
        query_string = CleanSearchURL.encode(query_dict)
        # `complex string-g`
        self.assertEqual(query_string, "complex%20string-g")

        params_dict = {"qt0": "kwd", "q0": "complex string"}
        query_dict = QueryDict(urlencode(params_dict))
        query_string = CleanSearchURL.encode(query_dict)
        # `complex string-k`
        self.assertEqual(query_string, "complex%20string-k")

        ## NOT feature
        params_dict = {"qt0": "all", "q0": "complex string", "not0": "on"}
        query_dict = QueryDict(urlencode(params_dict))
        query_string = CleanSearchURL.encode(query_dict)
        # `complex string-Q`
        self.assertEqual(query_string, "complex%20string-Q")

        ## Complex query string
        # The second NOT filter is not applied because its value is != 'on'
        params_dict = {
            "qt0": "all",
            "q0": "complex string",
            "not0": "on",
            "qt1": "author",
            "q1": "My author",
            "not1": "anything",
        }
        query_dict = QueryDict(urlencode(params_dict))
        query_string = CleanSearchURL.encode(query_dict)
        # `complex string-k`
        self.assertEqual(query_string, "complex%20string-My%20author-Qa")

        ## Facets
        # groupe%20de-Julie-%22Sorin%20Dumitrescu%22-%5B2020%20TO%202023%5D-qQno
        params_dict = {
            "qt0": "all",
            "q0": "complex string",
            "f": [
                "collection_title_facet:Revue 1",
                "ar:My author",
                "year_facet:[1980 TO 2000]",
                "year_facet:1980",
                "{!tag=firstletter}firstNameFacetLetter:a",
                "dt:anything",
                "msc_facet:anything",
            ],
            "page": 3,
        }
        query_dict = QueryDict(urlencode(params_dict, True))
        query_string = CleanSearchURL.encode(query_dict)
        # `complex string-Revue 1-My author-[1980 TO 2000]-1980-a-anything-anything-3-qpnoyrsmt`
        self.assertEqual(
            query_string,
            "complex%20string-Revue%201-My%20author-%5B1980%20TO%202000%5D-1980-a-anything-anything-3-qpnoyrsmt",
        )
