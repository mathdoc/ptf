import json

from django.conf import settings
from django.test import TestCase
from django.test.client import RequestFactory
from django.urls import reverse

from ...cmds import solr_cmds
from ...models import Article
from ...models import Collection
from ...models import Container

XML_JOURNAL_SMS_PATH = "tests/data/xml/journal-sms.xml"
XML_ISSUE_SMS_PATH = "tests/data/xml/issue.xml"
XML_COLLECTION_PATH = "tests/data/xml/collection.xml"
XML_COLLECTION_MSF_PATH = "tests/data/xml/collection-msmf.xml"
XML_BOOK_MONO_PATH = "tests/data/xml/book-monograph-with-toc.xml"
XML_BOOK_MULTI_PATH = "tests/data/xml/book-with-part.xml"


class RequestTestCase(TestCase):
    def get_solr(self):
        return solr_cmds.solrFactory.get_solr()

    def setUp(self):
        settings.DEBUG = True
        self.factory = RequestFactory()

        solr_cmds.solrFactory.solr_url = settings.SOLR_URL

        self.get_solr().delete(q="*:*")

    def solrSearch(self, searchStr):
        return self.get_solr().search(searchStr)

    def tearDown(self):
        self.get_solr().delete(q="*:*")

    def _post_new_journal(self, xml_path=XML_JOURNAL_SMS_PATH):
        with open(xml_path) as xml_file:
            return self.client.post(
                reverse("upload-serials"), data=xml_file.read(), content_type="application/xml"
            )

    def _post_new_issue(self, xml_path=XML_ISSUE_SMS_PATH):
        with open(xml_path) as xml_file:
            return self.client.post(
                reverse("issue_upload"), data=xml_file.read(), content_type="application/xml"
            )

    def _post_new_book(self, xml_path):
        with open(xml_path) as xml_file:
            return self.client.post(
                reverse("book_upload"), data=xml_file.read(), content_type="application/xml"
            )


class BasicTestCase(RequestTestCase):
    def test_02_wrong_url(self):
        print("** test 02_wrong_url ")
        response = self.client.get("/volume/(/")
        self.assertEqual(response.status_code, 404)

    def test_03_container_without_relatedObject(self):
        print("** test 03_container_without_relatedObjec")
        response = self.client.get("/issue/frontmatter/MSMF_1993_2_52_.pdf/")
        self.assertEqual(response.status_code, 404)

    def test_00_authors_wrong_pagination(self):
        print("** test 04_bug_recherche_auteurs ")
        response = self.client.get(
            "/authors?103-t/"
        )  # 103 correspond à la nième page résultat QueryDict
        self.assertEqual(response.status_code, 404)


class JournalRequestTestCase(RequestTestCase):
    def test_404__status_without_journal(self):
        print("** test 404__status_without_journal")
        response = self.client.get(reverse("collection_status", kwargs={"colid": "SMS"}))
        self.assertTrue(response.status_code == 404)

    def test_ok__add_new_journal(self):
        print("** test ok__add_new_journal")
        response = self._post_new_journal()
        self.assertEqual(response.status_code, 201)

        queryset = Collection.objects.filter(pid="SMS")
        self.assertTrue(queryset.exists())
        self.assertTrue(queryset.count() == 1)
        journal = queryset.get()
        self.assertEqual(journal.title_tex, "Séminaire Schützenberger")

    def test_ok__status_with_journal(self):
        print("** test ok__status_with_journal")
        self._post_new_journal()
        response = self.client.get(reverse("collection_status", kwargs={"colid": "SMS"}))
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content.decode("utf-8"))
        self.assertEqual(content["status"], "200")
        self.assertEqual(content["pid"], "SMS")

    def test_err__add_duplicate_journal(self):
        print("** test err__add_duplicate_journal")
        self._post_new_journal()
        response = self._post_new_journal()
        self.assertEqual(response.status_code, 409)


class IssueRequestTestCase(RequestTestCase):
    def setUp(self):
        super().setUp()
        self._post_new_journal()

    def test_404__status_without_issues(self):
        print("** test 404__status_without_issues")
        response = self.client.get(reverse("issue_status", kwargs={"iid": "SMS_1969-1970__1_"}))
        self.assertTrue(response.status_code == 404)

    def test_ok__add_new_issue(self):
        print("** test ok__add_new_issue")
        response = self._post_new_issue()
        self.assertEqual(response.status_code, 201)
        self.assertEqual(Container.objects.count(), 1)
        issue = Container.objects.all()[0]
        self.assertEqual(issue.pid, "SMS_1969-1970__1_")
        self.assertEqual(Article.objects.count(), 2)

    def test_ok__status_with_issue(self):
        print("** test ok__status_with_issue")
        self._post_new_issue()
        response = self.client.get(reverse("issue_status", kwargs={"iid": "SMS_1969-1970__1_"}))
        content = json.loads(response.content.decode("utf-8"))
        self.assertEqual(content["status"], "200")
        self.assertEqual(content["pid"], "SMS_1969-1970__1_")

    def test_err__add_duplicate_issue(self):
        print("** test err__add_duplicate_issue")
        self._post_new_issue()
        response = self._post_new_issue()
        self.assertEqual(response.status_code, 201)


class BookTestCase(RequestTestCase):
    def setUp(self):
        super().setUp()
        self._post_new_journal(XML_COLLECTION_PATH)
        self._post_new_journal(XML_COLLECTION_MSF_PATH)

    def test_ok__add_new_mono_book(self):
        print("** test ok__add_new_mono_book")
        response = self._post_new_book(XML_BOOK_MONO_PATH)
        self.assertEqual(response.status_code, 201)
        queryset = Container.objects.filter(pid="CIF_1970-1971__3__1_0")
        self.assertTrue(queryset.exists())
        book = queryset.get()
        self.assertEqual(book.ctype, "book-monograph")

    def test_ok__add_new_multi_book(self):
        print("** test ok__add_new_multi_book")
        response = self._post_new_book(XML_BOOK_MULTI_PATH)
        self.assertEqual(response.status_code, 201)
        queryset = Container.objects.filter(pid="MSMF_1993_2_52_")
        self.assertTrue(queryset.exists())
        book = queryset.get()
        self.assertEqual(book.ctype, "book-edited-book")

    def test_ok__articles_created_with_multi_book(self):
        print("** test ok__articles_created_with_multi_book")
        self._post_new_book(XML_BOOK_MULTI_PATH)
        article = Article.objects.get(pid="MSMF_1993_2_52__1_0")
        self.assertEqual(article.datastream_set.count(), 2)
