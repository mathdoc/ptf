from pysolr import Results

from ptf.solr.search_helpers import create_facets_in_category


def test_category_facets():
    path = "/search"
    data = ["1980", 2, "1975", 1, "1990", 3]
    category = "year_facet"
    active_filters = set()
    solr_results = Results({})

    setattr(solr_results, "facets", {"facet_fields": {"anything_facet": data}})
    category_facets = create_facets_in_category(solr_results, category, active_filters, path)
    assert len(category_facets) == 0

    # Test facet sorting and default
    setattr(solr_results, "facets", {"facet_fields": {category: data}})
    category_facets = create_facets_in_category(solr_results, category, active_filters, path)
    assert len(category_facets) == 3
    assert category_facets[0].name == "1980"
    assert category_facets[1].name == "1975"
    assert category_facets[2].name == "1990"

    category_facets = create_facets_in_category(
        solr_results, category, active_filters, path, sort=True, reverse=True
    )
    assert len(category_facets) == 3
    assert category_facets[0].name == "1990"
    assert category_facets[1].name == "1980"
    assert category_facets[2].name == "1975"

    # Test site facet
    # crmath & crphys
    data = ["26", 4, "28", 18]
    category = "sites"
    setattr(solr_results, "facets", {"facet_fields": {category: data}})
    category_facets = create_facets_in_category(solr_results, category, active_filters, path)
    assert len(category_facets) == 2
    assert category_facets[0].name == "Mathématique"
    assert category_facets[1].name == "Physique"

    # Test filtering
    data = ["26", 4, "28", 18]
    category = "sites"
    setattr(solr_results, "facets", {"facet_fields": {category: data}})
    active_filters = {'sites:"28"'}
    category_facets = create_facets_in_category(solr_results, category, active_filters, path)
    assert len(category_facets) == 2
    assert category_facets[0].active == "not-active"
    assert category_facets[1].active == "active"

    data = ["Author 1", 4, "Author 2", 18]
    category = "ar"
    setattr(solr_results, "facets", {"facet_fields": {category: data}})
    active_filters = {'ar:"Author 1"'}
    category_facets = create_facets_in_category(solr_results, category, active_filters, path)
    assert len(category_facets) == 2
    assert category_facets[0].active == "active"
    assert category_facets[0].name == "Author 1"
    assert category_facets[0].href == "/search"
    assert category_facets[1].active == "not-active"
    assert category_facets[1].name == "Author 2"
    assert category_facets[1].href == "/search&f=ar%3A%22Author+2%22"
