import datetime

from django.conf import settings
from django.test import Client
from django.test import TestCase
from django.urls import reverse

from ptf.models import Article
from ptf.models import Collection
from ptf.models import Container
from ptf.models import Contribution
from ptf.models import PtfSite
from ptf.models import Publisher
from ptf.models import SiteMembership


class ExportBiblioTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        col = Collection.objects.create(
            pid="PID",
            title_tex="Titre de la collection",
            coltype="journal",
            alive=True,
            fyear=1949,
            lyear=2022,
        )

        container1 = Container.objects.create(
            my_collection=col,
            pid="PID_2022__24_1",
            lang="fr",
            trans_lang="und",
            ctype="issue",
            year="2022",
            number="1",
            volume="24",
            volume_int=24,
            number_int=1,
            seq=0,
            last_modified=datetime.datetime(2022, 7, 13, 12, 56, 19, 100, tzinfo=datetime.UTC),
        )

        article = Article.objects.create(
            my_container=container1,
            seq=0,
            pseq=0,
            fpage=101,
            lpage=106,
            pid="PID_2022__24_1_888_8",
            doi="10.5802/gh.56",
            lang="en",
            title_tex="Article title",
            trans_lang="fr",
            trans_title_tex="Translated article title",
        )

        publisher = Publisher.objects.create(
            pub_key=1,
            pub_name="Publisher_name",
            pub_loc="Grenoble",
        )

        container2 = Container.objects.create(
            my_collection=col,
            my_publisher=publisher,
            pid="PID_2014__3_",
            title_tex="Titre",
            lang="fr",
            ctype="book-edited-book",
            year="2014",
            number="3",
            number_int=3,
            vseries_int=0,
            seq=0,
            last_modified=datetime.datetime(2014, 7, 13, 12, 56, 19, 100, tzinfo=datetime.UTC),
        )

        incollection = Article.objects.create(
            my_container=container2,
            seq=1,
            pseq=1,
            fpage=1,
            lpage=10,
            pid="PID_2014__3__B_0",
            doi="10.5802/mbk.999999",
            lang="fr",
            title_tex="Article title",
        )

        site = PtfSite.objects.get(id=settings.SITE_ID)
        SiteMembership.objects.create(resource=article, site=site)
        SiteMembership.objects.create(resource=incollection, site=site)

        containers = []
        containers.append(container1)
        containers.append(container2)
        for container in containers:
            SiteMembership.objects.create(resource=container, site=site)

        number_of_authors = 2
        for author_id in range(number_of_authors):
            Contribution.objects.create(
                resource=article,
                seq=author_id,
                first_name=f"First_name {author_id}",
                last_name=f"Last_name {author_id}",
                role="author",
            )

        for resource in containers + [article]:
            number_of_editors = 2
            for editor_id in range(number_of_editors):
                Contribution.objects.create(
                    resource=resource,
                    seq=editor_id,
                    first_name=f"First_name {editor_id}",
                    last_name=f"Last_name {editor_id}",
                    role="editor",
                )

        cls.article = {}
        with open("tests/data/export_citation/article.bib") as f:
            cls.article["bib"] = f.read().splitlines()
        with open("tests/data/export_citation/article.ris") as f:
            cls.article["ris"] = f.read().splitlines()
        with open("tests/data/export_citation/article.enw") as f:
            cls.article["enw"] = f.read().splitlines()

        cls.book = {}
        with open("tests/data/export_citation/book.bib") as f:
            cls.book["bib"] = f.read().splitlines()
        with open("tests/data/export_citation/book.ris") as f:
            cls.book["ris"] = f.read().splitlines()
        with open("tests/data/export_citation/book.enw") as f:
            cls.book["enw"] = f.read().splitlines()

        cls.incollection = {}
        with open("tests/data/export_citation/incollection.bib") as f:
            cls.incollection["bib"] = f.read().splitlines()
        with open("tests/data/export_citation/incollection.ris") as f:
            cls.incollection["ris"] = f.read().splitlines()
        with open("tests/data/export_citation/incollection.enw") as f:
            cls.incollection["enw"] = f.read().splitlines()

    def setUp(self):
        self.client = Client()

    def test_01_export_citation_article(self):
        for ext, data in self.article.items():
            params = {"pid": "PID_2022__24_1_888_8", "ext": ext}
            response = self.client.get(reverse("export_citation", kwargs=params))
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.content.decode().splitlines(), data)

    def test_02_export_citation_book(self):
        for ext, data in self.book.items():
            params = {"pid": "PID_2014__3_", "ext": ext}
            response = self.client.get(reverse("export_citation", kwargs=params))
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.content.decode().splitlines(), data)

    def test_03_export_citation_incollection(self):
        for ext, data in self.incollection.items():
            params = {"pid": "PID_2014__3__B_0", "ext": ext}
            response = self.client.get(reverse("export_citation", kwargs=params))
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.content.decode().splitlines(), data)
