import pytest

from django.conf import settings
from django.test import Client
from django.urls import reverse

from ptf.cmds.solr_cmds import addArticleSolrCmd
from ptf.cmds.solr_cmds import solrDeleteCmd
from ptf.model_data_converter import db_to_article_data

from ..factories import AbstractFactory
from ..factories import ArticleWithSiteFactory
from ..factories import AwardFactory
from ..factories import BibItemFactory
from ..factories import CollectionFactory
from ..factories import CollectionWithSiteFactory
from ..factories import ContainerFactory
from ..factories import ContainerWithSiteFactory
from ..factories import ContributionFactory
from ..factories import ExtIdFactory
from ..factories import KwdFactory
from ..factories import PublisherFactory
from ..views import get_suggested_articles


@pytest.mark.django_db
def test_api_articles():
    ArticleWithSiteFactory()
    response = Client().get(reverse("api-articles"))
    assert response.status_code == 200
    assert response.content.decode() == '{"articles": {"ids": ["pid1"], "total": 1}}'


@pytest.mark.django_db
def test_api_article_dump():
    article = ArticleWithSiteFactory()
    AbstractFactory(resource=article)
    AwardFactory(resource=article)
    KwdFactory(resource=article)
    ExtIdFactory(resource=article)
    b = BibItemFactory(resource=article)
    ContributionFactory(resource=article, bibitem=b)
    response = Client().get(reverse("api-article-dump", kwargs={"pid": article.pid}))
    assert response.status_code == 200
    r = response.content.decode()
    assert (
        r
        == '{"title_tex": "", "title_html": "", "trans_title_tex": "", "trans_title_html": "", "lang": "und", "doi": "10.1111/site.1", "pid": "pid1", "authors": "Smith, John", "issue_pid": "PID_2014__3_", "colid": "PID", "volume": "", "number": "3", "year": "2014", "issue_title": "Container1", "citation": "Smith, John. . Collection1, no. 3 (2014). doi : 10.1111/site.1. http://testserver/articles/10.1111/site.1/", "date_accepted": "2023-01-01", "date_online_first": "2023-01-01", "date_published": "2023-01-01", "date_received": null, "date_revised": null, "page_count": 0, "body_html": "", "pci_section": "", "extids": [["zbl-item-id", "1111.11111"]], "kwds": [{"type": "", "lang": "en", "value": "kw1"}], "awards": [["award1", "1"]], "abstracts": [{"lang": "en", "value_tex": "abstract1", "value_html": ""}], "bibitems": [""]}'
    )


@pytest.mark.django_db
def test_api_book_dump():
    book = ContainerWithSiteFactory()
    AbstractFactory(resource=book)
    b = BibItemFactory(resource=book, chapter_title_tex="Chapter")
    ContributionFactory(resource=book, bibitem=b)
    ExtIdFactory(resource=book)
    response = Client().get(reverse("api-book-dump", kwargs={"pid": book.pid}))
    assert response.status_code == 200
    assert (
        response.content.decode()
        == '{"title_tex": "Container1", "title_html": "", "doi": "", "citation": "Smith, John. Container1. Collection1, no. 3 (2014),  /item/PID_2014__3_/", "extids": [["zbl-item-id", "1111.11111"]], "kwds": [], "abstracts": [{"lang": "en", "value_tex": "abstract1", "value_html": ""}], "bibitems": [""]}'
    )


@pytest.mark.django_db
def test_api_citedby():
    article = ArticleWithSiteFactory()
    try:
        response = Client().get(reverse("api_citedby", kwargs={"doi": article.doi}))
    except Exception:
        pass
    else:
        assert response.status_code == 200


@pytest.mark.django_db
def test_api_export_article_html():
    article = ArticleWithSiteFactory()
    response = Client().get(reverse("export_article_html", kwargs={"doi": article.doi}))
    assert response.status_code == 200
    assert response.content.decode() == ""


@pytest.mark.django_db
def test_api_articles_recents():
    article = ArticleWithSiteFactory(title_html="Article title")
    ContributionFactory(resource=article)
    response = Client().get(reverse("api-articles-recents"))
    assert response.status_code == 200
    assert (
        response.content.decode()
        == '{"articles": [{"title": "Article title", "authors": "Smith, John", "collection": "Collection1", "doi": "10.1111/site.1", "citation_source": ", no. 3 (2014).", "url": "https://doi.org/10.1111/site.1"}]}'
    )


@pytest.mark.django_db
def test_mlt_article():
    article = ArticleWithSiteFactory()
    site_id = settings.SITE_ID
    params = {
        "id": article.id,
        "pid": article.pid,
        "db_obj": article,
        "sites": [site_id],
        "xobj": db_to_article_data(article),
    }
    cmd = addArticleSolrCmd(params)
    cmd.add_collection(article.get_collection())
    cmd.set_container(article.my_container)
    cmd.do()
    response = Client().get(reverse("mlt_article", kwargs={"doi": article.doi}))
    assert response.status_code == 200
    assert response.json() == {
        "params": {
            "debugQuery": "true",
            "fl": "*,score",
            "fq": "pid:/pid1.*/",
            "min_score": 80,
            "mlt.boost": "true",
            "mlt.fl": "all",
            "mlt.interestingTerms": "details",
            "mlt.maxdfpct": 1,
            "mlt.maxqt": 50,
            "mlt.maxwl": 100,
            "mlt.mindf": 2,
            "mlt.mintf": 2,
            "mlt.minwl": 4,
            "q": "id:4",
        },
        "docs": [],
        "numFound": 0,
        "interestingTerms": [],
        "explain": {},
    }

    response = Client().get(reverse("mlt_article", kwargs={"doi": "10.2222/a2"}))
    assert response.status_code == 404


@pytest.mark.django_db
def test_search():
    response = Client().get("/search?*-[1940 TO 2000]-qo&eprint=False/")
    assert response.status_code == 200

    response = Client().get("/search?*-[1940 TO 2000]-qo&eprint=zz/")
    assert response.status_code == 200

    response = Client().post("/search?")
    assert response.status_code == 302
    assert response.url == "/help/"

    params = {"q0": [r"*"], "qt0": [r"all"], "f": [r"year_facet:[1940 TO 2000]"]}
    response = Client().post("/search?", params)
    assert response.status_code == 302


@pytest.mark.django_db
def test_item_view():
    col = CollectionFactory(coltype="lectures")
    c = ContainerWithSiteFactory(my_collection=col, pid="pid1", ctype="issue")
    url = reverse("item_id", kwargs={"pid": c.pid})
    response = Client().get(url)
    assert response.status_code == 200
    assert response.context["coltype"] == "lectures"
    assert response.context["obj"] == c

    col = CollectionFactory(coltype="book-series")
    c = ContainerWithSiteFactory(my_collection=col, pid="pid2")
    url = reverse("item_id", kwargs={"pid": c.pid})
    response = Client().get(url)
    assert response.status_code == 200
    assert response.context["coltype"] == "book-series"
    assert response.context["obj"] == c

    col = CollectionFactory(coltype="book-series")
    c = ContainerWithSiteFactory(my_collection=col, pid="pid3", ctype="book-monograph")
    url = reverse("item_id", kwargs={"pid": c.pid})
    response = Client().get(url)
    assert response.status_code == 200
    assert response.context["coltype"] == "book-series"
    assert response.context["obj"] == c

    col = CollectionFactory(coltype="book-series")
    c = ContainerWithSiteFactory(my_collection=col, pid="pid4", ctype="lecture-notes")
    url = reverse("item_id", kwargs={"pid": c.pid})
    response = Client().get(url)
    assert response.status_code == 200
    assert response.context["coltype"] == "book-series"
    assert response.context["obj"] == c

    # TODO: change the SITE_NAME is not safe. Other tests might fail. The original value should be restored
    settings.SITE_NAME = "crchim"
    col = CollectionFactory(coltype="lectures", pid="CRCHIM")
    c = ContainerWithSiteFactory(
        my_collection=col, pid="pid6", ctype="issue", with_online_first=True
    )
    url = reverse("item_id", kwargs={"pid": c.pid})
    response = Client().get(url)
    assert response.status_code == 200
    assert response.context["coltype"] == "lectures"
    assert response.context["obj"] == c

    settings.SITE_NAME = "ALCO"
    for n, coltype in enumerate(["journal", "acta", "book-series"]):
        col = CollectionWithSiteFactory(coltype=coltype, pid=f"col{n}")
        ContainerWithSiteFactory(my_collection=col)
        url = reverse("item_id", kwargs={"pid": col.pid})
        response = Client().get(url)
        assert response.status_code == 200
        assert response.context["coltype"] == coltype


@pytest.mark.django_db
def test_volume_items():
    col = CollectionFactory(coltype="lectures")
    c = ContainerWithSiteFactory(my_collection=col, pid="pid1")
    url = reverse("volume-items", kwargs={"vid": c.pid})
    response = Client().get(url)
    assert response.status_code == 200
    assert response.context["coltype"] == "lectures"
    assert response.context["collection"] == col
    assert response.context["issue"] == c

    settings.SITE_NAME = "crchim"
    col = CollectionFactory(coltype="lectures", pid="CRCHIM")
    c = ContainerWithSiteFactory(my_collection=col, pid="pid2")
    url = reverse("volume-items", kwargs={"vid": c.pid})
    response = Client().get(url)
    assert response.status_code == 200
    assert response.context["coltype"] == "lectures"
    assert response.context["collection"] == col
    assert response.context["issue"] == c


@pytest.mark.django_db
def test_api_graphical_abstract():
    kwargs = {"doi": "10.1111/a1"}
    response = Client().post(reverse("api-graphical-abstract", kwargs=kwargs))
    assert response.status_code == 404

    article = ArticleWithSiteFactory()
    kwargs = {"doi": article.doi}
    response = Client().post(reverse("api-graphical-abstract", kwargs=kwargs))
    assert response.status_code == 200
    assert response.content.decode() == '{"message": "OK"}'


@pytest.mark.django_db
def test_api_update_suggest():
    kwargs = {"doi": "10.1111/a1"}
    response = Client().post(reverse("api-update-suggest", kwargs=kwargs))
    assert response.status_code == 200
    assert response.content.decode() == '{"message": "No article found"}'

    article = ArticleWithSiteFactory()
    kwargs = {"doi": article.doi}
    response = Client().post(reverse("api-update-suggest", kwargs=kwargs))
    assert response.status_code == 200
    assert response.content.decode() == '{"message": "OK"}'


@pytest.mark.django_db
def test_article_html():
    settings.SITE_NAME = "crchim"
    col = CollectionFactory(pid="CRCHIM")
    p = PublisherFactory()
    c1 = ContainerWithSiteFactory(my_collection=col, my_publisher=p, with_online_first=True)
    ArticleWithSiteFactory(my_container=c1, pid="CRCHIM_0", doi="10.1111/1")
    response = Client().get(reverse("article", kwargs={"aid": "10.1111/1"}))
    assert response.status_code == 200


@pytest.mark.django_db
def test_article_html2():
    settings.SITE_NAME = "crbiol"
    col = CollectionFactory(pid="CRBIOL")
    p = PublisherFactory()
    c1 = ContainerWithSiteFactory(my_collection=col, my_publisher=p, year=2023)
    ArticleWithSiteFactory(my_container=c1, pid="CRBIOL_0", doi="10.1111/2")
    response = Client().get(reverse("article", kwargs={"aid": "10.1111/2"}))
    assert response.status_code == 200


@pytest.mark.django_db
def test_get_suggested_articles():
    settings.SITE_NAME = "crchim"
    col = CollectionFactory(pid="CRCHIM")
    c1 = ContainerWithSiteFactory(my_collection=col)
    solrDeleteCmd({"q": "pid:*"}).do()
    site_id = settings.SITE_ID

    for x in range(4):
        article = ArticleWithSiteFactory(my_container=c1, pid=f"CRCHIM_{x}", doi=f"10.1111/{x}")
        article.title_tex = f"Electronic structure of 2D van der Waals crystals and heterostructures investigated by spatially and angle resolved photoemission {x}"
        xobj = db_to_article_data(article)
        params = {
            "id": article.id,
            "doi": article.doi,
            "pid": article.pid,
            "db_obj": article,
            "sites": [site_id],
            "xobj": xobj,
        }
        cmd = addArticleSolrCmd(params)
        cmd.add_collection(article.my_container.my_collection)
        cmd.set_container(article.my_container)
        cmd.do()
    assert get_suggested_articles(article) == []


@pytest.mark.django_db
def test_latest_issue():
    col = CollectionFactory(pid=settings.COLLECTION_PID)
    ContainerFactory(my_collection=col)
    response = Client().get(reverse("latest_issue", kwargs={"pid": col.pid}))
    assert response.status_code == 302


@pytest.mark.django_db
def test_api_item_xml():
    article = ArticleWithSiteFactory()
    response = Client().get(reverse("api-item-xml", kwargs={"pid": article.pid}))
    assert response.status_code == 200
    response = Client().get(reverse("api-item-xml", kwargs={"pid": article.doi}))
    assert response.status_code == 200


@pytest.mark.django_db
def test_api_issues():
    col = CollectionFactory()
    response = Client().get(reverse("api-issues", kwargs={"colid": col.pid}))
    assert response.status_code == 200


@pytest.mark.django_db
def test_api_issue_list():
    article = ArticleWithSiteFactory()
    response = Client().get(reverse("api-issue-list", kwargs={"pid": article.pid}))
    assert response.status_code == 200


@pytest.mark.django_db
def test_api_collection_export_csv():
    article = ArticleWithSiteFactory()
    colid = article.my_container.my_collection
    response = Client().get(reverse("api_collection_export_csv", kwargs={"colid": colid}))
    assert response.status_code == 200
    assert (
        response.content.decode()
        == """doi\ttitle\tdate_accepted\tdate_first_publication\r\n"=LIEN.HYPERTEXTE(""https://doi.org/10.1111/site.1""; ""10.1111/site.1"")"\t\t2023-01-01\t2023-01-01\r\n"""
    )


@pytest.mark.django_db
def test_api_all_issues():
    response = Client().get(reverse("api-all-issues"))
    assert response.status_code == 200


@pytest.mark.django_db
def test_api_item_file_list():
    article = ArticleWithSiteFactory()
    response = Client().get(reverse("api-item-file-list", kwargs={"pid": article.pid}))
    assert response.status_code == 200


@pytest.mark.django_db
def test_api_all_collections():
    CollectionFactory()
    response = Client().get(reverse("api-all-collections"))
    assert response.status_code == 200
