from django.conf import settings
from django.test import TestCase
from django.test import override_settings
from django.test.client import RequestFactory

from ...cmds import solr_cmds
from ...cmds import xml_cmds
from ...display import resolver
from ...models import Abstract
from ...models import Article
from ...models import BibItem
from ...models import BibItemId
from ...models import Container
from ...models import Contribution
from ...models import Kwd
from ...models import Publisher


class DisplayResolverTestCase(TestCase):
    def get_solr(self):
        return solr_cmds.solrFactory.get_solr()

    def setUp(self):
        solr_cmds.solrFactory.solr_url = settings.SOLR_URL
        self.get_solr().delete(q="*:*")
        self.factory = RequestFactory()

    def tearDown(self):
        self.get_solr().delete(q="*:*")

    def solrSearch(self, searchStr):
        return self.get_solr().search(searchStr)

    def check_empty_database(self):
        # self.assertEqual(Collection.objects.count(), 0)
        self.assertEqual(Container.objects.count(), 0)
        self.assertEqual(Article.objects.count(), 0)
        self.assertEqual(BibItem.objects.count(), 0)
        self.assertEqual(Abstract.objects.count(), 0)
        self.assertEqual(Contribution.objects.count(), 0)
        self.assertEqual(Kwd.objects.count(), 0)
        self.assertEqual(Publisher.objects.count(), 0)

    def test_01_resolver_id(self):
        print("** test_01_ptf.display.resolver.resolver_id")

        self.check_empty_database()

        f = open("tests/data/xml/collection-jedp.xml")
        body = f.read()
        f.close()
        cmd = xml_cmds.addCollectionsXmlCmd({"body": body})
        cmd.do()

        f = open("tests/data/xml/issue-jedp.xml")
        body = f.read()
        f.close()

        cmd = xml_cmds.addIssueXmlCmd({"body": body})
        cmd.do()

        article = Article.objects.get(doi="10.5802/jedp.615")

        # 1 generic ext-link, 1 eid
        bib = BibItem.objects.get(resource__pid=article.pid, sequence=1)
        # self.assertEqual(bib.citation_html, ' (2009), Research Paper 21 <a href="http://www.toto.org" target="_blank">toto</a>')

        bibitemid = BibItemId.objects.get(bibitem=bib, id_type="zbl-item-id")
        self.assertEqual(bibitemid.get_href(), "https://zbmath.org/?q=an:1016.35018")
        bibitemid = BibItemId.objects.get(bibitem=bib, id_type="mr-item-id")
        self.assertEqual(
            bibitemid.get_href(), "https://mathscinet.ams.org/mathscinet-getitem?mr=1893845"
        )

        bib = BibItem.objects.get(resource__pid=article.pid, sequence=8)
        bibitemid = BibItemId.objects.get(bibitem=bib, id_type="mathdoc-id")
        self.assertEqual(bibitemid.get_href(), "/item/AIHPA_1987__46_1_113_0")

        article2 = Article.objects.get(doi="10.5802/jedp.627")
        bib = BibItem.objects.get(resource__pid=article2.pid, sequence=11)
        bibitemid = BibItemId.objects.get(bibitem=bib, id_type="eudml-item-id")
        self.assertEqual(bibitemid.get_href(), "https://eudml.org/doc/39561")

    #       TODO id_type == 'sps-id', 'arxiv', 'hal', 'tel','theses.fr',"orcid","semantic-scholar"

    def test_02_find_id_type(self):
        print("** test_02_ptf.display.resolver.find_id_type")
        self.assertEqual(resolver.find_id_type("10.5802/jedp.615"), "doi")
        self.assertEqual(resolver.find_id_type("hal-01164777v1"), "hal")
        self.assertEqual(resolver.find_id_type("arXiv:2303.04687"), "arxiv")

    def test_02_get_mimetype(self):
        print("** test_02_ptf.display.resolver.get_mimetype")
        self.assertEqual(resolver.get_mimetype("MonArtile.PDF"), "application/pdf")

    def test_03_get_disk_location(self):
        print("** test_03_ptf.display.resolver.get_disk_location")
        self.assertEqual(
            resolver.get_disk_location(
                "/mersenne_test_date", "JEDP", "pdf", "JEDP_2003___", "JEDP_2003____A1_0", False
            ),
            "/mersenne_test_date/JEDP/JEDP_2003___/JEDP_2003____A1_0/JEDP_2003____A1_0.pdf",
        )

    @override_settings(RESOURCES_ROOT="tests/data/archive")
    def test_04_get_bibtex_from_tex(self):
        print("** test_04_ptf.display.resolver.get_bibtex_from_tex")
        self.assertEqual(
            resolver.get_bibtex_from_tex(
                "tests/data/archive/AIF/AIF_2015__65_6/2012089_LamEF/2012089_LamEF.tex"
            ),
            "Lam",
        )
