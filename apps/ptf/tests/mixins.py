from django.contrib.messages.middleware import MessageMiddleware
from django.contrib.sessions.middleware import SessionMiddleware
from django.test import RequestFactory


class MessageDependentTestMixin:
    """
    Usefull class when one needs to emulate HTTPRequest with messages, without
    using the Django test client.
    """

    def setUpMessageData(self) -> None:
        self.request_factory = RequestFactory()
        dummy_func = lambda x: x
        self.session_middleware = SessionMiddleware(dummy_func)
        self.message_middleware = MessageMiddleware(dummy_func)

    def prepare_request(self, request):
        self.session_middleware.process_request(request)
        self.message_middleware.process_request(request)

    def get_request(self, url: str):
        request = self.request_factory.get(url)
        self.prepare_request(request)
        return request
