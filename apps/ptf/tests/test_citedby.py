import json

from django.conf import settings
from django.test import TestCase

from ptf.citedby import built_citations
from ptf.citedby import citedby_ads_refs
from ptf.citedby import citedby_crossref_refs
from ptf.citedby import citedby_semantic_refs
from ptf.citedby import citedby_zbmath_refs


class CitedbyTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        with open("tests/data/citedby/crossref.json", encoding="utf8") as f:
            cls.data_crossref = json.load(f)
        with open("tests/data/citedby/zbmath.json", encoding="utf8") as f:
            cls.data_zbmath = json.load(f)
        with open("tests/data/citedby/ads.json", encoding="utf8") as f:
            cls.data_ads = json.load(f)
        with open("tests/data/citedby/semantic.json", encoding="utf8") as f:
            cls.data_semantic = json.load(f)
        with open("tests/data/citedby/built_citations.json", encoding="utf8") as f:
            cls.results = json.load(f)

        settings.CROSSREF_USER = ""
        settings.CROSSREF_PWD = ""
        settings.ADS_TOKEN = ""

    def test_citedby_executors(self):
        data = []
        data.extend(citedby_crossref_refs(self.data_crossref))
        data.extend(citedby_zbmath_refs(self.data_zbmath))
        data.extend(citedby_ads_refs(self.data_ads))
        data.extend(citedby_semantic_refs(self.data_semantic))
        self.assertEqual(built_citations(data)[0], self.results[0])
