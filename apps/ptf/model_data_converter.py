##################################################################################################
#
# README
#
# Operations on the xml data objects
#     Django DB -> Data objects
#
##################################################################################################

import types

from django.db.models import Q

from ptf.cmds.xml.citation_html import get_citation_html
from ptf.cmds.xml.jats.builder.issue import get_single_title_xml
from ptf.cmds.xml.jats.builder.issue import get_title_xml
from ptf.cmds.xml.xml_base import RefBase
from ptf.cmds.xml.xml_utils import escape
from ptf.cmds.xml.xml_utils import get_contrib_xml
from ptf.model_data import ArticleData
from ptf.model_data import BookData
from ptf.model_data import BookPartData
from ptf.model_data import Foo
from ptf.model_data import IssueData
from ptf.model_data import JournalData
from ptf.model_data import MathdocPublicationData
from ptf.model_data import PublisherData
from ptf.model_data import RefData
from ptf.model_data import create_contributor


def db_append_obj_with_location_to_list(resource_qs, data_list):
    for obj_with_location in resource_qs:
        data = {
            "rel": obj_with_location.rel,
            "mimetype": obj_with_location.mimetype,
            "location": obj_with_location.location,
            "base": obj_with_location.base.base if obj_with_location.base else "",
        }
        # 'seq': obj_with_location.seq}

        for attr in ["metadata", "text", "caption"]:
            if hasattr(obj_with_location, attr):
                data[attr] = getattr(obj_with_location, attr)

        data_list.append(data)


def db_to_contributors(qs):
    contributors = []
    for contribution in qs.all():
        contributor = create_contributor()

        contributor["first_name"] = contribution.first_name
        contributor["last_name"] = contribution.last_name
        contributor["prefix"] = contribution.prefix
        contributor["suffix"] = contribution.suffix
        contributor["orcid"] = contribution.orcid if contribution.orcid else ""
        contributor["email"] = contribution.email
        contributor["string_name"] = contribution.string_name
        contributor["mid"] = contribution.mid if contribution.mid else ""
        contributor["addresses"] = [
            contrib_address.address for contrib_address in contribution.contribaddress_set.all()
        ]
        contributor["role"] = contribution.role
        contributor["deceased_before_publication"] = contribution.deceased_before_publication
        contributor["equal_contrib"] = contribution.equal_contrib
        contributor["corresponding"] = contribution.corresponding
        contributor["contrib_xml"] = contribution.contrib_xml
        # contributor["author_id"] = contribution.id

        contributors.append(contributor)

    return contributors


def db_to_resource_data_common(resource, data_resource):
    data_resource.pid = resource.pid
    data_resource.doi = resource.doi

    data_resource.lang = resource.lang
    data_resource.title_xml = resource.title_xml
    data_resource.title_tex = resource.title_tex
    data_resource.title_html = resource.title_html
    data_resource.abbrev = resource.abbrev

    data_resource.trans_lang = resource.trans_lang
    data_resource.trans_title_tex = resource.trans_title_tex
    data_resource.trans_title_html = resource.trans_title_html

    data_resource.funding_statement_xml = resource.funding_statement_xml
    data_resource.funding_statement_html = resource.funding_statement_html
    data_resource.footnotes_xml = resource.footnotes_xml
    data_resource.footnotes_html = resource.footnotes_html

    data_resource.ids = [(id.id_type, id.id_value) for id in resource.resourceid_set.all()]
    data_resource.extids = [(extid.id_type, extid.id_value) for extid in resource.extid_set.all()]

    db_append_obj_with_location_to_list(resource.extlink_set.all(), data_resource.ext_links)
    db_append_obj_with_location_to_list(resource.datastream_set.all(), data_resource.streams)
    db_append_obj_with_location_to_list(
        resource.relatedobject_set.all(), data_resource.related_objects
    )

    # Ignore related_objects and figures: they are updated by the FullText import after the Cedrics import
    # db_append_obj_with_location_to_list(resource.relatedobject_set.all(),
    #                                     data_resource.related_objects)
    # db_append_obj_with_location_to_list(resource.relatedobject_set.filter(rel='html-image'),
    #                                     data_resource.figures)
    db_append_obj_with_location_to_list(
        resource.relatedobject_set.filter(Q(rel="supplementary-material") | Q(rel="review")),
        data_resource.supplementary_materials,
    )

    data_resource.counts = [
        (count.name, count.value) for count in resource.resourcecount_set.all()
    ]

    data_resource.contributors = db_to_contributors(resource.contributions)

    data_resource.kwds = [
        {"type": kwd.type, "lang": kwd.lang, "value": kwd.value} for kwd in resource.kwd_set.all()
    ]
    data_resource.subjs = [
        {"type": subj.type, "lang": subj.lang, "value": subj.value}
        for subj in resource.subj_set.all()
    ]

    data_resource.abstracts = [
        {
            "tag": abstract.tag,
            "lang": abstract.lang,
            "value_xml": abstract.value_xml,
            "value_tex": abstract.value_tex,
            "value_html": abstract.value_html,
        }
        for abstract in resource.abstract_set.all()
    ]

    data_resource.awards = [
        {"abbrev": award.abbrev, "award_id": award.award_id} for award in resource.award_set.all()
    ]

    for relation in resource.subject_of.all():
        obj = Foo()
        obj.rel_type = relation.rel_info.left
        obj.id_value = relation.object_pid
        data_resource.relations.append(obj)

    for relation in resource.object_of.all():
        obj = Foo()
        obj.rel_type = relation.rel_info.right
        obj.id_value = relation.subject_pid
        data_resource.relations.append(obj)
    if hasattr(resource, "issn"):
        data_resource.issn = resource.issn
    if hasattr(resource, "e_issn"):
        data_resource.e_issn = resource.e_issn


def db_to_publisher_data(publisher):
    data_publisher = PublisherData()

    data_publisher.name = publisher.pub_name
    data_publisher.loc = publisher.pub_loc

    # TODO: ext_links ?
    data_publisher.ext_links = []

    return data_publisher


def db_to_publication_data(collection):
    data_col = MathdocPublicationData()

    db_to_resource_data_common(collection, data_col)

    data_col.coltype = collection.coltype
    data_col.wall = collection.wall
    data_col.issn = collection.issn
    data_col.e_issn = collection.e_issn

    return data_col


def db_to_journal_data(collection):
    data_journal = JournalData()

    # A JournalData has no coltype ?

    # A JournalData has a publisher but it does not seem to be used anywhere ?
    # The publisher seems to belong to the issue/article and not to the Journal.

    db_to_resource_data_common(collection, data_journal)
    return data_journal


def db_to_collection_data(collection):
    data_col = MathdocPublicationData()

    db_to_resource_data_common(collection, data_col)

    data_col.coltype = collection.coltype
    data_col.issn = collection.issn
    data_col.e_issn = collection.e_issn

    # attributes used for CollectionMembership
    if hasattr(collection, "vseries"):
        data_col.vseries = collection.vseries
    if hasattr(collection, "volume"):
        data_col.volume = collection.volume
    if hasattr(collection, "seq"):
        data_col.seq = collection.seq

    return data_col


def db_to_issue_data(container, articles=None):
    data_issue = IssueData()

    db_to_resource_data_common(container, data_issue)

    data_issue.ctype = container.ctype

    data_issue.year = container.year
    data_issue.vseries = container.vseries
    data_issue.volume = container.volume
    data_issue.number = container.number

    data_issue.last_modified_iso_8601_date_str = (
        container.last_modified.isoformat() if container.last_modified else ""
    )
    data_issue.prod_deployed_date_iso_8601_date_str = (
        container.deployed_date().isoformat() if container.deployed_date() else ""
    )

    data_issue.journal = db_to_journal_data(container.my_collection)
    data_issue.publisher = db_to_publisher_data(container.my_publisher)
    data_issue.provider = container.provider.name

    # a Container has a seq, but it is used only for the books collections

    # articles may have been prefetched / filtered before
    if not articles:
        articles = container.article_set.all()

    for article in articles:
        data_article = db_to_article_data(article)
        data_issue.articles.append(data_article)

    return data_issue


def db_to_book_data(container):
    data_book = BookData()

    db_to_resource_data_common(container, data_book)

    data_book.ctype = container.ctype
    setattr(data_book, "year", container.year)

    data_book.publisher = db_to_publisher_data(container.my_publisher)
    data_book.provider = container.provider

    data_col = db_to_collection_data(container.my_collection)
    # These attributes are required when adding a container to solr
    if not hasattr(data_col, "vseries"):
        setattr(data_col, "vseries", 0)
    if not hasattr(data_col, "volume"):
        setattr(data_col, "volume", 0)
    data_book.incollection.append(data_col)
    for collection in container.my_other_collections.all():
        data_col = db_to_collection_data(container.my_collection)
        data_book.incollection.append(data_col)

    if hasattr(container, "frontmatter") and container.frontmatter is not None:
        data_book.frontmatter_xml = container.frontmatter.value_xml
        data_book.frontmatter_toc_html = container.frontmatter.value_html
        data_book.frontmatter_foreword_html = container.frontmatter.foreword_html
    data_book.body = container.get_body()

    data_book.last_modified_iso_8601_date_str = (
        container.last_modified.isoformat() if container.last_modified else ""
    )
    data_book.prod_deployed_date_iso_8601_date_str = (
        container.deployed_date().isoformat() if container.deployed_date() else ""
    )

    for bookpart in container.article_set.all():
        data_bookpart = db_to_bookpart_data(bookpart)
        data_book.parts.append(data_bookpart)

    for bibitem in container.bibitem_set.all():
        data_ref = db_to_ref_data(bibitem, data_book.lang)
        data_book.bibitems.append(data_ref)
        data_book.bibitem.append(data_ref.citation_html)

    return data_book


def db_to_article_data(article):
    data_article = ArticleData()

    db_to_resource_data_common(article, data_article)

    data_article.atype = article.atype
    data_article.seq = str(article.seq)

    data_article.fpage = article.fpage
    data_article.lpage = article.lpage
    data_article.page_range = article.page_range
    data_article.page_type = article.page_type

    data_article.article_number = article.article_number
    data_article.talk_number = article.talk_number
    data_article.elocation = article.elocation
    data_article.coi_statement = article.coi_statement if article.coi_statement else ""

    data_article.date_published_iso_8601_date_str = (
        article.date_published.isoformat() if article.date_published else ""
    )
    data_article.prod_deployed_date_iso_8601_date_str = (
        article.deployed_date().isoformat()
        if article.my_container and article.deployed_date()
        else ""
    )

    data_article.history_dates = [
        {"type": type, "date": date.isoformat()}
        for type, date in [
            ("received", article.date_received),
            ("revised", article.date_revised),
            ("accepted", article.date_accepted),
            ("online", article.date_online_first),
        ]
        if date
    ]

    data_article.body = article.get_body()
    data_article.body_html = article.body_html
    data_article.body_tex = article.body_tex
    data_article.body_xml = article.body_xml

    for bibitem in article.bibitem_set.all():
        data_ref = db_to_ref_data(bibitem, "und")
        data_article.bibitems.append(data_ref)
        data_article.bibitem.append(data_ref.citation_html)

    for trans_article in article.translations.all():
        trans_data_article = db_to_article_data(trans_article)
        data_article.translations.append(trans_data_article)

    return data_article


def db_to_bookpart_data(article):
    data_bookpart = BookPartData()

    db_to_resource_data_common(article, data_bookpart)

    data_bookpart.atype = article.atype

    data_bookpart.fpage = article.fpage
    data_bookpart.lpage = article.lpage
    data_bookpart.page_range = article.page_range
    data_bookpart.page_type = article.page_type

    if hasattr(article, "frontmatter") and article.frontmatter is not None:
        data_bookpart.frontmatter_xml = article.frontmatter.value_xml
        data_bookpart.frontmatter_toc_html = article.frontmatter.value_html
        data_bookpart.frontmatter_foreword_html = article.frontmatter.foreword_html
    data_bookpart.body = article.get_body()

    for bibitem in article.bibitem_set.all():
        data_ref = db_to_ref_data(bibitem, data_bookpart.lang)
        data_bookpart.bibitems.append(data_ref)
        data_bookpart.bibitem.append(data_ref.citation_html)

    return data_bookpart


def db_to_ref_data(bibitem, lang):
    data_ref = RefData(lang=lang)

    data_ref.type = bibitem.type
    data_ref.user_id = bibitem.user_id
    data_ref.label = bibitem.label

    data_ref.citation_xml = bibitem.citation_xml
    data_ref.citation_tex = bibitem.citation_tex
    data_ref.citation_html = bibitem.citation_html

    data_ref.publisher_name = bibitem.publisher_name
    data_ref.publisher_loc = bibitem.publisher_loc

    data_ref.article_title_tex = bibitem.article_title_tex
    data_ref.chapter_title_tex = bibitem.chapter_title_tex
    data_ref.institution = bibitem.institution
    data_ref.series = bibitem.series
    data_ref.volume = bibitem.volume
    data_ref.issue = bibitem.issue
    data_ref.month = bibitem.month
    data_ref.year = bibitem.year
    data_ref.comment = bibitem.comment
    data_ref.annotation = bibitem.annotation
    data_ref.fpage = bibitem.fpage
    data_ref.lpage = bibitem.lpage
    data_ref.page_range = bibitem.page_range
    data_ref.size = bibitem.size
    data_ref.source_tex = bibitem.source_tex

    data_ref.extids = [
        (bibitemid.id_type, bibitemid.id_value) for bibitemid in bibitem.bibitemid_set.all()
    ]

    data_ref.contributors = db_to_contributors(bibitem.contributions)

    return data_ref


def jats_from_ref_comment(ref):
    attr = getattr(ref, "comment")
    if attr is None:
        return ""

    text = ""
    start = attr.find("http://")
    if start == -1:
        start = attr.find("https://")

    if start != -1:
        end = attr.find(" ", start)
        if end == -1:
            url = escape(attr[start:])
        else:
            url = escape(attr[start:end])

        text = escape(attr[0:start])
        text += f'<ext-link xlink:href="{url}">{url}</ext-link>'

        if end != -1:
            text += escape(attr[end + 1 :])
    else:
        text = escape(attr)

    text = f'<comment xml:space="preserve">{text}</comment>'

    return text


def jats_from_ref_attr(
    ref,
    attr_name,
    jats_tag="",
    preserve=False,
    attr_type=None,
    attr_type_value="",
    convert_html_tag=False,
):
    if not hasattr(ref, attr_name):
        return ""

    text = ""
    attr = getattr(ref, attr_name)
    if len(jats_tag) == 0:
        jats_tag = attr_name
    if attr and preserve:
        value = get_single_title_xml(attr) if convert_html_tag else escape(attr)
        if attr_type is not None:
            text = f'<{jats_tag} {attr_type}="{attr_type_value}" xml:space="preserve">{escape(attr)}</{jats_tag}>'
        else:
            text = f'<{jats_tag} xml:space="preserve">{value}</{jats_tag}>'
    elif attr:
        value = get_single_title_xml(attr) if convert_html_tag else escape(attr)
        if attr_type is not None:
            text = f'<{jats_tag} {attr_type}="{attr_type_value}">{value}</{jats_tag}>'
        else:
            text = f"<{jats_tag}>{escape(attr)}</{jats_tag}>"

    return text


def jats_from_abstract(abstract_lang, article_lang, abstract):
    if abstract_lang == article_lang:
        return f'<abstract xml:lang="{article_lang}">{abstract.value_xml}</abstract>'
    else:
        return f'<trans-abstract xml:lang="{abstract_lang}">{abstract.value_xml}</trans-abstract>'


def jats_from_ref(ref):
    text = ""
    authors = ref.get_authors()
    if authors is not None:
        text += "".join([author["contrib_xml"] for author in authors])

    text += jats_from_ref_attr(
        ref, "article_title_tex", "article-title", preserve=True, convert_html_tag=True
    )
    text += jats_from_ref_attr(ref, "chapter_title_tex", "chapter-title", convert_html_tag=True)
    text += jats_from_ref_attr(ref, "source_tex", "source", preserve=True, convert_html_tag=True)

    editors = ref.get_editors()
    if editors is not None:
        text += "".join([editor["contrib_xml"] for editor in editors])

    text += jats_from_ref_attr(ref, "series", preserve=True)
    text += jats_from_ref_attr(ref, "volume")
    text += jats_from_ref_attr(ref, "publisher_name", "publisher-name")
    text += jats_from_ref_attr(ref, "publisher_loc", "publisher-loc")
    text += jats_from_ref_attr(ref, "institution")
    text += jats_from_ref_attr(ref, "year")
    text += jats_from_ref_attr(ref, "issue")
    text += jats_from_ref_attr(
        ref, "doi", "pub-id", attr_type="pub-id-type", attr_type_value="doi"
    )
    text += jats_from_ref_attr(ref, "fpage")
    text += jats_from_ref_attr(ref, "lpage")
    text += jats_from_ref_attr(ref, "size", "size")
    text += jats_from_ref_comment(ref)

    return text


def update_ref_data_for_jats(ref, i, with_label=True):
    """
    Set with_label=False if you do not want a label in the citation_html (for example in the citedby)
    """

    if hasattr(ref, "eid") and ref.eid is not None and ref.eid != "":
        eids = [item for item in ref.extids if item[0] == "eid"]
        if len(eids) > 0:
            ref.extids.remove(eids[0])
        ref.extids.append(("eid", ref.eid))

    label = ref.label
    if not label and with_label:
        label = f"[{i}]"
        ref.label = label

    if ref.type == "unknown":
        if not ref.citation_html:
            if with_label and ref.citation_tex.find(label) != 0:
                ref.citation_html = f"{label} {ref.citation_tex}"
            else:
                ref.citation_html = ref.citation_tex

        if not ref.citation_xml:
            ref.citation_xml = f'<label>{escape(ref.label)}</label><mixed-citation xml:space="preserve">{ref.citation_tex}</mixed_ciation>'
    else:
        ref.label = f"{label}" if with_label else ""
        # ref can be a Munch dictionary, or a RefData object.
        # Add RefBase member functions, like get_authors
        # ref_base = RefBase(lang='und')
        # ref_base.from_dict(ref)
        ref.get_authors = types.MethodType(RefBase.get_authors, ref)
        ref.get_editors = types.MethodType(RefBase.get_editors, ref)
        text = get_citation_html(ref)
        ref.citation_html = ref.citation_tex = text

    for contrib in ref.contributors:
        contrib["contrib_xml"] = get_contrib_xml(contrib, is_ref=True)

    if ref.type != "unknown":
        element_citation = jats_from_ref(ref)
        ref.citation_xml = f'<label>{escape(ref.label)}</label><element-citation publication-type="{ref.type}">{element_citation}</element-citation>'


def update_data_for_jats(data_article, create_author_if_empty=False, with_label=True):
    if not data_article.title_html:
        data_article.title_html = data_article.title_tex
    if not data_article.trans_title_html:
        data_article.trans_title_html = data_article.trans_title_tex
    if not data_article.title_xml:
        data_article.title_xml = get_title_xml(
            data_article.title_tex, data_article.trans_title_tex, data_article.trans_lang
        )

    for contrib in data_article.contributors:
        contrib["contrib_xml"] = get_contrib_xml(contrib)

    if data_article.doi is not None:
        value = ("doi", data_article.doi)
        if value not in data_article.ids:
            data_article.ids.append(value)

    if create_author_if_empty and len(data_article.contributors) == 0:
        contrib = create_contributor()
        contrib["role"] = "author"
        contrib["contrib_xml"] = get_contrib_xml(contrib)
        data_article.contributors = [contrib]

    for i, ref in enumerate(data_article.bibitems, start=1):
        update_ref_data_for_jats(ref, i, with_label=with_label)

    for trans_data_article in data_article.translations:
        update_data_for_jats(trans_data_article, create_author_if_empty, with_label)


def convert_refdata_for_editor(ref):
    contribs_text = "\n".join(
        [f"{contrib['last_name']}, {contrib['first_name']}" for contrib in ref.contributors]
    )
    ref.contribs_text = contribs_text

    if not ref.article_title_tex and not ref.chapter_title_tex and not ref.source_tex:
        ref.type = "unknown"

    ref.doi = ""
    for extid in ref.extids:
        if extid[0] == "doi":
            ref.doi = extid[1]
        elif extid[0] == "eid":
            ref.eid = extid[1]
    # URLs are in <comment>
    # ref.url = ''
    # for ext_link in ref.ext_links:
    #     if ext_link['link_type'] == '':
    #         ref.url = ext_link['location']
