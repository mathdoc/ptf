from unidecode import unidecode

from django.conf import settings
from django.db.models import Q
from django.utils import timezone

from ptf import exceptions
from ptf import model_helpers

from ..models import Abstract
from ..models import Article
from ..models import Author
from ..models import Award
from ..models import BibItem
from ..models import BibItemId
from ..models import Collection
from ..models import CollectionMembership
from ..models import Container
from ..models import Contrib
from ..models import ContribAddress
from ..models import Contribution
from ..models import DataStream
from ..models import ExtId
from ..models import ExtLink
from ..models import FrontMatter
from ..models import Kwd
from ..models import MetaDataPart
from ..models import Provider
from ..models import PtfSite
from ..models import Publisher
from ..models import RelatedObject
from ..models import RelationName
from ..models import Relationship
from ..models import ResourceCount
from ..models import ResourceId
from ..models import ResourceInSpecialIssue
from ..models import SiteMembership
from ..models import Stats
from ..models import Subj
from ..models import SupplementaryMaterial
from ..models import TranslatedArticle
from ..models import XmlBase
from ..models import parse_page_count
from .base_cmds import baseCmd
from .base_cmds import make_int

# from .xml.jats import jats_parser

# TODO: call full_clean before each .save() call to validate a Model object
# It requires to update models.py and add some blank=True to fields that can be null/empty
# Example: number and vseries for a Container


def get_first_letter(name):
    letter = ""
    if name:
        letter = name[0]
        letter = unidecode(letter)
        if len(letter) > 1:
            letter = letter[0]
        letter = letter.upper()
        if letter == "?":
            letter = "Z"

        # with   't Hooft, G   => first_letter is H
        if name.startswith("'t") and len(name) > 3:
            letter = name[3].upper()

    return letter


def add_contributors(contributors, resource=None, bibitem=None):
    if resource is None and bibitem is None:
        raise RuntimeError("add_contributors: resource and bibitem are None")

    seq = 0
    for contrib in contributors:
        seq += 1

        contrib["seq"] = seq
        contribution_fields_list = Contribution.get_fields_list()
        contributions_fields = {
            key: contrib[key] for key in contribution_fields_list if key in contrib
        }

        contribution = Contribution(resource=resource, bibitem=bibitem, **contributions_fields)
        contribution.save()

        for addr in contrib["addresses"]:
            contrib_address = ContribAddress(contribution=contribution, address=addr)
            contrib_address.save()

        if bibitem is None and contribution.role == "author" and not contribution.is_etal():
            ref_name = contribution.mid if contribution.mid else str(contribution)
            try:
                author = Author.objects.get(name=ref_name)
                author.count += 1
            except Author.DoesNotExist:
                letter = ref_name[0]
                letter = unidecode(letter)
                if len(letter) > 1:
                    letter = letter[0]
                letter = letter.upper()
                if letter == "?":
                    letter = "Z"

                # with   't Hooft, G   => first_letter is H
                if ref_name.startswith("'t") and len(ref_name) > 3:
                    letter = ref_name[3].upper()

                author = Author(name=ref_name, first_letter=letter, count=1)
            author.save()


def add_counts(xobj, resource, add_total=False):
    seq = 1
    for name, value in xobj.counts:
        try:
            ResourceCount.objects.get(resource=resource, name=name)
            raise exceptions.ResourceExists(f"The ResourceCount {name} already exists")
        except ResourceCount.DoesNotExist:
            resource_count = ResourceCount(resource=resource, name=name, seq=seq, value=value)

            resource_count.save()

            if name == "page-count" and add_total:
                value = parse_page_count(value)

                try:
                    total = Stats.objects.get(name=name)
                    total.value += value
                except Stats.DoesNotExist:
                    total = Stats(name=name, value=value)

                total.save()

        seq += 1


def add_biblio(xobj, resource):
    for seq, xbibitem in enumerate(xobj.bibitems, start=1):
        bibitem = BibItem(
            resource=resource,
            sequence=seq,
            label=xbibitem.label,
            citation_xml=xbibitem.citation_xml,
            citation_tex=xbibitem.citation_tex,
            citation_html=xbibitem.citation_html,
            type=xbibitem.type,
            user_id=xbibitem.user_id,
            article_title_tex=xbibitem.article_title_tex,
            chapter_title_tex=xbibitem.chapter_title_tex,
            source_tex=xbibitem.source_tex,
            publisher_name=xbibitem.publisher_name,
            publisher_loc=xbibitem.publisher_loc,
            institution=xbibitem.institution,
            series=xbibitem.series,
            volume=xbibitem.volume,
            issue=xbibitem.issue,
            month=xbibitem.month,
            year=xbibitem.year,
            comment=xbibitem.comment,
            annotation=xbibitem.annotation,
            fpage=xbibitem.fpage,
            lpage=xbibitem.lpage,
            page_range=xbibitem.page_range,
            size=xbibitem.size,
        )

        bibitem.save()

        add_contributors(contributors=xbibitem.contributors, bibitem=bibitem)

        if xbibitem.extids:
            the_types = []
            for id_type, id_value in xbibitem.extids:
                if id_type and id_type not in the_types:
                    the_types.append(id_type)

                    bibitemid = BibItemId(
                        bibitem=bibitem,
                        id_type=id_type,
                        id_value=id_value,
                        checked=True,
                        false_positive=False,
                    )

                    bibitemid.save()


def add_relations(xobj, resource):
    for relation in xobj.relations:
        with_doi = "10." in relation.id_value
        if with_doi and resource.doi is None:
            raise ValueError(
                f"The article {resource.pid} has no DOI but uses the relation {relation.rel_type} with the DOI {relation.id_value}"
            )

        # First, we need to find if the relation is stored in the RelationName as a left or right attribute
        name = relation.rel_type
        relationname = model_helpers.get_relationname_left(left_name=name)

        if relationname:
            # RelationName with a left attribute (ex "follows"):
            #    the subject of the relationship is the xml parent (typically the article defined by the xml)
            #    the object is the id declared by the <related-article>
            # Example <article>
            #             <article_id ...>A</article-id>
            #             <related-article ... related-article-type="follows">B</related-article>
            #    => RelationShip with subject = A and object = B

            subject_pid = resource.doi if with_doi else resource.pid
            subject_resource = resource
            object_pid = relation.id_value
            # Find the resource to update the related article
            # But to work during a regular import, the related article must already be in the database
            # Depending on the import order, it might not work
            if with_doi:
                object_resource = model_helpers.get_article_by_doi(relation.id_value)
            else:
                object_resource = model_helpers.get_article(relation.id_value)
        else:
            relationname = model_helpers.get_relationname_right(right_name=name)

            if relationname:
                # RelationName with a right attribute (ex "followed-by"):
                #    the subject of the relationship is the id declared by the <related-article>
                #    the object is the xml parent (typically the article defined by the xml)
                # Example <article>
                #             <article_id ...>A</article-id>
                #             <related-article ... related-article-type="followed-by">B</related-article>
                #    => RelationShip with subject = B and object = A

                subject_pid = relation.id_value
                # Find the resource to update the related article
                # But to work during a regular import, the related article must already be in the database
                # Depending on the import order, it might not work
                if with_doi:
                    subject_resource = model_helpers.get_article_by_doi(relation.id_value)
                else:
                    subject_resource = model_helpers.get_article(relation.id_value)

                object_pid = resource.doi if with_doi else resource.pid
                object_resource = resource

        # Elsevier creates basic relation. Create the relationname on the fly if necessary
        if not relationname and name == "refers to":
            relationname = RelationName(
                left="refers to",
                right="is referenced by",
                gauche="fait référence à",
                droite="est référencé par",
            )
            relationname.save()

            subject_pid = resource.doi if with_doi else resource.pid
            subject_resource = resource
            object_pid = relation.id_value
            # Find the resource to update the related article
            # But to work during a regular import, the related article must already be in the database
            # Depending on the import order, it might not work
            if with_doi:
                object_resource = model_helpers.get_article_by_doi(relation.id_value)
            else:
                object_resource = model_helpers.get_article(relation.id_value)

        if relationname:
            params = {"subject_pid": subject_pid, "object_pid": object_pid, "solr_commit": False}
            cmd = addRelationshipDatabaseCmd(params)
            if subject_resource is not None:
                cmd.set_subject_resource(subject_resource)
            if object_resource is not None:
                cmd.set_object_resource(object_resource)
            cmd.set_relationname(relationname)

            try:
                relationship = Relationship.objects.get(
                    subject_pid=subject_pid, object_pid=object_pid, rel_info=relationname
                )
                # la première fois que l'on crée la relation une des 2 ressources ID est a priori None
                # la deuxième fois (la relation symétrique) il faut compléter la
                # RelationShip
                if relationship.resource is not None and relationship.related is not None:
                    raise exceptions.ResourceExists(
                        f"The Relationship {subject_pid} {relationname.left} {object_pid} already exists"
                    )
                if subject_resource is not None:
                    relationship.resource = subject_resource
                if object_resource is not None:
                    relationship.related = object_resource
                relationship.save()

            except Relationship.DoesNotExist:
                relationship = Relationship(
                    subject_pid=subject_pid,
                    object_pid=object_pid,
                    resource=subject_resource,
                    related=object_resource,
                    rel_info=relationname,
                )
                relationship.save()


def add_frontmatter(xobj, resource):
    if hasattr(xobj, "frontmatter_xml") and xobj.frontmatter_xml is not None:
        frontmatter = FrontMatter(
            resource=resource,
            value_xml=xobj.frontmatter_xml,
            value_html=xobj.frontmatter_toc_html,
            foreword_html=xobj.frontmatter_foreword_html,
        )
        frontmatter.save()


def add_publisher(xobj):
    if hasattr(xobj, "publisher") and xobj.publisher:
        publisher = model_helpers.get_publisher(xobj.publisher.name)

        if publisher is None:
            cmd = addPublisherDatabaseCmd({"xobj": xobj.publisher})
            publisher = cmd.do()

        return publisher


class addSiteDatabaseCmd(baseCmd):
    """
    addSiteDatabaseCmd: adds/remove a PtfSite

    Exception raised:
       - ValueError if the init params are empty
       - exceptions.ResourceExists during do if the site already exists
       - exceptions.ResourceDoesNotExist during undo if the site does not exist
       - RuntimeError during undo if resources are still deployed
    """

    def __init__(self, params=None):
        self.site_name = None
        self.site_domain = None
        self.site_id = None

        super().__init__(params)

        if not self.site_name:
            self.site_name = settings.SITE_NAME

        if not self.site_domain:
            self.site_domain = settings.SITE_DOMAIN

        self.required_params.extend(["site_name", "site_domain", "site_id"])

    # Returns a PtfSite
    def internal_do(self) -> PtfSite:
        super().internal_do()

        try:
            PtfSite.objects.get(name=self.site_name)
            raise exceptions.ResourceExists(
                "The site " + self.site_name + " " + self.site_domain + " already exists"
            )
        except PtfSite.DoesNotExist:
            site = PtfSite(
                domain=self.site_domain, name=self.site_name, acro=self.site_name, id=self.site_id
            )
            site.save()

        return site

    def pre_undo(self):
        super().pre_undo()

        if SiteMembership.objects.filter(site__name=self.site_name).exists():
            raise RuntimeError(
                "Impossible de supprimer le site car il y a encore des ressources publiees"
            )

    def internal_undo(self):
        super().internal_undo()

        try:
            # may throw PtfSite.DoesNotExist
            site = PtfSite.objects.get(name=self.site_name)
            id = site.id
            site.delete()
        except PtfSite.DoesNotExist:
            raise exceptions.ResourceDoesNotExist("The site " + self.site_name + " does not exist")

        return id


class addProviderDatabaseCmd(baseCmd):
    """
    addProviderDatabaseCmd: adds/remove a Provider

    Exception raised:
       - ValueError if the init params are empty
       - exceptions.ResourceExists during do if the provider already exists
       - exceptions.ResourceDoesNotExist during undo if the provider does not exist
    """

    def __init__(self, params=None):
        self.name = None
        self.pid_type = None
        self.sid_type = None

        super().__init__(params)

        self.required_params.extend(["name", "pid_type"])

    def internal_do(self):
        super().internal_do()

        try:
            Provider.objects.get(name=self.name, pid_type=self.pid_type, sid_type=self.sid_type)
            raise exceptions.ResourceExists("The provider " + self.name + " already exists")
        except Provider.DoesNotExist:
            p = Provider(name=self.name, pid_type=self.pid_type, sid_type=self.sid_type)
            p.save()

        return p

    def internal_undo(self):
        super().internal_undo()

        try:
            p = Provider.objects.get(
                name=self.name, pid_type=self.pid_type, sid_type=self.sid_type
            )
            id = p.id
            p.delete()
        except Provider.DoesNotExist:
            raise exceptions.ResourceDoesNotExist("The provider " + self.name + " does not exist")

        return id


class addXmlBaseDatabaseCmd(baseCmd):
    """
    addXmlBaseDatabaseCmd: adds/remove an XmlBase

    XmlBase is the root URL of an ExtLink (ex: http://archive.numdam.org/article)

    Exception raised:
       - ValueError if the init params are empty
       - exceptions.ResourceExists during do if the XmlBase already exists
       - exceptions.ResourceDoesNotExist during undo if the XmlBase does not exist
       - RuntimeError during undo if related extlinks or objects still exist
    """

    def __init__(self, params=None):
        self.base = None  # Ex: http://archive.numdam.org/article

        super().__init__(params)

        self.required_params.extend(["base"])

    def internal_do(self):
        super().internal_do()

        try:
            XmlBase.objects.get(base=self.base)
            raise exceptions.ResourceExists("The xmlbase " + self.base + " already exists")
        except XmlBase.DoesNotExist:
            xmlbase = XmlBase(base=self.base)
            xmlbase.save()

        return xmlbase

    def internal_undo(self):
        super().internal_undo()

        try:
            xmlbase = XmlBase.objects.get(base=self.base)
            id = xmlbase.id
        except XmlBase.DoesNotExist:
            raise exceptions.ResourceDoesNotExist("The xmlbase " + self.base + " does not exist")

        try:
            extlink = ExtLink.objects.get(base=xmlbase)

            if extlink:
                raise RuntimeError(
                    "Impossible de supprimer cette ressource car elle est encore utilisee par des ExtLinks"
                )

        except ExtLink.DoesNotExist:
            pass

        try:
            related_object = RelatedObject.objects.get(base=xmlbase)

            if related_object:
                raise RuntimeError(
                    "Impossible de supprimer cette ressource car elle est encore utilisee par des RelatedObjects"
                )

        except RelatedObject.DoesNotExist:
            pass

        xmlbase.delete()
        return id


class addExtLinkDatabaseCmd(baseCmd):
    """
    addExtLinkDatabaseCmd: adds/remove an ExtLink
    An extlink is a link to an external object (image...)

    Exception raised:
       - ValueError if the init params are empty
       - exceptions.ResourceExists during do if the ExtLink already exists
       - exceptions.ResourceDoesNotExist during undo if the ExtLink does not exist
       - RuntimeError during undo if resources are still deployed
    """

    def __init__(self, params=None):
        self.rel = None  # 'website' or 'small_icon'
        self.mimetype = None
        self.location = None
        self.metadata = None
        self.seq = None
        self.resource = None
        self.base = None

        super().__init__(params)

        self.required_params.extend(["rel", "location", "seq", "resource"])

    def set_resource(self, resource):
        self.resource = resource

    def set_base(self, base):
        self.base = base

    def internal_do(self):
        super().internal_do()

        try:
            ExtLink.objects.get(
                resource=self.resource,
                base=self.base,
                rel=self.rel,
                mimetype=self.mimetype,
                location=self.location,
                metadata=self.metadata,
                seq=self.seq,
            )
            raise exceptions.ResourceExists(
                "The ExtLink " + self.base + " " + self.rel + " already exists"
            )
        except ExtLink.DoesNotExist:
            extlink = ExtLink(
                resource=self.resource,
                base=self.base,
                rel=self.rel,
                mimetype=self.mimetype,
                location=self.location,
                metadata=self.metadata,
                seq=self.seq,
            )
            extlink.save()

        return extlink

    def internal_undo(self):
        super().internal_undo()

        try:
            extlink = ExtLink.objects.get(resource=self.resource, base=self.base, rel=self.rel)
            id = extlink.id
            extlink.delete()
        except ExtLink.DoesNotExist:
            raise exceptions.ResourceDoesNotExist("The extlink " + self.rel + " does not exist")

        return id


class addExtIdDatabaseCmd(baseCmd):
    """
    addExtIdDatabaseCmd: adds/remove an ExtId

    Exception raised:
       - ValueError if the init params are empty
       - exceptions.ResourceExists during do if the ExtId already exists
       - exceptions.ResourceDoesNotExist during undo if the ExtId does not exist
       - RuntimeError during undo if resources are still deployed
    """

    def __init__(self, params=None):
        self.resource = None
        self.id_type = None
        self.id_value = None
        self.checked = True
        self.false_positive = False

        super().__init__(params)

        self.required_params.extend(["id_type", "id_value", "resource"])

    def set_resource(self, resource):
        self.resource = resource

    def internal_do(self):
        super().internal_do()

        try:
            # May raise Resource.DoesNotExist
            extid = ExtId.objects.get(
                resource=self.resource, id_type=self.id_type, id_value=self.id_value
            )
        except ExtId.DoesNotExist:
            extid = ExtId(
                resource=self.resource,
                id_type=self.id_type,
                id_value=self.id_value,
                checked=self.checked,
                false_positive=self.false_positive,
            )
            extid.save()

        return extid

    def internal_undo(self):
        super().internal_undo()

        try:
            extid = ExtId.objects.get(
                resource=self.resource, id_type=self.id_type, id_value=self.id_value
            )
            id = extid.id
            extid.delete()
        except ExtId.DoesNotExist:
            raise exceptions.ResourceDoesNotExist("The extid " + self.id_value + " does not exist")

        return id


class addRelatedObjectDatabaseCmd(baseCmd):
    """
    addRelatedObjectDatabaseCmd: adds/remove a RelatedObject

    a related object is a pdf/djvu... attached to the object (article...)
    It can be for example a listing of a program attached to an article.

    Exception raised:
       - ValueError if the init params are empty
       - exceptions.ResourceExists during do if the RelatedObject already exists
       - exceptions.ResourceDoesNotExist during undo if the RelatedObject does not exist
       - RuntimeError during undo if resources are still deployed
    """

    def __init__(self, params=None):
        self.rel = None
        self.mimetype = None
        self.location = None
        self.metadata = None
        self.seq = None
        self.resource = None
        self.base = None

        super().__init__(params)

        self.required_params.extend(["rel", "location", "seq", "resource"])

    def set_resource(self, resource):
        self.resource = resource

    def set_base(self, base):
        self.base = base

    def internal_do(self):
        super().internal_do()

        try:
            RelatedObject.objects.get(
                resource=self.resource,
                base=self.base,
                rel=self.rel,
                mimetype=self.mimetype,
                location=self.location,
                metadata=self.metadata,
                seq=self.seq,
            )
            raise exceptions.ResourceExists(
                "The RelatedObject "
                + self.base
                + " "
                + self.rel
                + " "
                + self.location
                + " already exists"
            )
        except RelatedObject.DoesNotExist:
            related_object = RelatedObject(
                resource=self.resource,
                base=self.base,
                rel=self.rel,
                mimetype=self.mimetype,
                location=self.location,
                metadata=self.metadata,
                seq=self.seq,
            )

            related_object.save()

        return related_object

    def internal_undo(self):
        super().internal_undo()

        try:
            related_object = RelatedObject.objects.get(
                resource=self.resource, base=self.base, rel=self.rel, location=self.location
            )
            id = related_object.id
            related_object.delete()
        except RelatedObject.DoesNotExist:
            raise exceptions.ResourceDoesNotExist(
                "The relatedobject " + self.location + " does not exist"
            )

        return id


class addSupplementaryMaterialDatabaseCmd(addRelatedObjectDatabaseCmd):
    """
    addSupplementaryMaterialDatabaseCmd: adds/remove a Supplementary Materiel

    a supplementary material is a pdf/djvu... attached to the object (article...)
    It can be for example a listing of a program attached to an article.

    Exception raised:
       - ValueError if the init params are empty
       - exceptions.ResourceExists during do if the RelatedObject already exists
       - exceptions.ResourceDoesNotExist during undo if the RelatedObject does not exist
       - RuntimeError during undo if resources are still deployed
    """

    def __init__(self, params=None):
        super().__init__(params)

    def internal_do(self):
        supplementary_material, _created = SupplementaryMaterial.objects.update_or_create(
            caption=self.caption,
            resource=self.resource,
            base=self.base,
            rel=self.rel,
            mimetype=self.mimetype,
            location=self.location,
            metadata=self.metadata,
            seq=self.seq,
        )
        return supplementary_material

    def internal_undo(self):
        try:
            supplementary_material = SupplementaryMaterial.objects.get(
                resource=self.resource, base=self.base, rel=self.rel, location=self.location
            )
            pk = supplementary_material.pk
            supplementary_material.delete()
        except SupplementaryMaterial.DoesNotExist:
            raise exceptions.ResourceDoesNotExist(
                "The SupplementaryMaterial " + self.location + " does not exist"
            )
        return pk


class addDataStreamDatabaseCmd(baseCmd):
    """
    addDataStreamDatabaseCmd: adds/remove a DataStream

    a datastream is the pdf/djvu... of the object (container, article)

    Exception raised:
       - ValueError if the init params are empty
       - exceptions.ResourceExists during do if the DataStream already exists
       - exceptions.ResourceDoesNotExist during undo if the DataStream does not exist
       - RuntimeError during undo if resources are still deployed
    """

    def __init__(self, params=None):
        self.rel = None
        self.mimetype = None
        self.location = None
        self.text = None
        self.seq = None
        self.resource = None
        self.base = None

        super().__init__(params)

        self.required_params.extend(["rel", "location", "seq", "resource"])

    def set_resource(self, resource):
        self.resource = resource

    def set_base(self, base):
        self.base = base

    def internal_do(self):
        super().internal_do()

        try:
            DataStream.objects.get(
                resource=self.resource,
                base=self.base,
                rel=self.rel,
                mimetype=self.mimetype,
                location=self.location,
                text=self.text,
                seq=self.seq,
            )
            raise exceptions.ResourceExists(
                "The DataStream "
                + self.base
                + " "
                + self.rel
                + " "
                + self.location
                + " already exists"
            )
        except DataStream.DoesNotExist:
            datastream = DataStream(
                resource=self.resource,
                base=self.base,
                rel=self.rel,
                mimetype=self.mimetype,
                location=self.location,
                text=self.text,
                seq=self.seq,
            )

            datastream.save()

        return datastream

    def internal_undo(self):
        super().internal_undo()

        try:
            datastream = DataStream.objects.get(
                resource=self.resource, base=self.base, rel=self.rel, location=self.location
            )
            id = datastream.id
            datastream.delete()
        except DataStream.DoesNotExist:
            raise exceptions.ResourceDoesNotExist(
                "The datastream " + self.location + " does not exist"
            )

        return id


class addResourceCountDatabaseCmd(baseCmd):
    """
    addResourceCountDatabaseCmd: adds/remove a ResourceCount

    A ResourceCount is a generic count element.
    Exemple: page count, table count, image count...

    Exception raised:
       - ValueError if the init params are empty
       - exceptions.ResourceExists during do if the ResourceCount already exists
       - exceptions.ResourceDoesNotExist during undo if the ResourceCount does not exist
       - RuntimeError during undo if resources are still deployed
    """

    def __init__(self, params=None):
        self.seq = None
        self.name = None
        self.value = None
        self.resource = None
        self.add_total = False

        super().__init__(params)

        self.required_params.extend(["seq", "name", "value", "resource"])

    def set_resource(self, resource):
        self.resource = resource

    def internal_do(self):
        super().internal_do()

        try:
            ResourceCount.objects.get(resource=self.resource, name=self.name)
            raise exceptions.ResourceExists("The ResourceCount " + self.name + " already exists")
        except ResourceCount.DoesNotExist:
            resource_count = ResourceCount(
                resource=self.resource, name=self.name, seq=self.seq, value=self.value
            )

            resource_count.save()

            if self.name == "page-count" and self.add_total:
                value = parse_page_count(self.value)

                try:
                    total = Stats.objects.get(name=self.name)
                    total.value += value
                except Stats.DoesNotExist:
                    total = Stats(name=self.name, value=value)

                total.save()

        return resource_count

    def internal_undo(self):
        super().internal_undo()

        try:
            resource_count = ResourceCount.objects.get(resource=self.resource, name=self.name)
            id = resource_count.id
            resource_count.delete()
        except ResourceCount.DoesNotExist:
            raise exceptions.ResourceDoesNotExist("The count " + self.name + " does not exist")

        return id


class addMetaDataPartDatabaseCmd(baseCmd):
    """
    addMetaDataPartDatabaseCmd: adds/remove a MetaDataPart

    A MetaDataPart is a generic count element.
    Exemple: page count, table count, image count...

    Exception raised:
       - ValueError if the init params are empty
       - exceptions.ResourceExists during do if the MetaDataPart already exists
       - exceptions.ResourceDoesNotExist during undo if the MetaDataPart does not exist
       - RuntimeError during undo if resources are still deployed
    """

    def __init__(self, params=None):
        self.seq = None
        self.name = None
        self.data = None
        self.resource = None

        super().__init__(params)

        self.required_params.extend(["seq", "name", "data", "resource"])

    def set_resource(self, resource):
        self.resource = resource

    def internal_do(self):
        super().internal_do()

        try:
            MetaDataPart.objects.get(resource=self.resource, name=self.name)
            raise exceptions.ResourceExists("The MetaDataPart " + self.name + " already exists")
        except MetaDataPart.DoesNotExist:
            metadatapart = MetaDataPart(
                resource=self.resource, name=self.name, seq=self.seq, data=self.data
            )

            metadatapart.save()

        return metadatapart

    def internal_undo(self):
        super().internal_undo()

        try:
            metadatapart = MetaDataPart.objects.get(resource=self.resource, name=self.name)
            id = metadatapart.id
            metadatapart.delete()
        except MetaDataPart.DoesNotExist:
            raise exceptions.ResourceDoesNotExist(
                "The metadatapart " + self.name + " does not exist"
            )

        return id


class addBibItemDatabaseCmd(baseCmd):
    """
    addBibItemDatabaseCmd: adds/remove a BibItem

    No verification is done to check if a BibItem already exists
    Rationale: BibItems are only added in a loop within an article.
    The check is actually the existence of the article.

    Exception raised:
       - ValueError if the init params are empty
       - exceptions.ResourceDoesNotExist during undo if the BibItem does not exist
       - RuntimeError during undo if resources are still deployed
    """

    def __init__(self, params=None):
        self.sequence = None
        self.label = ""
        self.citation_xml = None
        self.citation_tex = None
        self.citation_html = None
        self.resource = None

        self.type = ""
        self.user_id = ""
        self.title_tex = ""
        self.publisher_name = ""
        self.publisher_loc = ""
        self.institution = ""
        self.series = ""
        self.volume = ""
        self.issue = ""
        self.month = ""
        self.year = ""
        self.comment = ""
        self.annotation = ""
        self.fpage = ""
        self.lpage = ""
        self.page_range = ""
        self.size = ""
        self.source = ""
        self.article_title_tex = ""
        self.chapter_title_tex = ""

        self.contributors = None

        super().__init__(params)

        self.required_params.extend(["sequence", "citation_xml", "resource"])

    def set_resource(self, resource):
        self.resource = resource

    def internal_do(self):
        super().internal_do()

        bibitem = BibItem(
            resource=self.resource,
            sequence=self.sequence,
            label=self.label,
            citation_xml=self.citation_xml,
            citation_tex=self.citation_tex,
            citation_html=self.citation_html,
            type=self.type,
            user_id=self.user_id,
            article_title_tex=self.article_title_tex,
            chapter_title_tex=self.chapter_title_tex,
            source_tex=self.source_tex,
            publisher_name=self.publisher_name,
            publisher_loc=self.publisher_loc,
            institution=self.institution,
            series=self.series,
            volume=self.volume,
            issue=self.issue,
            month=self.month,
            year=self.year,
            comment=self.comment,
            annotation=self.annotation,
            fpage=self.fpage,
            lpage=self.lpage,
            page_range=self.page_range,
            size=self.size,
        )

        bibitem.save()

        return bibitem

    def post_do(self, bibitem):
        super().post_do(bibitem)

        add_contributors(contributors=self.contributors, bibitem=bibitem)

    def internal_undo(self):
        super().internal_undo()

        try:
            bibitem = BibItem.objects.get(resource=self.resource, sequence=self.sequence)
            id = bibitem.id
            bibitem.delete()
        except BibItem.DoesNotExist:
            raise exceptions.ResourceDoesNotExist(
                "The bibitem " + self.sequence + " does not exist"
            )

        return id


class addBibItemIdDatabaseCmd(baseCmd):
    """
    addBibItemIdDatabaseCmd: adds/remove a BibItemId

    No verification is done to check if a BibItemId already exists
    Rationale: BibItems are only added in a loop within an article.
    The check is actually the existence of the article.

    Exception raised:
       - ValueError if the init params are empty
       - exceptions.ResourceDoesNotExist during undo if the BibItemId does not exist
       - RuntimeError during undo if resources are still deployed
    """

    def __init__(self, params=None):
        self.bibitem = None
        self.id_type = None
        self.id_value = None
        self.checked = True
        self.false_positive = False

        super().__init__(params)

        self.required_params.extend(["bibitem", "id_type", "id_value"])

    def set_bibitem(self, bibitem):
        self.bibitem = bibitem

    def internal_do(self):
        super().internal_do()

        bibitemid = BibItemId(
            bibitem=self.bibitem,
            id_type=self.id_type,
            id_value=self.id_value,
            checked=self.checked,
            false_positive=self.false_positive,
        )

        bibitemid.save()

        return bibitemid

    def internal_undo(self):
        super().internal_undo()

        try:
            bibitemid = BibItemId.objects.get(
                bibitem=self.bibitem, id_type=self.id_type, id_value=self.id_value
            )
            id = bibitemid.id
            bibitemid.delete()
        except BibItemId.DoesNotExist:
            raise exceptions.ResourceDoesNotExist(
                "The bibitemid " + self.value + " does not exist"
            )

        return id


class addFrontMatterDatabaseCmd(baseCmd):
    """
    addFrontMatterDatabaseCmd: adds/remove a FrontMatter

    No verification is done to check if a FrontMatter already exists
    Rationale: FrontMatters are only added in a loop within an article.
    The check is actually the existence of the article.

    Exception raised:
       - ValueError if the init params are empty
       - exceptions.ResourceDoesNotExist during undo if the FrontMatter does not exist
       - RuntimeError during undo if resources are still deployed
    """

    def __init__(self, params=None):
        self.value_xml = ""
        self.value_html = ""
        self.foreword_html = ""
        self.resource = None

        super().__init__(params)

        self.required_params.extend(["value_xml", "resource"])

    def set_resource(self, resource):
        self.resource = resource

    def internal_do(self):
        super().internal_do()

        frontmatter = FrontMatter(
            resource=self.resource,
            value_xml=self.value_xml,
            value_html=self.value_html,
            foreword_html=self.foreword_html,
        )

        frontmatter.save()

        return frontmatter

    def internal_undo(self):
        super().internal_undo()

        try:
            frontmatter = FrontMatter.objects.get(resource=self.resource)
            id = frontmatter.id
            frontmatter.delete()
        except FrontMatter.DoesNotExist:
            raise exceptions.ResourceDoesNotExist(
                "The front-matter " + self.text + " does not exist"
            )

        return id


class addRelationshipDatabaseCmd(baseCmd):
    """
    addRelationshipDatabaseCmd: adds/remove a Relationship

    Relationship relates 2 resources (ex: articles) with a relation. ex "follows", "followed-by"

    RelationName are created with a fixture (see app/ptf/apps/ptf/fixtures/initial_data.json
    Example { "left" : "follows", "right" : "followed-by" }
    A related-article of an article has 1 relation name (ex "follows" or "followed-by")
    You need to know if the relation was stored in the left or right attribute of a RelationName,
    so that you can create/search the Relationship with the correct object/subject.
    Ex: with A "follows" B, A is the subject and B the object because "follows" is a RelationName.left attribute
        with A "followed-by" B, A is the object the B the subject because
        "followed-by" is a RelationName.right attribute
    A Relationship relates 2 resources with a RelationName


    Exception raised:
       - ValueError if the init params are empty
       - exceptions.ResourceExists during do if the Relationship already exists
       - exceptions.ResourceDoesNotExist during undo if the Relationship does not exist
       - RuntimeError during undo if resources are still deployed
    """

    def __init__(self, params=None):
        self.subject_resource = None
        self.subject_pid = None
        self.object_resource = None
        self.object_pid = None
        self.relationname = None

        super().__init__(params)

        self.required_params.extend(["subject_pid", "object_pid", "relationname"])

    def set_subject_resource(self, resource):
        self.subject_resource = resource
        # self.subject_pid = resource.pid

    def set_object_resource(self, resource):
        self.object_resource = resource
        # self.object_pid = resource.pid

    def set_relationname(self, relationname):
        self.relationname = relationname

    def internal_do(self):
        super().internal_do()

        try:
            relationship = Relationship.objects.get(
                subject_pid=self.subject_pid,
                object_pid=self.object_pid,
                rel_info=self.relationname,
            )
            # la première fois que l'on crée la relation une des 2 ressourcesID est a priori None
            # la deuxième fois (la relation symétrique) il faut compléter la
            # RelationShip
            if relationship.resource is not None and relationship.related is not None:
                raise exceptions.ResourceExists(
                    "The Relationship {} {} {} already exists".format(
                        self.subject_pid, self.relationname.left, self.object_pid
                    )
                )
            if self.subject_resource is not None:
                relationship.resource = self.subject_resource
            if self.object_resource is not None:
                relationship.related = self.object_resource
            relationship.save()

        except Relationship.DoesNotExist:
            relationship = Relationship(
                subject_pid=self.subject_pid,
                object_pid=self.object_pid,
                resource=self.subject_resource,
                related=self.object_resource,
                rel_info=self.relationname,
            )

            relationship.save()

        return relationship

    def internal_undo(self):
        super().internal_undo()

        try:
            relationship = Relationship.objects.get(
                subject_pid=self.subject_pid,
                object_pid=self.object_pid,
                rel_info=self.relationname,
            )
            id = relationship.id

            # Create relationship is typically a 2 steps process
            #    (1: create the relationship with only 1 resource. 2: update the relationship with the 2nd resource)
            # Undo is also a 2 steps process
            #    (1: unset a resource. 2: delete the relationship)
            if relationship.resource is not None and relationship.related is not None:
                # Both left and right resources are set: unset the resource
                # that was given as param
                if self.subject_resource is None:
                    relationship.related = None
                else:
                    relationship.resource = None
                relationship.save()
            else:
                relationship.delete()
        except Relationship.DoesNotExist:
            raise exceptions.ResourceDoesNotExist(
                "The relationship " + self.relationname + " does not exist"
            )

        return id


class addResourceDatabaseCmd(baseCmd):
    """
    addResourceDatabaseCmd: base class for all resources

    Exception raised:
       - exceptions.ResourceDoesNotExist during undo if the Resource does not exist
    """

    def __init__(self, params=None):
        self.xobj = None  # model_data object

        self.provider = None
        self._prod_deployed_date = None

        super().__init__(params)

        self.required_params.extend(["xobj", "provider"])

    # May raise ValueError
    def check_params(self):
        super().check_params()

        if hasattr(self.xobj, "pid") and not self.xobj.pid and not self.xobj.sid:
            raise ValueError("pid et sid sont vides")

    def set_provider(self, provider):
        self.provider = provider

    # May raise Provider.DoesNotExist
    def set_provider_by_name_or_id(self, provider_name="", pid_type="", sid_type=None):
        self.provider = Provider.objects.get(
            name=provider_name, pid_type=pid_type, sid_type=sid_type
        )

    def post_do(self, resource):
        super().post_do(resource)
        self.object_to_be_deleted = resource

        site = model_helpers.get_or_create_site(settings.SITE_ID, resource.pid)
        if site:
            resource.deploy(site, self._prod_deployed_date)

        # Creation of SiteMembership for production website on ptf_tools is handled in DeployJatsIssue
        # Restoration of SiteMembership is handled in importExtraDataPtfCmd

        for id_type, id_value in self.xobj.ids:
            if (
                id_type != self.provider.pid_type
                and id_type != self.provider.sid_type
                and (id_type != "numdam-id" or self.provider.pid_type != "mathdoc-id")
            ):
                try:
                    # May raise Resource.DoesNotExist
                    resource_id = ResourceId.objects.get(
                        resource=resource, id_type=id_type, id_value=id_value
                    )
                except ResourceId.DoesNotExist:
                    resource_id = ResourceId(resource=resource, id_type=id_type, id_value=id_value)
                    resource_id.save()

        for id_type, id_value in self.xobj.extids:
            if (
                id_type != self.provider.pid_type
                and id_type != self.provider.sid_type
                and (id_type != "numdam-id" or self.provider.pid_type != "mathdoc-id")
            ):
                try:
                    # May raise Resource.DoesNotExist
                    ExtId.objects.get(resource=resource, id_type=id_type, id_value=id_value)
                except ExtId.DoesNotExist:
                    ext_id = ExtId(resource=resource, id_type=id_type, id_value=id_value)
                    ext_id.save()

        seq = 1
        # abstract_set = self.xobj.abstract_set.all()
        for a in self.xobj.abstracts:
            value_xml = a["value_xml"] if "value_xml" in a else ""
            value_html = a["value_html"] if "value_html" in a else ""
            value_tex = a["value_tex"] if "value_tex" in a else ""

            la = Abstract(
                resource=resource,
                tag=a["tag"],
                lang=a["lang"],
                seq=seq,
                value_xml=value_xml,
                value_html=value_html,
                value_tex=value_tex,
            )
            la.save()
            seq += 1

        add_contributors(contributors=self.xobj.contributors, resource=resource)

        for i, kwd in enumerate(self.xobj.kwds):
            k = Kwd(
                resource=resource, lang=kwd["lang"], type=kwd["type"], value=kwd["value"], seq=i
            )
            k.save()

        for i, subj in enumerate(self.xobj.subjs):
            s = Subj(
                resource=resource, lang=subj["lang"], type=subj["type"], value=subj["value"], seq=i
            )
            s.save()

        seq = 1
        for a in self.xobj.awards:
            abbrev = a["abbrev"]
            award_id = a["award_id"]

            award = Award(resource=resource, abbrev=abbrev, award_id=award_id, seq=seq)
            award.save()
            seq += 1

        # TODO custom meta

    def pre_undo(self):
        super().pre_undo()

        # None value not detected in check_params (required_delete_params)
        # => undo was already called
        if self.object_to_be_deleted is None:
            raise exceptions.ResourceDoesNotExist("The object to be deleted no longer exists")

        # Django automatically deletes related objects such as ResourceIds,
        # Abstracts...

        # Author are deleted by signals (see models.py)
        # Another solution would be to do it here

        # Undeploy the resource in all sites
        for site in self.object_to_be_deleted.sites.all():
            self.object_to_be_deleted.undeploy(site)

    def internal_undo(self):
        super().internal_undo()

        id = self.object_to_be_deleted.id
        self.object_to_be_deleted.delete()

        return id

    def post_undo(self):
        super().internal_undo()

        self.object_to_be_deleted = None  # prevents a 2nd attempt to delete


class addPublisherDatabaseCmd(baseCmd):
    """
    addPublisherDatabaseCmd: adds/remove a publisher

    Exception raised:
       - ValueError if the init params are empty
       - exceptions.ResourceExists during do if the Publisher already exists
       - exceptions.ResourceDoesNotExist during undo if the Publisher does not exist
    """

    def __init__(self, params=None):
        self.xobj = None  # model_data object
        self.object_to_be_deleted = None

        super().__init__(params)

        self.required_params.extend(["xobj"])
        self.required_delete_params.append("object_to_be_deleted")

    def set_object_to_be_deleted(self, obj):
        self.object_to_be_deleted = obj

    def internal_do(self):
        super().internal_do()

        # A Publisher is a resource. Therefore it needs a provider (that provides the publisher id/key)
        # As we are creating the key, Mathdoc is the provider
        provider = Provider.objects.get(name="mathdoc", pid_type="mathdoc-id")
        key = model_helpers.make_key(self.xobj.name)

        try:
            Publisher.objects.get(pub_key=key)
            raise exceptions.ResourceExists("The publisher " + self.xobj.name + " already exists")
        except Publisher.DoesNotExist:
            publisher = Publisher(
                pub_key=key,
                pub_name=self.xobj.name,
                pub_loc=self.xobj.loc,
                pid=key,
                provider=provider,
            )
            publisher.save()

        self.object_to_be_deleted = publisher
        return publisher

    def internal_undo(self):
        super().internal_undo()

        # None value not detected in check_params (required_delete_params)
        # => undo was already called
        if self.object_to_be_deleted is None:
            raise exceptions.ResourceDoesNotExist("The object to be deleted no longer exists")

        self.object_to_be_deleted.refresh_from_db()

        if self.object_to_be_deleted.publishes.count():
            raise RuntimeError(
                "Impossible de supprimer ce publisher car il a encore des resources qui sont publiées par ce publisher"
            )

        id = self.object_to_be_deleted.id
        self.object_to_be_deleted.delete()
        self.object_to_be_deleted = None

        return id


class addContainerDatabaseCmd(addResourceDatabaseCmd):
    """
    addContainerDatabaseCmd: adds/remove a container

    an Container needs a Collection

    Exception raised:
       - ValueError if the init params are empty
       - exceptions.ResourceExists during do if the container already exists
       - exceptions.ResourceDoesNotExist during undo if the Container does not exist
    """

    def __init__(self, params=None):
        self.last_modified = None
        self.collection = None
        self._publisher = None

        super().__init__(params)

        self.required_params.extend(["collection"])

    def add_collection(self, collection):
        if not self.collection:
            self.collection = collection

    def pre_do(self):
        super().pre_do()

        self._publisher = add_publisher(self.xobj)

    def internal_do(self):
        super().internal_do()

        vseries = volume = number = seq = ""
        if hasattr(self.xobj, "volume"):
            vseries = self.xobj.vseries
            volume = self.xobj.volume
            number = self.xobj.number
            seq = self.xobj.seq
        elif len(self.xobj.incollection) > 0:
            # Books do not have vseries/volume/number, but a list of incollection
            # Set vseries/volume/number from the 1st incollection (the main collection)
            incol = self.xobj.incollection[0]
            vseries = incol.vseries
            volume = ""
            number = incol.volume
            seq = incol.seq

        seq = make_int(volume) if volume else make_int(self.xobj.year)

        last_modified = model_helpers.parse_date_str(self.xobj.last_modified_iso_8601_date_str)
        if self.xobj.prod_deployed_date_iso_8601_date_str:
            self._prod_deployed_date = model_helpers.parse_date_str(
                self.xobj.prod_deployed_date_iso_8601_date_str
            )

        with_online_first = (
            self.xobj.with_online_first if hasattr(self.xobj, "with_online_first") else False
        )

        try:
            Container.objects.get(
                pid=self.xobj.pid,
                provider__pid_type=self.provider.pid_type,
                my_collection__pid=self.collection.pid,
            )
            raise exceptions.ResourceExists("The container " + self.xobj.pid + " already exists")

        except Container.DoesNotExist:
            container = Container(
                ctype=self.xobj.ctype,
                doi=self.xobj.doi,
                pid=self.xobj.pid,
                lang=self.xobj.lang,
                title_xml=self.xobj.title_xml,
                title_tex=self.xobj.title_tex,
                title_html=self.xobj.title_html,
                trans_lang=self.xobj.trans_lang,
                trans_title_tex=self.xobj.trans_title_tex,
                trans_title_html=self.xobj.trans_title_html,
                provider=self.provider,
                my_publisher=self._publisher,
                my_collection=self.collection,
                year=self.xobj.year,
                vseries=vseries,
                vseries_int=make_int(vseries),
                volume=volume,
                volume_int=make_int(volume),
                number=number,
                number_int=make_int(number),
                seq=seq,
                last_modified=last_modified,
                with_online_first=with_online_first,
                body_html=self.xobj.body_html,
                body_tex=self.xobj.body_tex,
                body_xml=self.xobj.body_xml,
            )

            container.save()

        return container

    def post_do(self, container):
        super().post_do(container)

        if hasattr(self.xobj, "incollection"):
            for incol in self.xobj.incollection:
                # Ignore the first incollection which was treated as the main collection
                if incol.pid != self.collection.pid:
                    collection = model_helpers.get_collection(incol.pid)
                    if collection:
                        collection_membership = CollectionMembership(
                            collection=collection,
                            container=container,
                            seq=incol.seq,
                            vseries=incol.vseries,
                            volume="",
                            number=incol.volume,
                            vseries_int=make_int(incol.vseries),
                            volume_int=0,
                            number_int=make_int(incol.volume),
                        )
                        collection_membership.save()

        add_biblio(self.xobj, container)
        add_counts(self.xobj, container, add_total=True)
        add_relations(self.xobj, container)
        add_frontmatter(self.xobj, container)

    def post_undo(self):
        collection = self.object_to_be_deleted.get_collection()
        super().post_undo()

        if self._publisher is not None:
            if self._publisher.publishes.count() == 0:
                self._publisher.delete()
                self._publisher = None

        if collection.parent and collection.content.count() == 0:
            # Child collection that was created on the fly is removed automatically
            # if it no longer has any content
            collection.delete()


class addResourceInSpecialIssueDatabaseCmd(baseCmd):
    """
    addResourceInSpecialIssueDatabaseCmd: adds/remove a ResourceInSpecialIssue

    a ResourceInSpecialIssue needs a  issue

    params :
        - obj : the resource 'article, book, ...'
        - obj_doi: the doi of the resource
        - container : the special issue using the resource
        - seq : sequence of the resource in the special issue
        - citation : html citation of the resource

    Exception raised:
       - ValueError if the init params are empty
       - exceptions.ResourceExists during do if the article already exists
       - exceptions.ResourceDoesNotExist during undo if the Article does not exist
    """

    def __init__(self, params=None):
        # parents are used for book parts.

        # Container containing the article
        self.container = params["container"]
        # self.collection = None
        # self.provider = params["provider"]
        self.obj_doi = params["obj_doi"]
        self.seq = params["seq"]
        # self.xobj = params["xobj"]
        self.citation = params["citation"]
        # self.resource = None

        # super().__init__(params)

    def do(self):
        # super().internal_do()
        # changer nom resource en x...
        # xresource = self.xobj
        resource_doi = self.obj_doi
        container = self.container
        seq = self.seq
        citation = self.citation
        # if not citation:
        #     citation = resource.citation_html
        # juste mettre la vérif sur le champ doi car toutes les resources ont ce champ
        # et mettre model_helper get_resource...
        # if isinstance(resource, jats_parser.JatsArticle):
        if resource_doi:
            resource = Article.objects.get(doi=resource_doi)
        else:
            pass
        try:
            ResourceInSpecialIssue.objects.get(
                resource=resource, resource_doi=resource_doi, my_container=container
            )
            raise exceptions.ResourceExists(
                f"The ResourceInSpecialIssue {resource} for issue {container} already exists"
            )

        except ResourceInSpecialIssue.DoesNotExist:
            resource_in_special_issue = ResourceInSpecialIssue(
                resource=resource,
                resource_doi=resource_doi,
                my_container=container,
                citation=citation,
                seq=seq,
            )

            resource_in_special_issue.save()
        return resource_in_special_issue


class addArticleDatabaseCmd(addResourceDatabaseCmd):
    """
    addArticleDatabaseCmd: adds/remove an article

    an article needs a container (book or issue)

    Exception raised:
       - ValueError if the init params are empty
       - exceptions.ResourceExists during do if the article already exists
       - exceptions.ResourceDoesNotExist during undo if the Article does not exist
    """

    def __init__(self, params=None):
        # parents are used for book parts.
        self.pseq = 0  # parent seq
        self.parent = None  # article parent

        # Container containing the article
        self.container = None
        self.collection = None

        self.assign_doi = False
        self.seq = 0

        self.translated_articles = []

        super().__init__(params)

    def check_params(self):
        super().check_params()

        check_title = True
        if hasattr(self.xobj, "doi") and self.xobj.doi and self.xobj.doi.find("pcjournal") > 0:
            check_title = False
        if "original_article" in self.required_params:
            # A Translation may not have a title
            check_title = False
        if (
            check_title
            and self.is_add_cmd
            and not self.xobj.title_tex
            and not self.xobj.trans_title_tex
        ):
            raise ValueError("title_tex et trans_title_tex sont vides")

    def set_container(self, container):
        self.container = container
        self.collection = container.my_collection
        self.set_provider(container.provider)

    def set_collection(self, collection):
        self.collection = collection
        self.set_provider(collection.provider)

    def set_parent(self, parent):
        self.parent = parent

    def parse_dates(self):
        dates = {
            "accepted": None,
            "received": None,
            "revised": None,
            "online": None,
            "published": None,
        }

        for date_entry in self.xobj.history_dates:
            if date_entry["date"] is None:
                raise ValueError(
                    f"The date {date_entry['type']} of the article {self.xobj.pid} is None"
                )

            date = model_helpers.parse_date_str(date_entry["date"])
            dates[date_entry["type"]] = date

        if self.xobj.date_published_iso_8601_date_str:
            dates["published"] = model_helpers.parse_date_str(
                self.xobj.date_published_iso_8601_date_str
            )

        if self.xobj.prod_deployed_date_iso_8601_date_str:
            self._prod_deployed_date = model_helpers.parse_date_str(
                self.xobj.prod_deployed_date_iso_8601_date_str
            )

        return dates

    def add_translations(self, xobj, article):
        if hasattr(xobj, "translations") and xobj.translations is not None:
            for xarticle in xobj.translations:
                cmd = addTranslatedArticleDatabaseCmd({"xobj": xarticle})
                cmd.set_original_article(article)
                cmd.set_provider(article.provider)
                trans_article = cmd.do()
                self.translated_articles.append(trans_article)

    def internal_do(self):
        super().internal_do()

        doi = self.xobj.doi
        if self.assign_doi and not doi and self.container:
            colid = self.container.my_collection.pid
            doi = model_helpers.assign_doi(colid)

        seq = self.xobj.seq or self.seq
        dates = self.parse_dates()

        try:
            Article.objects.get(
                pid=self.xobj.pid, doi=doi, provider=self.provider, my_container=self.container
            )
            raise exceptions.ResourceExists("The article " + self.xobj.pid + " already exists")
        except Article.DoesNotExist:
            article = Article(
                pid=self.xobj.pid,
                doi=doi,
                lang=self.xobj.lang,
                title_xml=self.xobj.title_xml,
                title_tex=self.xobj.title_tex,
                title_html=self.xobj.title_html,
                trans_lang=self.xobj.trans_lang,
                trans_title_tex=self.xobj.trans_title_tex,
                trans_title_html=self.xobj.trans_title_html,
                provider=self.provider,
                my_container=self.container,
                fpage=self.xobj.fpage,
                lpage=self.xobj.lpage,
                seq=seq,
                atype=self.xobj.atype,
                page_range=self.xobj.page_range,
                page_type=self.xobj.page_type,
                elocation=self.xobj.elocation,
                article_number=self.xobj.article_number,
                talk_number=self.xobj.talk_number,
                pseq=self.pseq,
                parent=self.parent,
                date_accepted=dates["accepted"],
                date_received=dates["received"],
                date_revised=dates["revised"],
                date_online_first=dates["online"],
                date_published=dates["published"],
                coi_statement=self.xobj.coi_statement,
                funding_statement_html=self.xobj.funding_statement_html,
                funding_statement_xml=self.xobj.funding_statement_xml,
                footnotes_html=self.xobj.footnotes_html,
                footnotes_xml=self.xobj.footnotes_xml,
                body_html=self.xobj.body_html,
                body_tex=self.xobj.body_tex,
                body_xml=self.xobj.body_xml,
            )

            article.save()

        if doi:
            collection = self.collection
            doi_number = model_helpers.get_number_from_doi(doi)
            if doi_number > collection.last_doi:
                collection.last_doi = doi_number
                collection.save()

        return article

    def post_do(self, article):
        super().post_do(article)

        add_biblio(self.xobj, article)
        add_counts(self.xobj, article, add_total=True)
        add_relations(self.xobj, article)
        self.add_translations(self.xobj, article)


class addCollectionDatabaseCmd(addResourceDatabaseCmd):
    """
    addCollectionDatabaseCmd: adds/remove a collection

    Exception raised:
       - ValueError if the init params are empty
       - exceptions.ResourceExists during do if the book already exists
       - exceptions.ResourceDoesNotExist during undo if the book does not exist
    """

    def __init__(self, params=None):
        # self.coltype = None
        # self.wall = 5
        self.parent = None

        super().__init__(params)

    def set_parent(self, parent):
        self.parent = parent

    def internal_do(self):
        super().internal_do()

        title_sort = (
            self.xobj.title_tex if len(self.xobj.title_tex) < 129 else self.xobj.title_tex[0:127]
        )

        try:
            col = Collection.objects.get(
                pid=self.xobj.pid,
                title_tex=self.xobj.title_tex,
                provider__pid_type=self.provider.pid_type,
            )
        except Collection.DoesNotExist:
            col = Collection.objects.create(
                coltype=self.xobj.coltype,
                abbrev=self.xobj.abbrev,
                wall=self.xobj.wall,
                pid=self.xobj.pid,
                lang=self.xobj.lang,
                title_xml=self.xobj.title_xml,
                title_tex=self.xobj.title_tex,
                title_html=self.xobj.title_html,
                title_sort=title_sort,
                trans_lang=self.xobj.trans_lang,
                trans_title_tex=self.xobj.trans_title_tex,
                trans_title_html=self.xobj.trans_title_html,
                provider=self.provider,
                parent=self.parent,
            )
        else:
            raise exceptions.ResourceExists(f"The collection pid:{self.xobj.pid} already exists")
        return col

    # def internal_undo(self):
    #     super().internal_undo()
    #
    #     try:
    #         col = Collection.objects.get(pid=self.pid,
    #                                      provider__pid_type=self.provider.pid_type)
    #         id = col.id
    #         if col.parent and not col.ancestors.all().count() :
    #             col.parent = None
    #             col.delete()
    #
    #
    #         elif not col.parent and not col.ancestors.all().count() :
    #             col.delete()
    #
    #     except Collection.DoesNotExist:
    #         raise exceptions.ResourceDoesNotExist(
    #             "The collection " + self.pid + " does not exist")
    #
    #     return id


class addTranslatedArticleDatabaseCmd(addArticleDatabaseCmd):
    """
    addTranslatedArticleDatabaseCmd: adds/remove a translated article
    """

    def __init__(self, params=None):
        super().__init__(params)

        self.original_article = None

        self.required_params = ["xobj", "original_article"]

    def set_original_article(self, original_article):
        self.original_article = original_article

    def internal_do(self):
        # DO NOT CALL the parent internal_do or it will create an Article

        dates = self.parse_dates()

        try:
            TranslatedArticle.objects.get(pid=self.xobj.pid)
            raise exceptions.ResourceExists(
                "The translated article " + self.xobj.pid + " already exists"
            )
        except Article.DoesNotExist:
            article = TranslatedArticle(
                original_article=self.original_article,
                pid=self.xobj.pid,
                doi=self.xobj.doi,
                lang=self.xobj.lang,
                title_xml=self.xobj.title_xml,
                title_tex=self.xobj.title_tex,
                title_html=self.xobj.title_html,
                trans_lang=self.xobj.trans_lang,
                trans_title_tex=self.xobj.trans_title_tex,
                trans_title_html=self.xobj.trans_title_html,
                provider=self.provider,
                my_container=None,
                fpage=self.xobj.fpage,
                lpage=self.xobj.lpage,
                seq=self.xobj.seq,
                atype=self.xobj.atype,
                page_range=self.xobj.page_range,
                page_type=self.xobj.page_type,
                elocation=self.xobj.elocation,
                article_number=self.xobj.article_number,
                talk_number=self.xobj.talk_number,
                pseq=self.pseq,
                parent=self.parent,
                date_accepted=dates["accepted"],
                date_received=dates["received"],
                date_revised=dates["revised"],
                date_online_first=dates["online"],
                date_published=dates["published"],
                coi_statement=self.xobj.coi_statement,
                funding_statement_html=self.xobj.funding_statement_html,
                funding_statement_xml=self.xobj.funding_statement_xml,
                footnotes_html=self.xobj.footnotes_html,
                footnotes_xml=self.xobj.footnotes_xml,
                body_html=self.xobj.body_html,
                body_tex=self.xobj.body_tex,
                body_xml=self.xobj.body_xml,
            )

            article.save()

        return article

    def post_do(self, container):
        super().post_do(container)

        if hasattr(self.xobj, "incollection"):
            for incol in self.xobj.incollection:
                # Ignore the first incollection which was treated as the main collection
                if incol.pid != self.collection.pid:
                    collection = model_helpers.get_collection(incol.pid)
                    if collection:
                        collection_membership = CollectionMembership(
                            collection=collection,
                            container=container,
                            seq=incol.seq,
                            vseries=incol.vseries,
                            volume="",
                            number=incol.volume,
                            vseries_int=make_int(incol.vseries),
                            volume_int=0,
                            number_int=make_int(incol.volume),
                        )
                        collection_membership.save()

        add_biblio(self.xobj, container)
        add_counts(self.xobj, container, add_total=True)
        add_relations(self.xobj, container)
        add_frontmatter(self.xobj, container)

    def post_undo(self):
        collection = self.object_to_be_deleted.get_collection()
        super().post_undo()

        if self._publisher is not None:
            if self._publisher.publishes.count() == 0:
                self._publisher.delete()
                self._publisher = None

        if collection.parent and collection.content.count() == 0:
            # Child collection that was created on the fly is removed automatically
            # if it no longer has any content
            collection.delete()


##########################################################################
##########################################################################
#
#                                 Update Commands
#
##########################################################################
##########################################################################


class updateCollectionDatabaseCmd(addResourceDatabaseCmd):
    """
    updateCollectionDatabaseCmd: updates a Collection (journal, acta)

    a Collection needs a Provider

    Exception raised:
       - ValueError if the init params are empty
       - exceptions.ResourceDoesNotExist during do if the Collection does not exist
    """

    def __init__(self, params=None):
        super().__init__(params)

    def internal_do(self):
        super().internal_do()

        try:
            collection = Collection.objects.get(
                pid=self.xobj.pid, provider__pid_type=self.provider.pid_type
            )
        except Collection.DoesNotExist:
            raise exceptions.ResourceDoesNotExist(f"The journal {self.xobj.pid} does not exist")

        # delete objects in direct relation with the collection
        # the new related objects (extlink, resourceid...) will be added in
        # addResourceDatabaseCmd::post_do
        collection.extlink_set.all().delete()
        collection.resourceid_set.all().delete()
        collection.abstract_set.all().delete()

        title_sort = (
            self.xobj.title_tex if len(self.xobj.title_tex) < 129 else self.xobj.title_tex[0:127]
        )

        collection.coltype = self.xobj.coltype
        collection.lang = self.xobj.lang
        collection.title_xml = self.xobj.title_xml
        collection.title_tex = self.xobj.title_tex
        collection.title_html = self.xobj.title_html
        collection.title_sort = title_sort
        collection.abbrev = self.xobj.abbrev

        collection.wall = self.xobj.wall
        collection.save()

        return collection


class updateExtLinkDatabaseCmd(baseCmd):
    """
    An extlink is a link to an external object (website, image...)
    updateExtLinkDatabaseCmd:
    - updates an ExtLink, or
    - creates the object if it does not exist, or
    - deletes the object if it exists and the new location value is empty

    Exception raised:
       - ValueError if the init params are empty
    """

    def __init__(self, params=None):
        self.rel = None  # 'website' or 'small_icon'
        self.mimetype = None
        self.location = None
        self.metadata = None
        self.seq = None
        self.resource = None
        self.base = None

        super().__init__(params)

        self.required_params.extend(["rel", "resource"])

    def set_resource(self, resource):
        self.resource = resource

    def set_base(self, base):
        self.base = base

    def internal_do(self):
        super().internal_do()

        extlink = None

        try:
            extlink = ExtLink.objects.get(resource=self.resource, rel=self.rel)
            self.seq = extlink.seq

        except ExtLink.DoesNotExist:
            if self.location:
                if not self.seq:
                    self.seq = ExtLink.objects.filter(resource=self.resource).count() + 1

                extlink = ExtLink(resource=self.resource, rel=self.rel, seq=self.seq)
                extlink.save()

        if self.location:
            extlink.location = self.location

            if self.rel in ["website", "test_website"]:
                self.metadata = "website"

            if self.base:
                extlink.base = self.base
            if self.mimetype:
                extlink.mimetype = self.mimetype
            if self.metadata:
                extlink.metadata = self.metadata

            extlink.seq = self.seq

            extlink.save()
        elif extlink:
            extlink.delete()
            extlink = None

        return extlink


class updateResourceIdDatabaseCmd(baseCmd):
    """
    A ResourceId is another id of the resource (doi, issn...)
    updateResourceIdDatabaseCmd:
    - updates an ResourceId, or
    - creates the object if it does not exist, or
    - deletes the object if it exists and the new location value is empty

    Exception raised:
       - ValueError if the init params are empty
    """

    def __init__(self, params={}):
        self.id_type = None  # 'doi', 'issn', 'e-issn'
        self.id_value = None
        self.resource = None

        super().__init__(params)

        self.required_params.extend(["id_type", "resource"])

    def set_resource(self, resource):
        self.resource = resource

    def internal_do(self):
        super().internal_do()

        resourceid = None
        id_type = self.id_type
        id_value = self.id_value

        if id_type == "ojs-id":
            # ojs-id (aka article folder name in /cedram_dev) may not be unique
            # create an internal id
            if id_value == "edito":
                qs = ResourceId.objects.filter(
                    id_type="ojs-id", id_value__startswith=id_value + "$$"
                )
                count = qs.count()
                if count:
                    id_value = id_value + "$$" + str(count + 1)
            else:
                qs = ResourceId.objects.filter(id_type="ojs-id", id_value__startswith=id_value)
                count = qs.count()
                if count:
                    id_value = id_value + "$$" + str(count + 1)
        try:
            resourceid = ResourceId.objects.get(resource=self.resource, id_type=id_type)

        except ResourceId.DoesNotExist:
            if id_value:
                resourceid = ResourceId(resource=self.resource, id_type=id_type, id_value=id_value)

        if id_value:
            resourceid.id_value = id_value
            resourceid.save()
        elif resourceid:
            resourceid.delete()
            resourceid = None

        return resourceid


class deleteResourceDatabaseCmd(baseCmd):
    """
    deleteResourceDatabaseCmd: base class for all resources
    NOT USED. TODO: remove
    """

    def __init__(self, params=None):
        self.pid = None  # primary id given by the provider
        self.sid = None  # surrogate id given by the provider
        self.provider = None

        super().__init__(params)

    def pre_do(self):
        super().pre_do()

        if self.pid is None and self.sid is None:
            raise ValueError("pid et sid sont vides")

        if self.provider is None:
            raise ValueError("provider est vide")

    def set_provider(self, provider):
        self.provider = provider

    # May raise Provider.DoesNotExist
    def set_provider_by_name_or_id(self, provider_name="", pid_type="", sid_type=None):
        self.provider = Provider.objects.get(
            name=provider_name, pid_type=pid_type, sid_type=sid_type
        )


class publishArticleDatabaseCmd(baseCmd):
    """
    Publish an article <=> Create a pub-date
    @return: list of updated articles
    """

    def __init__(self, params=None):
        self.article = None
        self.container = None
        self.pre_publish = False  # Publish on the test website
        self.update_db = True
        super().__init__(params)

        self.required_params.extend(["article"])

    def set_article(self, article):
        self.article = article
        self.container = article.my_container
        if self.container is None and article.classname == "TranslatedArticle":
            self.container = self.article.original_article.my_container

    def publish_article(self, article, update_articles):
        # In a collection with standalone articles, the article number is based on
        # the publication order.
        # It has to be updated when we publish because of the articles excluded for publication
        update_article_number = False
        # TODO: use a param instead of checking PCJ
        if self.container.my_collection.pid == "PCJ" and not self.pre_publish:
            update_article_number = True

        article_number = (
            self.container.article_set.filter(date_published__isnull=False).count() + 1
        )

        if (not article.date_published and not self.pre_publish) or (
            not article.date_pre_published and self.pre_publish
        ):
            today = model_helpers.parse_date_str(timezone.now().isoformat())

            # Les articles numérisés n'ont pas à avoir de date_published
            # Les anciens articles issus de Cedrics ont déjà une date_published.
            # Les nouveaux articles du Centre Mersenne sont publiés à partir de 2019.
            year = article.get_year()
            fyear, lyear = model_helpers.get_first_last_years(year)
            try:
                fyear = int(fyear)
            except ValueError:
                fyear = 0

            if fyear > 2016 and not self.container.with_online_first:
                if self.pre_publish:
                    article.date_pre_published = today
                else:
                    article.date_published = today
                    if update_article_number:
                        article.article_number = "e" + str(article_number)
                        article_number += 1
                if self.update_db:
                    article.save()
                update_articles.append(article)
            elif fyear == 0 or self.container.with_online_first:
                if not article.date_online_first and not self.pre_publish:
                    # Online First
                    article.date_online_first = today
                    if self.update_db:
                        article.save()
                    update_articles.append(article)
                elif self.pre_publish:
                    article.date_pre_published = today
                    if self.update_db:
                        article.save()
                    update_articles.append(article)

    def internal_do(self):
        update_articles = []

        # In a collection with standalone articles, the article number is based on
        # the publication order.
        # It has to be updated when we publish because of the articles excluded for publication
        update_article_number = False
        # TODO: use a param instead of checking PCJ
        if self.container.my_collection.pid == "PCJ" and not self.pre_publish:
            update_article_number = True

        self.publish_article(self.article, update_articles)

        # TODO: update in upload articles ?

        # PCJ: update the article seq, it is used by the breadcrumb
        if update_article_number:
            i = 1
            for article in self.container.article_set.exclude(do_not_publish=True).order_by(
                "-date_published"
            ):
                article.seq = i
                i += 1
                article.save()

        return update_articles


class publishContainerDatabaseCmd(publishArticleDatabaseCmd):
    """
    Publish a container <=> Create a pub-date for all articles/book-parts of the container
    @var : fake : if True, return list of potentially updated articles BUT don't update database
    @return: list of updated articles
    """

    def __init__(self, params=None):
        super().__init__(params)

        self.required_params = ["container"]

    def set_container(self, container):
        self.container = container

    def internal_do(self):
        update_articles = []

        # In a collection with standalone articles, the article number is based on
        # the publication order.
        # It has to be updated when we publish because of the articles excluded for publication
        update_article_number = False
        # TODO: use a param instead of checking PCJ
        if self.container.my_collection.pid == "PCJ" and not self.pre_publish:
            update_article_number = True

        for article in self.container.article_set.exclude(do_not_publish=True):
            self.publish_article(article, update_articles)

        # PCJ: update the article seq, it is used by the breadcrumb
        if update_article_number:
            i = 1
            for article in self.container.article_set.exclude(do_not_publish=True).order_by(
                "-date_published"
            ):
                article.seq = i
                i += 1
                article.save()

        return update_articles

        # TODO Le container(book) peut également avoir une date de publication


def convert_contribs(pid, delete=False):
    if pid == "PCJ":
        # PCJ: pid start with "10_24072"
        pid = "10_24072"

    if delete:
        revert_contribs(pid)

    if pid is not None:
        qs = Contrib.objects.filter(
            Q(group__resource__pid__startswith=pid)
            | Q(group__bibitem__resource__pid__startswith=pid)
        )
    else:
        qs = Contrib.objects.all()[0:100]

    total = qs.count()
    i = 1
    percent = 0

    for contrib in qs:
        if int(i / total * 100) > percent:
            percent += 1
            if percent % 5 == 0:
                print(pid, percent)
        i += 1

        # contrib_fields_list = Contrib.get_fields_list()
        # contrib_fields = {
        #     attr_name: getattr(contrib, attr_name) for attr_name in contrib_fields_list
        # }

        # Augment the contrib attributes to match Contribution fields
        role = contrib.contrib_type
        if not role:
            role = contrib.group.content_type
        if not role:
            role = "author"
        if role[-1] == "s":
            role = role[0:-1]
        contrib.corresponding = role == "corresponding"
        contrib.role = role if role != "corresponding" else "author"

        ref_name = contrib.last_name if contrib.last_name else None
        if ref_name is None:
            ref_name = contrib.string_name if contrib.string_name else None
        letter = get_first_letter(ref_name) if ref_name else ""
        contrib.first_letter = letter

        contrib.deceased_before_publication = contrib.deceased
        contrib.idref = None
        contrib.mid = None

        contribution_fields_list = Contribution.get_fields_list()
        contributions_fields = {
            attr_name: getattr(contrib, attr_name) for attr_name in contribution_fields_list
        }

        contribution = Contribution(
            resource=contrib.group.resource, bibitem=contrib.group.bibitem, **contributions_fields
        )
        contribution.save()

        for contrib_address in contrib.contribaddress_set.all():
            contrib_address.contribution = contribution
            contrib_address.save()

    qs = Contribution.objects.filter(
        Q(resource__pid__startswith=pid) | Q(bibitem__resource__pid__startswith=pid)
    )
    print(qs.count())


def revert_contribs(pid):
    print(f"Delete Contribution for {pid}", end=" ")
    Contribution.objects.filter(
        Q(resource__pid__startswith=pid) | Q(bibitem__resource__pid__startswith=pid)
    ).delete()
    print("done")
