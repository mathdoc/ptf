import copy
import datetime
import os.path
import subprocess
import sys
import traceback

from lxml import ElementInclude
from lxml import etree

from django.conf import settings
from django.db import transaction
from django.db.models import Prefetch
from django.utils import timezone

from ptf import exceptions
from ptf import model_data
from ptf import model_data_comparator
from ptf import model_data_converter
from ptf import model_helpers
from ptf import tex
from ptf import utils
from ptf.cmds import ptf_cmds
from ptf.cmds import solr_cmds
from ptf.cmds.base_cmds import baseCmd
from ptf.cmds.xml import xml_utils
from ptf.cmds.xml.cedrics import cedrics_parser

# KEEP THIS UNUSED IMPORT THEY ARE USED
from ptf.cmds.xml.jats import jats_parser
from ptf.cmds.xml.jats import xmldata as xmldata_jats
from ptf.cmds.xml.xml_utils import normalize
from ptf.display import resolver

# from ptf.models import Resource
from ptf.models import Article
from ptf.models import Collection
from ptf.models import Container
from ptf.models import Person
from ptf.models import PtfSite
from ptf.models import backup_obj_not_in_metadata
from ptf.models import backup_translation
from ptf.models import restore_obj_not_in_metadata
from ptf.models import restore_translation


def find_file(name):
    paths = settings.MANAGER_XSLT_DIRS
    for path in paths:
        for root, _, files in os.walk(path):
            if name in files:
                return os.path.join(root, name)
    return None


def get_transform(name):
    file_path = find_file(f"{name}.xsl")
    xslt_doc = etree.parse(file_path)
    return etree.XSLT(xslt_doc)


class addXmlCmd(baseCmd):
    """
    addXmlCmd: base class for commands that take an XML as input
    The XML is passed with the body param

    from_folder / to_folder: location of binary files to copy

    Example with a file:
    f = open('journal.xml')
    body = f.read()
    f.close()
    cmd = add...XmlCmd( { "body":body } )

    Exception raised:
       - ValueError if the init params are empty
    """

    use_body = True
    body: str | None = None
    tree = None
    solr_commit_at_the_end = True
    xml_filename_in_log = None
    remove_blank_text = False
    xml_file_folder = None

    def __init__(self, params=None):
        super().__init__(params)

        if self.use_body:
            self.required_params.extend(["body"])

    def get_logname(self):
        filename = ""

        if hasattr(settings, "LOG_DIR"):
            i = 0
            today = datetime.date.today()
            basename = str(today) + "-" + self.__class__.__name__ + "-"
            filename = os.path.join(settings.LOG_DIR, basename + str(i) + ".xml")

            while os.path.isfile(filename):
                i += 1
                filename = os.path.join(settings.LOG_DIR, basename + str(i) + ".xml")

        return filename

    def pre_do(self):
        super().pre_do()

        if self.use_body:
            # The Cedrics -> JATS XSLT transform manually adds space=preserve around
            # the nodes with mixed-content, but leaves the text unchanged.
            # As such, parsing the Cedrics XML cannot be done with remove_blank_text=True
            # Or the spaces will be removed whereas the JATS XML will keep them.
            # We still need the remove_blank_text=True for JATS XML for all the other nodes
            parser = etree.XMLParser(
                huge_tree=True,
                recover=True,
                remove_blank_text=self.remove_blank_text,
                remove_comments=True,
                resolve_entities=True,
            )
            # if isinstance(self.body, str):
            #    self.body = self.body
            if self.xml_file_folder is not None:
                if self.xml_file_folder[-1] != "/":
                    self.xml_file_folder += "/"
                # For ElementInclude to find the href
                self.body = self.body.replace(
                    'xmlns:xlink="http://www.w3.org/1999/xlink"', ""
                ).replace("xlink:href", "href")
            tree = etree.fromstring(self.body.encode("utf-8"), parser=parser)

            if self.xml_file_folder is not None:
                ElementInclude.include(tree, base_url=self.xml_file_folder)
            # t = get_transform('strip-namespace')
            # self.tree = t(tree).getroot()
            self.tree = tree

            if self.tree is None:
                raise ValueError("tree est vide")

        # Write the xml body on disk
        if hasattr(settings, "LOG_DIR") and self.body and self.use_body:
            self.xml_filename_in_log = self.get_logname()

            with open(self.xml_filename_in_log, "w", encoding="utf-8") as file_:
                file_.write(self.body)

    @transaction.atomic
    def do(self, parent=None):
        try:
            obj = super().do(parent)
        except Exception as e:
            ptf_cmds.do_solr_rollback()

            # Empty sub_cmds to ignore undo
            self.cmds = []

            # Write the xml body on disk
            if hasattr(settings, "LOG_DIR") and self.body and self.use_body:
                with open(
                    os.path.join(settings.LOG_DIR, "cmds.log"), "a", encoding="utf-8"
                ) as file_:
                    file_.write("----------------------\n")

                    if self.xml_filename_in_log is None:
                        self.xml_filename_in_log = self.get_logname()

                    file_.write(self.xml_filename_in_log + " : FAILED\n")
                    exc_type, exc_value, exc_traceback = sys.exc_info()
                    lines = traceback.format_exception(exc_type, exc_value, exc_traceback)
                    for line in lines:
                        file_.write(line + "\n")
                    file_.write("----------------------\n")

            raise e

        if self.solr_commit_at_the_end:
            ptf_cmds.do_solr_commit()

        return obj

    def post_undo(self):
        super().post_undo()

        Person.objects.clean()

    def post_do(self, resource=None):
        super().post_do(resource)

        Person.objects.clean()

        if hasattr(settings, "LOG_DIR") and resource and self.use_body:
            today = datetime.date.today()
            basename = str(today) + "-" + self.__class__.__name__

            pids = ""
            first = True
            if isinstance(resource, list):
                for resource_item in resource:
                    if first:
                        first = False
                    else:
                        pids += ", "

                    pids += resource_item.pid
            else:
                pids = resource.pid

            with open(os.path.join(settings.LOG_DIR, "cmds.log"), "a", encoding="utf-8") as file_:
                file_.write(basename + " : " + pids + "\n")

            if hasattr(resource, "my_collection") and resource.my_collection:
                folder = os.path.join(
                    settings.LOG_DIR, resource.get_top_collection().pid, resource.pid
                )
                filename = os.path.join(folder, resource.pid + ".xml")
                resolver.create_folder(folder)
                with open(filename, "w", encoding="utf-8") as file_:
                    file_.write(self.body)

        # #if test, then raise an exeption if self.warnings not empty (in self.warnings we have all tags not parsed)
        # if 'test' in sys.argv:
        #     if len(self.warnings) > 0:
        #         print(self.warnings)
        #         raise UserWarning("All tags are not parsed", self.warnings)

    def undo(self):
        super().undo()

        if self.solr_commit_at_the_end:
            ptf_cmds.do_solr_commit()

    def add_objects_with_location(self, xobjs, resource, cmd_type):
        seq = 1

        for xobj in xobjs:
            base = None

            if xobj["base"]:
                base_name = xobj["base"]
                base = model_helpers.get_xmlbase(base_name)
                if base is None:
                    cmd = ptf_cmds.addXmlBasePtfCmd({"base": xobj["base"], "solr_commit": False})
                    base = cmd.do(self)

            rel = xobj["rel"]
            location = xobj["location"]

            params = {
                "rel": rel,
                "mimetype": xobj.get("mimetype", ""),
                "location": location,
                "seq": seq,
                "solr_commit": False,
                "from_folder": self.from_folder,
                "to_folder": self.to_folder,
            }

            # Ignore XML file
            if params["mimetype"] != "application/xml":
                if "metadata" in xobj:
                    params["metadata"] = xobj["metadata"]

                if "text" in xobj:
                    params["text"] = xobj["text"]

                # TODO: cmd factory ?
                cmd = None
                if cmd_type == "ExtLink":
                    cmd = ptf_cmds.addExtLinkPtfCmd(params)
                elif cmd_type == "RelatedObject":
                    cmd = ptf_cmds.addRelatedObjectPtfCmd(params)
                elif cmd_type == "SupplementaryMaterial":
                    params["caption"] = xobj.get("caption", "")
                    params["supplementary_material"] = True
                    cmd = ptf_cmds.addSupplementaryMaterialPtfCmd(params)
                elif cmd_type == "DataStream":
                    cmd = ptf_cmds.addDataStreamPtfCmd(params)

                # Always try to add an ExtLink or a RelatedObject
                # May raise ResourceExists if the ExtLink/RelatedObject is added twice

                if cmd is not None:
                    cmd.set_base(base)
                    cmd.set_resource(resource)

                    cmd.do(self)

                seq += 1

    # def add_metadata_parts(self, xobj, resource):
    #     for (seq, name, data) in xobj.metadataparts:
    #         params = {"name": name,
    #                   "data": data,
    #                   "seq": seq,
    #                   "solr_commit": False}
    #
    #         cmd = ptf_cmds.addMetaDataPartPtfCmd(params)
    #         cmd.set_resource(resource)
    #         cmd.do(self)

    @staticmethod
    def remove_publisher(publisher):
        cmd = ptf_cmds.addPublisherPtfCmd()
        cmd.set_object_to_be_deleted(publisher)
        cmd.undo()

    # Update the published years of a collection (journal/acta/book-series...)
    @staticmethod
    def update_collection_years(pid, container, save=True):
        collection = Collection.objects.get(pid=pid)
        if container.year:
            year = container.year
            fyear, lyear = model_helpers.get_first_last_years(year)
            fyear = int(fyear)
            lyear = int(lyear)

            if fyear < collection.fyear or not collection.fyear:
                collection.fyear = fyear

            if lyear > collection.lyear or not collection.lyear:
                collection.lyear = lyear

            if save:
                collection.save()


class addCollectionsXmlCmd(addXmlCmd):
    """
    addCollectionsXmlCmd: adds/remove a collection

    TODO: merge Collection and Journal ?

    Exception raised:
       - exceptions.ResourceExists during do
             if the Collection already exists
             if the collection defines the same extlink/relatedobject multiple times
       - exceptions.ResourceDoesNotExist
             during undo if the Collection does not exist
             during do of the provider does not exist
              <custom-meta-group><custom-meta><meta-name>provider</meta-name><meta-value>
       - RuntimeError during undo if resources are still published
    """

    provider = None
    xml_format = None

    def set_provider(self, provider):
        self.provider = provider

    def add_collection(self, xcol, update=False):
        if not xcol:
            return None

        if xcol.provider:
            provider = model_helpers.get_provider_by_name(xcol.provider)
        else:
            provider = self.provider

        col_id = xcol.pid
        collection = model_helpers.get_collection(col_id)

        existing = False

        if collection is not None:
            existing = True
            if not update:
                raise exceptions.ResourceExists(f"Collection {collection.pid} already exists")

        # Create a collection
        params = {
            "xobj": xcol,
            "from_folder": self.from_folder,
            "to_folder": self.to_folder,
            "solr_commit": False,
        }

        cls = ptf_cmds.addCollectionPtfCmd
        if update and existing:
            cls = ptf_cmds.updateCollectionPtfCmd

        cmd = cls(params)
        cmd.set_provider(provider)
        collection = cmd.do(self)

        self.add_objects_with_location(xcol.ext_links, collection, "ExtLink")

        # if publisher:
        #    model_helpers.publish_resource(publisher, journal)

        return collection

    def internal_do(self):
        super().internal_do()

        collections = []

        if self.tree.tag == "journal-meta":
            raise ValueError(
                "Creation of a journal on the fly from an article is not yet supported"
            )
            # # Code used when a journal is created on the fly while parsing an article (GDML - OAI)
            # # TODO 1 : Refactor all the JATS parsers (eudml/bdim/dmlcz/....)
            # #          to be compatible with jats_parser.py
            # # TODO 2 : Prevent the creation of the collection on the fly ?
            # #          Shouldn't the collection be monitored/controlled ?
            # xmldata = globals()[self.xml_format]
            # xcol = xmldata.Journal(self.tree)
            # collection = self.add_collection(xcol, update=True)
            # collections.append(collection)
        else:
            for node in self.tree:
                xcol = None
                if node.tag == "collection-meta":
                    raise ValueError("Collection can only be created from <publication-meta>")
                    # xcol = jats_parser.BitsCollection(tree=node)
                elif node.tag == "journal-meta":
                    raise ValueError(
                        "Collection can only be created from <publication-meta>, <journal-meta> are handled while parsing a <journal-issue>"
                    )
                    # xcol = jats_parser.JatsJournal(tree=node)
                elif node.tag == "publication-meta":
                    xcol = jats_parser.MathdocPublication(tree=node)

                collection = self.add_collection(xcol)
                collections.append(collection)

        return collections


class addIssueXmlCmd(addXmlCmd):
    """
     addIssueXmlCmd: adds/remove an issue

    from_folder / to_folder (declared in addXmlCmd): location of binary files to copy

     extra_folder: folder where extra data (extid false_positive...) are stored in a json
     It is used
     - when you call addIssueXmlCmd directly to import from an archive,
     - when you call addOrUpdateIssueXmlCmd and we need to restore extra data after the import

     Exception raised:
    - exceptions.ResourceExists during do if the issue already exists
    - exceptions.ResourceDoesNotExist
           during undo if the Issue does not exist
           during do if the serial/provider does not exist
             <custom-meta-group><custom-meta><meta-name>provider</meta-name><meta-value>
    - RuntimeError during undo if resources are still published
    """

    assign_doi = False
    full_text_folder = ""
    extra_folder = None
    prod_deployed_date_iso_8601_date_str = None
    xissue = None
    count = 0
    no_bib = False  # Ignore the references during the import (used in Geodesic)
    embargo = False  # Import only the open articles (used in Geodesic)

    def create_child_collection(self, xjournal, journal):
        issn = xjournal.issn if xjournal.issn else xjournal.e_issn

        new_xjournal = copy.deepcopy(xjournal)
        new_xjournal.wall = 0
        new_xjournal.pid = f"{xjournal.pid}-{issn}"
        new_xjournal.coltype = journal.coltype

        params = {"xobj": new_xjournal}
        provider = model_helpers.get_provider_by_name("mathdoc")

        cmd = ptf_cmds.addCollectionPtfCmd(params)
        cmd.set_parent(journal)
        cmd.set_provider(provider)

        collection = cmd.do()
        # collection.parent = journal
        # journal = collection
        return collection

    def get_historic_collection(self, xjournal, journal):
        use_meta_collections = (
            settings.USE_META_COLLECTIONS if hasattr(settings, "USE_META_COLLECTIONS") else False
        )

        if not use_meta_collections:
            return journal

        # meta-collections are used : journal may be the top collection or one of its children

        value = id_type = None

        # Take care of special case of STNB :
        # For that, we ignore the issn of STNB 2nd series
        if xjournal.pid == "JTNB" and xjournal.issn == "0989-5558":
            xjournal.issn = None
            xjournal.e_issn = None
            xjournal.ids = []
        else:
            if xjournal.issn:
                value = xjournal.issn
                id_type = "issn"
            elif xjournal.e_issn:
                value = xjournal.e_issn
                id_type = "e-issn"

        if value:
            # collection has at least one issn
            qs = Collection.objects.filter(resourceid__id_value=value, resourceid__id_type=id_type)
            if qs.exists():
                journal = qs.first()
            else:
                # xjournal does not exist yet.
                journal = self.create_child_collection(xjournal, journal)
        else:
            # collection has no issn
            possible_pids = [xjournal.pid, f"{xjournal.pid}-{value}"]
            qs = Collection.objects.exclude(resourceid__id_value__isnull=False).filter(
                pid__in=possible_pids
            )
            if qs.exists():
                journal = qs.first()
            else:
                journal = self.create_child_collection(xjournal, journal)

        return journal

    def internal_do(self):
        super().internal_do()

        #######################################################################
        # get xissue

        if self.xissue:
            xissue = self.xissue
        else:
            xissue = jats_parser.JatsIssue(tree=self.tree, no_bib=self.no_bib)
            self.warnings.extend(xissue.warnings)

        #######################################################################
        # Check if there is an existing issue / journal

        issue_id = xissue.pid
        issue = model_helpers.get_container(issue_id)

        if issue is not None:
            raise exceptions.ResourceExists(f"Issue {issue_id} already exists")

        xjournal = xissue.journal
        journal_id = xjournal.pid
        journal = model_helpers.get_collection(journal_id)

        # Note: Why use <issue-meta><custom-meta-group><custom-meta> to find the provider and then the journal
        #       as there is a <journal-meta> with an id ?
        #       The ptf_resource table (Resource objects) are created with only 1 id.
        #       When you add a journal, the journal id is the one of its
        #       <custom-meta-group><custom-meta> provider.
        #       If you want to find the journal of an issue based on the <journal-meta> information, you might
        #       have to search among the other ids (ptf_resourceid table, ResourceId objects) : sql JOIN select
        #       To avoid the join select, it's better to use <issue-meta><custom-meta-group><custom-meta> to make sure
        #       we use the correct provider. A simple select in the ptf_resource table is then needed.
        if journal is None:
            raise exceptions.ResourceDoesNotExist(f"Journal {journal_id} does not exist")

        # Journal is the top collection (ex: AFST)
        # We want to get (or create) the journal that corresponds to the issue
        journal = self.get_historic_collection(xjournal, journal)

        if self.embargo and journal.wall > 0:
            # Geodesic is for open access articles.
            # We do not want to import the issues under embargo
            if resolver.embargo(journal.wall, xissue.year):
                print(f"Embargo, ignore {xissue.pid}")
                return None

        #######################################################################
        # Get provider/publisher

        provider_name = xissue.provider if xissue.provider else "mathdoc"
        provider = model_helpers.get_provider_by_name(provider_name)

        #######################################################################
        # Add the issue

        params = {
            "xobj": xissue,
            "pid": xissue.pid,
            "from_folder": self.from_folder,
            "to_folder": self.to_folder,
            "solr_commit": False,
        }

        cmd = ptf_cmds.addContainerPtfCmd(params)
        cmd.add_collection(journal)
        cmd.set_provider(provider)
        issue = cmd.do(self)

        self.add_objects_with_location(xissue.ext_links, issue, "ExtLink")
        self.add_objects_with_location(xissue.related_objects, issue, "RelatedObject")
        self.add_objects_with_location(xissue.streams, issue, "DataStream")

        #######################################################################
        # Add the issue's articles

        # JatsIssue is an iterator (has the __iter__ function)
        # you simply iterate the xissue to get its articles
        if xissue.ctype == "issue":
            for seq, xarticle in enumerate(xissue, start=1):
                params = {
                    "xarticle": xarticle,
                    "journal": journal,
                    "issue": issue,
                    "seq": seq,
                    "provider": provider,
                    "assign_doi": self.assign_doi,
                    "full_text_folder": self.full_text_folder,
                    "use_body": False,
                    "from_folder": self.from_folder,
                    "to_folder": self.to_folder,
                    "solr_commit_at_the_end": False,
                }
                cmd = addArticleXmlCmd(params)
                cmd.do(self)
        elif xissue.ctype == "issue_special":
            site = PtfSite.objects.get(id=settings.SITE_ID)
            issue.deploy(site)
            for seq, xresource in enumerate(xissue.articles, start=1):
                # en fait on peut appeler directement la ptfCMD
                # et on peut supprimer la xml cmd

                params = {
                    "use_body": False,
                    "xcontainer": issue,
                    "seq": seq,
                    # on veut juste passer le champ resource_doi dans ma fonction
                    "xresource": xresource,
                    "resource_doi": xresource.doi,
                }
                cmd = addResourceInSpecialIssueXmlCmd(params)
                cmd.do(self)

        # Update the top journal first year and last year
        self.update_collection_years(journal_id, issue)

        # The collection maybe updated with update_collection_years and the assign_doi param (col.last_doi)
        # Update issue before returning the object.
        # Note that refresh_from_db does not update ForeignKey fields, we can't simply call issue.refresh_from_db()
        issue.my_collection.refresh_from_db()

        # Used in post_do
        self._prod_deployed_date_iso_8601_date_str = xissue.prod_deployed_date_iso_8601_date_str

        return issue

    def post_do(self, resource=None):
        super().post_do(resource)

        # Si le XML de l'issue a une last-modified, on la garde, sinon on en créé une.
        if resource.last_modified is None:
            resource.last_modified = timezone.now()
            resource.save()

        # Sur ptf-tools, si le XML de l'issue a une prod_deployed_date,
        # On la propage aux Articles/Issue.
        # La restoration éventuelle des données (avec importExtraDataPtfCmd) peut écraser prod_deployed_date
        if self._prod_deployed_date_iso_8601_date_str and settings.SITE_NAME == "ptf_tools":
            prod_deployed_date = model_helpers.parse_date_str(
                self._prod_deployed_date_iso_8601_date_str
            )
            journal_site = model_helpers.get_site_mersenne(resource.my_collection.pid)
            if journal_site:
                model_helpers.update_deployed_date(resource, journal_site, prod_deployed_date)

        if self.extra_folder:
            ptf_cmds.importExtraDataPtfCmd(
                {"pid": resource.pid, "import_folder": self.extra_folder}
            ).do()


class addResourceInSpecialIssueXmlCmd(addXmlCmd):
    """
    addResourceXmlCmd: adds/remove resource from special issue
    """

    xcontainer = None
    resource_doi = ""
    xresource = None
    seq = 0
    citation = ""
    provider = None

    def __init__(self, params=None):
        super().__init__(params)
        self.required_params.extend(["xcontainer"])

    def internal_do(self):
        super().internal_do()
        # for later, check the type of the resource first
        resource_in_special_issue = model_helpers.get_resource_in_special_issue_by_doi(
            self.resource_doi
        )
        resource_doi = self.resource_doi

        # if self.xcontainer:
        container = model_helpers.get_container(self.xcontainer.pid)

        seq = self.seq
        # needs_to_restore_resource = False

        if resource_in_special_issue is not None:
            # temporary
            raise ValueError(
                "First step of developpement require to manually delete all resources in special issue"
            )
        # self.provider = self.xresource.provider
        # 2 is the id of ptf_tools. If we are not in ptf tools we are dealing with jats article which has no citation
        if settings.SITE_ID == 2:
            citation = self.xresource["citation"]
        else:
            citation = ""
        params = {
            # "xobj": self.xresource,
            "obj_doi": resource_doi,
            "container": container,
            "seq": seq,
            "citation": citation,
            # "provider": self.provider,
        }

        cmd = ptf_cmds.addResourceInSpecialIssuePtfCmd(params)
        resource_in_special_issue = cmd.do(self)

        return resource_in_special_issue


class addArticleXmlCmd(addXmlCmd):
    """
    addArticleXmlCmd: adds/remove an issue

    Exception raised:
       - exceptions.ResourceExists during do if the article already exists
       - exceptions.ResourceDoesNotExist
             during undo if the Article does not exist
             during do if the serial/issue/provider does not exist
             <custom-meta-group><custom-meta><meta-name>provider</meta-name><meta-value>
    """

    xarticle = None
    journal = None
    issue = None
    provider = None
    provider_col = None
    assign_doi = False
    full_text_folder = ""
    xml_format = "xmldata_jats"
    # restricted_mode is used by maxiDML. We do not try to import all the metadata, but only a subset
    restricted_mode = False
    # standalone is used to import isolated article, without issues
    standalone = False
    seq = (
        0  # seq is used by the breadcrumbs. Generate it if it's not specified in the XML (ex: PCJ)
    )
    keep_translations = False

    def set_collection(self, collection):
        self.journal = collection
        self.provider = collection.provider

    def set_xml_format(self, xml_format):
        self.xml_format = xml_format

    def set_provider(self, provider):
        self.provider = provider

    def set_provider_col(self, provider_col):
        self.provider_col = provider_col

    def set_article_single_mode(self):
        self.xarticle = jats_parser.JatsArticle(tree=self.tree)
        self.warnings.extend(self.xarticle.warnings)

        # TODO: MaxiDML: allow the creation of an issue on the fly
        # if not self.provider:
        #     self.provider = model_helpers.get_provider_by_name(self.xarticle.provider)
        #
        # xmldata_jats.set_pid_type(self.provider.pid_type)
        #
        # bdy = etree.tostring(self.xarticle.journal.tree).decode("utf-8")
        # cmd = addCollectionsXmlCmd({'body': bdy,
        #                             'xml_format': self.xml_format,
        #                             'coltype': "journal"})
        # cmd.set_provider(self.provider_col if self.provider_col else self.provider)
        # self.journal = cmd.do()[0]
        #
        # self.issue = model_helpers.get_container(self.xarticle.issue_id)
        # if self.issue is None:
        #     # need to create the issue
        #     date = datetime.datetime.strptime(self.xarticle.date_published_iso_8601_date_str,
        #                                       '%Y-%m-%d')
        #     pid = "{name}_{year}".format(name=self.journal.pid, year=date.year)
        #     self.issue = model_helpers.get_container(pid)
        #     if self.issue is None:
        #         params = {'ctype': 'issue', 'year': date.year, 'pid': pid,
        #                   'last_modified_iso_8601_date_str': datetime.datetime.now().strftime(
        #                       "%Y-%m-%d %H:%M:%S"), 'volume': self.xarticle.volume,
        #                   # if copy binary, need from_folder / to_folder
        #                   }
        #
        #         cmd = ptf_cmds.addContainerPtfCmd(params)
        #         cmd.add_collection(self.journal)
        #         cmd.set_provider(self.provider)
        #         self.issue = cmd.do()

    def get_oai_identifier(self):
        return self.xarticle.oai_identifier

    def update_xobj_with_body(self):
        # Import CEDRICS, le plein texte provient d'un fichier séparé
        if self.full_text_folder and not self.xarticle.body:
            if self.full_text_folder == settings.CEDRAM_TEX_FOLDER:
                text = ""
                locs = [
                    stream["location"]
                    for stream in self.xarticle.streams
                    if stream["mimetype"] == "application/pdf"
                ]
                if locs:
                    full_pdf_location = os.path.join(self.full_text_folder, locs[0])
                    text = utils.pdf_to_text(full_pdf_location)
                self.xarticle.body = text
            else:
                full_text_file = self.full_text_folder + self.xarticle.pid + ".xml"

                with open(full_text_file, mode="rb") as file_:
                    body = file_.read()

                parser = etree.XMLParser(huge_tree=True, recover=True)
                tree = etree.fromstring(body, parser=parser)
                node = tree.find("body")
                self.xarticle.body = xml_utils.get_text_from_node(node)
            self.xarticle.body_xml = xml_utils.get_xml_from_text("body", self.xarticle.body)
        elif not self.xarticle.body_xml and hasattr(self.xarticle, "pii"):
            full_text_file = os.path.join(
                "/numdam_dev/acquisition/donnees_traitees",
                self.journal.pid,
                self.issue.pid,
                self.xarticle.pid,
                self.xarticle.pid + ".xml",
            )
            if os.path.isfile(full_text_file):
                with open(full_text_file, mode="rb") as file_:
                    body = file_.read()

                parser = etree.XMLParser(huge_tree=True, recover=True)
                tree = etree.fromstring(body, parser=parser)
                node = tree.find("body")
                self.xarticle.body = xml_utils.get_text_from_node(node)
                self.xarticle.body_xml = xml_utils.get_xml_from_text("body", self.xarticle.body)

    def internal_do(self):
        super().internal_do()

        if self.xarticle is None and self.journal is not None:
            # self.restricted_mode = True
            self.set_article_single_mode()
            self.update = True
        else:
            self.update = False

        if self.xarticle.pid is None:
            self.xarticle.pid = (
                self.xarticle.doi.replace("/", "_").replace(".", "_").replace("-", "_")
            )

        for xtranslated_article in self.xarticle.translations:
            for xtream in xtranslated_article.streams:
                if xtream["mimetype"] == "text/html":
                    if self.from_folder is None:
                        raise ValueError(
                            "The article has its full text in a separate HTML file. You need to set from_folder"
                        )

                    location = os.path.join(self.from_folder, xtream["location"])
                    body_html = resolver.get_body(location)
                    body = xml_utils.get_text_from_xml_with_mathml(body_html)
                    xtranslated_article.body_html = body_html
                    xtranslated_article.body = body

        for stream in self.xarticle.streams:
            if stream["mimetype"] == "text/html":
                location = os.path.join(self.from_folder, stream["location"])
                body_html = resolver.get_body(location)
                body = xml_utils.get_text_from_xml_with_mathml(body_html)
                self.xarticle.body_html = body_html
                self.xarticle.body = body

        if self.xarticle.doi:
            article = model_helpers.get_article_by_doi(self.xarticle.doi)
        else:
            article = model_helpers.get_article(self.xarticle.pid)
        needs_to_restore_article = False

        if article is not None:
            if self.update or self.standalone:
                if self.standalone:
                    self.provider = article.provider

                needs_to_restore_article = True
                backup_obj_not_in_metadata(article)

                if self.keep_translations:
                    backup_translation(article)

                cmd = ptf_cmds.addArticlePtfCmd(
                    {
                        "pid": article.pid,
                        "to_folder": self.to_folder,  # on supprime les fichiers pour être sûr
                    }
                )
                cmd.set_object_to_be_deleted(article)
                cmd.undo()
            else:
                raise exceptions.ResourceExists(f"Article {self.xarticle.pid} already exists")

        # Override seq
        if self.standalone and article is not None:
            self.xarticle.seq = article.seq
        elif (
            not self.standalone and self.issue and int(self.xarticle.seq) == 0 and self.seq != 0
        ) or (hasattr(self, "pii") and self.seq != 0):
            self.xarticle.seq = self.seq

        # Get the article's text (body) for SolR if it is empty from the PDF
        self.update_xobj_with_body()

        params = {
            "xobj": self.xarticle,
            "pid": self.xarticle.pid,
            "from_folder": self.from_folder,
            "to_folder": self.to_folder,
            "assign_doi": self.assign_doi and not self.xarticle.doi,
            "solr_commit": False,
        }

        cmd = ptf_cmds.addArticlePtfCmd(params)
        if self.issue or not self.standalone:
            cmd.set_container(self.issue)
        cmd.add_collection(self.journal)
        article = cmd.do(self)

        self.add_objects_with_location(self.xarticle.ext_links, article, "ExtLink")
        self.add_objects_with_location(self.xarticle.streams, article, "DataStream")
        if not self.restricted_mode:
            self.add_objects_with_location(
                self.xarticle.supplementary_materials, article, "SupplementaryMaterial"
            )

        if (
            hasattr(settings, "SHOW_BODY") and settings.SHOW_BODY
        ) or settings.SITE_NAME == "ptf_tools":
            self.add_objects_with_location(self.xarticle.figures, article, "RelatedObject")

        for xtrans_article, trans_article in zip(
            self.xarticle.translations, cmd.cmd.translated_articles
        ):
            self.add_objects_with_location(xtrans_article.streams, trans_article, "DataStream")

        if needs_to_restore_article:
            restore_obj_not_in_metadata(article)

            if self.keep_translations:
                restore_translation(article)

        return article


class addTranslatedArticleXmlCmd(addXmlCmd):
    """
    addTranslatedArticleXmlCmd: adds/remove translations.
       The original article is not changed
       The current translations are first removed
    """

    lang = ""
    html_file_name = ""
    pdf_file_name = ""
    date_published_str = ""

    def internal_do(self):
        super().internal_do()

        xarticle = jats_parser.JatsArticle(tree=self.tree)
        article = model_helpers.get_article(xarticle.pid)

        if article is None:
            raise exceptions.ResourceDoesNotExist(f"Article {self.xarticle.pid} does not exist")

        # Merge existing article with new translation
        data_article = model_data_converter.db_to_article_data(article)
        new_translations = [
            translation
            for translation in data_article.translations
            if translation.lang != self.lang
        ]

        for xtrans_article in xarticle.translations:
            if xtrans_article.lang == self.lang:
                # Upload/views has copied the HTML file on disk
                # Add a DataStream.
                # TODO: check if the datastream is not already present
                if self.html_file_name:
                    data = model_data.create_datastream()
                    data["rel"] = "full-text"
                    data["mimetype"] = "text/html"
                    data["location"] = self.html_file_name
                    xtrans_article.streams.append(data)

                if self.pdf_file_name:
                    # Create a pdf file
                    # pdf-translate needs the article/sub-article XML
                    # Simply add a datastream for now
                    # The new Article created in Django will be complete
                    # But generate the PDF file at the end
                    data = model_data.create_datastream()
                    data["rel"] = "full-text"
                    data["mimetype"] = "application/pdf"
                    data["location"] = self.pdf_file_name
                    xtrans_article.streams.append(data)

                if self.date_published_str:
                    xtrans_article.date_published_iso_8601_date_str = self.date_published_str

                new_translations.append(xtrans_article)

        data_article.translations = new_translations

        cmd = addArticleXmlCmd(
            {
                "xarticle": data_article,
                "use_body": False,
                "issue": article.my_container,
                "standalone": True,
                "from_folder": self.from_folder,
            }
        )
        cmd.set_collection(article.get_collection())
        article = cmd.do()

        # pdf-translate needs the article/sub-article XML
        xml = ptf_cmds.exportPtfCmd(
            {
                "pid": article.pid,
                "with_body": False,
                "with_djvu": False,
                "article_standalone": True,
                "collection_pid": settings.COLLECTION_PID,
            }
        ).do()

        tex.create_translated_pdf(
            article,
            xml,
            self.lang,
            os.path.join(self.from_folder, self.pdf_file_name),
            os.path.join(self.from_folder, self.html_file_name),
            # If the date_published is specified, we assume that the PDF already exists
            skip_compilation=self.date_published_str != "",
        )

        return article


class addPCJArticleXmlCmd(addXmlCmd):
    """
    addPCJArticleXmlCmd:
    """

    html_file_name = ""

    def internal_do(self):
        super().internal_do()

        xarticle = jats_parser.JatsArticle(tree=self.tree)

        if self.html_file_name:
            data = model_data.create_datastream()
            data["rel"] = "full-text"
            data["mimetype"] = "text/html"
            data["location"] = self.html_file_name
            xarticle.streams.append(data)

        cmd = addArticleXmlCmd(
            {
                "xarticle": xarticle,
                "use_body": False,
                "issue": self.issue,
                "standalone": True,
                "from_folder": self.from_folder,
            }
        )
        cmd.set_collection(self.collection)
        article = cmd.do()

        return article


class addBookXmlCmd(addXmlCmd):
    """
    addBookXmlCmd: adds/remove a book

    Exception raised:
       - exceptions.ResourceExists during do if the book already exists
       - exceptions.ResourceDoesNotExist
              during undo if the Book does not exist
              during do if the serial/provider does not exist
                <custom-meta-group><custom-meta><meta-name>provider</meta-name><meta-value>
       - RuntimeError during undo if resources are still published
    """

    provider = None
    import_oai_mode = False
    journal = None
    xml_format = "xmldata_jats"
    xbook = None
    _collection = None
    no_bib = False  # Ignore the references during the import (used in Geodesic)

    def set_provider(self, provider):
        self.provider = provider

    def add_parts(self, xparts, pseq):
        if xparts:
            seq = 1
            for xpart in xparts:
                self.add_part(xpart, seq, pseq)
                seq += 1

    def add_part(self, xpart, seq, pseq):
        if xpart is None:
            return

        # An Article is used to store a book part in the database
        article = model_helpers.get_article(xpart.pid)

        if article is not None:
            raise exceptions.ResourceExists(f"BookPart {xpart.pid} already exists")

        params = {
            "xobj": xpart,
            "pid": xpart.pid,
            "seq": seq,
            "pseq": pseq,
            # "deployed": deployed,
            "from_folder": self.from_folder,
            "to_folder": self.to_folder,
            "solr_commit": False,
        }

        cmd = ptf_cmds.addBookPartPtfCmd(params)
        cmd.set_container(self.book)
        cmd.add_collection(self._collection)
        article = cmd.do(self)

        self.add_objects_with_location(xpart.ext_links, article, "ExtLink")
        self.add_objects_with_location(xpart.streams, article, "DataStream")

        self.add_parts(xpart.parts, seq)

    def set_import_oai_mode(self):
        self.import_oai_mode = True

    def internal_do(self):
        super().internal_do()

        #######################################################################
        # Get xbook

        if self.import_oai_mode:
            xmldata = globals()[self.xml_format]
            xbook = xmldata.Book(self.tree)
            self.journal = model_helpers.get_collection("GDML_Books")

        else:
            if self.xbook:
                xbook = self.xbook
            else:
                xbook = jats_parser.BitsBook(tree=self.tree, no_bib=self.no_bib)
                self.warnings.extend(xbook.warnings)

        #######################################################################
        # Get existing book if any

        if not self.provider:
            provider = model_helpers.get_provider_by_name(xbook.provider)
            self.provider = provider

        book_id = xbook.pid
        book = model_helpers.get_container(book_id)

        #######################################################################
        # Delete any existing book

        if book is not None:
            if self.import_oai_mode:
                publisher = book.my_publisher

                # Note: the existing collection is not removed even if it no longer has a resource
                # TODO: urls/commands to add/update/delete a collection

                # Removes the book
                cmd = ptf_cmds.addContainerPtfCmd()
                cmd.set_object_to_be_deleted(book)
                cmd.undo()

                if publisher and publisher.publishes.count() == 0:
                    self.remove_publisher(publisher)
            else:
                raise exceptions.ResourceExists("Book %s already exists" % book_id)

        #######################################################################
        # Add new book

        if xbook.incollection:
            colid = xbook.incollection[0].pid
            self._collection = model_helpers.get_collection(colid)
            if self._collection is None:
                raise exceptions.ResourceDoesNotExist(f"The collection {colid} does not exist")
        elif self.import_oai_mode:
            self._collection = self.journal

        params = {
            "xobj": xbook,
            "pid": xbook.pid,
            "from_folder": self.from_folder,
            "to_folder": self.to_folder,
            "solr_commit": False,
        }

        cmd = ptf_cmds.addContainerPtfCmd(params)
        cmd.add_collection(self._collection)
        cmd.set_provider(provider)

        book = cmd.do(self)
        self.book = book

        self.add_objects_with_location(xbook.ext_links, book, "ExtLink")
        self.add_objects_with_location(xbook.related_objects, book, "RelatedObject")
        self.add_objects_with_location(xbook.streams, book, "DataStream")

        # self.add_metadata_parts(xbook, book)  TODO support Metadataparts ?

        #######################################################################
        # Add Book parts

        # JatsIssue is an iterator (has the __iter__ function)
        # TODO make JatsBook an iterator as well ?
        self.add_parts(xbook.parts, 0)

        # Update the collection first year and last year
        for incol in xbook.incollection:
            self.update_collection_years(incol.pid, book)

        return book


######################################################################################
######################################################################################
#
#                                 Update Commands
#
######################################################################################
######################################################################################


class updateCollectionsXmlCmd(addXmlCmd):
    """
    updateSerialsXmlCmd: updates one or more journals

    Exception raised:
       - exceptions.ResourceDoesNotExist during do if the Collection does not exist
       - RuntimeError if undo is called
    """

    def update_collection(self, xcol, do_update=True):
        if not xcol:
            return None

        provider = model_helpers.get_provider_by_name(xcol.provider)

        col_id = xcol.pid
        col = model_helpers.get_collection(col_id)

        if col is None:
            raise exceptions.ResourceDoesNotExist("Collection %s does not exist" % xcol.pid)

        if do_update:
            params = {
                "xobj": xcol,
                "solr_commit": False,
                "from_folder": self.from_folder,
                "to_folder": self.to_folder,
            }

            # The existing other_ids, abstracts are removed in updateCollectionDatabaseCmd::internal_do
            # and the new ones are added in the post_do (addResourceDatabaseCmd)

            cmd = ptf_cmds.updateCollectionPtfCmd(params)
            cmd.set_provider(provider)
            # cmd.set_publisher(publisher)
            col = cmd.do()

            # The existing extlinks are removed in updateCollectionDatabaseCmd::internal_do
            self.add_objects_with_location(xcol.ext_links, col, "ExtLink")
            resolver.copy_binary_files(col, self.from_folder, self.to_folder)

            # if publisher:
            #    model_helpers.publish_resource(publisher, col)

        return col

    def internal_do(self):
        super().internal_do()

        collections = []

        # First, check that all journals exist
        for node in self.tree:
            xcol = None
            if node.tag == "collection-meta":
                xcol = jats_parser.BitsCollection(tree=node)
            elif node.tag == "journal-meta":
                xcol = jats_parser.JatsJournal(tree=node)
            elif node.tag == "publication-meta":
                xcol = jats_parser.MathdocPublication(tree=node)
            self.update_collection(xcol, False)

        for node in self.tree:
            xcol = None
            if node.tag == "collection-meta":
                xcol = jats_parser.BitsCollection(tree=node)
            elif node.tag == "journal-meta":
                xcol = jats_parser.JatsJournal(tree=node)
            elif node.tag == "publication-meta":
                xcol = jats_parser.MathdocPublication(tree=node)
            self.warnings.extend(xcol.warnings)
            xcol = self.update_collection(xcol)
            collections.append(xcol)

        return collections

    def internal_undo(self):
        raise RuntimeError("update commands do not support the undo")


#####################################################################
#
# replaceIssueXmlCmd: updates an issue
#
# Exception raised:
#    - exceptions.ResourceDoesNotExist during do if the Collection/Issue/Provider does not exist
#       <custom-meta-group><custom-meta><meta-name>provider</meta-name><meta-value>
#    - RuntimeError if undo is called
#
######################################################################
class replaceIssueXmlCmd(addXmlCmd):
    def internal_do(self):
        super().internal_do()

        xissue = jats_parser.JatsIssue(tree=self.tree)
        self.warnings.extend(xissue.warnings)

        xjournal = xissue.journal
        journal_id = xjournal.pid
        journal = model_helpers.get_collection(journal_id)

        if journal is None:
            raise exceptions.ResourceDoesNotExist("Journal %s does not exist" % xjournal.pid)

        issue_id = xissue.pid
        issue = model_helpers.get_container(issue_id)

        if issue is None:
            raise exceptions.ResourceDoesNotExist("Issue %s does not exist" % issue_id)

        publisher = issue.my_publisher

        cmd = ptf_cmds.addContainerPtfCmd()
        cmd.set_object_to_be_deleted(issue)
        cmd.undo()

        if publisher.publishes.count() == 0:
            self.remove_publisher(publisher)

        # update the journal first and last year
        for the_issue in journal.content.all():
            self.update_collection_years(journal_id, the_issue, False)

        journal.save()

        cmd = addIssueXmlCmd(
            {
                "xissue": xissue,
                "use_body": False,
                "solr_commit": False,
                "extra_folder": self.from_folder,
                "to_folder": self.to_folder,
            }
        )
        issue = cmd.do()

        return issue

        # node_tag = self.tree.tag
        # for child in self.tree:
        #     node_tag = child.tag

    def internal_undo(self):
        raise RuntimeError("update commands do not support the undo")


class updateBookXmlCmd(addXmlCmd):
    """
    updateBookXmlCmd: updates a book

    Exception raised:
       - exceptions.ResourceDoesNotExist during do if the Book does not exist
       - RuntimeError if undo is called
    """

    no_bib = False  # Ignore the references during the import (used in Geodesic)

    def internal_do(self):
        super().internal_do()

        xbook = jats_parser.BitsBook(tree=self.tree, no_bib=self.no_bib)
        self.warnings.extend(xbook.warnings)

        book_id = xbook.pid
        book = model_helpers.get_container(book_id)

        if book is None:
            raise exceptions.ResourceDoesNotExist("Book %s does not exist" % xbook.pid)

        # unpublish and delete the existing publisher if necessary
        # self.update_publisher(xbook, book)

        # Note: the existing collection is not removed even if it no longer has a resource
        # TODO: urls/commands to add/update/delete a collection

        # Removes the book
        cmd = ptf_cmds.addContainerPtfCmd()
        cmd.set_object_to_be_deleted(book)
        cmd.undo()

        cmd = addBookXmlCmd(
            {
                "xbook": xbook,
                "use_body": False,
                "solr_commit": False,
                "from_folder": self.from_folder,
                "no_bib": self.no_bib,
                "to_folder": self.to_folder,
            }
        )
        book = cmd.do()

        return book

    def internal_undo(self):
        raise RuntimeError("update commands do not support the undo")


class addOrUpdateContainerXmlCmd(addXmlCmd):
    """
    addOrUpdateContainerXmlCmd: detects Container type from xml and adds or updates an issue or a book

    just detect Container type (do not check params etc.)
    """

    keep_metadata = False
    keep_translations = False
    backup_folder = None
    full_text_folder = ""
    fake = False  # Parse the XML but do not import
    no_bib = False  # Ignore the references during the import (used in Geodesic)
    embargo = False  # Import only the open articles (used in Geodesic)

    def check_params(self):
        super().check_params()

    def internal_do(self):
        super().internal_do()

        tag = normalize(self.tree.tag)

        if tag == "journal-issue":
            cmd = addOrUpdateIssueXmlCmd(
                {
                    "body": self.body,
                    "keep_metadata": self.keep_metadata,
                    "keep_translations": self.keep_translations,
                    "backup_folder": self.backup_folder,
                    "to_folder": self.to_folder,
                    "from_folder": self.from_folder,
                    "xml_file_folder": self.xml_file_folder,
                    "fake": self.fake,
                    "no_bib": self.no_bib,
                    "embargo": self.embargo,
                }
            )
            obj = cmd.do()
            self.warnings.extend(cmd.warnings)
            return obj
        elif tag == "book":
            cmd = addOrUpdateBookXmlCmd(
                {
                    "body": self.body,
                    "from_folder": self.from_folder,
                    "to_folder": self.to_folder,
                    "no_bib": self.no_bib,
                    "embargo": self.embargo,
                }
            )
            obj = cmd.do()
            self.warnings.extend(cmd.warnings)
            return obj
        else:
            raise RuntimeError("addOrupdateContainer command can't detect container type")

    def internal_undo(self):
        raise RuntimeError("update commands do not support the undo")


class addOrUpdateIssueXmlCmd(addXmlCmd):
    """
    addOrUpdateIssueXmlCmd: adds or updates an issue

    Adds an issue if it is not in the system or updates the issue if it is already there.
    By default, no DOI is assigned for the articles. Set assign_doi to True.

    from_folder / to_folder (declared in addXmlCmd): location of binary files to copy
    backup_folder: folder where extra data (extid false_positive...) are (to be) stored in a json

    keep_metadata:
        True if you want to back up extra data (icon, dates, matching ids, ...) in the backup_folder
        Default: False
        Note: backup_obj_not_in_metadata / restore_obj_not_in_metadata is always called
              We always want to preserve GraphicalAbstracts (they are not in the issue XML)

    keep_translations:
        True if you want back up/restore translations.
        Default: False
        Note: When you post an article to a journal (test) website, the translation is declared in the XML
              But if you import a Cedrics article in Trammel, the XML does not list translations

    Exception raised:
       - exceptions.ResourceDoesNotExist during do if the Collection/Issue/Provider does not exist
          <custom-meta-group><custom-meta><meta-name>provider</meta-name><meta-value>
       - RuntimeError if undo is called
    """

    keep_metadata = False
    keep_translations = False
    backup_folder = None
    assign_doi = False
    full_text_folder = ""

    xissue = None
    fake = False  # Parse the XML but do not import
    no_bib = False  # Ignore the references during the import (used in Geodesic)
    embargo = False  # Import only the open articles (used in Geodesic)

    def check_params(self):
        super().check_params()

        if self.keep_metadata and self.assign_doi:
            raise ValueError("keep_metadata and assign_doi cannot both be true.")

        if self.keep_metadata and self.backup_folder is None:
            raise ValueError("backup_folder needs to be set when keep_metadata is true.")

    def internal_do(self):
        super().internal_do()

        if not self.xissue:
            self.xissue = xissue = jats_parser.JatsIssue(
                tree=self.tree,
                from_folder=self.from_folder,
                no_bib=self.no_bib,
            )
            if len(xissue.warnings) > 0 and self.xml_file_folder:
                warnings = []
                warning_keys = []
                for warning in xissue.warnings:
                    for key, value in warning.items():
                        if value not in warning_keys:
                            warning_keys.append(value)
                            warnings.append({key: value})
                for warning in warnings:
                    print(warning)
            self.warnings.extend(xissue.warnings)
        else:
            xissue = self.xissue

        if self.fake:
            return

        xjournal = xissue.journal
        journal_id = xjournal.pid
        journal = model_helpers.get_collection(journal_id)

        if journal is None:
            raise exceptions.ResourceDoesNotExist("Journal %s does not exist" % xjournal.pid)

        existing_issue = model_helpers.get_container(xissue.pid)

        if existing_issue:
            if self.embargo and existing_issue.embargo():
                # Geodesic is for open access articles.
                # We do not want to import the issues under embargo
                print(f"Embargo, ignore {xissue.pid}")
                return None

            if self.keep_metadata:
                # On commence par faire un backup de l'existant en cas de bug.
                ptf_cmds.exportPtfCmd(
                    {
                        "pid": existing_issue.pid,
                        "with_internal_data": True,
                        "with_binary_files": False,
                        "for_archive": False,
                        "export_folder": os.path.join(settings.MERSENNE_TMP_FOLDER, "backup"),
                    }
                ).do()

                # On sauvegarde les données additionnelles (extid, deployed_date,...)
                # dans un json qui sera ré-importé avec l'import du nouvel issue
                params = {
                    "pid": existing_issue.pid,
                    "export_folder": self.backup_folder,
                    "export_all": True,
                    "with_binary_files": True,
                }
                ptf_cmds.exportExtraDataPtfCmd(params).do()

            for article in existing_issue.article_set.all():
                backup_obj_not_in_metadata(article)
                if self.keep_translations:
                    backup_translation(article)
            # changer nom de variable resource
            for resource_in_special_issue in existing_issue.resources_in_special_issue.all():
                # External article can be part of special issue and backup can bug if so

                if resource_in_special_issue.resource:
                    backup_obj_not_in_metadata(resource_in_special_issue.resource)

            # On efface l'issue existant, sinon l'import va se plaindre d'articles existants

            cmd = ptf_cmds.addContainerPtfCmd()
            cmd.set_object_to_be_deleted(existing_issue)
            cmd.undo()

            # update the journal first and last year
            for the_issue in journal.content.all():
                self.update_collection_years(journal_id, the_issue, False)

            journal.save()
        else:
            issue_to_appear = model_helpers.get_issue_to_appear(journal_id)

            # Dans le cas des AIF, les articles du volume à paraitre sont déplacés
            #     dans un nouveau volume avant publication (de AIF_0__0_ vers AIF_2018... par ex)
            #     La 1ère fois, AIF_2018_ n'est pas encore dans PTF et existing_issue vaut None.
            # Exemple : AIF_0_0 contient doi1, doi2 et doi3, AIF_2018 contient doi1 et doi2.
            # L'import va échouer car on ne peut avoir 2 fois le même article.
            # La solution d'effacer AIF_0_0 n'est pas bonne car on perd doi3.
            # Il faut supprimer les articles en commun (de _0__0 et 2018_) avant l'import
            #     du nouveau volume sinon il va y avoir des conflits.

            if issue_to_appear and xissue.pid != issue_to_appear.pid:
                # On sauvegarde les données additionnelles (extid, deployed_date,...)
                # dans un json qui sera ré-importé avec l'import du nouvel issue
                # ainsi que image associée via ptf-tools
                if self.keep_metadata:
                    params = {
                        "pid": issue_to_appear.pid,
                        "force_pid": xissue.pid,
                        "export_folder": self.backup_folder,
                        "export_all": True,
                        "with_binary_files": True,
                    }
                    ptf_cmds.exportExtraDataPtfCmd(params).do()

                for xarticle in xissue.articles:
                    if isinstance(xarticle, dict):
                        xdoi = xarticle["doi"]
                    else:
                        xdoi = getattr(xarticle, "doi")
                    article = issue_to_appear.article_set.filter(doi=xdoi).first()
                    if article:
                        backup_obj_not_in_metadata(article)
                        if self.keep_translations:
                            backup_translation(article)

                        params = {"to_folder": self.to_folder}  # pour suppression des binaires
                        cmd = ptf_cmds.addArticlePtfCmd(params)
                        cmd.set_object_to_be_deleted(article)
                        cmd.undo()

        # si backup_folder est différent de None, alors addIssueXmlCmd.post_do() utilise importExtraDataPtfCmd
        cmd = addIssueXmlCmd(
            {
                "xissue": xissue,
                "use_body": False,
                # "body": self.body,
                "assign_doi": self.assign_doi,
                "full_text_folder": self.full_text_folder,  # Cedrics: the full text for SolR is in a separate file
                "extra_folder": self.backup_folder,
                "from_folder": self.from_folder,
                "to_folder": self.to_folder,
                "no_bib": self.no_bib,
                "embargo": self.embargo,
                "solr_commit": False,
            }
        )
        new_issue = cmd.do()

        if new_issue:
            new_articles = new_issue.article_set.all()

            # Avec l'option self.assign_doi, on vérifie que les doi ont bien été assignés
            for article in new_articles:
                if self.assign_doi and article.doi is None:
                    raise exceptions.ResourceHasNoDoi("The article %s has no DOI" % article.pid)

                # TODO garbage collector on articles no longer in the issue
                restore_obj_not_in_metadata(article)
                if self.keep_translations:
                    restore_translation(article)
            if new_issue.ctype == "issue_special":
                resources_in_special_issue = new_issue.resources_in_special_issue.all()
                for resource_in_special_issue in resources_in_special_issue:
                    # External article can be part of special issue and restore can bug if so
                    if resource_in_special_issue.resource:
                        restore_obj_not_in_metadata(resource_in_special_issue.resource)

        return new_issue

    def internal_undo(self):
        raise RuntimeError("update commands do not support the undo")


class addOrUpdateBookXmlCmd(addXmlCmd):
    xbook = None
    no_bib = False  # Ignore the references during the import (used in Geodesic)

    def internal_do(self):
        super().internal_do()

        if not self.xbook:
            xbook = jats_parser.BitsBook(tree=self.tree, no_bib=self.no_bib)
            self.warnings.extend(xbook.warnings)
        else:
            xbook = self.xbook

        book_id = xbook.pid
        book = model_helpers.get_container(book_id)

        if book:
            cmd = ptf_cmds.addContainerPtfCmd()
            cmd.set_object_to_be_deleted(book)
            cmd.undo()

            collection = book.get_collection()

            # update the collection first and last year
            for container in collection.content.all():
                self.update_collection_years(collection.pid, container, False)

            collection.save()

        cmd = addBookXmlCmd(
            {
                "xbook": xbook,
                "use_body": False,
                # "body": self.body,
                "from_folder": self.from_folder,
                "to_folder": self.to_folder,
                "no_bib": self.no_bib,
                "solr_commit": False,
            }
        )
        book = cmd.do()
        return book


class updateBibitemCitationXmlCmd(baseCmd):
    """ """

    def __init__(self, params=None):
        self.bibitem = None

        super().__init__(params)

        self.required_params.extend(["bibitem"])

    def set_bibitem(self, bibitem):
        self.bibitem = bibitem

    def internal_do(self):
        super().internal_do()

        new_ids = {}
        for bibitemid in self.bibitem.bibitemid_set.all():
            new_ids[bibitemid.id_type] = {
                "id_type": bibitemid.id_type,
                "id_value": bibitemid.id_value,
                "checked": bibitemid.checked,
                "false_positive": bibitemid.false_positive,
            }

        xbibitem = jats_parser.update_bibitem_xml(self.bibitem, new_ids)
        self.warnings.extend(xbibitem.warnings)

        self.bibitem.citation_xml = xbibitem.citation_xml
        self.bibitem.citation_html = xbibitem.citation_html
        self.bibitem.citation_tex = xbibitem.citation_tex
        self.bibitem.save()

    def internal_undo(self):
        raise RuntimeError("update commands do not support the undo")


######################################################################################
######################################################################################
#
#                                 Import Commands
#
######################################################################################
######################################################################################


class collectEntireCollectionXmlCmd(baseCmd):
    """
    Get the PIDs of all the XML of a collection (collection.xml, issues.xml) of a given folder

    results:
    """

    def __init__(self, params=None):
        self.pid = None
        self.folder = None

        super().__init__(params)

        self.required_params.extend(["pid", "folder"])

    def internal_do(self):
        super().internal_do()
        pids = [pid for pid, _ in resolver.iterate_collection_folder(self.folder, self.pid)]
        return pids


class importEntireCollectionXmlCmd(baseCmd):
    """
    Import all the XML of a collection (collection.xml, issues.xml) of a given folder

    results:
    """

    def __init__(self, params=None):
        self.pid = None
        self.from_folder = None
        self.to_folder = None
        self.backup_folder = None
        self.keep_metadata = False
        self.keep_translations = False

        self.with_cedrics = True
        self.from_cedrics = False  # The entire collection is in Cedrics format
        self.date_for_pii = False  # Fetch publication_date for Elsevier articles
        self.first_issue = ""
        self.fake = False  # Parse the XML but do not import

        self.no_bib = False  # Ignore the references during the import (used in Geodesic)
        self.embargo = False  # Import only the open articles (used in Geodesic)

        self.caller = None
        self.callback = None
        self.job = None

        super().__init__(params)

        self.required_params.extend(["pid", "from_folder"])

    def internal_do(self):
        super().internal_do()

        pid = self.pid
        resource = model_helpers.get_resource(pid)
        if not resource and not self.fake:
            body = resolver.get_archive_body(self.from_folder, pid, None)
            journals = addCollectionsXmlCmd(
                {"body": body, "from_folder": self.from_folder, "to_folder": self.to_folder}
            ).do()
            if not journals:
                raise ValueError(self.from_folder + " does not contain a collection")
            resource = journals[0]

        obj = resource.cast()

        if obj.classname != "Collection":
            raise ValueError(pid + " does not contain a collection")

        if self.with_cedrics:
            # with_cedrics means that you want to import everything from scratch
            # Delete solr documents (01/28/2020: Solr can have multiple docs with the same PID)
            cmd = solr_cmds.solrDeleteCmd({"q": "pid:" + self.pid + "*"})
            cmd.do()

        i = 0
        for pid, file_ in resolver.iterate_collection_folder(
            self.from_folder, self.pid, self.first_issue
        ):
            if self.callback is None:
                print(pid)

            if self.from_cedrics:
                cmd = importCedricsIssueDirectlyXmlCmd(
                    {
                        "colid": self.pid,
                        "input_file": file_,
                        "remove_email": False,
                        "remove_date_prod": True,
                        "copy_files": True,
                        "force_dois": False,
                    }
                )
            else:
                body = resolver.get_body(file_)
                xml_file_folder = os.path.dirname(file_)
                cmd = addOrUpdateContainerXmlCmd(
                    {
                        "body": body,
                        "from_folder": self.from_folder,
                        "to_folder": self.to_folder,
                        "backup_folder": self.backup_folder,  # Read extra data (if any) stored in a json file
                        "xml_file_folder": xml_file_folder,  # when article.XML are in separate files
                        "keep_metadata": self.keep_metadata,  # Backup/Restore existing data not in the XML
                        "keep_translations": self.keep_translations,  # Backup/Restore existing translations
                        "no_bib": self.no_bib,
                        "embargo": self.embargo,
                        # Needed in Trammel
                        "fake": self.fake,
                    }
                )
            cmd.do()

            i += 1
            if self.callback:
                self.callback(self.job, i)

        if self.with_cedrics:
            src_folder = os.path.join(settings.CEDRAM_XML_FOLDER, self.pid, "metadata")

            xml_files = [
                os.path.join(src_folder, f)
                for f in os.listdir(src_folder)
                if os.path.isfile(os.path.join(src_folder, f)) and f.endswith(".xml")
            ]
            for xml_file in xml_files:
                if self.callback is None:
                    print(xml_file)

                cmd = importCedricsIssueXmlCmd(
                    {
                        "colid": self.pid,
                        "input_file": xml_file,
                        "from_folder": self.from_folder,
                        "to_folder": self.to_folder,
                    }
                )
                cmd.do()


class importCedricsIssueXmlCmd(baseCmd):
    def __init__(self, params=None):
        self.colid = None
        self.input_file = None
        self.remove_email = True
        self.remove_date_prod = True
        self.diff_only = False
        self.body = None
        self.xissue = None
        self.copy_files = True

        super().__init__(params)

        self.required_params.extend(["colid"])

    def import_full_text(self, issue):
        """
        Some journals want to display the full text in HTML (CRCHIM/CRGEOS/CEBIOL)
        Read the XML file and convert the body in HTML
        """
        tex_src_folder = resolver.get_cedram_issue_tex_folder(self.colid, issue.pid)
        tex_folders, _ = resolver.get_cedram_tex_folders(self.colid, issue.pid)

        if len(tex_folders) > 0:
            i = 0
            for article in issue.article_set.all():
                article_folder = tex_folders[i]
                xml_file = os.path.join(
                    tex_src_folder, article_folder, "FullText", article_folder + ".xml"
                )

                cmd = ptf_cmds.updateResourceIdPtfCmd(
                    {"id_type": "ojs-id", "id_value": article_folder}
                )
                cmd.set_resource(article)
                cmd.do()

                if os.path.isfile(xml_file):
                    with open(xml_file, encoding="utf-8") as f:
                        body = f.read()

                    cmd = addBodyInHtmlXmlCmd(
                        {
                            "body": body,
                            "from_folder": settings.CEDRAM_XML_FOLDER,
                            # nécessaire pour la copie des binaires type image
                            "to_folder": settings.MERSENNE_TEST_DATA_FOLDER,  # idem
                        }
                    )
                    cmd.set_article(article)
                    cmd.do()

                i += 1

    def import_in_db(self):
        """
        Import Cedrics issue from /cedram_dev/exploitation/cedram
        This worflow is no longer used.
        """

        # Cedrics: the full text for SolR is in a separate file
        full_text_folder = os.path.dirname(os.path.dirname(self.input_file)) + "/plaintext/"

        params = {
            "assign_doi": False,
            "full_text_folder": full_text_folder,
            "keep_metadata": True,
            "keep_translations": True,
            "use_body": False,
            "xissue": self.xissue,
            "backup_folder": settings.MERSENNE_TMP_FOLDER,
            "from_folder": settings.CEDRAM_XML_FOLDER,
            "to_folder": settings.MERSENNE_TEST_DATA_FOLDER if self.copy_files else None,
        }

        # params['body'] = self.body

        cmd = addOrUpdateIssueXmlCmd(params)
        issue = cmd.do()
        self.warnings.extend(cmd.get_warnings())

        # resolver.copy_binary_files(
        #     issue,
        #     settings.CEDRAM_XML_FOLDER,
        #     settings.MERSENNE_TEST_DATA_FOLDER)

        self.import_full_text(issue)

        return issue

    def compare_issue(self):
        xissue = self.xissue
        issues_diff = {}
        result = True

        time1 = timezone.now()

        new_dois = [article.doi for article in xissue.articles]

        article_qs = Article.objects.filter(doi__in=new_dois).prefetch_related(
            "abstract_set",
            "kwd_set",
            "subj_set",
            "datastream_set",
            "relatedobject_set",
            "resourcecount_set",
            "contributions",
            "contributions__contribaddress_set",
            "bibitem_set__bibitemid_set",
            "bibitem_set__contributions",
            "bibitem_set__contributions__contribaddress_set",
        )

        issue = None
        try:
            issue = (
                Container.objects.select_related("my_collection", "my_publisher")
                .prefetch_related(
                    Prefetch("article_set", queryset=article_qs, to_attr="articles_from_doi")
                )
                .get(sites__id=settings.SITE_ID, pid=xissue.pid)
            )
        except Container.DoesNotExist:
            pass

        if issue:
            data_issue = model_data_converter.db_to_issue_data(issue, issue.articles_from_doi)

            time2 = timezone.now()
            delta = time2 - time1

            delta.seconds + delta.microseconds / 1e6
            print(delta)

            # Handle xml cmds side effects (ex: "numdam" changed into "mathdoc", ...)
            model_data_comparator.prepare_issue_for_comparison(xissue)

            issue_comparator = model_data_comparator.IssueDataComparator()

            result = issue_comparator.compare(data_issue, xissue, issues_diff)

        return (result, issues_diff, xissue)

    def delete_previous_file(self, output_folder):
        basename = os.path.basename(self.input_file)

        output_file = os.path.join(output_folder, self.colid, basename)
        if os.path.isfile(output_file):
            os.remove(output_file)

        os.makedirs(output_folder, exist_ok=True)
        os.makedirs(os.path.dirname(output_file), exist_ok=True)

        return output_file

    def import_cedrics_issue(self):
        """
        Import Cedrics issue from /cedram_dev/exploitation/cedram
        This worflow is no longer used.
        Cedrics issues are imported from /cedram_dev/production_tex/CEDRAM
        (see importCedricsIssueDirectlyXmlCmd below)
        """

        output_folder = settings.MERSENNE_TMP_FOLDER
        ptf_xsl_folder = settings.PTF_XSL_FOLDER
        log_file = os.path.join(output_folder, settings.MERSENNE_LOG_FILE)

        # 1. Delete the previous file
        output_file = self.delete_previous_file(output_folder)

        # 2. Transform the cedrics XML into JATS
        cmd_folder = os.path.join(ptf_xsl_folder, "cedram")

        cmd_str = 'cd {}; {} cedram2ptf.py -v -x {} -p {} -o {} -b "" -l {} {} {} > {} 2>&1'.format(
            cmd_folder,
            os.path.join(settings.VIRTUALENV_DIR, "bin/python"),
            "-s" if self.colid in settings.MERSENNE_SEMINARS else "",
            self.input_file,
            output_folder,
            log_file + "1",
            # option -e for cedram2ptf.py for not removing email
            "-e" if not self.remove_email else "",
            "-t" if self.remove_date_prod else "",
            log_file,
        )

        log_file2 = log_file + "2"
        with open(log_file2, "w", encoding="ascii") as file_:
            file_.write(cmd_str + "\n")

            sys.path.append(ptf_xsl_folder + "/lib")

            try:
                result = subprocess.check_output(cmd_str, shell=True)
            except Exception as e:
                with open(log_file) as logfile_:
                    logfile_body = logfile_.read()
                message = str(e) + "\n" + logfile_body + "\n"
                file_.write(message)
                file_.close()
                raise RuntimeError(message)

            file_.write(str(result) + "\n")

        # Check if the output_file has been created
        if not os.path.isfile(output_file):
            raise RuntimeError("The file was not converted in JATS")

        with open(output_file, encoding="utf-8") as f:
            self.body = f.read()

        parser = etree.XMLParser(
            huge_tree=True, recover=True, remove_blank_text=True, remove_comments=True
        )
        tree = etree.fromstring(self.body.encode("utf-8"), parser=parser)
        self.xissue = jats_parser.JatsIssue(tree=tree)
        self.warnings.extend(self.xissue.warnings)

    def internal_do(self):
        super().internal_do()

        if not self.xissue:
            self.import_cedrics_issue()

        result = None

        if self.diff_only:
            result = self.compare_issue()
        else:
            result = self.import_in_db()

        return result


# import from /cedram_dev/production_tex/CEDRAM
class importCedricsIssueDirectlyXmlCmd(importCedricsIssueXmlCmd):
    def __init__(self, params=None):
        self.is_seminar = False
        self.article_folders = None
        self.force_dois = True
        super().__init__(params)

    def read_file(self, filename, skip_lines=2):
        i = 0
        lines = []
        try:
            with open(filename, encoding="utf-8") as fr:
                for line in fr:
                    if i > skip_lines:
                        lines.append(line)
                    i += 1
        except UnicodeDecodeError:
            i = 0
            lines = []
            with open(filename, encoding="iso-8859-1") as fr:
                for line in fr:
                    if i > skip_lines:
                        lines.append(line)
                    i += 1

        return lines

    def import_cedrics_issue(self):
        """
        Parse the Cedrics XML directly, without Cedrics -> JATS transformation
        The deplace_fasc script is no longer needed, but the Cedrics issue XML has to be created
        Workflow
        1. Get the list of articles from /cedram_dev/production_tex/CEDRAM
        2. Cat the article XML files into one issue.XML
        3. Read the Cedrics issue.XML

        :return:
        """

        output_folder = settings.MERSENNE_TMP_FOLDER
        output_file = self.delete_previous_file(output_folder)

        basename = os.path.basename(self.input_file)
        if "-cdrxml" in basename:
            pid = basename.split("-cdrxml.")[0]
        else:
            pid = basename.split(".xml")[0]

        # 1. Get the list of articles
        tex_src_folder = resolver.get_cedram_issue_tex_folder(self.colid, pid)
        self.article_folders, self.dois = resolver.get_cedram_tex_folders(self.colid, pid)

        # 2. Create the issue XML file
        with open(output_file, "w", encoding="utf-8") as fw:
            # 2.a. Start the issue.xml based on @pid-cdrxml.xml
            fw.write('<?xml version="1.0" encoding="utf-8" standalone="no"?>\n')
            fw.write('<!DOCTYPE cedram SYSTEM "/home/cedram/XML/dtd/cedram.dtd">\n')
            fw.write("<cedram>\n")

            lines = self.read_file(self.input_file)
            for line in lines:
                fw.write(line)

            # 2.b. Cat the article XML files
            for basename in self.article_folders:
                src_file = os.path.join(tex_src_folder, basename, basename + "-cdrxml.xml")

                lines = self.read_file(src_file)
                for line in lines:
                    fw.write(line)

            fw.write("</cedram>\n")

        # 3. Read the Cedrics issue.XML
        with open(output_file, encoding="utf-8") as f:
            self.body = f.read()

        parser = etree.XMLParser(
            huge_tree=True, recover=True, remove_blank_text=False, remove_comments=True
        )
        tree = etree.fromstring(self.body.encode("utf-8"), parser=parser)
        self.xissue = cedrics_parser.CedricsIssue(
            tree=tree,
            is_seminar=self.is_seminar,
            ignore_date_published=self.remove_date_prod,
            article_folders=self.article_folders,
            dois=self.dois,
        )
        if self.force_dois:
            for xarticle in self.xissue.articles:
                if xarticle.doi is None:
                    raise ValueError(xarticle.pid, "n'a pas de doi")

        self.warnings.extend(self.xissue.warnings)

    def import_in_db(self):
        params = {
            "assign_doi": False,
            "full_text_folder": settings.CEDRAM_TEX_FOLDER,  # the full text for SolR is in a separate file
            "keep_metadata": True,
            "keep_translations": True,  # The cedrics XML does not have the translations. backup/restore them.
            "use_body": False,
            "xissue": self.xissue,
            "backup_folder": settings.MERSENNE_TMP_FOLDER,  # temp folder used to backup/restore info during the import
            "from_folder": settings.CEDRAM_TEX_FOLDER,
            "to_folder": settings.MERSENNE_TEST_DATA_FOLDER if self.copy_files else None,
        }

        cmd = addOrUpdateIssueXmlCmd(params)
        issue = cmd.do()
        self.warnings.extend(cmd.get_warnings())

        self.import_full_text(issue)

        return issue


class addCedricsIssueXmlCmd(addXmlCmd):
    assign_doi = False
    full_text_folder = ""
    import_folder = None
    prod_deployed_date_iso_8601_date_str = None
    xissue = None
    remove_blank_text = False
    is_seminar = False

    def internal_do(self):
        super().internal_do()

        self.xissue = cedrics_parser.CedricsIssue(tree=self.tree, is_seminar=self.is_seminar)

        return self.xissue


class addorUpdateCedricsArticleXmlCmd(baseCmd):
    def __init__(self, params=None):
        self.container_pid = None
        self.article_folder_name = None

        super().__init__(params)

        self.required_params.extend(["container_pid", "article_folder_name"])

    def internal_do(self):
        super().internal_do()

        issue = model_helpers.get_container(self.container_pid)
        if not issue:
            raise exceptions.ResourceDoesNotExist(f"Issue {self.container_pid} does not exist")

        colid = issue.my_collection.pid
        article_folder = os.path.join(
            settings.CEDRAM_TEX_FOLDER, colid, self.container_pid, self.article_folder_name
        )

        # 1. Read the Cedrics article.XML
        input_file = os.path.join(article_folder, f"{self.article_folder_name}-cdrxml.xml")
        with open(input_file, encoding="utf-8") as f:
            body = f.read()

        # 2. Parse the file and create an xarticle
        is_seminar = colid in settings.MERSENNE_SEMINARS
        parser = etree.XMLParser(
            huge_tree=True, recover=True, remove_blank_text=False, remove_comments=True
        )
        tree = etree.fromstring(body.encode("utf-8"), parser=parser)
        xarticle = cedrics_parser.CedricsArticle(
            tree=tree,
            colid=colid,
            issue_id=self.container_pid,
            is_seminar=is_seminar,
            ignore_date_published=True,
            article_folder=self.article_folder_name,
        )
        if xarticle.doi is None:
            raise ValueError(xarticle.pid, "n'a pas de doi")

        # Get the article position in its issue (seq) to preserve its order
        article_folders, dois = resolver.get_cedram_tex_folders(colid, self.container_pid)
        i = 1
        for folder in article_folders:
            if folder == self.article_folder_name:
                xarticle.seq = i
            i += 1

        existing_article = model_helpers.get_article(xarticle.pid)
        temp_folder = settings.MERSENNE_TMP_FOLDER

        # 3. Backup/Suppression de l'article existant
        if existing_article:
            # On commence par faire un backup de l'existant en cas de bug.
            ptf_cmds.exportPtfCmd(
                {
                    "pid": self.container_pid,
                    "with_internal_data": True,
                    "with_binary_files": False,
                    "for_archive": False,
                    "export_folder": os.path.join(temp_folder, "backup"),
                }
            ).do()

            # On sauvegarde les données additionnelles (extid, deployed_date,...) dans un json
            params = {
                "pid": existing_article.pid,
                "export_folder": temp_folder,
                "export_all": True,
                "with_binary_files": True,
            }
            ptf_cmds.exportExtraDataPtfCmd(params).do()

            backup_obj_not_in_metadata(existing_article)
            backup_translation(existing_article)

            # Inutile d'effacer l'article existant, addArticleXmlCmd le fait en mode standalone

        # 4. Ajout de l'article dans Django/SolR
        params = {
            "xarticle": xarticle,
            "issue": issue,
            "standalone": True,
            "use_body": False,  # No self.body with the content of the XML file; xarticle is passed directly
            "full_text_folder": settings.CEDRAM_TEX_FOLDER,  # the full text for SolR is in a separate file
            # temp folder used to backup/restore info during the import
            "from_folder": settings.CEDRAM_TEX_FOLDER,
            "to_folder": settings.MERSENNE_TEST_DATA_FOLDER,
            "keep_translations": True,
        }

        cmd = addArticleXmlCmd(params)
        cmd.set_collection(issue.my_collection)
        article = cmd.do()

        # 5. Lecture du full text en HTML
        xml_file = os.path.join(article_folder, "FullText", self.article_folder_name + ".xml")
        if os.path.isfile(xml_file):
            with open(xml_file, encoding="utf-8") as f:
                body = f.read()

            cmd = addBodyInHtmlXmlCmd(
                {
                    "body": body,
                    "from_folder": settings.CEDRAM_XML_FOLDER,
                    # nécessaire pour la copie des binaires type image
                    "to_folder": settings.MERSENNE_TEST_DATA_FOLDER,  # idem
                    "remove_blank_text": False,
                }
            )
            cmd.set_article(article)
            cmd.do()

        # 6. On ajoute l'ojs-id pour ptf-tools
        cmd = ptf_cmds.updateResourceIdPtfCmd(
            {"id_type": "ojs-id", "id_value": self.article_folder_name}
        )
        cmd.set_resource(article)
        cmd.do()

        # 7. On restaure les données additionnelles (extid, deployed_date,...)
        if existing_article:
            ptf_cmds.importExtraDataPtfCmd(
                {"pid": existing_article.pid, "import_folder": temp_folder}
            ).do()

            restore_obj_not_in_metadata(article)
            restore_translation(article)

        return article


class transformBodyInHtmlXmlCmd(addXmlCmd):
    """
    transformBodyInHtmlXmlCmd: transform the JATS body in HTML

    TODO: handle images,...

    """

    use_body = False

    def internal_do(self):
        super().internal_do()

        xsl_file = settings.PTF_HTML_XSL
        xslt_doc = etree.parse(xsl_file)
        t = etree.XSLT(xslt_doc)

        html_tree = t(self.tree).getroot()

        body = html_tree.find("body/article/main")
        text = xmldata_jats.innerxml(body).decode("utf-8")

        return text


class addBodyInHtmlXmlCmd(addXmlCmd):
    """
    addBodyInHtmlXmlCmd: read the JATS body of an article
    and create the corresponding HTML

    TODO: handle images,... manage warnings for unused tag ?

    """

    def __init__(self, params=None):
        self.article = None
        self.pid = None

        super().__init__(params)

    def set_article(self, article):
        self.article = article

    def pre_do(self):
        super().pre_do()

        if self.pid is None and self.article is None:
            raise ValueError("pid et article sont vides")

        if self.article is None:
            self.article = model_helpers.get_article(self.pid)

        if self.pid is None:
            self.pid = self.article.pid

    def internal_do(self):
        super().internal_do()

        xarticle = jats_parser.JatsArticle(tree=self.tree, pid=self.pid)
        #  faut il récupérer les warnings du parseHTML ?
        # self.warnings.extend(xarticle.warnings)
        self.article.relatedobject_set.filter(rel="html-image").delete()
        self.add_objects_with_location(xarticle.figures, self.article, "RelatedObject")

        params = {
            "body_html": xarticle.body_html,
            "body_tex": xarticle.body_tex,
            "body_xml": xarticle.body_xml,
            "use_page_count": False,
        }

        cmd = ptf_cmds.updateArticlePtfCmd(params)
        cmd.set_article(self.article)
        cmd.do()

        # copy_binary_files will call resolver.copy_html_images
        # to copy the article images
        # because updateArticlePtfCmd is not from addPtfCmd, need to copy files here

        resolver.copy_html_images(
            self.article, settings.MERSENNE_TEST_DATA_FOLDER, settings.CEDRAM_XML_FOLDER
        )


class updateCacheXmlCmd(baseCmd):
    """
    recreate the citation_html field of the bibitems

    Params: colid: pid of the collection to process
    """

    def __init__(self, params=None):
        self.colid = None
        self.start_id = None

        super().__init__(params)

        self.required_params.extend(["colid"])

    def update_article(self, xarticle):
        article = model_helpers.get_article(xarticle.pid)
        if article is None:
            raise exceptions.ResourceDoesNotExist(f"Article {xarticle.pid} does not exist")

        article.title_html = xarticle.title_html
        article.title_tex = xarticle.title_tex
        article.trans_title_html = xarticle.trans_title_html
        article.trans_title_tex = xarticle.trans_title_tex
        article.save()

        for xabstract, abstract in zip(xarticle.abstracts, article.abstract_set.all()):
            abstract.value_html = xabstract["value_html"]
            abstract.value_tex = xabstract["value_tex"]
            abstract.save()

        # for xkwd_group, kwd_group in zip(xarticle.kwd_groups, article.kwdgroup_set.all()):
        #     kwd_group.value_html = xkwd_group['value_html']
        #     kwd_group.value_tex = xkwd_group['value_tex']
        #     kwd_group.save()

        for xbib, bib in zip(xarticle.bibitems, article.bibitem_set.all()):
            bib.citation_html = xbib.citation_html
            bib.citation_tex = xbib.citation_tex
            bib.article_title_tex = xbib.article_title_tex
            bib.chapter_title_tex = xbib.chapter_title_tex
            bib.source_tex = xbib.source_tex
            bib.volume = xbib.volume
            bib.save()

        if hasattr(settings, "SHOW_BODY") and settings.SHOW_BODY:
            params = {
                "body_html": xarticle.body_html,
                "body_tex": xarticle.body_tex,
                "body_xml": xarticle.body_xml,
                "use_page_count": False,
            }

            cmd = ptf_cmds.updateArticlePtfCmd(params)
            cmd.set_article(article)
            cmd.do()

    def internal_do(self):
        super().internal_do()

        collection = model_helpers.get_collection(self.colid)
        if collection is None:
            raise exceptions.ResourceDoesNotExist(f"Collection {self.colid} does not exist")

        qs = collection.content.all().order_by("pid")
        start = self.start_id is None
        for container in qs:
            if not start and container.pid == self.start_id:
                start = True

            if start:
                print(container.pid)
                with_body = hasattr(settings, "SHOW_BODY") and settings.SHOW_BODY
                xml_body = ptf_cmds.exportPtfCmd(
                    {"pid": container.pid, "with_body": with_body}
                ).do()

                parser = etree.XMLParser(
                    huge_tree=True,
                    recover=True,
                    remove_blank_text=False,
                    remove_comments=True,
                    resolve_entities=True,
                )
                tree = etree.fromstring(xml_body.encode("utf-8"), parser=parser)
                xissue = jats_parser.JatsIssue(tree=tree)

                for xarticle in xissue:
                    self.update_article(xarticle)
