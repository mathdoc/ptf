#####################################################################
#
# baseCmd: base class of all commands
#
# Set self.required_params to declare the required params
#     They are checked at the beginning of the do()
#
# All strings params are converted to unicode, except for "body"
#    body is used in xml cmds (see xml_cmds.py):
#    etree will give an error if you pass unicode to the fromstring function
#    with an XML that defines its encoding (<?xml version="1.0" encoding="utf-8"?>)
#
# sub cmds can be declared by passing the parent command in the .do() function
# sub cmds are undoed in the reverse order during .undo()
#
######################################################################
from __future__ import annotations
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from django.db import models


def make_int(value):
    if value is None or not value:
        return 0

    if not isinstance(value, int):
        value = value.split("-")[0]
        try:
            value = int(value)
        except ValueError:
            value = "".join(char for char in value if char.isdigit())
            try:
                value = int(value)
            except ValueError:
                value = 0
        else:
            pass
    return value


class baseCmd:
    def __init__(self, params={}):
        super().__init__()

        self.from_folder = None
        self.to_folder = None
        self.set_params(params)

        self.required_params = []
        self.cmds = []

        self.is_add_cmd = True
        self.has_do_been_called = False
        self.required_delete_params = []
        self.warnings = []

    def set_params(self, params):
        if params is not None:
            for key in params:
                setattr(self, key, params[key])

    #    def set_if_not_defined(self, name, value):
    #        if not hasattr(self, name) or not getattr(self, name):
    #            setattr(self, name, value)

    def check_params(self):
        required_params = self.required_params if self.is_add_cmd else self.required_delete_params

        for name in required_params:
            if not hasattr(self, name) or not getattr(self, name):
                raise ValueError(
                    "required param " + name + " est vide : " + self.__class__.__name__
                )

    def set_additional_info(self, data, infos):
        for info in infos:
            value = getattr(self, info)
            if value:
                data[info] = value

    def do(self, parent: baseCmd | None = None):
        self.has_do_been_called = True
        self.cmds = []
        self.check_params()
        self.pre_do()  # Ex: add required things, like a provider for addPublisherDatabaseCmd
        obj = self.internal_do()
        if obj is not None:
            self.post_do(obj)  # Ex: add other things, like ResourceId

        if parent:
            parent.cmds.append(self)

        return obj

    def pre_do(self):
        pass

    def internal_do(self) -> models.Model | None:
        return None

    def post_do(self, obj: models.Model):
        pass

    def undo(self):
        if not self.has_do_been_called:
            self.is_add_cmd = False

        self.check_params()
        self.pre_undo()
        id = self.internal_undo()
        self.post_undo()

        return id

    def pre_undo(self):
        for cmd in reversed(self.cmds):
            cmd.undo()

    def internal_undo(self):
        pass

    def post_undo(self):
        pass

    def get_warnings(self):
        return self.warnings
