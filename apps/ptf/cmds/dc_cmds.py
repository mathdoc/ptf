import datetime
import re

from ptf import exceptions
from ptf import model_helpers
from ptf.cmds import ptf_cmds

from ..model_data import create_articledata
from ..model_data import create_issuedata
from ..model_data import create_publisherdata
from .xml.dublin_core import data

# TODO: derive from addCollectionXmlCmd and addArticleXmlCmd instead of recreating a class


class addCollectionDc:
    def __init__(self, journal, provider):
        self.journal = journal
        self.provider = provider

    def do(self):
        return self.internal_do()

    def internal_do(self):
        # Create a collection
        col_params = {
            "coltype": "journal",
            "pid": self.journal,
            "lang": "en",
            "title_xml": self.journal,
            "title_tex": self.journal,
            "title_html": self.journal,
            "trans_title_html": self.journal,
            "trans_title_tex": self.journal,
            "solr_commit": False,
            "wall": 0,
        }

        collection = model_helpers.get_collection(self.journal)
        cls = ptf_cmds.addCollectionPtfCmd if not collection else ptf_cmds.updateCollectionPtfCmd
        cmd = cls(col_params)
        cmd.set_provider(self.provider)
        return cmd.do()


class addArticleDc:
    journal = None
    xml_format = "data_dc"
    provider = None
    lang = "en"
    eprint = False
    thesis = False
    source = None

    def __init__(self, item):
        self.item = item
        self.update = True

    def set_xml_format(self, xml_format):
        self.xml_format = xml_format

    def set_provider(self, provider):
        self.provider = provider

    def set_lang(self, lang):
        self.lang = lang

    def set_eprint(self, eprint):
        self.eprint = eprint

    def set_source(self, source):
        self.source = source

    def set_thesis(self, thesis):
        self.thesis = thesis

    def do(self):
        return self.internal_do()

    def internal_do(self):
        article = data.Article(self.item, self.lang)

        article_identifier = article.get_identifier()
        reg_doi = re.compile("doi")
        if reg_doi.search(article_identifier):
            article_identifier = article_identifier.replace("doi:", "")
        article_db = model_helpers.get_article(article_identifier)

        # c = Collection.objects.filter(resourceid__in=[r])

        # cmd = addCollectionDc(article.get_collection(), self.provider)
        if article_db is not None:
            container = article_db.my_container

            self.journal = container.my_collection

        if article_db is not None:
            if self.update:
                oai_article = None
                if hasattr(article_db, "oai_article"):
                    oai_article = article_db.oai_article
                cmd = ptf_cmds.addArticlePtfCmd(
                    {
                        "pid": article_db.pid,
                        "provider": article_db.provider,
                        "container": article_db.my_container,
                    }
                )
                cmd.set_provider(article_db.provider)
                cmd.set_object_to_be_deleted(article_db)
                cmd.undo()
                if oai_article:
                    oai_article.delete()
            else:
                raise exceptions.ResourceExists("Article %s already exists" % article_db.pid)

        year = article.get_date()
        year = article.get_date().year if year else 0
        pid = f"{self.journal.pid}_{year}"
        issue = model_helpers.get_container(pid)
        if issue is None:
            xissue = create_issuedata()
            xissue.pid = pid
            xissue.ctype = "issue"
            xissue.abstracts = article.get_abstract()
            xissue.year = year
            xissue.volume = article.get_volume()
            xissue.number = article.get_volume_number()
            xissue.last_modified_iso_8601_date_str = datetime.datetime.now().strftime(
                "%Y-%m-%d %H:%M:%S"
            )

            cmd_container = ptf_cmds.addContainerPtfCmd({"xobj": xissue})
            cmd_container.add_collection(self.journal)
            cmd_container.set_provider(self.provider)

            if article.get_publisher():
                publisher = model_helpers.get_publisher(article.get_publisher())
                if not publisher:
                    xpub = create_publisherdata()
                    xpub.name = article.get_publisher()
                    #     publisher = cmd1a.do()
                    cmd = ptf_cmds.addPublisherPtfCmd({"xobj": xpub})
                    publisher = cmd.do()
                cmd_container.set_publisher(publisher)
            issue = cmd_container.do()

        fpage, lpage = article.get_pages()
        params = {
            "title_xml": article.get_title(),
            "title_html": article.get_title(),
            "title_tex": article.get_title(),
            "kwd_groups": [
                {"content_type": "msc", "kwds": article.get_kwd(), "lang": article.get_lang()}
            ],
            "abstracts": article.get_abstract(),
            "seq": 0,
            "lang": article.get_lang(),
            "fpage": fpage,
            "lpage": lpage,
        }
        xarticle = create_articledata()
        xarticle.date_published_iso_8601_date_str = article.get_date_str()
        xarticle.title_xml = article.get_title()
        xarticle.title_html = article.get_title()
        xarticle.title_tex = article.get_title()
        xarticle.pid = article_identifier
        xarticle.kwd_groups = [
            {"content_type": "msc", "kwds": article.get_kwd(), "lang": article.get_lang()}
        ]
        xarticle.seq = 0
        xarticle.lang = article.get_lang()
        xarticle.fpage = fpage
        xarticle.lpage = lpage
        cmd = ptf_cmds.addArticlePtfCmd({"xobj": xarticle})
        cmd.set_eprint(self.eprint)
        cmd.set_source(self.source)
        cmd.set_thesis(self.thesis)
        cmd.set_container(issue)
        cmd.add_collection(self.journal)
        article_db = cmd.do()

        params = {
            "rel": "link",
            "mimetype": "application/html",
            "location": article.get_link(),
            "seq": 1,
            "solr_commit": False,
            "metadata": article.get_link(),
        }
        cmd = ptf_cmds.addExtLinkPtfCmd(params)
        cmd.set_resource(article_db)
        cmd.do()

        return article_db
