import datetime
import os
import shutil
import subprocess

import lxml.etree as etree
import requests

from django.conf import settings

from ptf import model_helpers
from ptf.cmds.database_cmds import baseCmd
from ptf.display import resolver

from .base_ptf_cmds import exportExtraDataPtfCmd
from .base_ptf_cmds import exportPtfCmd


def create_toc_xml(colid, issues):
    """
    Create the toc.xml file for the collection
    """
    if os.access(os.path.join(settings.MATHDOC_ARCHIVE_FOLDER, colid), os.R_OK):
        os.chdir(os.path.join(settings.MATHDOC_ARCHIVE_FOLDER, colid))
    if os.access("toc.xml", os.R_OK):
        os.remove("toc.xml")
    journal = etree.Element("journal")

    for issue in issues:
        new_node = etree.Element("journal-meta")

        issue_id = etree.Element("journal-id")  # Par exemple : JEP
        issue_id.text = colid
        new_node.append(issue_id)

        year = etree.Element("year")  # Par exemple : 2019
        year.text = issue.year
        new_node.append(year)

        if len(issue.vseries) != 0:
            series = etree.Element("series")  # Peut être null
            series.text = issue.vseries
            new_node.append(series)

        if len(issue.volume) != 0:
            volume = etree.Element("volume")  # Par exemple : 1
            volume.text = issue.volume
            new_node.append(volume)

        if len(issue.number) != 0:
            number = etree.Element("number")  # Peut être null
            number.text = issue.number
            new_node.append(number)

        folder = etree.Element("folder")
        folder.text = issue.pid
        new_node.append(folder)

        journal.append(new_node)

    node_str = etree.tostring(journal, pretty_print=True, encoding="unicode")
    toc_file = open("toc.xml", "w+")  # Création du fichier toc.xml
    toc_file.write(node_str)
    toc_file.close()


class archiveCollectionPtfCmd(baseCmd):
    """
    Archive the collection on disk
    """

    def __init__(self, params=None):
        super().__init__(params)
        if params is None:
            params = {}
        else:
            self.pid = params["colid"]
        self.mathdoc_archive = settings.MATHDOC_ARCHIVE_FOLDER
        self.binary_files_folder = settings.MERSENNE_PROD_DATA_FOLDER
        self.issues = params["issues"]
        self.required_params.extend(["colid"])

    def internal_do(self):
        exportPtfCmd(
            {
                "pid": self.pid,  # On exporte la collection
                "export_folder": self.mathdoc_archive,  # On exporte dans le répertoire de la collection
                "for_archive": True,  # On exporte pour l'archive
                "with_internal_data": False,  # On n'exporte pas les données internes
                "with_binary_files": True,  # On exporte les fichiers binaires (ex : img)
                "binary_files_folder": self.binary_files_folder,
            }
        ).do()  # On exporte dans le répertoire temporaire

        create_toc_xml(self.pid, self.issues)


class archiveIssuePtfCmd(baseCmd):
    """
    Archive the issue on disk
    """

    def __init__(self, params=None):
        self.pid = None  # container pid
        self.article = None  # Allow archiving of only 1 article
        self.skip_pdfa = False
        self.xml_only = False  # Geodesic needs to archive only the XML
        if "issue" in params.keys():
            self.issue = params["issue"]
        else:
            self.issue = None

        # The derived archiveNumdamIssuePtfCmd class has other default values
        if not hasattr(self, "export_folder"):
            self.export_folder = settings.MATHDOC_ARCHIVE_FOLDER
        if not hasattr(self, "binary_files_folder"):
            self.binary_files_folder = settings.MERSENNE_PROD_DATA_FOLDER

        super().__init__(params)

        self.required_params.extend(["pid"])

    # Pass an article to archive only 1 article
    def set_article(self, article):
        self.article = article

    def archive_tex_src(self, article, colid, tex_src_folder, tex_article_folder):
        src_folder = os.path.join(tex_src_folder, tex_article_folder)
        dest_folder = os.path.join(self.export_folder, article.get_relative_folder(), "src/tex/")

        resolver.create_folder(dest_folder)

        with open(os.path.join(settings.LOG_DIR, "archive.log"), "a", encoding="utf-8") as file_:
            file_.write(f"Create {dest_folder}\n")

        # 1. Copy tex file
        tex_file = os.path.join(src_folder, tex_article_folder + ".tex")
        resolver.copy_file(tex_file, dest_folder)

        # 2. sty files
        sty_files = [
            os.path.join(src_folder, f)
            for f in os.listdir(src_folder)
            if os.path.isfile(os.path.join(src_folder, f)) and f.endswith(".sty")
        ]
        for sty_file in sty_files:
            resolver.copy_file(sty_file, dest_folder)

        # 3. bib file
        aux_file = os.path.join(src_folder, tex_article_folder + ".aux")
        if os.path.isfile(aux_file):
            cmd_str = "cd " + src_folder + "; grep bibdata " + aux_file
            try:
                result = subprocess.check_output(cmd_str, shell=True).decode(encoding="utf-8")
                for line in result.split("\n"):
                    words = line.split("{")
                    if len(words) > 1:
                        line = words[1]
                        words = line.split("}")
                        if len(words) > 1:
                            base_file = words[0]

                            full_src_file = os.path.join(src_folder, base_file + ".bib")
                            if os.path.isfile(full_src_file):
                                full_dest_file = os.path.join(dest_folder, base_file + ".bib")
                                resolver.copy_file(full_src_file, full_dest_file)
            except subprocess.CalledProcessError as e:
                if e.returncode != 1:  # grep returns 1 if nothing was found
                    message = f'Error {e.returncode} with "{cmd_str}": {e.output}'
                    raise RuntimeError(message)
        else:
            base_file = resolver.get_bibtex_from_tex(tex_file)
            if base_file:
                full_src_file = os.path.join(src_folder, base_file + ".bib")
                if os.path.isfile(full_src_file):
                    full_dest_file = os.path.join(dest_folder, base_file + ".bib")
                    resolver.copy_file(full_src_file, full_dest_file)

        # 4. cdrdoidates, figures/ folder,...
        cmd_str = (
            "cd "
            + src_folder
            + r"; grep -v /usr/local/texlive/ *.fls | grep '\./'  | grep -v '\.out$' | sort -u"
        )

        try:
            result = subprocess.check_output(cmd_str, shell=True).decode(encoding="utf-8")
            for line in result.split("\n"):
                words = line.split(" ")
                if len(words) > 1:
                    file = words[1]
                    file_folder = os.path.dirname(file)

                    full_src_file = os.path.join(src_folder, file)
                    if os.path.isfile(full_src_file):
                        file_dest_folder = os.path.join(dest_folder, file_folder)
                        resolver.create_folder(file_dest_folder)

                        full_dest_file = os.path.join(dest_folder, file)
                        resolver.copy_file(full_src_file, full_dest_file)
        except subprocess.CalledProcessError as e:
            if e.returncode != 1:  # grep returns 1 if nothing was found
                message = f'Error {e.returncode} with "{cmd_str}": {e.output}'
                raise RuntimeError(message)

    def create_pdfa(self, colid, article_pid):
        """
        Create the pdfa files of the pdfs associated with the collection
        """

        in_file = os.path.join(
            self.export_folder, colid, self.pid, article_pid, article_pid + ".pdf"
        )
        out_file = os.path.join(
            self.export_folder, colid, self.pid, article_pid, article_pid + "_PDFA.pdf"
        )

        code_return = os.system(
            f"gs -dSAFER -dBATCH -DNOPAUSE -sPAPERSIZE=halfletter -dPDFFitPage -dFIXEDMEDIA "
            f"-dEmbedAllFonts=true -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPrinted=false -dPDFSETTINGS=/printer "
            f"-q -o {out_file} {in_file}"
        )  # On convertit le fichier pdf en fichier pdfa
        if code_return != 0:  # Si le code retour de la commande n'est pas 0, il y a eu une erreur
            with open(
                os.path.join(settings.LOG_DIR, "archive_error.log"), "a", encoding="utf-8"
            ) as file_:
                file_.write(article_pid + " , PDF/A\n")

            raise RuntimeError(f"Le fichier {in_file} n'a pas pu être converti en PDFA")

        with open(os.path.join(settings.LOG_DIR, "archive.log"), "a", encoding="utf-8") as file_:
            file_.write(f"Write {out_file}\n")

    def backup_pdfa_if_necessary(self, colid, article_pids):
        """
        Since it is time consuming to create a PDF/A, check if they have to be re-created
        before deleting the /mathdoc_archive issue folder.

        If so, backup the PDF/A in a temp folder. They will be put back in /mathdoc_archive in create_or_restore_pdfa()

        To check, we compare the date of the article PDF between
        - /mathdoc_archive and
        - self.binary_files_folder (/mersenne_prod_data or /numdam_data)
        """

        pdfas = {}
        tmp_folder = os.path.join(settings.LOG_DIR, "tmp/archive", colid, self.pid)
        resolver.create_folder(tmp_folder)

        with open(os.path.join(settings.LOG_DIR, "archive.log"), "a", encoding="utf-8") as file_:
            file_.write(f"Create {tmp_folder}\n")

        # Before deleting the issue folder, check if we have to recreate the PDF/A (save them to a tmp folder)
        # If the PDF has been updated in /mersenne_prod_data, we recreate the PDF/A
        for article_pid in article_pids:
            pdf_in_archive = resolver.get_disk_location(
                self.export_folder, colid, "pdf", self.pid, article_pid
            )
            pdf_in_prod = resolver.get_disk_location(
                self.binary_files_folder, colid, "pdf", self.pid, article_pid
            )

            do_create_pdfa = True

            if os.path.isfile(pdf_in_prod) and os.path.isfile(pdf_in_archive):
                pdfa_in_archive = os.path.join(
                    self.export_folder, colid, self.pid, article_pid, article_pid + "_PDFA.pdf"
                )

                date_pdf_in_archive = datetime.datetime.fromtimestamp(
                    os.stat(pdf_in_prod).st_mtime
                ).strftime("%Y-%m-%d")
                date_pdf_in_prod = datetime.datetime.fromtimestamp(
                    os.stat(pdf_in_archive).st_mtime
                ).strftime("%Y-%m-%d")

                do_create_pdfa = (
                    not os.path.isfile(pdfa_in_archive) or date_pdf_in_prod != date_pdf_in_archive
                )

            pdfas[article_pid] = do_create_pdfa
            if not do_create_pdfa:
                # Copy the PDF/A in the temp folder
                src_pdfa = os.path.join(
                    self.export_folder, colid, self.pid, article_pid, article_pid + "_PDFA.pdf"
                )
                dest_pdfa = os.path.join(tmp_folder, article_pid + "_PDFA.pdf")
                resolver.copy_file(src_pdfa, dest_pdfa)

                with open(
                    os.path.join(settings.LOG_DIR, "archive.log"), "a", encoding="utf-8"
                ) as file_:
                    file_.write(f"Backup {dest_pdfa}\n")

        return pdfas

    def create_or_restore_pdfa(self, colid, article_pids, pdfas):
        tmp_folder = os.path.join(settings.LOG_DIR, "tmp/archive", colid, self.pid)

        for article_pid in article_pids:
            if pdfas[article_pid]:
                self.create_pdfa(colid, article_pid)
            else:
                src_pdfa = os.path.join(tmp_folder, article_pid + "_PDFA.pdf")
                dest_pdfa = os.path.join(
                    self.export_folder, colid, self.pid, article_pid, article_pid + "_PDFA.pdf"
                )
                resolver.copy_file(src_pdfa, dest_pdfa)

                with open(
                    os.path.join(settings.LOG_DIR, "archive.log"), "a", encoding="utf-8"
                ) as file_:
                    file_.write(f"Restore {dest_pdfa}\n")

        if os.path.isdir(tmp_folder):
            shutil.rmtree(tmp_folder)

            with open(
                os.path.join(settings.LOG_DIR, "archive.log"), "a", encoding="utf-8"
            ) as file_:
                file_.write(f"Delete {tmp_folder}\n")

    def archive_files(self, colid, container, articles, article_pids, pdfas):
        # II. Copy binary files (PDF...)
        for a in articles:
            article_folder = a.get_relative_folder()

            with open(
                os.path.join(settings.LOG_DIR, "archive.log"), "a", encoding="utf-8"
            ) as file_:
                file_.write(f"Delete {article_folder}\n")

            resolver.delete_object_folder(article_folder, to_folder=self.export_folder)
            resolver.copy_binary_files(a, self.binary_files_folder, self.export_folder)

        params = {"pid": self.pid, "export_folder": self.export_folder, "export_all": False}
        exportExtraDataPtfCmd(params).do()

        tex_src_folder = resolver.get_cedram_issue_tex_folder(colid, self.pid)
        tex_folders, _ = resolver.get_cedram_tex_folders(colid, self.pid)

        # III. Articles written in LaTeX. We need to archive files needed to re-compile the LaTex source code
        if len(tex_folders) > 0:
            i = 0
            for article in container.article_set.exclude(do_not_publish=True):
                if self.article is None or self.article.pid == article.pid:
                    self.archive_tex_src(article, colid, tex_src_folder, tex_folders[i])

                i += 1

        # IV. Digitized papers (Numdam). We basically need to archive the images (*.tif)
        copy_numdam_src_files(colid, self.pid, article_pids, self.export_folder)

        # V. Create PDF/A
        if not self.skip_pdfa:
            self.create_or_restore_pdfa(colid, article_pids, pdfas)

    def internal_do(self):
        super().internal_do()

        if self.article is None:
            container = model_helpers.get_container(self.pid, prefetch=False)
            qs = container.article_set.all()
            article_pids = list(qs.values_list("pid", flat=True))
            articles = qs
        else:
            container = self.article.my_container
            article_pids = [self.article.pid]
            articles = [self.article]

        colid = container.get_top_collection().pid
        self.pid = container.pid

        pdfas = {}
        if not self.xml_only and not self.skip_pdfa:
            # Backup PDF/A before deleting the issue folder
            # (it is time consuming to create a PDF/A, we will check if they have to be re-created)
            pdfas = self.backup_pdfa_if_necessary(colid, article_pids)

        # Delete the issue folder if we archive an issue
        if self.article is None:
            issue_folder = container.get_relative_folder()

            with open(
                os.path.join(settings.LOG_DIR, "archive.log"), "a", encoding="utf-8"
            ) as file_:
                file_.write(f"Delete {self.export_folder}/{issue_folder}\n")

            resolver.delete_object_folder(issue_folder, to_folder=self.export_folder)

        # I. Always archive the issue XML, even if we archive only 1 article
        exportPtfCmd(
            {
                "pid": self.pid,
                "export_folder": self.export_folder,
                "with_binary_files": self.article
                is None,  # binary files for 1 article are copied below
                "for_archive": True,
                "binary_files_folder": self.binary_files_folder,
            }
        ).do()

        if not self.xml_only:
            self.archive_files(colid, container, articles, article_pids, pdfas)


def archive_numdam_xml(colid, pid, export_folder):
    """
    Get the XML of a collection or an issue
    """
    url = settings.NUMDAM_URL + "/api-item-xml/"
    if pid is None:
        url += colid
    else:
        url += pid
    response = requests.get(url)
    response.raise_for_status()

    xml_body = response.content.decode("utf-8")

    if xml_body:
        file = resolver.get_archive_filename(export_folder, colid, pid, "xml", True)

        with open(file, "w", encoding="utf-8") as f:
            f.write(xml_body)


def get_numdam_issues_list(colid):
    """
    Get the list of issues of a collection from numdam.org
    """
    response = requests.get(f"{settings.NUMDAM_URL}/api-issues/{colid}")
    response.raise_for_status()

    return response.json()["issues"]


def get_numdam_file_list(colid, pid):
    """
    Get the list of files to archive (of a collection or an issue)
    The files are those visible by the user, like PDF or DjVus
    """
    url = settings.NUMDAM_URL + "/api-item-file-list/"
    if pid is None:
        url += colid
    else:
        url += pid
    response = requests.get(url)
    response.raise_for_status()

    data = response.json()
    return data


def copy_numdam_src_files(colid, pid, article_pids, export_folder, log_file=None):
    src_folder = os.path.join(settings.NUMDAM_ISSUE_SRC_FOLDER, colid, pid)

    if not os.path.isdir(src_folder):
        return

    # 1. Files related to the issue
    dest_folder = os.path.join(
        export_folder, resolver.get_relative_folder(colid, pid), "src/digitisation/"
    )

    if log_file:
        log_file.write("Create " + dest_folder + "...")

    resolver.create_folder(dest_folder)

    if log_file:
        log_file.write("done\n")

    # 1a. issue.xml
    full_src_file = os.path.join(src_folder, pid + ".xml")
    if os.path.isfile(full_src_file):
        full_dest_file = os.path.join(dest_folder, pid + ".xml")

        if log_file:
            log_file.write(f"Copy {os.path.basename(full_src_file)} ...")

        resolver.copy_file(full_src_file, full_dest_file)

        if log_file:
            log_file.write("done\n")

    # 1b. tif, jpg files
    img_files = [
        os.path.join(src_folder, f)
        for f in os.listdir(src_folder)
        if (
            os.path.isfile(os.path.join(src_folder, f))
            and (f.endswith(".tif") or f.endswith(".jpg"))
        )
    ]

    for img_file in img_files:
        if log_file:
            log_file.write(f"Copy {os.path.basename(img_file)} ...")

        resolver.copy_file(img_file, dest_folder)

        if log_file:
            log_file.write("done\n")

    # 2. Files related to articles
    for article_pid in article_pids:
        src_folder = os.path.join(settings.NUMDAM_ARTICLE_SRC_FOLDER, colid, pid, article_pid)

        dest_folder = os.path.join(
            export_folder,
            resolver.get_relative_folder(colid, pid, article_pid),
            "src/digitisation/",
        )

        if log_file:
            log_file.write("Create " + dest_folder + "...")

        resolver.create_folder(dest_folder)

        if log_file:
            log_file.write("done\n")

        # 2a. article.xml (Full Text)
        full_src_file = os.path.join(src_folder, article_pid + ".xml")
        if os.path.isfile(full_src_file):
            full_dest_file = os.path.join(dest_folder, article_pid + ".xml")

            if log_file:
                log_file.write(f"Copy {os.path.basename(full_src_file)} ...")

            resolver.copy_file(full_src_file, full_dest_file)

            if log_file:
                log_file.write("done\n")

        # 2b. tif, jpg files
        img_files = [
            os.path.join(src_folder, f)
            for f in os.listdir(src_folder)
            if (
                os.path.isfile(os.path.join(src_folder, f))
                and (f.endswith(".tif") or f.endswith(".jpg"))
            )
        ]

        for img_file in img_files:
            if log_file:
                log_file.write(f"Copy {os.path.basename(img_file)} ...")

            resolver.copy_file(img_file, dest_folder)

            if log_file:
                log_file.write("done\n")

        # PDF/DJVU without headers
        # Olivier 09/05/2019: these files are almost identicals to the final PDFs
        # There is no need to archive them. Just use the final PDF and remove the first page
        # if needed

        # # 2c. pdf files (without header)
        # full_src_file = os.path.join(src_folder, article_pid + '.pdf')
        # if os.path.isfile(full_src_file):
        #     full_dest_file = os.path.join(dest_folder, article_pid + '.pdf')
        #     if file:
        #         file.write("Copy {} ...".format(os.path.basename(full_src_file)))
        #     copy_file(full_src_file, full_dest_file)
        #     if file:
        #         file.write("done\n")
        #


# def copy_numdam_djvu(colid, pid, article_pids, export_folder, log_file=None):
#     """
#     Djvu might not be visible/listed in centre Mersenne articles, but might exist in Numdam
#     """
#
#     if hasattr(settings, "NUMDAM_DATA_ROOT"):
#         for article_pid in article_pids:
#             article_folder = resolver.get_relative_folder(colid, pid, article_pid)
#             full_src_file = os.path.join(
#                 settings.NUMDAM_DATA_ROOT, article_folder, article_pid + ".djvu"
#             )
#             if os.path.isfile(full_src_file):
#                 full_dest_file = os.path.join(export_folder, article_folder, article_pid + ".djvu")
#                 if log_file:
#                     log_file.write(f"Copy {os.path.basename(full_src_file)} ...")
#                 resolver.copy_file(full_src_file, full_dest_file)
#                 if log_file:
#                     log_file.write("done\n")


class archiveNumdamResourcePtfCmd(archiveIssuePtfCmd):
    """
    Archive a Container or a Collection (just the collection level) stored in Numdam
    """

    def __init__(self, params=None):
        self.colid = None  # self.pid from the base class is the id of the container
        self.export_folder = settings.MATHDOC_ARCHIVE_FOLDER
        self.binary_files_folder = settings.NUMDAM_DATA_ROOT

        super().__init__(params)

        self.required_params.extend(["colid"])
        # self.pid is optional when you want to archive a Collection
        self.required_params = [id for id in self.required_params if id != "pid"]

    def internal_do(self):
        """
        Archive files of Numdam.
        - Send http requests to numdam.org to get the list of user files (PDF/DjVu/XML) to preserve
        - Copy these files to self.export_older (/mathdoc_archive)
        - Copy src files (mainly digitized TIF/JPG files) from /numdam_dev

        Warning: this class does not operate with Resource objects stored in the database (Collection, Container...)
                 since the data comes from numdam.org
                 Information is only based on pids (collection, issue, article)
        """

        # 1. Get the list of user files to archive (files visible by the user, like PDF or DjVus) from numdam.org
        data = get_numdam_file_list(self.colid, self.pid)
        pdfas = {}
        article_pids = []

        # 2. Prepare the backup or an Issue (backup, delete previous folder)
        if self.pid is not None:  # Archive an issue
            article_pids = [item["pid"] for item in data["articles"]] if "articles" in data else []

            # Backup PDF/A before deleting the issue folder
            # (it is time consuming to create a PDF/A, we will check if they have to be re-created)
            pdfas = self.backup_pdfa_if_necessary(self.colid, article_pids)

            issue_folder = resolver.get_relative_folder(self.colid, self.pid)

            with open(
                os.path.join(settings.LOG_DIR, "archive.log"), "a", encoding="utf-8"
            ) as file_:
                file_.write(f"Delete {self.export_folder}/{issue_folder}\n")

            # Delete the issue folder
            resolver.delete_object_folder(issue_folder, self.export_folder)

        # 3. Archive the JATS XML of the pid
        # TODO: The XML coming from Numdam does not list PDF/A in the <self-uri> of the articles, only PDF/DjVu
        # create_or_restore_pdfa is going to add the PDF/A on disk
        # We should modify the xml to add the PDF/A
        archive_numdam_xml(self.colid, self.pid, self.export_folder)

        # 4. Archive the user files (list gotten in 2.)
        if "files" in data:
            # Files of a Collection or a Container
            resolver.copy_binary_files(
                None,
                self.binary_files_folder,
                self.export_folder,
                data["files"],
            )

        if "articles" in data:
            # In case of a Container, files of each article
            for article_data in data["articles"]:
                resolver.copy_binary_files(
                    None,
                    self.binary_files_folder,
                    self.export_folder,
                    article_data["files"],
                )

        if self.pid is not None:
            # 5. Archive the src files (tiff, pdf/djvu without headers,...)
            copy_numdam_src_files(self.colid, self.pid, article_pids, self.export_folder)

            # 6. Create PDF/A
            self.create_or_restore_pdfa(self.colid, article_pids, pdfas)
