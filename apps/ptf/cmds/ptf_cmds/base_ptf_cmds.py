import copy
import datetime
import json
import os
import subprocess
import sys

import lxml.etree as etree
from PIL import Image

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.template.loader import render_to_string

from ptf import exceptions
from ptf import model_helpers
from ptf import utils
from ptf.cmds.database_cmds import add_contributors
from ptf.cmds.database_cmds import addArticleDatabaseCmd
from ptf.cmds.database_cmds import addBibItemDatabaseCmd
from ptf.cmds.database_cmds import addBibItemIdDatabaseCmd
from ptf.cmds.database_cmds import addCollectionDatabaseCmd
from ptf.cmds.database_cmds import addContainerDatabaseCmd
from ptf.cmds.database_cmds import addDataStreamDatabaseCmd
from ptf.cmds.database_cmds import addExtIdDatabaseCmd
from ptf.cmds.database_cmds import addExtLinkDatabaseCmd
from ptf.cmds.database_cmds import addFrontMatterDatabaseCmd
from ptf.cmds.database_cmds import addProviderDatabaseCmd
from ptf.cmds.database_cmds import addPublisherDatabaseCmd
from ptf.cmds.database_cmds import addRelatedObjectDatabaseCmd
from ptf.cmds.database_cmds import addRelationshipDatabaseCmd
from ptf.cmds.database_cmds import addResourceCountDatabaseCmd
from ptf.cmds.database_cmds import addResourceInSpecialIssueDatabaseCmd
from ptf.cmds.database_cmds import addSiteDatabaseCmd
from ptf.cmds.database_cmds import addSupplementaryMaterialDatabaseCmd
from ptf.cmds.database_cmds import addXmlBaseDatabaseCmd
from ptf.cmds.database_cmds import baseCmd
from ptf.cmds.database_cmds import publishArticleDatabaseCmd
from ptf.cmds.database_cmds import publishContainerDatabaseCmd
from ptf.cmds.database_cmds import updateCollectionDatabaseCmd
from ptf.cmds.database_cmds import updateExtLinkDatabaseCmd
from ptf.cmds.database_cmds import updateResourceIdDatabaseCmd
from ptf.cmds.solr_cmds import addArticleSolrCmd
from ptf.cmds.solr_cmds import addBookPartSolrCmd
from ptf.cmds.solr_cmds import addContainerSolrCmd
from ptf.cmds.solr_cmds import solrFactory
from ptf.cmds.solr_cmds import updateResourceSolrCmd
from ptf.cmds.xml import xml_utils
from ptf.display import resolver
from ptf.model_data import PublisherData
from ptf.models import ExtLink
from ptf.models import Person
from ptf.models import Relationship


def myconverter(o):
    if isinstance(o, datetime.datetime):
        return o.__str__()


def do_solr_commit():
    solrFactory.do_solr_commit()


def do_solr_rollback():
    solrFactory.do_solr_rollback()


#####################################################################
#
# addPtfCmd: base class of PtfCmds
#
# PtfCmds may have a cmd and a sub-cmd
# The cmd is executed, and the id of the returned object is passed
#   to the sub-cmd before its execution.
#
# This allows to store an object in Django, get the django object id,
# then store the corresponding document in Solr
#
######################################################################


class addPtfCmd(baseCmd):
    def __init__(self, params=None):
        if params is not None and "solr_commit" in params:
            params["commit"] = params["solr_commit"]

        super().__init__(params)
        self.required_delete_params.append("object_to_be_deleted")

        self.cmd: baseCmd | None = None
        self.sub_cmd = None

    def internal_do(self):
        obj = super().internal_do()

        if self.cmd:
            obj = self.cmd.do()

        if self.sub_cmd:
            self.sub_cmd.db_obj = obj
            self.sub_cmd.id = obj.id
            self.sub_cmd.pid = obj.pid

            # if hasattr(obj, "title_tex"):
            #    self.sub_cmd.title = obj.title_tex

            self.sub_cmd.do()
        # au cas d'un futur undo sur la cmd
        self.set_object_to_be_deleted(obj)
        return obj

    def set_object_to_be_deleted(self, obj):
        if obj is not None:
            self.object_to_be_deleted = obj
            self.cmd.object_to_be_deleted = obj

    def internal_undo(self):
        id = super().internal_undo()

        if self.cmd:
            id = self.cmd.undo()

        if self.sub_cmd:
            self.sub_cmd.id = id
            self.sub_cmd.undo()

        return id


#####################################################################
#
# addSitePtfCmd: adds/remove a PtfSite
#  params: 'site_name', 'site_domain'
#
# Exception raised:
#    - ValueError if the init params are empty
#    - exceptions.ResourceExists during do if the site already exists
#    - exceptions.ResourceDoesNotExist during undo if the site does not exist
#    - RuntimeError during undo if resources are still published
#
######################################################################
class addSitePtfCmd(addPtfCmd):
    def __init__(self, params=None):
        super().__init__(params)

        self.cmd = addSiteDatabaseCmd(params)


#####################################################################
#
# addProviderPtfCmd: adds/remove a Provider
# params: 'name', 'pid_type', 'sid_type'
#
# Exception raised:
#    - ValueError if the init params are empty
#    - exceptions.ResourceExists during do if the provider already exists
#    - exceptions.ResourceDoesNotExist during undo if the provider does not exist
#
######################################################################
class addProviderPtfCmd(addPtfCmd):
    def __init__(self, params=None):
        super().__init__(params)

        self.cmd = addProviderDatabaseCmd(params)


#####################################################################
#
# addXmlBasePtfCmd: adds/remove an XmlBase
# XmlBase is the root URL of an ExtLink (ex: http://archive.numdam.org/article)
# params: 'base'
#
# Exception raised:
#    - ValueError if the init params are empty
#    - exceptions.ResourceExists during do if the XmlBase already exists
#    - exceptions.ResourceDoesNotExist during undo if the XmlBase does not exist
#    - RuntimeError during undo if related extlinks or objects still exist
#
######################################################################
class addXmlBasePtfCmd(addPtfCmd):
    def __init__(self, params=None):
        super().__init__(params)

        self.cmd = addXmlBaseDatabaseCmd(params)


#####################################################################
#
# addExtLinkPtfCmd: adds/remove an ExtLink
# params:  'rel':  'website' or 'small_icon'
#          'mimetype', 'location', 'metadata', 'seq'
#
# Needs a Resource object (required) and a XmlBase object (option)
#
# Exception raised:
#    - ValueError if the init params are empty
#    - exceptions.ResourceExists during do if the ExtLink already exists
#    - exceptions.ResourceDoesNotExist during undo if the ExtLink does not exist
#    - RuntimeError during undo if resources are still published
#
######################################################################
class addExtLinkPtfCmd(addPtfCmd):
    cmd: addExtLinkDatabaseCmd

    def __init__(self, params=None):
        super().__init__(params)

        self.cmd = addExtLinkDatabaseCmd(params)

    def set_resource(self, resource):
        self.cmd.set_resource(resource)

    def set_base(self, base):
        self.cmd.set_base(base)

    def pre_do(self):
        super().pre_do()

        if self.to_folder and self.location.find("file:") == 0:
            # import avec un full path de fichier (ex: Elsevier CRAS)
            # 1. On copie le fichier
            # 2. On met à jour le champs location pour utiliser l'arborescence PTF
            # On fait ça dans le pre_do pour stocker un objet avec le champ location final
            from_path = self.location[5:]

            convert_image = False
            extension = os.path.splitext(self.location)[1]
            if extension == ".tif" or extension == ".tiff":
                convert_image = True
                extension = ".jpg"

            resource = self.cmd.resource
            relative_path = resource.pid + extension
            new_location = os.path.join(resource.get_relative_folder(), relative_path)
            to_path = os.path.join(self.to_folder, new_location)

            dest_folder = os.path.dirname(to_path)
            os.makedirs(dest_folder, exist_ok=True)

            if convert_image:
                im = Image.open(from_path)
                im.thumbnail(im.size)
                im.save(to_path, "JPEG", quality=100)
            else:
                resolver.copy_file(from_path, to_path)

            self.location = new_location
            self.cmd.location = new_location


#####################################################################
#
# addExtIdPtfCmd: adds/remove an ExtId
# params:  'id_type', 'id_value'
#
# Needs a Resource object
#
# Exception raised:
#    - ValueError if the init params are empty
#    - exceptions.ResourceExists during do if the ExtId already exists
#    - exceptions.ResourceDoesNotExist during undo if the ExtId does not exist
#    - RuntimeError during undo if resources are still published
#
######################################################################
class addExtIdPtfCmd(addPtfCmd):
    def __init__(self, params=None):
        super().__init__(params)

        self.cmd = addExtIdDatabaseCmd(params)

    def set_resource(self, resource):
        self.cmd.set_resource(resource)


#####################################################################
#
# addRelatedObjectPtfCmd: adds/remove a RelatedObject
# params:  'rel':
#          'mimetype', 'location', 'metadata', 'seq'
#
# Needs a Resource object and a XmlBase object
#
# Exception raised:
#    - ValueError if the init params are empty
#    - exceptions.ResourceExists during do if the RelatedObject already exists
#    - exceptions.ResourceDoesNotExist during undo if the RelatedObject does not exist
#    - RuntimeError during undo if resources are still published
#
######################################################################
class addRelatedObjectPtfCmd(addPtfCmd):
    def __init__(self, params=None):
        super().__init__(params)
        self.do_linearize = True

        # need Resource to construct complete path
        self.required_delete_params.append("resource")

        self.cmd = addRelatedObjectDatabaseCmd(params)

    def set_resource(self, resource):
        self.resource = resource
        self.cmd.set_resource(resource)

    def set_base(self, base):
        self.cmd.set_base(base)

    def pre_do(self):
        super().pre_do()

        full_path_pos = self.location.find("file:")
        if (
            self.from_folder and self.to_folder and self.from_folder == settings.CEDRAM_TEX_FOLDER
        ) or (self.to_folder and full_path_pos != -1):
            # A. Import d'un XML Cedrics. Les champs location sont relatifs au from_folder.
            # (contrairement à un import Cedrics transformé en JATS où les champs sont plus ou moins
            # relatifs au to_folder)
            # B. Autre possibilité: import avec un full path de fichier (ex: Elsevier CRAS)
            #                       RelatedObject est utilisé pour les images des articles (HTML)
            #                       Pour les images de couvertures des numéros, ce sont des ExtLink
            #                                                               (voir addExtLinkPtfCmd)
            # 1. On copie le fichier
            # 2. On met à jour le champs location pour utiliser l'arborescence PTF
            # On fait ça dans le pre_do pour stocker un objet avec le champ location final
            location = self.location
            if full_path_pos > -1:
                from_path = location[full_path_pos + 5 :].replace(
                    "/ums_dev/numdam_dev", "/numdam_dev"
                )
            else:
                from_path = os.path.join(self.from_folder, location)

            convert_image = False
            extension = os.path.splitext(from_path)[1]
            resource = self.cmd.resource

            if full_path_pos > -1 and extension in xml_utils.get_elsevier_image_extensions():
                convert_image = True
                extension = ".jpg"

            if full_path_pos > 0:
                relative_path = location[0:full_path_pos]
            else:
                i = location.find("/Attach/")
                if i > 0:
                    relative_path = "a" + location[i + 2 :]
                elif extension == ".tex":
                    relative_path = os.path.join("src/tex", resource.pid + extension)
                elif extension == ".jpg":
                    basename = os.path.splitext(os.path.basename(from_path))[0]
                    relative_path = os.path.join("src/tex/figures", basename + extension)
                elif hasattr(self, "supplementary_material") and self.supplementary_material:
                    # Supplements from Elsevier. They are declared with "file://"
                    # They need to be copied in attach/basename
                    relative_path = "attach/" + os.path.basename(from_path)
                else:
                    relative_path = resource.pid + extension

            new_location = os.path.join(resource.get_relative_folder(), relative_path)
            to_path = os.path.join(self.to_folder, new_location)

            dest_folder = os.path.dirname(to_path)
            os.makedirs(dest_folder, exist_ok=True)

            do_copy = True
            # linearize_pdf directly create the to_path (ptf-tools only)
            # there is no need to copy the file in that case
            if extension.lower() == ".pdf" and self.do_linearize:
                do_copy = utils.linearize_pdf(from_path, to_path)
            if do_copy:
                if convert_image:
                    im = Image.open(from_path)
                    size = 1000, 1000
                    im.thumbnail(size, Image.Resampling.LANCZOS)
                    im.save(to_path, "JPEG", quality=90)
                else:
                    resolver.copy_file(from_path, to_path)

            self.location = new_location
            self.cmd.location = new_location

    def post_do(self, obj):
        super().post_do(obj)
        # on est dans le cas où on veut récupérer depuis mathdoc_archive (sinon les fichiers sont copiés dans le pre_do)
        if self.from_folder == settings.MATHDOC_ARCHIVE_FOLDER and self.to_folder:
            # on passe ds binary files pour profiter de la logique copy_binary_files qui copie aussi les ExtLink (icon, small-icon)
            # sinon ces fichiers ne sont pas copiés -> soit icon dans DataStream ou peut-être créer une classe addBinaryFiles dont dépendraient ts les objects avec fichiers
            # les couvertures ne sont pas dans les xml cedram donc pas de question à se poser dans ce cas
            resolver.copy_binary_files(obj.resource, self.from_folder, self.to_folder)

    def pre_undo(self):
        super().pre_undo()
        if self.to_folder:
            path = os.path.join(self.to_folder, self.object_to_be_deleted.location)
            resolver.delete_file(path=path)


#####################################################################
#
# addSupplementaryMaterialPtfCmd: adds/remove a Supplementary Material
# params:  'rel':
#          'mimetype', 'location', 'metadata', 'seq', 'caption'
#
# Needs a Resource object and a XmlBase object
#
# Exception raised:
#    - ValueError if the init params are empty
#    - exceptions.ResourceExists during do if the RelatedObject already exists
#    - exceptions.ResourceDoesNotExist during undo if the RelatedObject does not exist
#    - RuntimeError during undo if resources are still published
#
######################################################################
class addSupplementaryMaterialPtfCmd(addRelatedObjectPtfCmd):
    def __init__(self, params=None):
        super().__init__(params)
        self.cmd = addSupplementaryMaterialDatabaseCmd(params)
        self.do_linearize = False


#####################################################################
#
# addDataStreamPtfCmd: adds/remove a RelatedObject
# params:  'rel':
#          'mimetype', 'location', 'metadata', 'seq'
#
# Needs a Resource object and a XmlBase object
#
# Exception raised:
#    - ValueError if the init params are empty
#    - exceptions.ResourceExists during do if the DataStream already exists
#    - exceptions.ResourceDoesNotExist during undo if the DataStream does not exist
#    - RuntimeError during undo if resources are still published
#
######################################################################
class addDataStreamPtfCmd(addRelatedObjectPtfCmd):
    def __init__(self, params=None):
        super().__init__(params)
        self.cmd = addDataStreamDatabaseCmd(params)


# #####################################################################
# #
# # addOrUpdateDataStreamPtfCmd: adds or Update  a Datastream
# # params:  'rel':
# #          'mimetype', 'location', 'metadata', 'seq'
# #
# # if new location specify params: 'new_location'
# # Needs a Resource object and a XmlBase object
# #
# # Exception raised:
# #    - ValueError if the init params are empty
# #    - RuntimeError during undo if resources are still published
# #
# ######################################################################
# class addOrUpdateDataStreamPtfCmd(baseCmd):
#     def set_resource(self, resource):
#         self.resource = resource
#
#     def internal_do(self):
#         super(addOrUpdateDataStreamPtfCmd, self).internal_do()
#         # copy new article pdf cedram_dev to mersenne_test_data
#         datastream_qs = DataStream.objects.filter(resource=self.resource,
#                                                base=self.base,
#                                                rel=self.rel,
#                                                location=self.location)
#
#         cmd = addDataStreamPtfCmd({'rel':self.rel,
#                                   'mimetype':self.mimetype,
#                                   'location':self.location,
#                                   'text':self.text,
#                                   'seq':self.seq
#                                 })
#         cmd.set_base(self.base)
#         cmd.set_resource(self.resource)
#

#         if datastream_qs.count() > 0:
#             cmd.set_object_to_be_deleted(datastream_qs.get())
#             cmd.undo()
#         cmd.set_params({'location': self.new_location})
#         cmd.do()


#####################################################################
#
# addResourceCountPtfCmd: adds/remove a ResourceCount
#
# A ResourceCount is a generic count element.
# Exemple: page count, table count, image count...
#
# params:  'name', 'value', 'seq'
#
# Needs a Resource object
#
# Exception raised:
#    - ValueError if the init params are empty
#    - exceptions.ResourceExists during do if the ResourceCount already exists
#    - exceptions.ResourceDoesNotExist during undo if the ResourceCount does not exist
#    - RuntimeError during undo if resources are still published
#
######################################################################
class addResourceCountPtfCmd(addPtfCmd):
    def __init__(self, params=None):
        super().__init__(params)

        self.cmd = addResourceCountDatabaseCmd(params)

    def set_resource(self, resource):
        self.cmd.set_resource(resource)


#####################################################################
#
# addBibItemPtfCmd: adds/remove a BibItem
#
# No verification is done to check if a BibItem already exists
# Rationale: BibItems are only added in a loop within an article.
# The check is actually the existence of the article.
#
# Exception raised:
#    - ValueError if the init params are empty
#    - exceptions.ResourceDoesNotExist during undo if the BibItem does not exist
#    - RuntimeError during undo if resources are still published
#
######################################################################
class addBibItemPtfCmd(addPtfCmd):
    def __init__(self, params=None):
        super().__init__(params)

        self.cmd = addBibItemDatabaseCmd(params)

    def set_resource(self, resource):
        self.cmd.set_resource(resource)


#####################################################################
#
# addBibItemIdPtfCmd: adds/remove a BibItemId
#
# No verification is done to check if a BibItemId already exists
# Rationale: BibItems are only added inside an article/book
# The check is actually the existence of the resource.
#
# Exception raised:
#    - ValueError if the init params are empty
#    - exceptions.ResourceDoesNotExist during undo if the BibItemId does not exist
#    - RuntimeError during undo if resources are still published
#
######################################################################
class addBibItemIdPtfCmd(addPtfCmd):
    def __init__(self, params=None):
        super().__init__(params)

        self.cmd = addBibItemIdDatabaseCmd(params)

    def set_bibitem(self, bibitem):
        self.cmd.set_bibitem(bibitem)


#####################################################################
#
# addFrontMatterPtfCmd: adds/remove a FrontMatter
#
# No verification is done to check if a FrontMatter already exists
# Rationale: FrontMatters are only added inside a book
# The check is actually the existence of the book.
#
# Exception raised:
#    - ValueError if the init params are empty
#    - exceptions.ResourceDoesNotExist during undo if the FrontMatter does not exist
#    - RuntimeError during undo if resources are still published
#
######################################################################
class addFrontMatterPtfCmd(addPtfCmd):
    def __init__(self, params=None):
        super().__init__(params)

        self.cmd = addFrontMatterDatabaseCmd(params)

    def set_resource(self, resource):
        self.cmd.set_resource(resource)


#####################################################################
#
# addRelationshipPtfCmd: adds/remove a Relationship
#
# Relationship relates 2 resources (ex: articles) with a relation. ex "follows", "followed-by"
#
# RelationName are created with a fixture (see app/ptf/apps/ptf/fixtures/initial_data.json
# Example { "left" : "follows", "right" : "followed-by" }
# A related-article of an article has 1 relation name (ex "follows" or "followed-by")
# You need to know if the relation was stored in the left or right attribute of a RelationName,
# so that you can create/search the Relationship with the correct object/subject.
# Ex: with A "follows" B, A is the subject and B the object because "follows" is a RelationName.left attribute
#     with A "followed-by" B, A is the object the B the subject because "followed-by" is a RelationName.right attribute
# A Relationship relates 2 resources with a RelationName
#
# Exception raised:
#    - ValueError if the init params are empty
#    - exceptions.ResourceExists during do if the Relationship already exists
#    - exceptions.ResourceDoesNotExist during undo if the Relationship does not exist
#    - RuntimeError during undo if resources are still published
#
######################################################################
class addRelationshipPtfCmd(addPtfCmd):
    def __init__(self, params=None):
        super().__init__(params)

        self.cmd = addRelationshipDatabaseCmd(params)

    def set_subject_resource(self, resource):
        self.cmd.set_subject_resource(resource)

    def set_object_resource(self, resource):
        self.cmd.set_object_resource(resource)

    def set_relationname(self, relationname):
        self.cmd.set_relationname(relationname)


#####################################################################
#
# addPublisherPtfCmd: adds/remove a publisher
# params: 'name', 'location'
#
# Exception raised:
#    - ValueError if the init params are empty
#    - exceptions.ResourceExists during do if the Publisher already exists
#    - exceptions.ResourceDoesNotExist during undo if the Publisher does not exist
#
######################################################################
class addPublisherPtfCmd(addPtfCmd):
    def __init__(self, params=None):
        super().__init__(params)

        self.cmd = addPublisherDatabaseCmd(params)
        # self.sub_cmd = addPublisherSolrCmd(params)


#####################################################################
#
# addResourcePtfCmd: adds/remove folder for a Resource
#
#
# is responsible of creation/deletion of resource folders
######################################################################
class addResourcePtfCmd(addPtfCmd):
    def post_do(self, obj):
        super().post_do(obj)
        # if self.from_folder and self.to_folder:
        #     # binary_files (PDF, images, TeX, Attach) are copied in the addRelatedObjectPtfCmd::pre_do
        #     # We only need to copy the html images
        #     resolver.copy_html_images(obj, from_folder=self.from_folder, to_folder=self.to_folder)

    def pre_undo(self):
        super().pre_undo()
        if self.object_to_be_deleted and self.to_folder:
            resolver.delete_object_folder(
                object_folder=self.object_to_be_deleted.get_relative_folder(),
                to_folder=self.to_folder,
            )


#####################################################################
#
# addCollectionPtfCmd: adds/remove a journal
# a Collection needs a Provider object
#
# params: 'coltype', 'title_xml', 'wall',
#         'pid', 'sid',
#         'title_tex', 'title_html',
#         'other_ids'  Ex. [ ('cedram-id','AFST'), ('issn', '0240-2963') ]
#
# Exception raised:
#    - ValueError if the init params are empty
#    - exceptions.ResourceExists during do if the Collection already exists
#    - exceptions.ResourceDoesNotExist during undo if the Collection does not exist
#
######################################################################
class addCollectionPtfCmd(addResourcePtfCmd):
    def __init__(self, params=None):
        super().__init__(params)

        self.cmd = addCollectionDatabaseCmd(params)

    #        self.sub_cmd = addCollectionSolrCmd(params)

    def set_provider(self, provider):
        self.cmd.set_provider(provider)

    def set_parent(self, parent):
        self.cmd.set_parent(parent)


#####################################################################
#
# addContainerPtfCmd: adds/remove an issue
# a Container needs a Collection (journal, book-series) that needs a Provider object
#
# params: 'year', 'vseries', 'volume', 'number'
#         'doi','seq',
#
#    (params common to Container/Article)
#         'title_xml', 'title_tex', 'title_html', 'lang',
#         'other_ids'  Ex: [ ('cedram-id','AFST'), ('issn', '0240-2963') ]
#         'abstracts'  Ex: [ { 'tag': tag, 'lang': lang, 'value': value } ]
#         'contributors' Ex: [ { 'first_name': 'John', "corresponding": True...}, ... ]
#         'kwd_groups' Ex1: [ { 'content_type': content_type, 'lang': lang, 'value': value } ]
#                      Ex2: # [ { 'content_type': content_type, 'lang': lang,
#                                 'kwds': [ value1, value2,... ] } ]
#
# Exception raised:
#    - ValueError if the init params are empty
#    - exceptions.ResourceExists during do if the issue already exists
#    - exceptions.ResourceDoesNotExist during undo if the Container does not exist
#
######################################################################
class addContainerPtfCmd(addResourcePtfCmd):
    def __init__(self, params=None):
        super().__init__(params)
        self.required_params.extend(["xobj"])

        self.cmd = addContainerDatabaseCmd(params)
        if hasattr(self, "xobj") and (
            self.xobj.ctype.startswith("book") or self.xobj.ctype == "lecture-notes"
        ):
            self.sub_cmd = addContainerSolrCmd(params)

        self.article_ids = []

    def add_collection(self, collection):
        self.cmd.add_collection(collection)
        if self.sub_cmd:
            self.sub_cmd.add_collection(collection)

    def set_publisher(self, publisher):
        pass

    #        self.sub_cmd.publisher_id = publisher.id

    def set_provider(self, provider):
        self.cmd.set_provider(provider)

    def pre_undo(self):
        # To delete a container directly (cmd=addContainerPtfCmd({'pid':pid,'ctype':ctype}); cmd.undo() and
        # associated set)
        # you simply need to pass its pid AND ctype.
        # addContainerPtfCmd is then responsible to remove the issue and its articles from the system
        # Django automatically remove all objects related to the container (cascade)
        # But we need to manually remove the articles of the container from SolR
        # Store the article ids in pre_undo and delete the Solr articles in
        # internal_undo
        #
        # addResourcePtfCmd is responsible to remove articles binary files from the system

        super().pre_undo()
        if self.object_to_be_deleted:
            for article in self.object_to_be_deleted.article_set.all():
                self.article_ids.append(article.id)

                # Exception to the Django cascade mecanism: Relationship.
                # A Relationship links 2 articles.
                # If an article is removed, Django automatically deletes the Relationship.
                # It's not good, we want the relationship to remain, but the article field set to None

                qs = Relationship.objects.filter(resource=article)
                for r in qs:
                    if r.related is None:
                        r.delete()
                    else:
                        r.resource = None
                        r.save()
                qs = Relationship.objects.filter(related=article)
                for r in qs:
                    if r.resource is None:
                        r.delete()
                    else:
                        r.related = None
                        r.save()

    def internal_undo(self):
        for id in self.article_ids:
            cmd = addArticleSolrCmd({"id": id, "solr_commit": False})
            cmd.undo()

        id = super().internal_undo()
        return id

    def post_undo(self):
        super().post_undo()

        Person.objects.clean()


#####################################################################
#
# addArticlePtfCmd: adds/remove an article
# an Article needs a Container that needs a Collection (Journal) that needs a Provider object
#
# params: fpage, lpage, doi, seq, atype (article type), page_range, elocation, article_number, talk_number
#
#        pseq (parent seq)
#        related_article ?
#
#    (params common to Container/Article)
#         'title_xml', 'title_tex', 'title_html', 'lang',
#         'other_ids'  Ex: [ ('cedram-id','AFST'), ('issn', '0240-2963') ]
#         'abstracts'  Ex: [ { 'tag': tag, 'lang': lang, 'value': value } ]
#         'contributors' Ex: [ { 'first_name': 'John', "corresponding": True...}, ... ]
#         'kwd_groups' Ex1: [ { 'content_type': content_type, 'lang': lang, 'value': value } ]
#                      Ex2: # [ { 'content_type': content_type, 'lang': lang,
#                                 'kwds': [ value1, value2,... ] } ]


#
# Exception raised:
#    - ValueError if the init params are empty
#    - exceptions.ResourceExists during do if the article already exists
#    - exceptions.ResourceDoesNotExist during undo if the Article does not exist
#
######################################################################
class addResourceInSpecialIssuePtfCmd(addResourcePtfCmd):
    def __init__(self, params=None):
        super().__init__(params)
        self.cmd = addResourceInSpecialIssueDatabaseCmd(params)


class addArticlePtfCmd(addResourcePtfCmd):
    def __init__(self, params=None):
        super().__init__(params)
        self.cmd = addArticleDatabaseCmd(params)

        # is_cr = False
        # if (hasattr(settings, 'SITE_NAME') and len(settings.SITE_NAME) == 6 and settings.SITE_NAME[
        #                                                                         0:2] == "cr"):
        #     is_cr = True
        #
        # to_appear = False
        # if (params is not None and 'xobj' in params and
        #     hasattr(settings, 'ISSUE_TO_APPEAR_PID') and
        #     params['xobj'].pid.find(settings.ISSUE_TO_APPEAR_PID) == 0):
        #     to_appear = True
        #
        # # The articles to appear are not stored in the search engine.
        # if is_cr or not to_appear:
        self.sub_cmd = addArticleSolrCmd(params)

    def set_container(self, container):
        self.cmd.set_container(container)
        if self.sub_cmd:
            self.sub_cmd.set_container(container)

    def set_provider(self, provider):
        self.cmd.set_provider(provider)

    def set_eprint(self, eprint):
        self.sub_cmd.set_eprint(eprint)

    def set_source(self, source):
        self.sub_cmd.set_source(source)

    def set_thesis(self, thesis):
        self.sub_cmd.set_thesis(thesis)

    def add_collection(self, collection):
        self.cmd.set_collection(collection)

        if self.sub_cmd:
            self.sub_cmd.add_collection(collection)

    def post_do(self, article):
        super().post_do(article)
        for xtrans_article, trans_article in zip(
            self.xobj.translations, self.cmd.translated_articles
        ):
            solr_xtrans_article = copy.deepcopy(xtrans_article)
            solr_xtrans_article.trans_title_tex = self.xobj.title_tex
            solr_xtrans_article.trans_title_html = self.xobj.title_html
            if article.trans_lang == xtrans_article.lang:
                if article.trans_title_tex:
                    solr_xtrans_article.title_tex = article.trans_title_tex
                    solr_xtrans_article.title_html = article.trans_title_html
                for abstract in self.xobj.abstracts:
                    if abstract["tag"] == "abstract" and abstract["lang"] == xtrans_article.lang:
                        solr_xtrans_article.abstracts = [abstract]

            sub_cmd = addArticleSolrCmd({"xobj": solr_xtrans_article})
            sub_cmd.set_container(article.my_container)
            sub_cmd.add_collection(article.get_collection())
            sub_cmd.db_obj = trans_article
            sub_cmd.id = trans_article.id
            sub_cmd.pid = trans_article.pid
            sub_cmd.do()
            # xtrans_article.doi = doi_sav

    def pre_undo(self):
        super().pre_undo()

        qs = Relationship.objects.filter(resource=self.object_to_be_deleted)
        for r in qs:
            if r.related is None:
                r.delete()
            else:
                r.resource = None
                r.save()
        qs = Relationship.objects.filter(related=self.object_to_be_deleted)
        for r in qs:
            if r.resource is None:
                r.delete()
            else:
                r.related = None
                r.save()

    def internal_undo(self):
        if self.object_to_be_deleted:
            cmd = addArticleSolrCmd({"id": self.object_to_be_deleted.id, "solr_commit": False})
            cmd.undo()

            for trans_article in self.object_to_be_deleted.translations.all():
                cmd = addArticleSolrCmd({"id": trans_article.id, "solr_commit": False})
                cmd.undo()

        id = super().internal_undo()
        return id


#####################################################################
#
# addBookPartPtfCmd: adds/remove a book part
#
# TODO an Article is used to store a book part in the database. Why not use a JournalArticle in SolR ?
#
# params: 'year', 'fpage', 'lpage'
#         'colid'   Ex: [ 1,2 ]
#
#    (params common to Book)
#         'title_xml', 'title_tex', 'title_html', 'lang',
#         'other_ids'  Ex: [ ('cedram-id','AFST'), ('issn', '0240-2963') ]
#         'ext_ids'  Ex: [ ('zbl-item-id','0216.23901'), ('mr-item-id', '289322') ]
#         'abstracts'  Ex: [ { 'tag': tag, 'lang': lang, 'value': value } ]
#         'contributors' Ex: [ { 'first_name': 'John', "corresponding": True...}, ... ]
#         'kwd_groups' Ex1: [ { 'content_type': content_type, 'lang': lang, 'value': value } ]
#                      Ex2: # [ { 'content_type': content_type, 'lang': lang,
#                                 'kwds': [ value1, value2,... ] } ]
#         'bibitem' Ex: ["1) Name - Title", "2) Name2 - Title2" ]
#
# Exception raised:
#    - ValueError if the init params are empty
#    - exceptions.ResourceExists during do if the book part already exists
#    - exceptions.ResourceDoesNotExist during undo if the BookPart does not exist
#
######################################################################
class addBookPartPtfCmd(addResourcePtfCmd):
    def __init__(self, params=None):
        super().__init__(params)

        self.cmd = addArticleDatabaseCmd(params)
        self.sub_cmd = addBookPartSolrCmd(params)

    def set_container(self, container):
        self.cmd.set_container(container)
        self.sub_cmd.set_container(container)
        # 'colid' is used to find the collection of a book part
        # TODO store the book_id as well ?

    def add_collection(self, collection):
        # manage collection MBK : only index the other collection
        if collection.pid != "MBK":
            self.sub_cmd.add_collection(collection)


##########################################################################
##########################################################################
#
#                                 Update Commands
#
##########################################################################
##########################################################################


#####################################################################
#
# updateCollectionPtfCmd: updates a journal
# a Collection needs a Provider object
#
# params: 'coltype', 'title_xml', 'wall',
#         'pid', 'sid',
#         'title_tex', 'title_html',
#         'other_ids'  Ex. [ ('cedram-id','AFST'), ('issn', '0240-2963') ]
#
# Exception raised:
#    - ValueError if the init params are empty
#    - exceptions.ResourceDoesNotExist during do if the Collection does not exist
#
######################################################################
class updateCollectionPtfCmd(addPtfCmd):
    def __init__(self, params=None):
        super().__init__(params)

        self.cmd = updateCollectionDatabaseCmd(params)
        # self.sub_cmd = addCollectionSolrCmd(params)

    def set_provider(self, provider):
        self.cmd.set_provider(provider)

    def set_publisher(self, publisher):
        self.sub_cmd.set_publisher(publisher)


#####################################################################
#
# updateResourceIdPtfCmd: upates an existing ResourceId
# params:  'id_type':  'doi', 'issn', 'e-issn'
#          'id_value'
#
# Needs a Resource object (required)
#
# Exception raised:
#    - ValueError if the init params are empty
#    - exceptions.ResourceDoesNotExist during do if the ResourceId does not exist
#
######################################################################
class updateResourceIdPtfCmd(addPtfCmd):
    def __init__(self, params={}):
        super().__init__(params)

        self.cmd = updateResourceIdDatabaseCmd(params)

    def set_resource(self, resource):
        self.cmd.set_resource(resource)


#####################################################################
#
# updateExtLinkPtfCmd: upates an existing ExtLink
# params:  'rel':  'website' or 'small_icon'
#          'mimetype', 'location', 'metadata', 'seq'
#
# Needs a Resource object (required)
# TODO: update the related XmlBase object
#
# Exception raised:
#    - ValueError if the init params are empty
#    - exceptions.ResourceDoesNotExist during do if the ExtLink does not exist
#
######################################################################
class updateExtLinkPtfCmd(addPtfCmd):
    def __init__(self, params=None):
        super().__init__(params)

        self.cmd = updateExtLinkDatabaseCmd(params)

    def set_resource(self, resource):
        self.cmd.set_resource(resource)


class importExtraDataPtfCmd(baseCmd):
    """
    Restore additional info, such as checked/false_positive attributes on extid/bibitemid

    results: articles are updated
    """

    def __init__(self, params=None):
        self.pid = None
        self.import_folder = None

        super().__init__(params)

        self.required_params.extend(["pid", "import_folder"])

    def copy_file(self, filename, resource, from_pid):
        # on recupere potentiellement l'image positionnée via ptf-tools pour la resource
        # il faut renommer l'image car la logique est d'avoir une image avec pour nom pid.EXT
        # En cas de déplacement d'online first, from_pid peut être différent de resource.pid
        basename = os.path.basename(filename)
        extension = os.path.splitext(filename)[1]
        if (f"{from_pid}{extension}") == basename:
            new_basename = f"{resource.pid}{extension}"
            from_path = os.path.join(self.import_folder, filename)
            new_filename = os.path.join(resource.get_relative_folder(), new_basename)
            to_path = os.path.join(settings.MERSENNE_TEST_DATA_FOLDER, new_filename)
            resolver.copy_file(from_path, to_path)
            filename = new_filename
        return filename

    def import_article_extra_info(self, article, article_data):
        if article_data is None:
            return

        for extid_data in article_data["extids"]:
            model_helpers.add_or_update_extid(
                article,
                extid_data["type"],
                extid_data["value"],
                extid_data["checked"],
                extid_data["false_positive"],
                False,
            )

        for ref_data in article_data["references"]:
            bibitem = model_helpers.get_bibitem_by_seq(article, ref_data["seq"])
            if bibitem:
                for bibid_data in ref_data["bibids"]:
                    model_helpers.add_or_update_bibitemid(
                        bibitem,
                        bibid_data["type"],
                        bibid_data["value"],
                        bibid_data["checked"],
                        bibid_data["false_positive"],
                        False,
                    )

        if "date_published" in article_data:
            date = model_helpers.parse_date_str(article_data["date_published"])
            article.date_published = date
            article.save()

        if "date_pre_published" in article_data:
            date = model_helpers.parse_date_str(article_data["date_pre_published"])
            article.date_pre_published = date
            article.save()

        if "date_online_first" in article_data:
            date = model_helpers.parse_date_str(article_data["date_online_first"])
            article.date_online_first = date
            article.save()

        if "deployed_date" in article_data:
            date = model_helpers.parse_date_str(article_data["deployed_date"])
            ptfSite = model_helpers.get_site_mersenne(article.get_top_collection().pid)
            article.deploy(ptfSite, date)

        if "icon" in article_data:
            file = self.copy_file(article_data["icon"], article, article_data["pid"])
            cmd = addorUpdateExtLinkPtfCmd({"rel": "icon", "location": file})
            cmd.set_resource(article)
            cmd.do()

        if "show_body" in article_data:
            article.show_body = article_data["show_body"]
            article.save()

        if "do_not_publish" in article_data:
            article.do_not_publish = article_data["do_not_publish"]
            article.save()

        if (
            settings.SITE_NAME == "ptf_tools"
            and "doi_status" in article_data
            and article_data["doi_status"] != 0
        ):
            if (
                article.pid == article_data["pid"]
            ):  # on restreint aux articles qui ne changent pas de pid
                from mersenne_tools.models import DOIBatch
                from ptf_tools.doi import get_doibatch

                doib = get_doibatch(article)
                if not doib:
                    doibatch = DOIBatch(
                        resource=article,
                        status=article_data["doi_status"],
                        id=article_data["doibatch_id"],
                        xml=article_data["doibatch_xml"],
                        log="-- import --",
                    )
                    doibatch.save()

    def import_container_extra_info(self, container, data):
        ptfSite = model_helpers.get_site_mersenne(container.my_collection.pid)

        if "deployed_date" in data:
            date = model_helpers.parse_date_str(data["deployed_date"])
            container.deploy(ptfSite, date)

        if "icon" in data:
            file = self.copy_file(data["icon"], container, container.pid)
            cmd = addorUpdateExtLinkPtfCmd({"rel": "icon", "location": file})
            cmd.set_resource(container)
            cmd.do()

        for article_data in data["articles"]:
            article = None
            if article_data["doi"]:
                article = model_helpers.get_article_by_doi(article_data["doi"])
            if not article:
                article = model_helpers.get_article(article_data["pid"])
            if article:
                self.import_article_extra_info(article, article_data)

    def internal_do(self):
        super().internal_do()
        article_pid = None

        resource = model_helpers.get_resource(self.pid)
        if not resource:
            raise exceptions.ResourceDoesNotExist(f"Resource {self.pid} does not exist")

        obj = resource.cast()

        classname = obj.classname.lower()
        if classname == "article":
            article_pid = self.pid

        container = obj.get_container()
        container_pid = container.pid
        collection = container.my_collection

        file = resolver.get_archive_filename(
            self.import_folder, collection.pid, container_pid, "json", article_pid=article_pid
        )

        if os.path.exists(file):
            with open(file, encoding="utf-8") as f:
                data = json.load(f)

            fct_name = f"import_{classname}_extra_info"
            ftor = getattr(self, fct_name, None)
            if callable(ftor):
                ftor(obj, data)


#####################################################################
#
# addDjvuPtfCmd: add a Djvu to an existing issue
# Used when an issue is sent to Numdam by ptf-tools
#
# Needs a Resource object (required)
#
# Exception raised:
#    - ValueError if the init params are empty
#
######################################################################
class addDjvuPtfCmd(baseCmd):
    def __init__(self, params={}):
        self.resource = None

        super().__init__(params)

        self.required_params.extend(["resource"])

    def set_resource(self, resource):
        self.resource = resource

    def convert_pdf_to_djvu(self):
        obj = self.resource.cast()
        qs = obj.datastream_set.filter(mimetype="image/x.djvu")
        if qs.count() == 0:
            qs = obj.datastream_set.filter(mimetype="application/pdf")
            if qs.count() != 0:
                datastream = qs.first()
                location = datastream.location.replace(".pdf", ".djvu")

                folder = settings.MERSENNE_PROD_DATA_FOLDER
                if (
                    hasattr(settings, "NUMDAM_COLLECTIONS")
                    and obj.my_container.my_collection.pid in settings.NUMDAM_COLLECTIONS
                ):
                    folder = settings.MERSENNE_TEST_DATA_FOLDER

                # Create the djvu in MERSENNE_PROD_DATA_FOLDER (used to archive)
                djvu_filename = os.path.join(folder, location)

                if not os.path.isfile(djvu_filename):
                    pdf_filename = os.path.join(folder, datastream.location)
                    if not os.path.isfile(pdf_filename):
                        pdf_filename = os.path.join(
                            settings.MERSENNE_TEST_DATA_FOLDER, datastream.location
                        )

                    cmd_str = "pdf2djvu --quiet --dpi 600 --output {} {}".format(
                        djvu_filename, pdf_filename
                    )

                    subprocess.check_output(cmd_str, shell=True)

                    # Copy the new djvu in MERSENNE_TEST_DATA_FOLDER (used to deploy)
                    djvu_filename_in_test = os.path.join(
                        settings.MERSENNE_TEST_DATA_FOLDER, location
                    )
                    if djvu_filename_in_test != djvu_filename:
                        resolver.copy_file(djvu_filename, djvu_filename_in_test)

                cmd = addDataStreamDatabaseCmd(
                    {
                        "rel": "full-text",
                        "mimetype": "image/x.djvu",
                        "location": location,
                        "text": "Full (DJVU)",
                        "seq": qs.count() + 1,
                    }
                )
                cmd.set_resource(obj)
                cmd.do()

                if (
                    not hasattr(obj, "ctype")
                    or (hasattr(obj, "ctype") and obj.ctype.startswith("book"))
                    or (hasattr(obj, "ctype") and obj.ctype == "lecture-notes")
                ):
                    self.update_solr(obj, location)

    def update_solr(self, resource, djvu_location):
        params = {"djvu": djvu_location}
        cmd = updateResourceSolrCmd(params)
        cmd.set_resource(resource)
        cmd.do()

    # Convert the PDF in Djvu
    def internal_do(self):
        super().internal_do()

        self.convert_pdf_to_djvu()


#####################################################################
#
# addorUpdateContribsPtfCmd: update the list of contributions of a Resource
# Remove the existing contributions and replace with the new ones
#
# Needs a Resource object (required)
#
# Exception raised:
#    - ValueError if the init params are empty
#
######################################################################
class addorUpdateContribsPtfCmd(baseCmd):
    def __init__(self, params={}):
        self.resource = None
        self.contributors = []

        super().__init__(params)

        self.required_params.extend(["resource"])

    def set_resource(self, resource):
        self.resource = resource

    def internal_do(self):
        super().internal_do()

        self.resource.contributions.all().delete()
        add_contributors(self.contributors, self.resource)

        cmd = updateResourceSolrCmd({"contributors": self.contributors})
        cmd.set_resource(self.resource)
        cmd.do()


#####################################################################
#
# addorUpdateKwdsPtfCmd: update the keywords of a Resource
# Remove the existing keywords and replace with the new ones
#
# Needs a Resource object (required)
#
# TODO: pass a list of kwd_groups instead of separate kwd_<lang> values
#
# Exception raised:
#    - ValueError if the init params are empty
#
######################################################################
# class addorUpdateKwdsPtfCmd(baseCmd):
#     def __init__(self, params={}):
#         self.resource = None
#         self.kwds_fr = None
#         self.kwds_en = None
#         self.kwd_uns_fr = None
#         self.kwd_uns_en = None
#
#         super(addorUpdateKwdsPtfCmd, self).__init__(params)
#
#         self.required_params.extend(['resource'])
#
#     def set_resource(self, resource):
#         self.resource = resource
#
#     def addOrUpdateKwds(self, kwd_uns, kwds, lang):
#         kwds_groups_qs = self.resource.kwdgroup_set.filter(content_type='', lang=lang)
#         if kwds_groups_qs.exists():
#             # There is already a kwd_group.
#             group = kwds_groups_qs.first()
#             # First, delete all its kwds
#             group.kwd_set.all().delete()
#             group.delete()
#
#         new_kwd_group = None
#
#         if kwd_uns or kwds:
#             new_kwd_group = {'content_type': '', 'lang': lang, 'kwds': kwds}
#             if kwd_uns:
#                 new_kwd_group['value_tex'] = kwd_uns
#                 new_kwd_group['value_html'] = kwd_uns
#                 new_kwd_group[
#                     'value_xml'] = '<unstructured-kwd-group xml:space="preserve">' + kwd_uns + '</unstructured-kwd-group>'
#             else:
#                 # Build value_tex and value_html for display and SolR
#                 # But do not create value_xml: it is done by the XML export templates (OAI, PubMed)
#                 value = ''
#                 for kwd in kwds:
#                     if value:
#                         value += ', '
#                     value += kwd
#                 new_kwd_group['value_tex'] = value
#                 new_kwd_group['value_html'] = value
#
#             addKwdGroup(new_kwd_group, self.resource)
#
#         return new_kwd_group
#
#     def internal_do(self):
#         super(addorUpdateKwdsPtfCmd, self).internal_do()
#
#         kwd_groups = []
#         kwd_group = self.addOrUpdateKwds(self.kwd_uns_fr, self.kwds_fr, 'fr')
#         if kwd_group:
#             kwd_groups.append(kwd_group)
#
#         kwd_group = self.addOrUpdateKwds(self.kwd_uns_en, self.kwds_en, 'en')
#         if kwd_group:
#             kwd_groups.append(kwd_group)
#
#         cmd = updateResourceSolrCmd({'kwd_groups': kwd_groups})
#         cmd.set_resource(self.resource)
#         cmd.do()


#####################################################################
#
# addorUpdateExtLinkPtfCmd: update the list of contribs of a Resource
# Remove the existing contribs and replace with the new ones
#
# Needs a Resource object (required)
#
# Exception raised:
#    - ValueError if the init params are empty
#
# TODO : les images de couv - les icon - sont stockées ici mais du coup ne profite pas DIRECTEMENT de la logique de copie de fichiers des RelatedObjects
######################################################################
class addorUpdateExtLinkPtfCmd(baseCmd):
    def __init__(self, params={}):
        self.resource = None
        self.location = None
        self.rel = None
        self.mimetype = ""

        super().__init__(params)

        self.required_params.extend(["resource", "rel"])

    def set_resource(self, resource):
        self.resource = resource

    def internal_do(self):
        super().internal_do()

        extlink_qs = ExtLink.objects.filter(resource=self.resource, rel=self.rel)

        if extlink_qs.exists():
            extlink = extlink_qs.first()
            if self.location:
                extlink.location = self.location
                extlink.save()
            else:
                extlink.delete()
        elif self.location:
            params = {
                "rel": self.rel,
                "mimetype": self.mimetype,
                "location": self.location,
                "seq": 1,
                "metadata": "",
            }

            cmd = addExtLinkPtfCmd(params)
            cmd.set_resource(self.resource)
            cmd.do()


#####################################################################
#
# updateArticlePtfCmd: update an existing Article
# Olivier: 12/06/2020. This function needs major refactoring.
#    If page_count is not provided, it gets deleted.
#    There should be a way to pass only attributes to edit
#
# Needs an Article object (required)
#
# Exception raised:
#    - ValueError if the init params are empty
#
######################################################################
class updateArticlePtfCmd(baseCmd):
    def __init__(self, params={}):
        self.article = None
        self.title_xml = None
        self.title_html = None
        self.title_tex = None
        self.authors = None
        self.page_count = None
        self.use_page_count = True
        self.icon_location = None
        self.body = None
        self.body_tex = None
        self.body_html = None
        self.body_xml = None
        # self.use_kwds = None
        # self.kwds_fr = None
        # self.kwds_en = None
        # self.kwd_uns_fr = None
        # self.kwd_uns_en = None

        super().__init__(params)

        self.required_params.extend(["article"])

    def set_article(self, article):
        self.article = article

    def internal_do(self):
        super().internal_do()

        container = self.article.my_container
        collection = container.my_collection

        if self.title_tex and self.title_html and self.title_xml:
            self.article.title_tex = self.title_tex
            self.article.title_html = self.title_html
            self.article.title_xml = self.title_xml
            self.article.save()

        if self.body_xml or self.body_html or self.body_tex:
            self.article.body_tex = self.body_tex
            self.article.body_html = self.body_html
            self.article.body_xml = self.body_xml
            self.article.save()

        # Authors
        if self.authors:
            params = {"contributors": self.authors}
            cmd = addorUpdateContribsPtfCmd(params)
            cmd.set_resource(self.article)
            cmd.do()

        # Page count
        if self.use_page_count:
            qs = self.article.resourcecount_set.filter(name="page-count")
            if qs.exists():
                qs.first().delete()
            if self.page_count:
                seq = self.article.resourcecount_set.count() + 1
                params = {"name": "page-count", "value": self.page_count, "seq": seq}
                cmd = addResourceCountPtfCmd(params)
                cmd.set_resource(self.article)
                cmd.do()

        # Add a DataStream for the PDF
        qs = self.article.datastream_set.filter(mimetype="application/pdf")
        if not qs.exists():
            folder = resolver.get_relative_folder(collection.pid, container.pid, self.article.pid)
            location = os.path.join(folder, self.article.pid + ".pdf")
            params = {
                "rel": "full-text",
                "mimetype": "application/pdf",
                "location": location,
                "seq": self.article.datastream_set.count() + 1,
                "text": "Full (PDF)",
            }
            cmd = addDataStreamPtfCmd(params)
            cmd.set_resource(self.article)
            cmd.do()

        # image ajoutée via ptf-tools pour un article
        if self.icon_location:
            params = {"rel": "icon", "location": self.icon_location}
            cmd = addorUpdateExtLinkPtfCmd(params)
            cmd.set_resource(self.article)
            cmd.do()

        # Kwds
        # if self.use_kwds:
        #     params = {'kwds_en': self.kwds_en, 'kwds_fr': self.kwds_fr,
        #               'kwd_uns_en': self.kwd_uns_en, 'kwd_uns_fr': self.kwd_uns_fr}
        #     cmd = addorUpdateKwdsPtfCmd(params)
        #     cmd.set_resource(self.article)
        #     cmd.do()

        if self.body or self.title_tex:
            params = {}
            if self.body:
                params["body"] = self.body
            if self.title_tex and self.title_html:
                params["title_tex"] = self.title_tex
                params["title_html"] = self.title_html

            cmd = updateResourceSolrCmd(params)
            cmd.set_resource(self.article)
            cmd.do()


#####################################################################
#
# updateContainerPtfCmd: update an existing Container
#
# Needs a Container object (required)
#
# Exception raised:
#    - ValueError if the init params are empty
#
######################################################################
class updateContainerPtfCmd(baseCmd):
    def __init__(self, params={}):
        self.resource = None
        self.icon_location = None

        super().__init__(params)

        self.required_params.extend(["resource"])

    def set_resource(self, resource):
        self.resource = resource

    def internal_do(self):
        super().internal_do()

        params = {"rel": "icon", "location": self.icon_location}
        cmd = addorUpdateExtLinkPtfCmd(params)
        cmd.set_resource(self.resource)
        cmd.do()


##########################################################################
##########################################################################
#
#                                 Export Commands
#
##########################################################################
##########################################################################


class exportExtraDataPtfCmd(baseCmd):
    """
    Exports additional info, such as checked/false_positive attributes on extid/bibitemid

    force_pid is only used when the volume to be published becomes published
    Ex: AIF_0_0 becomes AIF_2018. We want to backup data in AIF_2018.json
        so that additional are restored when AIF_2018.xml is read

    export_all export all extids.
    If you want to archive, export_all should be False (checked extids are in the XML)
    If you want to store in a temp file (updateXML), then export_all should be True
        to preserve new extids found by the matching an not present in the XML

    if with_binary_files = True, copy in tempFolder, binary files set by ptf-tools ( extlink(rel='icon') )

    results: a json file on disk
    """

    def __init__(self, params=None):
        self.pid = None
        self.export_folder = None
        self.force_pid = None
        self.export_all = True
        self.with_binary_files = True

        super().__init__(params)

        self.required_params.extend(["pid", "export_folder"])

    def get_article_extra_info(self, article, export_all=False):
        data = None

        extids_data = []
        for extid in article.extid_set.all():
            extid_data = {}
            if export_all or not extid.checked or extid.false_positive:
                extid_data["type"] = extid.id_type
                extid_data["value"] = extid.id_value
                extid_data["checked"] = extid.checked
                extid_data["false_positive"] = extid.false_positive
            if extid_data:
                extids_data.append(extid_data)

        references_data = []
        for bib in article.bibitem_set.all():
            bibids_data = []
            for bibid in bib.bibitemid_set.all():
                bibid_data = {}
                if export_all or not bibid.checked or bibid.false_positive:
                    bibid_data["type"] = bibid.id_type
                    bibid_data["value"] = bibid.id_value
                    bibid_data["checked"] = bibid.checked
                    bibid_data["false_positive"] = bibid.false_positive

                if bibid_data:
                    bibids_data.append(bibid_data)

            if bibids_data:
                references_data.append({"seq": bib.sequence, "bibids": bibids_data})

        icon = None

        for extlink in article.extlink_set.filter(rel="icon"):
            if self.with_binary_files is True:
                icon = extlink.location

                # copy des imgs associées via ptf-tools
                from_path = os.path.join(settings.RESOURCES_ROOT, extlink.location)
                to_path = os.path.join(self.export_folder, extlink.location)
                resolver.create_folder(os.path.dirname(to_path))
                resolver.copy_file(from_path, to_path)

        if (
            extids_data
            or references_data
            or article.date_published
            or article.date_online_first
            or icon
        ):
            data = {
                "pid": article.pid,
                "doi": article.doi,
                "extids": extids_data,
                "references": references_data,
            }

            if export_all and icon:
                data["icon"] = icon

            if export_all and article.date_published:
                data["date_published"] = article.date_published

            if export_all and article.date_pre_published:
                data["date_pre_published"] = article.date_pre_published

            if export_all and article.date_online_first:
                data["date_online_first"] = article.date_online_first

            if export_all:
                data["show_body"] = article.show_body
                data["do_not_publish"] = article.do_not_publish

            if (
                export_all
                and settings.SITE_NAME == "ptf_tools"
                and not ((len(sys.argv) > 1 and sys.argv[1] == "test") or "pytest" in sys.modules)
            ):
                try:
                    data["doi_status"] = article.doibatch.status
                    data["doibatch_id"] = article.doibatch.id
                    data["doibatch_xml"] = article.doibatch.xml
                except ObjectDoesNotExist:
                    data["doi_status"] = 0

        return data

    def get_container_extra_info(self, container, export_all=False):
        result = {"pid": container.pid}

        collection = container.my_collection
        ptfSite = model_helpers.get_site_mersenne(collection.pid)

        if ptfSite and not self.force_pid:
            # si self.force_pid on est dans le cas où on passe un article de 0_0_0 vers issue final et dans ce cas là on ne conserve pas la deployed_date du 0_0_0
            deployed_date = container.deployed_date(ptfSite)
            if deployed_date:
                result["deployed_date"] = deployed_date

        icon = None
        for extlink in container.extlink_set.filter(rel="icon"):
            icon = extlink.location
            if self.with_binary_files is True:
                # copy des imgs associées via ptf-tools
                from_path = os.path.join(settings.MERSENNE_TEST_DATA_FOLDER, extlink.location)
                to_path = os.path.join(self.export_folder, extlink.location)
                resolver.create_folder(os.path.dirname(to_path))
                resolver.copy_file(from_path, to_path)

        if export_all and icon:
            result["icon"] = icon

        articles_data = []
        for article in container.article_set.all():
            data = self.get_article_extra_info(article, export_all)
            if data:
                articles_data.append(data)

        result["articles"] = articles_data

        return result

    def internal_do(self):
        super().internal_do()
        article_pid = None

        resource = model_helpers.get_resource(self.pid)
        if not resource:
            raise exceptions.ResourceDoesNotExist(f"Resource {self.pid} does not exist")

        obj = resource.cast()

        classname = obj.classname.lower()
        if classname == "article":
            article_pid = self.pid

        container = obj.get_container()
        container_pid = self.force_pid if self.force_pid else container.pid
        collection = container.get_top_collection()

        fct_name = f"get_{classname}_extra_info"
        ftor = getattr(self, fct_name, None)
        data = ftor(obj, self.export_all)

        file = resolver.get_archive_filename(
            self.export_folder,
            collection.pid,
            container_pid,
            "json",
            do_create_folder=True,
            article_pid=article_pid,
        )

        with open(file, "w", encoding="utf-8") as f:
            json.dump(data, f, default=myconverter)


class exportPtfCmd(baseCmd):
    """
    Generate the Article/Container/Collection XML

    Write on disk if export_folder is given as parameter
    Copy binary files if with_binary_files = True
    results: unicode string
    """

    def __init__(self, params=None):
        self.pid = None
        self.with_body = True
        self.with_djvu = True  # No djvu in Mersenne web sites
        self.article_standalone = False  # PCJ editor sets to True

        # Export le json des données internes (false_ids...).
        # Il faut alors un self.export_folder
        self.with_internal_data = False

        # Copie des fichiers binaires (PDF...) and l'export_folder
        self.with_binary_files = False

        self.export_folder = None

        # Permet de contrôler le répertoire source des fichiers binaires
        self.binary_files_folder = settings.RESOURCES_ROOT

        # Ajouter des métadonnées internes (deployed_date) ou non dans le XML
        self.for_archive = False

        # Permet au final d'exclure les articles marqués comme étant à ne pas publier
        self.export_to_website = False

        # Le XML dans l'onglet export n'a pas toutes les métadonnées
        self.full_xml = True

        super().__init__(params)

        self.required_params.extend(["pid"])

    def internal_do(self):
        super().internal_do()

        # J'AI ENLEVÉ LE SITES=fALSE donc si ça plante checker l'import de la collection dans la bdd
        resource = model_helpers.get_resource(self.pid)
        if not resource:
            raise exceptions.ResourceDoesNotExist(f"Resource {self.pid} does not exist")

        obj = resource.cast()

        # export Book ? need a visitor ? see oai_helpers
        if obj.classname == "Article":
            template_name = "oai/common-article_eudml-article2.xml"
            item_name = "article"
        elif obj.classname == "Container":
            if obj.ctype == "issue" or obj.ctype == "issue_special":
                template_name = "oai/common-issue_eudml-article2.xml"
                item_name = "container"
            # elif obj.ctype == "issue_special":
            #     template_name = "oai/special_issue.xml"
            #     item_name = "container"
            else:
                template_name = "oai/book_bits.xml"
                item_name = "book"
        elif obj.classname == "Collection":
            template_name = "collection.xml"
            item_name = "collection"
        else:
            raise ValueError("Only articles, containers or collections can be exported")

        if self.export_folder and self.with_internal_data and obj.classname == "Container":
            params = {
                "pid": self.pid,
                "export_folder": self.export_folder,
                "with_binary_files": self.with_binary_files,
            }
            exportExtraDataPtfCmd(params).do()

        p = model_helpers.get_provider("mathdoc-id")
        for_export = not self.for_archive
        safetext_xml_body = render_to_string(
            template_name,
            {
                item_name: obj,
                "no_headers": True,
                "provider": p.name,
                "with_body": self.with_body,
                "with_djvu": self.with_djvu,
                "for_disk": True,
                "for_export": for_export,
                "full_xml": self.full_xml,
                "export_to_website": self.export_to_website,
                "article_standalone": self.article_standalone,
            },
        )
        xml_body = str(safetext_xml_body)
        if not self.full_xml:
            parser = etree.XMLParser(
                huge_tree=True,
                recover=True,
                remove_blank_text=False,
                remove_comments=True,
                resolve_entities=True,
            )
            tree = etree.fromstring(xml_body.encode("utf-8"), parser=parser)
            xml_body = etree.tostring(tree, pretty_print=True).decode("utf-8")

        if self.export_folder:
            if obj.classname == "Collection":
                # Export of a collection XML: we don't attempt to write in the top collection
                file = resolver.get_archive_filename(
                    self.export_folder, obj.pid, None, "xml", True
                )
                with open(file, "w", encoding="utf-8") as f:
                    f.write(xml_body)

                if self.with_binary_files:
                    resolver.copy_binary_files(obj, self.binary_files_folder, self.export_folder)

            elif obj.classname == "Container":
                issue = obj
                collection = obj.get_top_collection()

                file = resolver.get_archive_filename(
                    self.export_folder, collection.pid, issue.pid, "xml", True
                )

                with open(file, "w", encoding="utf-8") as f:
                    f.write(xml_body)

                if self.with_binary_files:
                    resolver.copy_binary_files(issue, self.binary_files_folder, self.export_folder)

                    qs = issue.article_set.all()
                    if self.for_archive:
                        qs = qs.exclude(do_not_publish=True)
                    for article in qs:
                        resolver.copy_binary_files(
                            article, self.binary_files_folder, self.export_folder
                        )
            elif obj.classname == "Article":
                collection = obj.get_top_collection()
                file = resolver.get_archive_filename(
                    self.export_folder, collection.pid, None, "xml", True
                )

                with open(file, "w", encoding="utf-8") as f:
                    f.write(xml_body)

        return xml_body


class publishResourcePtfCmd(addPtfCmd):
    """
    Publish a container <=> Create a pub-date for all articles/book-parts of the container
    Publish an article <=> Create a pub-date
    """

    def __init__(self, params=None):
        self.params = params
        super().__init__(params)

    def set_resource(self, resource):
        obj = resource.cast()
        if obj.classname.find("Article") > -1:
            self.cmd = publishArticleDatabaseCmd(self.params)
            self.cmd.set_article(obj)
        else:
            self.cmd = publishContainerDatabaseCmd(self.params)
            self.cmd.set_container(obj)


def get_or_create_publisher(name):
    publisher = model_helpers.get_publisher(name)
    if publisher is None:
        publisher = PublisherData()
        publisher.name = name
        publisher = addPublisherPtfCmd({"xobj": publisher}).do()
    return publisher
