import html
import os

from lxml import etree
from lxml import objectify
from lxml.html import fromstring

from ptf.model_data import ContributorDict
from ptf.model_data import ExtLinkDict


# Unicode to XML
def escape(string: str):
    return string.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")


# Replace html entities like &phi; by their corresponding unicode characters
# except for XML reserved characters (& < >)
def replace_html_entities(text):
    # the mathtml 2 entities are not always identical to the HTML entities
    # See https://www.w3.org/TR/xml-entity-names/#changes20080721
    # Manually map the differences
    text = text.replace("&varepsilon;", chr(949))
    text = text.replace("&OverBar;", chr(175))
    text = text.replace("&UnderBar;", " " + chr(818))
    text = text.replace("&eacute;", chr(233))
    text = text.replace("&Eacute;", chr(201))
    text = text.replace("&ccedil;", chr(231))
    text = text.replace("&Ccedil;", chr(199))

    # cdrxml.xml files have XML/MathML (?) entities like &pĥiv;
    # There are converted to unicode caracters in recent /cedram_dev/exploitation files (AIF > 2013)
    # But are kept intact in old ones
    # Need to map the differences
    text = text.replace("&phiv;", chr(966))
    text = text.replace("&phi;", chr(981))

    # text has html entities like &phi; that need to be replaced by the unicode character.
    # But html.replace() will also replace &lt; &gt; &amp;
    # The proper solution would be to not call get_xml_from_node and continue the recursive parsing of mathml nodes
    # A hack is used: we change the &lt; call html.unescape then restore the &lt;
    text = text.replace("&lt;", "&mylt;").replace("&gt;", "&mygt;").replace("&amp;", "&myamp;")
    text = html.unescape(text)
    text = text.replace("&mylt;", "&lt;").replace("&mygt;", "&gt;").replace("&myamp;", "&amp;")

    # Bug in html.unescape ? Why does this module replace a unicode by another ?
    text = text.replace(chr(10216), chr(9001)).replace(chr(10217), chr(9002))
    text = text.replace(chr(10214), chr(12314)).replace(chr(10215), chr(12315))
    text = text.replace(chr(9183), chr(65080))

    return text


def normalize(name):
    if name[0] == "{":
        _, tag = name[1:].split("}")
        return tag
    return name


def get_xml_file_count(folder):
    count = 0
    for root, dirs, _files in os.walk(folder):
        for dir_ in dirs:
            file_ = os.path.join(folder, dir_, dir_ + ".xml")
            num_sep_this = root.count(os.path.sep)
            if num_sep_this < 3:
                if os.path.isfile(file_):
                    count += 1
    return count


def get_xml_from_text(tag, text):
    node = etree.Element(tag)
    node.text = text
    result = etree.tostring(node, encoding="UTF-8").decode("utf-8")

    return result


def remove_namespace(tree):
    for elem in tree.getiterator():
        if not hasattr(elem.tag, "find"):
            continue  # (1)
        i = elem.tag.find("}")
        if i >= 0:
            elem.tag = elem.tag[i + 1 :]
    objectify.deannotate(tree, cleanup_namespaces=True, xsi_nil=True)


def get_normalized_attrib(node, attrib_name):
    attrib_value = None
    if node is not None:
        for attrib in node.attrib:
            name = normalize(attrib)
            if name == attrib_name:
                attrib_value = node.attrib[attrib]

    return attrib_value


def get_xml_from_node(node):
    text = ""
    if node is not None:
        text = etree.tostring(
            node, encoding="unicode", method="xml", xml_declaration=False, with_tail=False
        )
    return text


def get_xml_from_node2(node, with_tail=False):
    tag = normalize(node.tag)

    text = "<" + tag + ">"
    if node.text:
        text += node.text

    for child in node:
        text += get_xml_from_node2(child, True)

    text += "</" + tag + ">"

    if node.tail and with_tail:
        text += node.tail

    return text


# tostring is a useless fonction for 'text': it simply removes the HTML entities !
def get_old_text_from_node(node):
    text = ""
    if node is not None:
        text = etree.tostring(
            node, encoding="unicode", method="text", xml_declaration=False, with_tail=False
        )
    return text


def get_text_from_node(node, **kwargs):
    text = ""

    is_top = kwargs["is_top"] = kwargs["is_top"] if "is_top" in kwargs else True

    if node is not None:
        text += replace_html_entities(node.text) if node.text is not None else ""

        kwargs["is_top"] = False

        for child in node:
            text += get_text_from_node(child, **kwargs)

        if not is_top and node.tail is not None:
            text += replace_html_entities(node.tail)

    return text


def fix_mfenced_in_mathml(text):
    i = 0
    keep_testing = True
    while keep_testing:
        i = text.find("<mfenced", i)
        keep_testing = i > -1
        if i > 0 and text[i - 1] != ">":
            j = i - 1
            while j > 0 and text[j] != ">":
                j -= 1
            mfenced = text[j + 1 : i].strip()
            if 0 < len(mfenced) < 3:
                if len(mfenced) == 1:
                    first = mfenced
                    second = ""
                else:
                    first = mfenced[0]
                    second = mfenced[1]

                left = text[: j + 1]
                right = text[i:]

                if second == "":
                    if mfenced in ("{", "("):
                        open_c = mfenced
                        close_c = ""
                    else:
                        close_c = mfenced
                        open_c = ""
                else:
                    ri = right.find('open=""')
                    rj = right.find('close=""')
                    if ri < rj:
                        open_c = first
                        close_c = second
                    else:
                        open_c = second
                        close_c = first
                right = right.replace('open=""', 'open="' + open_c + '"', 1)
                right = right.replace('close=""', 'close="' + close_c + '"', 1)
                text = left + right
        i += 1

    return text

    # chars = ('∥', '|')
    # for c in chars:
    #     if c + c in math_node_text:
    #         l = math_node_text.split(c + c)
    #         # Bug in lxml. A formula with open="∥" becomes wrong with tostring
    #         # A proper solution would be to rewrite get_xml_from_node and stop using tostring
    #         end_ = l[1].replace('open=""', 'open="' + c + '"', 1).replace('close=""', 'close="' + c + '"', 1)
    #         math_node_text = l[0] + end_


def add_mml_ns(node):
    if node is None:
        return

    tag = normalize(node.tag)
    tag = etree.QName("http://www.w3.org/1998/Math/MathML", tag)
    node.tag = tag

    for child in node:
        add_mml_ns(child)


def get_text_from_original_title_with_mathml(xml, **kwargs):
    # on ne garde que la lang principal
    parser = etree.XMLParser(
        huge_tree=True, recover=True, remove_blank_text=False, remove_comments=True
    )
    etree.register_namespace("mml", "http://www.w3.org/1998/Math/MathML")
    text = xml.replace('xmlns:xlink="http://www.w3.org/1999/xlink"', "")
    tree = etree.fromstring(text.encode("utf-8"), parser=parser)

    get_trans_title = kwargs.get("get_trans_title", False)

    for node in tree:
        tag = normalize(node.tag)
        if get_trans_title and tag == "trans-title-group":
            for child in node:
                tag = normalize(child.tag)
                if tag == "trans-title":
                    return get_text_from_node_with_mathml(child, **kwargs)
        elif not get_trans_title and tag in (
            "title",
            "journal-title",
            "article-title",
            "book-title",
        ):
            return get_text_from_node_with_mathml(node, **kwargs)


def get_text_from_xml_with_mathml(xml, **kwargs):
    parser = etree.XMLParser(
        huge_tree=True, recover=True, remove_blank_text=False, remove_comments=True
    )
    etree.register_namespace("mml", "http://www.w3.org/1998/Math/MathML")
    text = xml.replace('xmlns:xlink="http://www.w3.org/1999/xlink"', "")

    tree = etree.fromstring(text.encode("utf-8"), parser=parser)
    value = get_text_from_node_with_mathml(tree, **kwargs)
    return value


def get_text_from_node_with_mathml(node, **kwargs):
    text = ""

    if node is None:
        return text

    kwargs["is_top"] = kwargs["is_top"] if "is_top" in kwargs else True
    kwargs["with_mathml"] = kwargs["with_mathml"] if "with_mathml" in kwargs else False

    tag = normalize(node.tag)

    if tag == "inline-formula" or tag == "disp-formula":
        remove_namespace(node)

        for child in node:
            tag = normalize(child.tag)
            if tag == "alternatives":
                for alternative in child:
                    tag = normalize(alternative.tag)
                    if tag == "math" and kwargs["with_mathml"]:
                        add_mml_ns(alternative)
                        text = get_xml_from_node(alternative)
                    elif tag == "tex-math" and not kwargs["with_mathml"]:
                        text = get_xml_from_node(alternative)

    else:
        if node.text:
            text += node.text
            text = escape(text)

        kwargs["is_top"] = False

        for child in node:
            child_text = get_text_from_node_with_mathml(child, **kwargs)
            text += child_text

    if node.tail and not kwargs["is_top"]:
        text += node.tail

    return text


def make_links_clickable(href, string):
    if not href:
        href = string

    if href == "":
        return string

    if href[0] == "/" or href.startswith("http"):
        if "<" in href:
            # TODO: Bug in Cedrics. URLs can have formulas (https://aif.centre-mersenne.org/item/AIF_2013__63_1_155_0/ [6])
            href = href.split("<")[0]

            i = string.find("<")
            if i > 0:
                string = string[i:]

    if not string:
        string = href

    if href[0] == "/" or href.startswith("http"):
        if href[0] == "/":
            return f'<a href="{href}">{string}</a>'
        else:
            return f'<a href="{href}" target="_blank">{string}</a>'

    return string


def get_contrib_xml(contrib: ContributorDict, is_ref=False):
    xml = ""
    if not is_ref:
        xml = f'<contrib contrib-type="{contrib["role"]}"'
        if "corresponding" in contrib and contrib["corresponding"]:
            xml += ' corresp="yes"'
        if "deceased_before_publication" in contrib and contrib["deceased_before_publication"]:
            xml += ' deceased="yes"'
        if (
            "equal_contrib" in contrib
            and contrib["equal_contrib"] != ""
            and contrib["equal_contrib"]
        ):
            xml += ' equal-contrib="yes"'
        xml += ">"

    name = ""

    if "prefix" in contrib and contrib["prefix"]:
        name += f'<prefix>{escape(contrib["prefix"])}</prefix>'
    if "last_name" in contrib and contrib["last_name"]:
        name += f'<surname>{escape(contrib["last_name"])}</surname>'
    if "first_name" in contrib and contrib["first_name"]:
        name += f'<given-names>{escape(contrib["first_name"])}</given-names>'
    if "suffix" in contrib and contrib["suffix"]:
        name += f'<suffix>{escape(contrib["suffix"])}</suffix>'

    if name == "":
        if contrib["string_name"]:
            xml += f"<string-name>{contrib['string_name']}</string-name>"
        else:
            # TODO: Bug in Cedrics <nomcomplet> is ignored inside <bauteur> and <bediteur>
            xml += "<name/>"
    else:
        xml += f"<name>{name}</name>"

    if "addresses" in contrib:
        for address in contrib["addresses"]:
            xml += "<address><addr-line>" + escape(address) + "</addr-line></address>"

    if "email" in contrib and contrib["email"]:
        emails = contrib["email"].split("{{{")
        for email in emails:
            xml += "<email>" + escape(email) + "</email>"
    if "orcid" in contrib and contrib["orcid"]:
        xml += '<contrib-id contrib-id-type="orcid">' + escape(contrib["orcid"]) + "</contrib-id>"

    if "idref" in contrib and contrib["idref"]:
        xml += '<contrib-id contrib-id-type="idref">' + escape(contrib["idref"]) + "</contrib-id>"
    if not is_ref:
        xml += "</contrib>"

    return xml


def helper_update_name_params(params, use_initials=False):
    # Extract first/last name if they are empty
    if params["string_name"] and not params["last_name"]:
        array = params["string_name"].split(",")
        if len(array) > 1:
            params["last_name"] = array[0]
            params["first_name"] = array[1]

    if len(params["first_name"]) > 128:
        params["first_name"] = params["first_name"][0:128]
    if len(params["last_name"]) > 128:
        params["last_name"] = params["last_name"][0:128]
    if len(params["string_name"]) > 256:
        params["string_name"] = params["string_name"][0:256]
    if len(params["mid"]) > 256:
        params["mid"] = params["mid"][0:256]


def normalise_span(value):
    # Supprime les spans en trop dans les textes

    i = 0
    while i != -1:
        i = value.find("<span")
        if i > -1:
            j = value.find(">", i)
            if j > -1:
                value = value[0:i] + value[j + 1 :]
    value = value.replace("</span>", "")
    return value


def remove_html(string):
    if not string:
        return ""
    return "".join(fromstring(string).itertext())


def normalize_space(value):
    # Supprime les espaces en trop dans les textes

    # Common answers on the web " ".join(s.split())
    # If does not work if there's a nbsp;
    # Python splits it, xslt ignores it

    result = ""
    init_trim = True
    skips = (" ", "\t", "\n")

    for c in value:
        if c in skips:
            if not init_trim:
                result += c
            init_trim = True
        else:
            result += c
            init_trim = False

    if len(result) > 1 and result[-1] in skips:
        result = result[0:-1]

    return result


def clean_doi(value: str):
    i = value.find("10.")
    if i > 0:
        value = value[i:]
    value = normalize_space(value)

    return value


def int_to_Roman(num):
    val = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1]
    syb = ["m", "cm", "d", "cd", "c", "xc", "l", "xl", "x", "ix", "v", "iv", "i"]
    roman_num = ""
    i = 0
    while num > 0:
        for _ in range(num // val[i]):
            roman_num += syb[i]
            num -= val[i]
        i += 1
    return roman_num


def roman_to_int(s):
    """
    :type s: str
    :rtype: int
    """
    roman = {
        "I": 1,
        "V": 5,
        "X": 10,
        "L": 50,
        "C": 100,
        "D": 500,
        "M": 1000,
        "IV": 4,
        "IX": 9,
        "XL": 40,
        "XC": 90,
        "CD": 400,
        "CM": 900,
    }
    i = 0
    num = 0
    s = s.upper()
    while i < len(s):
        if i + 1 < len(s) and s[i : i + 2] in roman:
            num += roman[s[i : i + 2]]
            i += 2
        else:
            num += roman[s[i]]
            i += 1
    return num


def get_extid_value_from_link_data(link_data: ExtLinkDict):
    """
    Some links have an id to an external database (MR, ZBL, DOI, Numdam).
    Extract the link_type and value

    :param link_data: dict with link data (ref, mimetype, location...)
    :return: (link_type, value)
    """

    # rdoi: recommendation doi, used by PCI
    # preprint: id of the preprint, used by PCI
    referentials = [
        "jfm-item-id",
        "zbl-item-id",
        "mr-item-id",
        "nmid",
        "numdam-id",
        "mathdoc-id",
        "sps-id",
        "dmlid",
        "eudml-item-id",
        "doi",
        "eid",
        "arxiv",
        "tel",
        "hal",
        "theses.fr",
        "rdoi",
        "preprint",
        "pmid",
        "ark",
    ]

    # data['rel'] is the ext-link-type or the pub-id-type
    link_type = link_data["rel"] or ""

    # The value attribute is not required. Use the node's text when href is empty.
    value = link_data["location"]
    if value == "":
        value = link_data["metadata"]
    value = value.strip()

    if link_type == "" and value.find("doi.org") > 0:
        link_type = "doi"
    elif link_type == "" and value.find("arxiv.org") > 0:
        link_type = "arxiv"
    elif link_type == "" and value.find("hal-") > 0:
        link_type = "hal"

    extid_value = (None, None)

    if link_type in referentials:
        if link_type == "numdam-id":
            link_type = "mathdoc-id"

        if link_type == "doi":
            value = clean_doi(value)
        elif link_type == "arxiv":
            if link_data["metadata"] != "":
                value = link_data["metadata"].replace("arXiv:", "")
            else:
                value = link_data["location"]
            value = value.replace("http://arxiv.org/abs/", "").replace(
                "https://arxiv.org/abs/", ""
            )
        else:
            value = link_data["metadata"]

        extid_value = (link_type, value)

    return extid_value


def handle_pages(page_range):
    try:
        fpage, lpage = (int(page) for page in page_range.split("-"))
    except (AttributeError, ValueError):
        # means : page_range = None
        fpage, lpage = None, None
    return fpage, lpage


def split_kwds(text):
    list_ = text.split("$")

    if len(list_) % 2 == 0:
        # Formulas are encapsulated inside $$
        # If the list_ size is odd (number of '$' is odd), do not attempt to split keywords
        return [text]

    kwds = []
    cur_kwd = ""
    for i, item in enumerate(list_):
        if i % 2 == 0:
            items = item.replace(";", ",").split(",")
            if len(items) > 1:
                kwds.append(cur_kwd + items[0])
                kwds.extend(items[1:-1])
                cur_kwd = items[-1]
            else:
                cur_kwd += item
        else:
            cur_kwd += "$" + item + "$"

    if cur_kwd:
        kwds.append(cur_kwd)

    kwds = [kwd.strip() for kwd in kwds]
    return kwds


def get_elsevier_image_extensions():
    return ["tif", "tiff", "gif", "png", "jpg", "jpeg", "jc3", "eps", "jc4"]
