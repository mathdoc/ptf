from ptf.cmds.xml.xml_utils import get_extid_value_from_link_data
from ptf.model_data import ExtLinkDict
from ptf.model_data import RefData


class XmlParserBase:
    extids: list[tuple[str, str]] = []

    def add_extids_from_node_with_link(self, link_data: ExtLinkDict):
        extid_value = get_extid_value_from_link_data(link_data)

        if extid_value not in self.extids and extid_value[0] is not None:
            self.extids.append(extid_value)

        if extid_value[0] == "doi" and self.doi is None:
            self.doi = extid_value[1]

        return extid_value


class RefBase(RefData):
    def from_dict(self, my_dict):
        for key in my_dict:
            setattr(self, key, my_dict[key])

    def get_authors(self):
        contribs = [contrib for contrib in self.contributors if contrib["role"] == "author"]
        return contribs

    def get_editors(self):
        contribs = [contrib for contrib in self.contributors if contrib["role"] == "editor"]
        return contribs
