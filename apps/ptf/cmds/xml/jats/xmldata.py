import os
import re
import unicodedata

from lxml import etree

from django.conf import settings
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from ptf.cmds.xml.xml_utils import escape
from ptf.cmds.xml.xml_utils import normalize
from ptf.cmds.xml.xml_utils import remove_namespace


def get_attribute_value(node, fullname, basename=None, name=None):
    value = ""
    try:
        if basename == name:
            value = node.attrib[fullname]
    except KeyError:
        pass

    return value


def get_lang_attrib(node):
    lang = "und"
    if node is not None:
        for attrib in node.attrib:
            name = normalize(attrib)
            if name == "lang":
                lang = node.attrib[attrib]

    return lang


def get_href_attrib(node):
    href = None
    if node is not None:
        for attrib in node.attrib:
            name = normalize(attrib)
            if name == "href":
                href = node.attrib[attrib]

    return href


def innerxml(node):
    if node.text:
        parts = [escape(node.text)] + [
            etree.tostring(c, encoding="unicode") for c in node.getchildren()
        ]
    else:
        parts = [etree.tostring(c, encoding="unicode") for c in node.getchildren()]
    return "".join(parts).strip().encode("utf-8")


def get_node_text(node):
    text = ""
    if node is not None:
        text = etree.tostring(
            node, encoding="unicode", method="text", xml_declaration=False, with_tail=False
        )
    return text


##########################################################################
#
# get_mixed_content: recreate the xml string from a node
#
# Used to export data (OAI)
#
##########################################################################


def get_mixed_content(node):
    text = ""
    if node is not None:
        text = etree.tostring(
            node, encoding="unicode", method="xml", xml_declaration=False, with_tail=False
        )
    return text


##########################################################################
#
# get_tex: get the tex version of a node with mixed-content
#
# Strip the mathml alternative of formula
#
# Used to prepare the HTML pages. A Django template can simply display title_tex
#
##########################################################################
def get_tex(node, is_top=True, is_citation=False):
    text = ""
    is_citation_author = False
    is_citation_title = False

    if node is not None:
        normalized_tag = normalize(node.tag)

        if normalized_tag == "element-citation":
            text += get_element_citation_str(node, is_top)
        elif normalized_tag != "math":
            if normalized_tag == "mixed-citation":
                is_citation = True
            elif is_citation and normalized_tag == "string-name":
                is_citation_author = True
            elif is_citation and (
                normalized_tag == "article-title"
                or normalized_tag == "chapter-title"
                or normalized_tag == "italic"
            ):
                is_citation_title = True

            if node.text:
                text += node.text

            for child in node:
                text += get_tex(child, False)

            if is_citation_title:
                text = '<span class="citation-title">' + text + "</span>"
            elif is_citation_author:
                text = '<span class="citation-author">' + text.title() + "</span>"

            if node.tail and not is_top:
                text += node.tail

    return text


def make_links_clickable(href, string):
    if re.match(r"http+", href):
        return f'<a href="{href}" target="_blank">{string}</a>'
    if href.startswith("/"):
        return f'<a href="{href}">{string}</a>'
    return string


##########################################################################
#
# get_html_mixed_content_with_figures: get the mathml version of a node with mixed-content
#
# Strip the tex alternative of formula, add the tex version to the tooltip (HTML <title> tag)
#
# Used to prepare the HTML pages. A Django template can simply display the_html
#
# TODO: Use a dict to pass the params
#
##########################################################################
def get_html_mixed_content_with_figures(
    node,
    is_top=True,
    is_citation=False,
    is_comment=False,
    is_figure=False,
    prefix="",
    suffix="",
    sec_level=2,
    label_title="",
    figures=None,
    base_url="",
):
    text = ""
    is_citation_author = False
    is_citation_title = False
    is_citation_volume = False

    # specific case for element-citation as the order of the children
    # might not be the order of display
    if node is not None:
        normalized_tag = normalize(node.tag)

        if normalized_tag == "element-citation":
            text = get_element_citation_str(node, is_top)
        # pub-id are ignored by default are they are treated separately
        # Inside citations or comments, ext-links are converted in html links
        elif is_comment or (normalized_tag != "pub-id" and normalized_tag != "object-id"):
            if normalized_tag == "mixed-citation" or normalized_tag == "toc":
                is_citation = True
            # elif normalized_tag == "toc":
            #     is_toc = True
            elif normalized_tag == "comment":
                is_comment = True
            elif is_citation and normalized_tag == "string-name":
                is_citation_author = True
            elif is_citation and (
                normalized_tag == "article-title"
                or normalized_tag == "chapter-title"
                or normalized_tag == "italic"
            ):
                is_citation_title = True
            elif is_citation and normalized_tag == "volume":
                is_citation_volume = True

            text += prefix

            if is_citation and normalized_tag == "ext-link":
                type = node.get("ext-link-type")
                if type is None:
                    href = get_href_attrib(node)
                    if not href:
                        href = node.text
                    if "www.numdam.org" not in href:
                        href = make_links_clickable(href, node.text)
                        text += href
            elif is_citation and normalized_tag == "uri":
                href = get_href_attrib(node)
                if not href:
                    href = node.text
                href = make_links_clickable(href, node.text)
                text += href
            # elif normalized_tag == "nav-pointer":
            #     rid = get_attribute_value(node,'rid')
            #     if rid is not '':
            #         href = '/item/%s' % rid
            #         #href = make_links_clickable(href, node.text) non car make links clickable cree un lien absolu avec target _blank
            #         link = '<a href="%s">%s</a>' % (href, node.text)
            #     else:
            #         link = node.text
            #     text += link
            elif is_comment and node.text:
                match = re.match(r"[\n ]+", node.text)
                if not match:
                    comment = make_links_clickable(node.text, node.text)
                    text += comment
            elif node.text:
                text += node.text

            label = ""
            if (
                normalized_tag == "sec"
                or normalized_tag == "statement"
                or normalized_tag == "fig"
                or normalized_tag == "list-item"
                or normalized_tag == "table-wrap"
            ):
                child = node.find("label")
                if child is not None:
                    label += child.text
                    node.remove(child)
                child = node.find("title")
                if child is not None:
                    if label:
                        label += " "
                    label += child.text
                    node.remove(child)

            if normalized_tag == "sec" or normalized_tag == "statement":
                text = "<h" + str(sec_level) + ">" + label + "</h" + str(sec_level) + ">"
                sec_level += 1

            if normalized_tag == "table-wrap":
                text = "<strong>" + label + "</strong>"

            if normalized_tag == "fig":
                is_figure = True
                child = node.find("caption")
                if child is not None:
                    child_text, figures = get_html_mixed_content_with_figures(
                        child,
                        False,
                        is_citation,
                        is_comment,
                        is_figure,
                        "",
                        "",
                        sec_level,
                        "",
                        figures,
                        base_url,
                    )
                    label += " : " + child_text
                    node.remove(child)

            if normalized_tag == "list-item":
                label_title = label

            if normalized_tag == "p":
                if label_title:
                    text = label_title + " " + text
                    label_title = ""

            if normalized_tag == "inline-formula" or normalized_tag == "disp-formula":
                for child in node:
                    if child.tag == "alternatives":
                        math_text = ""
                        tex_text = ""

                        for great_child in child:
                            normalized_tag = normalize(great_child.tag)
                            if normalized_tag == "math":
                                math_text = get_mixed_content(great_child)
                            else:
                                tex_text = get_tex(great_child)

                        text += '<span title="' + tex_text + '">' + math_text + "</span>"

            else:
                for child in node:
                    child_text, figures = get_html_mixed_content_with_figures(
                        child,
                        False,
                        is_citation,
                        is_comment,
                        is_figure,
                        "",
                        "",
                        sec_level,
                        label_title,
                        figures,
                        base_url,
                    )
                    text += child_text

            if is_citation_title:
                text = '<span class="citation-document-title">' + text + "</span>"
            elif is_citation_author:
                text = '<span class="citation-author">' + text.title() + "</span>"
            elif is_citation_volume:
                text = '<span class="citation-volume">' + text + "</span>"
            elif normalized_tag == "list":
                type = node.get("list-type")
                if type is None or type == "bullet":
                    text = "<ul>" + text + "</ul>"
                else:
                    if type == "order":
                        text = '<ol type="1">' + text + "</ol>"
                    elif type == "alpha-lower":
                        text = '<ol type="a">' + text + "</ol>"
                    elif type == "alpha-upper":
                        text = '<ol type="A">' + text + "</ol>"
                    elif type == "roman-lower":
                        text = '<ol type="i">' + text + "</ol>"
                    elif type == "roman-upper":
                        text = '<ol type="I">' + text + "</ol>"
                    else:
                        text = (
                            '<ul class="no-bullet" style="list-style-type:none;">' + text + "</ul>"
                        )
            elif normalized_tag == "list-item":
                text = "<li>" + text + "</li>"
            elif normalized_tag == "strong" or normalized_tag == "bold":
                text = "<strong>" + text + "</strong>"
            elif normalized_tag == "italic":
                text = '<span class="italique">' + text + "</span>"
            elif normalized_tag == "p":
                type = node.get("specific-use")
                if type:
                    text = '<p class="' + type + '">' + text + "</p>"
                else:
                    text = "<p>" + text + "</p>"
            elif normalized_tag == "caption" and not is_figure:
                text = '<div class="caption">' + text + "</div>"
            elif normalized_tag == "sec" or normalized_tag == "statement":
                text = "<section>" + text + "</section>"
            elif normalized_tag == "fig":
                id = node.get("id")
                if id:
                    tag = '<figure id="' + id + '">'
                else:
                    tag = "<figure>"
                text = tag + text
                if label:
                    text += "<figcaption>" + label + "</figcaption>"
                text += "</figure>"
            elif normalized_tag == "sub" or normalized_tag == "sup":
                text = "<" + normalized_tag + ">" + text + "</" + normalized_tag + ">"
            elif normalized_tag == "xref":
                id = node.get("rid")
                if id:
                    text = '<a href="#' + id + '">' + text + "</a>"
            elif normalized_tag == "graphic" and is_figure:
                href = ""
                for attrib in node.attrib:
                    name = normalize(attrib)
                    href = node.attrib[attrib] if name == "href" else ""

                if len(href) > 0:
                    basename = os.path.basename(href)
                    ext = basename.split(".")[-1]
                    if ext == "png":
                        mimetype = "image/png"
                    else:
                        mimetype = "image/jpeg"

                    location = "src/tex/figures/" + basename
                    v = {
                        "rel": "image",
                        "mimetype": mimetype,
                        "location": location,
                        "base": None,
                        "text": node.text if node.text is not None else "",
                    }

                    if ext == "png":
                        location = os.path.join(base_url, "png", location)
                    else:
                        location = os.path.join(base_url, "jpg", location)
                    text = '<img src="' + location + '" class="article-body-img" />'

                    figures.append(v)
            elif (
                normalized_tag == "table"
                or normalized_tag == "th"
                or normalized_tag == "thead"
                or normalized_tag == "tr"
                or normalized_tag == "td"
            ):
                tag = "<" + normalized_tag
                if "rowspan" in node.attrib:
                    tag += ' rowspan="' + node.attrib["rowspan"] + '"'
                text = tag + ">" + text + "</" + normalized_tag + ">"
            elif normalized_tag == "table-wrap":
                tag = '<div class="table-wrap"'
                id = node.get("id")
                if id:
                    tag += ' id="' + id + '"'

                text = tag + ">" + text + "</div>"

            if node.tail and not is_top:
                # match = None
                # if is_citation:
                #     match = re.match(r'[\n ]+', node.tail)
                # if not match:
                text += node.tail

            text += suffix

    return text, figures


def get_html_mixed_content(
    node,
    is_top=True,
    is_citation=False,
    is_comment=False,
    prefix="",
    suffix="",
    sec_level=2,
    label="",
):
    text, _ = get_html_mixed_content_with_figures(
        node, is_top, is_citation, is_comment, False, prefix, suffix, sec_level, label, None
    )
    return text


##########################################################################
#
# get_element_citation_str: get the mixed content of an element-citation node
#
# An element-citation node is specific as the order of its children might not be
# the correct order for display
#
# Used to prepare the HTML pages. A Django template can simply display title_html
#
##########################################################################
def get_element_citation_str(node, is_top=False, is_html=True):
    text = document_title = ""
    REF_JEP_STYLE = getattr(settings, "REF_JEP_STYLE", False)

    # xbibitem = BibItem(node.getparent())
    # ids = xbibitem.extids

    if node is not None:
        type = node.get("publication-type")

        name_str = get_author_str(node)
        text += name_str

        if is_html:
            prefix = " "
            suffix = ""
            if REF_JEP_STYLE:
                prefix = " - &ldquo;"
                suffix = "&rdquo;"
            document_title += get_html_mixed_content(
                node.find("article-title"), True, True, False, prefix, suffix
            )

            if REF_JEP_STYLE and type == "incollection":
                document_title += get_html_mixed_content(
                    node.find("chapter-title"), True, True, False, prefix, suffix
                )
            else:
                document_title += get_html_mixed_content(
                    node.find("chapter-title"), True, True, False, " "
                )
        else:
            document_title += " " + get_tex(node.find("article-title"))
            document_title += " " + get_tex(node.find("chapter-title"))

        text += document_title

        prefix = ""
        suffix = "</span>"

        if document_title:
            if REF_JEP_STYLE and type == "incollection":
                prefix += ', in <span class="citation-publication-title">'
            else:
                prefix += ', <span class="citation-publication-title">'
        else:
            if name_str:
                prefix = " "
                if REF_JEP_STYLE:
                    prefix = " - "
                    if type in ["unpublished", "misc"]:
                        prefix += "&ldquo;"
                        suffix += "&rdquo;"
            prefix += '<span class="citation-document-title">'

        source = get_html_mixed_content(node.find("source"), True, True, False, prefix, suffix)
        if REF_JEP_STYLE and type == "book":
            source = f"<i>{source}</i>"
        if type in ["book", "incollection"]:
            editor = get_editor_str(node.find("person-group"))
            source += editor
        else:
            editor = ""
        text += source

        if document_title:
            if REF_JEP_STYLE and type == "incollection":
                prefix = ", "
            else:
                prefix = " ("
                suffix = ")"
        else:
            if REF_JEP_STYLE and type == "book":
                prefix = ', <span class="citation-publication-title-book">'
            else:
                prefix = ', <span class="citation-publication-title">'
            suffix = "</span>"

        serie = get_html_mixed_content(node.find("series"), True, True, False, prefix, suffix)
        text += serie

        if REF_JEP_STYLE:
            if type in ["incollection", "book"]:
                prefix = ", vol. "
            else:
                prefix = " "
        else:
            if document_title:
                prefix = " " if serie else ", "
            else:
                prefix = ", " if serie else " "
            prefix += str(_("Tome")) + " "

        text += get_html_mixed_content(node.find("volume"), True, True, False, prefix)
        if type in ["incollection", "book"]:
            text = text.replace("citation-volume", "citation-volume-incollection")
            text += get_html_mixed_content(node.find("publisher-name"), True, True, False, ", ")
            text += get_html_mixed_content(node.find("publisher-loc"), True, True, False, ", ")
            text += get_html_mixed_content(node.find("institution"), True, True, False, ", ")
            prefix = ", "
            suffix = ""
        elif type == "misc":
            prefix = ", "
            suffix = ""
        else:
            prefix = " ("
            suffix = ")"
        text += get_html_mixed_content(node.find("year"), True, True, False, prefix, suffix)
        text += get_html_mixed_content(node.find("issue"), True, True, False, " no. ")

        for child in node.findall("pub-id"):
            if child.get("pub-id-type") == "eid":
                text += ", " + child.text

        for child in node.findall("ext-link"):
            if child.get("ext-link-type") == "eid":
                if REF_JEP_STYLE:
                    text += ", article ID " + child.text
                else:
                    text += ", " + child.text

        if not (REF_JEP_STYLE and type == "book"):
            text += get_pages_str(node)

        for child in node.findall("ext-link"):
            type = child.get("ext-link-type")
            if type is None:
                href = get_href_attrib(child)
                if not href:
                    href = child.text
                # bibitem with ext-links pointing to numdam.org have a numdam-id
                # ext-links to doi.org are transformed in an extid
                # We can ignore both cases
                if "www.numdam.org" not in href and "doi.org" not in href and not REF_JEP_STYLE:
                    href = make_links_clickable(href, child.text)
                    text += " " + href

        if REF_JEP_STYLE:
            text += get_html_mixed_content(node.find("comment"), True, True, True, ", ")
        else:
            text += get_html_mixed_content(node.find("comment"), True, True, True, " (", ")")

        # if type is None or type == 'article':
        # elif type == 'book' or type == 'proceedings':
        # elif type == 'incollection':
        # elif type == 'conference':
        # elif type == 'unpublished':
        # elif type == "booklet":
        # elif type == 'inbook' or type == 'inproceedings':
        # elif type == "misc":
        # elif type == 'phdthesis' or type == 'masterthesis':
        # elif type == 'techreport' or type == 'manual':

        # Fallback in case the publication-type is unknown
        # else:
        #     if node.text:
        #         text += node.text
        #
        #     for child in node:
        #         text += get_html_mixed_content(child, False, True)
        #
        # if node.tail and not is_top:
        #     text += node.tail

    return text


def get_name_str(node):
    text = ""
    REF_JEP_STYLE = getattr(settings, "REF_JEP_STYLE", False)

    if node is not None:
        names = node.findall("name")
        i = 1
        for name_node in names:
            first_name = last_name = prefix = suffix = string_name = ""

            for child in name_node:
                if child.tag == "given-names":
                    if REF_JEP_STYLE:
                        first_name += child.get("initials", "")
                    else:
                        if child.text is None:
                            child.text = ""
                        first_name += child.text
                if child.tag == "surname":
                    last_name += child.text
                if child.tag == "prefix":
                    prefix += child.text
                if child.tag == "suffix":
                    suffix += child.text

            if prefix:
                string_name = prefix + " "

            if getattr(settings, "DISPLAY_FIRST_NAME_FIRST", False):
                if first_name:
                    string_name += first_name + " "
                string_name += last_name
            else:
                string_name += last_name

                if first_name:
                    string_name += ", " + first_name

            if suffix:
                string_name += " " + suffix

            if text:
                if i == len(names) and REF_JEP_STYLE:
                    text += " & "
                elif REF_JEP_STYLE:
                    text += ", "
                else:
                    text += "; "

            text += string_name
            i += 1

        names = node.findall("string-name")
        i = 1
        for name_node in names:
            string_name = get_tex(name_node)

            if text:
                if i == len(names) and REF_JEP_STYLE:
                    text += " & "
                elif REF_JEP_STYLE:
                    text += ", "
                else:
                    text += "; "

            text += string_name
            i += 1
    return text


def get_author_str(node):
    authors = get_name_str(node)
    return f'<span class="citation-author">{authors}</span>'


def get_editor_str(node):
    editors = get_name_str(node)
    if not editors:
        return ""
    # Here, we replace '&' (used in JEP) by ';' and then split in order to
    # find if there are multiple editors
    suffix = "eds." if len(editors.replace("&", ";").split(";")) > 1 else "ed."
    return f" ({editors}, {suffix})"


def get_pages_str(node):
    text = ""
    REF_JEP_STYLE = getattr(settings, "REF_JEP_STYLE", False)
    child = node.find("page-count")
    if child is not None:
        text += get_html_mixed_content(child, True, True, False, ", ", " pages")

    if not text:
        child = node.find("size")
        if child is not None:
            text += get_html_mixed_content(child, True, True, False, ", ", " pages")

    if not text:
        first_page_child = node.find("fpage")
        if first_page_child is not None:
            fpage_text = get_html_mixed_content(first_page_child, True, True, False)
            lpage_text = ""
            fpage_int = lpage_int = 0
            try:
                fpage_int = int(fpage_text)
            except BaseException:
                pass

            last_page_child = node.find("lpage")
            if last_page_child is not None:
                lpage_text = get_html_mixed_content(last_page_child, True, True, False)
                try:
                    lpage_int = int(lpage_text)
                except BaseException:
                    pass

            if lpage_int > 0 and lpage_int - fpage_int > 1 and not REF_JEP_STYLE:
                text += ", pp. "
            else:
                text += ", p. "
            text += fpage_text
            if lpage_text:
                text += "-" + lpage_text

    if not text:
        child = node.find("page-range")
        if child is not None:
            prefix = ", pp. "
            suffix = ""
            if REF_JEP_STYLE:
                prefix = ", p. "

            text += get_html_mixed_content(child, True, True, False, prefix, suffix)

    return text


##########################################################################
#
# Parse a name node ("name", "string-name", or "name-alternative) and find the fields related to a person name:
#   first_name <given-names>
#   last_name <surname>
#   prefix <prefix>
#   suffix <suffix>
#   string_name <string_name> or built with "<prefix> <last_name>, <first_name>, <suffix>"
#   reference_name <string_name specific-use="index"> or string_name
#       Used in Solr for facets (regroup multiple orthographies under the same person)
#
# Note: parse_name and get_name_str can not be merged...today
#       string-names in mixed-citation mix structured data (ex: "surname") and non structured content.
#       Ex: <surname>ROBERTSON</surname>, <given-names>D. H.</given-names></string-name>
#       Notice the ", " inside.
#       get_name_str is used for web pages and need to preserve everything (the ', " in particular)
#       parse_name is used to export bibtex: only structured data are preserved.
#       TODO: discuss this workflow. Why add or preserve the mix content of a string-name ?
#
# TODO: merge parse_name and parse_contrib
#    1. <contrib> can have multiple entries (ex: <name> then <string-name specific-use="index") for 1 single person,
#       whereas <mixed-citation> or <element-citation> use 1 entry per person.
#    2. string-name is a contrib is a simple text, string-name in mixed-citation is a tree
#
##########################################################################


def get_name_params(first_name, last_name, prefix, suffix, string_name="", reference_name=""):
    if string_name and not reference_name:
        reference_name = string_name

    if last_name and not string_name:
        if prefix:
            string_name = prefix + " "

        string_name += last_name

        if first_name:
            string_name += ", " + first_name

        if suffix:
            string_name += " " + suffix

    elif string_name and not last_name:
        array = string_name.split(",")
        if len(array) > 1:
            last_name = array[0]
            first_name = array[1]

    if not reference_name and last_name:
        if getattr(settings, "DISPLAY_FIRST_NAME_FIRST", False):
            reference_name = ""
            if first_name:
                reference_name = first_name + " "
            reference_name += last_name
        else:
            reference_name = last_name
            if first_name:
                reference_name += ", " + first_name

    params = {
        "first_name": first_name,
        "last_name": last_name,
        "prefix": prefix,
        "suffix": suffix,
        "string_name": string_name,
        "reference_name": reference_name,
    }

    return params


def parse_name(node):
    first_name = last_name = prefix = suffix = string_name = reference_name = ""

    if node is not None:
        if node.tag == "name":
            for child in node:
                if child.tag == "given-names":
                    first_name += child.text
                if child.tag == "surname":
                    last_name += child.text
                if child.tag == "prefix":
                    prefix += child.text
                if child.tag == "suffix":
                    suffix += child.text
        if node.tag == "string-name":
            for child in node:
                if child.text:
                    if child.tag == "given-names":
                        first_name += child.text
                    if child.tag == "surname":
                        last_name += child.text
                    if child.tag == "prefix":
                        prefix += child.text
                    if child.tag == "suffix":
                        suffix += child.text

            if not first_name and not last_name:
                string_name = node.text

        if node.tag == "name-alternatives":
            for child in node:
                if child.tag == "string-name":
                    if child.get("specific-use") == "index":
                        reference_name += child.text

    params = get_name_params(first_name, last_name, prefix, suffix, string_name, reference_name)

    return params


##########################################################################
#
# Parse a Contrib node and find the fields related to a person name:
#   first_name <given-names>
#   last_name <surname>
#   prefix <prefix>
#   suffix <suffix>
#   string_name <string_name> or built with "<prefix> <last_name>, <first_name>, <suffix>"
#   reference_name <string_name specific-use="index"> or string_name
#       Used in Solr for facets (regroup multiple orthographies under the same person)
#
##########################################################################


def parse_contrib(node):
    first_name = last_name = prefix = suffix = string_name = reference_name = ""

    if node is not None:
        for child in node:
            if child.tag == "name":
                for great_child in child:
                    if great_child.text is not None:
                        if great_child.tag == "given-names":
                            first_name += great_child.text
                        if great_child.tag == "surname":
                            last_name += great_child.text
                        if great_child.tag == "prefix":
                            prefix += great_child.text
                        if great_child.tag == "suffix":
                            suffix += great_child.text
            if child.tag == "string-name":
                if child.text is not None:
                    string_name += child.text
            if child.tag == "name-alternatives":
                for great_child in child:
                    if great_child.text is not None:
                        if great_child.tag == "string-name":
                            if great_child.get("specific-use") == "index":
                                reference_name += great_child.text

        if string_name and not reference_name:
            reference_name = string_name

        if last_name and not string_name:
            if prefix:
                string_name = prefix + " "

            string_name += last_name

            if first_name:
                string_name += ", " + first_name

            if suffix:
                string_name += " " + suffix

        elif string_name and not last_name:
            array = string_name.split(",")
            if len(array) > 1:
                last_name = array[0]
                first_name = array[1]

        if not reference_name and last_name:
            if getattr(settings, "DISPLAY_FIRST_NAME_FIRST", False):
                reference_name = ""
                if first_name:
                    reference_name = first_name + " "
                reference_name += last_name
            else:
                reference_name = last_name
                if first_name:
                    reference_name += ", " + first_name

    params = {
        "first_name": first_name,
        "last_name": last_name,
        "prefix": prefix,
        "suffix": suffix,
        "string_name": string_name,
        "reference_name": reference_name,
    }

    return params


def make_int(value):
    v = value.split("-")[0]
    try:
        v = int(v)
    except BaseException:
        v = [x for x in v if x.isdigit()]
        v = int(v)
    else:
        pass
    return v


def uni2ascii(s):
    s = unicodedata.normalize("NFKD", str(s)).encode("ascii", "ignore")
    return s


sid_type = None
pid_type = None


def set_sid_type(id_type):
    global sid_type
    sid_type = id_type


def set_pid_type(id_type):
    global pid_type
    pid_type = id_type


class XmlData:
    ids_xpath = None
    id_type_attr = "pub-id-type"

    extids_xpath = None
    extid_type_attr = None
    title_group_elt_path = None
    title_path = None
    trans_title_group_elt_path = None
    trans_title_path = None
    alternate_title_path = None
    alternate_title_group_elt_path = None
    meta_root_xpath = ""
    custom_meta_path = "custom-meta-group"
    counts_path = "counts"
    remove_links = False

    def __init__(self, tree):
        self.tree = tree
        if self.meta_root_xpath:
            self.meta_root = tree.find(self.meta_root_xpath)
        else:
            self.meta_root = None

    def __getattr__(self, name):
        mname = "get_" + name if "self" not in name else name
        getter = getattr(self, mname)
        obj = getter()
        setattr(self, name, obj)
        return obj

    def get_doi(self):
        return None

    def xpath(self, xpath):
        return self.tree.xpath(xpath)

    def xget_subtree(self, xpath):
        subtree = self.tree.xpath(xpath)
        if subtree:
            return subtree[0]
        return None

    def xget_subtrees(self, xpath):
        return self.tree.xpath(xpath)

    def get_subtree(self, path):
        return self.tree.find(path)

    def get_subtrees(self, path):
        return self.tree.findall(path)

    def get_node_text(self, path, return_none=""):
        node = self.tree.find(path)
        if node is None:
            return return_none
        if node.text is None:
            return return_none
        xml_text = etree.tostring(
            node, encoding="unicode", method="text", xml_declaration=False, with_tail=False
        )
        return xml_text

    def get_nodes_text(self, path):
        return [x.text for x in self.tree.findall(path)]

    def get_ascii_text(self, path, return_none=""):
        return uni2ascii(self.get_node_text(path, return_none=return_none))

    def xget_node_text(self, xpath, return_none=None):
        try:
            return self.tree.xpath(xpath)[0].text
        except BaseException:
            return return_none

    def xget_ascii_text(self, xpath, return_none=""):
        return uni2ascii(self.xget_node_text(xpath, return_none=return_none))

    def tostring(self):
        self.prune()
        return etree.tostring(self.tree, encoding="utf-8", xml_declaration=False)

    __str__ = tostring

    def prune(self):
        pass

    def get_ids(self):
        if self.ids_xpath is not None:
            nodes = self.xget_subtrees(self.ids_xpath)
            return [(x.get(self.id_type_attr), x.text) for x in nodes if x.text is not None]
        return []

    def get_mathdoc_id(self):
        if self.mathdoc_id_xpath is not None:
            try:
                node = self.xget_subtrees(self.mathdoc_id_xpath)[0]
            except IndexError:
                return None
            else:
                return node.text
        return None

    def get_title_xml(self):
        title_xml = ""
        node = self.tree.find(self.title_group_elt_path)
        if node is None and self.alternate_title_group_elt_path:
            node = self.tree.find(self.alternate_title_group_elt_path)
        if node is not None:
            title_xml = get_mixed_content(node)
        return title_xml

    def inner_get_title_html(self, path, alternate_path=None):
        title_html = ""
        node = self.tree.find(path)
        if node is None and alternate_path:
            node = self.tree.find(alternate_path)
        if node is not None:
            title_html = get_html_mixed_content(node)
        return title_html

    def get_title_html(self):
        return self.inner_get_title_html(self.title_path, self.alternate_title_path)

    def get_trans_title_html(self):
        return self.inner_get_title_html(self.trans_title_path)

    def inner_get_title_tex(self, path, alternate_path=None):
        title_tex = ""
        node = self.tree.find(path)
        if node is None and alternate_path:
            node = self.tree.find(alternate_path)
        if node is not None:
            title_tex = get_tex(node)
        return title_tex

    def get_title_tex(self):
        return self.inner_get_title_tex(self.title_path, self.alternate_title_path)

    def get_trans_title_tex(self):
        return self.inner_get_title_tex(self.trans_title_path)

    def get_lang(self):
        tree = self.tree

        lang = get_lang_attrib(tree)

        if lang == "und":
            parent = tree.getparent()

            grand_parent = parent
            while grand_parent is not None:
                parent = grand_parent
                grand_parent = parent.getparent()

            lang = get_lang_attrib(parent)

        return lang

    def get_trans_lang(self):
        lang = "und"
        node = self.tree.find(self.trans_title_group_elt_path)
        lang = get_lang_attrib(node)

        return lang

    def get_extids(self):
        if self.extids_xpath is not None:
            nodes = self.xget_subtrees(self.extids_xpath)
            links = []
            for n in nodes:
                id_type = n.get(self.extid_type_attr)
                value = n.text.strip()
                if id_type is None and value.find("doi.org/") > 0:
                    id_type = "doi"
                if id_type in (
                    "mr-item-id",
                    "zbl-item-id",
                    "sps-id",
                    "numdam-id",
                    "mathdoc-id",
                    "jfm-item-id",
                    "eudml-item-id",
                    "doi",
                    "eid",
                ):
                    if id_type == "numdam-id":
                        id_type = "mathdoc-id"
                    if id_type == "doi":
                        if value.find("doi.org") > 0:
                            value = value.replace("http://dx.doi.org/", "")
                            value = value.replace("https://doi.org/", "")
                            value = value.replace("doi:", "")
                    links.append((id_type, value))
                    if self.remove_links:
                        n.getparent().remove(n)
            return links
        return []

    def get_xml(self, path, return_none=""):
        node = self.get_subtree(path)
        if node is not None:
            return etree.tostring(node, encoding="utf-8", xml_declaration=False)
        return return_none

    def get_inner_xml(self, path, return_none=""):
        node = self.get_subtree(path)
        if node is not None:
            return innerxml(node)
        return return_none

    def xget_xml(self, path):
        node = self.xget_subtree(path)
        if node is not None:
            return etree.tostring(node, encoding="utf-8", xml_declaration=False)
        return ""

    def get_catxml(self, path):
        nodes = self.get_subtrees(path)
        text = []
        for node in nodes:
            text.append(etree.tostring(node))
        return "".join(text)

    def get_streams(self):
        if self.meta_root is not None:
            self_uris = self.meta_root.findall("self-uri")
        else:
            self_uris = self.tree.findall("self-uri")
        vv = []
        for node in self_uris:
            href = base = type = ""
            for attrib in node.attrib:
                name = normalize(attrib)

                href = node.attrib[attrib] if name == "href" else href
                base = node.attrib[attrib] if name == "base" else base
                type = node.attrib[attrib] if name == "content-type" else type

            v = {
                "rel": "full-text",
                "mimetype": type or "text/html",
                "location": href,
                "base": base,
                "text": node.text if node.text else "Link",
            }

            vv.append(v)
        return vv

    def get_related_objects(self):
        related = []
        if self.meta_root is not None:
            nodes = self.meta_root.findall("related-object")
        else:
            nodes = self.tree.findall("related-object")
        for node in nodes:
            rel = href = base = type = ""
            for attrib in node.attrib:
                name = normalize(attrib)

                rel = node.attrib[attrib] if name == "link-type" else rel
                href = node.attrib[attrib] if name == "href" else href
                base = node.attrib[attrib] if name == "base" else base
                type = node.attrib[attrib] if name == "content-type" else type

            text = innerxml(node)
            v = {"rel": rel, "mimetype": type, "location": href, "base": base, "metadata": text}
            related.append(v)
        return related

    def get_supplementary_materials(self):
        materials = []
        if self.meta_root is not None:
            nodes = self.meta_root.findall("supplementary-material")
        else:
            nodes = self.tree.findall("supplementary-material")
        for node in nodes:
            try:
                location = node.attrib["href"]
            except KeyError:
                location = node.attrib["id"]
            material = {
                "rel": node.attrib.get("content-type"),
                "mimetype": node.attrib.get("mimetype"),
                "location": location,
                "base": "",
                "metadata": "",
                "caption": node.xpath("caption/text()")[0],
            }
            materials.append(material)
        return materials

    def get_metadataparts(self):
        return []

    def get_custom_meta(self):
        cm = {}
        if self.custom_meta_path:
            node = self.tree.find(self.custom_meta_path)
            if node is not None:
                for child in node:
                    key = child[0].text
                    value = child[1].text
                    cm[key] = value
        return cm

    def get_wall(self):
        try:
            wall = self.custom_meta["wall"]
        except KeyError:
            return 0
        return int(wall)

    def get_pid(self):
        # try:
        #     name = self.custom_meta['provider']
        # except KeyError:
        #     return None
        # provider_id_type = name  + '-id'
        for id_type, id_value in self.ids:
            if id_type == pid_type or (
                (id_type == "numdam-id" or id_type == "mathdoc-id")
                and (pid_type == "numdam-id" or pid_type == "mathdoc-id")
            ):
                return id_value

    def get_provider(self):
        return self.custom_meta.get("provider", None)

    def get_sid(self):
        for id_type, id_value in self.ids:
            if id_type == sid_type:
                return id_value
        return None

    def get_counts(self):
        counts = []
        if self.counts_path:
            node = self.tree.find(self.counts_path)
            if node is not None:
                page_count = node.find("page-count")
                if page_count is None:
                    page_count = node.find("book-page-count")
                count = page_count.get("count")
                if not count:
                    count = get_node_text(node)
                counts.append(("page-count", count))
        return counts

    def get_ext_links(self):
        referentials = [
            "jfm-item-id",
            "zbl-item-id",
            "mr-item-id",
            "nmid",
            "numdam-id",
            "mathdoc-id",
            "sps-id",
            "dmlid",
            "eudml-item-id",
        ]
        result = []
        if self.meta_root is not None:
            nodes = self.meta_root.findall("ext-link")
        else:
            nodes = self.tree.findall("ext-link")
        for node in nodes:
            rel = href = base = ""
            for attrib in node.attrib:
                name = normalize(attrib)

                rel = node.attrib[attrib] if name == "ext-link-type" else rel
                href = node.attrib[attrib] if name == "href" else href
                base = node.attrib[attrib] if name == "base" else base

            if rel in referentials:
                continue

            text = innerxml(node)
            v = {"rel": rel, "mimetype": "", "location": href, "base": base, "metadata": text}
            result.append(v)
        return result

    def get_last_modified_iso_8601_date_str(self):
        if self.last_modified_path:
            node = self.tree.find(self.last_modified_path)
            if node is not None:
                last_modified_iso_8601_date_str = node.attrib["iso-8601-date"]
                return last_modified_iso_8601_date_str
            # on traite le cas où le container arrive via ptf-tools et donc la date de
            # dernière modification est la date d'import
            return timezone.now().isoformat()

    def get_date_published_iso_8601_date_str(self):
        date_str = None
        if self.published_path:
            node = self.tree.find(self.published_path)
            if node is not None:
                if "iso-8601-date" in node.attrib:
                    date_published_iso_8601_date_str = node.attrib["iso-8601-date"]
                    date_str = date_published_iso_8601_date_str
                else:
                    year = month = day = ""

                    sub_node = node.find("year")
                    if sub_node is not None:
                        year = sub_node.text
                    sub_node = node.find("month")
                    if sub_node is not None:
                        month = sub_node.text
                    sub_node = node.find("day")
                    if sub_node is not None:
                        day = sub_node.text

                    date_str = year
                    if date_str and month:
                        date_str += "-" + month
                    if date_str and day:
                        date_str += "-" + day

        return date_str

    def get_prod_deployed_date_iso_8601_date_str(self):
        if self.prod_deployed_date_path:
            node = self.tree.find(self.prod_deployed_date_path)
            if node is not None:
                prod_deployed_date_iso_8601_date_str = node.attrib["iso-8601-date"]
                return prod_deployed_date_iso_8601_date_str
            return None


class StreamGroup:
    def __init__(self, tree):
        self.use = tree.get("use").lower()
        streams = []
        for node in tree:
            link = node.find("link")
            rel = href = seq = type = ""
            for attrib in link.attrib:
                name = normalize(attrib)

                rel = link.attrib[attrib] if name == "rel" else rel
                href = link.attrib[attrib] if name == "href" else href
                seq = link.attrib[attrib] if name == "seq" else seq
                type = node.attrib[attrib] if name == "content-type" else type

            v = {
                "rel": rel,
                "mimetype": type,
                "location": href,
                "seq": seq,
                "text": link.text or "",
            }
            streams.append(v)
        self.streams = streams


###
#
class Work(XmlData):
    lang = "und"
    back_paths = ("back", "book-back")
    biblio_xpath = "ref-list"

    def inner_get_lang(self, node):
        the_lang = get_lang_attrib(node)
        if the_lang == "und":
            the_lang = self.lang

        return the_lang

    def inner_get_abstract(self, node, tag, attrs):
        if node is not None:
            the_lang = self.inner_get_lang(node)

            value_xml = get_mixed_content(node)
            value_html = get_html_mixed_content(node)
            value_tex = get_tex(node)

            attrs.append(
                {
                    "tag": tag,
                    "lang": the_lang,
                    "value_xml": value_xml,
                    "value_html": value_html,
                    "value_tex": value_tex,
                }
            )

    def get_abstracts(self):
        attrs = []
        nodes = self.tree.findall(self.abstract_path)
        for node in nodes:
            tag = node.get("abstract-type") or "abstract"
            self.inner_get_abstract(node, tag, attrs)

        nodes = self.tree.findall(self.trans_abstract_path)
        for node in nodes:
            tag = node.get("abstract-type") or "abstract"
            tag = "trans-" + tag
            self.inner_get_abstract(node, tag, attrs)

        return attrs

    def get_contrib_groups(self):
        groups = []
        grps = self.tree.findall(self.contrib_path)
        for g in grps:
            contribs = g.findall("contrib")
            gc = []
            for contrib in contribs:
                params = parse_contrib(contrib)
                params["contrib_type"] = contrib.get("contrib-type") or ""
                params["deceased"] = contrib.get("deceased") or ""
                params["contrib_xml"] = get_mixed_content(contrib)
                if (
                    params["first_name"]
                    or params["last_name"]
                    or params["string_name"]
                    or params["reference_name"]
                ):
                    gc.append(params)
            if gc:
                groups.append({"content_type": g.get("content-type") or "", "contribs": gc})
        return groups

    def get_kwd_groups(self):
        groups = []
        grps = self.tree.findall(self.kwd_path)
        for g in grps:
            ugrp = g.find("unstructured-kwd-group")
            the_lang = self.inner_get_lang(g)
            if ugrp is not None:
                value_xml = get_mixed_content(ugrp)
                value_tex = get_tex(ugrp)
                value_html = get_html_mixed_content(ugrp)
                groups.append(
                    {
                        "content_type": g.get("content-type") or "",
                        "lang": the_lang,
                        "value_xml": value_xml,
                        "value_html": value_html,
                        "value_tex": value_tex,
                        "kwds": [],
                    }
                )
            else:
                kwds = g.findall("kwd")
                values = [innerxml(x) for x in kwds]
                groups.append(
                    {
                        "content_type": g.get("kwd-group-type") or "",
                        "lang": the_lang,
                        "value": "",
                        "kwds": values,
                    }
                )
        return groups

    def get_subj_groups(self):
        groups = []
        grps = self.tree.findall(self.subj_path)
        for g in grps:
            the_lang = self.inner_get_lang(g)
            subjects = g.findall("subject")
            values = [innerxml(x) for x in subjects]
            groups.append(
                {
                    "content_type": g.get("subj-group-type") or "",
                    "lang": the_lang,
                    "value": "",
                    "subjects": values,
                }
            )
        return groups

    def get_awards(self):
        awards = []

        nodes = self.tree.findall(self.funding_path)
        for node in nodes:
            abbrev = award_id = None
            names = node.findall("funding-source/named-content")
            for name_node in names:
                tag = name_node.get("content-type") or ""
                if tag == "abbrevation":
                    abbrev = innerxml(name_node)
            id_node = node.find("award-id")
            if id_node is not None:
                award_id = innerxml(id_node)

            if abbrev is not None and id is not None:
                awards.append({"abbrev": abbrev, "award_id": award_id})

        return awards

    # def get_title_group(self):
    #     title_xml = ''
    #     group = self.tree.find(self.title_group_elt_path)
    #     if group is not None:
    #         title_xml = get_mixed_content(group)
    #     return title_xml
    #         return innerxml(group)
    #     return ""
    #
    # def get_title_text(self):
    #     return self.get_node_text(self.title_path)

    # def get_abstract(self):
    #     return self.get_xml(self.abstract_path)
    #
    # def get_abstract_text(self):
    #     return self.get_node_text(self.abstract_path)
    #
    # def get_trans_abstracts(self):
    #     return self.get_catxml(self.trans_abstract_path)

    def get_keywords(self):
        return self.get_catxml(self.kwd_path)

    def get_bibitems(self):
        for back_path in self.back_paths:
            back = self.tree.find(back_path)
            if back is not None:
                break
        if back is None:
            return []
        ref_list = back.find(self.biblio_xpath)
        if ref_list is None:
            return []
        items = []
        for ref in ref_list:
            if ref.tag == "ref":
                items.append(BibItem(ref))
        # try:
        #     self.tree.getroot().remove(back)
        # except:
        #     self.tree.remove(back)
        return items


class InCollection(XmlData):
    def __init__(self, tree):
        super().__init__(tree)
        self.volume, self.seq, self.vseries = get_volume_and_seq(tree)
        colmeta = tree.find("collection-meta")
        self.collection = Collection(colmeta)


class BitsCollection(XmlData):
    def __init__(self, tree):
        try:
            seq = int(tree.get("seq"))
        except BaseException:
            try:
                seq = int(tree.find("volume-in-collection/volume-number").text)
            except BaseException:
                seq = 0
        try:
            volume = tree.find("volume-in-collection/volume-number").text
        except BaseException:
            volume = ""
        try:
            series = tree.find("volume-in-collection/volume-series").text
        except BaseException:
            series = ""
        self.volume = volume
        self.seq = seq
        self.vseries = series
        self.collection = Collection(tree)


class Publisher(XmlData):
    mathdoc_id_xpath = 'publisher-id[@publisher-id-type="mathdoc-id"]'

    def get_name(self):
        return self.get_node_text("publisher-name")

    def get_loc(self):
        return self.get_node_text("publisher-loc")


class EventSeries(XmlData):
    def __init__(self, tree):
        super().__init__(tree)
        self.event_type = tree.get("event-type")

    def get_title(self):
        return self.get_node_text("event-name")

    def get_acro(self):
        return self.get_node_text("event-acronym")

    def get_short_title(self):
        return ""


class Event(XmlData):
    def __init__(self, tree):
        super().__init__(tree)
        self.event_type = tree.get("event-type")

    def get_title(self):
        return self.get_node_text("event-name")

    def get_acro(self):
        return self.get_node_text("event-acronym")

    def get_year(self):
        return self.get_node_text("event-date")

    def get_number(self):
        return self.get_node_text("event-num")

    def get_loc(self):
        return self.get_node_text("event-loc")


# <collection-meta> d'un <book>


class Collection(Work):
    lang = "und"
    title_group_elt_path = "title-group"
    title_path = "title-group/title"
    subtitle_path = "title-group/subtitle"
    abstract_path = "abstract"
    trans_abstract_path = "trans-abstract"
    kwd_path = "kwd-group"
    subj_path = "Not-supported"
    ids_xpath = "collection-id"
    mathdoc_id_xpath = 'collection-id[@collection-id-type="mathdoc-id"]'
    trans_title_group_elt_path = "title-group/trans-title-group"
    trans_title_path = "title-group/trans-title-group/trans-title"
    funding_path = "Not supported"

    contrib_path = "contrib-group"
    id_type_attr = "collection-id-type"

    def get_coltype(self):
        return self.tree.get("collection-type") or "collection"

    def get_publisher(self):
        node = self.tree.find("publisher")
        if node is not None:
            return Publisher(node)
        return None

    def get_title(self):
        return self.get_node_text("title-group/title")

    def get_abbrev(self):
        return self.get_node_text("title-group/abbrev-title")

    def get_ids(self):
        ids = XmlData.get_ids(self)
        issns = self.tree.findall("issn")
        for issn in issns:
            itp = issn.get("pub-type")
            if itp == "ppub":
                ids.append(("issn", issn.text))
            elif itp == "epub":
                ids.append(("e-issn", issn.text))
            else:
                pass
        return ids


# <journal-meta> d'un <journal-issue>


class Journal(Work):
    ids_xpath = "journal-id"
    id_type_attr = "journal-id-type"
    title_group_elt_path = "journal-title-group"
    title_path = "journal-title-group/journal-title"
    abbrev_title_path = "journal-title-group/abbrev-title"
    trans_title_group_elt_path = "journal-title-group/trans-title-group"
    trans_title_path = "journal-title-group/trans-title-group/trans-title"
    abstract_path = "abstract"
    trans_abstract_path = "trans-abstract"
    contrib_path = "contrib-group"
    kwd_path = "kwd-group"
    subj_path = "Not-supported"
    funding_path = "Not-supported"

    def get_ids(self):
        ids = XmlData.get_ids(self)
        issns = self.tree.findall("issn")
        for issn in issns:
            itp = issn.get("pub-type")
            if issn.text:
                if itp == "ppub":
                    ids.append(("issn", issn.text))
                elif itp == "epub":
                    ids.append(("e-issn", issn.text))
                else:
                    pass
        return ids

    def get_publisher(self):
        node = self.tree.find("publisher")
        if node is not None:
            return Publisher(node)
        return None

    def get_title_group(self):
        node = self.tree.find(self.title_group_elt_path)
        if node is not None:
            return innerxml(node)
        return ""

    def get_title_xml(self):
        title_xml = ""
        node = self.tree.find(self.title_group_elt_path)
        if node is not None:
            title_xml = get_mixed_content(node)
        return title_xml

    def get_title_html(self):
        title_html = ""
        node = self.tree.find(self.title_path)
        if node is not None:
            title_html = get_html_mixed_content(node)
        return title_html

    def get_title_tex(self):
        title_tex = ""
        node = self.tree.find(self.title_path)
        if node is not None:
            title_tex = get_tex(node)
        return title_tex

    def get_abbrev(self):
        return self.get_node_text(self.abbrev_title_path)

    def get_coltype(self):
        return self.custom_meta.get("serial-type")


class Publication(Journal):
    ids_xpath = "publication-id"
    id_type_attr = "publication-id-type"
    title_group_elt_path = "title-group"
    title_path = "title-group/title"
    abbrev_title_path = "title-group/abbrev-title"
    trans_title_group_elt_path = "title-group/trans-title-group"
    trans_title_path = "title-group/trans-title-group/trans-title"


class Issue(Work):
    mathdoc_id_xpath = 'issue-meta/issue-id[@issue-id-type="mathdoc-id"]'
    ids_xpath = "issue-meta/issue-id"
    abstract_path = "issue-meta/abstract"
    trans_abstract_path = "issue-meta/trans-abstract"
    kwd_path = "issue-meta/kwd-group"
    subj_path = "Not-supported"
    contrib_path = "issue-meta/contrib-group"
    title_group_elt_path = "issue-meta/issue-title"
    title_path = "issue-meta/issue-title"
    # TODO support langs in issue-title
    subtitle_path = ""
    trans_title_path = ""
    trans_title_group_elt_path = ""
    counts_path = "issue-meta/counts"
    last_modified_path = 'issue-meta/history/date[@date-type="last-modified"]'
    published_path = 'issue-meta/pub-date[@date-type="pub"]'
    prod_deployed_date_path = 'issue-meta/history/date[@date-type="prod-deployed-date"]'
    funding_path = "Not-supported"

    lang = "und"
    meta_root_xpath = "issue-meta"
    custom_meta_path = "issue-meta/custom-meta-group"

    def get_journal(self):
        node = self.tree.find("journal-meta")
        return Journal(node)

    def get_ctype(self):
        return "issue"

    def get_vseries(self):
        return self.get_node_text("issue-meta/volume-series")

    def get_vseries_int(self):
        v = self.get_node_text("issue-meta/volume-series")
        if v:
            return make_int(v)
        return 0

    def get_volume(self):
        return self.get_node_text("issue-meta/volume")

    def get_volume_int(self):
        v = self.get_node_text("issue-meta/volume")
        if v:
            return make_int(v)
        return 0

    def get_number(self):
        return self.get_node_text("issue-meta/issue")

    def get_number_int(self):
        v = self.get_node_text("issue-meta/issue")
        if v:
            return make_int(v)
        return 0

    def get_year(self):
        return self.get_node_text("issue-meta/pub-date/year")

    def get_event(self):
        node = self.tree.find("event")
        if node is not None:
            return Event(node)
        return None

    def get_publisher(self):
        xpublisher = None
        xjournal = self.get_journal()
        if xjournal is not None:
            xpublisher = xjournal.publisher
        return xpublisher

    def __iter__(self):
        body = self.tree.find("body")
        for node in body:
            yield Article(node)


class BibItem(XmlData):
    extids_xpath = "*/ext-link"
    extid_type_attr = "ext-link-type"

    # remove_links = True
    def __init__(self, tree):
        super().__init__(tree)
        self.extids = self.get_extids()

        # Temporary code
        # Some xml only have a pub-id (doi) and do not have an ext-link with a ext-link-type=doi
        # We need to manually create the link

        has_doi = False
        for id_type, _id_value in self.extids:
            if id_type == "doi":
                has_doi = True

        nodes = self.tree.findall("*/pub-id")
        for node in nodes:
            id_type = node.get("pub-id-type")
            if id_type == "doi" and not has_doi:
                value = node.text
                value = value.replace("http://dx.doi.org/", "")
                value = value.replace("https://doi.org/", "")
                value = value.replace("doi:", "")
                self.extids.append(("doi", value))
            elif id_type in ["eid", "arxiv", "tel", "hal", "theses.fr"]:
                value = node.text
                self.extids.append((id_type, value))

    def get_ref(self):
        return self.tostring()

    def split_label(self):
        """
        Used when sorting non-digit bibitems
        """
        label = self.label.lower()

        try:
            self.label_prefix, self.label_suffix = re.split(r"[\d]+", label)
        except ValueError:
            # Special case where label is similar as "Sma" instead of "Sma15"
            self.label_prefix, self.label_suffix = [label, ""]

    def get_label(self):
        node = self.tree.find("label")
        if node is not None:
            return node.text
        return ""

    def get_user_id(self):
        return self.tree.get("id", "")

    def get_citation_xml(self):
        text = ""
        for name in ("mixed-citation", "element-citation"):
            if not text:
                node = self.tree.find(name)
                text = get_mixed_content(node)

        label = self.get_label()
        if label:
            text = "<label>" + label + "</label>" + text

        return text

    def get_citation_html(self):
        text = ""
        for name in ("mixed-citation", "element-citation"):
            if not text:
                node = self.tree.find(name)
                text = get_html_mixed_content(node)

        label = self.get_label()
        if label:
            if label[0] != "[":
                label = "[" + label + "]"
            text = label + " " + text

        return text

    def get_citation_tex(self):
        text = ""
        for name in ("mixed-citation", "element-citation"):
            if not text:
                node = self.tree.find(name)
                text = get_tex(node)

        label = self.get_label()
        if label:
            if label[0] != "[":
                label = "[" + label + "]"
            text = label + " " + text

        return text

    def get_citation_node(self):
        tree = self.tree.find("element-citation")
        if tree is None:
            tree = self.tree.find("mixed-citation")

        return tree

    def get_type(self):
        type = "misc"

        tree = self.get_citation_node()
        if tree is not None:
            type = tree.get("publication-type", "misc")

        return type

    def get_node_text(self, node_name, tex=False):
        text = ""
        tree = self.get_citation_node()
        if tree is not None:
            node = tree.find(node_name)
            if node is not None:
                if tex:
                    text = get_tex(node)
                else:
                    text = node.text
        return text

    def get_publisher_name(self):
        return self.get_node_text("publisher-name")

    def get_publisher_loc(self):
        return self.get_node_text("publisher-loc")

    def get_institution(self):
        return self.get_node_text("institution")

    def get_series(self):
        return self.get_node_text("series")

    def get_volume(self):
        return self.get_node_text("volume")

    def get_issue(self):
        return self.get_node_text("issue")

    def get_year(self):
        return self.get_node_text("year")

    # TODO: comments may have ext-link like arxiv. Add ExtId ?
    def get_comment(self):
        return self.get_node_text("comment", tex=True)

    def get_fpage(self):
        return self.get_node_text("fpage")

    def get_lpage(self):
        return self.get_node_text("lpage")

    def get_page_range(self):
        return self.get_node_text("page-range")

    def get_size(self):
        text = self.get_node_text("page-count")
        if not text:
            text = self.get_node_text("size")
        return text

    def get_source_tex(self):
        return self.get_node_text("source", tex=True)

    def get_article_title_tex(self):
        return self.get_node_text("article-title", tex=True)

    def get_chapter_title_tex(self):
        return self.get_node_text("chapter-title", tex=True)

    def get_contrib_groups(self):
        groups = []

        tree = self.get_citation_node()
        if tree is not None:
            gc = []

            for child in tree:
                if (
                    child.tag == "name"
                    or child.tag == "string-name"
                    or child.tag == "name-alternatives"
                ):
                    params = parse_name(child)
                    params["contrib_type"] = ""
                    params["contrib_xml"] = get_mixed_content(child)
                    gc.append(params)

            groups.append({"content_type": "", "contribs": gc})
        return groups


class Relation(XmlData):
    def get_id_type(self):
        return self.tree.get("ext-link-type") or ""

    def get_rel_type(self):
        return self.tree.get("related-article-type") or ""

    def get_id_value(self):
        return self.tree.text or ""

    def get_right_pid(self):
        return self.id_value


class Article(Work):
    mathdoc_id_xpath = 'article-id[@pub-id-type="mathdoc-id"]'
    ids_xpath = "front/article-meta/article-id"
    article_xpath = "front/article-meta"

    extids_xpath = (
        'front/article-meta/ext-link[@ext-link-type="mr-item-id"]'
        '|front/article-meta/ext-link[@ext-link-type="zbl-item-id"]'
        '|front/article-meta/ext-link[@ext-link-type="sps-id"]'
        '|front/article-meta/ext-link[@ext-link-type="jfm-item-id"]'
    )
    extid_type_attr = "ext-link-type"
    title_group_elt_path = "front/article-meta/title-group"
    title_path = "front/article-meta/title-group/article-title"
    subtitle_path = "front/article-meta/title-group/subtitle"
    trans_title_group_elt_path = "front/article-meta/title-group/trans-title-group"
    trans_title_path = "front/article-meta/title-group/trans-title-group/trans-title"
    abstract_path = "front/article-meta/abstract"
    trans_abstract_path = "front/article-meta/trans-abstract"
    kwd_path = "front/article-meta/kwd-group"
    subj_path = "front/article-meta/article-categories/subj-group"
    contrib_path = "front/article-meta/contrib-group"
    meta_root_xpath = "front/article-meta"
    custom_meta_path = "front/article-meta/custom-meta-group"
    counts_path = "front/article-meta/counts"
    published_path = 'front/article-meta/pub-date[@date-type="pub"]'
    prod_deployed_date_path = 'front/article-meta/history/date[@date-type="prod-deployed-date"]'
    history_path = "front/article-meta/history/date"
    funding_path = "front/article-meta/funding-group/award-group"

    def __init__(self, tree):
        # Case when we import the JATS article from OAI.
        # The <article> tag is surrounded by a <header> tag. Remove this tag.
        if tree.tag != "article":
            remove_namespace(tree)
            tree = tree.xpath("metadata/article")[0]

        super().__init__(tree)
        self.article_meta = self.get_subtree(self.article_xpath)
        self.atype = tree.get("article-type") or ""
        self.numbering = ""
        self.lang = self.get_lang()

    def get_doi(self):
        try:
            text = self.tree.xpath('front/article-meta/article-id[@pub-id-type="doi"]')[0].text
        except BaseException:
            return None
        else:
            return text

    # When the JATS XML has only an <article>, we need to construct the Journal on the fly
    def get_journal(self):
        node = self.tree.xpath("front/journal-meta")[0]
        return Journal(node)

    def get_issue_id(self):
        try:
            return self.tree.xpath("front/article-meta/issue-id")[0].text
        except:
            return ""

    def get_volume(self):
        try:
            return self.tree.xpath("front/article-meta/volume")[0].text
        except:
            return ""

    def get_fpage(self):
        return self.get_node_text("front/article-meta/fpage")

    def get_lpage(self):
        return self.get_node_text("front/article-meta/lpage")

    def get_page_type(self):
        page_type = ""
        node = self.tree.find("front/article-meta/fpage")
        if node is not None:
            page_type = node.get("content-type")

        if page_type is None:
            page_type = ""

        return page_type

    # Olivier 2016-01-13 add page-range & elocation
    def get_page_range(self):
        return self.get_node_text("front/article-meta/page-range")

    def get_elocation(self):
        return self.get_node_text("front/article-meta/elocation-id")

    def get_body(self):
        node = self.tree.find("body")
        text = get_node_text(node)
        return text

    def body_jats_to_html(self, base_url):
        body_html = ""
        figures = []
        node = self.tree.find("body")
        if node is not None:
            body_html, figures = get_html_mixed_content_with_figures(
                node,
                is_top=True,
                is_citation=False,
                is_comment=False,
                is_figure=False,
                prefix="",
                suffix="",
                sec_level=2,
                label_title="",
                figures=figures,
                base_url=base_url,
            )
        return body_html, figures

    def get_body_tex(self):
        node = self.tree.find("body")
        # TODO: body_tex devrait être en fait le HTML va les fourmules TeX en texte
        value_tex = get_tex(node)
        return value_tex

    def get_body_xml(self):
        node = self.tree.find("body")
        value_xml = get_mixed_content(node)
        return value_xml

    def get_seq(self):
        issue = self.get_subtree("front/article-meta/issue")
        seq = 0
        if issue is not None:
            seq = issue.get("seq") or 0
        if not seq:
            fpage = self.get_subtree("front/article-meta/fpage")
            if fpage is not None:
                seq = fpage.get("seq") or 0
        try:
            seq = int(seq)
        except BaseException:
            seq = 0
        return seq

    def get_relations(self):
        relations = []
        nodes = self.tree.findall("front/article-meta/related-article")
        for n in nodes:
            rel = Relation(n)
            rel.left_pid = self.pid
            relations.append(rel)
        return relations

    def get_history_dates(self):
        dates = []
        nodes = self.tree.findall(self.history_path)
        for node in nodes:
            type = node.attrib["date-type"]
            date = node.attrib["iso-8601-date"]
            dates.append({"type": type, "date": date})

        return dates

    def get_article_number(self):
        return self.custom_meta.get("article-number", "")

    def get_talk_number(self):
        return self.custom_meta.get("talk-number", "")


class BookSeries(XmlData):
    mathdoc_id_xpath = 'collection-id[@collection-id-type="mathdoc-id"]'
    ids_xpath = "collection-id"
    extid_type_attr = "collection-id-type"
    title_group_elt_path = "title-group"
    title_path = "title-group/title"
    subtitle_path = "title-group/subtitle"
    lang = "und"

    def get_ids(self):
        ids = []
        issn = self.get_node_text("issn")
        if issn:
            ids.append(("issn", issn))
        nodes = self.tree.findall("collection-id")
        for n in nodes:
            id_type = n.get("collection-id-type")
            id_val = n.text
            ids.append((id_type, id_val))
        return ids

    def get_title(self):
        return self.get_node_text(self.title_path)

    def get_abbrev(self):
        return self.get_node_text("title-group/abbrev-title")

    def get_publisher(self):
        node = self.tree.find("publisher")
        if node is not None:
            return Publisher(node)
        return None

    def get_stype(self):
        return self.custom_meta.get("serial-type")


# Mixin
class HasParts:
    def get_parts(self):
        xparts = []
        for name in ("book-body", "body"):
            parts = self.xget_subtrees("%s/book-part" % name)
            if parts:
                break
        if parts:
            for tree in parts:
                part = self.__class__.get_book_part_class()(tree)
                xparts.append(part)
            for name in ("book-body", "body"):
                body = self.get_subtree(name)
                if body is not None:
                    break
            if body is not None:
                try:
                    self.tree.getroot().remove(body)  # XSLT result tree
                except BaseException:
                    self.tree.remove(body)  # Element tree
        return xparts


class BookPart(Work, HasParts):
    id_type_attr = "book-part-id-type"
    part_xpath = "book-part-meta"
    ids_xpath = "book-part-meta/book-part-id"
    mathdoc_id_xpath = 'book-part-meta/book-part-id[@book-part-id-type="mathdoc-id"]'
    meta_xpath = "book-part-meta"
    extids_xpath = (
        'book-part-meta/ext-link[@ext-link-type="mr-item-id"]'
        '|book-part-meta/ext-link[@ext-link-type="zbl-item-id"]'
        '|book-part-meta/ext-link[@ext-link-type="jfm-item-id"]'
    )
    extid_type_attr = "ext-link-type"
    title_group_elt_path = "book-part-meta/title-group"
    title_path = "book-part-meta/title-group/title"
    subtitle_path = "book-part-meta/title-group/subtitle"
    trans_title_group_elt_path = "book-part-meta/title-group/trans-title-group"
    trans_title_path = "book-part-meta/title-group/trans-title-group/trans-title"
    abstract_path = "book-part-meta/abstract"
    trans_abstract_path = "book-part-meta/trans-abstract"
    kwd_path = "book-part-meta/kwd-group"
    subj_path = "front/book-part-meta/article-categories/subj-group"
    contrib_path = "book-part-meta/contrib-group"
    meta_root_xpath = "book-part-meta"
    custom_meta_path = "book-part-meta/custom-meta-group"
    funding_path = "book-part-meta/funding-group/award-group"

    def __init__(self, tree):
        super().__init__(tree)
        self.part_meta = self.get_subtree(self.part_xpath)
        indexed = tree.get("indexed", "true")
        self.indexed = True if indexed == "true" else False
        self.atype = tree.get("book-part-type") or ""
        self.numbering = tree.get("book-part-number") or ""
        self.parts = self.get_parts()
        self.lang = self.get_lang()

    def get_fpage(self):
        return self.get_node_text("book-part-meta/fpage")

    def get_lpage(self):
        return self.get_node_text("book-part-meta/lpage")

    def get_page_range(self):
        return ""

    def get_page_type(self):
        page_type = ""
        node = self.tree.find("book-part-meta/fpage")
        if node is not None:
            page_type = node.get("content-type")

        if page_type is None:
            page_type = ""

        return page_type

    def get_seq(self):
        v = self.fpage
        try:
            v = int(v)
        except BaseException:
            return 0
        return v

    def get_body(self):
        node = self.tree.find("body")
        if node is not None:
            return etree.tostring(node, encoding="utf-8", xml_declaration=False)
        return ""

    def get_relations(self):
        relations = []
        nodes = self.tree.findall("book-part-meta/related-article")
        for n in nodes:
            rel = Relation(n)
            rel.left_pid = self.pid
            relations.append(rel)
        return relations

    def get_article_number(self):
        return self.custom_meta.get("article-number", "")

    def get_talk_number(self):
        return self.custom_meta.get("talk-number", "")


def get_volume_and_seq(incol):
    v = incol.find("volume")
    try:
        seq = int(incol.get("seq"))
    except BaseException:
        if v is None:
            seq = 0
        else:
            vt = v.text.split("-")[0]
            vt = [x for x in vt if x.isdigit()]
            try:
                seq = int(vt)
            except BaseException:
                seq = 0
    try:
        volume = v.text
    except BaseException:
        volume = ""
    try:
        vseries = incol.find("volume-series").text
    except BaseException:
        vseries = ""
    if vseries:
        try:
            # pas plus de 10000 ouvrages dans une série (gasp)
            seq = int(vseries) * 10000 + seq
        except BaseException:
            pass
    return (volume, seq, vseries)


class Book(Work, HasParts):
    id_type_attr = "book-id-type"
    mathdoc_id_xpath = 'book-meta/book-id[@book-id-type="mathdoc-id"]'
    ids_xpath = "book-meta/book-id"
    book_xpath = "book-meta"
    extids_xpath = (
        'book-meta/ext-link[@ext-link-type="mr-item-id"]'
        '|book-meta/ext-link[@ext-link-type="zbl-item-id"]'
        '|book-meta/ext-link[@ext-link-type="jfm-item-id"]'
    )
    extid_type_attr = "ext-link-type"

    title_group_elt_path = "book-meta/book-title-group"
    title_path = "book-meta/book-title-group/book-title"
    alternate_title_group_elt_path = "collection-meta/volume-in-collection/volume-title"
    alternate_title_path = "collection-meta/volume-in-collection/volume-title"
    trans_title_group_elt_path = "book-meta/book-title-group/trans-title-group"
    trans_title_path = "book-meta/book-title-group/trans-title-group/trans-title"
    subtitle_path = "book-meta/book-title-group/subtitle"

    abstract_path = "book-meta/abstract"
    trans_abstract_path = "book-meta/trans-abstract"
    kwd_path = "book-meta/kwd-group"
    subj_path = "Not-supported"
    contrib_path = "book-meta/contrib-group"
    meta_root_xpath = "book-meta"
    custom_meta_path = "book-meta/custom-meta-group"
    counts_path = "book-meta/counts"
    last_modified_path = 'book-meta/pub-history/date[@date-type="last-modified"]'
    published_path = 'book-meta/pub-date[@date-type="pub"]'
    prod_deployed_date_path = 'book-meta/pub-history/date[@date-type="prod-deployed-date"]'
    year_path = "book-meta/pub-date/year"
    funding_path = "Not-supported"

    mbook_seq = 0
    mbook_volume = ""
    mbook_vseries = ""

    def __init__(self, tree):
        # Case when we import the book from OAI.
        # The <book> tag is surrounded by a <header> tag. Remove this tag.
        if tree.tag != "book":
            remove_namespace(tree)
            tree = tree.xpath("metadata/book")[0]
            if tree.getchildren()[0].tag == "front":
                tree = tree.xpath("front")[0]

        super().__init__(tree)
        self.book_meta = self.get_subtree(self.book_xpath)
        self.contrib_groups = []
        try:
            self.book_type = tree.get("book-type") or "Book"
        except BaseException:
            self.book_type = tree.getroot().get("book-type") or "Book"
        # if self.book_type == 'proceedings' or self.book_type == 'edited-book'
        # or self.book_type == 'monograph' :
        if self.book_type:
            self.parts = self.get_parts()

            # patch for book without contrib-group:
            # 1 : monograph with book_parts : contrib-group of book egal to the
            #   contrib-group of the first book-part
            # OR 2 : edited-books with same author for all of its book_parts : book-type become 'monograph' and
            #   contrib-group of book equal to the contrib-group of the first book-part
            # OR 3 : edited-books but not same author for all book-parts : contrib-group of
            # book become "Collectif"
            self.contrib_groups = self.get_contrib_groups()
            if not self.contrib_groups:
                if self.book_type == "monograph" and self.parts:
                    first_part = self.parts[0]
                    self.contrib_groups = first_part.get_contrib_groups()
                elif self.book_type == "edited-book" and self.parts:
                    # check if authors of the book-parts are identical
                    equal = True
                    book_part_contrib_group = self.parts[0].get_contrib_groups()
                    for xparts in self.parts:
                        if xparts.get_contrib_groups() != book_part_contrib_group:
                            equal = False
                            break
                    if equal:
                        # FIXME : ? is it a check or an assignation ?
                        self.book_type == "monograph"
                        self.contrib_groups = book_part_contrib_group
                    else:
                        self.contrib_groups = [
                            {
                                "contribs": [
                                    {
                                        "first_name": "",
                                        "last_name": "Collectif",
                                        "suffix": "",
                                        "string_name": "Collectif",
                                        "reference_name": "Collectif",
                                        "contrib_xml": "<contrib><name><surname>Collectif</surname><given-names>"
                                        + "</given-names></name><name-alternatives>"
                                        + '<string-name specific-use="index">Collectif</string-name></name-alternatives></contrib>',
                                        "prefix": "",
                                        "contrib_type": "author",
                                    }
                                ],
                                "content_type": "authors",
                            }
                        ]

            self.body = ""
        # else: #or self.book_type == 'monograph': pour monograph pas de book-part, body contient le plein text
        #     self.parts = []
        self.incollection = self.get_incollection()

        self.lang = self.get_lang()

    @staticmethod
    def get_book_part_class():
        return BookPart

    def get_doi(self):
        try:
            text = self.tree.xpath('book-meta/book-id[@book-id-type="doi"]')[0].text
        except BaseException:
            return None
        else:
            return text

    def get_ctype(self):
        return "book-%s" % self.book_type

    def get_contrib_groups(self):
        if self.contrib_groups:
            return self.contrib_groups
        return super().get_contrib_groups()

    def get_publisher(self):
        node = self.tree.find("book-meta/publisher")
        if node is not None:
            return Publisher(node)
        return None

    def get_year(self):
        return self.get_node_text(self.year_path)

    def get_title(self):
        text = self.get_node_text("book-meta/title-group/title")
        if not text:
            self.get_node_text("collection-meta/volume-in-collection/volume-title")
        return text

    def get_body(self):
        node = self.tree.find("book-body")
        if node is not None:
            return etree.tostring(node, encoding="utf-8", xml_declaration=False)
        return ""

    def get_incollection(self):
        nodes = self.tree.findall("in-collection")
        incols = []
        for node in nodes:
            incols.append(InCollection(node))
        if incols:
            return incols
        nodes = self.tree.findall("collection-meta")
        for node in nodes:
            incols.append(BitsCollection(node))
        return incols

    def get_event(self):
        node = self.tree.find("book-meta/event")
        if node is not None:
            return Event(node)
        return None

    def get_event_series(self):
        node = self.tree.find("book-meta/event-series")
        if node is not None:
            return EventSeries(node)
        return None

    def get_vseries(self):
        return self.get_node_text("book-meta/volume-series")

    def get_frontmatter(self):
        node = self.tree.find("front-matter")
        if node is not None:
            return innerxml(node)
        return ""

    def get_relations(self):
        relations = []
        nodes = self.tree.findall("book-meta/related-article")
        for n in nodes:
            rel = Relation(n)
            rel.left_pid = self.pid
            relations.append(rel)
        return relations


factories = {
    "collection": Collection,
    "publisher": Publisher,
    "journal": Journal,
    "issue": Issue,
    "article": Article,
    "book": Book,
}


def xobj_fromtree(classname, tree):
    factory = factories[classname]
    return factory(tree)


def xobj_fromstring(classname, metadata):
    tree = etree.fromstring(metadata)
    return xobj_fromtree(classname, tree)


def xobj_fromfile(classname, path):
    metadata = open(path, "rb").read()
    return xobj_fromstring(classname, metadata)


def update_bibitem_xml(bibitem, new_ids):
    xml = "<ref>" + bibitem.citation_xml + "</ref>"
    parser = etree.XMLParser(
        huge_tree=True, recover=True, remove_blank_text=True, remove_comments=True
    )
    tree = etree.fromstring(xml, parser=parser)

    node = tree.find("element-citation")
    if node is None:
        node = tree.find("mixed-citation")
    if node is not None:
        children_to_remove = []
        for child in node:
            if child.tag == "ext-link":
                type = child.get("ext-link-type")
                if type and type in new_ids:
                    children_to_remove.append(child)
            elif child.tag == "pub-id":
                type = child.get("pub-id-type")
                if type and type in new_ids:
                    children_to_remove.append(child)

        for child in children_to_remove:
            node.remove(child)

        for type, value_dict in new_ids.items():
            if value_dict["checked"] and not value_dict["false_positive"]:
                if type in ["doi", "arxiv", "tel", "hal", "theses.fr"]:
                    new_node = etree.Element("pub-id")
                    new_node.set("pub-id-type", type)
                else:
                    new_node = etree.Element("ext-link")
                    new_node.set("ext-link-type", type)

                new_node.text = value_dict["id_value"]
                node.append(new_node)

    result = BibItem(tree)
    return result


#########################################################################################
#
#  Create XML strings based on internal data
#
#########################################################################################


def get_contrib_xml(type, first_name, last_name, prefix, suffix, deceased):
    xml = "<contrib"
    if type:
        xml += ' contrib-type="' + type + '"'
    if deceased:
        xml += ' deceased="yes"'
    xml += "><name>"

    if prefix:
        xml += "<prefix>" + prefix + "</prefix>"
    if first_name:
        xml += "<given-names>" + first_name + "</given-names>"
    if last_name:
        xml += "<surname>" + last_name + "</surname>"
    if suffix:
        xml += "<suffix>" + suffix + "</suffix>"

    xml += "</name></contrib>"

    return xml


def get_title_xml(title):
    xml = '<title-group xmlns:xlink="http://www.w3.org/1999/xlink"><article-title xml:space="preserve">'
    xml += title
    xml += "</article-title></title-group>"

    return xml
