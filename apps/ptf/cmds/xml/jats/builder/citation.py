from typing import NotRequired
from typing import TypedDict
from typing import Unpack

from ptf.cmds.xml.xml_utils import escape
from ptf.display.resolver import extids_formats


class ContribAuthor(TypedDict):
    template_str: str
    surname: NotRequired[str]
    given_names: NotRequired[str]


def get_all_authors_xml(template_str: str, authors_list: list[ContribAuthor]):
    format_dict: dict[str, str] = {}
    for i, author_dict in enumerate(authors_list):
        format_dict[f"author_{i}"] = get_author_xml(**author_dict)
    template_str = template_str.format(**format_dict)
    return f'<person-group person-group-type="author">{template_str}</person-group>'


def get_author_xml(**kwargs: Unpack[ContribAuthor]):
    """
    Args:
        template_str: String with placeholders. Valid placeholders are `{surname}` and `{given_names}`.
    """
    template_str = kwargs["template_str"].format(**kwargs)
    return f"<string_name>{template_str}</string_name>"


def get_source_xml(source: str):
    return f"<source>{escape(source)}</source>"


def get_year_xml(year: str):
    return f"<year>{year}</year>"


def get_volume_xml(volume: str):
    return f"<volume>{volume}</volume>"


def get_ext_link_xml(url: str, content: str, type: str = "uri"):
    if type not in extids_formats:
        type = "uri"
    return f"<ext-link ext-link-type='{type}' href='{url}'>{content}</ext-link>"


def get_publisher_xml(name: str):
    return f"<publisher-name>{name}</publisher-name>"
