"""
Creates Jats xmls.
Used by `crawler` and `ptf.cmds.xml.jats.jats_parser`
"""

from ptf.cmds.xml.xml_utils import escape


def get_single_title_xml(title: str):
    has_italic = title.find("<i>") > -1 and title.find("</i>") > -1
    has_superscript = title.find("<sup>") > -1 and title.find("</sup>") > -1
    has_subscript = title.find("<sub>") > -1 and title.find("</sub>") > -1

    if has_italic:
        title = title.replace("<i>", "|||i|||").replace("</i>", "|||/i|||")
    if has_superscript:
        title = title.replace("<sup>", "|||sup|||").replace("</sup>", "|||/sup|||")
    if has_subscript:
        title = title.replace("<sub>", "|||sub|||").replace("</sub>", "|||/sub|||")

    title = escape(title)

    if has_italic:
        title = title.replace("|||i|||", "<italic>").replace("|||/i|||", "</italic>")

    if has_superscript:
        title = title.replace("|||sup|||", "<sup>").replace("|||/sup|||", "</sup>")

    if has_subscript:
        title = title.replace("|||sub|||", "<sub>").replace("|||/sub|||", "</sub>")

    return title


def get_title_xml(title: str, trans_title=None, trans_lang=None, with_tex_values=True):
    """
    Get the title_xml given a simple title
    If the title has formulas, use CKeditorParser first, then call this function with the value_xml returned by the parser
    and set with_tex_values to False
    TODO: enhance CkeditorParser to accept both title and trans_title to build the xml in 1 shot.
    """
    if with_tex_values:
        title = get_single_title_xml(title)

    xml = '<title-group xmlns:xlink="http://www.w3.org/1999/xlink">'
    xml += f'<article-title xml:space="preserve">{title}</article-title>'

    if trans_title and trans_lang:
        if with_tex_values:
            trans_title = get_single_title_xml(trans_title)
        xml += f'<trans-title-group xml:lang="{trans_lang}"><trans-title>{trans_title}</trans-title></trans-title-group>'

    xml += "</title-group>"

    return xml


def get_issue_title_xml(title: str, lang: str, trans_title=None, trans_lang=None):
    """
    Get the title_xml given a simple title
    """
    title = get_single_title_xml(title)
    xml = f'<issue-title xml:lang="{lang}" xml:space="preserve">{title}</issue-title>'

    if trans_title and trans_lang:
        trans_title = get_single_title_xml(trans_title)
        xml += f'<issue-title xml:lang="{trans_lang}" xml:space="preserve">{trans_title}</issue-title>'

    return xml


# def get_name_params(first_name, last_name, prefix, suffix, orcid):
#     params = {
#         "first_name": first_name,
#         "last_name": last_name,
#         "prefix": prefix,
#         "suffix": suffix,
#         "orcid": orcid,
#     }
#     helper_update_name_params(params)
#     return params
