##################################################################################################
#
# README
#
# jats_parser.py is a replacement of xmldata.py
# The purpose is to parse a JATS xml (or BITS) tree from top to bottom.
# Each node is read only once.
#
# JatsArticle, JatsIssue, JatsJournal, BitsBook are the objects created by xml_cmds.
# The xml tree is parsed in the class constructor (__init__)
# These classes have parse_<tag> functions to parse the xml nodes and set instance variables.
# Some parse_<tag> functions are called directly.
#     Ex:      if tag == "article-meta":
#                  self.parse_article_meta(child)
# Other parse_<tag> functions are called "automatically"
#     fct_name = 'parse_' + tag.replace('-', '_')
#     ftor = getattr(self, fct_name, None)
#     if callable(ftor):
#         ftor(child)
#
# JatsBase and JatsArticleBase are base classes.
# They provide common instance variables and their corresponding parse_<tag> functions
#
# html_from_<tag> are used to generate the HTML text of a node with mixed content:
#    a node that mixes text, children and tail
# These functions can also extract data and set instance variables (ex: self.figures)
#
# get_data_from_* parse a node, but simply return data (text, dict,...) without side effects
#
# At the end of this file, there are some functions that are/were called by ptf-tools.
# They are kept here for simplicity: we can switch xmldata entirely with jats_parser
#
# TODO: the import OAI or the import of a collection could simply call the first function
#       (def parser(tree))
#
##################################################################################################

import copy
import inspect
import os
import re

from lxml import etree
from pylatexenc.latexencode import unicode_to_latex

from django.conf import settings
from django.urls import reverse
from django.utils import timezone

from matching import scrapping
from ptf.cmds.xml.citation_html import add_span_class_to_html_from_article_title
from ptf.cmds.xml.citation_html import add_span_class_to_html_from_authors
from ptf.cmds.xml.citation_html import add_span_class_to_html_from_chapter_title
from ptf.cmds.xml.citation_html import add_span_class_to_html_from_source
from ptf.cmds.xml.citation_html import add_span_class_to_html_from_volume
from ptf.cmds.xml.citation_html import get_citation_html
from ptf.cmds.xml.jats.builder.issue import get_single_title_xml
from ptf.cmds.xml.jats.builder.issue import get_title_xml
from ptf.cmds.xml.xml_base import RefBase
from ptf.cmds.xml.xml_base import XmlParserBase
from ptf.cmds.xml.xml_utils import escape
from ptf.cmds.xml.xml_utils import get_contrib_xml
from ptf.cmds.xml.xml_utils import get_elsevier_image_extensions
from ptf.cmds.xml.xml_utils import get_normalized_attrib
from ptf.cmds.xml.xml_utils import get_text_from_node
from ptf.cmds.xml.xml_utils import get_xml_from_node
from ptf.cmds.xml.xml_utils import helper_update_name_params
from ptf.cmds.xml.xml_utils import make_links_clickable
from ptf.cmds.xml.xml_utils import normalize
from ptf.cmds.xml.xml_utils import normalize_space
from ptf.cmds.xml.xml_utils import split_kwds
from ptf.display import resolver
from ptf.model_data import ArticleData
from ptf.model_data import BookData
from ptf.model_data import BookPartData
from ptf.model_data import CollectionData
from ptf.model_data import ExtLinkDict
from ptf.model_data import Foo
from ptf.model_data import IssueData
from ptf.model_data import JournalData
from ptf.model_data import MathdocPublicationData
from ptf.model_data import PublisherData
from ptf.model_data import RefData
from ptf.model_data import create_contributor
from ptf.model_data import create_extlink


class JatsBase(XmlParserBase):
    def __init__(self, *args, **kwargs):
        super().__init__()
        self.warnings = []
        self.fns = []
        self.tree = None
        # Used to convert an XML value for CKEditor (ie abstract)
        self.add_span_around_tex_formula = False
        # Used to create a Tex file from an XML value (ie abstract)
        self.for_tex_file = False

    def parse_tree(self, tree):
        self.tree = tree
        self.lang = get_normalized_attrib(tree, "lang") or "und"

    def post_parse_tree(self):
        if self.no_bib:
            # For Geodesic
            ext_link = create_extlink()
            ext_link["rel"] = "source"
            ext_link["location"] = "http://www.numdam.org/item/" + self.pid
            ext_link[
                "metadata"
            ] = "NUMDAM"  # Used as the source id to find the source in the GDML Views
            self.ext_links.append(ext_link)

    def parse_node_with_article_title(self, node, **kwargs):
        tex, html = self.parse_inner_node(node, **kwargs)

        is_mixed_citation = kwargs["is_mixed_citation"] if "is_mixed_citation" in kwargs else False
        if is_mixed_citation:
            html = add_span_class_to_html_from_article_title(html, **kwargs)

        return tex, html

    def parse_node_with_break(self, node, **kwargs):
        tex = "\\newline\n" if self.for_tex_file else " "
        html = "<br/>"

        return tex, html

    def parse_node_with_chem_struct_wrap(self, node, **kwargs):
        table_id = label = None
        inner_text = ""

        if "id" in node.attrib:
            table_id = node.attrib["id"]

        for child in node:
            tag = normalize(child.tag)
            if tag == "label":
                _, label = self.parse_node_with_mixed_content(child, **kwargs)
            else:
                _, child_text = self.parse_node_with_mixed_content(child, **kwargs)
                inner_text += child_text

        text = "<table "
        if table_id:
            text += f'id="{table_id}" '
        text += f'class="formula"><tr><td class="formula-inner">{inner_text}</td>'

        text += '<td class="formula-label">'
        if label:
            text += label
        text += "</td></tr>"
        text += "</table>"

        return text, text

    def parse_node_with_disp_quote(self, node, **kwargs):
        tex, html = self.parse_inner_node(node, **kwargs)

        html = f'<div class="disp-quote">{html}</div>'
        tex = f'<div class="disp-quote">{tex}</div>'

        return tex, html

    def parse_node_with_boxed_text(self, node, **kwargs):
        box_id = node.attrib["id"] if "id" in node.attrib else None

        _, node_html = self.parse_inner_node(node, **kwargs)

        if box_id:
            html = f'<div id="{box_id}" class="boxed-text">'
        else:
            html = '<div class="boxed-text">'

        html = f"{html}{node_html}</div>"

        return "", html

    def parse_node_with_fig(self, node, **kwargs):
        """
        Ex: <fig><label>LABEL</label><caption><title>TITLE</title>CAPTION</caption><graphic/></fig>
        becomes: <figure><img><figcaption>LABEL : TITLE<p>CAPTION</p></figcaption></figure>

        :param node: XML node of a fig
        :return: the HTML text + the dict representing the image (mimetype, location,...)
        """
        html = ""

        fig_id = label_html = title_html = caption_html = None
        img_html = ""

        if "id" in node.attrib:
            fig_id = node.attrib["id"]

        for child in node:
            tag = normalize(child.tag)
            if tag == "label":
                _, label_html = self.parse_node_with_mixed_content(child, **kwargs)
            elif tag == "caption":
                for caption_child in child:
                    tag = normalize(caption_child.tag)
                    if tag == "title":
                        _, title_html = self.parse_node_with_mixed_content(caption_child, **kwargs)
                    elif tag == "p":
                        _, caption_p_html = self.parse_node_with_mixed_content(
                            caption_child, **kwargs
                        )
                        if caption_html:
                            caption_html = caption_html.replace(
                                "<p>", '<p class="fig-first-caption">', 1
                            )
                            caption_html += caption_p_html.replace(
                                "<p>", '<p class="fig-small-caption">', 1
                            )
                        else:
                            caption_html = caption_p_html
                    else:
                        self.warnings.append(
                            {
                                self.pid: self.__class__.__name__
                                + "."
                                + inspect.currentframe().f_code.co_name
                                + " "
                                + tag
                            }
                        )

            elif tag == "graphic":
                _, graphic_html = self.parse_node_with_graphic(child, **kwargs)
                img_html += graphic_html
            elif tag == "attrib":
                _, html = self.parse_node_with_mixed_content(child, **kwargs)
                caption_html = f'{caption_html}<p class="fig-small-caption">{html}</p>'
            elif tag == "permissions":
                for gchild in child:
                    if gchild.tag == "copyright-statement":
                        _, html = self.parse_node_with_mixed_content(gchild, **kwargs)
                        caption_html = f'{caption_html}<p class="fig-small-caption">{html}</p>'
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )

        if fig_id:
            html = '<figure id="' + fig_id + '">'
        else:
            html = "<figure>"

        if len(img_html) > 0:
            html += img_html

        if label_html or title_html or (caption_html is not None and len(caption_html) > 0):
            html += "<figcaption>"

            if label_html:
                html += label_html
            if label_html and title_html:
                html += " : "
            if title_html:
                html += title_html
            if caption_html:
                html += caption_html

            html += "</figcaption>"

        html += "</figure>"

        if (
            "append_floats" in kwargs
            and kwargs["append_floats"]
            and hasattr(self, "floats")
            and fig_id is not None
        ):
            self.floats[fig_id] = html

        return "", html

    def parse_node_with_fn(self, node, **kwargs):
        """
        Ex: <fn><label>LABEL</label><p>TEXT</p></fn>

        :param node: XML node of a fn
        :return: ''. the text is stripped from the HTML. but a list of fn is built
        """
        html = fn_html = ""

        label_html = fn_id = None

        if "id" in node.attrib:
            fn_id = node.attrib["id"]

        for child in node:
            tag = normalize(child.tag)
            if tag == "label":
                _, label_html = self.parse_node_with_mixed_content(child, **kwargs)
            elif tag == "p":
                _, fn_html = self.parse_node_with_mixed_content(child, **kwargs)
                fn_html = fn_html.replace("<p>", "").replace("</p>", "")
            else:
                warning = (
                    self.__class__.__name__
                    + "."
                    + inspect.currentframe().f_code.co_name
                    + " "
                    + tag
                )
                self.warnings.append({self.pid: warning})

        if fn_id:
            html = '<p id="' + fn_id + '">'
        else:
            html = "<p>"

        if label_html and ("keep_fn_label" not in kwargs or kwargs["keep_fn_label"]):
            html += f"<sup>{label_html}</sup> "

        html += fn_html + "</p>"

        if not kwargs["keep_fn"] and html not in self.fns:
            self.fns.append(html)

        html = html if kwargs["keep_fn"] else ""
        return "", html

    def parse_node_with_graphic(self, node, **kwargs):
        """
        The href value of graphics used in our XML can have the following values
            - relative path to the issue XML folder  (Elsevier JATS)
            - full path starting with "file:/" (Elsevier JATS created in early 2022)
            - simple file name (with no relative path) in the RVT FullText XML

        After the import, we want
            - the files located in the src/tex/figures article folder
            - the url pointing to the image, built thanks to kwargs['base_url']

        addRelatedObjectPtfCmd will copy the images to the src/tex/figures folder if the location starts with file:/
        => change the location to "file:/..." for Elsevier JATS (the xarticle has a pii attribute)
        """
        href = ""

        for attrib in node.attrib:
            name = normalize(attrib)
            if name == "href":
                href = node.attrib[attrib]

        if href:
            basename = os.path.basename(href)
            ext = basename.split(".")[-1]
            if ext == "png":
                mimetype = "image/png"
            else:
                mimetype = "image/jpeg"

            img_url = "src/tex/figures/" + basename

            if ext in get_elsevier_image_extensions():  # Elsevier uses "jc3" instead of jpg. WTF ?
                img_url = img_url[0 : -len(ext)] + "jpg"

            data_location = href if "file:/" in href else img_url
            if (
                hasattr(self, "pii")
                and hasattr(self, "issue")
                and "file:/" not in href
                and self.from_folder
            ):
                base_dir = self.issue.journal.pid
                if os.path.dirname(href) != base_dir:
                    href = os.path.join(self.from_folder, base_dir, self.issue.pid, href)
                    data_location = "file:" + href

            data = {
                "rel": "html-image",
                "mimetype": mimetype,
                "location": data_location,
                "base": None,
                "metadata": node.text if node.text is not None else "",
            }

            if ext == "png":
                img_url = os.path.join(kwargs["base_url"], "png", img_url)
            else:
                img_url = os.path.join(kwargs["base_url"], "jpg", img_url)
            img_text = '<a href="' + img_url + '" data-lightbox="image-'
            img_text += str(len(self.figures)) + '" title="">'
            img_text += '<img src="' + img_url + '" class="article-body-img" />'
            img_text += "</a>"

            if data not in self.figures:
                self.figures.append(data)
                self.related_objects.append(data)

        return "", img_text

    def parse_node_with_inline_formula(self, node, **kwargs):
        # MathJAX is doing a good job with formulae and is now the standard
        # MathML could be ignored in HTML (the original XML value is preserved with value_xml)
        # We could simply return the tex-math text
        # But there are multiple errors in the TeX of the Mersenne articles.
        # We first need to fix those mistakes before switching to TeX

        tex_math = ""
        math_text = ""
        formula_id = label = None

        if "id" in node.attrib:
            formula_id = node.attrib["id"]

        for child in node:
            tag = normalize(child.tag)
            if tag == "alternatives":
                for alternative in child:
                    tag = normalize(alternative.tag)
                    if tag == "tex-math":
                        tex_math = alternative.text or ""
                    elif tag == "math":
                        # remove_namespace(child)
                        # Elsevier sometimes provide the formula a an alternative image. Remove it.
                        alternative.attrib.pop("altimg", None)

                        math_text = get_xml_from_node(alternative).replace("mml:", "")
                        math_text = math_text.replace(
                            'xmlns:xlink="http://www.w3.org/1999/xlink"', ""
                        )
                        math_text = math_text.replace(
                            'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"', ""
                        )
                        if node.tag == "disp-formula":
                            math_text = math_text.replace("<math", '<math display="block"')
            elif tag == "label":
                label = child.text or ""
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )

        if (math_text == "" and tex_math != "") or (math_text != "" and tex_math == ""):
            stack = inspect.stack()
            stack_str = " ".join(
                [
                    frameinfo[3]
                    for frameinfo in stack[1:]
                    if frameinfo[3].find("parse_") == 0
                    and frameinfo[3].find("parse_node") == -1
                    and frameinfo[3].find("parse_inner") == -1
                    and frameinfo[3].find("parse_tree") == -1
                    and frameinfo[3].find("parse_article_meta") == -1
                ]
            )
            print(f"{self.pid} no math formula for {stack_str}")
            # raise ValueError("No formula alternative")

        if node.tag != "disp-formula":
            if tex_math != "" and tex_math[0] != "$":
                tex_math = "$" + tex_math
            if tex_math != "" and tex_math[-1] != "$":
                tex_math = tex_math + "$"

        tex = tex_math

        html = ""
        if label or node.tag == "disp-formula":
            html += '<table class="formula"><tr><td class="formula-inner">'

        html += '<span class="mathjax-formula" '
        if formula_id:
            html += 'id="' + formula_id + '" '
        alt_text = tex_math.replace("\n", "") if node.tag == "disp-formula" else tex_math
        if math_text:
            html += f'data-tex="{alt_text}">{math_text}</span>'
        else:
            html += f'data-tex="{alt_text}">{tex_math}</span>'

        if label or node.tag == "disp-formula":
            html += '</td><td class="formula-label">'
            if label:
                html += label
            html += "</td></tr>"
            html += "</table>"

        if self.add_span_around_tex_formula:
            tex = f'<span class="mathjax-formula">\\({tex[1:-1]}\\)</span>'

        return tex, html

    def parse_node_with_institution_id(self, node, **kwargs):
        return "", ""

    def parse_node_with_italic(self, node, **kwargs):
        tex, html = self.parse_inner_node(node, **kwargs)

        # is_mixed_citation = kwargs['is_mixed_citation'] if 'is_mixed_citation' in kwargs else False
        # is_citation = kwargs['is_citation'] if 'is_citation' in kwargs else False
        # is_comment = kwargs['is_comment'] if 'is_comment' in kwargs else False
        #
        # if inner_text == '' or kwargs['temp_tex'] or (is_citation and not is_mixed_citation and not is_comment):
        #     text = inner_text
        # else:
        #     text = '<span class="italique">' + inner_text + '</span>'

        html = f'<span class="italique">{html}</span>'

        if self.for_tex_file:
            tex = "{\\it " + tex + "}"
        else:
            tex = f"<i>{tex}</i>"

        return tex, html

    def parse_node_with_list(self, node, **kwargs):
        tex, html = self.parse_inner_node(node, **kwargs)

        start = None
        continued_from = node.get("continued-from")
        if continued_from is not None:
            start = self.get_list_start_value(node) + 1

        list_type = node.get("list-type")
        if list_type == "bullet" or list_type == "simple":
            if self.for_tex_file:
                tex = "\n\\begin{itemize}\n" + tex + "\\end{itemize}\n"
            else:
                tex = f"<ul>{tex}</ul>"

            html = f"<ul>{html}</ul>"
        else:
            if self.for_tex_file:
                tex = "\n\\begin{enumerate}\n" + tex + "\\end{enumerate}\n"
            else:
                if list_type == "order" or list_type == "number":
                    if start is not None:
                        html = f'<ol type="1" start="{str(start)}">{html}</ol>'
                        tex = f'<ol type="1" start="{str(start)}">{tex}</ol>'
                    else:
                        html = f'<ol type="1">{html}</ol>'
                        tex = f'<ol type="1">{tex}</ol>'
                elif list_type == "alpha-lower":
                    html = f'<ol type="a">{html}</ol>'
                    tex = f'<ol type="a">{tex}</ol>'
                elif list_type == "alpha-upper":
                    html = f'<ol type="A">{html}</ol>'
                    tex = f'<ol type="A">{tex}</ol>'
                elif list_type == "roman-lower":
                    html = f'<ol type="i">{html}</ol>'
                    tex = f'<ol type="i">{tex}</ol>'
                elif list_type == "roman-upper":
                    html = f'<ol type="I">{html}</ol>'
                    tex = f'<ol type="I">{tex}</ol>'
                else:
                    html = f'<ul class="no-bullet" style="list-style-type:none;">{html}</ul>'
                    tex = f'<ul class="no-bullet" style="list-style-type:none;">{tex}</ul>'

        return tex, html

    def parse_node_with_list_item(self, node, **kwargs):
        """
        <list-item><label>LABEL</label><p>TEXT</p> becomes
        <li>LABEL TEXT</li>
        (same with <title>)

        :param node:
        :return:
        """

        title_tex = (
            title_html
        ) = label_tex = label_html = p_tex = p_html = content_tex = content_html = ""

        for child in node:
            tag = normalize(child.tag)
            if tag == "label":
                label_tex, label_html = self.parse_node_with_mixed_content(child, **kwargs)
            elif tag == "title":
                title_tex, title_html = self.parse_node_with_mixed_content(child, **kwargs)
            elif tag == "p":
                if p_html == "" and content_html == "":
                    p_tex, p_html = self.parse_inner_node(child, **kwargs)
                else:
                    content_tex, content_html = self.parse_inner_node(child, **kwargs)
                    content_html = f"<p>{content_html}</p>"
            elif tag == "list":
                content_tex, content_html = self.parse_node_with_mixed_content(child, **kwargs)
            # TODO if tag == "def-list":
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )

        inner_tex = ""
        if label_tex:
            inner_tex += label_tex + " "
        if title_tex:
            inner_tex += title_tex + " "
        inner_tex += p_tex + content_tex

        if self.for_tex_file:
            tex = "\\item " + inner_tex + "\n"
        else:
            tex = f"<li>{inner_tex}</li>"

        html = "<li>"
        if label_html:
            html += label_html + " "
        if title_html:
            html += title_html + " "
        html += p_html + content_html + "</li>"

        return tex, html

    def parse_node_with_name_content(self, node, **kwargs):
        tex, html = self.parse_inner_node(node, **kwargs)
        return tex, html

    def parse_node_with_p(self, node, **kwargs):
        tex, html = self.parse_inner_node(node, **kwargs)

        if not self.for_tex_file:
            tex = f"<p>{tex}</p>"

        node_type = node.get("specific-use")
        if node_type:
            html = f'<p class="{node_type}">{html}</p>'
        else:
            html = f"<p>{html}</p>"

        if hasattr(self, "floats_to_insert") and hasattr(self, "floats"):
            while len(self.floats_to_insert) > 0:
                float_id = self.floats_to_insert.pop(0)
                if float_id in self.floats:
                    html += self.floats[float_id]
                    self.floats.pop(float_id)

        return tex, html

    def parse_node_with_h1(self, node, **kwargs):
        tex, html = self.parse_inner_node(node, **kwargs)

        if not self.for_tex_file:
            tex = f"<h1>{tex}</h1>"

        node_type = node.get("specific-use")
        if node_type:
            html = f'<h1 class="{node_type}">{html}</h1>'
        else:
            html = f"<h1>{html}</h1>"

        if hasattr(self, "floats_to_insert") and hasattr(self, "floats"):
            while len(self.floats_to_insert) > 0:
                float_id = self.floats_to_insert.pop(0)
                if float_id in self.floats:
                    html += self.floats[float_id]
                    self.floats.pop(float_id)

        return tex, html

    def parse_node_with_sc(self, node, **kwargs):
        tex, html = self.parse_inner_node(node, **kwargs)
        html = f'<span class="smallcaps">{html}</span>'

        return tex, html

    def parse_node_with_sec(self, node, **kwargs):
        """
        <sec><title>TITLE</title><p>TEXT</p> becomes
        <section><h@i>TITLE</h@i><p>TEXT</p>   (i is the current level and is increased for children)

        :param node:
        :param kwargs:
        :return:
        """

        label_tex = label_html = title_tex = title_html = None
        sec_level = kwargs["sec_level"] = kwargs["sec_level"] if "sec_level" in kwargs else 2

        inner_tex = inner_html = ""
        kwargs["sec_level"] += 1

        for child in node:
            tag = normalize(child.tag)
            if tag == "label":
                label_tex, label_html = self.parse_node_with_mixed_content(child)
            elif tag == "title":
                title_tex, title_html = self.parse_node_with_mixed_content(child)
            else:
                child_tex, child_html = self.parse_node_with_mixed_content(child, **kwargs)
                inner_tex += child_tex
                inner_html += child_html

        tex = ""
        html = "<section>"

        if label_html or title_html:
            html += f"<h{str(sec_level)}>"
            if label_html:
                tex += label_tex
                html += label_html
            if label_html and title_html:
                tex += " "
                html += " "
            if title_html:
                tex += title_tex
                html += title_html
            html += f"</h{str(sec_level)}>"

        tex += inner_tex
        html += inner_html + "</section>"

        return tex, html

    def parse_node_with_string_name(self, node, **kwargs):
        tex, html = self.parse_inner_node(node, **kwargs)

        is_mixed_citation = kwargs["is_mixed_citation"] if "is_mixed_citation" in kwargs else False
        if is_mixed_citation:
            html = add_span_class_to_html_from_authors(html.title(), **kwargs)

        return tex, html

    def parse_node_with_strong(self, node, **kwargs):
        tex, html = self.parse_inner_node(node, **kwargs)

        if self.for_tex_file:
            tex = "{\\bf " + tex + "}"
        else:
            tex = f"<strong>{tex}</strong>"
        html = f"<strong>{html}</strong>"

        return tex, html

    def parse_node_with_styled_content(self, node, **kwargs):
        tex, html = self.parse_inner_node(node, **kwargs)

        if "style" in node.attrib:
            style = node.attrib["style"]
            if style != "":
                html = f'<span style="{style}">{html}</span>'

        return tex, html

    def parse_node_with_sub(self, node, **kwargs):
        tex, html = self.parse_inner_node(node, **kwargs)

        if self.for_tex_file:
            tex = "\\textsubscript{" + tex + "}"
        else:
            tex = f"<sub>{tex}</sub>"
        html = f"<sub>{html}</sub>"

        return tex, html

    def parse_node_with_sup(self, node, **kwargs):
        tex, html = self.parse_inner_node(node, **kwargs)

        if self.for_tex_file:
            tex = "\\textsuperscript{" + tex + "}"
        else:
            tex = f"<sup>{tex}</sup>"
        html = f"<sup>{html}</sup>"

        return tex, html

    def parse_node_with_table_generic(self, node, **kwargs):
        tex, html = self.parse_inner_node(node, **kwargs)

        tag = normalize(node.tag)
        if tag == "row":
            tag = "tr"
        elif tag == "entry":
            tag = "td"
        open_tag = "<" + tag

        if tag == "table":
            class_table = "table"

            cols = node.xpath("colgroup/col")
            i = 1
            for col in cols:
                if "width" in col.attrib:
                    class_table += f" nowrap-col-{i}"
                i += 1

            open_tag += f' class="{class_table}"'
        if "rowspan" in node.attrib:
            open_tag += ' rowspan="' + node.attrib["rowspan"] + '"'
        if "colspan" in node.attrib:
            open_tag += ' colspan="' + node.attrib["colspan"] + '"'
        if "align" in node.attrib:
            open_tag += ' align="' + node.attrib["align"] + '"'
        if "valign" in node.attrib:
            open_tag += ' class="td-valign-' + node.attrib["valign"] + '"'
        if "style" in node.attrib:
            open_tag += ' style="' + node.attrib["style"] + '"'
        open_tag += ">"

        html = f"{open_tag}{html}</{tag}>"

        return "", html

    def parse_node_with_table_wrap(self, node, **kwargs):
        """
        Create a <div class="table-wrap"> around the table
        :param node:
        :return:
        """

        table_id = label = caption = None
        inner_text = ""

        if "id" in node.attrib:
            table_id = node.attrib["id"]

        for child in node:
            tag = normalize(child.tag)
            if tag == "label":
                _, label = self.parse_node_with_mixed_content(child, **kwargs)
            elif tag == "caption":
                _, caption = self.parse_node_with_mixed_content(child, **kwargs)
            else:
                _, child_text = self.parse_node_with_mixed_content(child, **kwargs)
                inner_text += child_text

        if table_id:
            text = '<div class="table-wrap table-responsive" id="' + table_id + '">'
        else:
            text = '<div class="table-wrap table-responsive">'

        if label or caption:
            text += '<div class="table-wrap-header">'

        if label:
            text += "<strong>" + label + "</strong>"

        if caption:
            if label:
                text += " "
            if caption:
                text += caption

        if label or caption:
            text += "</div>"

        text += inner_text
        text += "</div>"

        if (
            "append_floats" in kwargs
            and kwargs["append_floats"]
            and hasattr(self, "floats")
            and table_id is not None
        ):
            self.floats[table_id] = text

        return "", text

    def parse_node_with_table_wrap_foot(self, node, **kwargs):
        """
        Create a <div class="table-wrap-foot"> at bottom of the table
        Keep the footnotes inside this div
        :param node:
        :return:
        """

        text = '<div class="table-wrap-foot">'
        kwargs["keep_fn"] = True

        for child in node:
            tag = normalize(child.tag)
            if tag == "fn-group":
                _, html = self.parse_node_with_mixed_content(child, **kwargs)
                text += html

        text += "</div>"

        return "", text

    def parse_node_with_toc(self, node, **kwargs):
        tex, html = self.parse_inner_node(node, **kwargs)

        html = f"<table>{html}</table>"

        # text = '<ul class="no-bullet book-toc">'
        # text += inner_text + '</ul>'

        return "", html

    def parse_node_with_toc_entry(self, node, **kwargs):
        html = label = title = child_text = page = anchor = ""
        inside_toc_entry = "inside_toc_entry" in kwargs and kwargs["inside_toc_entry"]
        toc_class = "inside-toc" if inside_toc_entry else ""
        # # toc-entry may be embedded inside toc-entry: create a wrapping <ul>
        # html = '<tr class="inside-toc">'
        # #html = '<ul class="no-bullet book-toc">'

        for child in node:
            tag = normalize(child.tag)
            if tag == "title":
                _, title = self.parse_node_with_mixed_content(child, **kwargs)
            elif tag == "label":
                _, label = self.parse_node_with_mixed_content(child, **kwargs)
            elif tag == "nav-pointer":
                _, page = self.parse_node_with_mixed_content(child, **kwargs)
            elif tag == "nav-pointer-group":
                for grandchild in child:
                    if (
                        grandchild.tag == "nav-pointer"
                        and "specific-use" in grandchild.attrib
                        and grandchild.attrib["specific-use"] == "pagenum"
                    ):
                        _, page = self.parse_node_with_mixed_content(grandchild, **kwargs)
                    if (
                        grandchild.tag == "nav-pointer"
                        and "specific-use" in grandchild.attrib
                        and grandchild.attrib["specific-use"] == "pageindex"
                    ):
                        anchor = int(grandchild.text) + 1
            elif tag == "toc-entry":
                _, text = self.parse_node_with_mixed_content(child, inside_toc_entry=True)
                child_text += text

        toc_text = f"{label} {title}"
        page_text = f"p. {page}"

        if anchor:
            href = reverse("item-pdf", kwargs={"pid": self.pid, "extension": "pdf"})
            href += f"#page={anchor}"
            toc_text = f'<a href="{href}">{toc_text}</a>'
            page_text = f'<a href="{href}">{page_text}</a>'

        html += f'<tr><td class="{toc_class}">{toc_text}</td><td class="toc-page">{page_text}</td></tr>'
        if len(child_text) > 0:
            html += child_text
        # html += f'<li>{title} <span> p. {page}</span>{child_text}</li>'

        # if 'inside_toc_entry' in kwargs and kwargs['inside_toc_entry']:
        #     html += '</tr>'
        #     #html += '</ul>'

        return "", html

    def parse_node_with_underline(self, node, **kwargs):
        tex, html = self.parse_inner_node(node, **kwargs)
        tex = f"<u>{tex}</u>"
        html = f"<u>{html}</u>"

        return tex, html

    def parse_node_with_volume(self, node, **kwargs):
        tex, html = self.parse_inner_node(node, **kwargs)

        is_mixed_citation = kwargs["is_mixed_citation"] if "is_mixed_citation" in kwargs else False
        if is_mixed_citation:
            html = add_span_class_to_html_from_volume(html, **kwargs)

        return tex, html

    def parse_node_with_xref(self, node, **kwargs):
        tex = html = ""

        if "ignore_xref" in kwargs and kwargs["ignore_xref"]:
            return tex, html

        xref_id = node.get("rid")
        if xref_id:
            rids = xref_id.split()

            tex, html = self.parse_inner_node(node, **kwargs)
            rid0 = rids[0]
            if rid0.find("bib") == 0:
                rid0 = "r" + rid0[3:]
            html = f'<a href="#{rid0}">{html}</a>'

            for rid in rids:
                ref_type = node.get("ref-type") or None
                if ref_type in ["fig", "table", "textbox"] and hasattr(self, "floats_to_insert"):
                    self.floats_to_insert.append(rid)

        return tex, html

    def parse_inner_node(self, node, **kwargs):
        """
        Used by html_from_mixed_content for nodes that have a different tag in HTML
        :param node:
        :param kwargs:
        :return:
        """
        tex = html = ""
        kwargs["is_top"] = False
        kwargs["is_body_html"] = kwargs["is_body_html"] if "is_body_html" in kwargs else False

        if node.text:
            node_text = node.text
            if self.for_tex_file:
                node_text = unicode_to_latex(node_text)
            tex = node_text
            html = escape(node.text)

        for child in node:
            child_tex, child_html = self.parse_node_with_mixed_content(child, **kwargs)
            tex += child_tex
            html += child_html

        return tex, html

    def parse_node_with_mixed_content(self, node, **kwargs):
        """
        Parse and return the HTML text of an XML node which mixes text and XML sub-nodes.
        Ex: <node>text1 <a>text_a</a> text2 <b>text_b</b>b_tail</node>
        Some inner nodes are removed, others are kept or replaced by their HTML equivalent.
        html_from_mixed_content is called recursively to get the HTML text of the children.

        :param node: XML Node
        :param kwargs: params of the function
        :return: HTML text
        """

        if node is None:
            return "", ""

        # The tail is the text following the end of the node
        # Ex: <node>text1<a>text_a</a>a_tail</node>
        # The HTML text has to include the tail
        #   only if html_from_mixed_content was called recursively
        kwargs["is_top"] = kwargs["is_top"] if "is_top" in kwargs else True

        # sec_level is used to add <h1>, <h2>,... in the HTML text while parsing nodes like <sec>
        kwargs["sec_level"] = kwargs["sec_level"] if "sec_level" in kwargs else 2

        # Text in <comment> is parsed to add HTML link.
        kwargs["add_HTML_link"] = kwargs["add_HTML_link"] if "add_HTML_link" in kwargs else False

        # base_url to image links
        kwargs["base_url"] = kwargs["base_url"] if "base_url" in kwargs else ""

        # footnotes are removed from the fulltext (and put at the end) except for those in a table
        kwargs["keep_fn"] = kwargs["keep_fn"] if "keep_fn" in kwargs else False

        kwargs["is_citation"] = kwargs["is_citation"] if "is_citation" in kwargs else False
        kwargs["is_comment"] = kwargs["is_comment"] if "is_comment" in kwargs else False
        # mixed-citation ignores ext-link
        kwargs["add_ext_link"] = kwargs["add_ext_link"] if "add_ext_link" in kwargs else False

        # TODO remove once jats_parser has been validated agains xmldata
        kwargs["temp_math"] = kwargs["temp_math"] if "temp_math" in kwargs else False
        kwargs["temp_tex"] = kwargs["temp_tex"] if "temp_tex" in kwargs else False
        kwargs["is_mixed_citation"] = (
            kwargs["is_mixed_citation"] if "is_mixed_citation" in kwargs else False
        )
        kwargs["is_body_html"] = kwargs["is_body_html"] if "is_body_html" in kwargs else False

        tag = normalize(node.tag)

        # pub-id/object-id are ignored by default are they are treated separately
        if not (kwargs["is_comment"]) and tag in ("pub-id", "object-id"):
            return "", ""

        if tag in ("mixed-citation", "toc"):
            kwargs["is_citation"] = True
        elif tag == "comment":
            kwargs["is_comment"] = True

        tex = html = inner_tex = inner_html = ""

        # I. Add the node's text.
        # Some tag have a corresponding parse_node_with_@tag function to generate the HTML text.

        # Check if the parse_node_with_@tag exists
        tag_mapped = {
            "statement": "sec",
            "disp-formula": "inline-formula",
            "chapter-title": "article-title",
            "bold": "strong",
            "table": "table-generic",
            "th": "table-generic",
            "tr": "table-generic",
            "td": "table-generic",
            "thead": "table-generic",
            "tbody": "table-generic",
            "colgroup": "table-generic",
            "col": "table-generic",
            "tgroup": "table-generic",
            "entry": "table-generic",
            "row": "table-generic",
        }

        fct_name = tag_mapped[tag] if tag in tag_mapped else tag
        fct_name = "parse_node_with_" + fct_name.replace("-", "_")
        ftor = getattr(self, fct_name, None)
        if callable(ftor):
            inner_tex, inner_html = ftor(node, **kwargs)
        elif tag in ("ext-link", "uri"):
            # Add HTML links
            inner_tex = inner_html = self.helper_add_link_from_node(node, **kwargs)
            # Update self.ext_links. Useful for <ext-link> deep in a <mixed_citation>,
            # and not caught by parse_citation_node
            if tag == "ext-link" and not kwargs["is_comment"] and kwargs["add_ext_link"]:
                is_extid_value = self.parse_ext_link(node, **kwargs)
                if is_extid_value and kwargs["is_mixed_citation"]:
                    # an extid has been found in a mixed_citation, no need to add the text of the id here
                    inner_tex = inner_html = ""
        elif tag == "supplementary-material":
            self.parse_supplementary_material(node, **kwargs)
        else:
            # II.1. Add the node text (before the children text)
            if node.text is not None:
                node_text = node.text
                if self.for_tex_file:
                    node_text = unicode_to_latex(node_text)
                inner_tex += node_text
                inner_html += escape(node.text)

            # II.2. children
            # child_text = html_from_mixed_content(child, params)

            child_kwargs = kwargs.copy()
            child_kwargs["is_top"] = False

            for child in node:
                child_tex, child_html = self.parse_node_with_mixed_content(child, **child_kwargs)

                # Case where an ext-link has been removed in a mixed-citation
                # We may have "title. , (year)"
                # Remove the comma that is now useless
                if (
                    kwargs["is_mixed_citation"]
                    and child_html
                    and child_html[0] in [",", "."]
                    and inner_html[-2:] == ". "
                ):
                    inner_html = inner_html[0:-1]
                    child_html = child_html[1:]
                    inner_tex = inner_tex[0:-1]
                    child_tex = child_tex[1:]

                inner_tex += child_tex
                inner_html += child_html

            # II.3. wrap the children text with html links
            if kwargs["add_HTML_link"] and node.text:
                match = re.match(r"[\n ]+", node.text)
                if not match:
                    inner_html = make_links_clickable(node.text, inner_html)

        tex += inner_tex
        html += inner_html

        # III. Add the node's tail for children
        if node.tail and not kwargs["is_top"]:
            node_tail = node.tail
            if self.for_tex_file:
                node_tail = unicode_to_latex(node_tail)
            tex += node_tail
            html += escape(node.tail)

        return tex, html

    def parse_abstract(self, node, **kwargs):
        # tag = get_normalized_attrib(node, "abstract-node_type") or "abstract"
        tag = get_normalized_attrib(node, "abstract-type") or "abstract"
        if tag == "author":
            tag = "abstract"
        lang = get_normalized_attrib(node, "lang") or self.lang
        value_tex, value_html = self.parse_node_with_mixed_content(node)
        value_xml = get_xml_from_node(node)
        self.abstracts.append(
            {
                "tag": tag,
                "lang": lang,
                "value_xml": value_xml,
                "value_html": value_html,
                "value_tex": value_tex,
            }
        )

    def parse_aff_alternatives(self, node, **kwargs):
        xref_id = get_normalized_attrib(node, "id") or ""
        address = ""
        aff_to_all = True

        for child in node:
            tag = normalize(child.tag)

            if tag == "aff":
                # Skip the formatted aff and use only the complete address text
                # TODO support <aff> properly
                for aff in child:
                    if aff.tag == "label" and address == "":
                        label = get_text_from_node(aff)
                        address = get_text_from_node(child)[len(label) :]
                        aff_to_all = False
                if address == "" and child.text:
                    address = child.text
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )

        if address != "":
            for contrib in self.contributors:
                if address not in contrib["addresses"] and (
                    ("xrefs" in contrib and xref_id in contrib["xrefs"]) or aff_to_all
                ):
                    contrib["addresses"].append(address)
                    contrib["contrib_xml"] = get_contrib_xml(contrib)

    def parse_award_group(self, node, **kwargs):
        abbrev = award_id = None

        for child in node:
            tag = normalize(child.tag)

            if tag == "award-id":
                award_id = child.text
            elif tag == "funding-source":
                abbrev = get_text_from_node(child)
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )

        if abbrev is not None and award_id is not None:
            self.awards.append({"abbrev": abbrev, "award_id": award_id})

    def parse_contrib_group(self, node, **kwargs):
        role = node.get("content-type") or ""
        if role and role[-1] == "s":
            role = role[0:-1]

        for child in node:
            tag = normalize(child.tag)

            if tag == "contrib":
                contrib = self.get_data_from_contrib(child)
                contrib["role"] = f"{role}|{contrib['role']}" if contrib["role"] else role
                contrib["contrib_xml"] = get_xml_from_node(child)
                self.contributors.append(contrib)
            elif tag == "aff-alternatives":
                self.parse_aff_alternatives(child)
            elif tag == "fn":
                _, html = self.parse_node_with_fn(child, keep_fn=True, keep_fn_label=False)
                xml = get_xml_from_node(child)
                self.footnotes_xml += xml
                self.footnotes_html += html
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )

    def parse_counts(self, node, **kwargs):
        for child in node:
            count_value = child.get("count")
            if count_value is None:
                count_value = child.text

            if count_value is not None:
                tag = normalize(child.tag)
                if tag == "book-page-count":
                    tag = "page-count"

                self.counts.append((tag, count_value))

    def parse_ext_link(self, node, **kwargs):
        datas = self.get_data_from_ext_link(node)
        extid_value = self.add_extids_from_node_with_link(datas)

        add_ext_link = kwargs["add_ext_link"] if "add_ext_link" in kwargs else False
        if (
            add_ext_link
            and extid_value[0] is None
            and datas not in self.ext_links
            and datas["rel"] != "cover"
        ):
            self.ext_links.append(datas)

        return extid_value[0] is not None

    def parse_front_matter(self, node, **kwargs):
        self.frontmatter_xml = get_xml_from_node(node)
        self.frontmatter_foreword_html = ""

        for child in node:
            tag = normalize(child.tag)

            if tag == "foreword":
                _, self.frontmatter_foreword_html = self.parse_node_with_mixed_content(child)
            elif tag == "toc":
                _, self.frontmatter_toc_html = self.parse_node_with_mixed_content(child)

    def parse_id(self, node, **kwargs):
        node_id = node.text
        if "pub-id-type" in node.attrib:
            node_type = node.attrib["pub-id-type"]
        elif "book-id-type" in node.attrib:
            node_type = node.attrib["book-id-type"]
        elif "book-part-id-type" in node.attrib:
            node_type = node.attrib["book-part-id-type"]
        else:
            node_type = ""

        if node_type == "pii":
            # Elsevier ids get a special treatment: web scrapping to find the date_published
            if self.pid and len(self.pid) > 2 and self.pid[0:2] == "CR":
                self.pii = node_id
        elif node_type in ("numdam-id", "mathdoc-id"):
            self.pid = node_id
        elif node_type == "ark":
            self.extids.append((node_type, node_id))
        elif node_type in ("doi", "eid"):
            self.ids.append((node_type, node_id))
            if node_type == "doi":
                self.doi = node_id

    def parse_kwd_group(self, node, **kwargs):
        kwds = []
        value_html = value_tex = ""
        for child in node:
            tag = normalize(child.tag)

            if tag == "kwd":
                value_tex, value_html = self.parse_node_with_mixed_content(child)
                kwds.append(value_tex)
            elif tag == "unstructured-kwd-group":
                # value_xml = get_xml_from_node(child)
                value_tex, value_html = self.parse_node_with_mixed_content(child)
                kwds = split_kwds(value_tex)
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )

        content_type = node.get("content-node_type") or ""
        if content_type == "":
            content_type = node.get("kwd-group-type") or ""
        lang = get_normalized_attrib(node, "lang") or self.lang

        self.kwds.extend([{"type": content_type, "lang": lang, "value": kwd} for kwd in kwds])

    def parse_ref_list(self, node, **kwargs):
        for child in node:
            tag = normalize(child.tag)

            if tag == "ref":
                ref = JatsRef(tree=child, lang=self.lang)
                self.warnings.extend(ref.warnings)
                self.bibitems.append(ref)
                self.bibitem.append(ref.citation_html)
            elif tag == "p":
                # Elsevier can store supplementary-material inside ref-list / p
                self.parse_node_with_mixed_content(child)
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )

    def parse_related_article(self, node, **kwargs):
        rel_type = get_normalized_attrib(node, "related-article-type") or ""
        id_value = node.text

        if hasattr(self, "pii") and id_value and id_value.find("10.") == -1 and id_value != "NONE":
            # a pii is used instead of a DOI
            # Call Elsevier to get the doi
            doi = scrapping.fetch_article(self.doi, id_value, pii_doi_equivalence=True)
            id_value = doi

        obj = Foo()
        obj.rel_type = rel_type
        obj.id_value = id_value

        self.relations.append(obj)

    def parse_related_object(self, node, **kwargs):
        node_type = node.get("content-type") or ""
        rel = node.get("link-type") or ""
        href = get_normalized_attrib(node, "href") or ""
        base = get_normalized_attrib(node, "base") or ""
        text = get_xml_from_node(node)

        data = {
            "rel": rel,
            "mimetype": node_type,
            "location": href,
            "base": base,
            "metadata": text,
        }

        document_id_type = node.get("document-id-type") or ""
        if document_id_type:
            id_value = node.get("document-id") or ""
            if id_value != "NONE":
                if id_value and id_value.find("10.") == -1:
                    # a pii is used instead of a DOI
                    # Call Elsevier to get the doi
                    doi = scrapping.fetch_article(self.doi, id_value, pii_doi_equivalence=True)
                    id_value = doi

                obj = Foo()
                obj.rel_type = "refers to"
                obj.id_value = id_value

                self.relations.append(obj)
        else:
            self.related_objects.append(data)

    def parse_sec(self, node, **kwargs):
        for child in node:
            tag = normalize(child.tag)

            if tag == "title":
                pass
            elif tag == "ref-list":
                self.parse_ref_list(child)
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )

    def parse_self_uri(self, node, **kwargs):
        node_type = node.get("content-type") or "text/html"
        href = get_normalized_attrib(node, "href") or ""
        base = get_normalized_attrib(node, "base") or ""

        # The XML of the Elsevier archive do not declare the PDF location like the other Mathdoc collections:
        # The collection folder is missing: add it back
        if hasattr(self, "pii") and hasattr(self, "issue"):
            base_dir = self.issue.journal.pid
            if os.path.dirname(href) != base_dir:
                href = os.path.join(base_dir, self.issue.pid, href)

        if self.no_bib:
            href = "http://www.numdam.org/item/" + os.path.basename(href)

        data = {
            "rel": "full-text",
            "mimetype": node_type,
            "location": href,
            "base": base,
            "text": normalize_space(node.text) if node.text is not None else "",
        }

        # Ext-links, Related-objects used metadata instead of text. Strange difference ?
        # xml_cmds ignore "application/xml" in add_objects_with_location: they are ignored here.
        if node_type != "application/xml":
            self.streams.append(data)

    def parse_sub_article(self, node, **kwargs):
        # Used for translations
        trans_article = JatsArticle(tree=node)
        self.translations.append(trans_article)

    def parse_subj_group(self, node, **kwargs):
        lang = get_normalized_attrib(node, "lang") or self.lang
        type_ = node.get("subj-group-type") or ""

        for child in node:
            tag = normalize(child.tag)

            if tag == "subject":
                self.subjs.append(
                    {"type": type_, "lang": lang, "value": get_text_from_node(child)}
                )
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )

    def parse_supplementary_material(self, node, **kwargs):
        caption = ""
        for child in node:
            if child.tag == "caption":
                _, caption = self.parse_node_with_mixed_content(child)

        location = get_normalized_attrib(node, "href") or None
        if location is None:
            location = get_normalized_attrib(node, "id") or ""

        mimetype = node.attrib.get("mimetype") or None
        if mimetype is None:
            mimetype = resolver.get_mimetype(location)

        material = {
            "rel": node.attrib.get("content-type") or "supplementary-material",
            "mimetype": mimetype,
            "location": location,
            "base": "",
            "metadata": "",
            "caption": caption if caption else "",
        }
        base_location = os.path.basename(location)
        found_list = [
            item
            for item in self.supplementary_materials
            if os.path.basename(item["location"]) == base_location
        ]
        if len(found_list) == 0:
            self.supplementary_materials.append(material)

    def parse_title(self, node, **kwargs):
        self.title_tex, self.title_html = self.parse_node_with_mixed_content(
            node, ignore_xref=True
        )
        # In xmldata.py, title_xml had the <title_group> tag:
        # self.title_xml can't be set in parse_title

    def parse_title_group(self, node, **kwargs):
        has_fn_group = False

        for child in node:
            tag = normalize(child.tag)

            if tag in ("title", "journal-title", "article-title", "book-title", "issue-title"):
                self.parse_title(child)
            elif tag == "subtitle":
                title_tex, title_html = self.parse_node_with_mixed_content(child)
                self.title_tex += " " + title_tex
                self.title_html += " " + title_html
            elif tag == "trans-title-group":
                self.parse_trans_title_group(child)
            elif tag == "abbrev-title":
                _, self.abbrev = self.parse_node_with_mixed_content(child)
            elif tag == "fn-group":
                has_fn_group = True
                for fn_node in child:
                    if fn_node.tag == "fn":
                        _, html = self.parse_node_with_fn(
                            fn_node, keep_fn=True, keep_fn_label=False
                        )
                        xml = get_xml_from_node(fn_node)
                        self.footnotes_xml += xml
                        self.footnotes_html += html
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )

        if has_fn_group:
            # fn-group is now a funding statement and will be exported separately in the XML:
            # => remove it from the title-group
            new_node = etree.Element("title-group")
            for child in node:
                tag = normalize(child.tag)
                if tag != "fn-group":
                    new_node.append(copy.deepcopy(child))
            self.title_xml = get_xml_from_node(new_node)
        else:
            self.title_xml = get_xml_from_node(node)

    def parse_trans_abstract(self, node, **kwargs):
        tag = get_normalized_attrib(node, "abstract-type") or "abstract"
        if tag == "author":
            tag = "abstract"
        lang = get_normalized_attrib(node, "lang") or "und"
        value_tex, value_html = self.parse_node_with_mixed_content(node)
        value_xml = get_xml_from_node(node)
        self.abstracts.append(
            {
                "tag": tag,
                "lang": lang,
                "value_xml": value_xml,
                "value_html": value_html,
                "value_tex": value_tex,
            }
        )

    def parse_trans_title(self, node, **kwargs):
        self.trans_title_tex, self.trans_title_html = self.parse_node_with_mixed_content(node)
        self.trans_title_xml = get_xml_from_node(node)

    def parse_trans_title_group(self, node, **kwargs):
        for child in node:
            tag = normalize(child.tag)

            if tag == "trans-title":
                self.parse_trans_title(child)
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )

        self.trans_lang = get_normalized_attrib(node, "lang") or "und"

    def get_data_from_contrib(self, node):
        """
        <contrib> creates 1 person, defined in <name>, <string-name> or <name-alternatives>
        In a <mixed-citation>, each <name> creates 1 person: we can't use the same code
        :param node:
        :return:
        """

        params = create_contributor()

        for child in node:
            if child.tag == "name":
                self.update_data_from_name(child, params)
            elif child.tag == "string-name":
                self.update_data_from_name(child, params)
                if params["first_name"] == "" and params["last_name"] == "":
                    params["string_name"] = child.text or ""
            elif child.tag == "name-alternatives":
                params["mid"] = self.get_data_from_name_alternatives(child)
            elif child.tag == "contrib-id":
                type_ = child.get("contrib-id-type") or ""
                if type_ == "orcid":
                    params["orcid"] = child.text or ""
                if type_ == "idref":
                    params["idref"] = child.text or ""
            elif child.tag == "address":
                addr = get_text_from_node(child)
                params["addresses"].append(addr)
            elif child.tag == "email":
                params["email"] = child.text or ""
            elif child.tag == "xref":
                # Elsevier uses xref/aff-alternatives to store affiliations
                type_ = child.get("ref-type") or ""
                if type_ == "aff":
                    xref = child.get("rid") or ""
                    if xref == "":
                        xref = get_text_from_node(child)
                    if xref != "":
                        if "xrefs" not in params:
                            params["xrefs"] = [xref]
                        else:
                            params["xrefs"].append(xref)
            elif child.tag == "collab":
                params["string_name"] = child.text or ""
            elif child.tag == "role":
                pass
                # Role is used in BJHTUP11 as a textual description of the role (ex "Présidente").
                # The node value can not be assigned to params['role'] as we want a controlled vocabulary
                # (author /editor / organizer...)
                # Ignore the value
                # params["role"] = child.text or ""
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + child.tag
                    }
                )

        # Remove the sort, it causes differences between the HTML and the PDF (discovered in PCJ)
        # Sort was introduced on 22/09/2020, based on differences between the Cedrics->JATS XSLT et the Cedrics import
        # params['addresses'].sort()

        helper_update_name_params(params)

        corresp = node.get("corresp") or ""
        if corresp == "yes":
            params["corresponding"] = True

        deceased_ = node.get("deceased") or "no"
        params["deceased_before_publication"] = deceased_ == "yes"

        equal_contrib_ = node.get("equal-contrib") or "no"
        params["equal_contrib"] = equal_contrib_ == "yes"

        return params

    def get_data_from_custom_meta(self, node):
        name = ""
        value = ""

        for child in node:
            tag = normalize(child.tag)

            if tag == "meta-name":
                name = child.text
            elif tag == "meta-value":
                value = child.text
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )

        return name, value

    def get_data_from_date(self, node, ignore_month=False):
        date_str = ""
        if "iso-8601-date" in node.attrib:
            date_str = node.attrib["iso-8601-date"]
        else:
            year = month = day = ""
            for child in node:
                tag = normalize(child.tag)

                if tag == "year":
                    year = child.text
                elif tag == "month" and not ignore_month:
                    month = child.text
                elif tag == "day":
                    day = child.text
                else:
                    self.warnings.append(
                        {
                            self.pid: self.__class__.__name__
                            + "."
                            + inspect.currentframe().f_code.co_name
                            + " "
                            + tag
                        }
                    )

            date_str = year
            if date_str and month:
                date_str += "-" + month
            if date_str and day:
                date_str += "-" + day

        return date_str

    def get_data_from_ext_link(self, node, **kwargs):
        link_type = node.get("ext-link-type") or ""
        href = get_normalized_attrib(node, "href") or ""
        base = get_normalized_attrib(node, "base") or ""

        kwargs["add_HTML_link"] = False
        _, metadata = self.parse_inner_node(node, **kwargs)

        data = {
            "rel": link_type,
            "mimetype": "",
            "location": href,
            "base": base,
            "metadata": metadata,
        }

        return data

    def get_data_from_history(self, node):
        history_dates = []
        # TODO: transform history_dates in a hash where date-type is the key
        #       => Change database_cmds
        for child in node:
            if "date-type" in child.attrib:
                date_type = child.attrib["date-type"]
                date_str = self.get_data_from_date(child)
                history_dates.append({"type": date_type, "date": date_str})
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + child.tag
                    }
                )

        return history_dates

    def update_data_from_name(self, node, contributor):
        for child in node:
            if child.text is not None:
                if child.tag == "given-names":
                    contributor["first_name"] = child.text
                elif child.tag == "surname":
                    contributor["last_name"] = child.text
                elif child.tag == "prefix":
                    contributor["prefix"] = child.text
                elif child.tag == "suffix":
                    contributor["suffix"] = child.text
                else:
                    self.warnings.append(
                        {
                            self.pid: self.__class__.__name__
                            + "."
                            + inspect.currentframe().f_code.co_name
                            + " "
                            + child.tag
                        }
                    )

    def get_data_from_name_alternatives(self, node):
        mid = ""

        for child in node:
            if child.text is not None:
                if child.tag == "string-name":
                    if child.get("specific-use") == "index":
                        mid = child.text
                else:
                    self.warnings.append(
                        {
                            self.pid: self.__class__.__name__
                            + "."
                            + inspect.currentframe().f_code.co_name
                            + " "
                            + child.tag
                        }
                    )

        return mid

    def get_data_from_uri(self, node, **kwargs):
        href = get_normalized_attrib(node, "href") or ""

        kwargs["add_HTML_link"] = False
        _, metadata = self.parse_inner_node(node, **kwargs)

        data = {"rel": None, "mimetype": "", "location": href, "base": "", "metadata": metadata}

        return data

    def helper_add_link_from_node(self, node, **kwargs):
        text = node.text or ""
        tag = normalize(node.tag)
        fct_name = "get_data_from_" + tag.replace("-", "_")
        meth = getattr(self, fct_name)
        data = meth(node, **kwargs)
        if not data["rel"] or data["rel"] == "uri":
            href = data["location"]
            if self.for_tex_file:
                text = "\\href{" + href + "}{" + data["metadata"] + "}"
            else:
                text = make_links_clickable(href, data["metadata"])
        return text

    def get_list_start_value(self, list_node):
        continued_from = list_node.get("continued-from")
        if continued_from is None:
            start = 0
        else:
            from_node = self.tree.find(f'.//*[@id="{continued_from}"]')
            if from_node is not None:
                start = len(from_node) + self.get_list_start_value(from_node)

        return start


class MathdocPublication(MathdocPublicationData, JatsBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.parse_tree(kwargs["tree"])

    def parse_tree(self, tree):
        super().parse_tree(tree)

        for node in tree:
            tag = normalize(node.tag)

            if tag in ("publication-id", "collection-id"):
                node_type = node.get("publication-id-type")
                if node_type is None or node_type in ["numdam-id", "mathdoc-id"]:
                    self.pid = node.text
            elif tag == "title-group":
                self.parse_title_group(node)
            elif tag == "issn":
                node_type = node.get("pub-type")
                if node_type == "ppub":
                    self.issn = node.text
                    self.ids.append(("issn", node.text))
                elif node_type == "epub":
                    self.e_issn = node.text
                    self.ids.append(("e-issn", node.text))
            elif tag == "ext-link":
                data = self.get_data_from_ext_link(node)
                self.ext_links.append(data)
            elif tag == "custom-meta-group":
                self.parse_custom_meta_group(node)
            elif tag == "description":
                self.parse_description(node)
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )

    def parse_custom_meta_group(self, node, **kwargs):
        for child in node:
            tag = normalize(child.tag)

            if tag == "custom-meta":
                name, value = self.get_data_from_custom_meta(child)

                if name == "serial-type":
                    self.coltype = value
                elif name == "wall":
                    self.wall = int(value)
                elif name == "provider":
                    self.provider = value
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )

    def parse_description(self, node, **kwargs):
        # tag = get_normalized_attrib(node, "abstract-node_type") or "abstract"
        tag = "description"
        lang = get_normalized_attrib(node, "lang") or self.lang
        value_xml = get_xml_from_node(node)
        value_tex = value_html = value_xml.replace("<decription", "").replace("</description>", "")
        self.abstracts.append(
            {
                "tag": tag,
                "lang": lang,
                "value_xml": value_xml,
                "value_html": value_html,
                "value_tex": value_tex,
            }
        )


class JatsPublisher(PublisherData):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.warnings = []
        self.parse_tree(kwargs["tree"])
        self.warnings = []

    def parse_tree(self, tree):
        for node in tree:
            tag = normalize(node.tag)

            if tag == "publisher-name":
                self.name = node.text
            elif tag == "publisher-loc":
                self.loc = node.text
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )


class JatsJournal(JournalData, JatsBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.parse_tree(kwargs["tree"])

    def parse_tree(self, tree):
        super().parse_tree(tree)

        for node in tree:
            tag = normalize(node.tag)

            if tag == "journal-id":
                id_type = node.get("journal-id-type") or "numdam-id"
                if id_type == "numdam-id" or id_type == "mathdoc-id":
                    self.pid = node.text
            elif tag == "journal-title-group":
                self.parse_title_group(node)
            elif tag == "publisher":
                self.publisher = JatsPublisher(tree=node)
            elif tag == "issn":
                node_type = node.get("pub-type") or "ppub"
                if node_type == "ppub":
                    self.issn = node.text
                    self.ids.append(("issn", node.text))
                elif node_type == "epub":
                    self.e_issn = node.text
                    self.ids.append(("e-issn", node.text))
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )


class JatsEdito(ArticleData, JatsBase):
    def __init__(self, *args, **kwargs):  # , tree, pid=None):
        super().__init__(*args, **kwargs)
        self.pid = kwargs["pid"] if "pid" in kwargs else None
        self.issue = kwargs["issue"] if "issue" in kwargs else None

        self.add_span_around_tex_formula = (
            kwargs["add_span_around_tex_formula"]
            if "add_span_around_tex_formula" in kwargs
            else False
        )
        self.for_tex_file = kwargs["for_tex_file"] if "for_tex_file" in kwargs else False
        self.from_folder = kwargs["from_folder"] if "from_folder" in kwargs else None
        self.no_bib = kwargs.get("no_bib", False)

        self.parse_tree(kwargs["tree"])

    def parse_tree(self, tree):
        super().parse_tree(tree)
        for node in tree:
            text_html = ""

            tag = normalize(node.tag)
            if tag == "p":
                text_html = get_text_from_node(node)
                if text_html:
                    self.body_html += "<p>" + text_html + "</p>"
            elif tag == "h1":
                text_html = get_text_from_node(node)
                if text_html:
                    self.body_html += "<h1>" + text_html + "</h1>"

        return self.body_html


class JatsIssue(IssueData, JatsBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # from_folder is used to change the location of Elsevier graphics to a full path location
        self.from_folder = kwargs["from_folder"] if "from_folder" in kwargs else None
        self.no_bib = kwargs.get("no_bib", False)

        self.parse_tree(kwargs["tree"])

    def parse_tree(self, tree):
        super().parse_tree(tree)

        for node in tree:
            tag = normalize(node.tag)
            if tag == "journal-meta":
                self.journal = JatsJournal(tree=node)
            elif tag == "issue-meta":
                ctype = get_normalized_attrib(node, "issue_type")
                if ctype == "issue_special":
                    self.ctype = "issue_special"
                self.parse_issue_meta(node)
            elif tag == "body":
                for child in node:
                    tag = normalize(child.tag)

                    if tag == "article":
                        article = JatsArticle(
                            tree=child,
                            issue=self,
                            from_folder=self.from_folder,
                            no_bib=self.no_bib,
                        )
                        self.warnings.extend(article.warnings)
                        self.articles.append(article)

                    else:
                        self.warnings.append(
                            {
                                self.pid: self.__class__.__name__
                                + "."
                                + inspect.currentframe().f_code.co_name
                                + " "
                                + tag
                            }
                        )
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )

        if self.journal is not None:
            self.publisher = self.journal.publisher

        # Issue editors may be replicated in all the articles, remove them
        issue_editors = [contrib for contrib in self.contributors if contrib["role"] == "editor"]

        is_elsevier = False
        for xarticle in self.articles:
            if hasattr(xarticle, "pii"):
                is_elsevier = True

            editors = [contrib for contrib in xarticle.contributors if contrib["role"] == "editor"]
            is_equal = len(editors) == len(issue_editors)
            i = 0
            while is_equal and i < len(editors):
                if (
                    editors[i]["last_name"] != issue_editors[i]["last_name"]
                    or editors[i]["first_name"] != issue_editors[i]["first_name"]
                ):
                    is_equal = False
                i += 1
            if is_equal:
                xarticle.contributors = [
                    contrib for contrib in xarticle.contributors if contrib["role"] != "editor"
                ]

        if is_elsevier:
            # Fix location of icons
            for link in self.ext_links:
                if link["rel"] in ["icon", "small_icon"]:
                    base_dir = self.journal.pid
                    location = link["location"]
                    if os.path.dirname(location) != base_dir:
                        location = os.path.join(base_dir, self.pid, location)
                        if self.from_folder:
                            location = os.path.join(self.from_folder, location)
                            location = "file:" + location
                        link["location"] = location

            # Fix article types and subjects
            for xarticle in self.articles:
                article_type = "research-article"
                old_type = ""
                new_subjs = []

                if xarticle.fpage != "":
                    try:
                        value = int(xarticle.fpage)
                    except ValueError:
                        # fpage is not a number: the article is an editorial
                        article_type = "editorial"

                if article_type == "research-article":
                    for subj in xarticle.subjs:
                        if subj["type"] == "type":
                            # Fix article types
                            value = subj["value"].lower()
                            old_type = value
                            if value == "discussion":
                                article_type = "letter"
                            elif value == "editorial":
                                if xarticle.title_tex.lower().find("foreword") == 0:
                                    article_type = "foreword"
                                else:
                                    article_type = "editorial"
                            elif value in ["mini review", "review article", "book review"]:
                                article_type = "review"
                            elif value == "research article":
                                article_type = "research-article"
                            elif value == "short communication":
                                article_type = "foreword"
                            elif value == "correspondence":
                                article_type = "letter"
                            elif value.find("conference") == 0:
                                article_type = "congress"
                        elif subj["type"] == "heading" and not xarticle.title_tex:
                            # The title may be stored in the heading: fix it
                            xarticle.title_tex = xarticle.title_html = subj["value"]
                            xarticle.title_xml = get_title_xml(subj["value"])
                        elif subj["type"] == "heading":
                            value = subj["value"].lower().strip()
                            issue_title = self.title_tex.lower()
                            if issue_title.find("dossier: ") == 0:
                                issue_title = issue_title[9:]
                                self.title_tex = self.title_html = self.title_tex[9:]
                                self.title_xml = (
                                    "<issue-title>"
                                    + get_single_title_xml(issue_title)
                                    + "</issue-title>"
                                )

                            # Some heading values are in fact article type
                            if value.find("erratum") == 0:
                                article_type = "erratum"
                            elif value.find("corrigendum") == 0:
                                article_type = "corrigendum"
                            elif value.find("foreword") == 0:
                                article_type = "foreword"
                            elif value.find("nécrologie") == 0 or value.find("obituary") == 0:
                                article_type = "history-of-sciences"
                            elif (
                                value.find("block calendar/éphéméride") == 0
                                or value.find("chronique") == 0
                            ):
                                article_type = "history-of-sciences"
                            elif value.find("histoire") == 0 or value.find("historic") == 0:
                                article_type = "history-of-sciences"
                            elif value.find("tribute/hommage") == 0:
                                article_type = "history-of-sciences"
                            elif value.find("note historique") == 0:
                                article_type = "historical-commentary"
                            elif (
                                value.find("le point sur") == 0 or value.find("le point-sur") == 0
                            ):
                                article_type = "review"
                            elif (
                                value.find("review") == 0
                                or value.find("revue") == 0
                                or value.find("concise review") == 0
                            ):
                                article_type = "review"
                            elif value.find("conférence") == 0:
                                article_type = "congress"
                            elif (
                                value.find("communication") == 0 or value.find("preliminary") == 0
                            ):
                                article_type = "preliminary-communication"
                            elif value.find("perspective") == 0 and old_type in [
                                "correspondence",
                                "short communication",
                            ]:
                                article_type = "opinion"
                            elif value.find("debate") == 0:
                                article_type = "opinion"
                            elif (
                                value.find("index") == 0
                                or value.find("keyword") == 0
                                or value.find("sommaire") == 0
                            ):
                                article_type = "editorial"
                            elif (
                                value.find("table auteurs") == 0
                                or value.find("table sommaire") == 0
                            ):
                                article_type = "editorial"
                            elif value.find("page présentation des index") == 0:
                                article_type = "editorial"
                            elif value.find("fac-similé") == 0:
                                # Article de crbiol, Pubmed les met en "Classical Article"
                                article_type = "historical-commentary"
                                # On ajoute le sujet dans ce cas pour garder la mention de "fac-similé" (== recopie)
                                new_subjs.append(subj)
                            # Ignore the issue titles
                            elif (
                                not self.title_tex
                                or value.find(self.title_tex.lower().strip()) != 0
                            ):
                                # Exclude headings that are redundant with article types
                                exclude_list = [
                                    "editorial",
                                    "éditorial",
                                    "avant-propos",
                                    "book review",
                                    "comment",
                                    "concise review paper",
                                    "answer",
                                    "commentaire",
                                    "commentary",
                                    "reply",
                                    "foreword",
                                    "full paper",
                                    "mémoire",
                                ]
                                if len([x for x in exclude_list if value.find(x) == 0]) == 0:
                                    new_subjs.append(subj)
                        else:
                            new_subjs.append(subj)

                # print(old_type, '-', old_heading, '-', article_type, '-', xarticle.pid, '-', xarticle.fpage)
                xarticle.atype = article_type
                xarticle.subjs = new_subjs

    def parse_custom_meta_group(self, node, **kwargs):
        for child in node:
            tag = normalize(child.tag)

            if tag == "custom-meta":
                name, value = self.get_data_from_custom_meta(child)

                if name == "provider":
                    self.provider = value
                elif name == "efirst":
                    self.with_online_first = value == "yes"
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )

    def parse_issue_meta(self, node, **kwargs):
        for child in node:
            tag = normalize(child.tag)

            if tag == "issue-id":
                self.parse_id(child)
            elif tag == "volume-series":
                self.vseries = child.text
            elif tag == "volume":
                self.volume = child.text
            elif tag == "issue":
                self.number = child.text
            elif tag == "pub-date":
                self.year = self.get_data_from_date(child, ignore_month=True)
            elif tag == "history":
                history_dates = self.get_data_from_history(child)
                for date in history_dates:
                    if date["type"] == "last-modified":
                        self.last_modified_iso_8601_date_str = date["date"]
                    elif date["type"] == "prod-deployed-date":
                        self.prod_deployed_date_iso_8601_date_str = date["date"]
            elif tag == "issue-title":
                content_type = child.get("content-type") or ""
                if content_type != "subtitle" and content_type != "cover-date":
                    # Elsevier stores contributors in subtitles. Ignore.
                    lang = get_normalized_attrib(child, "lang") or "und"
                    if not self.title_tex and (
                        self.lang == "und" or lang == "und" or lang == self.lang
                    ):
                        self.parse_title(child)
                        # In xmldata, title_xml had the <title_group> tag:
                        # self.title_xml can't be set in parse_title
                        self.title_xml += get_xml_from_node(child)
                    else:
                        self.trans_lang = lang
                        (
                            self.trans_title_tex,
                            self.trans_title_html,
                        ) = self.parse_node_with_mixed_content(child)
                        self.title_xml += get_xml_from_node(child)
            elif tag == "issue-title-group":
                self.parse_title_group(child)
            else:
                fct_name = "parse_" + tag.replace("-", "_")
                ftor = getattr(self, fct_name, None)
                if callable(ftor):
                    ftor(child, add_ext_link=True)
                else:
                    self.warnings.append(
                        {
                            self.pid: self.__class__.__name__
                            + "."
                            + inspect.currentframe().f_code.co_name
                            + " "
                            + tag
                        }
                    )

        if self.last_modified_iso_8601_date_str is None:
            self.last_modified_iso_8601_date_str = timezone.now().isoformat()


class JatsArticleBase(JatsBase):
    def parse_custom_meta_group(self, node, **kwargs):
        for child in node:
            tag = normalize(child.tag)

            if tag == "custom-meta":
                name, value = self.get_data_from_custom_meta(child)

                if name == "article-number":
                    self.article_number = value
                elif name == "talk-number":
                    self.talk_number = value
                elif name == "presented":
                    presenter = create_contributor()
                    presenter["role"] = "presenter"
                    presenter["string_name"] = value.replace("Presented by ", "").replace(
                        "Présenté par ", ""
                    )
                    presenter["contrib_xml"] = get_contrib_xml(presenter)
                    self.contributors.append(presenter)
                elif name == "provider":
                    self.provider = value

            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )


class JatsArticle(ArticleData, JatsArticleBase):
    def __init__(self, *args, **kwargs):  # , tree, pid=None):
        super().__init__(*args, **kwargs)
        self.pid = kwargs["pid"] if "pid" in kwargs else None
        self.issue = kwargs["issue"] if "issue" in kwargs else None

        self.add_span_around_tex_formula = (
            kwargs["add_span_around_tex_formula"]
            if "add_span_around_tex_formula" in kwargs
            else False
        )
        self.for_tex_file = kwargs["for_tex_file"] if "for_tex_file" in kwargs else False
        self.from_folder = kwargs["from_folder"] if "from_folder" in kwargs else None
        self.no_bib = kwargs.get("no_bib", False)

        self.parse_tree(kwargs["tree"])

    def parse_tree(self, tree):
        super().parse_tree(tree)

        self.atype = get_normalized_attrib(tree, "article-type") or ""

        # First loop to catch float-groups that are inserted inside the body
        for node in tree:
            tag = normalize(node.tag)

            if tag == "front":
                for child in node:
                    tag = normalize(child.tag)

                    if tag == "article-meta":
                        self.parse_article_meta(child)
                    else:
                        self.warnings.append(
                            {
                                self.pid: self.__class__.__name__
                                + "."
                                + inspect.currentframe().f_code.co_name
                                + " "
                                + tag
                            }
                        )
            elif tag == "front-stub":
                self.parse_article_meta(node)
            elif tag == "floats-group":
                self.parse_floats_group(node)

        for node in tree:
            tag = normalize(node.tag)
            if tag == "back":
                for child in node:
                    tag = normalize(child.tag)

                    if tag == "ref-list" and not self.no_bib:
                        print("Parse bib")
                        self.parse_ref_list(child)
                    elif tag == "ack":
                        self.parse_ack(child)
                    elif tag == "sec":
                        self.parse_sec(child)
                    elif tag == "app-group":
                        self.parse_app_group(child)
                    elif tag == "fn-group":
                        self.parse_fn_group(child)
                    else:
                        self.warnings.append(
                            {
                                self.pid: self.__class__.__name__
                                + "."
                                + inspect.currentframe().f_code.co_name
                                + " "
                                + tag
                            }
                        )

            elif tag == "body":
                self.parse_body(node)
            elif tag == "sub-article":
                self.parse_sub_article(node)
            elif tag == "floats-group" or tag == "front":
                # Handled above
                pass
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )

        # Add the footnotes at the end
        if len(self.fns) > 0:
            fn_text = '<div class="footnotes">'
            for fn in self.fns:
                fn_text += fn
            fn_text += "</div>"

            self.body_html = fn_text if not self.body_html else self.body_html + fn_text

        if (
            len(self.funding_statement_xml) > 0
            and self.funding_statement_xml.find('<name-content content-type="fn"') == -1
        ):
            self.funding_statement_xml = (
                f'<name-content content-type="fn">{self.funding_statement_xml}</name-content>'
            )

        # Case for XML with <body>, then <back> and <floats_group>
        # The figures/tables of the floats_group are added inside the body_html
        # (close to their first <xref>)
        # It's too complicated to do the same for the body_xml as we use the get_xml_from_node function.
        # Instead, we append the floats_group_xml to the body_xml
        if hasattr(self, "floats_group_xml"):
            self.body_xml += self.floats_group_xml

        # Special treatment for Elsevier articles: web scrapping to find the date_published
        # Moved to the import management commands since Elsevier blocks IP after 1000+ requests
        # if hasattr(self, 'pii') and self.date_published_iso_8601_date_str is None:
        #     article_data = scrapping.fetch_article(self.doi, self.pii)
        #     self.date_published_iso_8601_date_str = article_data.date_published_iso_8601_date_str

        self.post_parse_tree()

    def update_body_content(self, node, **kwargs):
        if len(node) == 0:
            # Most journals do not display the Full text
            # the <body> is then used to store the text for the search engine and has no children
            # Let's not compute body_html in this case.
            # We want the same behavior for journals that display the Full text,
            # but with old articles without Full text.
            return

        # <front> has to be put before <body> so self.pid is defined here
        if hasattr(settings, "SITE_URL_PREFIX"):
            prefix = settings.SITE_URL_PREFIX
            base_article = settings.ARTICLE_BASE_URL
            base_url = "/" + prefix + base_article + self.pid
        else:
            base_url = os.path.join(settings.ARTICLE_BASE_URL, self.pid)
        kwargs["base_url"] = base_url

        append_to_body = True
        current_len = len(self.supplementary_materials)

        if "use_sec" in kwargs and kwargs["use_sec"]:
            # Hack for Elsevier: convert <ack> into <sec> of the <body>
            body_tex, body_html = self.parse_node_with_sec(node, **kwargs)
        else:
            body_tex, body_html = self.parse_node_with_mixed_content(node, **kwargs)

        if len(self.supplementary_materials) != current_len:
            # Elsevier stores supplementary-material in app-group.
            # They are extracted, but ignored in the body_html if the appendix has only supplements
            append_to_body = False

            for child in node:
                if child.tag == "p":
                    for gchild in child:
                        if gchild.tag != "supplementary-material":
                            append_to_body = True

        if append_to_body:
            self.body_tex = body_tex if not self.body_tex else self.body_tex + body_tex
            self.body_html = body_html if not self.body_html else self.body_html + body_html

            body_xml = get_xml_from_node(node)
            if not self.body_xml:
                self.body_xml = body_xml
            else:
                if "use_sec" in kwargs and kwargs["use_sec"]:
                    self.body_xml = f"{self.body_xml[0:-7]}<sec>{body_xml[5:-6]}</sec></body>"
                else:
                    self.body_xml = f"{self.body_xml[0:-7]}{body_xml}</body>"

    def parse_ack(self, node, **kwargs):
        content_type = node.get("content-type") or ""
        if content_type == "COI-statement":
            self.coi_statement = get_text_from_node(node)
        else:
            # Hack for Elsevier: convert <ack> into <sec> of the <body>
            self.update_body_content(node, use_sec=True)

    def parse_app(self, node, **kwargs):
        for child in node:
            tag = normalize(child.tag)

            if tag == "sec":
                # Elsevier can store all appendixes inside one <app> ?!?
                # One of them can store the supplements and has to be ignored in the body_html
                self.update_body_content(child)
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )

    def parse_app_group(self, node, **kwargs):
        for child in node:
            tag = normalize(child.tag)

            if tag == "app":
                self.parse_app(child)
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )

    def parse_article_categories(self, node, **kwargs):
        for child in node:
            tag = normalize(child.tag)

            if tag == "subj-group":
                self.parse_subj_group(child)
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )

    def parse_article_meta(self, node, **kwargs):
        for child in node:
            tag = normalize(child.tag)

            if tag == "article-id":
                self.parse_id(child)
            elif tag == "fpage":
                self.fpage = child.text
                self.page_type = child.get("content-type") or ""
            elif tag == "lpage":
                self.lpage = child.text or ""
            elif tag == "page-range":
                self.page_range = child.text
            elif tag in ("page-count", "size"):
                self.size = child.text
            elif tag == "elocation-id":
                self.elocation = child.text
            elif tag == "pub-date":
                date_type = child.get("date-type") or "pub"
                if date_type == "pub":
                    self.date_published_iso_8601_date_str = self.get_data_from_date(child)
                else:
                    date_str = self.get_data_from_date(child)
                    self.history_dates.append({"type": "online", "date": date_str})
            elif tag == "history":
                self.history_dates += self.get_data_from_history(child)
                for date in self.history_dates:
                    if date["type"] == "prod-deployed-date":
                        self.prod_deployed_date_iso_8601_date_str = date["date"]
            elif tag in ["volume", "issue-id", "permissions", "pub-date-not-available"]:
                pass
                # TODO: store permissions in XML
            elif tag == "author-notes":
                # 2022/11/15 Mersenne meeting. ignore author-notes
                pass
                # self.parse_author_notes(child)
            else:
                fct_name = "parse_" + tag.replace("-", "_")
                ftor = getattr(self, fct_name, None)
                if callable(ftor):
                    ftor(child, add_ext_link=True)
                else:
                    self.warnings.append(
                        {
                            self.pid: self.__class__.__name__
                            + "."
                            + inspect.currentframe().f_code.co_name
                            + " "
                            + tag
                        }
                    )

    def parse_author_notes(self, node, **kwargs):
        for child in node:
            tag = normalize(child.tag)
            if tag == "fn":
                _, html = self.parse_node_with_fn(child, keep_fn=True, keep_fn_label=False)
                xml = get_xml_from_node(child)
                self.footnotes_xml += xml
                self.footnotes_html += html

    def parse_body(self, node, **kwargs):
        self.body = get_text_from_node(node)

        if hasattr(self, "floats"):
            self.floats_to_insert = []

        self.update_body_content(node, **kwargs)

        if not self.body_xml:
            self.body_xml = get_xml_from_node(node)

    def parse_boxed_text(self, node, **kwargs):
        """
        Parse <boxed-text> inside <floats-group> and fills the self.float_boxed_texts dictionary.
        The dictionary is then used during parse_body to embed the boxed-text inside the body HTML.
        """
        box_id = node.attrib["id"] if "id" in node.attrib else None

        _, html = self.parse_node_with_boxed_text(node, **kwargs)

        if box_id is not None:
            self.floats[box_id] = html

    def parse_floats_group(self, node, **kwargs):
        if hasattr(settings, "SITE_URL_PREFIX"):
            prefix = settings.SITE_URL_PREFIX
            base_article = settings.ARTICLE_BASE_URL
            base_url = "/" + prefix + base_article + self.pid
        else:
            base_url = os.path.join(settings.ARTICLE_BASE_URL, self.pid)

        self.floats = {}
        for child in node:
            tag = normalize(child.tag)

            if tag == "fig":
                self.parse_node_with_fig(child, append_floats=True, base_url=base_url)
            elif tag == "table-wrap":
                self.parse_node_with_table_wrap(child, append_floats=True, base_url=base_url)
            elif tag == "boxed-text":
                self.parse_boxed_text(child, base_url=base_url)
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )

        self.floats_group_xml = get_xml_from_node(node)

    def parse_fn_group(self, node, **kwargs):
        for child in node:
            tag = normalize(child.tag)

            if tag == "fn":
                _, html = self.parse_node_with_fn(child, keep_fn=True)
                xml = get_xml_from_node(child)

                self.footnotes_html += html
                self.footnotes_xml += xml
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )

    def parse_funding_group(self, node, **kwargs):
        for child in node:
            tag = normalize(child.tag)

            if tag == "award-group":
                self.parse_award_group(child)
            elif tag == "funding-statement":
                for funding_node in child:
                    if funding_node.tag == "name-content":
                        for funding_child in funding_node:
                            if funding_child.tag == "fn":
                                _, html = self.parse_node_with_fn(funding_child, keep_fn=True)
                                self.funding_statement_html += html
                        self.funding_statement_xml = get_xml_from_node(funding_node)

                # TODO: handle funding-statement with simple texts
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )

    def parse_issue(self, node, **kwargs):
        # Elsevier stores bs in the seq attribute
        self.seq = "0" if hasattr(self, "pii") else (node.get("seq") or "0")


class JatsRef(RefBase, JatsBase):
    def __init__(self, *args, tree, lang="und", **kwargs):
        super().__init__(*args, lang=lang, **kwargs)
        self.parse_tree(tree)

    def parse_tree(self, tree):
        super().parse_tree(tree)

        self.user_id = get_normalized_attrib(tree, "id") or ""

        for node in tree:
            tag = normalize(node.tag)

            if tag == "label":
                self.label = node.text or ""

                if self.label:
                    if self.label[0] != "[":
                        self.label = "[" + self.label + "]"

            elif tag == "mixed-citation" or tag == "note":
                self.parse_citation_node(node)

                self.citation_tex, self.citation_html = self.parse_node_with_mixed_content(
                    node,
                    is_citation=True,
                    is_mixed_citation=True,
                    add_ext_link=True,
                    ref_type="misc",
                )

                if self.label:
                    self.citation_html = self.label + " " + self.citation_html
                    self.citation_tex = self.label + " " + self.citation_tex

            elif tag == "element-citation":
                self.parse_citation_node(node)

                self.citation_tex = self.citation_html = get_citation_html(self)
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )

            # With xmldata, citation_xml does not have '<ref>', but only the text of the children
            self.citation_xml += get_xml_from_node(node)

    def get_data_from_name_in_ref(self, node, role):
        params = create_contributor()
        params["role"] = role

        if node.tag == "name":
            self.update_data_from_name(node, params)
        elif node.tag == "string-name":
            self.update_data_from_name(node, params)
            if params["first_name"] == "" and params["last_name"] == "":
                params["string_name"] = node.text or ""
        elif node.tag == "name-alternatives":
            params["mid"] = self.get_data_from_name_alternatives(node)
        elif node.tag == "collab":
            params["string_name"] = node.text or ""

        use_initials = getattr(settings, "REF_JEP_STYLE", False)
        helper_update_name_params(params, use_initials)
        params["contrib_xml"] = "<etal/>" if node.tag == "etal" else get_xml_from_node(node)

        return params

    def parse_node_with_chapter_title(self, node, **kwargs):
        tex, html = self.parse_inner_node(node, **kwargs)

        is_mixed_citation = kwargs["is_mixed_citation"] if "is_mixed_citation" in kwargs else False
        if is_mixed_citation:
            html = add_span_class_to_html_from_chapter_title(html, **kwargs)

        return tex, html

    def parse_node_with_source(self, node, **kwargs):
        tex, html = self.parse_inner_node(node, **kwargs)

        is_mixed_citation = kwargs["is_mixed_citation"] if "is_mixed_citation" in kwargs else False
        if is_mixed_citation:
            html = add_span_class_to_html_from_source(html, **kwargs)

        return tex, html

    def parse_citation_node(self, node, **kwargs):
        self.type = get_normalized_attrib(node, "publication-type") or "misc"

        # Elsevier can store data about a translation after comments (<source>...)
        # Append these tags in the comment
        has_comment = False

        for child in node:
            tag = normalize(child.tag)

            if tag in ("page-count", "size"):
                if not self.size:
                    self.size = child.text
            elif tag == "comment":
                has_comment = True
                # comments may have ext-links or uri. HTML <a> links will be added
                _, comment = self.parse_node_with_mixed_content(
                    child, is_citation=True, is_comment=True, add_HTML_link=True
                )
                if self.comment:
                    self.comment += " "
                self.comment += comment
            elif tag == "source":
                # TODO: migration to store source_tex and source_html
                _, source_tex = self.parse_node_with_mixed_content(child, is_citation=True)

                if self.type in ["book", "inproceedings"] and len(self.source_tex) > 0:
                    # Multiple source for a book, store the extra source in series
                    if self.series and has_comment:
                        self.comment += " " + source_tex
                    else:
                        if self.series:
                            self.series += ", "
                        self.series += get_text_from_node(child)
                else:
                    if self.source_tex and has_comment:
                        self.comment += " " + source_tex
                    else:
                        self.source_tex = source_tex
            elif tag == "series":
                series = get_text_from_node(child)
                if self.series and has_comment:
                    self.comment += ", " + series
                else:
                    if self.series:
                        self.series += ", "
                    self.series += series
            elif tag == "annotation":
                if not self.annotation:
                    self.annotation = get_text_from_node(child)
            elif tag == "article-title":
                # TODO: migration to store article_title_tex and article_title_html
                _, article_title_tex = self.parse_node_with_mixed_content(child, is_citation=True)

                if self.type == "book":
                    # Elsevier uses article-title for books !?!
                    if len(self.source_tex) == 0:
                        if has_comment:
                            self.comment += " " + article_title_tex
                        else:
                            self.source_tex = article_title_tex
                    else:
                        if self.series and has_comment:
                            self.comment += ", " + article_title_tex
                        else:
                            self.series += get_text_from_node(child)
                elif self.type == "inproceedings":
                    if self.chapter_title_tex and has_comment:
                        self.comment += " " + article_title_tex
                    else:
                        self.chapter_title_tex = article_title_tex
                else:
                    if self.article_title_tex and has_comment:
                        self.comment += " " + article_title_tex
                    else:
                        self.article_title_tex = article_title_tex
            elif tag == "chapter-title":
                # TODO: migration to store chapter_title_tex and chapter_title_html
                _, chapter_title_tex = self.parse_node_with_mixed_content(child, is_citation=True)
                if self.chapter_title_tex and has_comment:
                    self.comment += " " + chapter_title_tex
                else:
                    self.chapter_title_tex = chapter_title_tex
            elif tag == "conf-name":
                _, conf_tex = self.parse_node_with_mixed_content(child, is_citation=True)
                if self.source_tex and has_comment:
                    self.comment += ", " + conf_tex
                else:
                    self.source_tex = conf_tex
            elif tag in ("name", "string-name", "name-alternatives", "etal", "collab"):
                params = self.get_data_from_name_in_ref(child, "author")
                self.contributors.append(params)
            elif tag == "person-group":
                self.parse_person_group(child)
            elif tag == "ext-link":
                self.parse_ext_link(child, add_ext_link=True)
            elif tag == "pub-id":
                self.parse_pub_id(child)
            elif tag == "date":
                self.year = get_text_from_node(child)
            elif tag == "date-in-citation":
                date_ = child.get("iso-8601-date") or ""
                if date_:
                    if self.comment:
                        self.comment += ", "
                    self.comment += "Accessed " + date_
            elif tag == "isbn":
                if self.annotation:
                    self.annotation += ", "
                self.annotation += "ISBN: " + child.text
            elif tag == "issn":
                if self.annotation:
                    self.annotation += ", "
                self.annotation += "ISSN: " + child.text
            elif child.text is not None:
                variable_name = tag.replace("-", "_")
                if has_comment and hasattr(self, variable_name) and getattr(self, variable_name):
                    if tag == "fpage":
                        self.comment += ", pp. "
                    elif tag == "lpage":
                        self.comment += "-"
                    else:
                        self.comment += ", "
                    self.comment += child.text
                elif not hasattr(self, variable_name) or not getattr(self, variable_name):
                    setattr(self, variable_name, child.text)

    def parse_person_group(self, node, **kwargs):
        role = node.get("person-group-type") or ""
        if role and role[-1] == "s":
            role = role[:-1]

        for child in node:
            tag = normalize(child.tag)

            if tag in ("name", "string-name", "name-alternatives", "etal", "collab"):
                contrib = self.get_data_from_name_in_ref(child, role)
                self.contributors.append(contrib)
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )

    def parse_pub_id(self, node, **kwargs):
        node_type = node.get("pub-id-type") or ""

        data: ExtLinkDict = {
            "rel": node_type,
            "mimetype": "",
            "location": "",
            "base": "",
            "metadata": node.text,
        }

        self.add_extids_from_node_with_link(data)

    def split_label(self):
        """
        Used when sorting non-digit bibitems
        """
        label = self.label.lower()
        if len(label) > 1:
            label = label[1:-1]

        try:
            self.label_prefix, self.label_suffix = re.split(r"[\d]+", label)
        except ValueError:
            # Special case where label is similar as "Sma" instead of "Sma15"
            self.label_prefix, self.label_suffix = [label, ""]


class BitsCollection(CollectionData, JatsBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.parse_tree(kwargs["tree"])

    def parse_tree(self, tree):
        super().parse_tree(tree)

        if tree is not None:
            tag = normalize(tree.tag)
            collection_meta_node = None
            if tag == "collection-meta":
                self.parse_collection_meta(tree)
                collection_meta_node = tree
            elif tag == "in-collection":
                for node in tree:
                    tag = normalize(node.tag)

                    if tag == "collection-meta":
                        self.parse_collection_meta(node)
                        collection_meta_node = node
                    elif tag == "volume":
                        self.parse_volume(node)
                    elif tag == "volume-series":
                        self.parse_volume_series(node)
                    elif tag == "volume-title":
                        self.parse_volume_title(node)
                    else:
                        self.warnings.append(
                            {
                                self.pid: self.__class__.__name__
                                + "."
                                + inspect.currentframe().f_code.co_name
                                + " "
                                + tag
                            }
                        )

            if collection_meta_node is not None:
                self.set_seq(collection_meta_node)
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )

        self.collection = Foo()
        self.collection.pid = self.pid

    def parse_collection_meta(self, node, **kwargs):
        self.coltype = node.get("collection-type")

        for child in node:
            tag = normalize(child.tag)

            if tag == "collection-id":
                self.pid = child.text
            elif tag == "title-group":
                self.parse_title_group(child)
            elif tag == "issn":
                node_type = child.get("pub-type")
                if node_type == "ppub":
                    self.issn = child.text
                    self.ids.append(("issn", child.text))
                elif node_type == "epub":
                    self.e_issn = child.text
                    self.ids.append(("e-issn", child.text))
            elif tag == "ext-link":
                data = self.get_data_from_ext_link(child)
                self.ext_links.append(data)
            elif tag == "volume-in-collection":
                self.parse_volume_in_collection(child)
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )

    def parse_volume(self, node, **kwargs):
        self.volume = node.text

    def parse_volume_in_collection(self, node, **kwargs):
        for child in node:
            tag = normalize(child.tag)

            if tag == "volume-number":
                self.parse_volume(child)
            elif tag == "volume-series":
                self.parse_volume_series(child)
            elif tag == "volume-title":
                self.parse_volume_title(child)
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )

    def parse_volume_series(self, node, **kwargs):
        self.vseries = node.text

    def parse_volume_title(self, node, **kwargs):
        self.title_tex, self.title_html = self.parse_node_with_mixed_content(node)
        self.title_xml = get_xml_from_node(node)

    def set_seq(self, node):
        try:
            # First, use the seq attribute, if any
            self.seq = int(node.get("seq") or "")
        except ValueError:
            # Second, use self.volume (which can be like "158-159")
            if not self.volume:
                self.seq = 0
            else:
                text = self.volume.split("-")[0]
                try:
                    self.seq = int(text)
                except ValueError:
                    self.seq = 0

        # Third, use self.vseries as an offset
        try:
            # pas plus de 10000 ouvrages dans une série (gasp)
            self.seq = int(self.vseries) * 10000 + self.seq
        except ValueError:
            pass


class BitsBook(BookData, JatsBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.no_bib = kwargs.get("no_bib", False)

        self.parse_tree(kwargs["tree"])

    def parse_tree(self, tree):
        super().parse_tree(tree)

        book_type = get_normalized_attrib(tree, "book-type") or "Book"
        self.ctype = "book-" + book_type

        for node in tree:
            if type(tree) == type(node):
                tag = normalize(node.tag)

                if tag in ("collection-meta", "in-collection"):
                    col = BitsCollection(tree=node)
                    self.incollection.append(col)
                elif tag == "book-meta":
                    self.parse_book_meta(node)
                elif tag == "book-body":
                    self.parse_book_body(node)
                elif tag == "front-matter":
                    self.parse_front_matter(node)
                elif tag == "book-back":
                    for child in node:
                        tag = normalize(child.tag)
                        if tag == "ref-list":
                            self.parse_ref_list(child)
                        else:
                            self.warnings.append(
                                {
                                    self.pid: self.__class__.__name__
                                    + "."
                                    + inspect.currentframe().f_code.co_name
                                    + " "
                                    + tag
                                }
                            )
                else:
                    self.warnings.append(
                        {
                            self.pid: self.__class__.__name__
                            + "."
                            + inspect.currentframe().f_code.co_name
                            + " "
                            + tag
                        }
                    )

        self.set_contribs()
        self.set_title()
        self.post_parse_tree()

    def parse_book_body(self, node, **kwargs):
        for child in node:
            if type(child) == type(node):
                tag = normalize(child.tag)

                if tag == "book-part":
                    book_part = BitsBookPart(tree=child, no_bib=self.no_bib)
                    self.warnings.extend(book_part.warnings)
                    self.parts.append(book_part)
                else:
                    self.warnings.append(
                        {
                            self.pid: self.__class__.__name__
                            + "."
                            + inspect.currentframe().f_code.co_name
                            + " "
                            + tag
                        }
                    )

        if not self.parts:
            self.body = get_text_from_node(node)

    def parse_book_meta(self, node, **kwargs):
        for child in node:
            tag = normalize(child.tag)

            if tag == "book-id":
                self.parse_id(child)
            elif tag == "pub-date":
                self.year = self.get_data_from_date(child)
            elif tag == "book-volume-number":
                self.volume = child.text
                self.volume_int = child.text
            elif tag == "pub-history":
                history_dates = self.get_data_from_history(child)
                for date in history_dates:
                    if date["type"] == "last-modified":
                        self.last_modified_iso_8601_date_str = date["date"]
                    elif date["type"] == "prod-deployed-date":
                        self.prod_deployed_date_iso_8601_date_str = date["date"]
            elif tag == "book-title-group":
                self.parse_title_group(child)
            elif tag == "publisher":
                self.publisher = JatsPublisher(tree=child)
            else:
                fct_name = "parse_" + tag.replace("-", "_")
                ftor = getattr(self, fct_name, None)
                if callable(ftor):
                    ftor(child, add_ext_link=True)
                else:
                    self.warnings.append(
                        {
                            self.pid: self.__class__.__name__
                            + "."
                            + inspect.currentframe().f_code.co_name
                            + " "
                            + tag
                        }
                    )

        if self.last_modified_iso_8601_date_str is None:
            self.last_modified_iso_8601_date_str = timezone.now().isoformat()

    def parse_custom_meta_group(self, node, **kwargs):
        for child in node:
            tag = normalize(child.tag)

            if tag == "custom-meta":
                name, value = self.get_data_from_custom_meta(child)

                if name == "provider":
                    self.provider = value

    def set_contribs(self):
        """
        Update the contrib_groups if the XML does not declare any
        - with the authors of the first part
           - if the book is a monograph
           - if all parts are written by the same authors

        :return:
        """

        authors = [contrib for contrib in self.contributors if contrib["role"] == "author"]
        if not authors:
            if self.ctype == "book-monograph" and self.parts:
                first_part = self.parts[0]
                self.contributors = first_part.contributors
            elif (
                self.ctype == "book-edited-book" or self.ctype == "book-lecture-notes"
            ) and self.parts:
                # check if authors of the book-parts are identical
                equal = True
                book_part_contributors = self.parts[0].contributors
                i = 1
                while equal and i < len(self.parts):
                    part = self.parts[i]
                    if part.contributors != book_part_contributors:
                        equal = False
                    i += 1
                if equal:
                    if self.ctype == "book-edited-book":
                        self.ctype = "book-monograph"
                    self.contributors = book_part_contributors
                else:
                    contrib = create_contributor()
                    contrib["string_name"] = "Collectif"
                    contrib["role"] = "author"
                    contrib["contrib_xml"] = get_contrib_xml(contrib)
                    self.contributors.append(contrib)

    def set_title(self):
        if self.title_xml == "" and len(self.incollection) > 0:
            self.title_xml = self.incollection[0].title_xml
            self.title_html = self.incollection[0].title_html
            self.title_tex = self.incollection[0].title_tex


class BitsBookPart(BookPartData, JatsArticleBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.no_bib = kwargs.get("no_bib", False)

        self.parse_tree(kwargs["tree"])

    def parse_tree(self, tree):
        super().parse_tree(tree)

        self.atype = get_normalized_attrib(tree, "book-part-type") or ""
        try:
            self.seq = int(get_normalized_attrib(tree, "seq") or "")
        except ValueError:
            pass

        for node in tree:
            tag = normalize(node.tag)

            if tag == "book-part-meta":
                self.parse_book_part_meta(node)
            elif tag == "body":
                self.parse_body(node)
            elif tag == "front-matter":
                self.parse_front_matter(node)
            elif tag == "back":
                for child in node:
                    tag = normalize(child.tag)

                    if tag == "ref-list":
                        self.parse_ref_list(child)
                    else:
                        self.warnings.append(
                            {
                                self.pid: self.__class__.__name__
                                + "."
                                + inspect.currentframe().f_code.co_name
                                + " "
                                + tag
                            }
                        )
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )

        # Work around a numdam-plus bug where a book-part can have a trans-title without a title
        # TODO: Fix numdam-plus, the books impacted and remove the hack
        self.set_title()

        self.post_parse_tree()

    def parse_book_part_meta(self, node, **kwargs):
        for child in node:
            tag = normalize(child.tag)

            if tag == "book-part-id":
                self.parse_id(child)
            elif tag == "fpage":
                self.fpage = child.text
                self.page_type = get_normalized_attrib(child, "content-type") or ""
            elif tag == "lpage":
                self.lpage = child.text
            elif tag == "page-range":
                self.page_range = child.text
            else:
                fct_name = "parse_" + tag.replace("-", "_")
                ftor = getattr(self, fct_name, None)
                if callable(ftor):
                    ftor(child)
                else:
                    self.warnings.append(
                        {
                            self.pid: self.__class__.__name__
                            + "."
                            + inspect.currentframe().f_code.co_name
                            + " "
                            + tag
                        }
                    )

    def parse_body(self, node, **kwargs):
        for child in node:
            tag = normalize(child.tag)

            if tag == "book-part":
                book_part = BitsBookPart(tree=child, no_bib=self.no_bib)
                self.warnings.extend(book_part.warnings)
                self.parts.append(book_part)
            else:
                self.warnings.append(
                    {
                        self.pid: self.__class__.__name__
                        + "."
                        + inspect.currentframe().f_code.co_name
                        + " "
                        + tag
                    }
                )

        self.body = get_text_from_node(node)

    def set_title(self):
        """
        Bug in some books: some chapters may have a trans-title, but no title !
        Hack and manually set the title*
        :return:
        """

        if self.trans_title_html and not self.title_html:
            self.title_html = self.trans_title_html
            self.title_tex = self.trans_title_tex


######################################################################################
#
# Functions used by ptf-tools
#
######################################################################################


def update_bibitem_xml(bibitem, new_ids):
    xml = "<ref>" + bibitem.citation_xml + "</ref>"
    the_parser = etree.XMLParser(
        huge_tree=True, recover=True, remove_blank_text=False, remove_comments=True
    )
    tree = etree.fromstring(xml, parser=the_parser)

    node = tree.find("element-citation")
    if node is None:
        node = tree.find("mixed-citation")
    if node is not None:
        children_to_remove = []
        for child in node:
            if child.tag == "ext-link":
                child_type = child.get("ext-link-type")
                if child_type and child_type in [
                    "zbl-item-id",
                    "mr-item-id",
                    "doi",
                    "numdam-id",
                    "mathdoc-id",
                    "eid",
                ]:
                    children_to_remove.append(child)
            elif child.tag == "pub-id":
                child_type = child.get("pub-id-type")
                if child_type and child_type in [
                    "zbl-item-id",
                    "mr-item-id",
                    "doi",
                    "numdam-id",
                    "mathdoc-id",
                ]:
                    children_to_remove.append(child)

        for child in children_to_remove:
            node.remove(child)

        for id_type, value_dict in new_ids.items():
            if value_dict["checked"] and not value_dict["false_positive"]:
                if id_type in ["doi", "arxiv", "tel", "hal", "theses.fr"]:
                    new_node = etree.Element("pub-id")
                    new_node.set("pub-id-type", id_type)
                else:
                    new_node = etree.Element("ext-link")
                    new_node.set("ext-link-type", id_type)

                new_node.text = value_dict["id_value"]
                node.append(new_node)

    # TODO Modify the call to update_bibitem_xml and pass the parent's lang
    result = JatsRef(tree=tree, lang="und")
    return result


def check_bibitem_xml(bibitem: RefData):
    xml = "<ref>" + bibitem.citation_xml + "</ref>"
    the_parser = etree.XMLParser(
        huge_tree=True, recover=True, remove_blank_text=False, remove_comments=True
    )
    tree = etree.fromstring(xml, parser=the_parser)

    result = JatsRef(tree=tree, lang="und")
    return result


#  Create XML strings based on internal data


def get_tex_from_xml(xml, tag, **kwargs):
    parser_ = etree.XMLParser(
        huge_tree=True, recover=True, remove_blank_text=False, remove_comments=True
    )
    etree.register_namespace("mml", "http://www.w3.org/1998/Math/MathML")
    # text = xml.replace('xmlns:xlink="http://www.w3.org/1999/xlink"', '')
    text = xml

    if tag in ["abstract", "title"]:
        text = f"<article><front><article-meta>{text}</article-meta></front></article>"

    tree = etree.fromstring(text.encode("utf-8"), parser=parser_)
    xarticle = JatsArticle(tree=tree, **kwargs)

    result = ""
    if tag == "abstract":
        result = xarticle.abstracts[0]["value_tex"]
    elif tag == "title":
        result = xarticle.title_tex, xarticle.trans_title_tex

    return result
