from django.conf import settings

from ptf.cmds.xml.xml_base import RefBase
from ptf.cmds.xml.xml_utils import make_links_clickable
from ptf.model_data import ContributorDict
from ptf.utils import get_display_name

# Bug fixed:
# 1. article_title / chapter_title
#     TODO: 02/05/2020. Restore the line in comment below. There's a bug in production
#     # &ldquo; is inside the <span>, but &rdquo; is outside
# 2. authors. if there is no author, there is still a span in HTML
# 3. institution is ignored in citation_html (except JEP)
# 4. month is ignored in citation_html (except JEP)
# 5. series. suffix is set to '</span>' but the opening <span> is missing
# 6. volume JEP. ", " is inside the <span> of the volume


REF_JEP_STYLE = getattr(settings, "REF_JEP_STYLE", False)
REF_ALCO_STYLE = getattr(settings, "REF_ALCO_STYLE", False)
REF_PCJ_STYLE = getattr(settings, "REF_PCJ_STYLE", False)


def helper_decorate_text(text, prefix, suffix):
    if text is None or text == "":
        return ""

    text = prefix + text + suffix
    return text


def get_html_from(tag, ref_data):
    text = ""

    fct_name = "get_html_from_" + tag.replace("-", "_")
    if fct_name in globals() and callable(globals()[fct_name]):
        text += globals()[fct_name](ref_data)
    else:
        fct_name = "add_prefix_to_html_from_" + tag.replace("-", "_")
        if fct_name in globals() and callable(globals()[fct_name]):
            text += globals()[fct_name](ref_data)

        fct_name = "add_span_class_to_html_from_" + tag.replace("-", "_")
        if fct_name in globals() and callable(globals()[fct_name]):
            text = globals()[fct_name](text, ref_type=ref_data.type)

    return text


def get_html_from_authors(ref_data: RefBase):
    """
    authors may have <etal/> that require 2 spans
    We cannot use the add_prefix_ then add_span_class_ functions

    :param ref_data:
    :return:
    """
    contribs = ref_data.get_authors()
    text = get_html_from_contribs(contribs, with_span=True) if contribs else ""

    return text


def get_html_from_contribs(contribs: list[ContributorDict], is_editor=False, with_span=False):
    text = ""
    ref_jep_style = getattr(settings, "REF_JEP_STYLE", False)
    ref_pcj_style = getattr(settings, "REF_PCJ_STYLE", False)
    i = 1
    size = len(contribs)

    if with_span and not is_editor:
        text = '<span class="citation-author">'

    for contrib in contribs:
        first_name = contrib["first_name"]
        if (ref_jep_style or ref_pcj_style) and first_name:
            # JEP_STYLE: Only the first name initials are used
            words = first_name.split()  # first_name may have multiple words
            initials = []
            for word in words:
                parts = word.split("-")  # a first_name may be composed (ex: "Jean-Pierre")
                word = "-".join([f"{part[0]}." for part in parts if part])
                initials.append(word)
            first_name = " ".join(initials)

        string_name = get_display_name(
            contrib["prefix"],
            first_name,
            contrib["last_name"],
            contrib["suffix"],
            contrib["string_name"],
        )

        if i > 1:
            if i == size and contrib["contrib_xml"] == "<etal/>":
                text += '</span> <span class="citation_etal">'
            elif i == size and ref_jep_style:
                text += " & "
            elif ref_jep_style:
                text += ", "
            else:
                text += "; "

        if contrib["contrib_xml"] == "<etal/>":
            text += "et al."
        else:
            text += string_name
        i += 1

    if is_editor:
        if len(contribs) > 1:
            text += ", eds."
        else:
            text += ", ed."
    elif with_span:
        text += "</span>"

    return text


def get_html_from_pages(ref_data):
    ref_jep_style = getattr(settings, "REF_JEP_STYLE", False)
    if ref_jep_style and ref_data.type == "book":
        return ""

    text = helper_decorate_text(ref_data.size, ", ", " pages")

    if not text and ref_data.fpage:
        fpage_int = lpage_int = 0

        try:
            fpage_int = int(ref_data.fpage)
        except ValueError:
            pass

        try:
            lpage_int = int(ref_data.lpage)
        except ValueError:
            pass

        if lpage_int > 0 and lpage_int - fpage_int >= 1 and not ref_jep_style:
            text += ", pp. "
        else:
            text += ", p. "

        text += ref_data.fpage
        if ref_data.lpage:
            text += "-" + ref_data.lpage

    if not text and ref_data.page_range:
        prefix = ", pp. "
        suffix = ""
        if ref_jep_style:
            prefix = ", p. "
        text += helper_decorate_text(ref_data.page_range, prefix, suffix)

    return text


def get_html_from_source(ref_data):
    with_article_or_chapter_title = ref_data.article_title_tex or ref_data.chapter_title_tex
    value_html = add_prefix_to_html_from_source(ref_data)
    text = add_span_class_to_html_from_source(
        value_html,
        ref_type=ref_data.type,
        with_article_or_chapter_title=with_article_or_chapter_title,
    )

    return text


def add_prefix_to_html_from_annotation(ref_data):
    if ref_data.type in ["phdthesis", "masterthesis", "mastersthesis"]:
        prefix = ", "
        suffix = ""
    else:
        prefix = " ("
        suffix = ")"

    text = helper_decorate_text(ref_data.annotation, prefix, suffix)
    return text


def add_prefix_to_html_from_article_title(ref_data):
    prefix = " "
    suffix = ""
    ref_jep_style = getattr(settings, "REF_JEP_STYLE", False)

    if ref_jep_style:
        prefix = " - &ldquo;"
        suffix = "&rdquo;"

    text = helper_decorate_text(ref_data.article_title_tex, prefix, suffix)
    return text


def add_prefix_to_html_from_authors(ref_data: RefBase):
    contribs = ref_data.get_authors()
    text = get_html_from_contribs(contribs) if contribs else ""
    return text


def add_prefix_to_html_from_chapter_title(ref_data):
    prefix = " "
    suffix = ""
    ref_jep_style = getattr(settings, "REF_JEP_STYLE", False)

    if ref_jep_style:
        prefix = " - &ldquo;"
        suffix = "&rdquo;"

    text = helper_decorate_text(ref_data.chapter_title_tex, prefix, suffix)
    return text


def add_prefix_to_html_from_comment(ref_data):
    ref_jep_style = getattr(settings, "REF_JEP_STYLE", False)

    if ref_jep_style:
        prefix = ", "
        suffix = ""
    else:
        if "(" in ref_data.comment:
            prefix = " "
            suffix = ""
        else:
            prefix = " ("
            suffix = ")"

    text = helper_decorate_text(ref_data.comment, prefix, suffix)
    return text


def add_prefix_to_html_from_editors(ref_data):
    contribs = ref_data.get_editors()
    text = get_html_from_contribs(contribs, is_editor=True) if contribs else ""
    text = helper_decorate_text(text, " (", ")")
    return text


def add_prefix_to_html_from_eids(ref_data):
    text = ""
    ref_jep_style = getattr(settings, "REF_JEP_STYLE", False)

    for extid in ref_data.extids:
        if extid[0] == "eid":
            if ref_jep_style:
                text += ", article ID " + extid[1]
            elif REF_ALCO_STYLE:
                text += ", Paper no. " + extid[1]
            else:
                text += ", " + extid[1]
    return text


def add_prefix_to_html_from_ext_links(ref_data):
    text = ""
    ref_jep_style = getattr(settings, "REF_JEP_STYLE", False)

    for link in ref_data.ext_links:
        href = link["location"]

        # bibitem with ext-links pointing to numdam.org have a numdam-id
        # ext-links to doi.org are transformed in an extid
        # We can ignore both cases
        if "www.numdam.org" not in href and "doi.org" not in href and not ref_jep_style:
            href = make_links_clickable(href, link["metadata"])
            text += " " + href
    return text


def add_prefix_to_html_from_institution(ref_data):
    text = helper_decorate_text(ref_data.institution, ", ", "")
    return text


def add_prefix_to_html_from_label(ref_data):
    text = helper_decorate_text(ref_data.label, "", " ")
    return text


def add_prefix_to_html_from_month(ref_data):
    text = helper_decorate_text(ref_data.month, ", ", "")
    return text


def add_prefix_to_html_from_number(ref_data):
    text = helper_decorate_text(ref_data.issue, " no. ", "")
    return text


def add_prefix_to_html_from_publisher(ref_data):
    text = helper_decorate_text(ref_data.publisher_name, ", ", "")
    text += helper_decorate_text(ref_data.publisher_loc, ", ", "")
    return text


def add_prefix_to_html_from_series(ref_data):
    ref_jep_style = getattr(settings, "REF_JEP_STYLE", False)

    if ref_data.chapter_title_tex or ref_data.article_title_tex:
        if ref_jep_style and ref_data.type == "incollection":
            prefix = ", "
            suffix = ""
        else:
            prefix = " ("
            suffix = ")"
    else:
        prefix = ", "
        suffix = ""

    text = helper_decorate_text(ref_data.series, prefix, suffix)
    return text


def add_prefix_to_html_from_source(ref_data):
    ref_jep_style = getattr(settings, "REF_JEP_STYLE", False)
    ref_pcj_style = getattr(settings, "REF_PCJ_STYLE", False)
    if ref_data.article_title_tex or ref_data.chapter_title_tex:
        if ref_jep_style and ref_data.type == "incollection":
            prefix = ", in "
        elif ref_pcj_style and ref_data.type == "inbook":
            prefix = " In: "
        else:
            prefix = ", "
        suffix = ""
    else:
        prefix = " "
        suffix = ""

        if ref_jep_style:
            prefix = " - "
            if ref_data.type in ["unpublished", "misc"]:
                prefix += "&ldquo;"
                suffix = "&rdquo;"

    text = helper_decorate_text(ref_data.source_tex, prefix, suffix)
    return text


def add_prefix_to_html_from_volume(ref_data):
    ref_jep_style = getattr(settings, "REF_JEP_STYLE", False)

    if ref_jep_style:
        if ref_data.type in ["incollection", "book"]:
            prefix = ", vol. "
        else:
            prefix = " "
    else:
        if ref_data.article_title_tex or ref_data.chapter_title_tex:
            prefix = ", Volume "
        else:
            prefix = ", "

    text = helper_decorate_text(ref_data.volume, prefix, "")
    return text


def add_prefix_to_html_from_year(ref_data):
    ref_jep_style = getattr(settings, "REF_JEP_STYLE", False)

    if ref_jep_style and ref_data.type in ["phdthesis", "masterthesis", "mastersthesis"]:
        prefix = ", " if ref_data.month == "" else " "
        suffix = ""
    elif ref_data.type in ["incollection", "book", "misc"]:
        prefix = ", "
        suffix = ""
    else:
        prefix = " ("
        suffix = ")"

    text = helper_decorate_text(ref_data.year, prefix, suffix)
    return text


def add_span_class_to_html_from_article_title(value_html, **kwargs):
    text = helper_decorate_text(value_html, '<span class="citation-document-title">', "</span>")
    return text


def add_span_class_to_html_from_authors(value_html, **kwargs):
    text = helper_decorate_text(value_html, '<span class="citation-author">', "</span>")
    return text


def add_span_class_to_html_from_chapter_title(value_html, **kwargs):
    text = helper_decorate_text(value_html, '<span class="citation-document-title">', "</span>")
    return text


def add_span_class_to_html_from_series(value_html, **kwargs):
    # TODO: Check JEP for series (not span except for books: citation-publication-title-book (not found in CSS)
    text = helper_decorate_text(value_html, '<span class="citation-series">', "</span>")
    return text


def add_span_class_to_html_from_source(value_html, **kwargs):
    suffix = "</span>"

    if "with_article_or_chapter_title" not in kwargs or kwargs["with_article_or_chapter_title"]:
        prefix = '<span class="citation-publication-title">'
    else:
        prefix = '<span class="citation-document-title">'

    text = helper_decorate_text(value_html, prefix, suffix)
    return text


def add_span_class_to_html_from_volume(value_html, **kwargs):
    ref_jep_style = getattr(settings, "REF_JEP_STYLE", False)

    if ref_jep_style and "ref_type" in kwargs and kwargs["ref_type"] in ["incollection", "book"]:
        text = helper_decorate_text(
            value_html, '<span class="citation-volume-incollection">', "</span>"
        )
    else:
        text = helper_decorate_text(value_html, '<span class="citation-volume">', "</span>")
    return text


def get_citation_for_article(ref_data):
    text = get_html_from("authors", ref_data)
    text += get_html_from("article_title", ref_data)
    text += get_html_from("source", ref_data)
    text += get_html_from("series", ref_data)
    text += get_html_from("volume", ref_data)
    text += get_html_from("year", ref_data)
    text += get_html_from("number", ref_data)
    text += get_html_from("eids", ref_data)
    text += get_html_from("pages", ref_data)
    text += get_html_from("ext_links", ref_data)
    return text


def get_citation_for_book(ref_data):
    text = get_html_from("authors", ref_data)
    text += get_html_from("source", ref_data)
    text += get_html_from("editors", ref_data)
    text += get_html_from("series", ref_data)
    text += get_html_from("volume", ref_data)
    text += get_html_from("publisher", ref_data)
    text += get_html_from("institution", ref_data)
    text += get_html_from("year", ref_data)
    text += get_html_from("number", ref_data)
    text += get_html_from("eids", ref_data)
    text += get_html_from("pages", ref_data)
    text += get_html_from("ext_links", ref_data)
    return text


def get_citation_for_incollection(ref_data):
    text = get_html_from("authors", ref_data)

    # <title> becomes a <chapter-title> in JATS for inbook,
    # but becomes a <source> for incollection.
    # We can call get_html_from_chapter_title then get_html_from_source because 1 of them will be empty in both cases
    text += get_html_from("chapter_title", ref_data)
    text += get_html_from("source", ref_data)

    # TODO: BUG in JATS; editors are not displayed for inbook
    if ref_data.type != "inbook":
        text += get_html_from("editors", ref_data)

    text += get_html_from("series", ref_data)
    text += get_html_from("volume", ref_data)
    text += get_html_from("publisher", ref_data)
    text += get_html_from("institution", ref_data)
    text += get_html_from("year", ref_data)
    text += get_html_from("number", ref_data)
    text += get_html_from("eids", ref_data)
    text += get_html_from("pages", ref_data)
    text += get_html_from("ext_links", ref_data)
    return text


def get_citation_for_misc(ref_data):
    text = get_html_from("authors", ref_data)

    # TODO: BUG in JATS ? article_title is used for misc but source for unpublished ?
    text += get_html_from("article_title", ref_data)
    text += get_html_from("source", ref_data)
    text += get_html_from("series", ref_data)
    text += get_html_from("volume", ref_data)
    text += get_html_from("year", ref_data)
    text += get_html_from("number", ref_data)
    text += get_html_from("eids", ref_data)
    text += get_html_from("pages", ref_data)
    text += get_html_from("ext_links", ref_data)
    return text


def get_citation_for_thesis(ref_data):
    text = get_html_from("authors", ref_data)
    text += get_html_from("source", ref_data)
    text += get_html_from("series", ref_data)
    text += get_html_from("volume", ref_data)

    text += get_html_from("annotation", ref_data)
    text += get_html_from("institution", ref_data)
    text += get_html_from("publisher", ref_data)
    text += get_html_from("month", ref_data)
    text += get_html_from("year", ref_data)
    text += get_html_from("number", ref_data)

    text += get_html_from("eids", ref_data)
    text += get_html_from("pages", ref_data)
    text += get_html_from("ext_links", ref_data)

    return text


def get_citation_html(ref_data):
    text = get_html_from("label", ref_data)

    # article - book - incollection inbook - thesis phdthesis masterthesis - misc unpublished
    # manual techreport coursenotes proceedings ?
    type_ = ref_data.type

    if type_ in ("inbook", "inproceedings"):
        type_ = "incollection"
    elif "thesis" in type_:
        type_ = "thesis"
    elif type_ not in ("article", "book", "incollection"):
        type_ = "misc"

    fct_name = "get_citation_for_" + type_.replace("-", "_")
    if fct_name in globals() and callable(globals()[fct_name]):
        text += globals()[fct_name](ref_data)

    text += get_html_from("comment", ref_data)

    if ref_data.type not in ["phdthesis", "masterthesis", "mastersthesis"]:
        text += get_html_from("annotation", ref_data)

    return text
