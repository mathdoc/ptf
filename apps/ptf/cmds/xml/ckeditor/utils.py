##################################################################################################
#
# Utils functions to parse strings with HTML
#
##################################################################################################

from .ckeditor_parser import CkeditorParser


def replace_text_with_formula(text, delimiter, is_disp=False):
    result = ""
    cur_pos = begin_pos = 0

    delimiter_end = delimiter
    if delimiter_end == "\\(":
        delimiter_end = "\\)"
    elif delimiter_end == "\\[":
        delimiter_end = "\\]"

    begin_pos = text.find(delimiter, begin_pos)

    while begin_pos > -1:
        end_pos = text.find(delimiter_end, begin_pos + 1)
        if end_pos > -1:
            result += text[cur_pos:begin_pos]
            if is_disp:
                result += "<div><p>"
            result += (
                '<span class="mathjax-formula">'
                + text[begin_pos + len(delimiter) : end_pos]
                + "</span>"
            )
            if is_disp:
                result += "</p></div>"

            begin_pos = cur_pos = end_pos + len(delimiter_end)
            begin_pos = text.find(delimiter, begin_pos)
        else:
            begin_pos = -1

    result += text[cur_pos:]
    return result


def get_html_and_xml_from_text_with_formulas(text, delimiter_inline="$", delimiter_disp="$"):
    """
    1. Detect formulas inside '$' '$' (or other delimiter) and replace with <span class="mathjax-formula"> and </span>
    2. Call CkeditorParser to get the html and xml
    """

    text = replace_text_with_formula(text, delimiter_inline)
    if delimiter_inline != delimiter_disp:
        text = replace_text_with_formula(text, delimiter_disp, is_disp=True)

    xobj = CkeditorParser(html_value=text, mml_formulas=[], ignore_p=True)
    return xobj.value_html, xobj.value_xml
