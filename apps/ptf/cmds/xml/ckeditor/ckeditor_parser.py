##################################################################################################
#
# README
#
# ckeditor_parser.py parses the HTML strings created by a CKEditor
# with tex formulas inside <span class="math-tex">
# It returns the JATS equivalent.
#
# Ex: <p>Te&lt;st&nbsp;<span class="math-tex">\(x = {-b \pm \sqrt{b^2-4ac} \over 2a}\)</span> done</p>
#     <ul><li>Item</li></ul><ol><li>Item 1<br />New line</li><li>&nbsp;</li></ol>
#
##################################################################################################

if __name__ == "__main__":
    import os
    import sys

    BASE_DIR = os.path.dirname(
        os.path.dirname(
            os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
        )
    )
    sys.path.append(BASE_DIR)

import os

from lxml import etree

from django.conf import settings

from ptf.cmds.xml.xml_utils import escape
from ptf.cmds.xml.xml_utils import normalize
from ptf.cmds.xml.xml_utils import replace_html_entities

# from ptf.utils import create_innerlink_for_citation


class CkeditorParser:
    def __init__(self, *args, **kwargs):
        self.warnings = []
        self.value_xml = ""
        self.value_html = ""
        self.value_tex = ""

        if "tree" not in kwargs and "html_value" in kwargs:
            parser = etree.XMLParser(
                huge_tree=True,
                recover=True,
                remove_blank_text=False,
                remove_comments=True,
                resolve_entities=True,
            )
            html_value = kwargs["html_value"].replace("\n\n", "")
            body = f"<body>{replace_html_entities(html_value)}</body>"
            tree = etree.fromstring(body.encode("utf-8"), parser=parser)
        else:
            tree = kwargs["tree"]

        self.mml_formulas = kwargs["mml_formulas"]
        self.ignore_p = kwargs["ignore_p"] if "ignore_p" in kwargs else False
        self.pid = kwargs.get("pid", None)
        self.volume = kwargs.get("volume", None)
        self.issue_pid = kwargs.get("issue_pid", None)
        self.check_citation = kwargs.get("check_citation", False)
        self.biblio = kwargs.get("biblio", None)
        self.for_pcj_display = kwargs.get("for_pcj_display", False)

        self.parse_tree(tree)

    def parse_formula(self, node, **kwargs):
        formula = node.text or ""
        display = kwargs.get("display", None)
        if len(formula) > 0 and formula.find("\\(") == 0:
            formula = formula[2:-2]
        # elif len(formula) > 0 and formula.find("\[") == 0:
        #     formula = formula[1:-1]
        mml = ""
        if len(self.mml_formulas) > 0:
            mml = self.mml_formulas.pop(0)

        is_inline = True
        parent = node.getparent()
        if parent is not None and parent.tag == "p" and not parent.text and not parent.tail:
            is_inline = False
        if self.for_pcj_display:
            formula = rf"\({formula}\)"
        else:
            formula = f"${formula}$"
        if mml:
            html_text = f'<span class="mathjax-formula" title="{formula}">{mml}</span>'
        elif display:
            html_text = f'<span class="mathjax-formula display" title="{formula}">{formula}</span>'
        else:
            html_text = f'<span class="mathjax-formula" title="{formula}">{formula}</span>'
        tex_text = formula

        if is_inline:
            xml_text = "<inline-formula><alternatives>"
            if len(mml) > 0:
                xml_text += mml
            xml_text += f"<tex-math>{escape(formula)}</tex-math>"
            xml_text += "</alternatives></inline-formula>"
        else:
            prefix = '<table class="formula mathjax-formula"><tr><td class="formula-inner">'
            suffix = '</td><td class="formula-label"></td></tr></table>'
            html_text = prefix + html_text + suffix
            tex_text = prefix + tex_text + suffix

            xml_text = '<disp-formula xml:space="preserve">\n<alternatives>'
            if len(mml) > 0:
                xml_text += mml
            xml_text += f"<tex-math>{escape(formula)}</tex-math>"
            xml_text += "</alternatives></disp-formula>"

        return html_text, tex_text, xml_text

    def parse_list(self, node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, **kwargs
        )

        list_type = "simple" if node.tag == "ul" else "number"

        xml_text = f'<list list-type="{list_type}">'
        xml_text += inner_jats_xml_text
        xml_text += "</list>"

        # # JATS requires <list> to be inside <p>
        # parent = node.getparent()
        # if parent is None or parent.tag != "p":
        #     xml_text = f"<p>{xml_text}</p>"
        html_text = f"<{node.tag}>{inner_html_text}</{node.tag}>"
        tex_text = f"<{node.tag}>{inner_tex_text}</{node.tag}>"

        return html_text, tex_text, xml_text

    def parse_node_inner(self, node, **kwargs):
        """
        Used by parse_node_with_mixed_content for nodes that have a different tag in JATS or HTML
        :param node:
        :param kwargs:
        :return:
        """

        kwargs["is_top"] = False
        inner_html_text = inner_tex_text = inner_jats_xml_text = ""

        if node.text:
            text = node.text

            if len(text) > 0 and text[0] == "\n" and node.tag in ("list", "item"):
                text = text[1:]

            inner_jats_xml_text += escape(text)
            inner_html_text += escape(text) if kwargs["escape"] else text
            inner_tex_text += escape(text) if kwargs["escape"] else text

        # if self.check_citation and node.tag != "a":
        #     inner_html_text = create_innerlink_for_citation(inner_html_text, self.biblio)

        for i in range(len(node)):
            child = node[i]

            (
                child_html_text,
                child_tex_text,
                child_jats_xml_text,
            ) = self.parse_node_with_mixed_content(child, **kwargs)
            inner_html_text += child_html_text
            inner_tex_text += child_tex_text
            inner_jats_xml_text += child_jats_xml_text

        return inner_html_text, inner_tex_text, inner_jats_xml_text

    def parse_node_with_a(self, node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, **kwargs
        )

        href = ""
        for attrib in node.attrib:
            name = normalize(attrib)
            if name == "href":
                href = node.attrib[attrib]

        if not href:
            href = inner_tex_text

        html_text = f'<a href="{href}">{inner_html_text}</a>'
        tex_text = f'<a href="{href}">{inner_tex_text}</a>'
        xml_text = f'<ext-link ext-link-type="uri" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{href}">{inner_jats_xml_text}</ext-link>'

        return html_text, tex_text, xml_text

    def parse_node_with_br(self, node, **kwargs):
        html_text = tex_text = "<br/>"
        xml_text = "<break/>"

        return html_text, tex_text, xml_text

    def parse_node_with_colgroup(self, node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, **kwargs
        )
        classe = ""
        for attrib in node.attrib:
            name = normalize(attrib)
            if name == "class":
                classe = node.attrib[name]
        html_text = f"<colgroup class={classe}>{inner_html_text}</colgroup>"
        tex_text = f"<colgroup class={classe}>{inner_tex_text}</colgroup>"

        xml_text = '<colgroup xml:space="preserve">' + inner_jats_xml_text + "</colgroup>"
        return html_text, tex_text, xml_text

    def parse_node_with_col(self, node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, **kwargs
        )
        classe = ""
        style = ""
        for attrib in node.attrib:
            name = normalize(attrib)
            if name == "class":
                classe = node.attrib[name]
            elif name == "style":
                style = node.attrib[name]
        if classe:
            html_text = f"<col class={classe} style='{style}'>{inner_html_text}</col>"
            tex_text = f"<col class={classe} style='{style}'>{inner_tex_text}</col>"
        else:
            html_text = f"<col style='{style}'>{inner_html_text}</col>"
            tex_text = f"<col style='{style}'>{inner_tex_text}</col>"

        xml_text = '<col xml:space="preserve">' + inner_jats_xml_text + "</col>"
        return html_text, tex_text, xml_text

    def parse_node_with_div(self, node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, **kwargs
        )
        classe = ""
        for attrib in node.attrib:
            name = normalize(attrib)
            if name == "class":
                classe = node.attrib[name]
            # Next condition checks style identification with pandoc library used
            # for docx --> html conversion
            elif name == "data-custom-style":
                if node.attrib[name] == "PCJ Equation":
                    classe = "mathjax-formula PCJ-Equation"
                else:
                    classe = node.attrib[name].replace(" ", "-")
        if classe == "PCJ-Section" and "References" in inner_html_text:
            html_text = tex_text = xml_text = ""
            return html_text, tex_text, xml_text
        elif classe == "PCJ-Reference":
            html_text = tex_text = xml_text = ""
            return html_text, tex_text, xml_text

        html_text = f"<div class='{classe}'>{inner_html_text}</div>"
        tex_text = f"<div class='{classe}'>{inner_tex_text}</div>"

        xml_text = '<div xml:space="preserve">' + inner_jats_xml_text + "</div>"
        return html_text, tex_text, xml_text

    def parse_node_with_em(self, node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, **kwargs
        )

        html_text = f'<span class="italique">{inner_html_text}</span>'
        tex_text = f"<i>{inner_tex_text}</i>"

        if len(inner_jats_xml_text) > 0:
            xml_text = f"<italic>{inner_jats_xml_text}</italic>"
        else:
            xml_text = "<italic/>"

        return html_text, tex_text, xml_text

    def parse_node_with_h1(self, node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, **kwargs
        )
        classe = ""
        for attrib in node.attrib:
            name = normalize(attrib)
            if name == "class":
                classe = node.attrib[name]
        html_text = f"<h1 class={classe}>{inner_html_text}</h1>"
        tex_text = f"<h1 class={classe}>{inner_tex_text}</h1>"

        xml_text = '<h1 xml:space="preserve">' + inner_jats_xml_text + "</h1>"

        return html_text, tex_text, xml_text

    def parse_node_with_h2(self, node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, **kwargs
        )
        classe = ""
        for attrib in node.attrib:
            name = normalize(attrib)
            if name == "class":
                classe = node.attrib[name]
        html_text = f"<h2 class={classe}>{inner_html_text}</h2>"
        tex_text = f"<h2 class={classe}>{inner_tex_text}</h2>"

        xml_text = '<h2 xml:space="preserve">' + inner_jats_xml_text + "</h2>"

        return html_text, tex_text, xml_text

    def parse_node_with_h3(self, node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, **kwargs
        )
        classe = ""
        for attrib in node.attrib:
            name = normalize(attrib)
            if name == "class":
                classe = node.attrib[name]
        html_text = f"<h3 class={classe}>{inner_html_text}</h3>"
        tex_text = f"<h3 class={classe}>{inner_tex_text}</h3>"

        xml_text = '<h3 xml:space="preserve">' + inner_jats_xml_text + "</h3>"

        return html_text, tex_text, xml_text

    def parse_node_with_h4(self, node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, **kwargs
        )
        classe = ""
        for attrib in node.attrib:
            name = normalize(attrib)
            if name == "class":
                classe = node.attrib[name]
        html_text = f"<h4 class={classe}>{inner_html_text}</h4>"
        tex_text = f"<h4 class={classe}>{inner_tex_text}</h4>"

        xml_text = '<h4 xml:space="preserve">' + inner_jats_xml_text + "</h4>"
        return html_text, tex_text, xml_text

    def parse_node_with_h5(self, node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, **kwargs
        )
        classe = ""
        for attrib in node.attrib:
            name = normalize(attrib)
            if name == "class":
                classe = node.attrib[name]
        html_text = f"<h5 class={classe}>{inner_html_text}</h5>"
        tex_text = f"<h5 class={classe}>{inner_tex_text}</h5>"

        xml_text = '<h5 xml:space="preserve">' + inner_jats_xml_text + "</h5>"

        return html_text, tex_text, xml_text

    def parse_node_with_h6(self, node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, **kwargs
        )
        classe = ""
        for attrib in node.attrib:
            name = normalize(attrib)
            if name == "class":
                classe = node.attrib[name]
        html_text = f"<h6 class={classe}>{inner_html_text}</h6>"
        tex_text = f"<h6 class={classe}>{inner_tex_text}</h6>"

        xml_text = '<h6 xml:space="preserve">' + inner_jats_xml_text + "</h6>"
        return html_text, tex_text, xml_text

    def parse_node_with_img(self, node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, **kwargs
        )

        # node.attribe["style"] = ""
        try:
            prefix = settings.SITE_URL_PREFIX
        except AttributeError:
            prefix = ""

        # src = f"{prefix}/media/img/{self.volume}/{self.pid}/src/media"
        src = f"{prefix}/media/img/{self.issue_pid}/{self.pid}/src/media"
        href = ""
        classe = ""
        for attrib in node.attrib:
            name = normalize(attrib)
            if name == "src":
                img = os.path.basename(node.attrib[name])
                name, ext = os.path.splitext(img)
                # If an image was convreted to jpg, pandoc still wrote the html with the previous extension,
                # '.tiff' for exemple
                if ext in [".tiff", ".tif"]:
                    img = name + ".jpg"
                src = f"{src}/{img}"
            elif name == "style":
                classe = "article-body-img"
            elif name == "data-custom-style":
                classe = node.attrib[name].replace(" ", "-")

        html_text = f"<img src={src} class={classe}>{inner_html_text}</img>"
        tex_text = f"<img src={src} class={classe}>{inner_html_text}</img>"
        xml_text = f'<graphic xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{href}">{inner_jats_xml_text}</graphic>'

        return html_text, tex_text, xml_text

    def parse_node_with_li(self, node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, **kwargs
        )
        parent_node = node.getparent()
        if parent_node.tag == "ul":
            html_text = f"<li >{inner_html_text}</li>"
            tex_text = f"<li >{inner_tex_text}</li>"
        else:
            html_text = f"<li class='article-list'>{inner_html_text}</li>"
            tex_text = f"<li class='article-list'>{inner_tex_text}</li>"

        xml_text = f"<list-item><p>{inner_jats_xml_text}</p></list-item>"

        return html_text, tex_text, xml_text

    def parse_node_with_mixed_content(self, node, **kwargs):
        """
        Parse and return the text of an XML node which mixes text and XML sub-nodes.
        Ex: <node>text1 <a>text_a</a> text2 <b>text_b</b>b_tail</node>
        Some inner nodes are removed, others are kept or replaced.

        Cedrics XMLs store the MathML and the TeX formulas in 2 siblings.
        Parse the 2 nodes at the same time.

        The JATS xml string is constructed at the same time because it is used during a PTF export

        :param node: XML Node (with MathML), XML Node (with TexMath)
        :param kwargs: params of the function
        :return: HTML text, TeX test, XML text
        """

        html_text = tex_text = jats_xml_text = ""

        if node is None:
            return html_text, tex_text, jats_xml_text

        # The tail is the text following the end of the node
        # Ex: <node>text1<a>text_a</a>a_tail</node>
        # The HTML text has to include the tail
        #   only if html_from_mixed_content was called recursively
        kwargs["is_top"] = kwargs["is_top"] if "is_top" in kwargs else True

        # lxml replace HTML entities in node.tex and node.tail (like &lt;)
        # kwargs['escape'] allows to escape back the values
        kwargs["escape"] = kwargs["escape"] if "escape" in kwargs else True

        tag = node.tag

        inner_html_text = inner_tex_text = inner_jats_xml_text = ""

        # I. Add the node's text.
        # Some tag have a corresponding html_from_@tag function to generate the HTML text.

        fct_name = tag
        fct_name = "parse_node_with_" + fct_name.replace("-", "_")
        ftor = getattr(self, fct_name, None)
        if callable(ftor):
            inner_html_text, inner_tex_text, inner_jats_xml_text = ftor(node, **kwargs)
        else:
            inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
                node, **kwargs
            )

        html_text += inner_html_text
        tex_text += inner_tex_text
        jats_xml_text += inner_jats_xml_text

        # III. Add the node's tail for children
        if node.tail:
            # if self.check_citation and node.tag != "a":
            #     html_text = create_innerlink_for_citation(html_text, self.biblio)
            # node.tail = create_innerlink_for_citation(node.tail, self.biblio)
            if self.check_citation:
                kwargs["escape"] = False
            html_text += escape(node.tail) if kwargs["escape"] else node.tail
            tex_text += escape(node.tail) if kwargs["escape"] else node.tail
            jats_xml_text += escape(node.tail)

        # if self.check_citation and node.tag != "a":
        #         html_text = create_innerlink_for_citation(html_text, self.biblio)

        return html_text, tex_text, jats_xml_text

    def parse_node_with_ol(self, node, **kwargs):
        # # JATS requires <list> to be inside <p>
        # parent = node.getparent()
        # if parent is None or parent.tag != "p":
        #     xml_text = f"<p>{xml_text}</p>"

        return self.parse_list(node, **kwargs)

    def parse_node_with_p(self, node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, **kwargs
        )

        html_text = inner_html_text if self.ignore_p else f"<p>{inner_html_text}</p>"
        tex_text = inner_tex_text if self.ignore_p else f"<p>{inner_tex_text}</p>"
        if self.ignore_p:
            xml_text = inner_jats_xml_text
        elif len(inner_jats_xml_text) > 0:
            xml_text = '<p xml:space="preserve">' + inner_jats_xml_text + "</p>"
        else:
            xml_text = '<p xml:space="preserve"/>'

        return html_text, tex_text, xml_text

    def parse_node_with_span(self, node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, **kwargs
        )

        the_class = node.get("class")
        display = the_class == "math display"
        if the_class in ["math inline", "math display"]:
            the_class = "mathjax-formula"

        if the_class == "mathjax-formula":
            html_text, tex_text, xml_text = self.parse_formula(node, display=display)
        elif the_class is not None:
            html_text = f'<span class="{the_class}">{inner_html_text}</span>'
            tex_text = f'<span class="{the_class}">{inner_tex_text}</span>'
            xml_text = inner_jats_xml_text
        else:
            html_text = f"<span>{inner_html_text}</span>"
            tex_text = f"<span>{inner_tex_text}</span>"
            xml_text = inner_jats_xml_text

        return html_text, tex_text, xml_text

    def parse_node_with_strong(self, node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, **kwargs
        )

        html_text = f"<strong>{inner_html_text}</strong>"
        tex_text = f"<strong>{inner_tex_text}</strong>"

        if len(inner_jats_xml_text) > 0:
            xml_text = f"<bold>{inner_jats_xml_text}</bold>"
        else:
            xml_text = "<bold/>"

        return html_text, tex_text, xml_text

    def parse_node_with_sub(self, node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, **kwargs
        )

        html_text = f"<sub>{inner_html_text}</sub>"
        tex_text = f"<sub>{inner_tex_text}</sub>"
        xml_text = f"<sub>{inner_jats_xml_text}</sub>"

        return html_text, tex_text, xml_text

    def parse_node_with_sup(self, node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, **kwargs
        )

        html_text = f"<sup>{inner_html_text}</sup>"
        tex_text = f"<sup>{inner_tex_text}</sup>"
        xml_text = f"<sup>{inner_jats_xml_text}</sup>"

        return html_text, tex_text, xml_text

    def parse_node_with_table(self, node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, **kwargs
        )
        classe = ""
        for attrib in node.attrib:
            name = normalize(attrib)
            if name == "class":
                classe = node.attrib[name]
            # Next condition checks style identification with pandoc library used
            # for docx --> html conversion
            elif name == "data-custom-style":
                classe = node.attrib[name].replace(" ", "-")
        if "PCJ" in self.issue_pid:
            html_text = (
                f"<div class='PCJ-table'><table class={classe}>{inner_html_text}</table></div>"
            )
            tex_text = (
                f"<div class='PCJ-table'><table class={classe}>{inner_tex_text}</table></div>"
            )
        else:
            html_text = f"<table class={classe}>{inner_html_text}</table>"
            tex_text = f"<table class={classe}>{inner_tex_text}</table>"

        xml_text = '<table xml:space="preserve">' + inner_jats_xml_text + "</table>"
        return html_text, tex_text, xml_text

    def parse_node_with_tbody(self, node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, **kwargs
        )
        classe = ""
        for attrib in node.attrib:
            name = normalize(attrib)
            if name == "class":
                classe = node.attrib[name]
        html_text = f"<tbody class={classe}>{inner_html_text}</tbody>"
        tex_text = f"<tbody class={classe}>{inner_tex_text}</tbody>"

        xml_text = '<tbody xml:space="preserve">' + inner_jats_xml_text + "</tbody>"
        return html_text, tex_text, xml_text

    def parse_node_with_td(self, node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, **kwargs
        )
        classe = ""
        rowspan = ""
        colspan = ""
        for attrib in node.attrib:
            name = normalize(attrib)
            if name == "class":
                classe = node.attrib[name]
            elif name == "rowspan":
                rowspan = node.attrib[name]
            elif name == "colspan":
                colspan = node.attrib[name]
        if classe:
            html_text = f"<td class={classe} rowspan='{rowspan}' colspan='{colspan}'>{inner_html_text}</td>"
            tex_text = (
                f"<td class={classe} rowspan='{rowspan}' colspan='{colspan}'>{inner_tex_text}</td>"
            )
        else:
            html_text = f"<td  rowspan='{rowspan}' colspan='{colspan}'>{inner_html_text}</td>"
            tex_text = f"<td  rowspan='{rowspan}' colspan='{colspan}'>{inner_tex_text}</td>"

        xml_text = '<td xml:space="preserve">' + inner_jats_xml_text + "</td>"
        return html_text, tex_text, xml_text

    def parse_node_with_th(self, node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, **kwargs
        )
        classe = ""
        rowspan = ""
        colspan = ""
        for attrib in node.attrib:
            name = normalize(attrib)
            if name == "class":
                classe = node.attrib[name]
            elif name == "rowspan":
                rowspan = node.attrib[name]
            elif name == "colspan":
                colspan = node.attrib[name]
        if classe:
            html_text = f"<th class={classe} rowspan='{rowspan}' colspan='{colspan}'>{inner_html_text}</th>"
            tex_text = (
                f"<th class={classe} rowspan='{rowspan}' colspan='{colspan}'>{inner_tex_text}</th>"
            )
        else:
            html_text = f"<th  rowspan='{rowspan}' colspan='{colspan}'>{inner_html_text}</th>"
            tex_text = f"<th  rowspan='{rowspan}' colspan='{colspan}'>{inner_tex_text}</th>"

        xml_text = '<th xml:space="preserve">' + inner_jats_xml_text + "</th>"
        return html_text, tex_text, xml_text

    def parse_node_with_tr(self, node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, **kwargs
        )
        classe = ""

        html_text = f"<tr class='{classe}'>{inner_html_text}</tr>"
        tex_text = f"<tr class='{classe}'>{inner_tex_text}</tr>"

        xml_text = '<tr xml:space="preserve">' + inner_jats_xml_text + "</tr>"
        return html_text, tex_text, xml_text

    def parse_node_with_ul(self, node, **kwargs):
        return self.parse_list(node, **kwargs)

    def parse_tree(self, tree):
        self.value_html, self.value_tex, self.value_xml = self.parse_node_with_mixed_content(
            tree, is_top=True
        )
        # if self.check_citation:
        #     self.value_html = create_innerlink_for_citation(self.value_html, self.biblio)


if __name__ == "__main__":
    html_value = r'<p>Te&lt;st&nbsp;<span class="mathjax-formula">\(x = {-b \pm \sqrt{b^2-4ac} \over 2a}\)</span> done</p><ul><li>Item</li></ul><ol><li>Item 1<br />New line</li><li>&nbsp;</li></ol>'
    parser = CkeditorParser(html_value=html_value)
    result = parser.value_xml
    print(result)
