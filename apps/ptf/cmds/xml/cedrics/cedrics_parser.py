##################################################################################################
#
# README
#
# cedrics_parser.py is the equivalent of jats_parser for Cedrics XML
#
# Bugs fixed:
# - <xref> with url in "dx.doi.org" were filtered in ptf-xsl
# - Non structured references (bibitemdata in Cedrics) got ext_links only if the <xref> node
#     is one step below the <bibitemdata> node
# - comments that started with ' ' were ignored (AIF_2008__58_2_689_0 [9])
#
##################################################################################################

import html
import re
from operator import attrgetter

from django.conf import settings
from django.utils import timezone

from ptf.cmds.xml.citation_html import get_citation_html
from ptf.cmds.xml.xml_base import RefBase
from ptf.cmds.xml.xml_base import XmlParserBase
from ptf.cmds.xml.xml_utils import clean_doi
from ptf.cmds.xml.xml_utils import escape
from ptf.cmds.xml.xml_utils import fix_mfenced_in_mathml
from ptf.cmds.xml.xml_utils import get_contrib_xml
from ptf.cmds.xml.xml_utils import get_normalized_attrib
from ptf.cmds.xml.xml_utils import get_text_from_node
from ptf.cmds.xml.xml_utils import get_xml_from_node
from ptf.cmds.xml.xml_utils import helper_update_name_params
from ptf.cmds.xml.xml_utils import int_to_Roman
from ptf.cmds.xml.xml_utils import make_links_clickable
from ptf.cmds.xml.xml_utils import normalize
from ptf.cmds.xml.xml_utils import normalize_space
from ptf.cmds.xml.xml_utils import replace_html_entities
from ptf.cmds.xml.xml_utils import split_kwds
from ptf.model_data import ArticleData
from ptf.model_data import Foo
from ptf.model_data import IssueData
from ptf.model_data import JournalData
from ptf.model_data import PublisherData
from ptf.model_data import create_contributor


def helper_add_link_from_node(node):
    text = node.text or ""
    tag = normalize(node.tag)
    fct_name = "get_data_from_" + tag.replace("-", "_")
    data = globals()[fct_name](node)
    if not data["rel"]:
        href = data["location"]
        if "www.numdam.org" not in href:
            text = make_links_clickable(href, data["metadata"])
        else:
            text = ""
    return text


def get_data_from_custom_meta(node):
    name = ""
    value = ""

    for child in node:
        tag = normalize(child.tag)

        if tag == "meta-name":
            name = child.text
        elif tag == "meta-value":
            value = child.text

    return name, value


def get_data_from_date(node):
    date_str = ""
    if "iso-8601-date" in node.attrib:
        date_str = node.attrib["iso-8601-date"]
    else:
        year = month = day = ""
        for child in node:
            tag = normalize(child.tag)

            if tag == "year":
                year = child.text
            elif tag == "month":
                month = child.text
            elif tag == "day":
                day = child.text
        date_str = year
        if date_str and month:
            date_str += "-" + month
        if date_str and day:
            date_str += "-" + day

    return date_str


def get_data_from_ext_link(node):
    link_type = node.get("ext-link-type") or ""
    href = get_normalized_attrib(node, "href") or ""
    base = get_normalized_attrib(node, "base") or ""

    data = {
        "rel": link_type,
        "mimetype": "",
        "location": href,
        "base": base,
        "metadata": node.text or "",
    }

    return data


def get_data_from_history(node):
    history_dates = []
    # TODO: transform history_dates in a hash where date-type is the key
    #       => Change database_cmds
    for child in node:
        if "date-type" in child.attrib:
            date_type = child.attrib["date-type"]
            date_str = get_data_from_date(child)
            history_dates.append({"type": date_type, "date": date_str})
    return history_dates


def get_data_from_uri(node):
    href = text = ""
    href = get_normalized_attrib(node, "href") or ""
    text = node.text or ""

    data = {"rel": "", "mimetype": "", "location": href, "base": "", "metadata": text}

    return data


class CedricsBase(XmlParserBase):
    def __init__(self, *args, **kwargs):
        super().__init__()
        self.warnings = []

    def parse_tree(self, tree):
        pass

    def set_titles(self):
        pass

    def post_parse_tree(self):
        self.set_titles()

    def filter_text(self, text):
        text = text.replace("<allowbreak/>", "")
        return text

    def get_location_from_xref(self, node, **kwargs):
        location = get_normalized_attrib(node, "url") or ""

        if location == "":
            text = get_text_from_node(node)
            location = self.filter_text(text)

        return location

    def get_data_from_xref(self, node, **kwargs):
        href = text = ""

        href = get_normalized_attrib(node, "url") or ""

        # TODO: BUG in JATS. JEP_2017__4__435_0 [9]
        # The comment has an ext-link with a display embedded in <monospace>
        # jats_parser produces 2 <a> (1 for the <ext-link>, 1 for the text inside the <monospace>
        # The code below should be removed
        is_comment = "is_comment" in kwargs and kwargs["is_comment"]
        if is_comment and node.text is None:
            kwargs["add_HTML_link"] = True

        html_text, _, xml_text = self.parse_node_inner(node, None, **kwargs)

        is_bibitemdata = kwargs["is_bibitemdata"] if "is_bibitemdata" in kwargs else False

        if href == "":
            text = get_text_from_node(node)
            text = self.filter_text(text)
            href = text

        bibitemdata_display = html_text
        if is_bibitemdata and node.text is None:
            html_text = ""

        data = {
            "rel": "",
            "mimetype": "",
            "location": href,
            "base": "",
            "metadata": html_text,
            "xml_text": xml_text,
        }

        if is_bibitemdata:
            data["bibitemdata_display"] = bibitemdata_display

        return data

    def get_numeric_value(self, node):
        systnum = node.get("systnum") or ""

        value = node.text
        if systnum.lower() == "romain":
            value = int_to_Roman(int(value))

        return value

    def parse_node_inner(self, node, tex_node, **kwargs):
        """
        Used by parse_node_with_mixed_content for nodes that have a different tag in JATS or HTML
        :param node:
        :param kwargs:
        :return:
        """

        kwargs["is_top"] = False
        inner_html_text = inner_tex_text = inner_jats_xml_text = ""

        if node.text:
            text = node.text

            if len(text) > 0 and text[0] == "\n" and node.tag in ("list", "item"):
                text = text[1:]

            inner_jats_xml_text += escape(text)
            inner_html_text += text
            inner_tex_text += text

        for i in range(len(node)):
            child = node[i]
            text_child = tex_node[i] if (tex_node is not None and len(tex_node) > i) else None

            (
                child_html_text,
                child_tex_text,
                child_jats_xml_text,
            ) = self.parse_node_with_mixed_content(child, text_child, **kwargs)
            inner_html_text += child_html_text
            inner_tex_text += child_tex_text
            inner_jats_xml_text += child_jats_xml_text

        if "add_HTML_link" in kwargs and kwargs["add_HTML_link"]:
            match = re.match(r"[\n ]+", inner_html_text)
            if not match:
                inner_html_text = make_links_clickable(inner_html_text, inner_html_text)

        return inner_html_text, inner_tex_text, inner_jats_xml_text

    def parse_node_with_b(self, node, tex_node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, tex_node, **kwargs
        )

        html_text = "<strong>" + inner_html_text + "</strong>"
        tex_text = "<strong>" + inner_tex_text + "</strong>"
        if len(inner_jats_xml_text) > 0:
            xml_text = "<bold>" + inner_jats_xml_text + "</bold>"
        else:
            xml_text = "<bold/>"

        return html_text, tex_text, xml_text

    def parse_node_with_cit(self, node, tex_node, **kwargs):
        html_text = tex_text = get_text_from_node(node)
        xml_text = escape(html_text)

        return html_text, tex_text, xml_text

    def parse_node_with_hi(self, node, tex_node, **kwargs):
        rend = node.get("rend")

        if rend == "it":
            return self.parse_node_with_i(node, tex_node, **kwargs)
        elif rend == "bold":
            return self.parse_node_with_b(node, tex_node, **kwargs)
        else:
            fct_name = "parse_node_with_" + rend.replace("-", "_")
            ftor = getattr(self, fct_name, None)
            if callable(ftor):
                return ftor(node, tex_node, **kwargs)

        return self.parse_node_inner(node, tex_node, **kwargs)

    def parse_node_with_i(self, node, tex_node, **kwargs):
        # TODO: BUG in JATS: unlike <monospace>, no HTLM links are added in italics
        kwargs["add_HTML_link"] = False

        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, tex_node, **kwargs
        )

        is_bibitemdata = kwargs["is_bibitemdata"] if "is_bibitemdata" in kwargs else False
        is_citation = kwargs["is_citation"] if "is_citation" in kwargs else False
        is_comment = kwargs["is_comment"] if "is_comment" in kwargs else False

        tex_text = f"<i>{inner_tex_text}</i>"

        if inner_html_text == "" or (is_citation and not is_bibitemdata and not is_comment):
            html_text = inner_html_text
        else:
            html_text = '<span class="italique">' + inner_html_text + "</span>"

        if len(inner_jats_xml_text) > 0:
            xml_text = "<italic>" + inner_jats_xml_text + "</italic>"
        else:
            xml_text = "<italic/>"

        return html_text, tex_text, xml_text

    def parse_node_with_label(self, node, tex_node, **kwargs):
        html_text = tex_text = xml_text = ""

        self.list_item_label = get_text_from_node(node)

        return html_text, tex_text, xml_text

    def parse_node_with_large(self, node, tex_node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, tex_node, **kwargs
        )

        xml_text = "<large>" + inner_jats_xml_text + "</large>"

        return inner_html_text, inner_tex_text, xml_text

    def parse_node_with_list(self, node, tex_node, **kwargs):
        self.list_item_label = None

        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, tex_node, **kwargs
        )

        list_type = node.get("type")

        if list_type is None:
            xml_text = "<list>"
        else:
            xml_text = '<list list-type="' + list_type + '">'
        xml_text += inner_jats_xml_text
        xml_text += "</list>"

        if list_type is None or list_type == "bullet" or list_type == "simple":
            prefix = "<ul>"
            suffix = "</ul>"
        else:
            suffix = "</ol>"

            if list_type == "order" or list_type == "number":
                prefix = '<ol type="1">'
            elif list_type == "alpha-lower":
                prefix = '<ol type="a">'
            elif list_type == "alpha-upper":
                prefix = '<ol type="A">'
            elif list_type == "roman-lower":
                prefix = '<ol type="i">'
            elif list_type == "roman-upper":
                prefix = '<ol type="I">'
            else:
                prefix = '<ul class="no-bullet" style="list-style-type:none;">'
                suffix = "</ul>"

        html_text = prefix + inner_html_text + suffix
        tex_text = prefix + inner_tex_text + suffix

        return html_text, tex_text, xml_text

    def parse_node_with_item(self, node, tex_node, **kwargs):
        """
        <list-item><label>LABEL</label><p>TEXT</p> becomes in HTML
        <li>LABEL TEXT</li>
        (same with <title>)

        :param node:
        :return:
        """

        label = self.list_item_label or ""
        if label == "":
            label = node.get("label") or ""

        self.list_item_label = None

        kwargs["no_p"] = True
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, tex_node, **kwargs
        )

        xml_text = "<list-item>"
        if label:
            xml_text += "<label>" + label + "</label>"
        xml_text += inner_jats_xml_text
        xml_text += "</list-item>"

        text = "<li>"
        if label:
            text += label + " "

        html_text = text + inner_html_text + "</li>"
        tex_text = text + inner_tex_text + "</li>"

        return html_text, tex_text, xml_text

    def parse_node_with_formula(self, node, tex_node, **kwargs):
        # '\n' are added in this function, because the Cedrics -> XML transformation
        # does not add xml:space="preserve" with formulas (?)
        # An abstract with <p> and <formula> will have mix of "preserve".

        html_text = tex_text = jats_xml_text = ""
        type_ = node.attrib["type"] or "inline"
        tex_type = tex_node.attrib["textype"] if tex_node is not None else "inline"

        math_node = node[0]
        math_node_text = get_xml_from_node(math_node)
        math_node_text = replace_html_entities(math_node_text)
        # The Cedrics Mathml transform rounds up the width value
        math_node_text = math_node_text.replace(".em", "em")
        math_node_text = math_node_text.replace(".pt", "pt")

        tex_prefix = tex_suffix = "$"
        if type_ != "inline":
            tex_prefix = "\n\\["
            tex_suffix = "\\]\n"
            if tex_node is not None and tex_type not in ("inline", "display"):
                tex_prefix = "\n\\begin{" + tex_type + "}\n"
                tex_suffix = "\n\\end{" + tex_type + "}\n"

        math_node_text = fix_mfenced_in_mathml(math_node_text)

        if not kwargs["is_citation"]:
            math_node_text = math_node_text.replace(
                ' xmlns:xlink="http://www.w3.org/1999/xlink"', ""
            )
        math_node_text = math_node_text.replace('mode="display"', 'display="block"')

        if tex_node is None:
            # TODO: BUG in JATS. No need for a '$$' in the title if there is no tex formula
            # The '$$' at the end of the next line is to be compatible with jats_parser

            if type_ == "inline":
                tex_node_text = "$$"
            else:
                tex_node_text = ""
        else:
            tex_node_text = tex_prefix + tex_node.text + tex_suffix

        if type_ == "inline":
            jats_xml_text = "<inline-formula>"
        else:
            jats_xml_text = '<disp-formula xml:space="preserve">\n'

        jats_xml_text += "<alternatives>" + math_node_text
        jats_tex_text = escape(tex_node_text)

        if type_ != "inline":
            jats_xml_text += "\n"

        jats_xml_text += "<tex-math>" + jats_tex_text + "</tex-math>"

        if type_ != "inline":
            jats_xml_text += "\n"

        jats_xml_text += "</alternatives>"

        if type_ == "inline":
            jats_xml_text += "</inline-formula>"
        else:
            jats_xml_text += "\n</disp-formula>"
            node.tail = ""

        if "bug_cedrics" in kwargs and kwargs["bug_cedrics"]:
            # TODO: Bug in Cedrics. AIF_2012__62_6_2053_0 [16]
            # If there is no texmath, a <tex-math>$$</tex-math> is added and
            # get_text_from_node appends the 2.
            tex_text = get_text_from_node(node)
            if tex_node is None:
                tex_text += "$$"
        else:
            tex_text = tex_node_text

        data_tex = tex_node_text if type_ == "inline" else tex_node_text.replace("\n", "")
        html_text = f'<span class="mathjax-formula" data-tex="{data_tex}">{math_node_text}</span>'

        if type_ != "inline":
            prefix = '<table class="formula"><tr><td class="formula-inner">'
            suffix = '</td><td class="formula-label"></td></tr></table>'

            html_text = prefix + html_text + suffix

        # tex_text = escape(tex_text)

        return html_text, tex_text, jats_xml_text

    def parse_node_with_mixed_content(self, node, tex_node, **kwargs):
        """
        Parse and return the text of an XML node which mixes text and XML sub-nodes.
        Ex: <node>text1 <a>text_a</a> text2 <b>text_b</b>b_tail</node>
        Some inner nodes are removed, others are kept or replaced.

        Cedrics XMLs store the MathML and the TeX formulas in 2 siblings.
        Parse the 2 nodes at the same time.

        The JATS xml string is constructed at the same time because it is used during a PTF export

        :param node: XML Node (with MathML), XML Node (with TexMath)
        :param kwargs: params of the function
        :return: HTML text, TeX test, XML text
        """

        html_text = tex_text = jats_xml_text = ""

        if node is None:
            return html_text, tex_text, jats_xml_text

        name_ = type(node).__name__
        # Found 1 exception with <title>Дополнение к&nbsp;работе (AIF_2013__63_4)
        # The XML parser creates a different node with no tag for "&nbsp;"
        if name_ != "_Element":
            html_text = tex_text = jats_xml_text = html.unescape(node.text)
            if node.tail and not kwargs["is_top"]:
                html_text += node.tail
                tex_text += node.tail
                jats_xml_text += escape(node.tail)
            return html_text, tex_text, jats_xml_text

        # The tail is the text following the end of the node
        # Ex: <node>text1<a>text_a</a>a_tail</node>
        # The HTML text has to include the tail
        #   only if html_from_mixed_content was called recursively
        kwargs["is_top"] = kwargs["is_top"] if "is_top" in kwargs else True

        # sec_level is used to add <h1>, <h2>,... in the HTML text while parsing nodes like <sec>
        kwargs["sec_level"] = kwargs["sec_level"] if "sec_level" in kwargs else 2

        # Text in <comment> is parsed to add HTML link.
        kwargs["add_HTML_link"] = kwargs["add_HTML_link"] if "add_HTML_link" in kwargs else False

        # base_url to image links
        kwargs["base_url"] = kwargs["base_url"] if "base_url" in kwargs else ""

        kwargs["is_citation"] = kwargs["is_citation"] if "is_citation" in kwargs else False
        kwargs["is_comment"] = kwargs["is_comment"] if "is_comment" in kwargs else False

        # TODO remove once jats_parser has been validated agains xmldata
        kwargs["temp_math"] = kwargs["temp_math"] if "temp_math" in kwargs else False
        kwargs["temp_tex"] = kwargs["temp_tex"] if "temp_tex" in kwargs else False
        kwargs["temp_mixed_citation"] = (
            kwargs["temp_mixed_citation"] if "temp_mixed_citation" in kwargs else False
        )

        tag = normalize(node.tag)

        # pub-id/object-id are ignored by default are they are treated separately
        if not (kwargs["is_comment"]) and tag in ("pub-id", "object-id"):
            print(tag, "in", jats_xml_text)
            return html_text, tex_text, jats_xml_text

        if tag in ("bibitemdata", "toc"):
            kwargs["is_citation"] = True
            kwargs["temp_mixed_citation"] = True
        elif tag == "comment":
            kwargs["is_comment"] = True

        inner_html_text = inner_tex_text = inner_jats_xml_text = ""

        # I. Add the node's text.
        # Some tag have a corresponding html_from_@tag function to generate the HTML text.

        # Check if the html_from_@tag exists
        tag_mapped = {
            "statement": "sec",
            "disp-formula": "inline-formula",
            "chapter-title": "article-title",
            "bold": "strong",
            "table": "table-generic",
            "th": "table-generic",
            "tr": "table-generic",
            "td": "table-generic",
            "thead": "table-generic",
            "tbody": "table-generic",
            "colgroup": "table-generic",
            "col": "table-generic",
            "em": "i",
        }

        fct_name = tag_mapped[tag] if tag in tag_mapped else tag
        fct_name = "parse_node_with_" + fct_name.replace("-", "_")
        ftor = getattr(self, fct_name, None)
        if callable(ftor):
            inner_html_text, inner_tex_text, inner_jats_xml_text = ftor(node, tex_node, **kwargs)
        # Code if fc_name is a module fonction, not a class function:
        # if fct_name in globals():
        # Call the html_from_@tag function
        # inner_text = globals()[fct_name](node, **kwargs)
        else:
            # II.1. Add the node text (before the children text)

            # TODO Add HTML links to the text with URLs
            # if tag in ("ext-link", "uri"):
            #     if kwargs['include_ext_link']:
            #         inner_text += helper_add_link_from_node(node)
            # elif kwargs['add_HTML_link'] and node.text:
            #     match = re.match(r'[\n ]+', node.text)
            #     if not match:
            #         comment = make_links_clickable(node.text, node.text)
            #         inner_text += comment
            # elif node.text:
            #     inner_text += node.text

            # II.2. children
            # child_text = html_from_mixed_content(child, params)

            inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
                node, tex_node, **kwargs
            )

        html_text += inner_html_text
        tex_text += inner_tex_text
        jats_xml_text += inner_jats_xml_text

        # III. Add the node's tail for children
        if node.tail and not kwargs["is_top"] and tag not in ("p", "list", "item", "label"):
            html_text += node.tail
            tex_text += node.tail
            jats_xml_text += escape(node.tail)

        return html_text, tex_text, jats_xml_text

    def parse_node_with_p(self, node, tex_node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, tex_node, **kwargs
        )

        if "no_p" in kwargs and kwargs["no_p"]:
            # <p> inside <item> are removed in HTML to avoid a carriage return
            html_text = inner_html_text
        else:
            node_type = node.get("specific-use")
            if node_type:
                html_text = '<p class="' + node_type + '">' + inner_html_text + "</p>"
            else:
                html_text = "<p>" + inner_html_text + "</p>"

        # TODO: BUG in JATS (no <p> in the tex version)
        tex_text = inner_tex_text

        if len(inner_jats_xml_text) > 0:
            xml_text = '<p xml:space="preserve">' + inner_jats_xml_text + "</p>"
        else:
            xml_text = '<p xml:space="preserve"/>'

        return html_text, tex_text, xml_text

    def parse_node_with_ref(self, node, tex_node, **kwargs):
        label = node.text

        html_text = ""
        tex_text = ""
        xml_text = '<xref ref-type="bibr">' + escape(label) + "</xref>"

        return html_text, tex_text, xml_text

    def parse_node_with_sansserif(self, node, tex_node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, tex_node, **kwargs
        )

        xml_text = "<sans-serif>" + inner_jats_xml_text + "</sans-serif>"

        return inner_html_text, inner_tex_text, xml_text

    def parse_node_with_sc(self, node, tex_node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, tex_node, **kwargs
        )

        html_text = '<span class="smallcaps">' + inner_html_text + "</span>"
        tex_text = '<span class="smallcaps">' + inner_tex_text + "</span>"

        if len(inner_jats_xml_text) > 0:
            xml_text = "<sc>" + inner_jats_xml_text + "</sc>"
        else:
            xml_text = "<sc/>"

        return html_text, tex_text, xml_text

    def parse_node_with_slanted(self, node, tex_node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, tex_node, **kwargs
        )

        xml_text = "<slanted>" + inner_jats_xml_text + "</slanted>"

        return inner_html_text, inner_tex_text, xml_text

    def parse_node_with_small(self, node, tex_node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, tex_node, **kwargs
        )

        xml_text = "<small>" + inner_jats_xml_text + "</small>"

        return inner_html_text, inner_tex_text, xml_text

    def parse_node_with_sub(self, node, tex_node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, tex_node, **kwargs
        )

        html_text = "<sub>" + inner_html_text + "</sub>"
        tex_text = "<sub>" + inner_tex_text + "</sub>"
        xml_text = "<sub>" + inner_jats_xml_text + "</sub>"

        return html_text, tex_text, xml_text

    def parse_node_with_sup(self, node, tex_node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, tex_node, **kwargs
        )

        html_text = "<sup>" + inner_html_text + "</sup>"
        tex_text = "<sup>" + inner_tex_text + "</sup>"
        xml_text = "<sup>" + inner_jats_xml_text + "</sup>"

        return html_text, tex_text, xml_text

    def parse_node_with_texmath(self, node, tex_node, **kwargs):
        html_text = tex_text = xml_text = ""

        tex_text = "$" + get_text_from_node(node) + "$"

        return html_text, tex_text, xml_text

    def parse_node_with_tt(self, node, tex_node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, tex_node, **kwargs
        )

        if len(inner_jats_xml_text) > 0:
            xml_text = "<monospace>" + inner_jats_xml_text + "</monospace>"
        else:
            xml_text = "<monospace/>"

        return inner_html_text, inner_tex_text, xml_text

    def parse_node_with_underline(self, node, tex_node, **kwargs):
        inner_html_text, inner_tex_text, inner_jats_xml_text = self.parse_node_inner(
            node, tex_node, **kwargs
        )

        xml_text = "<underline>" + inner_jats_xml_text + "</underline>"

        return inner_html_text, inner_tex_text, xml_text

    def parse_node_with_xref(self, node, tex_node, **kwargs):
        """
        Parse an xref.
        Extract extids (doi, mr-item-id,...) and ext_links

        :param node:
        :param tex_node:
        :param kwargs:
        :return: html_text, tex_text, xml_text
        """

        location = self.get_location_from_xref(node)

        kwargs["add_HTML_link"] = False
        html_text, tex_text, xml_text = self.parse_node_inner(node, None, **kwargs)
        metadata = html_text
        html_text = make_links_clickable(location, html_text)
        tex_text = make_links_clickable(location, tex_text)

        is_comment = "is_comment" in kwargs and kwargs["is_comment"]

        # No ext-links is added while parsing titles or abstracts
        add_ext_link = kwargs["add_ext_link"] if "add_ext_link" in kwargs else True

        xref_data = {
            "rel": "",
            "mimetype": "",
            "location": location,
            "base": "",
            "metadata": metadata,
        }

        extid_value = (None, None)

        if add_ext_link and not is_comment:
            extid_value = self.add_extids_from_node_with_link(xref_data)

        # <ext-link> in a bibitemdata, in a comment, or if the xref is not converted into an extid
        # if is_bibitemdata or is_comment or extid_value[0] is None:
        xml_text = (
            '<ext-link xlink:href="' + html.escape(location) + '">' + xml_text + "</ext-link>"
        )

        if (
            add_ext_link
            and not is_comment
            and extid_value[0] is None
            and xref_data not in self.ext_links
        ):
            self.ext_links.append(xref_data)

        return html_text, tex_text, xml_text

    def parse_article_subject(self, node):
        lang = get_normalized_attrib(node, "lang") or self.lang

        subjects = [text.lstrip() for text in node.text.split(",")]

        for subject in subjects:
            self.subjs.append({"type": "subject", "lang": lang, "value": subject})

    def parse_article_subjects(self, node):
        for child in node:
            tag = normalize(child.tag)

            if tag == "article-subject":
                self.parse_article_subject(child)

    def parse_article_type(self, node):
        lang = get_normalized_attrib(node, "lang") or self.lang

        subjects = [node.text]

        for subject in subjects:
            self.subjs.append({"type": "type", "lang": lang, "value": subject})

    def parse_article_types(self, node):
        # 2023/12/05 <articletype> has been added to store the type
        if self.has_articletype:
            return

        for child in node:
            tag = normalize(child.tag)

            if tag == "article-type":
                self.parse_article_type(child)

    def parse_articletype(self, node):
        self.atype = node.text
        self.has_articletype = True

    def parse_auteur(self, node, is_ref=False):
        self.parse_common_contrib(node, "author", is_ref)

    def _get_abstract_data(self, node, abstract_type: str = None):
        tex_node = node.getnext()
        value_html, value_tex, value_xml = self.parse_node_with_mixed_content(
            node, tex_node, add_ext_link=False
        )

        lang = get_normalized_attrib(node, "lang") or ""
        if abstract_type is None:
            if lang == self.lang:
                value_xml = f"<abstract>{value_xml}</abstract>"
            elif self.lang == "und":
                value_xml = f'<abstract xml:lang="{lang}">{value_xml}</abstract>'
            else:
                value_xml = f'<trans-abstract xml:lang="{lang}">{value_xml}</trans-abstract>'
        else:
            value_xml = f'<abstract xml:lang="{lang}" abstract-type="{abstract_type}">{value_xml}</abstract>'

        abstract_data = {
            "tag": abstract_type if abstract_type is not None else "",
            "lang": lang,
            "value_xml": value_xml,
            "value_html": value_html,
            "value_tex": value_tex,
        }
        return abstract_data

    def parse_avertissement(self, node):
        self.abstracts.append(self._get_abstract_data(node, "avertissement"))

    def parse_note(self, node):
        self.abstracts.append(self._get_abstract_data(node, "note"))

    def parse_biblio(self, node):
        biblio_type = node.get("type") or ""
        for child in node:
            tag = normalize(child.tag)

            if tag == "bib_entry":
                type_ = child.get("type") or biblio_type
                is_mixed_citation = type_ == "flat"

                ref = CedricsRef(tree=child, lang="und", is_mixed_citation=is_mixed_citation)
                self.bibitems.append(ref)
                # TODO: Remove bibitem. This is used for solrCmds.
                #  solrCmds should use bibitems instead.
                self.bibitem.append(ref.citation_html)

        self.sort_bibitems()

    def parse_common_contrib(self, node, role, is_ref=False):
        contributor = create_contributor()

        if role and role[-1] == "s":
            role = role[0:-1]
        contributor["role"] = role

        equal_contrib_ = node.get("equal-contrib") or "no"
        contributor["equal_contrib"] = equal_contrib_ == "yes"

        corresp = node.get("author-role") or ""
        if corresp == "corresponding":
            contributor["corresponding"] = True

        is_etal = False
        has_children = False
        middlename = ""

        for child in node:
            has_children = True
            tag = normalize(child.tag)

            if tag == "nomcomplet":
                # TODO: Bug in Cedrics <nomcomplet> is ignored inside <bauteur> and <bediteur>
                if not is_ref:
                    contributor["string_name"] = child.text
                deceased_ = child.get("deceased") or "no"
                contributor["deceased_before_publication"] = deceased_ == "yes"
            elif tag == "prenom":
                contributor["first_name"] = child.text or ""
                if middlename != "":
                    contributor["first_name"] += " " + middlename
                    middlename = ""
            elif tag in ("middlename", "particule"):
                contributor["first_name"] += " " + child.text
                middlename = child.text
            elif tag == "initiale":
                pass
                # if len(contributor['first_name']) > 0:
                #     contributor['first_initials'] = child.text or ''
            elif tag == "junior":
                contributor["suffix"] = child.text
            elif tag == "nom":
                contributor["last_name"] = child.text or ""
            elif tag == "adresse":
                text = get_text_from_node(child)
                text = normalize_space(text).replace("\n", " ")
                if len(text) > 0:
                    contributor["addresses"].append(text)
            elif tag == "author-orcid":
                contributor["orcid"] = child.text
            elif tag == "mel":
                email = None
                for greatchild in child:
                    tag = normalize(greatchild.tag)
                    if tag == "xref":
                        email = greatchild.get("url")
                if email is None:
                    email = child.text
                if email is not None:
                    if len(contributor["email"]) > 0:
                        contributor["email"] += "{{{"
                    contributor["email"] += email
            elif tag == "etal":
                is_etal = True

        if has_children:
            use_initials = is_ref and getattr(settings, "REF_JEP_STYLE", False)
            helper_update_name_params(contributor, use_initials)

            contributor["contrib_xml"] = (
                "<etal/>" if is_etal else get_contrib_xml(contributor, is_ref=is_ref)
            )
        elif node.text is not None:
            contributor["string_name"] = node.text
            contributor["contrib_xml"] = (
                '<string-name xml:space="preserve">' + escape(node.text) + "</string-name>"
            )

        contributor["addresses"].sort()

        # email is ignored by jats_parser
        contributor["email"] = ""

        self.contributors.append(contributor)

    def parse_financement(self, node):
        abbrev = award_id = None

        for child in node:
            tag = normalize(child.tag)

            if tag == "bourse":
                award_id = child.text
            elif tag == "financeur":
                abbrev = get_text_from_node(child)

        if abbrev is not None and award_id is not None:
            self.awards.append({"abbrev": abbrev, "award_id": award_id})

    def parse_financements(self, node):
        for child in node:
            tag = normalize(child.tag)

            if tag == "financement":
                self.parse_financement(child)

    def parse_langue(self, node):
        self.lang = node.text

    def parse_motcle(self, node):
        lang = get_normalized_attrib(node, "lang") or self.lang
        tex_node = node.getnext()

        kwds = []
        for child in tex_node:
            tag = normalize(child.tag)

            if tag == "mot":
                value_html, value_tex, value_xml_inner = self.parse_node_with_mixed_content(
                    child, None
                )
                # text = normalize_space(get_text_from_node(child))
                kwds.append(value_tex)

        if len(kwds) == 0:
            value_html, value_tex, value_xml_inner = self.parse_node_with_mixed_content(
                node, tex_node
            )
            kwds = split_kwds(value_tex)

        self.kwds.extend([{"type": "", "lang": lang, "value": kwd} for kwd in kwds])

    def parse_msc(self, node):
        lang = get_normalized_attrib(node, "lang") or self.lang
        kwds = node.text.split(",")
        kwds = [kwd.strip() for kwd in kwds if len(kwd) > 0]

        self.kwds.extend([{"type": "msc", "lang": lang, "value": kwd} for kwd in kwds])

    def parse_resp(self, node):
        role = node.get("role") or "editeur"
        if role == "editeur":
            role = "editor"
        elif role == "organisateur":
            role = "organizer"

        self.parse_common_contrib(node, role)

    def parse_resume(self, node):
        lang = get_normalized_attrib(node, "lang") or self.lang
        """
        tag = "abstract"
        tex_node = node.getnext()

        value_html, value_tex, value_xml_inner = self.parse_node_with_mixed_content(
            node, tex_node, add_ext_link=False
        )

        if lang == self.lang:
            value_xml = "<abstract"
        elif self.lang == "und":
            value_xml = '<abstract xml:lang="' + lang + '"'
        else:
            value_xml = '<trans-abstract xml:lang="' + lang + '"'

        if len(value_xml_inner) == 0:
            value_xml += "/>"
        else:
            value_xml += ">" + value_xml_inner

            if lang == self.lang or self.lang == "und":
                value_xml += "</abstract>"
            else:
                value_xml += "</trans-abstract>"

        abstract_data = {
            "tag": tag,
            "lang": lang,
            "value_xml": value_xml,
            "value_html": value_html,
            "value_tex": value_tex,
        }
        """
        if lang == self.lang:
            # JATS puts the trans_abstract after the abstract
            self.abstracts.insert(0, self._get_abstract_data(node, None))
        else:
            self.abstracts.append(self._get_abstract_data(node))

    def parse_supplement(self, node):
        location = None
        caption = ""

        for child in node:
            tag = normalize(child.tag)

            if tag == "xref":
                location = self.get_location_from_xref(child)
            elif tag == "caption":
                caption = escape(node.text)

        if location:
            pos = location.find("/attach/")
            if pos > -1:
                if hasattr(self, "colid") and hasattr(self, "issue_id"):
                    text = location
                    location = self.colid + "/" + self.issue_id + "/"

                    if hasattr(self, "article_folder") and self.article_folder is not None:
                        location += self.article_folder + "/Attach/" + text[pos + 8 :]
                    else:
                        location += self.pid + text[pos:]

            relation = node.attrib.get("content-type")
            assert relation in ["supplementary-material", "review"], (
                f"Dans la balise supplement de {self.pid}, "
                f'content-type être "supplementary-material" ou "review" '
                f'au lieu de "{relation}"'
            )

            material = {
                "rel": node.attrib.get("content-type"),
                "mimetype": node.attrib.get("mimetype"),
                "location": location,
                "base": "",
                "metadata": "",
                "caption": caption,
            }
            self.supplementary_materials.append(material)

    def parse_supplements(self, node):
        for child in node:
            tag = normalize(child.tag)

            if tag == "supplement":
                self.parse_supplement(child)

    # TODO: It is a node with mix content
    # Transform the function in parse_node_with_motcle to handle formulas
    def parse_texmotcle(self, node):
        lang = get_normalized_attrib(node, "lang") or self.lang
        tex_node = node.getnext()

        kwds = []
        for child in tex_node:
            tag = normalize(child.tag)

            if tag == "mot":
                value_html, value_tex, value_xml_inner = self.parse_node_with_mixed_content(child)
                kwds.append(value_tex)

        if len(kwds) == 0:
            value_html, value_tex, value_xml_inner = self.parse_node_with_mixed_content(node)
            kwds = split_kwds(value_tex)

        self.kwds.extend([{"type": "", "lang": lang, "value": kwd} for kwd in kwds])

    def parse_titre(self, node):
        lang = get_normalized_attrib(node, "lang") or "und"
        tex_node = node.getnext()

        # node.set("{http://www.w3.org/XML/1998/namespace}space", "preserve")
        # tex_node.set("{http://www.w3.org/XML/1998/namespace}space", "preserve")

        title_html, title_tex, title_xml = self.parse_node_with_mixed_content(node, tex_node)

        if len(title_xml) > 0:
            self.titres.append(
                {
                    "lang": lang,
                    "title_html": title_html,
                    "title_tex": title_tex,
                    "title_xml": title_xml,
                }
            )

    def sort_bibitems(self):
        if len(self.bibitems):
            label = self.bibitems[0].label.strip("[]")  # Sometimes, labels are surrounded by []
            if len(label):
                # First, we split each label into label_prefix and label_suffix
                for bib in self.bibitems:
                    bib.split_label()

                if label.isdigit():

                    def sort_bibitem(bibitem):
                        return int(bibitem.label_prefix)

                    self.bibitems = sorted(self.bibitems, key=sort_bibitem)
                else:
                    self.bibitems = sorted(
                        self.bibitems, key=attrgetter("label_prefix", "year", "label_suffix")
                    )


class CedricsPublisher(PublisherData):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.parse_tree(kwargs["tree"])

    def parse_tree(self, tree):
        self.name = tree.text


class CedricsJournal(JournalData, CedricsBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.parse_tree(kwargs["tree"])

    def parse_tree(self, tree):
        super().parse_tree(tree)

        for node in tree:
            tag = normalize(node.tag)

            if tag == "acrocedram":
                self.pid = node.text
            elif tag == "jtitre":
                self.title_html = self.title_tex = node.text
                self.title_xml = "<journal-title-group><journal-title>" + escape(node.text)
            elif tag == "jtitrecourt":
                self.title_xml += (
                    '</journal-title><abbrev-journal-title abbrev-type="short-title">'
                    + escape(node.text)
                )
                self.title_xml += "</abbrev-journal-title></journal-title-group>"
            elif tag == "jediteur":
                self.publisher = CedricsPublisher(tree=node)
            elif tag == "issn":
                self.issn = node.text
            elif tag == "E-issn":
                self.e_issn = node.text


class CedricsIssue(IssueData, CedricsBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Jats has a title/trans_title
        # Cedrics has multiples <titre xml:lang>
        # Use self.titres to store the titles temporary.
        # self.title_* and self_trans_title* are set at the end of the concrete parse_tree
        self.titres = []

        self.ignore_date_published = (
            kwargs["ignore_date_published"] if "ignore_date_published" in kwargs else False
        )
        self.is_seminar = kwargs["is_seminar"] if "is_seminar" in kwargs else False
        self.colid = None
        self.provider = "mathdoc"
        self.article_folders = kwargs["article_folders"] if "article_folders" in kwargs else []
        self.dois = kwargs["dois"] if "dois" in kwargs else []

        self.parse_tree(kwargs["tree"])
        self.post_parse_tree()

    def parse_tree(self, tree):
        super().parse_tree(tree)

        seq = 1

        for node in tree:
            tag = normalize(node.tag)

            if tag == "notice":
                self.parse_notice(node)
            elif tag == "article":
                article_folder = (
                    self.article_folders[seq - 1] if len(self.article_folders) > 0 else ""
                )
                doi = self.dois[seq - 1] if len(self.dois) > 0 else ""
                article = CedricsArticle(
                    tree=node,
                    colid=self.colid,
                    issue_id=self.pid,
                    doi=doi,
                    ignore_date_published=self.ignore_date_published,
                    is_seminar=self.is_seminar,
                    article_folder=article_folder,
                )
                article.seq = str(seq)
                seq += 1
                self.articles.append(article)

    def parse_gestion(self, node):
        for child in node:
            tag = normalize(child.tag)

            if tag == "efirst":
                self.with_online_first = child.text == "yes"

    def parse_notice(self, node):
        for child in node:
            tag = normalize(child.tag)

            if tag == "idvol":
                self.pid = child.text
            elif tag == "tome":
                self.volume = child.text
            elif tag == "fascicule":
                self.number = child.text
            elif tag == "serie":
                self.vseries = child.text
            elif tag == "annee":
                self.year = child.text
            else:
                fct_name = "parse_" + tag.replace("-", "_")
                ftor = getattr(self, fct_name, None)
                if callable(ftor):
                    ftor(child)

        if self.last_modified_iso_8601_date_str is None:
            self.last_modified_iso_8601_date_str = timezone.now().isoformat()

    def parse_revue(self, node):
        self.journal = CedricsJournal(tree=node)
        self.colid = self.journal.pid
        self.publisher = self.journal.publisher

    def set_titles(self):
        # TODO: BUG in JATS: title_html is the one of the last title (bug if title in multiple langs)
        for titre in self.titres:
            if titre["lang"] == self.lang or self.lang == "und":
                self.title_html = titre["title_html"]
                self.title_tex = titre["title_tex"]
            else:
                self.trans_lang = titre["lang"]
                self.trans_title_html = titre["title_html"]
                self.trans_title_tex = titre["title_tex"]

        if self.title_html:
            self.title_xml = "<issue-title-group>"

            for titre in self.titres:
                if titre["lang"] == self.lang or self.lang == "und":
                    self.title_xml += (
                        '<issue-title xml:space="preserve" xml:lang="'
                        + titre["lang"]
                        + '">'
                        + titre["title_xml"]
                        + "</issue-title>"
                    )

            for titre in self.titres:
                if titre["lang"] != self.lang and self.lang != "und":
                    self.title_xml += '<trans-title-group xml:lang="' + titre["lang"] + '">'
                    self.title_xml += (
                        '<trans-title xml:space="preserve">'
                        + titre["title_xml"]
                        + "</trans-title>"
                    )
                    self.title_xml += "</trans-title-group>"

            self.title_xml += "</issue-title-group>"


class CedricsArticle(ArticleData, CedricsBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.ignore_date_published = (
            kwargs["ignore_date_published"] if "ignore_date_published" in kwargs else False
        )
        self.is_seminar = kwargs["is_seminar"] if "is_seminar" in kwargs else False
        self.article_folder = kwargs["article_folder"] if "article_folder" in kwargs else None

        # Jats has a title/trans_title
        # Cedrics has multiples <titre xml:lang>
        # Use self.titres to store the titles temporary.
        # self.title_* and self_trans_title* are set at the end of the concrete parse_tree
        self.titres = []

        self.pid = kwargs["pid"] if "pid" in kwargs else None
        self.colid = kwargs["colid"]
        self.issue_id = kwargs["issue_id"]
        self.atype = "normal"

        if "doi" in kwargs and kwargs["doi"] is not None:
            self.doi = clean_doi(kwargs["doi"])
            self.ids.append(("doi", self.doi))

        self.publishTeX = False
        self.tex_filename = None
        self.has_articletype = (
            False  # 2023/12/05 <articletype> has been added. Ignore <article-types>
        )

        self.parse_tree(kwargs["tree"])
        self.post_parse_tree()

    def parse_tree(self, tree):
        super().parse_tree(tree)

        for node in tree:
            tag = normalize(node.tag)

            if tag == "idart":
                self.pid = node.text
            elif tag == "doi":
                self.doi = clean_doi(node.text)
                # TODO: Remove as ResourceId do not seem useful (needs to upate templates)
                value = ("doi", self.doi)
                if value not in self.ids:
                    self.ids.append(value)
            elif tag == "pagedeb":
                self.fpage = self.get_numeric_value(node)
            elif tag == "pagefin":
                self.lpage = self.get_numeric_value(node)
            elif tag == "ordreart":
                # Set article_number or talk_number
                # Side effect in Cedrics: set page-count (handled at the end of this function)
                if self.is_seminar:
                    self.talk_number = node.text
                else:
                    self.article_number = node.text
            elif tag == "msn-id":
                self.extids.append(("mr-item-id", node.text))
            elif tag == "zbl-id":
                self.extids.append(("zbl-item-id", node.text))

            # elif tag == 'pub-date':
            #     date_type = child.get('date-type') or 'pub'
            #     if date_type == 'pub':
            #         self.date_published_iso_8601_date_str = get_data_from_date(child)
            #     else:
            #         date_str = get_data_from_date(child)
            #         self.history_dates.append({'type': 'online', 'date': date_str})
            # elif tag == "history":
            #     self.history_dates += get_data_from_history(child)
            #     for date in self.history_dates:
            #         if date['type'] == 'prod-deployed-date':
            #             self.prod_deployed_date_iso_8601_date_str = date['date']

            else:
                fct_name = "parse_" + tag.replace("-", "_")
                print("function " + fct_name)
                ftor = getattr(self, fct_name, None)
                if callable(ftor):
                    ftor(node)

    def parse_gestion(self, node):
        for child in node:
            tag = normalize(child.tag)

            if tag == "date_online" and not self.ignore_date_published:
                self.history_dates.append({"type": "online", "date": child.text})
            elif tag == "date_acceptation":
                self.history_dates.append({"type": "accepted", "date": child.text})
            elif tag == "date_reception":
                self.history_dates.append({"type": "received", "date": child.text})
            elif tag == "date_revision":
                self.history_dates.append({"type": "revised", "date": child.text})
            elif tag == "publishTeX":
                self.publishTeX = child.text == "yes"

    def parse_production(self, node):
        for child in node:
            tag = normalize(child.tag)

            if tag == "date_prod_PDF" and not self.ignore_date_published:
                self.date_published_iso_8601_date_str = child.text
            elif tag == "fichier_tex":
                self.tex_filename = child.text

    def parse_relations(self, node):
        rel_type = get_normalized_attrib(node, "type") or ""
        id_value = node.text

        relations = {
            "corrige": "corrects",
            "estcorrige": "corrected-by",
            "complete": "complements",
            "estcomplete": "complemented-by",
            "suitede": "follows",
            "estsuivide": "followed-by",
            "pagesprec": "prev-pages",
            "pagessuiv": "next-pages",
            "solutionde": "resolves",
            "apoursolution": "resolved-by",
            "commente": "comments",
            "estcommente": "commented-by",
            "remplace": "replaces",
            "estremplace": "replaced-by",
        }

        if rel_type in relations:
            obj = Foo()
            obj.rel_type = relations[rel_type]
            obj.id_value = id_value

            self.relations.append(obj)

    def post_parse_tree(self):
        # Some values in Cedrics XMLs are not embedded in groups (ex: authors)
        # We need to wait at the end of the parsing to finish the job

        super().post_parse_tree()

        if len(self.talk_number) > 0 or len(self.article_number) > 0:
            try:
                fpage_int = int(self.fpage)
                lpage_int = int(self.lpage)
                count_value = lpage_int - fpage_int + 1
                self.counts.append(("page-count", str(count_value)))
            except ValueError:
                pass

        # The (data)streams of the article's PDF and TeX are added automatically
        if hasattr(self, "colid") and hasattr(self, "issue_id"):
            location = self.colid + "/" + self.issue_id + "/"
            if self.article_folder:
                location += self.article_folder + "/" + self.article_folder + ".pdf"
            else:
                location += self.pid + "/" + self.pid + ".pdf"

            data = {
                "rel": "full-text",
                "mimetype": "application/pdf",
                "location": location,
                "base": "",
                "text": "Full (PDF)",
            }
            self.streams.append(data)

            if self.publishTeX and self.tex_filename:
                location = self.colid + "/" + self.issue_id + "/"
                if self.article_folder:
                    location += self.article_folder + "/" + self.tex_filename + ".tex"
                else:
                    location += self.pid + "/src/tex/" + self.tex_filename + ".tex"

                data = {
                    "rel": "full-text",
                    "mimetype": "application/x-tex",
                    "location": location,
                    "base": "",
                    "text": "TeX source",
                }
                self.streams.append(data)

    def set_titles(self):
        for titre in self.titres:
            if titre["lang"] == self.lang or self.lang == "und":
                self.title_html = titre["title_html"]
                self.title_tex = titre["title_tex"]
                if len(titre["title_xml"]) > 0:
                    self.title_xml = (
                        '<article-title xml:space="preserve">'
                        + titre["title_xml"]
                        + "</article-title>"
                    )
            else:
                self.trans_title_html = titre["title_html"]
                self.trans_title_tex = titre["title_tex"]
                if len(titre["title_xml"]):
                    self.trans_title_xml = '<trans-title-group xml:lang="' + titre["lang"] + '">'
                    self.trans_title_xml += '<trans-title xml:space="preserve">'
                    self.trans_title_xml += (
                        titre["title_xml"] + "</trans-title></trans-title-group>"
                    )
                self.trans_lang = titre["lang"]

        if len(self.title_xml) > 0:
            self.title_xml = (
                "<title-group>" + self.title_xml + self.trans_title_xml + "</title-group>"
            )


class CedricsRef(RefBase, CedricsBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.citation_xml = self.citation_html = self.citation_tex = ""
        self.REF_JEP_STYLE = getattr(settings, "REF_JEP_STYLE", False)

        self.is_mixed_citation = (
            kwargs["is_mixed_citation"] if "is_mixed_citation" in kwargs else False
        )
        self.eprint_id = None
        self.archive_name = None
        self.has_doi = False

        self.editeur_citation_xml = (
            ""  # bediteur is not in the correct order. Store the xml temporarily
        )

        self.parse_tree(kwargs["tree"])

    def parse_address(self, node):
        self.publisher_loc = normalize_space(get_text_from_node(node))
        self.citation_xml += "<publisher-loc>" + escape(self.publisher_loc) + "</publisher-loc>"

    def parse_archive_name(self, node):
        # TODO 1 JEP ref has a formula in its archive-name (for biorxiv)
        # It should be modified to use common names "biorxiv"

        self.archive_name = node.text.lower()

    def parse_article_id(self, node):
        eid = node.text
        self.extids.append(("eid", eid))

        self.citation_xml += '<pub-id pub-id-type="eid">' + escape(eid) + "</pub-id>"

    def parse_bauteur(self, node):
        self.parse_auteur(node, is_ref=True)

        last_contribution = self.contributors[-1]
        self.citation_xml += last_contribution["contrib_xml"]

    def parse_bediteur(self, node):
        self.parse_common_contrib(node, "editor", is_ref=True)

        last_contribution = self.contributors[-1]
        self.editeur_citation_xml += last_contribution["contrib_xml"]

    def parse_bibitemdata(self, node):
        tex_node = node.getnext()

        # TODO: Bug in Cedrics. if bibitemdata has no text between the nodes,
        # the XML is pretty printed. But since space="preserve" is added on the fly on mixed-citation
        # The \n and spaces should be preserved.
        # This bug is ignored (JTNB_2014__26_3_757_0 [1])

        value_html, value_tex, value_xml = self.parse_node_with_mixed_content(
            node, tex_node, is_bibitemdata=True
        )
        self.citation_html += value_html
        self.citation_tex += value_tex
        self.citation_xml += (
            '<mixed-citation xml:space="preserve">' + value_xml + "</mixed-citation>"
        )

    def parse_booktitle(self, node):
        tex_node = node.getnext()
        title_html, title_tex, title_xml = self.parse_node_with_mixed_content(
            node, tex_node, is_citation=True
        )

        self.source_tex = title_tex
        if title_xml != "":
            self.citation_xml += '<source xml:space="preserve">' + title_xml + "</source>"

    def parse_burl(self, node):
        for child in node:
            tag = normalize(child.tag)

            if tag == "xref":
                html_text, tex_text, xml_text = self.parse_node_with_xref(
                    child, None, keep_link=True, is_citation=True
                )

                self.citation_xml += xml_text

    def parse_chapter(self, node):
        # TODO: Bug in Cedrics <chapter> for types other than inbook
        #       becomes a text outside tags (AIF_2017__67_1_237_0 [16], CML_2013__5_1)
        # The info is not present in the PDF. It should not be in the Cedrics XML
        if self.type != "inbook":
            raise ValueError("<chapter> can be used only for an inbook")

        tex_node = node.getnext()
        title_html, title_tex, title_xml = self.parse_node_with_mixed_content(
            node, tex_node, is_citation=True
        )

        self.citation_xml += (
            '<chapter-title xml:space="preserve">' + title_xml + "</chapter-title>"
        )
        self.chapter_title_tex = title_tex

    def parse_doi(self, node):
        if node.text is None:
            raise ValueError("a doi can not be empty")

        if "http" in node.text:
            raise ValueError(node.text, "should not have http in it")

        doi_value = clean_doi(node.text)
        if self.doi is not None and self.doi != doi_value:
            raise ValueError(
                "Multiple dois for the same ref "
                + self.label
                + ": "
                + self.doi
                + " and "
                + doi_value
            )

        if self.doi is None:
            self.doi = doi_value
            self.extids.append(("doi", self.doi))

        self.has_doi = True

        # TODO: bug in Cedrics if the doi has a &nbsp; in it
        # the doi and the burl might not match and the dx.doi.org is no longer filtered
        # (bug²)
        # A doi should not have a space in it. raise an exception
        other_doi = self.doi.strip().replace(chr(160), "")
        if other_doi != self.doi:
            raise ValueError(self.doi, "has a space in it")

        if self.doi.lower().startswith("doi:"):
            raise ValueError('Remove "DOI:" in ' + self.doi)

        self.citation_xml += '<pub-id pub-id-type="doi">' + escape(node.text) + "</pub-id>"

    def parse_edition(self, node):
        # TODO: BUG in JATS (The edition is ignored in the HTML version)
        self.parse_node_common(node, "edition", "edition")

    def parse_editor(self, node):
        # TODO: Bug in Cedrics <editeur> becomes a <string-name> and we lose the info author vs editor
        self.parse_auteur(node, is_ref=True)

        last_contribution = self.contributors[-1]
        self.citation_xml += last_contribution["contrib_xml"]

    def parse_eprint_id(self, node):
        # Cannot add an ext_ids yet. Need to see if there's a archive-name
        self.eprint_id = escape(node.text)

    def parse_institution(self, node):
        self.parse_node_common(node, "institution", "institution")

    def parse_journal(self, node):
        tex_node = node.getnext()
        title_html, title_tex, title_xml = self.parse_node_with_mixed_content(
            node, tex_node, is_citation=True
        )

        self.source_tex = title_html
        if len(title_xml) > 0:
            self.citation_xml += '<source xml:space="preserve">' + title_xml + "</source>"

    def parse_mixed_citation(self, node):
        for child in node:
            tag = normalize(child.tag)

            if tag == "reference":
                self.parse_reference(child)
                if len(self.label) > 0:
                    self.citation_html = self.citation_tex = self.label + " "
            elif tag == "bibitemdata":
                self.parse_bibitemdata(child)

    def parse_month(self, node):
        # TODO: Bug in Cedrics. month is ignored in the PDF ? JEP_2019__6__737_0 [Hoe63]
        self.parse_node_common(node, "month", "month")

    def parse_msn_id(self, node):
        self.extids.append(("mr-item-id", node.text))
        self.citation_xml += (
            '<ext-link ext-link-type="mr-item-id">' + escape(node.text) + "</ext-link>"
        )

    def parse_node_common(self, node, variable_name, jats_tag, **kwargs):
        text = get_text_from_node(node)
        if "keep_space" not in kwargs:
            text = normalize_space(text)
        setattr(self, variable_name, text)

        self.citation_xml += "<" + jats_tag
        if "jats_params" in kwargs and len(kwargs["jats_params"]) > 0:
            self.citation_xml += " " + kwargs["jats_params"]

        self.citation_xml += ">" + escape(text) + "</" + jats_tag + ">"

    def parse_note(self, node):
        value_html, value_tex, value_xml = self.parse_node_with_mixed_content(
            node, None, is_citation=True, is_comment=True, add_HTML_link=True, temp_math=True
        )

        self.comment = value_html

        if len(value_html) > 0:
            self.citation_xml += '<comment xml:space="preserve">' + value_xml + "</comment>"

    def parse_number(self, node):
        self.parse_node_common(node, "issue", "issue", keep_space=True)

    def parse_pagedeb(self, node):
        self.parse_node_common(node, "fpage", "fpage", keep_space=True)

    def parse_pagefin(self, node):
        self.parse_node_common(node, "lpage", "lpage", keep_space=True)

    def parse_pages(self, node):
        if len(self.fpage) == 0 and len(self.lpage) == 0:
            tag = "size" if (self.type == "book" or "thesis" in self.type) else "fpage"
            params = 'units="pages"' if tag == "size" else ""
            self.parse_node_common(node, tag, tag, jats_params=params)

    def parse_page_total_number(self, node):
        self.parse_node_common(node, "size", "size", jats_params='units="pages"')

    def parse_publisher(self, node):
        self.publisher_name = normalize_space(get_text_from_node(node))
        self.citation_xml += "<publisher-name>" + escape(self.publisher_name) + "</publisher-name>"

    def parse_reference(self, node):
        cedrics_label = get_text_from_node(node)

        if cedrics_label and cedrics_label[0] != "[":
            self.label = "[" + cedrics_label + "]"
        else:
            self.label = cedrics_label

        if self.label:
            if self.is_mixed_citation:
                self.citation_xml += "<label>" + escape(self.label) + "</label>"
            else:
                self.citation_xml += "<label>" + escape(cedrics_label) + "</label>"

    def parse_series(self, node):
        self.parse_node_common(node, "series", "series")

    def parse_structured_citation(self, node):
        wrapper_tag_added = False
        eprint_done = False

        for child in node:
            tag = normalize(child.tag)

            # The <label> is outside the <element-citation> in JATS
            if tag != "reference" and not wrapper_tag_added:
                self.citation_xml += '<element-citation publication-type="' + self.type + '">'
                wrapper_tag_added = True

            if self.eprint_id is not None and tag not in ("archive-prefix", "archive-name"):
                self.post_parse_eprint()
                eprint_done = True

            # TODO: brevue bcoll bconference bseries btome... (util/bibitem.xsl)

            if tag in ["howpublished"]:
                self.parse_title(child)
            elif tag in ("institution", "organization", "school"):
                self.parse_institution(child)
            elif tag not in ("TeXtitle", "TeXbooktitle", "archive-prefix"):
                fct_name = "parse_" + tag.replace("-", "_")
                ftor = getattr(self, fct_name, None)
                if callable(ftor):
                    ftor(child)

        if self.eprint_id is not None and not eprint_done:
            self.post_parse_eprint()

        # ptf-xsl mets les <bediteur> à la fin en JATS
        if len(self.editeur_citation_xml) > 0:
            self.citation_xml += '<person-group person-group-type="editor">'
            self.citation_xml += self.editeur_citation_xml
            self.citation_xml += "</person-group>"

        self.citation_xml += "</element-citation>"

        text = get_citation_html(self)
        self.citation_html = self.citation_tex = text

    def parse_title(self, node):
        tex_node = node.getnext()

        title_html, title_tex, title_xml = self.parse_node_with_mixed_content(
            node, tex_node, is_citation=True, add_ext_link=True
        )

        if self.type == "incollection":
            self.chapter_title_tex = title_html
            self.citation_xml += (
                '<chapter-title xml:space="preserve">' + title_xml + "</chapter-title>"
            )
        elif self.type in [
            "book",
            "inbook",
            "unpublished",
            "phdthesis",
            "masterthesis",
            "mastersthesis",
            "manual",
            "techreport",
            "coursenotes",
            "proceedings",
        ] or node.tag in ["booktitle", "howpublished"]:
            self.source_tex = title_html
            self.citation_xml += '<source xml:space="preserve">' + title_xml + "</source>"
        else:
            self.article_title_tex = title_html
            self.citation_xml += (
                '<article-title xml:space="preserve">' + title_xml + "</article-title>"
            )

    def parse_tree(self, tree):
        super().parse_tree(tree)

        self.user_id = get_normalized_attrib(tree, "user-id") or ""
        self.type = get_normalized_attrib(tree, "doctype") or "misc"
        if self.type == "none":
            self.type = "misc"

        if self.is_mixed_citation:
            self.parse_mixed_citation(tree)
        else:
            self.parse_structured_citation(tree)

    def parse_type(self, node):
        tex_node = node.getnext()
        value_html, value_tex, value_xml = self.parse_node_with_mixed_content(
            node, tex_node, bug_cedrics=True
        )

        self.annotation = value_tex

        if len(value_xml) > 0:
            self.citation_xml += (
                '<annotation><p xml:space="preserve">' + value_xml + "</p></annotation>"
            )

    def parse_url_last_visited(self, node):
        self.citation_xml += '<date-in-citation content-type="access-date" iso-8601-date="'
        self.citation_xml += node.text
        self.citation_xml += '">' + node.text
        self.citation_xml += "</date-in-citation>"

    def parse_volume(self, node):
        text = normalize_space(get_text_from_node(node))

        if text is not None and len(text) > 0:
            self.volume = text
            self.citation_xml += "<volume>" + escape(self.volume) + "</volume>"

    def parse_year(self, node):
        self.parse_node_common(node, "year", "year")

    def parse_zbl_id(self, node):
        self.extids.append(("zbl-item-id", node.text))
        self.citation_xml += (
            '<ext-link ext-link-type="zbl-item-id">' + escape(node.text) + "</ext-link>"
        )

    def post_parse_eprint(self):
        if self.eprint_id is not None:
            if self.archive_name is None:
                # Assumption made by the XSLT transform
                self.archive_name = "arxiv"

            if self.archive_name in ["arxiv", "tel", "hal", "theses.fr"]:
                # The Cedrics archive-prefix is ignored (the URL could change overtime)
                self.extids.append((self.archive_name, self.eprint_id))

                self.citation_xml += (
                    '<pub-id pub-id-type="'
                    + self.archive_name
                    + '">'
                    + self.eprint_id
                    + "</pub-id>"
                )

    def split_label(self):
        """
        Used when sorting non-digit bibitems
        """
        label = self.label.lower()
        # CRAS <reference> do not allow a simple sort (?!?)
        # labels with "XXX et al." need to be put after "XXX"
        label = label.replace(" et al.", "ZZZ").replace(" et al.", "ZZZ")
        if len(label) > 1:
            label = label[1:-1]

        if label.isdigit():
            self.label_prefix = label
        else:
            try:
                self.label_prefix, self.label_suffix = re.split(r"[\d]+", label)
            except ValueError:
                # Special case where label is similar as "Sma" instead of "Sma15"
                self.label_prefix, self.label_suffix = [label, ""]
