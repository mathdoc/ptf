from ptf.cmds.xml.dublin_core.hal import Article as ArticleBase


class Article(ArticleBase):
    def get_collection(self):
        return "TEL"

    def get_id(self):
        return "tel-id"
