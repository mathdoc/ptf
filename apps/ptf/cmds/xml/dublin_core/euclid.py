from ptf.cmds.xml.dublin_core.data import Article as ArticleBase


class Article(ArticleBase):
    volume = 0

    def get_pages(self):
        if len(self.record["identifier"]) == 1:
            return super().get_pages()
        sp = self.record["identifier"][1].split(",")
        pages = sp[-1]
        sp = pages.split("-")
        if len(sp) > 1:
            return sp[0], sp[1]
        return "", ""

    def get_identifier(self):
        s = self.record["identifier"][0].split("/")
        return s[4]

    def get_id(self):
        return "euclid-id"

    def get_collection(self):
        s = self.record["identifier"][1].split(",")
        s2 = s[0].split(" ")
        stop_index = len(s2) - 1
        for index, l in enumerate(s2):
            if l.isdigit():
                self.volume = l
                stop_index = index
                break

        return " ".join(s2[0:stop_index])

    def get_volume(self):
        return self.volume if self.volume else 0

    def get_volume_number(self):
        try:
            s = self.record["identifier"][1].split(",")
            s2 = s[1].split(" ")
            if s2[2].isdigit():
                return s2[2]
            else:
                return 0
        except:
            return 0
