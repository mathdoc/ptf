from ptf.cmds.xml.dublin_core.data import Article as ArticleBase


class Article(ArticleBase):
    def get_collection(self):
        return "arXiv"

    def get_id(self):
        return "arxiv-id"

    def get_identifier(self):
        r = self.record["identifier"][0]
        return r.split("/")[-1]

    def get_volume(self):
        return self.get_date().year
