import re
from datetime import datetime


class Article:
    def __init__(self, record, lang):
        self.record = record
        self.lang = lang
        self.reg_doi = re.compile("doi")
        self.reg_eudml = re.compile("eudml")
        self.reg_zbl = re.compile("zbl-item")
        self.eudml_id = None

    def get_pages(self):
        return "", ""

    def get_publisher(self):
        return self.record["publisher"][0] if "publisher" in self.record else None

    def get_identifier(self):
        index_doi = ""
        index_eudml = ""
        index_zbl = ""
        index = ""
        for pos, identifier in enumerate(self.record["identifier"]):
            if self.reg_doi.search(identifier):
                index_doi = pos
            if self.reg_eudml.search(identifier):
                index_eudml = pos
                reg_eudml_id = re.compile(r"\:\d+")
                id_part = reg_eudml_id.search(identifier)
                if id_part:
                    id_part = id_part[0]
                    id_part.split(":")
                    id_eudml = id_part[1]
                    self.eudml_id = id_eudml
            if self.reg_zbl.search(identifier):
                index_zbl = pos
        if index_doi != "":
            self.record["identifier"][index_doi] = self.record["identifier"][index_doi].replace(
                "doi:", ""
            )
            index = index_doi
        if index_eudml != "" and index_doi == "":
            index = index_eudml
        if index_zbl != "" and index == "":
            index = index_zbl
        if index == "":
            index = 0
        return self.record["identifier"][index]

    def get_date(self):
        if "date" not in self.record:
            return None

        formats = ["%Y-%m-%d", "%Y-%m", "%Y"]
        r = None
        for f in formats:
            try:
                r = datetime.strptime(self.record["date"][0], f)
                break
            except ValueError:
                continue
        return r

    def get_date_str(self):
        return self.record["date"][0] if "date" in self.record else ""

    def get_authors(self):
        return self.record["creator"] if "creator" in self.record else []

    def get_lang(self):
        lang = self.lang
        if "language" in self.record and self.record["language"][0] != "":
            lang = self.record["language"][0]
        return lang

    def get_abstract(self):
        desc = max(self.record["description"], key=len) if "description" in self.record else ""

        return [
            {
                "tag": "abstract",
                "lang": self.get_lang(),
                "value_xml": desc,
                "value_html": desc,
                "value_tex": desc,
            }
        ]

    def get_format(self):
        return self.record["format"]

    def get_title(self):
        return self.record["title"][0] if "title" in self.record else ""

    def get_kwd(self):
        return filter(None, self.record["subject"]) if "subject" in self.record else []

    def get_contribs(self):
        h = {}
        h["content_type"] = "authors"
        h["contribs"] = []
        for author in self.get_authors():
            a = author.split(",")
            h["contribs"].append(
                {
                    "contrib_type": "author",
                    "last_name": a[0] if len(a) > 1 else "",
                    "first_name": a[1] if len(a) > 1 else "",
                    "reference_name": author,
                    "prefix": "",
                    "suffix": "",
                    "string_name": author,
                    "contrib_xml": "",
                }
            )
        return [h]

    def get_link(self):
        if self.eudml_id is not None:
            return "https://eudml.org/doc/" + self.eudml_id
        else:
            return self.record["identifier"][0]

    def get_volume(self):
        return 0

    def get_volume_number(self):
        return 0
