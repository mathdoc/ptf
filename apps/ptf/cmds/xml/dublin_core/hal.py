import validators

from ptf.cmds.xml.dublin_core.data import Article as ArticleBase


class Article(ArticleBase):
    def get_collection(self):
        return "HAL"

    def get_id(self):
        return "hal-id"

    def get_link(self):
        for s in self.record["source"]:
            if validators.url(s):
                return s

    def get_abstract(self):
        return [
            {
                "tag": "abstract",
                "lang": self.get_lang(),
                "value_xml": self.record["description"][-1]
                if "description" in self.record
                else "",
                "value_html": self.record["description"][-1]
                if "description" in self.record
                else "",
                "value_tex": self.record["description"][-1]
                if "description" in self.record
                else "",
            }
        ]

    def get_volume(self):
        return self.get_date().year
