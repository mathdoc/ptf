from ptf.cmds.xml.dublin_core.data import Article as ArticleBase


class Article(ArticleBase):
    def get_identifier(self):
        s = self.record["identifier"][0].split("?")
        return s[1]

    def get_id(self):
        return "gdz-id"
