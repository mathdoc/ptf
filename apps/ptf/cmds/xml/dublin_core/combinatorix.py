from ptf.cmds.xml.dublin_core.data import Article as ArticleBase


class Article(ArticleBase):
    volume = None
    volume_number = 0

    def get_id(self):
        return "combinatorix-id"

    def get_collection(self):
        return "The Electronic Journal of Combinatorics"

    def get_volume(self):
        try:
            s = self.record["source"][0].split(";")
            s2 = s[1].split(",")
            s3 = s2[0].split(" ")
            if s3[-1].isdigit():
                self.volume = s3[-1]

            s3bis = s2[1].split(" ")
            if s3bis[2].isdigit():
                self.volume_number = s3bis[2]
        except Exception as e:
            print(e)

        return self.volume if self.volume else self.get_date().year

    def get_volume_number(self):
        return self.volume_number
