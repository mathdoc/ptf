from ptf.cmds.xml.dublin_core.data import Article as ArticleBase


class Article(ArticleBase):
    volume = None
    volume_number = 0
    fpage = ""
    lpage = ""

    def get_id(self):
        return "dm-id"

    def get_collection(self):
        return "DOCUMENTA MATHEMATICA"

    def get_volume(self):
        try:
            s = self.record["source"][0].split(";")
            sbis = s[2].split("-")
            self.fpage, self.lpage = sbis[0], sbis[1]

            s2 = s[1].split(" ")
            if s2[2].isdigit():
                self.volume = s2[2]

        except Exception as e:
            print(e)

        print(self.volume)
        return self.volume if self.volume else self.get_date().year

    def get_volume_number(self):
        return self.volume_number

    def get_pages(self):
        self.get_volume()
        return self.fpage, self.lpage
