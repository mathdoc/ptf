from ptf.cmds.xml.dublin_core.data import Article as ArticleBase


class Article(ArticleBase):
    volume = None
    volume_number = 0

    lpage, fpage = "", ""

    def get_id(self):
        return "mc-id"

    def get_collection(self):
        return "Mathematical Communications"

    def get_authors(self):
        authors = self.record["creator"] if "creator" in self.record else []
        ret = []
        for i in authors:
            s = i.split(";")
            ret.append(s[0])
        return ret

    def get_identifier(self):
        s = self.record["identifier"][0].split("/")
        return f"mc{s[-1]}"

    def get_pages(self):
        self.get_volume()
        print(self.fpage, self.lpage)
        return self.fpage, self.lpage

    def get_volume(self):
        try:
            s = self.record["source"][0].split(";")
            s2 = s[1].split(",")
            s3 = s2[0].split(" ")
            if s3[-1].isdigit():
                self.volume = s3[-1]

            print(self.volume)
            s3bis = s2[1].split(" ")
            print(s3bis)
            if s3bis[2].isdigit():
                self.volume_number = s3bis[2]

            s4 = s[-1].split("-")
            self.fpage, self.lpage = s4[0], s4[1]
        except Exception as e:
            print(e)

        return self.volume if self.volume else self.get_date().year

    def get_volume_number(self):
        return self.volume_number
