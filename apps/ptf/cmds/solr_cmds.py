import pysolr

from django.conf import settings

from ptf.cmds.base_cmds import baseCmd
from ptf.cmds.base_cmds import make_int
from ptf.display import resolver
from ptf.site_register import SITE_REGISTER
from ptf.solr import search_helpers
from ptf.utils import get_display_name

# Not used so far.
# nlm2solr use normalize-space for volume and volume-series,
# but make_int is called to convert into int: spaces are also trimmed
# def normalize_whitespace(str):
#    import re
#    str = str.strip()
#    str = re.sub(r'\s+', ' ', str)
#    return str


class solrFactory:
    solr = None
    solr_url = None

    @staticmethod
    def get_solr():
        if solrFactory.solr is None:
            if solrFactory.solr_url is None:
                solrFactory.solr_url = settings.SOLR_URL
            solrFactory.solr = pysolr.Solr(solrFactory.solr_url, timeout=10)
        return solrFactory.solr

    @staticmethod
    def do_solr_commit():
        if hasattr(settings, "IGNORE_SOLR") and settings.IGNORE_SOLR:
            return

        solr = solrFactory.get_solr()
        solr.commit()

    @staticmethod
    def do_solr_rollback():
        if hasattr(settings, "IGNORE_SOLR") and settings.IGNORE_SOLR:
            return

        solr = solrFactory.get_solr()
        msg = "<rollback />"
        solr._update(msg)

    @staticmethod
    def reset():
        if solrFactory.solr:
            solrFactory.solr.get_session().close()
        solrFactory.solr = None


def solr_add_contributors_to_data(contributors, data):
    if contributors is not None:
        author_names = []
        ar = []
        aul = []

        for c in contributors:
            if c["role"] in ["author", "editor", "translator"]:
                display_name = get_display_name(
                    "", c["first_name"], c["last_name"], "", c["string_name"]
                )
                ref_name = c["mid"] if c["mid"] else display_name

                if ref_name:
                    ar.append(ref_name)
                if display_name:
                    author_names.append(display_name)
                if c["last_name"]:
                    aul.append(c["last_name"])

        data["au"] = "; ".join(author_names)
        # auteurs de references
        data["ar"] = ar
        # Surnames / Lastnames / Nom de famille
        data["aul"] = aul

        if author_names:
            data["fau"] = author_names[0]


def solr_add_kwds_to_data(kwds, data):
    data["kwd"] = ", ".join(
        [kwd["value"] for kwd in kwds if kwd["type"] != "msc" and kwd["lang"] == "fr"]
    )
    data["trans_kwd"] = ", ".join(
        [kwd["value"] for kwd in kwds if kwd["type"] != "msc" and kwd["lang"] != "fr"]
    )
    data["msc"] = [kwd["value"].upper() for kwd in kwds if kwd["type"] == "msc"]


#####################################################################
#
# solrCmd: base class for Solr commands
#
######################################################################
class solrCmd(baseCmd):
    def __init__(self, params={}):
        super().__init__(params)

    def do(self, parent=None):
        if hasattr(settings, "IGNORE_SOLR") and settings.IGNORE_SOLR:
            return None

        return super().do(parent)

    def post_do(self, resource=None):
        super().post_do(resource)

    def undo(self):
        if hasattr(settings, "IGNORE_SOLR") and settings.IGNORE_SOLR:
            return None

        return super().undo()


#####################################################################
#
# solrDeleteCmd: generic to delete Solr documents, based on a query
#
######################################################################
class solrDeleteCmd(solrCmd):
    def __init__(self, params={}):
        self.commit = True
        self.q = None

        super().__init__(params)

        self.required_params.extend(["q"])

    def internal_do(self):
        super().internal_do()

        solrFactory.get_solr().delete(q=self.q, commit=self.commit)

        return None


#####################################################################
#
# solrAddCmd: base class for Solr Add commands
#
######################################################################
class solrAddCmd(solrCmd):
    def __init__(self, params={}):
        self.commit = True
        self.db_obj = None
        self.id = None
        self.pid = None
        self.data = {}

        super().__init__(params)

        self.required_params.extend(["id", "pid"])
        self.required_delete_params.extend(["id"])

    def pre_do(self):
        super().pre_do()

        self.data["id"] = self.id
        self.data["pid"] = self.pid
        # parfois, lors d'erreur et/ou upload simultané, il y a plusieurs enregistrement pour un PID
        # pour éviter d'avoir +sieurs résultats de recherche pour un PID, on supprime tout avant le internal_do
        cmd = solrDeleteCmd({"q": "pid:" + self.pid})
        cmd.do()

    def internal_do(self):
        super().internal_do()

        datas = [self.data]

        solrFactory.get_solr().add(docs=datas, commit=self.commit)

        return None

    def internal_undo(self):
        id = super().internal_undo()

        solrFactory.get_solr().delete(id=self.id, commit=self.commit)

        return id


#####################################################################
#
# addResourceSolrCmd: base class for solrAddCmds adding a Resource
#
######################################################################
class addResourceSolrCmd(solrAddCmd):
    def __init__(self, params={}):
        self.xobj = None  # model_data object

        # fields of the xobj to pass to SolR
        self.fields = [
            "lang",
            "doi",
            "title_tex",
            "title_html",
            "trans_title_tex",
            "trans_title_html",
            "abstract_tex",
            "abstract_html",
            "trans_abstract_tex",
            "trans_abstract_html",
            "collection_title_tex",
            "collection_title_html",
            "collection_id",
            "year",
            "body",
            "bibitem",
        ]

        # Used to filter the articles based on their site
        self.sites = None

        super().__init__(params)

        self.required_params.extend(["xobj"])

    def add_collection(self, collection):
        self.data["collection_id"] = collection.id

        if "collection_title_tex" not in self.data:
            self.data["collection_title_tex"] = [collection.title_tex]
        else:
            self.data["collection_title_tex"].append(collection.title_tex)

        if "collection_title_html" not in self.data:
            self.data["collection_title_html"] = [collection.title_html]
        else:
            self.data["collection_title_html"].append(collection.title_html)

        # classname is used only by PCJ for the article types
        if collection.coltype == "journal":
            self.data["dt"] = ["Article de revue"]
        elif collection.coltype == "acta":
            self.data["dt"] = ["Acte de séminaire"]
        elif collection.coltype == "thesis":
            self.data["classname"] = "Thèse"
            self.data["dt"] = ["Thèse"]
        elif collection.coltype == "lecture-notes":
            self.data["classname"] = "Notes de cours"
            self.data["dt"] = ["Notes de cours"]
        elif collection.coltype == "proceeding":
            self.data["classname"] = "Acte de rencontre"
            self.data["dt"] = ["Acte de rencontre"]
        else:
            self.data["classname"] = "Livre"
            self.data["dt"] = ["Livre"]

    def add_abstracts_to_data(self):
        for abstract in self.xobj.abstracts:
            lang = abstract["lang"]

            for field_type in ["tex", "html"]:
                abstract_field = "value_" + field_type
                field_name = "abstract_" + field_type
                if lang != "fr":
                    field_name = "trans_" + field_name

                self.data[field_name] = abstract[abstract_field]

    def add_year_to_data(self, year):
        if year:
            years = str(year).split("-")
            if len(years) > 1:
                self.data["year_facet"] = int(years[1])
            else:
                self.data["year_facet"] = int(year)

    def pre_do(self):
        super().pre_do()

        for field in self.fields:
            if hasattr(self.xobj, field):
                self.data[field] = getattr(self.xobj, field)

        self.add_abstracts_to_data()
        solr_add_kwds_to_data(self.xobj.kwds, self.data)
        solr_add_contributors_to_data(self.xobj.contributors, self.data)

        if "dt" not in self.data:
            raise ValueError(f"add SolR resource without dt - {self.xobj.pid}")

        # year either comes directly from xobj (container) or from set_container
        self.add_year_to_data(self.data["year"])

        if self.db_obj is not None:
            solr_fields = {
                "application/pdf": "pdf",
                "image/x.djvu": "djvu",
                "application/x-tex": "tex",
            }
            for stream in self.xobj.streams:
                mimetype = stream["mimetype"]
                if mimetype in solr_fields:
                    href = self.db_obj.get_binary_file_href_full_path(
                        "self", mimetype, stream["location"]
                    )
                    self.data[solr_fields[mimetype]] = href

        if self.db_obj is not None:
            self.data["wall"] = self.db_obj.get_wall()

        if self.sites:
            self.data["sites"] = self.sites
        else:
            self.data["sites"] = [settings.SITE_ID]


#####################################################################
#
# addContainerSolrCmd: adds/remove a container (issue/book)
#
# A container needs a collection (collection_title_tex etc.)
#
######################################################################
class addContainerSolrCmd(addResourceSolrCmd):
    def __init__(self, params={}):
        super().__init__(params)

        self.fields.extend(["ctype"])
        # self.data["dt"] = ["Livre"]

    def pre_do(self):
        super().pre_do()

        for field in ["volume", "number", "vseries"]:
            if hasattr(self.xobj, field):
                self.data["volume"] = make_int(getattr(self.xobj, field))

        if hasattr(self.xobj, "incollection") and len(self.xobj.incollection) > 0:
            incol = self.xobj.incollection[0]
            self.data["vseries"] = make_int(incol.vseries)
            self.data["volume"] = 0
            self.data["number"] = make_int(incol.volume)

            # if incol.coltype == "theses":
            #     self.data["dt"] = ["Thèse"]


#####################################################################
#
# addArticleSolrCmd: adds/remove an article
#
# an article needs a container (container_id) that needs a collection (collection_id)
#
######################################################################


class addArticleSolrCmd(addResourceSolrCmd):
    def __init__(self, params={}):
        super().__init__(params)

        self.fields.extend(
            ["page_range", "container_id", "volume", "number", "vseries", "article_number"]
        )
        # self.data["dt"] = ["Article"]

    def set_container(self, container):
        self.data["container_id"] = container.id
        self.data["year"] = container.year
        self.data["vseries"] = make_int(container.vseries)
        self.data["volume"] = make_int(container.volume)
        self.data["number"] = make_int(container.number)

    def set_eprint(self, eprint):
        self.data["dt"].append("e-print")

    def set_source(self, source):
        pass

    def set_thesis(self, thesis):
        self.data["dt"].append("thesis")

    def set_original_article(self, article):
        # TODO Replace some data (ie doi, pid) with the original article
        pass

    def pre_do(self):
        super().pre_do()

        self.data["classname"] = resolver.ARTICLE_TYPES.get(
            self.xobj.atype, "Article de recherche"
        )

        self.data["page_range"] = ""
        if not self.xobj.page_range:
            self.data["page_range"] = "p. "
            if self.xobj.fpage is not None:
                self.data["page_range"] += self.xobj.fpage
            if self.xobj.fpage and self.xobj.lpage:
                self.data["page_range"] += "-"
            if self.xobj.lpage is not None:
                self.data["page_range"] += self.xobj.lpage
        elif self.xobj.page_range[0] != "p":
            self.data["page_range"] = "p. " + self.xobj.page_range


#####################################################################
#
# addBookPartSolrCmd: adds/remove an book part (similar to an article)
#
# a book part needs a collection id (array)
#
######################################################################
class addBookPartSolrCmd(addResourceSolrCmd):
    def __init__(self, params={}):
        super().__init__(params)

        self.fields.extend(
            ["page_range", "container_title_tex", "container_title_html", "volume", "number"]
        )
        # self.data["dt"] = ["Chapitre de livre"]

    def set_container(self, container):
        self.data["container_id"] = container.id
        self.data["year"] = container.year
        self.data["volume"] = make_int(container.volume)
        self.data["number"] = make_int(container.number)
        self.data["container_title_tex"] = container.title_tex
        self.data["container_title_html"] = container.title_html

    def pre_do(self):
        super().pre_do()

        self.data["classname"] = "Chapitre de livre"

        self.data["page_range"] = ""
        if not self.xobj.page_range:
            self.data["page_range"] = "p. "
            if self.xobj.fpage is not None:
                self.data["page_range"] += self.xobj.fpage
            if self.xobj.fpage and self.xobj.lpage:
                self.data["page_range"] += "-"
            if self.xobj.lpage is not None:
                self.data["page_range"] += self.xobj.lpage
        elif self.xobj.page_range[0] != "p":
            self.data["page_range"] = "p. " + self.xobj.page_range


#####################################################################
#
# solrSearchCmd:
#
# called from ptf/views.py; SolrRequest(request, q, alias=alias,
#                                       site=site,
#                                       default={'sort': '-score'})
#
# Warning: As of July 2018, only 1 site id is stored in a SolR document
#  Although the SolR schema is already OK to store multiple sites ("sites" is an array)
#  no Solr commands have been written to add/remove sites
#  We only have add commands.
#  Search only works if the Solr instance is meant for individual or ALL sites
#
######################################################################
class solrSearchCmd(solrCmd):
    def __init__(self, params={}):
        # self.q = '*:*'
        self.q = ""
        self.qs = None
        self.filters = []  # TODO: implicit filters
        self.start = None
        self.rows = None
        self.sort = "-score"  # use ',' to specify multiple criteria
        self.site = None
        self.search_path = ""

        super().__init__(params)

        self.required_params.extend(["qs"])

    def get_q(self, name, value, exclude, first, last):
        if name == "all" and value == "*":
            return "*:*"

        if value == "*":
            value = ""

        q = ""
        if exclude:
            q += "-"

        if name == "date":
            q += "year:[" + first + " TO " + last + "]"

        else:
            if name == "author":
                q += "au:"
            if name == "author_ref":
                q += "ar:"
            elif name == "title":
                q += "title_tex:"
            elif name == "body":
                q += "body:"
            elif name == "references":
                q += "bibitem:"
            elif name == "abstract":
                q += "trans_abstract_tex:"
            if len(value) > 0 and value[0] == '"' and value[-1] == '"':
                q += value
            elif name == "kwd":
                terms = value.split()
                q += (
                    "(kwd:("
                    + " AND ".join(terms)
                    + ") OR trans_kwd:("
                    + " AND ".join(terms)
                    + "))"
                )
            else:
                terms = value.split()
                # new_terms = [ "*{}*".format(t for t in terms)]
                q += "(" + " AND ".join(terms) + ")"

        return q

    def internal_do(self) -> search_helpers.SearchResults:
        super().internal_do()

        if settings.COLLECTION_PID == "CR":
            cr_ids = ["CRMATH", "CRMECA", "CRPHYS", "CRCHIM", "CRGEOS", "CRBIOL"]
            ids = [SITE_REGISTER[item.lower()]["site_id"] for item in cr_ids]
            self.filters.append(f"sites:[{min(ids)} TO {max(ids)}]")
        elif settings.COLLECTION_PID != "ALL":
            self.filters.append(f"sites:{settings.SITE_ID}")

        sort = "score desc"
        if self.sort:
            sorts = []
            sort_array = self.sort.split(",")
            for spec in sort_array:
                spec = spec.strip()
                if spec[0] == "-":
                    spec = f"{spec[1:]} desc"
                else:
                    spec = f"{spec} asc"
                sorts.append(spec)
                sorts.append("year desc")
            sort = ", ".join(sorts)

        use_ar_facet = True
        q = ""
        qt = []
        if self.qs:
            for qi in self.qs:
                qt.append(qi["name"])
                if qi["name"] == "author_ref":
                    use_ar_facet = False
                if qi["value"] or qi["first"]:
                    new_q = self.get_q(qi["name"], qi["value"], qi["not"], qi["first"], qi["last"])
                    q += new_q + " "
        if q:
            self.q = q

        facet_fields = ["collection_title_facet", "msc_facet", "dt", "year_facet"]

        if use_ar_facet:
            facet_fields.append("ar")

        if settings.COLLECTION_PID == "CR":
            facet_fields.append("sites")
        elif settings.COLLECTION_PID == "PCJ":
            facet_fields.append("classname")

        params = {
            "q.op": "AND",
            "sort": sort,
            "facet.field": facet_fields,
            # Decades are built manually because we allow the user to
            # expand a decade and see individual years
            "facet.range": "year_facet",
            "f.year_facet.facet.range.start": 0,
            "f.year_facet.facet.range.end": 3000,
            "f.year_facet.facet.range.gap": 10,
            "facet.mincount": 1,
            "facet.limit": 100,
            "facet.sort": "count",
            # 'fl': '*,score', # pour debug
            # 'debugQuery': 'true', # pour debug
            "hl": "true",
            # 'hl.fl': "*", -> par defaut, retourne les champs de qf
            "hl.snippets": 1,
            "hl.fragsize": 300,
            "hl.simple.pre": "<strong>",
            "hl.simple.post": "</strong>",
            "defType": "edismax",
            "tie": 0.1,  # si on ne specifie pas, le score est egal au max des scores sur chaque champ : là on
            # ajoute 0.1 x le score des autres champs
            # "df": 'text', Not used with dismax queries
            # We want to retrieve the highlights in both _tex ad _html.
            # We need to specify the 2 in qf
            "qf": [
                "au^21",
                "title_tex^13",
                "title_html^13",
                "trans_title_tex^13",
                "trans_title_html^13",
                "abstract_tex^8",
                "trans_abstract_tex^8",
                "kwd^5",
                "trans_kwd^5",
                "collection_title_html^3",
                "collection_title_tex^3",
                "body^2",
                "bibitem",
            ],
            # field ar est multivalué dédié aux facettes
            # field au est utilisé pour la recherche et pour l'affichage
            # des resultats
        }

        if self.start:
            params["start"] = self.start

        if self.rows:
            params["rows"] = self.rows

        if self.filters:
            params["fq"] = self.filters

        solr_results = solrFactory.get_solr().search(self.q, facet="true", **params)

        search_results = search_helpers.SearchResults(
            solr_results, self.search_path, self.filters, qt, use_ar_facet
        )

        return search_results


#####################################################################
#
# solrInternalSearchCmd:
#
# called from ptf/views.py/book by author
#
######################################################################
class solrInternalSearchCmd(solrCmd):
    def __init__(self, params={}):
        self.q = "*:*"
        self.qs = None
        self.filters = []  # TODO: implicit filters
        self.start = None
        self.rows = None
        self.sort = None  # '-score'  # use ',' to specify multiple criteria
        self.site = None
        self.search_path = ""
        self.facet_fields = []
        self.facet_limit = 100
        self.fl = None
        self.create_facets = True
        # 10/03/2023 - UNUSED
        self.related_articles = False

        super().__init__(params)

        self.required_params.extend(["q"])

    def internal_do(self) -> search_helpers.SearchInternalResults | pysolr.Results:
        super().internal_do()

        # 10/03/2023 - UNUSED
        if self.site:
            self.fq.append(f"sites:{self.site}")

        the_facet_fields = []
        use_year_facet = False
        for field in self.facet_fields:
            if field == "firstLetter":
                the_facet_fields.append("{!ex=firstletter}firstNameFacetLetter")
            elif field == "author_facet":
                the_facet_fields.append("ar")
            else:
                the_facet_fields.append(field)

                if field == "year_facet":
                    use_year_facet = True

        # 10/03/2023 - UNUSED
        if self.related_articles:
            params = {
                "q.op": "OR",
                "hl": "true",
                "hl.fl": "title_tex, trans_title_tex, trans_kwd, kwd",
                "hl.snippets": 1,
                "hl.fragsize": 0,
                "hl.simple.pre": "<strong>",
                "hl.simple.post": "</strong>",
                # "hl.method": "unified"
            }
        else:
            params = {
                "q.op": "AND",
                # 'fl': '*,score', # pour debug
                # 'debugQuery': 'true', # pour debug
                "facet.field": the_facet_fields,
                # ["{!ex=firstletter}firstNameFacetLetter", 'year_facet', 'collection_title_facet'],
                "facet.mincount": 1,
                "facet.limit": self.facet_limit,
                "facet.sort": "index",
            }

        if use_year_facet:
            # Decades are built manually because we allow the user to expand a
            # decade and see individual years
            params.update(
                {
                    "facet.range": "year_facet",
                    "f.year_facet.facet.range.start": 0,
                    "f.year_facet.facet.range.end": 3000,
                    "f.year_facet.facet.range.gap": 10,
                }
            )

        if self.sort:
            params["sort"] = self.sort

        if self.start:
            params["start"] = self.start

        if self.rows:
            params["rows"] = self.rows

        if self.filters:
            params["fq"] = self.filters

        if self.fl:
            params["fl"] = self.fl

        solr_results = solrFactory.get_solr().search(self.q, facet="true", **params)
        results = solr_results

        if self.create_facets:
            results = search_helpers.SearchInternalResults(
                solr_results, self.search_path, self.filters, self.facet_fields
            )

        return results


#####################################################################
#
# solrGetDocumentByPidCmd:
#
#
######################################################################


class solrGetDocumentByPidCmd(solrCmd):
    def __init__(self, params={}):
        self.pid = None

        super().__init__(params)

        self.required_params.extend(["pid"])

    def internal_do(self):
        super().internal_do()

        result = None

        search = "pid:" + self.pid
        results = solrFactory.get_solr().search(search)

        if results is not None:
            docs = results.docs

            if docs:
                result = docs[0]

        return result


class updateResourceSolrCmd(solrAddCmd):
    """ """

    def __init__(self, params=None):
        self.resource = None

        super().__init__(params)
        self.params = params

    def set_resource(self, resource):
        self.resource = resource
        self.id = resource.id
        self.pid = resource.pid

    def pre_do(self):
        doc = solrGetDocumentByPidCmd({"pid": self.pid}).do()
        if doc:
            self.data = {**doc, **self.params}
            if "_version_" in self.data:
                del self.data["_version_"]
            if "contributors" in self.data:
                solr_add_contributors_to_data(self.data["contributors"], self.data)
                self.data.pop("contributors")
            # if 'kwd_groups' in self.data:
            #     solr_add_kwd_groups_to_data(self.data['kwd_groups'], self.data)
            #     self.data.pop('kwd_groups')
        super().pre_do()


def research_more_like_this(article):
    results = {"docs": []}
    doc = solrGetDocumentByPidCmd({"pid": article.pid}).do()
    if doc:
        # fields = "au,kwd,trans_kwd,title_tex,trans_title_tex,abstract_tex,trans_abstract_tex,body"
        fields = settings.MLT_FIELDS if hasattr(settings, "MLT_FIELDS") else "all"
        boost = settings.MLT_BOOST if hasattr(settings, "MLT_BOOST") else "true"
        min_score = 80 if boost == "true" else 40
        min_score = settings.MLT_MIN_SCORE if hasattr(settings, "MLT_MIN_SCORE") else min_score
        params = {"debugQuery": "true", "mlt.interestingTerms": "details"}
        params.update({"mlt.boost": boost, "fl": "*,score"})
        params.update({"mlt.minwl": 4, "mlt.maxwl": 100})
        params.update({"mlt.mintf": 2, "mlt.mindf": 2})
        params.update({"mlt.maxdfpct": 1, "mlt.maxqt": 50})
        # params.update({"mlt.qf": "trans_kwd^90 title_tex^80 body^1.7"})

        pid = article.pid.split("_")[0]
        if pid[:2] == "CR":
            # search suggested articles in all CR
            params.update({"fq": r"pid:/CR.*/"})
        else:
            params.update({"fq": f"pid:/{pid}.*/"})

        solr = solrFactory.get_solr()
        similar = solr.more_like_this(q=f'id:{doc["id"]}', mltfl=fields, **params)
        params.update({"q": f'id:{doc["id"]}', "mlt.fl": fields})
        params.update({"min_score": min_score})
        results["params"] = dict(sorted(params.items()))
        results["docs"] = similar.docs
        results["numFound"] = similar.raw_response["response"]["numFound"]
        results["interestingTerms"] = similar.raw_response["interestingTerms"]
        results["explain"] = similar.debug["explain"]
    return results


def is_excluded_suggested_article(title):
    match = settings.MLT_EXCLUDED_TITLES if hasattr(settings, "MLT_EXCLUDED_TITLES") else []
    start = (
        settings.MLT_EXCLUDED_TITLES_START
        if hasattr(settings, "MLT_EXCLUDED_TITLES_START")
        else []
    )
    return title.startswith(tuple(start)) or title in match


def auto_suggest_doi(suggest, article, results=None):
    if not results:
        results = research_more_like_this(article)

    if results and suggest.automatic_list:
        doi_list = []
        for item in results["docs"][:3]:
            if item["score"] > results["params"]["min_score"]:
                doi = item.get("doi", "")
                title = item.get("title_tex", "")
                if doi not in doi_list and not is_excluded_suggested_article(title):
                    doi_list.append(doi)
        suggest.doi_list = "\n".join(doi_list)
    return results
