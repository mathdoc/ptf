##################################################################################################
#
# README
#
#
##################################################################################################

from collections import OrderedDict

from ptf import model_helpers
from ptf.model_data_converter import db_to_journal_data

TEST_STRICT = False


def create_diff_obj(a, b, label):
    diff_obj = []
    result = True

    # contrib_xml is updated if you edit an article: other fields will be different
    # first_initials is not stored in the database
    if (
        label == "contrib_xml"
        or label == "first_initials"
        or label == "label_prefix"
        or label == "value_html"
        or label == "value_tex"
    ) and not TEST_STRICT:
        return result, diff_obj

    if hasattr(a, "__dict__"):
        a = a.__dict__
    if hasattr(b, "__dict__"):
        b = b.__dict__

    if type(a) == list and type(b) == list:
        if len(a) != len(b) and label != "streams":  # TeX can be added in streams
            result = False
            diff_obj.append(["len", len(a), len(b)])
        elif label not in [
            "streams",
            "supplementary_materials",
        ]:  # Cedrics import change the location: ignore the values
            for obj1, obj2 in zip(a, b):
                result_obj, list_diff_obj = create_diff_obj(obj1, obj2, label)
                if not result_obj:
                    result = False

                    if label == "contrib_groups":
                        child_label = obj1["content_type"]
                    elif label == "contribs":
                        child_label = obj1["string_name"]
                    else:
                        child_label = label

                    diff_obj.append([child_label, list_diff_obj])
    elif type(a) == dict and type(b) == dict:
        a = OrderedDict(sorted(a.items()))
        b = OrderedDict(sorted(b.items()))
        for key, value in a.items():
            if key in b:
                result_obj, values_diff_obj = create_diff_obj(value, b[key], key)
                if not result_obj:
                    result = False
                    diff_obj.append(values_diff_obj)
            else:
                result = False
                diff_obj.append([key, value, ""])
        for key, value in b.items():
            if key not in a:
                result = False
                diff_obj.append([key, "", value])
    else:
        if type(a) != type(b):
            result = False
        elif type(a) == str and type(b) == str:
            a = a.replace(' xmlns:xlink="http://www.w3.org/1999/xlink"', "")
            b = b.replace(' xmlns:xlink="http://www.w3.org/1999/xlink"', "")

            # The Cedrics -> JATS changes the order
            a = a.replace('close="" open="{" separators="', "")
            b = b.replace('separators="" open="{" close="', "")
            a = a.replace('separators="" open="|" close="', "")
            b = b.replace('close="" open="|" separators="', "")

            try:
                a_date = model_helpers.parse_date_str(a)
                b_date = model_helpers.parse_date_str(b)
                a = a_date
                b = b_date
            except:
                pass
            result = a == b
        else:
            result = a == b

        if not result:
            diff_obj = [label, a, b]

            if type(a) == str:
                i = 0
                while i < len(a) and i < len(b):
                    if a[i] != b[i]:
                        print(label, i, a[i : min(len(a), i + 30)])
                        print(label, i, b[i : min(len(b), i + 30)])
                        break
                    i += 1

    return result, diff_obj


class BaseComparator:
    def compare(self, obj1, obj2, diff_dict):
        return True

    def compare_list_of_simple_types(self, obj1, obj2, diff_dict, list_of_simple_types):
        result = True

        for attr in list_of_simple_types:
            # Matching info were reported in /cedram_dev/exploitation, not in /cedram_dev/production_tex
            # We have to ignore the DOIs
            if not (attr in ["doi", "citation_xml"] and hasattr(obj1, "citation_xml")) and not (
                attr == "doi"
                and hasattr(obj2, "pid")
                and obj2.pid is not None
                and obj2.pid.startswith("AIF_")
            ):
                obj1_attr = getattr(obj1, attr)
                obj2_attr = getattr(obj2, attr)

                if attr == "provider" and obj2_attr == "numdam":
                    obj2_attr = "mathdoc"

                if (
                    type(obj1_attr) == str
                    and obj1_attr is not None
                    and type(obj2_attr) == str
                    and obj2_attr is not None
                ):
                    obj1_attr = obj1_attr.replace(
                        ' xmlns:xlink="http://www.w3.org/1999/xlink"', ""
                    )
                    obj2_attr = obj2_attr.replace(
                        ' xmlns:xlink="http://www.w3.org/1999/xlink"', ""
                    )

                if obj1_attr != obj2_attr:
                    result = False
                    diff_dict[attr] = [obj1_attr, obj2_attr]

        return result

    def compare_list_of_list(self, obj1, obj2, diff_dict, list_of_list):
        result = True

        for attr in list_of_list:
            obj1_attr = getattr(obj1, attr)
            obj2_attr = getattr(obj2, attr)

            # Matching info were reported in /cedram_dev/exploitation, not in /cedram_dev/production_tex
            # We have to ignore the DOIs
            if TEST_STRICT or (
                not (attr == "extids" and hasattr(obj1, "citation_xml"))
                and not (
                    attr == "ids"
                    and hasattr(obj2, "pid")
                    and obj2.pid is not None
                    and obj2.pid.startswith("AIF_")
                )
            ):
                result_obj, diff_obj = create_diff_obj(obj1_attr, obj2_attr, attr)
                if not result_obj:
                    result = False
                    diff_dict[attr] = diff_obj

        return result

    def compare_list_of_objs(self, obj1, obj2, diff_dict, attr, attr_id_list, comparator):
        result = True

        list1 = getattr(obj1, attr)
        list2 = getattr(obj2, attr)
        if len(list1) != len(list2):
            if attr != "articles":
                diff_dict[attr] = [{"len": [len(list1), len(list2)]}]
        else:
            attr_diffs = []

            for list1_obj, list2_obj in zip(list1, list2):
                attr_diff_dict = {}
                if not comparator.compare(list1_obj, list2_obj, attr_diff_dict):
                    result = False
                    params = {"diff": attr_diff_dict}
                    for attr_id in attr_id_list:
                        params[attr_id] = getattr(list1_obj, attr_id)
                    attr_diffs.append(params)

            if not result:
                diff_dict[attr] = attr_diffs

        return result


class ResourceDataComparator(BaseComparator):
    def compare(self, obj1, obj2, diff_dict):
        result = super().compare(obj1, obj2, diff_dict)

        # Ignore trans_title_xml: it is not used in the Django DB
        result = (
            self.compare_list_of_simple_types(
                obj1,
                obj2,
                diff_dict,
                [
                    "lang",
                    "pid",
                    "doi",
                    "title_xml",
                    "title_tex",
                    "title_html",
                    "trans_lang",
                    "trans_title_html",
                    "trans_title_tex",
                    "funding_statement_html",
                    "footnotes_html",
                ],
            )
            and result
        )

        if False and len(obj1.abstracts) > 0 and len(obj2.abstracts) > 0:
            j = 0
            while j < len(obj1.abstracts):
                a1 = obj1.abstracts[j]["value_xml"]
                a2 = obj2.abstracts[j]["value_xml"]

                if a1 != a2:
                    j = len(obj1.abstracts)
                    i = 0
                    while i < len(a1) and i < len(a2):
                        if a1[i] != a2[i]:
                            print(i, a1[i], a2[i], ord(a1[i]), ord(a2[i]))
                        i += 1
                j += 1

        result = (
            self.compare_list_of_list(
                obj1,
                obj2,
                diff_dict,
                [
                    "abstracts",
                    "awards",
                    "relations",
                    # 'ids', 'extids',
                    # 'ext_links',
                    "streams",
                    "related_objects",
                    "counts",
                    "contributors",
                    "kwds",
                    "kwd_groups",
                    "figures",
                    "supplementary_materials",
                ],
            )
            and result
        )

        if TEST_STRICT:
            result = (
                self.compare_list_of_list(obj1, obj2, diff_dict, ["ids", "extids", "ext_links"])
                and result
            )

        result = (
            self.compare_list_of_objs(
                obj1, obj2, diff_dict, "bibitems", ["label"], RefDataComparator()
            )
            and result
        )

        return result


class MathdocPublicationDataComparator(ResourceDataComparator):
    def compare(self, obj1, obj2, diff_dict):
        result = super().compare(obj1, obj2, diff_dict)

        result = (
            self.compare_list_of_simple_types(
                obj1, obj2, diff_dict, ["coltype", "e_issn", "wall", "provider"]
            )
            and result
        )

        return result


class PublisherDataComparator(BaseComparator):
    def compare(self, obj1, obj2, diff_dict):
        if obj1 is None and obj2 is None:
            return True

        result = super().compare(obj1, obj2, diff_dict)

        result = (
            self.compare_list_of_simple_types(obj1, obj2, diff_dict, ["name", "loc"]) and result
        )

        return result


class JournalDataComparator(ResourceDataComparator):
    def compare(self, obj1, obj2, diff_dict):
        result = super().compare(obj1, obj2, diff_dict)

        publisher_comparator = PublisherDataComparator()
        pub_diff = {}
        if not publisher_comparator.compare(obj1.publisher, obj2.publisher, pub_diff):
            result = False
            diff_dict["publisher"] = pub_diff

        return result


class IssueDataComparator(ResourceDataComparator):
    def compare(self, obj1, obj2, diff_dict):
        result = super().compare(obj1, obj2, diff_dict)

        journal_comparator = JournalDataComparator()
        journal_diff = {}
        if not journal_comparator.compare(obj1.journal, obj2.journal, journal_diff):
            result = False
            diff_dict["journal"] = journal_diff

        publisher_comparator = PublisherDataComparator()
        pub_diff = {}
        if not publisher_comparator.compare(obj1.publisher, obj2.publisher, pub_diff):
            result = False
            diff_dict["publisher"] = pub_diff

        result = (
            self.compare_list_of_simple_types(
                obj1, obj2, diff_dict, ["provider", "ctype", "year", "vseries", "volume", "number"]
            )
            and result
        )

        # Ignore last_modified (it's OK if it has been edited in ptf-tools)
        # Ignore prod_deployed_date (set during import by database cmds: Django DB is different from the XML)

        result = (
            self.compare_list_of_objs(
                obj1, obj2, diff_dict, "articles", ["pid", "doi"], ArticleDataComparator()
            )
            and result
        )

        return result


class ArticleDataComparator(ResourceDataComparator):
    def compare(self, obj1, obj2, diff_dict):
        result = super().compare(obj1, obj2, diff_dict)

        result = (
            self.compare_list_of_simple_types(
                obj1,
                obj2,
                diff_dict,
                [
                    "pid",
                    "atype",
                    "seq",
                    "article_number",
                    "talk_number",
                    "fpage",
                    "lpage",
                    "page_range",
                    "size",
                    "page_type",
                    "elocation",
                    "coi_statement",
                ],
            )
            and result
        )

        dates1 = list(obj1.history_dates)
        dates2 = list(obj2.history_dates)
        # Ignore date_published as it is OK if they are different between DjangoDB and the XML

        for date1 in dates1:
            type1 = date1["type"]
            if type1 != "online":
                dates = [date for date in dates2 if date["type"] == type1]
                if len(dates) == 0:
                    result = False
                    diff_dict[type1] = [date1["date"], ""]
                else:
                    date2 = dates[0]
                    d1 = model_helpers.parse_date_str(date1["date"])
                    d2 = model_helpers.parse_date_str(date2["date"])
                    if d1 != d2:
                        result = False
                        diff_dict[type1] = [date1["date"], date2["date"]]
        for date2 in dates2:
            type2 = date2["type"]
            if type2 != "online":
                dates = [date for date in dates1 if date["type"] == type2]
                if len(dates) == 0:
                    result = False
                    diff_dict[type2] = ["", date2["date"]]

        return result


class RefDataComparator(ResourceDataComparator):
    def compare(self, obj1, obj2, diff_dict):
        result = super().compare(obj1, obj2, diff_dict)

        # Ignore citation_*. The difference will be flagged with the ext_ids
        result = (
            self.compare_list_of_simple_types(
                obj1,
                obj2,
                diff_dict,
                [
                    "lang",
                    "user_id",
                    "label",
                    # 'label_prefix', 'label_suffix',
                    "type",
                    "publisher_name",
                    "publisher_loc",
                    "institution",
                    "series",
                    "volume",
                    "issue",
                    "month",
                    "year",
                    "comment",
                    "annotation",
                    "fpage",
                    "lpage",
                    "page_range",
                    "size",
                    "source_tex",
                    "article_title_tex",
                    "chapter_title_tex",
                ],
            )
            and result
        )

        if TEST_STRICT:
            result = (
                self.compare_list_of_simple_types(
                    obj1, obj2, diff_dict, ["citation_xml", "citation_html", "citation_tex"]
                )
                and result
            )

        result = self.compare_list_of_list(obj1, obj2, diff_dict, ["contributors"]) and result

        return result


class CollectionDataComparator(ResourceDataComparator):
    def compare(self, obj1, obj2, diff_dict):
        result = super().compare(obj1, obj2, diff_dict)

        result = (
            self.compare_list_of_simple_types(
                obj1, obj2, diff_dict, ["coltype", "issn", "e_issn", "volume", "vseries", "seq"]
            )
            and result
        )

        return result


class BookDataComparator(ResourceDataComparator):
    def compare(self, obj1, obj2, diff_dict):
        result = super().compare(obj1, obj2, diff_dict)

        result = (
            self.compare_list_of_simple_types(
                obj1, obj2, diff_dict, ["ctype", "provider", "frontmatter", "body"]
            )
            and result
        )

        publisher_comparator = PublisherDataComparator()
        pub_diff = {}
        if not publisher_comparator.compare(obj1.publisher, obj2.publisher, pub_diff):
            result = False
            diff_dict["publisher"] = pub_diff

        result = (
            self.compare_list_of_objs(
                obj1, obj2, diff_dict, "incollection", ["pid"], CollectionDataComparator()
            )
            and result
        )

        result = (
            self.compare_list_of_objs(
                obj1, obj2, diff_dict, "parts", ["pid"], BookPartDataComparator()
            )
            and result
        )

        return result


class BookPartDataComparator(ArticleDataComparator):
    def compare(self, obj1, obj2, diff_dict):
        result = super().compare(obj1, obj2, diff_dict)

        result = (
            self.compare_list_of_simple_types(obj1, obj2, diff_dict, ["frontmatter"]) and result
        )

        return result


def prepare_issue_for_comparison(xml_issue):
    # xml_cmds does not use the jats_parser collection but retrieve it from the database
    journal_pid = xml_issue.journal.pid
    collection = model_helpers.get_collection(journal_pid)

    xml_issue.journal = db_to_journal_data(collection)

    for article in xml_issue.articles:
        for ref in article.bibitems:
            # the Django DB ignores the ref doi although it is set by the XML parser.
            ref.doi = None
