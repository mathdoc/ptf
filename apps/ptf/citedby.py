import concurrent.futures
import html
import re
from collections import defaultdict
from datetime import timedelta
from difflib import SequenceMatcher

import xmltodict
from bs4 import BeautifulSoup
from pylatexenc.latex2text import LatexNodes2Text
from requests.exceptions import ConnectionError
from requests.exceptions import Timeout
from requests_cache import CachedSession
from requests_cache import FileCache

from django.conf import settings

from ptf.bibtex import parse_bibtex
from ptf.cmds.xml.xml_utils import normalise_span
from ptf.model_data import RefData
from ptf.model_data import create_contributor
from ptf.model_data_converter import update_ref_data_for_jats
from ptf.model_helpers import get_extid
from ptf.models import BibItemId
from ptf.models import get_names
from ptf.utils import get_display_name

ADS_URL = "https://api.adsabs.harvard.edu/v1/search"
ARXIV_URL = "https://export.arxiv.org/api/query"
CROSSREF_URL = "https://doi.crossref.org/servlet/getForwardLinks"
SEMANTIC_URL = "https://api.semanticscholar.org/v1/paper/"
ZBMATH_URL = "https://zbmath.org"

ADS = "NASA ADS"
CROSSREF = "Crossref"
SEMANTIC = "Semantic Scholar"
ZBMATH = "zbMATH"


TIMEOUT = 4.0

PRIORITY = defaultdict(int, {ZBMATH: 10, ADS: 9, CROSSREF: 8, SEMANTIC: 7})

LATEX_PARSER = LatexNodes2Text(math_mode="verbatim")

session = CachedSession(
    backend=FileCache(
        getattr(settings, "REQUESTS_CACHE_LOCATION", None) or "/tmp/ptf_requests_cache",
        decode_content=False,
    ),
    headers={
        "User-Agent": getattr(settings, "REQUESTS_USER_AGENT", None) or "Mathdoc/1.0.0",
        "From": getattr(settings, "REQUESTS_EMAIL", None) or "accueil@listes.mathdoc.fr",
    },
    expire_after=timedelta(days=30),
)


def create_refdata(lang="und"):
    data = RefData(lang=lang)
    data.type = "misc"
    data.doi = None
    data.arxiv = None
    data.zbl = None
    data.semantic = None
    return data


def is_same_title(compare, titles, tol=0.90):
    compare = re.sub(r"\W", "", compare).lower()
    for title in titles:
        title = re.sub(r"\W", "", title).lower()
        if SequenceMatcher(None, compare, title).ratio() > tol:
            return True
    return False


def get_zbmath_bibtex(params):
    text = ""
    headers = {"Content-Type": "text/html"}
    response = session.get(ZBMATH_URL, params=params, headers=headers, timeout=0.5 * TIMEOUT)
    soup = BeautifulSoup(response.text, "html.parser")
    results = soup.find("div", {"class": "citations"})
    if results:
        for ref in results.find_all("a", href=True):
            headers = {"Content-Type": "text/x-bibtex"}
            url = ZBMATH_URL + "/bibtexoutput" + ref.get("href", "")
            response = session.get(url, headers=headers, timeout=0.5 * TIMEOUT)
            response.encoding = "utf-8"
            text += response.text
    return text


def citedby_zbmath(metadata):
    if "zbl_id" in metadata:
        params = {"q": "an:" + metadata["zbl_id"]}
    else:
        params = {"q": "en:" + metadata["doi"]}
        title_tex = normalise_span(metadata["title"]).replace("\xa0", " ")
        authors = "&au:".join(metadata["authors"])
        params = {"q": params["q"] + "|(ti:" + f'"{title_tex}"' + "&au:" + authors + ")"}
    text = get_zbmath_bibtex(params)
    citations = parse_bibtex(text)
    return citations


def citedby_crossref(metadata):
    citations = []
    user = settings.CROSSREF_USER
    password = settings.CROSSREF_PWD
    url = f"{CROSSREF_URL}?usr={user}&pwd={password}&doi={metadata['doi']}"
    response = session.get(url, timeout=TIMEOUT)
    response.encoding = "utf-8"
    if response.status_code == 200:
        data = xmltodict.parse(response.text)
        body = data["crossref_result"]["query_result"]["body"]
        if body:
            citations = body["forward_link"]

    if not isinstance(citations, list):
        citations = [citations]
    return citations


def get_arxiv_id(metadata):
    arxiv_id = None
    title_tex = normalise_span(metadata["title"]).replace("\xa0", " ")
    headers = {"Content-Type": "application/atom+xml"}
    query = "doi:" + metadata["doi"] + " OR (ti:" + f'"{title_tex}"' + ")"
    params = {"search_query": query, "max_results": 1}
    response = session.get(ARXIV_URL, params=params, headers=headers, timeout=0.5 * TIMEOUT)
    if response.status_code == 200:
        data = xmltodict.parse(response.text)
        if "entry" in data["feed"]:
            entry = data["feed"]["entry"]
            if is_same_title(title_tex, [entry["title"]]):
                arxiv_id = entry["id"].split("arxiv.org/abs/")
                arxiv_id = arxiv_id[-1].split("v")[0]
    return arxiv_id


def citedby_ads(metadata, by_doi=True, citedby=True):
    if by_doi:
        arxiv_id = get_arxiv_id(metadata)
    else:
        arxiv_id = metadata["arxiv_id"]
    if not arxiv_id:
        return []

    citations = []
    url = ADS_URL + "/query"
    headers = {"Authorization": f"Bearer:{settings.ADS_TOKEN}"}
    reference = "citation" if citedby else "reference"
    params = {"q": "identifier:" + arxiv_id, "fl": reference}
    response = session.get(url, headers=headers, params=params, timeout=0.5 * TIMEOUT)
    if response.status_code == 200:
        results = response.json().get("response", {}).get("docs")
        if results and isinstance(results, list) and reference in results[0]:
            url = ADS_URL + "/bigquery"
            bibcodes = "bibcode\n" + "\n".join(results[0][reference])
            filters = "abstract,author,bibcode,comment,doi,doctype,"
            filters += "eid,identifier,issue,keyword,orcid_pub,"
            filters += "page,page_count,page_range,pub,pub_raw,title,volume,year"
            params = {"q": "*:*", "fl": filters, "rows": 200}
            response = session.post(
                url, params=params, headers=headers, data=bibcodes, timeout=0.5 * TIMEOUT
            )
            response.encoding = "utf-8"
            if response.status_code == 200:
                citations = response.json().get("response", {}).get("docs")
    return citations


def citedby_semantic(metadata, citedby=True):
    citations = []
    reference = "citations" if citedby else "references"
    if settings.SITE_ID != 36:  # all but PCJ
        response = session.get(SEMANTIC_URL + metadata["doi"], timeout=TIMEOUT)
        response.encoding = "utf-8"
        if response.status_code == 200:
            citations.extend(response.json()[reference])
    return citations


def set_contributors(ref, api_contributors, orcids=None):
    if not isinstance(api_contributors, list):
        api_contributors = [api_contributors]

    contributors = []
    for contributor in api_contributors:
        first_name = last_name = ""
        if ref.provider == CROSSREF:
            first_name = contributor.get("given_name")
            last_name = contributor.get("surname")
        elif ref.provider in [ADS, ZBMATH]:
            result = contributor.split(", ")
            if result:
                first_name = result[1] if len(result) > 1 else ""
                last_name = result[0]
        elif ref.provider == SEMANTIC:
            result = contributor["name"].split(" ")
            if result:
                first_name = " ".join(result[0:-1])
                last_name = result[-1]
        contributor = create_contributor()
        contributor["first_name"] = first_name.strip() if first_name else ""
        contributor["last_name"] = last_name.strip() if last_name else ""
        contributor["role"] = "author"
        contributors.append(contributor)

    if orcids and len(contributors) == len(orcids):
        for contrib, orcid in zip(contributors, orcids):
            contrib["orcid"] = orcid if orcid != "-" else ""
    setattr(ref, "contributors", contributors)


def ads_to_bibtex_type(doc_type):
    if doc_type in ["article", "eprint"]:
        bibtex_type = "article"
    elif doc_type in [
        "book",
        "inbook",
        "inproceedings",
        "mastersthesis",
        "phdthesis",
        "proceedings",
        "techreport",
    ]:
        bibtex_type = doc_type
    else:
        bibtex_type = "misc"
    return bibtex_type


def crossref_to_bibtex_type(doc_type, item):
    if doc_type == "journal_cite":
        bibtex_type = "article"
    elif doc_type == "conf_cite":
        if "paper_title" in item:
            bibtex_type = "inproceedings"
        else:
            bibtex_type = "proceedings"
    elif doc_type == "book_cite":
        if "chapter_title" in item:
            bibtex_type = "inbook"
        else:
            bibtex_type = "book"
    else:
        bibtex_type = "misc"
    return bibtex_type


def citedby_crossref_refs(citations):
    refdata = []
    for item in citations:
        item.pop("@doi")  # the interior orderdict remains
        if not item:
            continue
        doc_type, item = item.popitem()
        ref = create_refdata()
        setattr(ref, "provider", CROSSREF)
        setattr(ref, "type", crossref_to_bibtex_type(doc_type, item))
        if "journal_title" in item and item["journal_title"]:
            setattr(ref, "source_tex", item["journal_title"])
        if "article_title" in item and item["article_title"]:
            setattr(ref, "article_title_tex", item["article_title"])
        if "volume_title" in item:  # book or proceedings title
            setattr(ref, "source_tex", item["volume_title"])
        if "paper_title" in item and item["paper_title"]:  # inproceedings title
            setattr(ref, "article_title_tex", item["paper_title"])
        if "chapter_title" in item and item["chapter_title"]:  # incollection or inbook
            setattr(ref, "chapter_title_tex", item["chapter_title"])
        if "first_page" in item:
            setattr(ref, "fpage", item["first_page"])
        if "last_page" in item:
            setattr(ref, "lpage", item["last_page"])
        if "volume" in item:
            setattr(ref, "volume", item["volume"])
        if "issue" in item:
            setattr(ref, "issue", item["issue"])
        if "year" in item and item["year"]:
            setattr(ref, "year", item["year"])
        if "contributors" in item and "contributor" in item["contributors"]:
            set_contributors(ref, item["contributors"]["contributor"])
        if "doi" in item and item["doi"]:
            setattr(ref, "doi", item["doi"]["#text"].lower())
        refdata.append(ref)
    return refdata


def citedby_zbmath_refs(citations):
    return bibtex_to_refs(citations)


def is_misc(doctype):
    if doctype not in [
        "article",
        "book",
        # "booklet",
        "conference",
        "inbook",
        "incollection",
        "inproceedings",
        # "manual",
        # "mastersthesis",
        "phdthesis",
        "proceedings",
        "techreport",
    ]:
        return True
    return False


def bibtex_to_refs(bibitems):
    refdata = []
    for item in bibitems:
        ref = create_refdata()
        setattr(ref, "provider", ZBMATH)
        item["doctype"] = "misc" if is_misc(item["doctype"]) else item["doctype"]
        setattr(ref, "type", item["doctype"])
        if "fjournal" in item:
            setattr(ref, "source_tex", item["fjournal"])
        elif "journal" in item:
            setattr(ref, "source_tex", item["journal"])
        elif "booktitle" in item:
            setattr(ref, "source_tex", item["booktitle"])
        elif "howpublished" in item:
            howpublished = re.sub(r" \([0-9]{4}\)\.?", "", item["howpublished"])
            setattr(ref, "source_tex", howpublished)
        if "fseries" in item:
            setattr(ref, "series", item["fseries"])
        elif "series" in item:
            setattr(ref, "series", item["series"])
        if "title" in item:
            if item["doctype"] in ["article", "misc"]:
                setattr(ref, "article_title_tex", item["title"])
            elif item["doctype"] in ["incollection", "inproceedings", "inbook"]:
                setattr(ref, "chapter_title_tex", item["title"])
            else:
                setattr(ref, "source_tex", item["title"])
        if "url" in item and not ref.source_tex:
            setattr(ref, "source_tex", item["url"])
        if "pages" in item and item["pages"]:
            result = [x for x in re.split(r"\W", item["pages"])]
            setattr(ref, "fpage", result[0])
            if len(result) == 2:
                setattr(ref, "lpage", result[1])
        if "volume" in item:
            setattr(ref, "volume", item["volume"])
        if "number" in item:
            setattr(ref, "issue", item["number"])
        if "issue" in item:
            setattr(ref, "issue", item["issue"])
        if "note" in item:
            setattr(ref, "comment", item["note"])
        if "year" in item:
            setattr(ref, "year", item["year"])
        if "author" in item:
            set_contributors(ref, item["author"].split(" and "))
        if "publisher" in item:
            setattr(ref, "publisher_name", item["publisher"])
        elif "school" in item:
            setattr(ref, "publisher_name", item["school"])
        elif "institution" in item:
            setattr(ref, "publisher_name", item["institution"])
        if "address" in item:
            setattr(ref, "publisher_loc", item["address"])
        if "doi" in item and item["doi"]:
            setattr(ref, "doi", item["doi"].lower())
        if "zbmath" in item:
            setattr(ref, "zbl", item["zbmath"])
        if "zbl" in item:
            setattr(ref, "zbl", item["zbl"])
        refdata.append(ref)
    return refdata


def citedby_ads_refs(citations):
    refdata: list[RefData] = []
    for item in citations:
        ref = create_refdata()
        setattr(ref, "provider", ADS)
        setattr(ref, "bibcode", item["bibcode"])
        setattr(ref, "type", ads_to_bibtex_type(item["doctype"]))
        if "title" in item and item["title"]:
            setattr(ref, "article_title_tex", item["title"][0])
        if "page_range" in item:
            result = item["page_range"].split("-")
            if len(result) == 2:
                setattr(ref, "fpage", result[0])
                setattr(ref, "lpage", result[1])
            elif "page" in item and item["page"] and item["page"][0].isdigit():
                setattr(ref, "fpage", item["page"][0])
                if "page_count" in item and item["page_count"]:
                    setattr(ref, "lpage", str(item["page_count"] - 1))
        if "year" in item and item["year"]:
            setattr(ref, "year", item["year"])
        if "author" in item and item["author"]:
            set_contributors(ref, item["author"], item.get("orcid_pub", []))
        if "issue" in item:
            setattr(ref, "issue", item["issue"])
        if "volume" in item:
            setattr(ref, "volume", item["volume"])
        if "doi" in item and item["doi"]:
            setattr(ref, "doi", item["doi"][0].lower())
        if "eid" in item and item["eid"]:
            arxiv = item["eid"].split("arXiv:")
            if "pub" in item and "arXiv" in item["pub"]:
                setattr(ref, "arxiv", arxiv[-1])
                setattr(ref, "source_tex", "arXiv")
        if "pub_raw" in item and item["pub_raw"] and ref.doi and not ref.arxiv:
            result = re.match(r"(^.+)?[,.]( vol. | Volume )", item["pub_raw"])
            if result:
                setattr(ref, "source_tex", result.group(1))
        elif "pub" in item and not ref.arxiv:
            setattr(ref, "source_tex", item["pub"])
        if "abstract" in item and item["abstract"]:
            setattr(ref, "abstract", [item["abstract"]])
        refdata.append(ref)
    return refdata


def citedby_semantic_refs(citations):
    refdata = []
    for item in citations:
        ref = create_refdata()
        setattr(ref, "provider", SEMANTIC)
        if "title" in item:
            title = item["title"]
            title = title.capitalize() if title.isupper() else item["title"]
            setattr(ref, "article_title_tex", title)
        if "year" in item and item["year"]:
            setattr(ref, "year", str(item["year"]))
        if "authors" in item and item["authors"]:
            set_contributors(ref, item["authors"])
        if "doi" in item and item["doi"]:
            setattr(ref, "doi", item["doi"].lower())
        if "arxivId" in item and item["arxivId"]:
            setattr(ref, "arxiv", item["arxivId"])
            setattr(ref, "source_tex", "arXiv")
        if "venue" in item and item["venue"]:
            setattr(ref, "source_tex", item["venue"])
        if "paperId" in item:
            setattr(ref, "semantic", item["paperId"])
        refdata.append(ref)
    return refdata


def get_extlinks(extids):
    extlinks = []
    for extid in extids:
        eid = BibItemId()
        eid.id_type, eid.id_value = extid
        extlink = ""
        if eid.id_type == "doi":
            extlink = "DOI:" + eid.id_value
        elif eid.id_type == "arxiv":
            extlink = "arXiv:" + eid.id_value
        elif eid.id_type == "zbl-item-id":
            extlink = "Zbl:" + eid.id_value
        elif eid.id_type == "semantic-scholar":
            extlink = "Semantic-scholar:" + eid.id_value
        if extlink:
            extlink = f' | <a href="{eid.get_href()}">{extlink}</a>'
            extlinks.append(extlink)
    return extlinks


def built_extlinks(ref):
    extids = []
    if ref.doi:
        extids.append(("doi", ref.doi))
    if ref.arxiv:
        extids.append(("arxiv", ref.arxiv))
    if ref.zbl:
        extids.append(("zbl-item-id", ref.zbl))
    if not any((ref.doi, ref.zbl, ref.arxiv)) and getattr(ref, "semantic", False):
        extids.append(("semantic-scholar", ref.semantic))
    setattr(ref, "extids", extids)


def get_values_for_stats(refs):
    """
    extract data of a ref and return as a dict
    @param refs: dict of RefData.__dict__
    @return: dict
    """

    citedby_for_stats = []
    for ref_item in refs.values():
        authors = []
        for author in ref_item.get("contributors"):
            if author["role"] == "author":
                display_name = get_display_name(
                    author["prefix"],
                    author["first_name"],
                    author["last_name"],
                    author["suffix"],
                    author["string_name"],
                )
                authors.append({"author": display_name})

        title_key = get_publication_title(ref_item, "title")
        title = ref_item[title_key]
        publication_title_key = get_publication_title(ref_item, "publication_title")
        publication_title = ref_item[publication_title_key]

        url = ""
        if ref_item["extlinks"]:
            result = re.search(r'href="(.+)">', ref_item["extlinks"][0])
            url = result.group(1) if result else ""

        result = {
            "authors": authors,
            "title": title,
            "publication_title": publication_title,
            "year": ref_item["year"],
            "url": url,
            "source": ref_item["provider"],
        }
        citedby_for_stats.append(result)
    return citedby_for_stats


def get_publication_title(ref_item, category="title"):
    type_ = ref_item.get("type")

    if "thesis" in type_:
        type_ = "thesis"
    else:
        type_ = "misc"

    dic = {
        "incollection": {"title": "source_tex", "publication_title": "series"},
        "thesis": {"title": "source_tex", "publication_title": "series"},
        "article": {"title": "article_title_tex", "publication_title": "source_tex"},
        "book": {"title": "source_tex", "publication_title": "series"},
        "inbook": {"title": "chapter_title_tex", "publication_title": "series"},
        "misc": {"title": "article_title_tex", "publication_title": "source_tex"},
    }
    return dic.get(type_).get(category)


def built_citations(data):
    # to match citations and add these ids when missing
    doi_arxiv = {ref.doi: ref.arxiv for ref in data if ref.doi and ref.arxiv}
    arxiv_doi = {v: k for k, v in doi_arxiv.items()}

    results = []
    for n, ref in enumerate(data):
        if ref.arxiv and not ref.doi:
            setattr(ref, "doi", arxiv_doi.get(ref.arxiv))
        elif not ref.arxiv and ref.doi:
            setattr(ref, "arxiv", doi_arxiv.get(ref.doi))
        built_extlinks(ref)
        update_ref_data_for_jats(ref, n, with_label=False)
        ref.citation_html = html.unescape(ref.citation_html)
        results.append(vars(ref))

    results.sort(
        key=lambda k: (
            -int(k["year"]) if k["year"] else 0,
            k["source_tex"],
            k["volume"],
            k["issue"],
            k["fpage"],
        ),
    )

    refs = {}
    titles = {
        item[get_publication_title(item)]
        for item in results
        if any((item["arxiv"], item["doi"], item["zbl"]))
    }

    for item in results:
        links = get_extlinks(item["extids"])
        level = PRIORITY[item["provider"]]
        citation = LATEX_PARSER.latex_to_text(item["citation_html"].replace("$$", "$"))
        ref = {"html": citation + "".join(links)}
        ref.update({"priority": level, "extlinks": links})
        ref.update(item)

        if item["doi"]:
            if item["doi"] not in refs or refs[item["doi"]]["priority"] < level:
                refs[item["doi"]] = ref
        elif item["zbl"]:
            refs[item["zbl"]] = ref
        elif item["arxiv"]:
            if item["arxiv"] not in refs or refs[item["arxiv"]]["priority"] < level:
                refs[item["arxiv"]] = ref
        elif item["semantic"] and (item["doi"] or item["arxiv"]):
            if not is_same_title(item[get_publication_title(item)], titles):
                refs[item["semantic"]] = ref

    sources = list({ref["provider"] for ref in refs.values()})
    sources = ", ".join(sorted(sources))
    citations_html = [citation["html"] for citation in refs.values()]
    citedby_for_stats = get_values_for_stats(refs)
    return citations_html, sources, citedby_for_stats


def citations_to_refs(provider, citations):
    if provider == CROSSREF:
        return citedby_crossref_refs(citations)
    elif provider == ZBMATH:
        return citedby_zbmath_refs(citations)
    elif provider == ADS:
        return citedby_ads_refs(citations)
    elif provider == SEMANTIC:
        return citedby_semantic_refs(citations)


def get_citations(resource):
    """Returns documents that cite this doi and sources used for the research."""
    data = {}
    authors = get_names(resource, "author")
    zbl_id = get_extid(resource, "zbl-item-id")
    preprint_id = get_extid(resource, "preprint")

    metadata = {
        "authors": authors,
        "doi": resource.doi,
        "preprint_id": preprint_id.id_value if preprint_id else "",
        "title": resource.title_tex,
    }

    if zbl_id and zbl_id.id_value:
        metadata.update({"zbl_id": zbl_id.id_value})

    with concurrent.futures.ThreadPoolExecutor(max_workers=4) as executor:
        future_to_provider = {
            executor.submit(citedby_crossref, metadata): CROSSREF,
            executor.submit(citedby_zbmath, metadata): ZBMATH,
            executor.submit(citedby_ads, metadata): ADS,
        }
        for future in concurrent.futures.as_completed(future_to_provider):
            provider = future_to_provider[future]
            try:
                if future.result():
                    data.update({provider: future.result()})
            except Timeout:
                continue
            except ConnectionError:
                continue

    citations = []
    for provider, cites in data.items():
        citations.extend(citations_to_refs(provider, cites))

    return built_citations(citations)
