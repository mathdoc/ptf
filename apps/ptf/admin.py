from django.contrib import admin
from django.contrib.auth.admin import GroupAdmin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Group
from django.contrib.auth.models import User

from ptf.models import Collection
from ptf.models import ExtLink
from ptf.models import Provider
from ptf.models import Publisher
from ptf.models import Resource


class PtfAdminSite(admin.AdminSite):
    site_header = "Administration"
    site_title = "Site admin"


class ExtLinkInline(admin.StackedInline):
    model = ExtLink


class SerialAdmin(admin.ModelAdmin):
    inlines = [ExtLinkInline]
    ordering = ["title_sort"]


class PublisherAdmin(admin.ModelAdmin):
    inlines = [ExtLinkInline]
    exclude = [
        "publishes",
        "provider",
        "published",
        "pid",
        "sid",
        "doi",
        "lang",
        "title_xml",
    ]
    ordering = ["pub_key"]


ptf_admin = PtfAdminSite(name="admin")
ptf_admin.register(User, UserAdmin)
ptf_admin.register(Group, GroupAdmin)
ptf_admin.register(Resource)
ptf_admin.register(Collection, SerialAdmin)
ptf_admin.register(Publisher, PublisherAdmin)
ptf_admin.register(ExtLink)
ptf_admin.register(Provider)
