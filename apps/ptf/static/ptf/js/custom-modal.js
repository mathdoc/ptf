function getPanelFromSwitch(modal, panelSwitch) {
    return modal.querySelector(`.custom-modal-panel[aria-labelledby="${panelSwitch.getAttribute("id")}"]`)
}


/**
 * Switch the modal's active panel to the given panel.
 * @param {*} panelSwitch   The panel switch element (class custom-modal-panel-switch)
 */
function switchModalPanel(panelSwitch) {
    let modal = panelSwitch?.closest(".custom-modal")
    if (!modal) {
        return
    }
    let panelSwitches = modal.getElementsByClassName("custom-modal-panel-switch")
    for (let panSwitch of panelSwitches) {
        panSwitch.classList.remove("active")
    }
    panelSwitch.classList.add("active")

    let panels = modal.getElementsByClassName("custom-modal-panel")
    for (let panel of panels) {
        panel.classList.remove("active")
    }
    let panel = getPanelFromSwitch(modal, panelSwitch)
    panel.classList.add("active")
}


function closeModal(modal) {
    if (!modal) {
        return
    }
    modal.classList.remove("active")
    modal.dispatchEvent(
        new CustomEvent(
            "onclose",
            {
                bubbles: false,
                target: modal
            }
        )
    )
}


document.addEventListener("DOMContentLoaded", () => {
    // Modal opening and init
    // All modal config data is passed via the clicked element
    Array.from(document.getElementsByClassName("custom-modal-opener")).forEach((btn) => {
        btn.addEventListener("click", () => {
            let modal = document.getElementById(btn.dataset.modalId)
            if (!modal || !modal.classList.contains("custom-modal")) {
                console.error(`No modal found with ID ${btn.dataset.modalId}.`)
                return
            }
            // Deactivate all active modals
            Array.from(document.querySelectorAll(".custom-modal.active")).forEach((el) => closeModal(el))

            // Activate selected modal
            let panelSwitch = modal.querySelector(`#${btn.dataset.modalPanel}`)
            if (panelSwitch) {
                switchModalPanel(panelSwitch)
            }
            modal.classList.add("active")

            modal.dispatchEvent(
                new CustomEvent(
                    "onopen",
                    {
                        bubbles: false,
                        target: modal,
                        detail: {
                            button: btn
                        }
                    })
            )
        })
    })

    // Close modal when user clicks outside of the modal content
    Array.from(document.getElementsByClassName("custom-modal")).forEach((modal) => {
        modal.addEventListener("click", (event) => {
            if (modal.classList.contains("active") && event.target == modal) {
                closeModal(modal)
            }
        })
    })

    // Close modal button
    Array.from(document.getElementsByClassName("custom-modal-close")).forEach((btn) => {
        btn.addEventListener("click", () => {
            let modal = btn.closest(".custom-modal")
            closeModal(modal)
        })
    })

    // Modal panel click
    Array.from(document.getElementsByClassName("custom-modal-panel-switch"))
        .forEach((panelSwitch) => {
            panelSwitch.addEventListener("click", () => {
                switchModalPanel(panelSwitch)
            })
        })
})
