$(document).ready(function () {
    $(".facet-header").on("click", function () {
        var more_facets = $(this).next().children(".more-facet");

        if (more_facets.css("display") == "none") {
            more_facets.show();
        } else {
            more_facets.hide();
        }

        $(this)
            .find(".facet-header-glyph")
            .each(function (index) {
                if ($(this).css("display") == "none") {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
    });

    // Expand or collapse a subfacet
    $(".facet-button").on("click", function () {
        var subfacets = $(this).parent().next();
        if (subfacets.css("display") == "none") {
            subfacets.show();
        } else {
            subfacets.hide();
        }

        $(this)
            .parent()
            .children(".facet-button")
            .each(function (index) {
                if ($(this).css("display") == "none") {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
    });
});
