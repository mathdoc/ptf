/** Contains the last valid anchor link found by the scrollspy */
var outlineLastAnchorLink = null;
/** Variables required to handle the outline auto-scroll when `scroll-behavior=smooth` */
var scrollData = {
    scrollTimeout: null,
    scrolling: false,
    elementToScroll: null,
    scrollToPerform: null
}

/**
 * Stores the requested anchor on initial navigation to the document.
 * We need that information because the anchor can be part of any async content not initially loaded.
 */
var requestedAnchor = null

/**
 * Scroll to the requested anchor name, if it exists in the document.
 * If `hard == true`, it will do a real navigation to the provided anchor.
 */
function scrollToRequestedAnchor(hard=false) {
    if (requestedAnchor == null) {
        return
    }
    let anchor = document.getElementById(requestedAnchor)
    if (anchor) {
        if (hard) {
            // We force a hard navigation to refresh the :target pseudo selector
            window.location.href = window.location.href.replace(/#.*$/, `#${requestedAnchor}`)
        }
        else {
            anchor.scrollIntoView()
        }
        requestedAnchor = null
    }
}

function resizeWindow() {
    var $window = $(window);

    // Get the size of the header, footer and window
    var header_height = $("#mersenne-header").outerHeight(); // Use outerHeight to get the padding height as well
    var footer_height = $("#footer").outerHeight();
    var header_plus_footer = header_height + footer_height;
    //var window_height = $window.height();  // no outerHeight for $(window)

    $("#page-container")
        .css("min-height", "100%")
        .css("min-height", "-=" + header_plus_footer + "px");
}

function fillOutline() {
    var ID = 0;

    var fullString = "";

    $("#article-body-html h2").each(function () {
        ID++;
        var elt_id = "body-html-" + ID;
        element = $(this);
        content = element.text();
        element.attr("id", elt_id);

        fullString += '<li><a class="submenu nav-link" href="#' + elt_id + '">' + content + "</a></li>";
    });

    element = $(".outline-fulltext");
    element.after(fullString);

    $('[data-bs-spy="scroll"]').each(function () {
        bootstrap.ScrollSpy.getOrCreateInstance(this).refresh();
    });
}

/**
 * Enable auto-scrolling of the outline. \
 * Patches some buggy behaviors related to bootstrap's scrollspy and scroll-behavior property set to smooth.
 */
function addOutlineListeners() {
    var outlineScrollspy = $('[data-bs-spy="scroll"][data-bs-target="#navbar-outline"]').first()

    if (outlineScrollspy) {
        // This listener is responsible of:
        // - Updating the fragment in the URL
        // - Scrolling the outline to the current activated link, or storing the necessary scroll in scrollData
        // - Correct a bootstrap bug where the outline is wrongly deactivated when scrolling through Metadata content
        window.addEventListener('activate.bs.scrollspy', function () {
            // Get the outline element (depending on screen size) and the active anchor link
            let outlinePanel = $(outlineScrollspy).find('.outline-div').first()
            let isSmallScreen = false
            let smOutlinePanel = $(outlineScrollspy).find('.outline-sm').first();
            if (outlinePanel.css('display') === 'none') {
                isSmallScreen = true
            }
            let activeAnchor = outlinePanel.find('a.active')
            let activeAnchorLink = null
            if (activeAnchor) {
                activeAnchorLink = activeAnchor.eq(0).attr('href')
            }

            // We can transfer the active class to the correct link inside smOutline
            if (activeAnchorLink) {
                smOutlinePanel.find('a').each(function() {
                    let listAnchor = $(this).attr('href')

                    $(this).removeClass('active');
                    if (listAnchor && listAnchor == activeAnchorLink) {
                        $(this).addClass('active')
                    }
                })
                return
            }

            // Update the URL with current anchor
            if (history.replaceState) {
                history.replaceState(null, null, activeAnchorLink)
            }
            else {
                location.hash = activeAnchorLink
            }
            outlineLastAnchorLink = activeAnchorLink

            // Scroll the outline panel if necessary
            let scrollableItem = null
            if (isSmallScreen) {
                scrollableItem = document.querySelector('.outline-sm  .dropdown-menu')
            }
            else {
                scrollableItem = document.querySelector('#outline-panel-body')
            }
            // This test checks whether the item is actually scrollable (if it has overflowing content)
            if (scrollableItem && scrollableItem.scrollHeight > scrollableItem.clientHeight) {
                let displayedSize = scrollableItem.offsetHeight
                let activeAnchorTop = activeAnchor.position().top
                let scroll = scrollableItem.scrollTop + activeAnchorTop - displayedSize / 2
                // Scroll right now if there's no other performing scroll
                if (!scrollData.scrolling) {
                    scrollableItem.scroll(0, scroll)
                    scrollData.elementToScroll = null
                    scrollData.scrollToPerform = null
                }
                // Store the scroll to be performed
                else {
                    scrollData.elementToScroll = scrollableItem
                    scrollData.scrollToPerform = scroll
                }
            }
        })

        // We add a listener to the click on a submenu link.
        // It's responsible for showing the article body if it's collapsed, then navigating after the uncollapsing is done.
        document.querySelectorAll('.outline-div li a.submenu, .outline-sm li a.submenu').forEach( function(submenu) {
            submenu.addEventListener('click', function (event) {
                var waitForUncollapse = false
                $('.open-on-outline-submenu').each(function () {
                    if ($(this).is(':hidden')){
                        waitForUncollapse = true
                        $(this).collapse('show')
                    }
                })
                if (waitForUncollapse){
                    var anchor = $($(this).attr('href')).get(0)
                    event.preventDefault()
                    setTimeout(function () {
                        anchor.scrollIntoView()
                    }, 170)
                }
            })
        })

        // This is required to enable scroll-behavior: smooth and the outline autoscroll.
        // When the document scroll is smooth (not instantaneous), it gets interrupted by any other scroll (for ex.
        // the outline autoscroll).
        // With this listener, we make a kind of "scrollend" event that will perform the outline scroll when all
        // the scrolling actions are finished
        document.addEventListener('scroll', function () {
            scrollData.scrolling = true
            if (scrollData.scrollTimeout) {
                clearTimeout(scrollData.scrollTimeout)
                scrollData.scrollTimeout = null
            }
            scrollData.scrollTimeout = setTimeout(function () {
                scrollData.scrolling = false
                clearTimeout(scrollData.scrollTimeout)
                if (scrollData.elementToScroll && scrollData.scrollToPerform) {
                    scrollData.elementToScroll.scroll(0, scrollData.scrollToPerform)
                }
                scrollData.scrollTimeout = null
            }, 100)
        })
    }
}

function citedby() {
    if ($("#citations-div").length === 0) {
        return;
    }

    var base_url = window.location.protocol + "//" + window.location.host;
    $.get(base_url + $("#citations-div").attr("data-url"), function (data, textStatus) {
        var list = data["result"]["citations"];
        if (list.length) {
            var citations = [];
            var len = list.length;
            for (var i = 0; i < len; i++) {
                citations.push("<li>" + list[i] + "</li>");
            }
            var citations_html = "<ul>" + citations.join("") + "</ul>";
        }

        if (len) {
            var doc = len === 1 ? "document" : "documents";
            $("#citations-div").html(citations_html);
            $("#citations-number").append(len + " " + doc + ".");
            $("#citations-sources").append(data["result"]["sources"]);
            $("#citations-html").parent().show();
            $("#citations-html").show();
            $('[id="citations-outline"]').show();
            requestMathjaxTypesetting(['#citations-div']);
            if (!$("#main-citations-li").is(":visible")) {
                $("#citations-html a").trigger("click");
            }
        }
    });
}

function relatedDiv() {
    // Placement des articles reliés par rapport au corps de l'article.
    var related = document.getElementById("recup_taille_div"); // id de la division "header"
    var divChange = document.getElementById("changeMargin"); // id de la division qui nécéssite ce changement
    var margin = window.getComputedStyle(related).marginBottom; // marge du bas
    var body = document.getElementById("body");

    if (related && divChange) {
        body.style.position = "static";
        var container = document.getElementsByClassName("page-container")[0];
        if (container) {
            var rect = container.getBoundingClientRect();
        }

        if (rect && body.getBoundingClientRect().width < 1500) {
            style =
                "position:relative; margin:0px; margin-top:20px; bottom:" +
                rect.bottom +
                "px; min-width:" +
                rect.width +
                "px;";
        } else {
            taille = related.clientHeight + parseInt(margin);
            style =
                "position:absolute; right:0; margin-top:" +
                taille.toString() +
                "px; margin-right:5px; overflow-x:hidden;";
        }
        divChange.style = style;
    }
}


/**
 * Request a typesetting task on the full document (default)
 * or on the provided elements (array of CSS selectors or HTML elements).
 */
function requestMathjaxTypesetting(elements=null) {
    if (window.mathjaxTypeset && typeof window.mathjaxTypeset === 'function') {
        window.mathjaxTypeset(elements)
    }
}

var ckeditorWaitTime = 0

/**
 * Contains CKEditor related code.
 *
 * This dynamically switches the CKEditor instances' toolbars according to the window width.
 * The switch is performed betwwen the toolbars named `toolbar_Full` and `toolbar_Basic`
 */
function ckeditorRelated() {
    // We potentially have to wait for ckeditor init script to load.
    // We call back this function until CKEditor is initialized or 3s have passed
    // (CKEditor might not be present in the document).
    if (!window.CKEDITOR) {
        if (ckeditorWaitTime > 5000) {
            return
        }
        setTimeout(ckeditorRelated, 100)
        ckeditorWaitTime += 100
        return
    }

    // The following code automatically switches the editor's toolbar between 'Full' and 'Basic'
    // according to the device width.
    var initInstancesChanged = []
    var editorToolbarWidthThreshold = 800
    var editorCurrentToolbar = 'Full'

    function replaceEditor(instanceName, toolbarCode) {
        let config = structuredClone(CKEDITOR.instances[instanceName].config)
        if (`toolbar_${toolbarCode}` in config) {
            config.toolbar = toolbarCode
            CKEDITOR.instances[instanceName].destroy()
            CKEDITOR.replace(instanceName, config)
        }
    }

    // Replace toolbar on init
    if (window.innerWidth < editorToolbarWidthThreshold) {
        editorCurrentToolbar = 'Basic'
        CKEDITOR.on('instanceReady', function (event) {
            let instanceName = event.editor.name
            if (!initInstancesChanged.includes(instanceName)) {
                replaceEditor(instanceName, 'Basic')
                initInstancesChanged.push(instanceName)
            }
        })
    }
    // Add listener for resize event
    addEventListener('resize', function () {
        if (this.innerWidth < editorToolbarWidthThreshold && editorCurrentToolbar == 'Full') {
            for (let instanceName in CKEDITOR.instances) {
                replaceEditor(instanceName, 'Basic')
            }
            editorCurrentToolbar = 'Basic'
        }
        if (this.innerWidth > editorToolbarWidthThreshold && editorCurrentToolbar == 'Basic') {
            for (let instanceName in CKEDITOR.instances) {
                replaceEditor(instanceName, 'Full')
            }
            editorCurrentToolbar = 'Full'
        }
    })
}


/**
 * Recursive replacement of all script nodes by fresh ones.
 * Doing this executes all replaced scripts.
 * This is required to execute scripts present in dynamically loaded HTML.
 */
function nodeScriptReplace(node) {
    if (node.tagName === 'SCRIPT') {
        node.parentNode.replaceChild(nodeScriptClone(node) , node)
    }
    else {
        var i = -1, children = node.childNodes
        while (++i < children.length) {
            nodeScriptReplace(children[i])
        }
    }
}

/**
 * Return a clone of a script node.
 */
function nodeScriptClone(node){
    var script = document.createElement("script")
    script.text = node.innerHTML

    var i = -1, attrs = node.attributes, attr
    while ( ++i < attrs.length ) {
        script.setAttribute((attr = attrs[i]).name, attr.value)
    }
    return script
}

var spinnerHtml = '<div class="loader-component"></div>'

/**
 * This makes AJAX requests for all additional HTML content to be loaded into the DOM.
 * These elements are marked in the Django template with the `.async-html` class.
 * It must contain the URL corresponding to the additional view (`data-url` attribute) and potential
 * javascript function name to be executed after the loading of the additional part (`data-js-callback` attribute).
 * TODO: make it recursive.
 */
function loadAsyncHtml() {
    Array.from(document.getElementsByClassName('async-html')).forEach((element) => {
        let viewUrl = element.dataset.url
        if (!viewUrl) {
            return
        }
        let initialTextAlign = element.style.textAlign
        // Eventually add a spinner while the content is loading
        if (element.dataset.spinner && element.dataset.spinner === 'true') {
            element.style.textAlign = 'center'
            element.innerHTML = spinnerHtml
        }
        let callbackFunction = element.dataset.jsCallback
        fetch(viewUrl)
            .then(response => {
                if (response.status >= 200 && response.status < 300) {
                    return response.text()
                }
                else {
                    // Keep the loading component if an error occurs
                    // element.style.textAlign = initialTextAlign
                    // element.innerHTML = ''
                    return Promise.reject(`Invalid HTTP response code: ${response.status}`)
                }
            })
            .then(plainHtml => {
                element.style.textAlign = initialTextAlign
                element.innerHTML = plainHtml
                // Execute all scripts in the incoming HTML
                nodeScriptReplace(element)
                // Execute callback
                if (callbackFunction && typeof window[callbackFunction] === 'function') {
                    window[callbackFunction]()
                }
            })
            .catch(message => console.error(`Error in loading additional HTML content:\n${message}`))
    })
}


$(document).ready(function() {

    loadAsyncHtml()

    try {
        var clipboard = new Clipboard(".copy-button");
    } catch (error) {
        console.error(error);
    }

    // we make sure checked property is here before submitting the form
    // input is not automatically checked because of css
    $("#language-switch-form").on("click", function () {
        $("#lang_onoffswitch").prop("checked", true);
        $(this).submit();
    });

    $("#sidebarCollapse").on("click", function () {
        $("#sidebar").toggle(300);
    });

    // Collapse accordion every time dropdown is shown
    $(".dropdown-accordion").on("show.bs.dropdown", function (event) {
        //$(this).children('.dropdown-menu').css({display:'block'});
        var accordion = $(this).find($(this).data("accordion"));
        accordion.find(".panel-collapse.in").collapse("hide");
    });

    // Prevent dropdown to be closed when we click on an accordion link
    $(".dropdown-accordion").on("click", 'a[data-bs-toggle="collapse"]', function (event) {
        event.preventDefault();
        event.stopPropagation();
        $($(this).data("parent")).find(".panel-collapse.in").collapse("hide");
        $($(this).attr("data-href")).collapse("show");
    });

    $(window).resize(resizeWindow);

    $("#journal-filter").on("input", function () {
        var text = $(this).val().toLowerCase();
        console.log(text);
        $(".journal-grid").hide();
        $(".journal-grid")
            .filter(function () {
                var data = $(this).attr("data-journal");
                var result = data.indexOf(text);
                return result > -1;
            })
            .show();
    });

    $("#journal-filter-remove").on("click", function () {
        $("#journal-filter").val("");
        $(".journal-grid").show();
    });

    $(".btn-letter").on("click", function () {
        $(".btn-letter").removeClass("active");
        $(this).addClass("active");
        var text = $(this).attr("data-letter");
        $(".journal-grid").hide();
        $(".journal-grid")
            .filter(function () {
                var letter = $(this).attr("data-letter");
                var result = letter == text || text == "all";
                return result;
            })
            .show();
    });

    fillOutline();

    // Now that the outline is filled, we can navigate to the provided anchor if any
    if (location.hash != '') {
        requestedAnchor = location.hash.substring(1)
        scrollToRequestedAnchor()
    }

    $('[data-bs-toggle="tooltip"]').tooltip({
        html: true,
    });

    var color = $(".affiliation-authors").css("color");
    $(".author_link")
        .mouseover(function () {
            $(this)
                .find("span")
                .each(function () {
                    $($(this).attr("href")).css({
                        color: "#3d6ac5",
                    });
                });
        })
        .mouseleave(function () {
            $(this)
                .find("span")
                .each(function () {
                    $($(this).attr("href")).css({
                        color: color,
                    });
                });
        });

    $("#house-affiliation").on("click", function () {
        $("#house-caret").toggleClass("fa-caret-up fa-caret-down");
    });

    $("#house-affiliation-a").on("click", function () {
        $("#house-caret").toggleClass("fa-caret-up fa-caret-down");
    });

    citedby();

    addOutlineListeners()

    Array.from(document.getElementsByClassName("click-stop-propagation")).forEach((el) => {
        el.addEventListener("click", (event) => {
            event.stopPropagation()
        })
    })
});
