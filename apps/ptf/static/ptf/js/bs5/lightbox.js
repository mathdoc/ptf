// Lightbox homemade script, don't forget to add the css!!!
//
// Paths:
// ptf/css/bs5/lightbox.css
// ptf/js/bs5/lightbox.js

class Lightbox {

  #ROOT_ID = "lightbox-root"
  #CONTAINER_ID = "lightbox-container";
  #IMAGE_DISPLAY_ID = "lightbox-img";
  #CLOSE_BUTTON_ID = "lightbox-close";
  #LOADER_ID = "lightbox-loader";
  #LOADER_ICON_ID = "lightbox-loader-icon";

  #SHOWED_CLASS = "showed";

  #OPENING_ANIMATION_DURATION = 500;
  #IMG_OPACITY_ANIMATION_DURATION = 250;

  #root;
  #container;
  #closeButton;
  #imageDisplay;

  #listenerController;

  constructor() {
    if (document.getElementById(this.#ROOT_ID)) {
      throw Error("The Lightbox object can only be initialized once");
    }

    this.#listenerController = new AbortController();

    this.#createRootElement();
    this.#initEvents();

    this.#appendToDOM();
  }

  showImage(lightboxElement) {
    this.#imageDisplay.classList.remove(this.#SHOWED_CLASS);
    this.#showOverlay();
    const href = lightboxElement.getAttribute("href");
    
    setTimeout(() => {
      this.#imageDisplay.src = href;
      this.#imageDisplay.classList.add(this.#SHOWED_CLASS);
    }, this.#IMG_OPACITY_ANIMATION_DURATION);
  }

  #appendToDOM() {
    const body = document.getElementsByTagName("body")[0];
    body.appendChild(this.#root);
  }

  #initEvents() {
    this.#container.addEventListener("click", (event) => {
      // What happens in the container stays in the container!!!
      // It blocks the root click event
      event.stopPropagation();
    });
  }

  #showOverlay() {
    this.#root.style.display = "flex";

    // Get the signal from the controller
    // Used to abort the listeners later on
    const { signal } = this.#listenerController;

    // Updating the background-color and the display property at the same time doesn't show the background-color transition for some reason
    // That's why we need to execute it on the next js "tick".
    setTimeout(() => this.#root.classList.add(this.#SHOWED_CLASS));
    setTimeout(() => {
      this.#root.addEventListener("click", (event) => this.#hideOverlay(), { signal });
      this.#closeButton.addEventListener("click", (event) => this.#hideOverlay(), { signal });
    }, this.#OPENING_ANIMATION_DURATION);
  }
  
  #hideOverlay() {
    // Remove listeners added before
    this.#listenerController.abort();
    // We need to have a new object to add the events back
    this.#listenerController = new AbortController();

    this.#root.classList.remove(this.#SHOWED_CLASS)
    // The CSS Transition takes 500ms
    setTimeout(() => this.#root.style.display = "none", this.#OPENING_ANIMATION_DURATION);
  }

  #createRootElement() {
    this.#root = document.createElement("div");
    this.#root.id = this.#ROOT_ID;
    this.#root.style.display = "none";
  
    // Container
  
    this.#container = document.createElement("div");
    this.#container.id = this.#CONTAINER_ID;
  
      // Image Display
  
      this.#imageDisplay = document.createElement("img");
      this.#imageDisplay.id = this.#IMAGE_DISPLAY_ID;
  
      this.#container.appendChild(this.#imageDisplay);
  
      // End Image Display
  
      // Close Button
  
      this.#closeButton = document.createElement("i");
      this.#closeButton.id = this.#CLOSE_BUTTON_ID;
      this.#closeButton.classList.add("fa", "fa-times");
  
      this.#container.appendChild(this.#closeButton);
  
      // End Close Button
  
      // Loader
  
      const loadingDiv = document.createElement("div");
      loadingDiv.id = this.#LOADER_ID;
  
        // Loader Icon
  
        const loadingIcon = document.createElement("i");
        loadingIcon.id = this.#LOADER_ICON_ID;
        loadingIcon.classList.add("fa", "fa-spinner");
  
        loadingDiv.appendChild(loadingIcon);
  
        // End Loader Icon
  
      this.#container.appendChild(loadingDiv);
  
      // End Loader
  
    this.#root.appendChild(this.#container);
  
    // End Container
  }
}

function initLightbox(cssSelector = ".lightbox") {
  if (!cssSelector) {
    throw new Error("The Lightbox CSS Selector is not valid");
  }

  const lightbox = new Lightbox();

  document.querySelectorAll(cssSelector).forEach(elem => {
    elem.addEventListener("click", (event) => {
      event.preventDefault();
      lightbox.showImage(elem);
    })
  });
}

initLightbox();