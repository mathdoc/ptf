// This is useful in case you want to use a Bootstrap 5 dropdown in a form
// Since the bootstrap-select library is not up to date, this is kind of a workaround
// 
// To use it, you can simply insert a bs5 dropdown in a form (It's supposed to be composed of a dropdown-toggle and an <ul>)
// Then you add an <input type="hidden"> between the dropdown-toggle and the <ul> with the parameter name you want.
// You also need to add the class "custom-select" to the <ul>
// Don't forget to add a value and action attribute on the dropdown items 

/* Example:

<!-- This could be a <a> or any variation that can trigger a dropdown -->
<button class="btn btn-primary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
    Value 1 <!-- You need to load the initial value yourself with Django Templates -->
</button>
<input type="hidden" name="parameter" value="value1">
<ul class="dropdown-menu custom-select" style="min-width: auto"> <!-- min-width is useful if you have small elements inside the dropdown -->
    <li>
        <a class="dropdown-item" href="#" value="value1" action="submit">Value 1</a>
    </li>
    <li>
        <a class="dropdown-item" href="#" value="value2" action="submit">Value 2</a>
    </li>
</ul>

*/
$(() => {
    $(".dropdown-menu.custom-select").on("click", ".dropdown-item", function () {
        // Here we go to the <ul> and get the <input type="hidden"> and set its value to that of the dropdown item
        const dropdownItems = $(this).closest(".custom-select");
        const hiddenInput = dropdownItems.prev("input[type=\"hidden\"]");
        hiddenInput.val($(this).attr("value"));

        // We copy what inside the dropdown-item inside the dropdown-toggle
        const dropdownToggle = hiddenInput.prev("[data-bs-toggle=\"dropdown\"]");
        dropdownToggle.html($(this).html());

        // If the action of the dropdown item is submit, then we submit the form.
        // Useful if you want to create a whole form that doesn't only contain the select
        if ($(this).attr("action") == "submit") {
            $(this).closest("form").trigger("submit");
        }
    });
})