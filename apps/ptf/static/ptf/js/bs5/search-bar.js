$(document).ready(function () {
    // At load time, if search input is not empty, display clear search button
    $(".searchinput").each(function () {
        if ($(this).val().length) {
            $(this).next().removeClass("hidden");
        }
    });

    // Display clear search button if search input is not empty
    $(".searchinput").on("input", function () {
        if ($(this).val().length) {
            $(this).next().removeClass("hidden");
        } else {
            $(this).next().addClass("hidden");
        }
    });

    // Clear search input fields when you click on the 'x'
    // Use the .on('click', 'selector'...) syntax so that DOM added dynamically
    // with the jquery .append() function can be selected
    $("#search-bar").on("click", ".searchclear", function () {
        $(this).prev().val("");
        $(this).addClass("hidden");
    });

    // Store the new value of the "select" dropdown button inside a hidden input
    // Use the .on('click', 'selector'...) syntax so that DOM added dynamically
    // with the jquery .append() function can be selected
    $("#search-bar").on("click", ".dropdown-menu a", function () {
        // New value
        var selection = $(this).html();

        // Change the button text
        $(this)
            .parent()
            .parent()
            .prev()
            .html(selection);

        var value = "";
        if (selection == "Tout" || selection == "All") {
            value = "all";
        } else if (selection == "Titre" || selection == "Title") {
            value = "title";
        } else if (selection == "Auteur" || selection == "Author") {
            value = "author";
        } else if (selection == "Date") {
            value = "date";
        } else if (selection == "Résumé" || selection == "Abstract") {
            value = "abstract";
        } else if (selection == "Plein texte" || selection == "Full text") {
            value = "body";
        } else if (selection == "Bibliographie" || selection == "References") {
            value = "references";
        } else if (selection == "Mots clés" || selection == "Keywords") {
            value = "kwd";
        }

        var ancestor = $(this).parent().parent().parent().parent();

        if (value == "date") {
            ancestor.children().slice(0, 1).hide();
            ancestor.children().slice(1, 2).show();
        } else {
            ancestor.children().slice(0, 1).show();
            ancestor.children().slice(1, 2).hide();
        }

        // Update the hidden input
        $(this).parent().parent().prev().prev().val(value);

        var fr_is_checked = $("#lang_onoffswitch").is(":checked");
        //var text = '';

        //if ( value == 'all') {
        //    text = fr_is_checked ? 'Rechercher partout' : 'Search everywhere';
        //}

        //ancestor.find('.searchinput').attr('placeholder', text);
    });

    // Add a new line to the search form
    $("#add-search-field").click(function () {
        var i = $("#search-bar form .search-group").length;

        // A prototype was added by the Django template
        // Clone it to create the new line
        html_text = $("#template-search-form").html();
        html_text = html_text.replace(/\#\#i\#\#/g, i);

        var the_parent = $("#line-container");
        the_parent.append(html_text);
        the_parent.find(".searchinput").last().focus();
        the_parent.find(".searchinput").last().on("input", function () {
            if ($(this).val().length) {
                $(this).next().removeClass("hidden");
            } else {
                $(this).next().addClass("hidden");
            }
        });
    });

    $("#search-bar").on("click", ".remove-search-field", function () {
        // reset the hidden input just in case
        //$(this).parent().children(':first-child').val('');

        var div_to_remove = $(this).parent().parent();
        var the_parent = div_to_remove.parent();

        div_to_remove.remove();
        the_parent.find(".searchinput").last().focus();
    });

    $(".letter").on("click", function () {
        var selection = $(this).html();

        $(".letter").removeClass("active");
        $(this).addClass("active");

        $(".letter-div").hide();
        $(".letter-" + selection).show();
    });
});
