$(document).ready(function () {
    var clipboard = new Clipboard(".copy-button");

    // Expand or collapse a facet
    $(".expand-facet").on("click", function () {
        // Show or hide expand/collapse button
        var expand_button = $(this).find(".hiddenable");
        var collapse_button = $(this).find(".hidden");
        expand_button.removeClass("hiddenable").addClass("hidden");
        collapse_button.removeClass("hidden").addClass("hiddenable");

        // Expand or collapse facets
        var hiddenable = $(this).closest(".facet-header").next().children(".hiddenable");
        var hidden = $(this).closest(".facet-header").next().children(".hidden");
        hidden.removeClass("hidden").addClass("hiddenable");
        hiddenable.removeClass("hiddenable").addClass("hidden");
    });

    // Expand or collapse a subfacet
    $(".expand-subfacet").on("click", function () {
        // Show or hide expand/collapse button
        var expand_button = $(this).children(".hiddenable");
        var collapse_button = $(this).children(".hidden");
        expand_button.removeClass("hiddenable").addClass("hidden");
        collapse_button.removeClass("hidden").addClass("hiddenable");

        // Expand or collapse facets
        var hiddenable = $(this).parent().next().find(".subfacet.hiddenable");
        var hidden = $(this).parent().next().find(".subfacet.hidden");
        hidden.removeClass("hidden").addClass("hiddenable");
        hiddenable.removeClass("hiddenable").addClass("hidden");
    });

    /*
    // Expand Subfacet
    $('#expand-subfacet').on('click', function() {
        $(this).removeClass('hiddenable').addClass('hidden')
        $(this).next().removeClass('hidden').addClass('hiddenable')
        $(this).parent().children('a.hidden').removeClass('hidden').addClass('hiddenable')
    })

    // Collapse Subfacet
    $('#collapse-subfacet').on('click', function() {
        $(this).removeClass('hiddenable').addClass('hidden')
        $(this).prev().removeClass('hidden').addClass('hiddenable')
        $(this).parent().children('a.hiddenable').removeClass('hiddenable').addClass('hidden')
    })*/
});
