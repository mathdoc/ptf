/**
 * Custom tooltips.
 *
 * Creates a tooltip on hover & click for any HTML element with attribute `data-tooltip`.
 * Config:
 *  -   `data-tooltip`              The value (HTML enabled) to display in the tooltip.
 *  -   `data-tooltip-position`     How to position the tooltip (left, right, top, bottom).
 *                                  The tooltip position might be adapted if the tooltip doesn't fit in the original
 *                                  position.
 *  -   `data-tooltip-html`         If `true`, render the tooltip content as HTML.
 *  -   `data-tooltip-class`        Additionnal classes to add to the generated tooltip instance.
 */

// Variables in px
/**
 * Min distance to position the tooltip rect from the target rect and the viewport edges.
 */
const tooltipMinDistance = 5
/**
 * Min width for `left` and `right` tooltips.
 * Used to shrink the tooltip's width if it doesn't fit the available space.
 * If the available space is less than this value, changes the tooltip position.
 */
const sideTooltipMinWidth = 100

/**
 * Request a typesetting operation to MathJax plugin.
 * Requires `mathjax-config.js` sript
 * @param {Array} elements
 */
function requestMathjaxTypesetting(elements=null) {
    if (window.mathjaxTypeset && typeof window.mathjaxTypeset == "function") {
        window.mathjaxTypeset(elements)
    }
}


/**
 * Creates a tooltip element with the given HTML value and position it wrt. the given
 * element.
 *
 * The tooltip should be `position:fixed` by the CSS.
 * The tooltip coordinates are explicitely computed from the target element.
 * NOTE: The tooltip is added to the document root, and is not hoverable nor selectable.
 *
 * @param {Element} element         The element for which the tooltip is generated.
 * @param {string} tooltipValue     The tooltip HTML value (can be just a string).
 */
function createTooltip(element, tooltipValue) {
    // Destroy all existing tooltips - Prevent more than 1 tooltip at the same time.
    destroyTooltip()
    document.removeEventListener("click", destroyTooltip)

    // Create tooltip HTML element
    let tooltip = document.createElement("div")
    tooltip.classList.add("custom-tooltip")
    // By default, the tooltip value is rendered as HTML except if `data-tooltip-html` is false
    const htmlTooltip = element.dataset.tooltipHtml === "true"
    if (htmlTooltip) {
        tooltip.innerHTML = tooltipValue
    }
    else {
        tooltip.appendChild(document.createTextNode(tooltipValue))
    }
    const tooltipId = `tooltip-${new Date().getTime()}`
    tooltip.setAttribute("id", tooltipId)
    // Add potential extra class to the tooltip
    let tooltipClass = element.dataset.tooltipClass?.split(" ")
    if (tooltipClass) {
        tooltip.classList.add(...tooltipClass)
    }

    // Add the tooltip to the DOM
    element.classList.add("custom-tooltip-active")
    element.setAttribute("aria-describedby", tooltipId)
    document.getElementsByTagName("body")[0].appendChild(tooltip)

    // Position the tooltip according to the parent element position and the viewport dimensions
    let clientWidth = document.documentElement.clientWidth
    let targetRect = element.getBoundingClientRect()
    let tooltipRect = tooltip.getBoundingClientRect()
    let tooltipPosition = element.dataset.tooltipPosition

    const centerHorizontally = (tooltip, tooltipRect, targetRect, clientWidth, minDistance) => {
        // Just center the tooltip wrt. the parent element
        // We clip the desired value to the left and right value of the viewport to prevent overflow
        let minLeft = minDistance
        let maxLeft = clientWidth - tooltipRect.width - minDistance
        // Desired position for perfect centering
        let left = (targetRect.left + targetRect.right - tooltipRect.width) / 2
        left = left < minLeft ? minLeft : left
        left = left > maxLeft ? maxLeft : left
        tooltip.style.left = `${left}px`
    }
    const centerVertically = (tooltip, tooltipRect, targetRect, minDistance) => {
        let minTop = minDistance
        let top = (targetRect.top + targetRect.bottom - tooltipRect.height) / 2
        top = top < minTop ? minTop : top
        tooltip.style.top = `${top}px`
    }

    switch (tooltipPosition) {
        case "left":
            let targetLeftA = targetRect.left - tooltipRect.width - tooltipMinDistance
            let availableSpaceA = targetRect.left - 2 * tooltipMinDistance
            if (availableSpaceA >= tooltipRect.width) {
                tooltip.style.left = `${targetLeftA}px`
                centerVertically(tooltip, tooltipRect, targetRect, tooltipMinDistance)
                break
            }
            else if (availableSpaceA >= sideTooltipMinWidth) {
                tooltip.style.width = `${availableSpaceA}px`
                tooltipRect = tooltip.getBoundingClientRect()
                targetLeftA = targetRect.left - tooltipRect.width - tooltipMinDistance
                tooltip.style.left = `${targetLeftA}px`
                centerVertically(tooltip, tooltipRect, targetRect, tooltipMinDistance)
                break
            }
            // Else move the tooltip into a `right` tooltip
        case "right":
            // Check if there's enough space to fit the tooltip after the target element.
            let targetLeftB = targetRect.right + tooltipMinDistance
            let availableSpaceB = clientWidth - targetRect.right - 2 * tooltipMinDistance
            if (availableSpaceB >= tooltipRect.width) {
                tooltip.style.left = `${targetLeftB}px`
                centerVertically(tooltip, tooltipRect, targetRect, tooltipMinDistance)
                break
            }
            // Check if we can resize the tooltip to fit the available space
            else if (availableSpaceB >= sideTooltipMinWidth) {
                tooltip.style.width = `${availableSpaceB}px`
                tooltip.style.left = `${targetLeftB}px`
                tooltipRect = tooltip.getBoundingClientRect()
                centerVertically(tooltip, tooltipRect, targetRect, tooltipMinDistance)
                break
            }
            // Else move the tooltip to a `top` tooltip
        case "top":
            // Check if there's enough space to fit the tooltip above the target element.
            // Otherwise transorm it into a bottom tooltip.
            let targetTop = targetRect.top - tooltipRect.height - tooltipMinDistance
            // Ok (enough space)
            if (targetTop > tooltipMinDistance) {
                tooltip.style.top = `${targetTop}px`
                centerHorizontally(tooltip, tooltipRect, targetRect, clientWidth, tooltipMinDistance)
                break
            }
            // Else move the tooltip to a `bottom` tooltip
        // Bottom
        default:
            tooltip.style.top = `${targetRect.bottom + tooltipMinDistance}px`
            centerHorizontally(tooltip, tooltipRect, targetRect, clientWidth, tooltipMinDistance)
            break
    }

    if (htmlTooltip) {
        requestMathjaxTypesetting([`#${tooltipId}`])
    }

    setTimeout(() => document.addEventListener("click", destroyTooltip), 300)
}

/**
 * Destroy all tooltips referenced by the given elements.
 * If the array is empty, we remove all tooltips.
 *
 * @param {Array} elements  The elements whose tooltip must be destroyed
 */
function destroyTooltip(event=null, elements=[]) {
    if (elements.length == 0) {
        elements = Array.from(document.getElementsByClassName("custom-tooltip-active"))
    }
    elements.forEach((element) => {
        element.classList.remove("custom-tooltip-active")
        let tooltipId = element.getAttribute("aria-describedby")
        element.removeAttribute("aria-describedby")
        let tooltip = document.getElementById(tooltipId)
        tooltip?.parentNode.removeChild(tooltip)
    })
    document.removeEventListener("click", destroyTooltip)
}

/**
 * Creates or destroys the "custom-tooltip" element referenced by the given element.
 * @param {Element} element
 */
function toggleTooltip(element) {
    let tooltipValue = element?.dataset.tooltip
    if (!tooltipValue) {
        return
    }

    // Destroy existing tooltip
    if (element.classList.contains("custom-tooltip-active")) {
        destroyTooltip(null, [element])
        return
    }

    // Create and display tooltip
    createTooltip(element, tooltipValue)
}


/**
 * Initialize custom tooltips.
 * An element will have an auto-generated tooltip on user action if it has the `data-tooltip` attribute.
 *
 * Tooltips are handled with mouseenter / mouseleave events (ie. hover) and clicks
 * for devices without pointer.
 */
function setTooltipListeners() {
    Array.from(document.querySelectorAll("*[data-tooltip]")).forEach((element) => {
        element.style.position = "relative"
        let tooltipValue = element.dataset.tooltip
        if (!tooltipValue) {
            return
        }
        element.addEventListener("click", (event) => {
            toggleTooltip(element)
        })
        element.addEventListener("mouseenter", (event) => {
            createTooltip(element, tooltipValue)
        })
        element.addEventListener("mouseleave", (event) => {
            destroyTooltip(event, [element])
        })
    })
}


document.addEventListener("DOMContentLoaded", () => {
    setTooltipListeners()
})
