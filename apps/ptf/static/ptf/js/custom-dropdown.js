var clickLikeKeys = [" ", "Spacebar", "Enter"]

/**
 * Return whether the given UIEvent should be consider a "click-like" action.
 * For example: space and enter keydown are considered click-like actions.
 *
 * @param {UIEvent} event
 * @returns {boolean}
 */
function isClickLikeAction(event) {
    if (event.type === "click") {
        return true
    }
    if (event.type === "keydown") {
        return clickLikeKeys.includes(event.key)
    }
    return false
}


/**
 * Closes all opened custom-dropdown, except if the event is performed on a custom-dropdown-content.
 * @param {UIEvent} event Click/keydown event
 */
function dropdownCloseAll(event) {
    // Only accept click-like and Escape keydown events if present
    if (
        event
        && !(isClickLikeAction(event) || (event.type === "keydown" && event.key === "Escape"))
    ) {
        return
    }
    let removeListener = true
    Array.from(document.getElementsByClassName("custom-dropdown-open")).forEach((dropdown) => {
        toggle = dropdown.querySelector(".custom-dropdown-toggle")
        let target = toggle.id
        let dropdownContent = document.querySelector(`.custom-dropdown-content[aria-labelledby='${target}']`)
        if (event && isClickLikeAction(event) && event.target) {
            let targetDropdown = event.target.closest(".custom-dropdown-content")
            if (targetDropdown == dropdownContent) {
                removeListener = false
                return
            }
        }
        dropdownContent.style.display = "none"
        dropdown.classList.remove("custom-dropdown-open")
    })

    if (removeListener) {
        document.removeEventListener("click", dropdownCloseAll)
        document.removeEventListener("keydown", dropdownCloseAll)
    }
}


/**
 * Handler for actions on our custom dropdown component.
 * @param {UIEvent} event
 */
function dropdownActionHandler(event) {
    if (!isClickLikeAction(event)) {
        return
    }
    let dropdownToggle = event.currentTarget
    if (!dropdownToggle.parentElement.classList.contains("custom-dropdown-open")) {
        let target = dropdownToggle.id
        dropdownCloseAll()
        dropdownToggle.parentElement.classList.add("custom-dropdown-open")
        let content = document.querySelector(`.custom-dropdown-content[aria-labelledby='${target}']`)
        content.style.display = "block"

        // We use this timeout to add the event listener after the current click event effects are finished.
        // Otherwise it will be called directly after this
        document.removeEventListener("click", dropdownCloseAll)
        document.removeEventListener("keydown", dropdownCloseAll)

        setTimeout(() => document.addEventListener("click", dropdownCloseAll), 500)
        setTimeout(() => document.addEventListener("keydown", dropdownCloseAll), 500)
    }
}


/**
 * The init of custom dropdowns is set as a window property so
 * it can be called by other scripts.
 */
window.setCustomDropdowns = () => {
    Array.from(document.getElementsByClassName("custom-dropdown-toggle")).forEach((element) => {
        element.removeEventListener("click", dropdownActionHandler)
        element.removeEventListener("keydown", dropdownActionHandler)
        element.addEventListener("click" , dropdownActionHandler)
        element.addEventListener("keydown" , dropdownActionHandler)
    })
}


document.addEventListener("DOMContentLoaded", () => {
    // Show / Hide custom-dropdown content
    window.setCustomDropdowns()
})
