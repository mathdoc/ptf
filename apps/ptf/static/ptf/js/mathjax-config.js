function replaceMml() {
    console.log("Replacing mml");
    $(".mathjax-formula").each(function () {
        var formula = $(this);
        var tex = formula.data("tex");
        if (tex != "") {
            formula.html(tex);
        }
    });
}

window.MathJax = {
    loader: {
        load: ["[tex]/upgreek", "[tex]/textmacros", "[tex]/noerrors", "[tex]/physics"],
    },
    tex: {
        inlineMath: [
            ["$", "$"],
            ["\\(", "\\)"],
        ],
        packages: {
            "[+]": ["upgreek", "textmacros", "noerrors", "physics"],
        },
        processEscapes: true, // use \$ to produce a literal dollar sign
        processEnvironments: true,
        macros: {
            CC: "\\mathbb{C}",
            PP: "\\mathbb{P}",
            QQ: "\\mathbb{Q}",
            RR: "\\mathbb{R}",
            SS: "\\mathbb{S}",
            ZZ: "\\mathbb{Z}",
            C: "\\mathbb{C}",
            P: "\\mathbb{P}",
            Q: "\\mathbb{Q}",
            R: "\\mathbb{R}",
            S: "\\mathbb{S}",
            Z: "\\mathbb{Z}",
            GL: "\\mathrm{GL}",
            smallF: "\\mathbb{F}",
            sp: "^",
            sb: "_",
            bb: "\\mathbb ",
            dbar: "\\overline\\partial",
            protect: "",
        },
        noundefined: {
            color: "red",
            background: "",
            size: "",
        },
    },
    options: {
        ignoreHtmlClass: "article-div", //  class that marks tags not to search
        processHtmlClass: "mathjax-formula", //  class that marks tags that should be searched
        enableMenu: true,
        a11y: {
            speech: true, // switch on speech output
            braille: true, // switch on Braille output
            subtitles: true, // show speech as a subtitle
            viewBraille: true, // display Braille output as subtitles
        },
    },
    startup: {
        pageReady: function () {
            console.log("MathJax pageReady");
            //replaceMml();
            //
            //  Do the usual startup (which does a typeset).
            //  When that is all done, un-hide the page.
            //
            return MathJax.startup.defaultPageReady().then(function () {
                console.log("MathJax startup processed");
            });
        },
    },
};


/**
 * This function "queues" an async typesetting task.
 * Each typesetting task added by this function is performed after all the previous typesetting tasks are finished by
 * using simple chaining promises.
 * The first one being the `MatJax.startup` typesetting.
 * Source https://docs.mathjax.org/en/latest/web/typeset.html#typeset-async
 */
window.mathjaxTypeset = (elements=null) => {
    MathJax.startup.promise = MathJax.startup.promise
        .then(() => {
            console.log('Mathjax additional typesetting')
            if (elements) {
                MathJax.typesetPromise(elements)
            }
            else {
                MathJax.typesetPromise()
            }
        })
}
