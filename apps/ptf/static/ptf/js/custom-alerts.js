document.addEventListener("DOMContentLoaded", () => {
    // Handle messages
    Array.from(document.querySelectorAll(".custom-alert-messages button")).forEach((button) => {
        // Remove corresponding alert from DOM
        button.addEventListener("click", () => {
            button.parentElement.remove()
        })
    })
    // Auto remove of alerts after 12s
    setTimeout(
        () => {
            let alertMessages = Array.from(document.getElementsByClassName("custom-alert-messages"))
            alertMessages.forEach((el) => el.classList.add("fade-out"))
            setTimeout(() => alertMessages.forEach((alert) => alert.remove()), 3000)
        },
        12000
    )
})
