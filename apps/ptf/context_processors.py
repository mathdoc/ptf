from django.conf import settings

from .models import Collection


def ptf(request):
    pid = settings.COLLECTION_PID
    try:
        collection = Collection.objects.get(pid=pid)
    except Collection.DoesNotExist:
        collection = ""
    return {
        "COLLECTION_PID": pid,
        "SITE_DOMAIN": settings.SITE_DOMAIN,
        "SITE_NAME": settings.SITE_NAME,
        "VOLUME_STRING": settings.VOLUME_STRING,
        "SHOW_DJVU": getattr(settings, "SHOW_DJVU", False),
        "SHOW_BODY": getattr(settings, "SHOW_BODY", False),
        "COLLECTION_TITLE": collection.title_tex
        if collection
        else settings.SITE_NAME.capitalize(),
        "ISSN": collection.issn if collection else "",
        "EISSN": collection.e_issn if collection else "",
        "USE_FLEX": settings.USE_FLEX if hasattr(settings, "USE_FLEX") else True,
        "ALLOW_TRANSLATION": settings.ALLOW_TRANSLATION
        if hasattr(settings, "ALLOW_TRANSLATION")
        else False,
        "COMMENTS_VIEWS_ARTICLE_COMMENTS": getattr(
            settings, "COMMENTS_VIEWS_ARTICLE_COMMENTS", False
        ),
        "COMMENTS_VIEWS_POLICY_LINK": getattr(settings, "COMMENTS_VIEWS_POLICY_LINK", ""),
    }
