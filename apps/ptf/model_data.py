##################################################################################################
#
# README
#
# pivot classes for PTF objects (articles, issues,...).
# They only have data members and are independent of XML, relational database and search engine.
# They can be used directly by cmds
# Parsers (JATS), Comparators,... inherit from these classes and add functions (parse_tree, compare)
#
##################################################################################################


# Simple object for compatibility reason:
# - in xml_cmds/add_relations
# - in xml_cmds/find_collection (needs an incollection.collection.pid)

from typing import TYPE_CHECKING
from typing import Literal
from typing import NotRequired
from typing import TypedDict
from typing import Unpack

if TYPE_CHECKING:
    from ptf.models import Collection


class Foo:
    pass


class AbstractDict(TypedDict):  # Inferred from usage
    tag: Literal["abstract", "biblio", "avertissement", "note", "description", "intro", "toc"]
    value_html: NotRequired[str]
    value_xml: NotRequired[str]
    value_tex: str
    lang: str


class ResourceData:
    def __init__(self, *args, **kwargs):
        super().__init__()

        self.lang = "und"

        self.pid: str | None = None
        self.doi: str | None = None

        self.url: str | None = None

        self.title_xml = ""
        self.title_tex = ""
        self.title_html = ""
        self.trans_lang = "und"
        self.trans_title_html = ""
        self.trans_title_tex = ""
        self.trans_title_xml = ""

        # Common to articles, books, book-parts
        self.abstracts: list[AbstractDict] = []
        self.bibitems: list[RefData] = []
        # TODO: Remove bibitem. This is used for solrCmds. solrCmds should use bibitems instead.
        self.bibitem = []
        self.awards = []
        self.relations = []

        self.ids = []  # Other id of the resource. Create ResourceId in the DB
        # id given by an external source (MR, ZBL...). Create an ExtId in the DB
        self.extids: list[tuple[str, str]] = []

        self.ext_links: list[
            ExtLinkDict
        ] = (
            []
        )  # <ext-link> can contain MR/ZBL... In this case, ExtLink are not created, only ExtId
        self.streams = []
        self.related_objects = []

        self.counts = []

        self.contributors: list[ContributorDict] = []
        self.kwds: list[SubjDict] = []
        self.kwd_groups = []
        self.subjs: list[str] = []
        self.subj_groups = []

        self.figures = []
        self.supplementary_materials = []

        self.funding_statement_html = ""
        self.funding_statement_xml = ""
        self.footnotes_html = ""
        self.footnotes_xml = ""

        self.body_html = ""
        self.body_tex = ""
        self.body_xml = ""
        self.body = ""


# Found in col.xml
# It is the main way to create a collection in PTF
# The upload urls will end up create MathdocPublicationData
class MathdocPublicationData(ResourceData):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.coltype: str | None = None
        self.pid: str | None = None
        self.issn = None
        self.e_issn = None
        self.wall = 0
        self.provider = None
        self.abbrev = ""


class PublisherData:
    def __init__(self, *args, **kwargs):
        super().__init__()
        self.name: str | None = None
        self.loc = ""
        # Note: add_publisher in xml_cmds tries to create ExtLinks for Publishers
        # But this is not possible in JATS:
        #   <publisher> only has <publisher-name> and <publisher-location>
        # TODO: remove ext_links ?
        self.ext_links = []


# JournalData typically comes from a <journal-meta> inside a <journal-issue>
# It is not the main way to create a Journal, but can be used to create a collection on the fly.
# In this case, it will get some attributes from its parent collection (ex: coltype)
class JournalData(ResourceData):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.coltype = None
        self.publisher = None
        self.provider = None
        self.issn = None
        self.e_issn = None
        self.wall = 0
        self.abbrev = ""


class IssueData(ResourceData):
    merged_year: str | None
    year: str | None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.journal: Collection | None = None
        self.publisher: PublisherData | None = None
        self.provider = None
        self.ctype = "issue"
        self.year = ""
        self.vseries = ""
        self.volume = ""
        self.number = ""
        self.last_modified_iso_8601_date_str: str | None = None
        self.prod_deployed_date_iso_8601_date_str: str | None = None
        self.articles: list[ArticleData] = []
        self.with_online_first = False
        self.seq = 0

    def __iter__(self):
        yield from self.articles


class ArticleData(ResourceData):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.atype = ""

        self.seq = 0
        self.fpage = self.lpage = self.page_range = self.size = ""
        self.page_type = ""

        self.article_number = ""
        self.talk_number = ""

        self.elocation = ""
        self.history_dates = []
        self.prod_deployed_date_iso_8601_date_str: str | None = None
        self.date_published_iso_8601_date_str: str | None = None

        self.pid = None

        self.coi_statement = ""  # Conflict of interest

        # list of ArticleData, translation of the article by others
        self.translations: list[ArticleData] = []


class RefData(ResourceData):
    # TODO: remove lang ? It is not used by Bibitem.

    def __init__(self, *args, lang, **kwargs):
        super().__init__(*args, **kwargs)

        self.lang = lang
        self.user_id = ""
        self.label = ""
        self.label_prefix = self.label_suffix = ""
        self.citation_xml = ""
        self.citation_html: str | None = None
        self.citation_tex: str | None = None
        self.type = "misc"
        self.publisher_name = ""
        self.publisher_loc = ""
        self.institution = ""
        self.series = ""
        self.volume = ""
        self.issue = ""
        self.month = ""
        self.year = ""
        self.comment = ""
        self.annotation = ""
        self.fpage = ""
        self.lpage = ""
        self.page_range = ""
        self.size = ""
        self.source_tex = ""
        self.article_title_tex = ""
        self.chapter_title_tex = ""


# Incollection found in books
# Mainly used to find the book number in its collection
class CollectionData(ResourceData):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.coltype = None
        self.issn = None
        self.e_issn = None
        self.volume = ""
        self.vseries = ""
        self.seq = 0


class BookData(ResourceData):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        book_type = "Book"
        self.ctype = "book-" + book_type
        self.frontmatter_xml = None
        self.frontmatter_toc_html = None
        self.frontmatter_foreword_html = None
        self.incollection = []
        self.publisher = None
        self.provider = None
        self.parts = []
        self.body = ""
        self.seq = 0

        self.last_modified_iso_8601_date_str = None
        self.prod_deployed_date_iso_8601_date_str = None


class BookPartData(ArticleData):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.atype = ""
        self.fpage = self.lpage = self.page_range = self.page_type = ""
        self.frontmatter_xml = None
        self.frontmatter_toc_html = None
        self.frontmatter_foreword_html = None
        self.parts = []
        self.body = None


def create_refdata(lang="und", doi=None):
    data = RefData(lang=lang)
    data.type = "unknown"
    data.citation_html = ""
    data.citation_tex = ""
    data.citation_xml = '<label></label><mixed-citation xml:space="preserve"></mixed_citation>'

    if doi is not None:
        data.doi = doi
        data.extids.append(("doi", doi))

    return data


def create_articledata(doi: str | None = None):
    data = ArticleData(doi=doi)
    return data


def create_issuedata():
    data = IssueData()
    return data


def create_bookdata():
    data = BookData()
    return data


def create_publicationdata():
    data = MathdocPublicationData()
    return data


def create_collectiondata():
    data = CollectionData()
    return data


def create_publisherdata():
    data = PublisherData()
    return data


class ContributorDict(TypedDict, total=False):  # Inferred from usage
    orcid: str
    idref: str
    mid: str
    first_name: str
    last_name: str
    prefix: str
    suffix: str
    email: str
    string_name: str
    addresses: list[str]
    address_text: str
    role: Literal["author", "editor", ""]
    deceased_before_publication: bool
    equal_contrib: bool
    contrib_xml: str
    corresponding: bool
    seq: int


def create_contributor(**kwargs: Unpack[ContributorDict]) -> ContributorDict:
    default: ContributorDict = {
        "orcid": "",
        "idref": "",
        "mid": "",
        "first_name": "",
        "last_name": "",
        "prefix": "",
        "suffix": "",
        "email": "",
        "string_name": "",
        "addresses": [],
        "address_text": "",
        "role": "",
        "deceased_before_publication": False,
        "equal_contrib": False,
        "contrib_xml": '<contrib content-type="author"><name><surname></surname><given-names></given-names></name></contrib>',
        "corresponding": False,
        "seq": 0,
    }
    return default | kwargs


class SubjDict(TypedDict, total=False):  # Inferred from usage
    lang: str
    type: str
    value: str


def create_subj(**kwargs: Unpack[SubjDict]) -> SubjDict:
    defaults: SubjDict = {
        "lang": "",
        "type": "",
        "value": "",
    }
    return defaults | kwargs


class ExtLinkDict(TypedDict, total=False):  # Inferred from usage
    rel: str
    mimetype: str
    location: str
    base: str
    metadata: str


def create_extlink(**kwargs: Unpack[ExtLinkDict]) -> ExtLinkDict:
    defaults: ExtLinkDict = {"rel": "", "mimetype": "", "location": "", "base": "", "metadata": ""}
    return defaults | kwargs


class DataStreamDict(TypedDict, total=False):  # Inferred from usage
    rel: str
    mimetype: str
    location: str
    base: str
    text: str


def create_datastream(**kwargs: Unpack[DataStreamDict]) -> DataStreamDict:
    defaults: DataStreamDict = {"rel": "", "mimetype": "", "location": "", "base": "", "text": ""}
    return defaults | kwargs


def get_extlink(resource: ResourceData, rel):
    if resource is None:
        return None

    results = [
        extlink for extlink in resource.ext_links if "rel" in extlink and extlink["rel"] == rel
    ]
    result = results[0] if len(results) > 0 else None

    return result
