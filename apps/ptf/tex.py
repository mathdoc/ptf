import os
import subprocess
import tempfile

import pypdf
from pylatexenc.latexencode import unicode_to_latex

from django.conf import settings

from ptf import model_helpers
from ptf import models
from ptf import utils
from ptf.cmds.xml import xml_utils
from ptf.cmds.xml.jats.jats_parser import get_tex_from_xml
from ptf.display import resolver


def get_tex_keyword_date_published(colid, article, lang=""):
    keyword = "\\dateposted" if colid in ["AHL", "AIF", "OGEO", "JTNB"] else "\\datepublished"

    if colid in ["CRMATH", "CRMECA", "CRPHYS", "CRGEOS", "CRCHIM", "CRBIOL"]:
        if not article.date_online_first:
            keyword = "\\dateposted"

    if lang != "":
        keyword = "\\CDRsetmeta{traduction_date_posted}"

    return keyword


def get_tex_keyword_date_online_first(colid, article, lang=""):
    return "\\dateposted"


def read_tex_file(filename):
    """
    read a tex file. Detects if it is in utf-8 or iso-8859-1
    returns a string of the body
    """

    lines = []

    if os.path.isfile(filename):
        try:
            with open(filename, encoding="utf-8") as f_:
                lines = f_.readlines()
        except UnicodeDecodeError:
            with open(filename, encoding="iso-8859-1") as f_:
                lines = f_.readlines()

    return lines


def convert_file_to_utf8(article_path, from_name, to_name):
    filename = os.path.join(article_path, from_name)
    lines = read_tex_file(filename)

    user = settings.MERSENNE_TEX_USER
    prefix = os.path.join(settings.LOG_DIR, "tmp/")
    resolver.create_folder(prefix)

    f = tempfile.NamedTemporaryFile(mode="w", encoding="utf-8", prefix=prefix, delete=False)
    fpath = f.name  # ex: /tmp/Rxsft
    f.write("".join(lines))
    f.close()

    out_filemane = os.path.join(article_path, to_name)
    # copy to mersenne-tex
    cmd = f"scp {fpath} {user}@mathdoc-tex:{out_filemane}"
    utils.execute_cmd(cmd)


def write_tex_file(filename, lines, create_temp_file=False):
    fpath = filename
    if create_temp_file:
        prefix = os.path.join(settings.LOG_DIR, "tmp/")
        resolver.create_folder(prefix)

        f = tempfile.NamedTemporaryFile(mode="w", encoding="utf-8", prefix=prefix, delete=False)

        fpath = f.name  # ex: /tmp/Rxsft
        f.write("".join(lines))
        f.close()
    else:
        with open(filename, "w", encoding="utf-8") as f_:
            f_.write("".join(lines))
    return fpath


def insert_date_published(new_lines, article, colid, begin_document_pos, lang=""):
    if article.date_published is not None:
        keyword = get_tex_keyword_date_published(colid, article, lang)
        str_ = f'{keyword}{{{article.date_published.strftime("%Y-%m-%d")}}}\n'
        new_lines.insert(begin_document_pos, str_)
        begin_document_pos += 1

    if article.my_container is not None:
        is_thematic_issue = len(article.my_container.title_html) > 0
        is_issue_finalized = not article.my_container.with_online_first
        is_thematic_finalized = is_thematic_issue and is_issue_finalized

        if is_thematic_finalized and article.date_online_first is None:
            # Finalized thematic issue where the article did not go through online first
            # => Add \datepublished so that "Issue date :" appears in the PDF
            keyword2 = "\\datepublished"
            if keyword2 != keyword:
                str_ = f'{keyword2}{{{article.date_published.strftime("%Y-%m-%d")}}}\n'
                new_lines.insert(begin_document_pos, str_)
                begin_document_pos += 1

    return begin_document_pos


def insert_date_online_first(new_lines, article, colid, begin_document_pos, lang=""):
    if article.date_online_first is not None:
        keyword = get_tex_keyword_date_online_first(colid, article, lang)
        str_ = f'{keyword}{{{article.date_online_first.strftime("%Y-%m-%d")}}}\n'
        new_lines.insert(begin_document_pos, str_)
        begin_document_pos += 1

    return begin_document_pos


def insert_end_page(new_lines, article, colid, begin_document_pos):
    if article.lpage:
        str_ = "\\makeatletter\\def\\cdr@end@page{" + article.lpage + "}\\makeatother\n"
        new_lines.insert(begin_document_pos + 1, str_)

    return begin_document_pos


def replace_dates_in_tex(lines, article, colid, replace_frontpage_only=False, lang=""):
    r"""
    add or replace \dateposted and \datepublished in the source Tex
    lines is a list of lines of the source Tex

    """
    new_lines = []
    bib_name = ""

    keyword_date_published = get_tex_keyword_date_published(colid, article, lang)
    keyword_date_online_first = get_tex_keyword_date_online_first(colid, article, lang)
    found_date_online_first = False
    found_date_published = False
    begin_document_pos = -1
    i = 0
    skip_lines = 0

    while i < len(lines):
        line = lines[i]
        len_line = len(line)
        j = 0
        while j < len_line and line[j] in [" ", "\t"]:
            j += 1

        if j < len_line and line[j] != "%":  # the line is not a comment
            if replace_frontpage_only and (
                line.find("\\datepublished{", j) == j
                or line.find("\\dateposted{", j) == j
                or line.find("\\CDRsetmeta{traduction_date_posted}{", j) == j
            ):
                skip_lines += 1
            elif (
                line.find(f"{keyword_date_published}{{", j) == j
            ):  # replace existing \datepublished
                found_date_published = True
                insert_date_published(new_lines, article, colid, len(new_lines), lang=lang)

            elif (
                line.find(f"{keyword_date_online_first}{{", j) == j
            ):  # replace existing \dateposted
                found_date_online_first = True
                insert_date_online_first(new_lines, article, colid, len(new_lines), lang=lang)

            elif (
                line.find("\\begin{document", j) == j
            ):  # \begin{document} add dates if not present
                begin_document_pos = i - skip_lines
                new_lines.append(line)

            elif line.find("\\documentclass", j) == j or line.find("{\\documentclass", j) == j:
                # remove published from \documentclass to allow compilation
                line = (
                    line.replace(",published,", ",")
                    .replace(",published", "")
                    .replace("published", "")
                )
                # # remove Unicode temporarily
                # line = line.replace(",Unicode,", ",").replace(",Unicode", "").replace("Unicode", "")
                new_lines.append(line)

            elif line.find("\\makeatletter\\def\\cdr@end@page", j) == j:
                # Command to specify the last page (present in the front page)
                # Move it after \begin{document}
                pass
            elif (
                line.find("\\bibliography", j) == j
                and line.find("\\bibliographystyle", j) != j
                and replace_frontpage_only
            ):
                end = line.find("}")
                if end > 0:
                    bib_name = line[j + 14 : end]
                    new_lines.append("\\bibliography{" + bib_name + "_FP}\n")
            else:
                new_lines.append(line)
        else:
            new_lines.append(line)

        i += 1

    if begin_document_pos > 0 and not found_date_online_first:
        begin_document_pos = insert_date_online_first(
            new_lines, article, colid, begin_document_pos, lang=lang
        )

    if begin_document_pos > 0 and not found_date_published:
        begin_document_pos = insert_date_published(
            new_lines, article, colid, begin_document_pos, lang=lang
        )

    if replace_frontpage_only and begin_document_pos > 0:
        begin_document_pos = insert_end_page(new_lines, article, colid, begin_document_pos)

    # Always add Unicode as the new tex file is in utf-8
    # new_lines = protect_tex(new_lines, "Unicode")

    return new_lines, bib_name


def protect_tex(lines, keyword="published"):
    new_lines = []

    i = 0
    inside_documentclass = False

    while i < len(lines):
        line = lines[i]
        len_line = len(line)
        j = 0
        while j < len_line and line[j] in [" ", "\t"]:
            j += 1

        if j < len_line and line[j] != "%":  # the line is not a comment
            if line.find("\\documentclass", j) == j or line.find("{\\documentclass", j) == j:
                # add published to \documentclass after compilation
                j = line.find("]")
                if j > 0:
                    if line.find("{cedram") > 0:  # Ignore {article}
                        line = line[0:j] + "," + keyword + line[j:]
                else:
                    inside_documentclass = True
            elif inside_documentclass:
                k = line.find("]")
                if k == j:
                    if line.find("{cedram") > 0:  # Ignore {article}
                        new_lines.append(f",{keyword}\n")
                    inside_documentclass = False
                elif k > -1:
                    if line.find("{cedram") > 0:  # Ignore {article}
                        line = line[0:k] + "," + keyword + line[k:]
                    inside_documentclass = False

        new_lines.append(line)
        i += 1

    return new_lines


def get_tex_corresponding_emails(author_contributions):
    emails = []

    for contribution in author_contributions:
        if contribution.corresponding and contribution.email:
            emails.append(unicode_to_latex(contribution.email).replace(r"\_", r"_"))

    return emails


def get_tex_authors(author_contributions):
    lines = []

    # are_all_equal = models.are_all_equal_contrib(author_contributions)

    for contribution in author_contributions:
        # \author{\firstname{Antoine} \lastname{Lavoisier}}
        # \address{Rue sans aplomb, Paris, France}
        # \email[A. Lavoisier]{a-lavois@lead-free-univ.edu}
        first_name = unicode_to_latex(contribution.first_name)
        last_name = unicode_to_latex(contribution.last_name)
        line = f"\\author{{\\firstname{{{first_name}}} \\lastname{{{last_name}}}"
        if contribution.orcid:
            line += f"\\CDRorcid{{{contribution.orcid}}}"
        if contribution.equal_contrib:  # and not are_all_equal:
            line += "\\IsEqualContrib"
        if contribution.deceased_before_publication:  # and not are_all_equal:
            line += "\\dead"
        lines.append(line + "}\n")

        for contribaddress in contribution.contribaddress_set.all():
            address = unicode_to_latex(contribaddress.address)
            lines.append(f"\\address{{{address}}}\n")

        if contribution.corresponding and len(contribution.email) > 0:
            email = unicode_to_latex(contribution.email)
            lines.append(f"\\email{{{email}}}\n")

        lines.append("\n")

    return lines


def create_tex_for_pcj(article):
    pci = article.get_pci_section()

    extid = model_helpers.get_extid(article, "rdoi")
    rdoi = extid.id_value if extid is not None else ""

    lines = [
        "\\documentclass[PCJ,Unicode,screen,Recup]{cedram}\n",
        "\\usepackage{pax}\n",
        "\\usepackage{mathrsfs}\n" "\n",
        "\\issueinfo{"
        + article.my_container.volume
        + "}{}{}{"
        + article.my_container.year
        + "}\n",
        f"\\renewcommand*{{\\thearticle}}{{{article.article_number}}}\n",
        f"\\DOI{{{article.doi}}}\n",
        f"\\RDOI{{{rdoi}}}\n",
        f"\\setPCI{{{pci}}}\n",
        f"\\CDRsetmeta{{articletype}}{{{article.atype}}}",
    ]

    conf = article.get_conference()
    if len(conf) > 0:
        lines.append(f"\\setPCIconf{{{conf}}}\n")

    author_contributions = article.get_author_contributions()

    corresponding_emails = get_tex_corresponding_emails(author_contributions)
    for email in corresponding_emails:
        lines.append(f"\\PCIcorresp{{{email}}}\n")

    lines.append("\n")

    # \title[Sample for the template]{Sample for the template, with quite a very long title}
    title = article.title_tex.replace("<i>", "|||i|||").replace("</i>", "|||/i|||")
    title = title.replace("<sup>", "|||sup|||").replace("</sup>", "|||/sup|||")
    title = title.replace("<sub>", "|||sub|||").replace("</sub>", "|||/sub|||")
    title = unicode_to_latex(title)
    title = title.replace("|||i|||", "\\protect\\emph{").replace("|||/i|||", "}")
    title = title.replace("|||sup|||", "\\protect\\textsuperscript{").replace("|||/sup|||", "}")
    title = title.replace("|||sub|||", "\\protect\\textsubscript{").replace("|||/sub|||", "}")
    lines.append(f"\\title{{{title}}}\n")
    lines.append("\n")
    lines.extend(get_tex_authors(author_contributions))

    # No keywords for PCJ
    # # \keywords{Example, Keyword}
    # kwd_gps = article.get_non_msc_kwds()
    # if len(kwd_gps) > 0:
    #     kwd_gp = kwd_gps.first()
    #     keywords = ", ".join([kwd.value for kwd in kwd_gp.kwd_set.all()])
    #     lines.append(f"\\keywords{{{unicode_to_latex(keywords)}}}\n")
    #     lines.append("\n")

    abstracts = article.get_abstracts()
    if len(abstracts) > 0:
        abstract = abstracts.first()
        value = get_tex_from_xml(abstract.value_xml, "abstract", for_tex_file=True)

        # .replace('<span class="mathjax-formula">$', '$').replace('$</span>', '$') \
        # .replace('<span class="italique">', '|||i|||').replace('</span>', '|||/i|||') \

        # value = abstract.value_tex \
        #     .replace('<i>', '|||i|||').replace('</i>', '|||/i|||') \
        #     .replace('<strong>', '|||strong|||').replace('</strong>', '|||/strong|||') \
        #     .replace('<sub>', '|||sub|||').replace('</sub>', '|||/sub|||') \
        #     .replace('<sup>', '|||sup|||').replace('</sup>', '|||/sup|||') \
        #     .replace('<p>', '').replace('</p>', '') \
        #     .replace('<ul>', '|||ul|||').replace('</ul>', '|||/ul|||') \
        #     .replace('<ol type="1">', '|||ol|||').replace('</ol>', '|||/ol|||') \
        #     .replace('<li>', '|||li|||').replace('</li>', '|||/li|||') \
        #     .replace('<br/>', '|||newline|||') \
        #     .replace('&amp;', '\\&') \
        #     .replace('&lt;', '<') \
        #     .replace('&gt;', '>')
        #
        # links = []
        # pos = value.find("<a href=")
        # while pos != -1:
        #     last_href = value.find('"', pos + 9)
        #     href = value[pos + 9:last_href]
        #     first_text = value.find('>', last_href) + 1
        #     last_text = value.find('</a>', first_text)
        #     text = value[first_text:last_text]
        #     links.append((href, text))
        #     value = value[0:pos] + '|||a|||' + value[last_text + 4:]
        #     pos = value.find("<a href=")
        #
        # value = unicode_to_latex(value)
        # value = value.replace('|||i|||', '{\\it ').replace('|||/i|||', '}')
        # value = value.replace('|||strong|||', '{\\bf ').replace('|||/strong|||', '}')
        # value = value.replace('|||sub|||', '\\textsubscript{').replace('|||/sub|||', '}')
        # value = value.replace('|||sup|||', '\\textsuperscript{').replace('|||/sup|||', '}')
        # value = value.replace('|||ul|||', '\n\\begin{itemize}\n').replace('|||/ul|||', '\\end{itemize}\n')
        # value = value.replace('|||ol|||', '\n\\begin{enumerate}\n').replace('|||/ol|||', '\\end{enumerate}\n')
        # value = value.replace('|||li|||', '\\item ').replace('|||/li|||', '\n')
        # value = value.replace('|||newline|||', '\\newline\n')
        # for link in links:
        #     text = f'\\href{{{link[0]}}}{{{link[1]}}}'
        #     value = value.replace('|||a|||', text, 1)

        lines.append("\\begin{abstract}\n")
        lines.append(value + "\n")
        lines.append("\\end{abstract}\n")

    date_ = article.date_published.strftime("%Y-%m-%d") if article.date_published else "AAAA-MM-DD"
    keyword = get_tex_keyword_date_published("PCJ", article)
    lines.append(f"{keyword}{{{date_}}}\n")

    lines.append("\\begin{document}\n")
    lines.append("\\maketitle\n")
    article_pdf = f"article_{article.pid}.pdf"
    lines.append(f"\\PCIincludepdf{{{article_pdf}}}\n")

    lines.append("\\end{document}\n")

    return lines


def compile_tex(lines, article, update=False):
    """
    1) Create a tex file from the list of lines
    2) Upload the file to mathdoc-tex (+ the pdf for PCJ)
    3) Compile the file
    4) Replace the pdf in /mersenne_test_data
    5) linearize the pdf
    TODO: merge ptf_tools/views create_frontpage (not done while PCJ is unstable to avoid compilation bugs in prod)
    """

    # Only allowed on ptf-tools
    if settings.SITE_NAME != "ptf_tools":
        return

    user = settings.MERSENNE_TEX_USER
    issue = article.my_container
    colid = issue.my_collection.pid
    issue_path = resolver.get_cedram_issue_tex_folder(colid, issue.pid)
    article_pdf = ""

    if colid != "PCJ":
        article_tex_name = article.get_ojs_id()
        if not article_tex_name:
            raise Exception(f"Article {article.pid} has no ojs-id -> cedram tex path")
        article_path = os.path.join(issue_path, article_tex_name)
    else:
        article_tex_name = article.pid
        article_path = os.path.join(issue_path, article_tex_name)
        article_pdf = f"article_{article.pid}.pdf"

        if not update:
            # Create the article folder
            cmd = f"ssh {user}@mathdoc-tex mkdir -p {article_path}"
            utils.execute_cmd(cmd)

            # copy the pdf to mersenne-tex
            relative_folder = resolver.get_relative_folder(colid, issue.pid, article.pid)
            folder = os.path.join(settings.RESOURCES_ROOT, relative_folder)
            pdf_file_name = os.path.join(folder, article.pid + ".pdf")

            cmd = f"scp {pdf_file_name} {user}@mathdoc-tex:{article_path}/{article_pdf}"
            utils.execute_cmd(cmd)

    article_tex_file_name = os.path.join(article_path, article_tex_name + ".tex")
    fpath = write_tex_file("", lines, create_temp_file=True)

    # copy to mersenne-tex
    cmd = f"scp {fpath} {user}@mathdoc-tex:{article_tex_file_name}"
    utils.execute_cmd(cmd)
    # os.unlink(f.name)

    # recompile article
    ptf_tools_bin = os.path.join(settings.BASE_DIR, "bin")
    # execute script to compile
    cmd = f"ssh {user}@mathdoc-tex 'bash -s' -- < {ptf_tools_bin}/create_frontpage.sh {article_path} {article_tex_name} {colid} {article_pdf}"
    utils.execute_cmd(cmd)

    # replace pdf
    cedram_pdf_location = os.path.join(article_path, article_tex_name + ".pdf")
    relative_folder = resolver.get_relative_folder(colid, issue.pid, article.pid)
    to_path = os.path.join(
        settings.MERSENNE_TEST_DATA_FOLDER, relative_folder, article.pid + ".pdf"
    )
    if settings.MERSENNE_CREATE_FRONTPAGE:
        utils.linearize_pdf(cedram_pdf_location, to_path)

    return to_path


def add_outline(reader, writer, outlines, parent=None):
    child_parent = parent
    for item in outlines:
        if type(item) == list:
            add_outline(reader, writer, item, child_parent)
        else:
            title = item["/Title"]
            page_num = reader.get_destination_page_number(item)

            if item["/Type"] == "/XYZ":
                child_parent = writer.add_outline_item(
                    title,
                    page_num,
                    parent,
                    None,
                    False,
                    False,
                    pypdf.generic.Fit("/XYZ", (item["/Left"], item["/Top"], 1)),
                )
            else:
                child_parent = writer.add_outline_item(title, page_num, parent, None, False, False)


def test():
    local_fp_pdf = "/home/touvierj/Bureau/test_FP.pdf"
    local_content_pdf = "/home/touvierj/Bureau/test_content.pdf"
    merged_pdf = "/home/touvierj/Bureau/test_merged.pdf"

    pdf_reader_fp = pypdf.PdfReader(local_fp_pdf, strict=False)
    pdf_reader_content = pypdf.PdfReader(local_content_pdf, strict=False)
    pdf_writer = pypdf.PdfWriter()

    for page in range(len(pdf_reader_fp.pages)):
        current_page = pdf_reader_fp.pages[page]
        if page == 0:
            pdf_writer.add_page(current_page)

    for page in range(len(pdf_reader_content.pages)):
        current_page = pdf_reader_content.pages[page]
        if page > 0:
            pdf_writer.add_page(current_page)

    # Add the Table of Contents (sidebar in a PDF reader)
    add_outline(pdf_reader_content, pdf_writer, pdf_reader_content.outline)

    # Add the anchors
    for dest in pdf_reader_content.named_destinations.values():
        pdf_writer.add_named_destination_object(dest)

    with open(merged_pdf, "wb") as f_:
        pdf_writer.write(f_)

    # Add metadata to the PDF, including EXIF data
    add_metadata(models.Article.objects.first(), local_content_pdf, merged_pdf)

    exit()

    fpage = "i"
    merged_pdf = "/home/touvierj/Bureau/good2.pdf"
    local_pdf = "/home/touvierj/Bureau/new2.pdf"

    is_roman = False
    try:
        first_page = int(fpage)
    except ValueError:
        first_page = xml_utils.roman_to_int(fpage)
        is_roman = True

    reader = pypdf.PdfReader(merged_pdf)
    writer = pypdf.PdfWriter()
    for page in reader.pages:
        writer.add_page(page)

    if is_roman:
        writer.set_page_label(page_index_from=0, page_index_to=first_page - 1, style="/r")
    else:
        writer.set_page_label(page_index_from=0, page_index_to=first_page - 1, style="/D")
    writer.write(local_pdf)
    writer.close()


def add_metadata(article, in_pdf, out_pdf):
    reader = pypdf.PdfReader(in_pdf, strict=False)

    metadata = reader.metadata
    cmd = f"exiftool -tagsFromFile {in_pdf}"

    if in_pdf == out_pdf:
        cmd += " -overwrite_original_in_place"

    container = article.my_container
    collection = article.get_collection()

    msc_kwds, kwds, trans_kwds = article.get_kwds_by_type()
    keywords = ", ".join([str(x.value) for x in kwds])

    lang = ""
    if article.lang == "fr":
        lang = "fr-FR"
    elif article.lang == "en":
        lang = "en-GB"

    if "/Title" in metadata:
        title = metadata["/Title"]
        if "'" in title and '"' not in title:
            cmd += f' -Title="{title}"'
        elif "'" not in title:
            cmd += f" -Title='{title}'"

    if "/Author" in metadata:
        author = metadata["/Author"]
        if "'" in author and '"' not in author:
            cmd += f' -Author="{author}"'
        elif "'" not in author:
            cmd += f" -Author='{author}'"

    cmd += " -Creator='Centre Mersenne'"
    cmd += " -Subject=''"
    if lang:
        cmd += f" -xmp-dc-Language='{lang}'"
    cmd += f" -xmp-dc:publisher='{container.my_publisher.pub_name}'"
    cmd += f" -xmp-prism:DOI='{article.doi}'"
    cmd += f" -Keywords='{keywords}'"
    cmd += f" -xmp-xmp:Keywords='{keywords}'"
    cmd += f" -xmp-pdf:Keywords='{keywords}'"
    cmd += " -xmp-pdf:Copyright='© The author(s)'"

    if container.volume:
        cmd += f" -xmp-prism:Volume='{container.volume}'"
    if container.number:
        cmd += f" -xmp-prism:Number='{container.number}'"
    if collection.issn:
        cmd += f" -xmp-prism:ISSN='{collection.issn}'"
    if collection.e_issn:
        cmd += f" -xmp-prism:EISSN='{collection.e_issn}'"
    if container.title_tex:
        cmd += f" -xmp-prism:IssueName='{container.title_tex}'"
    cmd += " " + out_pdf

    output = subprocess.check_output(cmd, shell=True)
    return output


def replace_front_page(
    article, article_tex_name, fp_pdf_file_name, content_pdf_file_name, final_pdf_file_name
):
    # At the point the PDF has been recompiled, possibly with a new template
    # Use the 1st page of the new PDF with the other pages of the .pdf_SAV

    user = settings.MERSENNE_TEX_USER

    # Copy the PDF files locally (pypdf is installed in ptf-tools)
    local_fp_pdf = os.path.join(settings.LOG_DIR, "tmp/", article_tex_name + ".pdf_FP")
    cmd = f"scp {user}@mathdoc-tex:{fp_pdf_file_name} {local_fp_pdf}"
    utils.execute_cmd(cmd)

    local_content_pdf = os.path.join(settings.LOG_DIR, "tmp/", article_tex_name + ".pdf_content")
    cmd = f"cp {content_pdf_file_name} {local_content_pdf}"
    utils.execute_cmd(cmd)

    pdf_reader_fp = pypdf.PdfReader(local_fp_pdf, strict=False)
    pdf_reader_content = pypdf.PdfReader(local_content_pdf, strict=False)
    pdf_writer = pypdf.PdfWriter()

    for page in range(len(pdf_reader_fp.pages)):
        current_page = pdf_reader_fp.pages[page]
        if page == 0:
            pdf_writer.add_page(current_page)

    for page in range(len(pdf_reader_content.pages)):
        current_page = pdf_reader_content.pages[page]
        if page > 0:
            pdf_writer.add_page(current_page)

    # Add the Table of Contents (sidebar in a PDF reader)
    add_outline(pdf_reader_content, pdf_writer, pdf_reader_content.outline)

    # Add the anchors
    for dest in pdf_reader_content.named_destinations.values():
        pdf_writer.add_named_destination_object(dest)

    merged_pdf = os.path.join(settings.LOG_DIR, "tmp/", article_tex_name + ".pdf_merged")
    with open(merged_pdf, "wb") as f_:
        pdf_writer.write(f_)

    # Compiled PDF are sometimes buggy (wrong xref table). Use pdftk to fix the file.
    ptf_tools_bin = os.path.join(settings.BASE_DIR, "bin")
    cmd = f"{ptf_tools_bin}/update_pdf.sh {local_content_pdf} {merged_pdf}"
    utils.execute_cmd(cmd)

    # Add metadata to the PDF, including EXIF data
    add_metadata(article, local_content_pdf, merged_pdf)

    local_pdf = os.path.join(settings.LOG_DIR, "tmp/", article_tex_name + ".pdf")

    # pypdf creates a PDF that starts on page 1, fix it
    if article.fpage:
        is_roman = False
        try:
            first_page = int(article.fpage)
        except ValueError:
            first_page = xml_utils.roman_to_int(article.fpage)
            is_roman = True

        reader = pypdf.PdfReader(merged_pdf)
        writer = pypdf.PdfWriter()
        page_count = 0
        for page in reader.pages:
            page_count += 1
            writer.add_page(page)

        if is_roman:
            writer.set_page_label(
                page_index_from=0, page_index_to=page_count - 1, start=first_page, style="/r"
            )
        else:
            writer.set_page_label(
                page_index_from=0, page_index_to=page_count - 1, start=first_page, style="/D"
            )
        writer.write(local_pdf)
        writer.close()

    # copy to mersenne-tex
    cmd = f"scp {local_pdf} {user}@mathdoc-tex:{final_pdf_file_name}"
    utils.execute_cmd(cmd)


def compile_article(
    article,
    colid,
    issue_id,
    article_path,
    article_tex_name,
    replace_frontpage_only=False,
    skip_compilation=False,
    lang="",
):
    user = settings.MERSENNE_TEX_USER

    if lang != "":
        article_tex_name += "-" + lang

    article_cfg_file_name = os.path.join(article_path, article_tex_name + ".cfg")
    # Regular compilation: compiled_pdf and final_pdf are the same
    # recompilation of the front page: compiled_pdf is the entire pdf with the new front page
    #                                  final_pdf is the pdf after the merge (new front page; old content)
    compiled_pdf_file_name = final_pdf_file_name = os.path.join(
        article_path, article_tex_name + ".pdf"
    )
    sav_pdf_file_name = compiled_pdf_file_name + "_SAV"

    # Save the pdf file
    cmd = f"ssh {user}@mathdoc-tex cp {compiled_pdf_file_name} {sav_pdf_file_name}"
    utils.execute_cmd(cmd)

    # Save the cfg file (no cfg for translations)
    if lang == "":
        cmd = f"ssh {user}@mathdoc-tex cp {article_cfg_file_name} {article_cfg_file_name}_SAV"
        utils.execute_cmd(cmd)

    # create temporarly file  ! attention sur ptf-tools apache n'a pas le droit d'écrire ds /tmp ?!.
    prefix = os.path.join(settings.LOG_DIR, "tmp/")
    resolver.create_folder(prefix)

    if replace_frontpage_only and skip_compilation:
        # We want to update the front page without compiling the tex:
        #   We copy the original <article_tex_name>.PDF to <article_tex_name>_FP.PDF

        article_tex_name2 = article_tex_name + "_FP"
        new_compiled_pdf_file_name = os.path.join(article_path, article_tex_name2 + ".pdf")
        cmd = f"ssh {user}@mathdoc-tex cp {compiled_pdf_file_name} {new_compiled_pdf_file_name}"
        utils.execute_cmd(cmd)
        compiled_pdf_file_name = new_compiled_pdf_file_name

    elif replace_frontpage_only:
        # Copy CFG/TEX/PDF to a new name. pdflatex will generate new files, thus preserving existing files
        article_tex_name2 = article_tex_name + "_FP"

        cmd = f"ssh {user}@mathdoc-tex rm -f {os.path.join(article_path, article_tex_name2)}.*"
        utils.execute_cmd(cmd)

        article_tex_file_name = os.path.join(article_path, article_tex_name + ".tex")
        article_tex_file_name2 = os.path.join(article_path, article_tex_name2 + ".tex")
        cmd = f"ssh {user}@mathdoc-tex cp {article_tex_file_name} {article_tex_file_name2}"
        utils.execute_cmd(cmd)

        article_cfg_file_name2 = os.path.join(article_path, article_tex_name2 + ".cfg")
        cmd = f"ssh {user}@mathdoc-tex cp {article_cfg_file_name} {article_cfg_file_name2}_SAV"
        utils.execute_cmd(cmd)

        article_cdrdoidates_file_name = os.path.join(
            article_path, article_tex_name + ".cdrdoidates"
        )
        if os.path.isfile(article_cdrdoidates_file_name):
            article_cdrdoidates_file_name2 = os.path.join(
                article_path, article_tex_name2 + ".cdrdoidates"
            )
            cmd = f"ssh {user}@mathdoc-tex cp {article_cdrdoidates_file_name} {article_cdrdoidates_file_name2}"
            utils.execute_cmd(cmd)

        article_tex_name = article_tex_name2
        article_cfg_file_name = os.path.join(article_path, article_tex_name + ".cfg")
        compiled_pdf_file_name = os.path.join(article_path, article_tex_name + ".pdf")
        final_pdf_file_name = compiled_pdf_file_name + ".new"

    if not skip_compilation:
        # Remove \ItIsPublished from the cfg file
        if lang == "":
            cmd = f'''ssh {user}@mathdoc-tex "sed 's/\\\\\\\\ItIsPublished//' {article_cfg_file_name}_SAV > {article_cfg_file_name}.1"'''
            utils.execute_cmd(cmd)
            cmd = f'''ssh {user}@mathdoc-tex "sed 's/\\\\\\\\gdef \\\\\\\\CDRpublished {{true}}//' {article_cfg_file_name}.1 > {article_cfg_file_name}"'''
            utils.execute_cmd(cmd)

        article_tex_file_name = os.path.join(article_path, article_tex_name + ".tex")

        # Save the tex file
        cmd = f"ssh {user}@mathdoc-tex cp {article_tex_file_name} {article_tex_file_name}_SAV"
        utils.execute_cmd(cmd)

        lines = read_tex_file(article_tex_file_name)
        new_lines, bib_name = replace_dates_in_tex(
            lines, article, colid, replace_frontpage_only, lang=lang
        )

        if bib_name and replace_frontpage_only:
            convert_file_to_utf8(article_path, bib_name + ".bib", bib_name + "_FP.bib")

        f = tempfile.NamedTemporaryFile(mode="w", encoding="utf-8", prefix=prefix, delete=False)
        fpath = f.name  # ex: /tmp/Rxsft
        f.write("".join(new_lines))
        f.close()

        # copy to mersenne-tex
        cmd = f"scp {fpath} {user}@mathdoc-tex:{article_tex_file_name}"
        utils.execute_cmd(cmd)
        # os.unlink(f.name)

        # recompile article
        ptf_tools_bin = os.path.join(settings.BASE_DIR, "bin")
        # execute script to compile
        cmd = f"ssh {user}@mathdoc-tex 'bash -s' -- < {ptf_tools_bin}/create_frontpage.sh {article_path} {article_tex_name}"
        utils.execute_cmd(cmd)

        # Protect the tex file with the 'published' option
        new_lines = protect_tex(new_lines)

        # create temporarly file  ! attention sur ptf-tools apache n'a pas le droit d'écrire ds /tmp ?!.
        prefix = os.path.join(settings.LOG_DIR, "tmp/")
        f = tempfile.NamedTemporaryFile(mode="w", encoding="utf-8", prefix=prefix, delete=False)
        fpath = f.name  # ex: /tmp/Rxsft
        f.write("".join(new_lines))
        f.close()

        # copy to mersenne-tex
        cmd = f"scp {fpath} {user}@mathdoc-tex:{article_tex_file_name}"
        utils.execute_cmd(cmd)

    if replace_frontpage_only:
        # At the point the PDF has been recompiled, possibly with a new template
        # Use the 1st page of the new PDF with the other pages of the production PDF
        datastream = article.datastream_set.filter(mimetype="application/pdf").get()
        content_pdf_file_name = os.path.join(
            settings.MERSENNE_PROD_DATA_FOLDER, datastream.location
        )

        replace_front_page(
            article,
            article_tex_name,
            compiled_pdf_file_name,
            content_pdf_file_name,
            final_pdf_file_name,
        )

    # Copy PDF to MERSENNE_TEST_DATA_FOLDER
    datastream = article.datastream_set.filter(mimetype="application/pdf").get()
    to_path = os.path.join(settings.MERSENNE_TEST_DATA_FOLDER, datastream.location)
    # remove destination if exists to test if final pdf is really created
    if os.path.exists(to_path):
        os.remove(to_path)
    utils.linearize_pdf(final_pdf_file_name, to_path)

    # if not replace_frontpage_only:
    #     # Add EXIF metadata in the final PDF (replace_front_page already does it)
    #     add_metadata(article, to_path, to_path)


def create_frontpage(
    colid,
    container,
    updated_articles,
    test=True,
    replace_frontpage_only=False,
    skip_compilation=False,
    lang="",
):
    # create frontpage by recompiling articles on mersenne-tex with date XXXX-XX-XX
    # flow :
    # get directory of article sources : cedram_dev/production/ ..
    # Add publication date in the source TeX
    # remote execute latexmk -pdf article.pdf
    # replace pdf of the article on mersenne_test_data

    # TODO refactor the code and only use compile_tex for all collections

    if colid == "PCJ":
        for article in updated_articles:
            lines = create_tex_for_pcj(article)
            compile_tex(lines, article, update=True)
        return

    try:
        year = int(container.year)
    except ValueError:
        year = 0

    if (
        colid in ["CRMATH", "CRMECA", "CRPHYS", "CRGEOS", "CRCHIM", "CRBIOL"]
        and year < 2020
        and lang == ""
    ):
        # No front page for Elsevier CRAS
        return

    issue_id = container.pid

    issue_path = resolver.get_cedram_issue_tex_folder(colid, issue_id)
    # non utilisé ?now = datetime.now().astimezone()
    # non utilisé ? timestamp = now.strftime("%Y-%m-%d %H:%M:%S %Z")

    try:
        for article in updated_articles:
            # article path
            article_tex_name = article.get_ojs_id()
            if not article_tex_name:
                raise Exception(f"Article {article.pid} has no ojs-id -> cedram tex path")
            article_path = os.path.join(issue_path, article_tex_name)
            # non utilisé ? file_date = os.path.join(article_path, article_tex_name + '.ptf')

            # publish_timestamp_file = os.path.join(article_path, article_tex_name + "-pdftimestamp.txt")
            # onlinefirst_timestamp_file = os.path.join(article_path, article_tex_name + "-dateposted.txt")

            # flow :
            # - si on est en test ;
            # date_pre_publish a été mis à jour mais pas les autres
            #  on ne crée une date temporaire type XXXX-XX-XX que pour online_first
            #  (car sinon lors de la mise en prod du online_first, la présence du fichier pdftimestamp
            #  avec XXXX-XX-XX ferait apparaitre cette date)
            #     - si article.my_container.with_online_first && et pas de article.date_online_first existe:
            #        : on met XXXX-xx-xx pour online first
            #       (si l'article a déjà une date online-first, il a à priori déjà était recompilé)
            # - si on passe en prod, on prend les dates de l'article
            #   si container.with_online_first:
            #        article.date_online_first ds le bon fichier
            #   si article.date_published : on met à jour le fichier qui va bien

            if not test and (article.date_online_first or article.date_published):
                compile_article(
                    article,
                    colid,
                    issue_id,
                    article_path,
                    article_tex_name,
                    replace_frontpage_only,
                    skip_compilation,
                    lang,
                )

    except Exception as e:
        # pas de rollback car on ne modifie rien en BDD / éventuellement remettre un pdf.SAV en place
        raise e


def create_translated_pdf(
    article, xml_content, lang, pdf_file_name, html_file_name, skip_compilation=False
):
    user = settings.MERSENNE_TEX_USER

    issue_path = resolver.get_cedram_issue_tex_folder(
        article.get_top_collection().pid, article.my_container.pid
    )
    article_tex_name = article.get_ojs_id()
    if not article_tex_name:
        raise Exception(f"Article {article.pid} has no ojs-id -> cedram tex path")
    article_path = os.path.join(issue_path, article_tex_name)

    xml_base_name = article_tex_name + ".xml"
    local_xml = os.path.join(settings.LOG_DIR, "tmp", xml_base_name)
    remote_xml = os.path.join(article_path, xml_base_name)

    if not skip_compilation:
        # Create the XML file locally
        with open(local_xml, "w", encoding="utf-8") as file_:
            file_.write(xml_content)

        # Copy XML file to mersenne-tex
        cmd = f"scp {local_xml} {user}@mathdoc-tex:{remote_xml}"
        utils.execute_cmd(cmd)

        remote_html_base_name = f"trad-{lang}.html"
        remote_html = os.path.join(article_path, remote_html_base_name)
        # Copy HTML file to mersenne-tex
        cmd = f"scp {html_file_name} {user}@mathdoc-tex:{remote_html}"
        utils.execute_cmd(cmd)

        # Create the PDF
        ptf_tools_bin = os.path.join(settings.BASE_DIR, "bin")
        # execute script to compile
        cmd = f"ssh {user}@mathdoc-tex 'bash -s' -- < {ptf_tools_bin}/translate_article.sh {article_path} {xml_base_name} {remote_html_base_name} {lang}"
        utils.execute_cmd(cmd)

    remote_pdf_base_name = f"{article_tex_name}-{lang}.pdf"
    remote_pdf = os.path.join(article_path, remote_pdf_base_name)
    # pdf-traduction should have created remote.pdf
    # Copy the PDF file
    cmd = f"scp {user}@mathdoc-tex:{remote_pdf} {pdf_file_name}"
    utils.execute_cmd(cmd)
