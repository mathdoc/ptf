import os
from urllib.parse import unquote

from langcodes import Language

from django import template
from django.contrib.staticfiles.finders import find
from django.http import QueryDict
from django.template.defaultfilters import stringfilter
from django.urls import reverse
from django.utils.translation import gettext as _

from ptf import models
from ptf.display import resolver
from ptf.model_helpers import get_resource as get_resource_helper
from ptf.solr.search_helpers import CleanSearchURL

register = template.Library()


@register.filter
@stringfilter
def get_resource(value):
    """Usage, {% if pid|get_resource %}"""
    resource = get_resource_helper(value)
    return resource


@register.simple_tag
def get_resource_url(pid):
    resource = models.Resource.objects.get(pid=pid)
    resource = resource.cast()
    return resource.get_url_absolute()


@register.filter
@stringfilter
def cleanURL_encode(value, arg):
    """Usage : {{ 'q=test&qt=all'|cleanURL_encode:request.path   }}"""
    query = QueryDict(value)
    return CleanSearchURL.encode(query, arg)


def decode_query_string(request):
    raw_query_string = request.META.get("QUERY_STRING", "")
    if raw_query_string:
        decoded_query_string = unquote(raw_query_string)
    else:
        decoded_query_string = ""
    return decoded_query_string


@register.simple_tag
def pretty_search(url, encoding, *args):
    """
    Usage {% pretty_search 'path' 'q' criteria1 criteria2 ... %}
    @param path: the url name
    @param encoding: one or many encodings reference to CleanSearchURL
    @param args: one or many criteria for search
    @return: url with querystring like search?q=test+a
    """
    path = reverse(url)
    query_parts = []
    for index in range(len(encoding)):
        if args[index]:
            query_parts.append(f"{args[index]}")
    query_string = " ".join(query_parts)
    return f"{path}?{query_string}-{encoding}"


@register.filter
@stringfilter
def cleanURL_params_paginate(value, page):
    """
     création du lien pour la pagination
    exemple Usage : {{ '?q=test+all+qa'|cleanURL_encode:"3"   }}
    @return: ?q=test+all+3+qat
    """
    print("Value ", value, " page ", str(page))
    _, query = CleanSearchURL.decode(value)
    print("Query ", query)
    queryDict = query.copy()
    if "page" in queryDict:
        del queryDict["page"]  # supprime la page demandée
    queryDict.appendlist("page", str(page))

    result = CleanSearchURL.encode(queryDict, "")
    return result


@register.filter
@stringfilter
def build_Url_paginate(value, page):
    print("Value", value, "Page", page)
    newQuery = ""
    if len(value) > 1:
        newQuery = value + "&"
    else:
        newQuery = value + "?"
    newQuery = newQuery + "page" + "=" + str(page)
    return newQuery


@register.filter
@stringfilter
def replace_basename_binary_file(value):
    """Usage, {% binary_file.pdf| static_basename_binary_file %}"""
    # if hasattr(settings, 'SITE_URL_PREFIX'):
    #     if len(value) > 0 and value[0] != '/':
    #         value = '/' + settings.SITE_URL_PREFIX + '/' + value
    #     else:
    #         value = '/' + settings.SITE_URL_PREFIX + value
    return value


@register.filter
@stringfilter
def basename(value):
    return os.path.basename(value)


@register.filter
def sort_by(queryset, order):
    return queryset.order_by(order)


@register.filter
def sort_by_date(queryset, container):
    if container.my_collection.pid == "PCJ":
        return queryset.order_by("-date_published")
    return queryset


# @register.filter
# def get_abstract_tag(queryset, tag):
#     return queryset.filter(tag=tag)


@register.simple_tag
def get_flag_icon(language):
    flag = "flag-icon-"

    if language == "en":
        flag += "gb"
    # elif language == "es":
    #     flag += "mx"
    else:
        flag += language

    return flag


@register.filter
def get_flag_unicode(lang):
    territory = Language.get(lang).maximize().territory
    if lang == "en":
        territory = "GB"
    elif lang == "pt":
        territory = "PT"
    points = [ord(x) + 127397 for x in territory]
    return chr(points[0]) + chr(points[1])


@register.filter
@stringfilter
def short_doi(value):
    if len(value) > 9:
        value = value[8:]

    return value


@register.filter
@stringfilter
def get_doi_url(value):
    if value and value.find("10.") == 0:
        value = resolver.get_doi_url(value)
    else:
        value = ""

    return value


@register.filter
@stringfilter
def get_type_value(value):
    value = resolver.ARTICLE_TYPES.get(value, "Article de recherche")
    return value


@register.filter
def exclude_do_not_publish_filter(queryset, export_to_website):
    """
    if we export to website (production), we don't export article for which do_not_publish=True
    """
    qs = queryset
    if export_to_website:
        qs = queryset.exclude(do_not_publish=True)
    return qs


@register.filter
def are_all_equal_contrib(contributions):
    return models.are_all_equal_contrib(contributions)


@register.filter
def filter_by_role(contributions, role):
    if contributions is None:
        return []
    return [contribution for contribution in contributions if contribution.role.find(role) == 0]


@register.filter
def get_addresses(contributions):
    addresses = []
    for contribution in contributions:
        for contrib_address in contribution.contribaddress_set.all():
            if contrib_address.address not in addresses:
                addresses.append(contrib_address.address)
    return addresses


@register.filter
def contributions_with_index(contributions, addresses):
    results = []
    for contribution in contributions:
        indexes = []
        for contrib_address in contribution.contribaddress_set.all():
            address = contrib_address.address
            index = addresses.index(address) + 1
            if index not in [item["index"] for item in indexes]:
                indexes.append({"index": index, "address": address})
        # indexes.sort(key=lambda d: d['index'])
        results.append((contribution, indexes))
    return results


@register.filter
def address_index_value(index, with_letter):
    value = index
    if with_letter:
        value = chr(ord("a") + index - 1)
    return value


@register.filter
def affiliation_id(with_letter):
    return "affilition_a" if with_letter else "affiliation"


@register.filter
def contributions_with_letter_index(contributions, addresses):
    results = []
    for contribution in contributions:
        indexes = []
        for contrib_address in contribution.contribaddress_set.all():
            address = contrib_address.address
            index = addresses.index(address) + 1
            letter = chr(ord("a") + index)
            if index not in [item["index"] for item in indexes]:
                indexes.append({"index": letter, "address": address})
        # indexes.sort(key=lambda d: d['index'])
        results.append((contribution, indexes))
    return results


@register.filter
def is_author_of_original_article(contribution, original_contributions):
    if original_contributions is None or len(original_contributions) == 0:
        return False

    is_original_author = False
    i = 0
    while not is_original_author and i < len(original_contributions):
        if contribution.is_equal(original_contributions[i]):
            is_original_author = True
        i += 1

    return is_original_author


@register.filter
def to_str(value):
    return str(value)


@register.filter
def get_dict_item(my_dict: dict, key: str):
    """
    Wrapper around dict.get() that can be used directly in templates.
    Its purpose is to the ability to pass a variable instead of a plain string.
    Usage `my_dict|get_dict_item:key`
    """
    return my_dict.get(key)


@register.filter
def get_article_heading(article):
    heading = article.get_subj_text()
    collection = article.get_top_collection()
    if collection.pid in ["CRMATH", "CRMECA", "CRPHYS", "CRGEOS", "CRCHIM", "CRBIOL"]:
        if int(article.my_container.year) > 2023:
            atype = resolver.ARTICLE_TYPES.get(article.atype, None)
            if atype is not None:
                if heading:
                    heading = _(atype) + " - " + heading
                else:
                    heading = _(atype)

    return heading


@register.filter
def get_image_path(journal: models.Collection, logo: bool = False):
    """
    Usage: `journal|get_image_path`
    @param journal: La revue dont l'on veut récupérer l'image
    @param logo: Booléen indiquant si l'on veut récupérer le logo ou non
    @return un string contenant le chemin vers l'image, par exemple : "pid/img/PID.jpg" ou "pid/img/pid.png" dans le cas d'un logo
    """
    if journal is None or type(journal) is not models.Collection:
        return ""

    pid = journal.pid
    # Si le pid final est en minuscule, on récupère le logo
    link = pid.lower() + "/img/" + (pid if not logo else pid.lower())
    for extension in [".jpg", ".png"]:
        # Si find renvoie None, c'est que l'image n'a pas été trouvé pour cette extension
        abs_path = find(link + extension)
        if abs_path is not None:
            break
    else:
        # Dans le cas où on a rien trouvé
        return ""

    return link + extension
