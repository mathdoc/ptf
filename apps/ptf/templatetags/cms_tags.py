from django import template

register = template.Library()


@register.simple_tag
def get_all_pages(request, position, with_home=False):
    """
    To be overridden in mersenne_cms
    """
    return []
