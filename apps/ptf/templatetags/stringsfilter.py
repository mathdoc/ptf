import re

from unidecode import unidecode

from django import template
from django.template.defaultfilters import stringfilter
from django.utils import timezone
from django.utils.translation import gettext as translate_text

from ptf.cmds.xml import xml_utils

register = template.Library()


@register.filter
def convert_jats_to_only_mathml(value):
    """Usage, {{ value_xml|convert_jats_to_only_mathml }}
    extract text from xml and preserve Mathml"""
    return xml_utils.get_text_from_xml_with_mathml(value, with_mathml=True)


@register.filter
def get_text_from_original_title_with_mathml(value, get_trans_title=False):
    """Usage, {{ value_xml|get_text_from_original_title_with_mathml }}
    extract text for principal lang from title_xml and preserve Mathml"""
    return xml_utils.get_text_from_original_title_with_mathml(
        value, with_mathml=True, get_trans_title=get_trans_title
    )


@register.filter
@stringfilter
def addXmlnsToBookTitle(value):
    """Usage, {{ value|addXmlnsToBookTitle }}
    add xmlns b: before tag beginning by book"""
    regex = re.compile(r"< ( /{0,1} ) ( book ) ", re.VERBOSE)
    return regex.sub(r"<\1b:\2", value)


@register.filter
@stringfilter
def removeSpaceAttribute(value):
    text = value.replace('xml:space="preserve"', "")
    return text


@register.filter(name="split")
def split(value, key):
    """
    Returns the value turned into a list.
    """
    return value.split(key)


@register.filter("startswith")
def startswith(text, starts):
    if isinstance(text, str):
        return text.startswith(starts)
    return False


@register.filter
def is_translation(value):
    """
    PIDs of translated articles end with "-@lang", using the ISO 639-1 language code
    """
    result = False
    if value.find("-") > 0:
        value = value.split("-")[-1]
        if (len(value) == 2 and not value.isdigit()) or value == "Latn":
            result = True

    return result


@register.filter
def allow_translation(doi):
    """
    Allow translations only for articles edited by centre Mersenne
    """
    return doi.find("10.5802/") == 0


@register.filter
@stringfilter
def eudml_filter(value):
    values = value.split(":")
    if len(values) > 0:
        value = values[-1]
    return value


@register.filter
@stringfilter
def normalize_span(value):
    return xml_utils.normalise_span(value)


@register.filter
@stringfilter
def remove_email(value):
    start = value.find("<email>")
    while start > 0:
        end = value.find("</email>")

        if end > 0:
            value = value[0:start] + value[end + 8 :]

        start = value.find("<email>")

    return value


@register.filter
@stringfilter
def first_letter_in_ascii(value):
    letter = value[0]
    new_letter = unidecode(letter).upper()
    return new_letter.upper()


@register.filter
@stringfilter
def get_volume_in_progress_msg(year):
    now = timezone.now()
    curyear = str(now.year)
    if curyear == year:
        return translate_text("Articles du volume en cours")
    else:
        return translate_text("Articles du volume")


@register.filter
def is_volume_with_numbers(value):
    is_with_numbers = True

    if "-" in value:
        parts = value.split("-")
        for part in parts:
            try:
                int(part)
            except ValueError:
                is_with_numbers = False
    else:
        try:
            int(value)
        except ValueError:
            is_with_numbers = False

    return is_with_numbers
