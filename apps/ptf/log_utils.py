"""
See :
    https://stackoverflow.com/questions/2052284/how-to-throttle-django-error-emails
    https://github.com/jedie/django-tools/blob/main/django_tools/log_utils/throttle_admin_email_handler.py
"""
# -*- coding: utf-8 -*-

from django.core.cache import cache
from django.utils.log import AdminEmailHandler


class ThrottledAdminEmailHandler(AdminEmailHandler):
    PERIOD_LENGTH_IN_SECONDS = 60
    MAX_EMAILS_IN_PERIOD = 25
    COUNTER_CACHE_KEY = "email_admins_counter"

    def __init__(self, include_html=False, email_backend=None, reporter_class=None):
        super().__init__()

    def increment_counter(self):
        try:
            cache.incr(self.COUNTER_CACHE_KEY)
        except ValueError:
            cache.set(self.COUNTER_CACHE_KEY, 1, self.PERIOD_LENGTH_IN_SECONDS)
        return cache.get(self.COUNTER_CACHE_KEY)

    def emit(self, record):
        try:
            counter = self.increment_counter()
        except Exception:
            pass
        else:
            if counter > self.MAX_EMAILS_IN_PERIOD:
                return
        super().emit(record)
