import datetime
import re
import string
from urllib.parse import quote
from urllib.parse import quote_plus
from urllib.parse import urlparse

from pysolr import Results

from django.conf import settings
from django.http import Http404
from django.http import QueryDict
from django.urls import reverse
from django.utils import translation

from ptf.display import resolver
from ptf.site_register import SITE_REGISTER


######################################################################
# CLEANSEARCHURL
######################################################################
class CleanSearchURL:
    """
    CleanSearchURL: url like search /search/*-"ma collection"-qp
    first part of the url is the path followed by criterias and last part is the encoding of criteria
    criterias are split by CleanSearchURL.separator
    """

    separator = "-"

    def __init__(self, base):
        self._base = base
        self._criteria = []
        self._encoding = []

    def append(self, criteria, type):
        if len(self._criteria) != len(self._encoding):
            raise
        self._criteria.append(criteria)
        self._encoding.append(type)

    def remove(self, criteria, type):
        """
        9/03/2023 - UNUSED.
        The class is used as an object only once in `pretty_search` method (helpers.py).
        """
        if len(self._criteria) != len(self._encoding):
            raise
        self._criteria.remove(criteria)
        self._encoding.remove(type)

    @staticmethod
    def decode(clean_url: str, path="") -> tuple[str, QueryDict]:
        """
        decode a pretty search url encoded like :
                    search   /1erterme-2erterme -   3   - Nom          -  1986     -abpfg
                    path     all      + author  + page  + facetAuteur  + facetDate +manière dont est encodée la requete



             Attention : pour les recherche en NOT, la lettre est en majuscule
                    "q" : qti = all
                    "a" : qti = author
                    "b" : qti = titre
                    "c" : qti = date
                    "d" : first date/last date formulaire de recherche
                    "f" : bibliographie
                    "g" : plein texte
                    "k" : qti = keywords
                    "x" : qti = abstract (Résumé)
                    i-m : reservé pour la recherche en cas d'ajout de champs
                    "n": facet auteur
                    "o": facet year range
                    "p": facet collection
                    "r":firstletter
                    "s": facet type document
                    "t":page
                    "u": classname (article type)
                    "y": facet year


        @param clean_url : critere(s) et encoding séparé par des - ( CleanSearchURL.separator )
        @param path : chemin de la recherche car peut être : /search /thesis /series etc.
        @return path, QueryDict: QueryDict : dict du type qt0: all, q0: 'larecherche' etc.
        """
        q_index = 0
        my_dict = {
            "q": ["q{}={}&qt{}=all", r".*"],
            "a": ["q{}={}&qt{}=author", r"\D*"],
            "b": ["q{}={}&qt{}=title", r".*"],
            "c": ["q{}={}&qt{}=author_ref", r"\D*"],
            "d": ["q-f-{}={}&q-l-{}={}&qt{}=date", r"\[(\d{4}|\*|) TO (\d{4}|\*|)\]"],
            "f": ["q{}={}&qt{}=references", r".*"],
            "g": ["q{}={}&qt{}=body", ".*"],
            "k": ["q{}={}&qt{}=kwd", ".*"],
            "m": ["f=msc_facet:{}", r".*"],
            "n": ["f=ar:{}", r"\D*"],
            "o": ["f=year_facet:{}", r"\[\d{4} TO \d{4}\]"],
            "p": ["f=collection_title_facet:{}", r".*"],
            # "r": ["f={{!tag=firstletter}}firstNameFacetLetter:{}", r'\"?[A-Z]{1}\"?'],
            # FIXME : a supprimer après sûr que les " autour de firstLetter ne sont pas nécessaires
            "r": ["f={{!tag=firstletter}}firstNameFacetLetter:{}", r"[A-Z]{1}"],
            "s": ["f=dt:{}", r".*"],
            "t": ["page={}", r"\d*"],
            "u": ["f=classname:{}", r".*"],
            "x": ["q{}={}&qt{}=abstract", r".*"],
            "y": ["f=year_facet:{}", r"\"\d{4}\""],
        }

        criteria = []
        chaine = ""
        inQuote = False
        # on itère sur clean_url pour séparer les critères en reconnaissant des chaînes de caractères protégées
        # par des "
        #
        try:
            for i in range(len(clean_url)):
                c = clean_url[i]
                # print(c)
                if inQuote or c != CleanSearchURL.separator:
                    chaine += c

                if (
                    c == '"'
                    and (i == 0 or clean_url[i - 1] == CleanSearchURL.separator)
                    and inQuote is False
                ):
                    # Debut de critere entre quote
                    inQuote = True
                elif c == '"' and clean_url[i + 1] == CleanSearchURL.separator and inQuote is True:
                    # Fin de critere entre quote
                    criteria.append(chaine)
                    inQuote = False
                    chaine = ""
                elif (
                    c == CleanSearchURL.separator and inQuote is False and clean_url[i - 1] != '"'
                ):
                    # Fin de critere sans quote et le critère n'était pas entouré de quote
                    criteria.append(chaine)
                    chaine = ""

            criteria.append(chaine)

            # encodage est le dernier critere
            encoding = criteria[-1]
            criteria = criteria[:-1]

            encoding = list(encoding)
            query = zip(encoding, criteria)
            query_string = ""
            # pour chaque critere, on crée la requête orientée solr associée
            for encoding_key, criteria_value in query:
                if criteria_value != "":
                    # on test si le critere respecte la regexp associée
                    reg_str = my_dict[encoding_key.lower()]
                    p = re.compile(reg_str[1])
                    if p.match(criteria_value):
                        # criteria_value must be url encoded to pass to QueryDict
                        if encoding_key.lower() in ["d"]:
                            # on traite un intervalle de date
                            begin, end = criteria_value.strip("[]").split(" TO ")
                            query_string += "&" + my_dict[encoding_key.lower()][0].format(
                                q_index, begin, q_index, end, q_index
                            )
                        elif encoding_key.lower() in ["q", "a", "b", "c", "f", "k", "g", "x"]:
                            query_string += "&" + (
                                my_dict[encoding_key.lower()][0].format(
                                    q_index, quote_plus(criteria_value), q_index
                                )
                            )
                            if encoding_key.lower() != encoding_key:
                                # on est dans le cas d'un NOT -> la clef est en
                                # majuscule
                                query_string += f"&not{q_index}=on"
                            q_index += 1
                        else:
                            query_string += "&" + (
                                my_dict[encoding_key.lower()][0].format(quote_plus(criteria_value))
                            )

            querydict = QueryDict(query_string.encode("utf-8"))
            return path, querydict
        except Exception:
            raise Http404()

    @staticmethod
    def encode(dict: QueryDict, path="") -> str:
        """
        encode QueryDict request in CleanURL
        @param QueryDict: POST request from search form
        @return: clean search absolute url
        """

        criteria = []
        encoding = []
        # a priori les filtres seront passés en GET uniquement
        # filters = []
        # filters = request.POST.getlist('f')

        i = 0
        qti = dict.get("qt" + str(i), None)

        while qti:
            qi = dict.get("q" + str(i), None)
            if qti == "all":
                criteria.append(qi)
                encoding.append("q")
            elif qti == "author":
                criteria.append(qi)
                encoding.append("a")
            elif qti == "author_ref":
                criteria.append(qi)
                encoding.append("c")
            elif qti == "title":
                criteria.append(qi)
                encoding.append("b")
            elif qti == "date":
                qfi = dict.get("q-f-" + str(i), "*")
                qli = dict.get("q-l-" + str(i), "*")
                criteria.append(f"[{qfi} TO {qli}]")
                encoding.append("d")
            elif qti == "references":
                criteria.append(qi)
                encoding.append("f")
            elif qti == "body":
                criteria.append(qi)
                encoding.append("g")
            elif qti == "kwd":
                criteria.append(qi)
                encoding.append("k")
            elif qti == "abstract":
                criteria.append(qi)
                encoding.append("x")
            # if qti == 'author_ref':
            #     keep_qs_in_display = False

            noti = dict.get("not" + str(i), None)
            if noti == "on":
                encoding[len(encoding) - 1] = encoding[len(encoding) - 1].upper()

            i += 1
            qti = dict.get("qt" + str(i), None)

        # on traite les filtres
        # "n": "f=ar:'{}'",
        # "o": "f=year_facet:'{}'",
        # "y": "f=year_facet:'{}'",
        # "p": "f=collection_title_facet:'{}'",
        # "r": "f={!tag=firstletter}firstNameFacetLetter:'{}'",
        # "s": "f=dt:'{}'",
        # "u": "f=classname:'{}'",
        filters = dict.getlist("f")
        for filter in filters:
            key, value = filter.split(":", 1)
            if key == "collection_title_facet":
                criteria.append(value)
                encoding.append("p")
            elif key == "ar":
                criteria.append(value)
                encoding.append("n")
            elif key == "year_facet":
                criteria.append(value)
                if value[0] == "[":
                    encoding.append("o")
                else:
                    encoding.append("y")
            elif key == "{!tag=firstletter}firstNameFacetLetter":
                criteria.append(value)
                encoding.append("r")
            elif key == "dt":
                criteria.append(value)
                encoding.append("s")
            elif key == "classname":
                # Used for article types
                criteria.append(value)
                encoding.append("u")
            elif key == "msc_facet":
                criteria.append(value)
                encoding.append("m")

        # on traite la  pagination
        # "t": "page={}"
        page = dict.get("page")
        if page:
            criteria.append(page)
            encoding.append("t")

        if not criteria:
            return path
        for i in range(len(criteria)):
            if criteria[i] and criteria[i][0] == '"' and criteria[i][-1] == '"':  # critere protege
                pass
            elif CleanSearchURL.separator in criteria[i] or '"' in criteria[i]:
                criteria[i] = f'"{criteria[i]}"'

        clean_url = "".join(
            [
                CleanSearchURL.separator.join(quote(item, "") for item in criteria),
                CleanSearchURL.separator,
                "".join(encoding),
            ]
        )
        path = path.strip("/")
        if path:
            return "/" + path + "?" + clean_url
        return clean_url

    def to_href(self):
        clean_url = (
            self._base
            + "/"
            + self.separator.join(quote(item, "") for item in self._criteria)
            + self.separator
            + "".join(self._encoding)
        )
        return clean_url


######################################################################
# FACETS & SEARCH RESULTS
######################################################################
class Facet:
    """
    Facet: a filter that you can select to narrow your search
    Example: "Journal article(25)" is a filter (of the colid_facets category)

    properties:
        name     Ex: "Journal article"
        count    Ex: 25
        active   Ex: True    (tells if the user has selected the filter)
        href     The url to set on the filter.
                 It concatenates the filters selected before
                 Ex: http://www.numdam.org/items/?q=au:(choquet)&f=dt:%22Journal%20article%22&f=year:1991

    Facets are returned in the SearchResultsGenerator
    """

    def __init__(
        self,
        name: str,
        count: int,
        state: str,
        filters: set[str] = set(),
        path="/search/",
        sub_facets=[],
    ):
        self.name = name
        self.count = count
        self.active = state
        self.sub_facets = sub_facets
        if filters:
            query = "&f=".join([quote_plus(x) for x in filters])
            # query est du type /search/?q=test&qt=all&f=....
            query = f"{path}&f={query}"
        else:
            query = path
        url = urlparse(query)
        params = url.query
        params = params.encode("utf-8")
        dict = QueryDict(params, True, "utf-8")
        href = CleanSearchURL.encode(dict, url.path)
        if hasattr(settings, "SITE_URL_PREFIX"):
            href = f"/{settings.SITE_URL_PREFIX}" + href
        self.href = href


def create_facets_in_category(
    solr_results: Results,
    category: str,
    active_filters: set[str],
    path: str,
    sort=False,
    reverse=False,
) -> list[Facet]:
    # Solr returns filters in a flat list
    # Example: facet_fields : { "year": [ "1878",1,"1879",0,"1912",3,"1971",5] }
    # To simplify the creation of Facet objects, we need a list of pairs: [ ('1878',1),('1879',2),...]
    # To do so, we use
    # 1) The python slice syntax on lists [start:stop:step]
    #         f[0::2] => [ '1878', '1879',...]
    #         f[1::2] => [1,2,...]
    # 2) The python list comprehensions [ expression for ... if ... ]
    #         [f[i::2 for i in range(2)] => [ ['1878','1879',...], [1,2,...] ]
    # 3) zip(*list) to unzip a list (see more details below)
    #        => [ ('1878',1), ('1879',2), ... ]
    #
    # zip(*list) <=> unzip.  Why ?
    #   zip() pairs up the elements from all inputs
    #   zip( lista, listb, ... listz ) => ( (a1,b1,...z1), (a2,b2,...,z2), ..., (an,bn,...,zn) )
    #   The output is a tuple (unmutable list)
    #   To recreate the lista, listb, you can re-apply zip on the elements of the tuple.
    #   But you have to unpack the tuple first (to recreate multiple arguments)
    #   *(tuple) creates multiple (ai,bi,...zi) lists
    #   zip(*tuple) combines the list
    #   The output is ( (a1,a2,...,an), (b1,b2,...bn), ..., (z1,z2,...,zn) )

    if category not in solr_results.facets["facet_fields"]:
        return []

    f = solr_results.facets["facet_fields"][category]
    solr_facets = list(zip(*[f[i::2] for i in range(2)]))

    if sort:
        solr_facets = sorted(solr_facets, key=lambda x: x[0], reverse=reverse)

    results = []
    active_filters = active_filters.copy()

    if category == "year_facet":
        # Selecting a year facet clears the active year range facet (if any)
        mylist = [v for v in active_filters if "year_facet:[" in v]
        if mylist:
            active_filters.remove(mylist[0])

    if category == "ar":
        my_list = [v for v in active_filters if "ar:" in v]
        if my_list:
            ar_active_filter = my_list[0]
        else:
            ar_active_filter = None

    for facet_name, count in solr_facets:
        this_filters = active_filters.copy()
        v = '{}:"{}"'.format(category, facet_name.replace('"', '\\"'))
        if category == "sites":
            facet_name = [
                SITE_REGISTER[key]["name"]
                for key in SITE_REGISTER
                if str(SITE_REGISTER[key]["site_id"]) == facet_name
            ][0]
        if v in active_filters:
            this_filters.remove(v)
            results.append(Facet(facet_name, count, "active", this_filters, path))
        else:
            # on n'autorise pas la multiple selection de facet auteur
            if category == "ar" and ar_active_filter is not None:
                this_filters.remove(ar_active_filter)
            this_filters.add(v)
            results.append(Facet(facet_name, count, "not-active", this_filters, path))

    return results


def create_year_range_facets(
    solr_results: Results, year_facets: list[Facet], active_filters: set[str], path: str
) -> list[Facet]:
    gap = solr_results.facets["facet_ranges"]["year_facet"]["gap"]
    f = solr_results.facets["facet_ranges"]["year_facet"]["counts"]
    solr_facets = list(zip(*[f[i::2] for i in range(2)]))

    solr_facets = sorted(solr_facets, key=lambda x: x[0], reverse=True)

    results = []

    now = datetime.datetime.now()
    i = 0  # current year_facet index
    year_facets_size = len(year_facets)

    for facet_name, count in solr_facets:
        start = facet_name
        start_i = int(start)
        end_i = int(facet_name) + gap - 1
        end = str(end_i)

        if end_i > now.year:
            end = str(now.year)

        # year_facets become sub_facets of a year_range_facet
        # We need to find the year_facets that are inside the year_range_facet
        if i < year_facets_size:
            yf = year_facets[i]
            year = int(yf.name)

        sub_year_facets = []
        this_filters = active_filters.copy()

        while i < year_facets_size and year >= start_i:
            sub_year_facets.append(yf)

            # If we click on a year range facet, we clear the active year facet
            # (if any)
            v = 'year_facet:"' + yf.name + '"'
            if v in active_filters:
                this_filters.remove(v)

            i += 1
            if i < year_facets_size:
                yf = year_facets[i]
                year = int(yf.name)

        facet_name = facet_name + "-" + str(end)
        v = "year_facet:[" + start + " TO " + end + "]"

        if v in active_filters:
            this_filters.remove(v)
            results.append(Facet(facet_name, count, "active", this_filters, path, sub_year_facets))
        else:
            this_filters.add(v)
            results.append(
                Facet(facet_name, count, "not-active", this_filters, path, sub_year_facets)
            )

    return results


def create_facets(
    solr_results: Results, path: str, filters: list[str], use_ar_facet=True
) -> dict[str, list[Facet]]:
    active_filters = set(filters)

    atype_facets = create_facets_in_category(solr_results, "classname", active_filters, path)
    author_facets = []
    if use_ar_facet:
        author_facets = create_facets_in_category(solr_results, "ar", active_filters, path)
    dt_facets = create_facets_in_category(solr_results, "dt", active_filters, path)
    msc_facets = create_facets_in_category(solr_results, "msc_facet", active_filters, path)
    collection_facets = create_facets_in_category(
        solr_results, "collection_title_facet", active_filters, path
    )
    sites_facets = create_facets_in_category(solr_results, "sites", active_filters, path)

    year_facets = create_facets_in_category(
        solr_results, "year_facet", active_filters, path, sort=True, reverse=True
    )
    if len(year_facets) == 1 and year_facets[0].active == "active":
        year_range_facets = year_facets
    else:
        year_range_facets = create_year_range_facets(
            solr_results, year_facets, active_filters, path
        )

    return {
        "author_facets": author_facets,
        "msc_facets": msc_facets,
        "year_range_facets": year_range_facets,
        "dt_facets": dt_facets,
        "atype_facets": atype_facets,
        "collection_facets": collection_facets,
        "sites_facets": sites_facets,
    }


class SearchResults:
    """
    Search results.
    Hold data returned by Solr
    Intermediary between solr_results and the Django template to display the results
    """

    # def __init__(self, solr_results, path, filters, sort): -> si activation
    # du tri

    def fix_truncated_value(self, value: str):
        """
        Highlighting may produce an HTML string truncated at the end.
        To display the search keywords in bold, we add <strong> around them.
        But we ask the template to display the highlight as |safe such that
        unclosed HTML tags will damage the HTML page layout.
        => fix_trunctated_value attempt to add missing HTML end tags.

        9/03/2023 - This cannot work properly. We should use a parser or something
        to correctly do this.
        """
        keywords = []
        i = 0
        quote = ""
        while i < len(value):
            if value[i] == '"':
                if quote == '"':
                    quote = ""
                else:
                    quote = '"'
            elif value[i] == "'":
                if quote == "'":
                    quote = ""
                else:
                    quote = "'"

            keyword = ""
            end_keyword = False
            if not quote and value[i] == "<":
                i += 1

                if i < len(value) and value[i] == "/":
                    end_keyword = True
                    i += 1

                while i < len(value) and value[i] != " " and value[i] != ">":
                    keyword += value[i]
                    i += 1

                if keyword and end_keyword:
                    if len(keywords) > 0 and keywords[-1] == keyword:
                        keywords.pop(-1)
                elif keyword:
                    keywords.append(keyword)

            i += 1

        if quote:
            value += quote

        while len(keywords) > 0:
            keyword = keywords.pop(-1)
            value += "</" + keyword + ">"

        return value

    def __init__(
        self,
        solr_results: Results,
        path: str,
        filters: list[str],
        qt: list[str],
        use_ar_facet=True,
    ):
        self.facets = create_facets(solr_results, path, filters, use_ar_facet)
        self.hits = solr_results.hits
        self.docs = solr_results.docs

        cur_language = translation.get_language()
        preferred_highlight_keywords = [
            "abstract_tex",
            "trans_abstract_tex",
            "kwd",
            "trans_kwd",
            "body",
            "bibitem",
        ]
        if cur_language != "fr":
            preferred_highlight_keywords = [
                "trans_abstract_tex",
                "abstract_tex",
                "trans_kwd",
                "kwd",
                "body",
                "bibitem",
            ]

        # References is bibitem
        qt = [s.replace("references", "bibitem") for s in qt]

        if any("kwd" in s for s in qt) and not any("trans_kwd" in s for s in qt):
            qt.append("trans_kwd")
        elif any("trans_kwd" in s for s in qt) and not any("kwd" in s for s in qt):
            qt.append("kwd")

        if any("abstract_tex" in s for s in qt) and not any("trans_abstract_tex" in s for s in qt):
            qt.append("trans_abstract_tex")
        elif any("trans_abstract_tex" in s for s in qt) and not any(
            "abstract_tex" in s for s in qt
        ):
            qt.append("abstract_tex")

        # We do not call the translation mechanism on a specific language
        # try:
        #     translation.activate('en')
        #     text = translation.gettext(u"Résumé")
        # finally:
        #     translation.activate(cur_language)

        # We get the translation based on the current language
        abstract_text = translation.gettext("Résumé")
        reference_text = translation.gettext("Bibliographie")
        keywords_text = translation.gettext("Mots clés")
        fulltext_text = translation.gettext("Plein texte")

        correspondance = {
            "abstract_tex": abstract_text,
            "trans_abstract_tex": abstract_text,
            "kwd": keywords_text,
            "trans_kwd": keywords_text,
            "body": fulltext_text,
            "bibitem": reference_text,
        }

        for index, doc in enumerate(self.docs):
            id_doc = doc["id"]
            doc["embargo"] = resolver.embargo(doc["wall"], doc["year"])
            hl = solr_results.highlighting[id_doc]
            for key in ["au", "year"]:
                if key in hl:
                    the_hl = hl[key][0]
                    the_hl = the_hl.replace("<strong>", "")
                    the_hl = the_hl.replace("</strong>", "")
                    value = doc[key]
                    pos = value.find(the_hl)
                    if pos > -1:
                        value = value.replace(the_hl, hl[key][0])

                    doc[key] = value

            for key in ["collection_title_tex", "collection_title_html"]:
                value = doc[key][0]
                if key in hl:
                    the_hl = hl[key][0]
                    the_hl = the_hl.replace("<strong>", "")
                    the_hl = the_hl.replace("</strong>", "")
                    pos = value.find(the_hl)
                    if pos > -1:
                        value = value.replace(the_hl, hl[key][0])

                doc[key] = value

            for key in preferred_highlight_keywords:
                doc["highlighting"] = {}
                doc["highlighting"]["value"] = ""
                if key in hl and ("all" in qt or key in qt):
                    doc["highlighting"]["field"] = correspondance[key]
                    for value in hl[key]:
                        if key == "bibitem":
                            value = self.fix_truncated_value(value)
                        doc["highlighting"]["value"] = (
                            doc["highlighting"]["value"] + "... " + value + " ...<br>"
                        )
                    break
                    # TODO: on ne veut le hl sur bibitem voire plein text que
                    # si il n'y a que ca qui matche
            if settings.SITE_NAME == "cr" and "sites" in doc and doc["sites"]:
                site_id = doc["sites"][0]
                site_domain = [
                    SITE_REGISTER[key]["site_domain"]
                    for key in SITE_REGISTER
                    if SITE_REGISTER[key]["site_id"] == site_id
                ][0]
                prefix = site_domain.split("/")[1]
                if "doi" in doc:
                    url = reverse("article", kwargs={"aid": doc["doi"]})
                else:
                    url = reverse("item_id", kwargs={"pid": doc["pid"]})
                doc_url = "/" + prefix + url
                doc["item_url"] = doc_url
                if doc["pdf"].find("/" + prefix) != 0:
                    doc["pdf"] = "/" + prefix + doc["pdf"]
                    if "tex" in doc:
                        doc["tex"] = "/" + prefix + doc["tex"]
            elif hasattr(settings, "SITE_URL_PREFIX"):
                if doc["pdf"].find("/" + settings.SITE_URL_PREFIX) != 0:
                    doc["pdf"] = "/" + settings.SITE_URL_PREFIX + doc["pdf"]
                    if "tex" in doc:
                        doc["tex"] = "/" + settings.SITE_URL_PREFIX + doc["tex"]

            self.docs[index] = doc

        self.filters = "&f=".join(filters)
        # self.sort = sort  -> si activation du tri


class SearchInternalResults:
    """
    Search results for sorted Books.
    Hold data returned by Solr
    Intermediary between solr_results and the Django template to display the results
    """

    # def __init__(self, solr_results, path, filters, sort): -> si activation
    # du tri
    def __init__(
        self, solr_results: Results, path: str, filters: list[str], facet_fields: list[str]
    ):
        year_range_facets = None
        letter_facets = None
        collection_facets = None
        author_facets = None

        firstletterFilter = ""
        this_filters = set(filters).copy()

        if "collection_title_facet" in facet_fields:
            collection_facets = create_facets_in_category(
                solr_results, "collection_title_facet", this_filters, path
            )

        if "author_facet" in facet_fields:
            author_facets = create_facets_in_category(solr_results, "ar", this_filters, path)

        if "year_facet" in facet_fields:
            year_facets = create_facets_in_category(
                solr_results, "year_facet", this_filters, path, sort=True, reverse=True
            )
            if len(year_facets) == 1 and year_facets[0].active == "active":
                year_range_facets = year_facets
            else:
                year_range_facets = create_year_range_facets(
                    solr_results, year_facets, this_filters, path
                )

        if "firstLetter" in facet_fields:
            for filter in filters:
                if filter.startswith("{!tag=firstletter}firstNameFacetLetter:"):
                    this_filters.remove(filter)
                    firstletterFilter = filter

            f = solr_results.facets["facet_fields"]["firstNameFacetLetter"]
            solr_facets = dict(zip(f[0::2], f[1::2]))

            letter_facets = []
            no_letter_selected = True
            for lettre in string.ascii_uppercase:
                v = f"{{!tag=firstletter}}firstNameFacetLetter:{lettre}"
                if lettre in solr_facets and v == firstletterFilter:
                    # on est dans le cas où la lettre est dans les résultats de
                    # recherche et le filtre est activé
                    letter_facets.append(
                        Facet(lettre, solr_facets[lettre], "active", this_filters, path)
                    )
                    no_letter_selected = False
                elif lettre in solr_facets:
                    my_filters = this_filters.copy()
                    my_filters.add(v)
                    letter_facets.append(
                        Facet(lettre, solr_facets[lettre], "not-active", my_filters, path)
                    )
                else:
                    letter_facets.append(Facet(lettre, 0, "disabled", "", path))
            if no_letter_selected:
                letter_facets.append(Facet("All", 1, "active", this_filters, path))
            else:
                letter_facets.append(Facet("All", 1, "not-active", this_filters, path))

        self.facets = {
            "year_range_facets": year_range_facets,
            "letter_facets": letter_facets,
            "collection_title_facets": collection_facets,
            "author_facets": author_facets,
        }

        self.hits = solr_results.hits
        self.docs = solr_results.docs
        self.filters = "&f=".join(filters)
        # self.sort = sort  -> si activation du tri

        for index, doc in enumerate(self.docs):
            doc["embargo"] = resolver.embargo(doc["wall"], doc["year"])
            self.docs[index] = doc
