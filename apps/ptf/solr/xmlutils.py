from lxml import etree


def escape(s):
    return s.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")


def innerxml(node, mixed=True):
    # traiter &
    if mixed:
        if node.text:
            parts = [escape(node.text)] + [
                etree.tostring(c, encoding="unicode") for c in node.getchildren()
            ]
        else:
            parts = [etree.tostring(c, encoding="unicode") for c in node.getchildren()]
    else:
        parts = [
            etree.tostring(c, encoding="unicode", with_tail=False) for c in node.getchildren()
        ]
    return "".join(parts).strip().encode("utf-8")
