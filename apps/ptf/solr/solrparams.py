class SolrRequest:
    def __init__(self, request, q="", alias=None, site=None, default={}):
        if not q:
            self.q = request.GET.get("q", None)
        else:
            self.q = q
        self.filters = request.GET.getlist("f")
        self.ifilters = []  # filtres implicites
        self.path = alias or request.path
        self.site = site
        self.params = get_params(request, default=default)

    def add_implicit_filter(self, filter):
        self.ifilters.append(filter)


class FilterRequest:
    def __init__(self, request, filters, site=None, default={}):
        self.q = None
        self.filters = filters
        self.ifilters = []  # filtres implicites
        self.path = "/select/"
        self.site = site
        self.params = get_params(request, default=default)


def get_params(request, default={}):
    try:
        page = int(request.GET.get("page", 1))
    except BaseException:
        page = 1
    rows = 20
    start = (page - 1) * rows
    sort = request.GET.get("order_by", default.get("sort", None))
    params = {"page": page, "start": start, "rows": rows}
    if sort is not None:
        solr_sort = []
        sort = sort.split(",")
        for spec in sort:
            spec = spec.strip()
            if spec[0] == "-":
                spec = f"{spec[1:]} desc"
            else:
                spec = f"{spec} asc"
            solr_sort.append(spec)
        params["sort"] = ", ".join(solr_sort)
    params["fl"] = ["id"]
    return params
