SITE_REGISTER = {
    "aif": {
        "site_id": 1,
        "collection_pid": "AIF",
        "site_domain": "aif.centre-mersenne.org",
        "en_only": False,
        "licences": [(2017, "CC-BY-ND 4.0")],
    },
    "ptf_tools": {
        "site_id": 2,
        "collection_pid": "ALL",
        "site_domain": "trammel.centre-mersenne.org",
        "en_only": False,
    },
    "numdam": {
        "site_id": 3,
        "collection_pid": "ALL",
        "site_domain": "www.numdam.org",
        "en_only": False,
    },
    "alco": {
        "site_id": 4,
        "collection_pid": "ALCO",
        "site_domain": "alco.centre-mersenne.org",
        "en_only": True,
        "licences": [(2018, "CC-BY 4.0")],
    },
    "ahl": {
        "site_id": 5,
        "collection_pid": "AHL",
        "site_domain": "ahl.centre-mersenne.org",
        "en_only": True,
        "licences": [(2018, "CC-BY 4.0")],
    },
    "centre_mersenne": {
        "site_id": 6,
        "collection_pid": "MERSENNE",
        "site_domain": "centre-mersenne.org",
        "en_only": False,
    },
    "ogeo": {
        "site_id": 7,
        "collection_pid": "OGEO",
        "site_domain": "opengeomechanics.centre-mersenne.org",
        "en_only": True,
        "licences": [(2019, "CC-BY-NC-SA 4.0")],
    },
    "jep": {
        "site_id": 8,
        "collection_pid": "JEP",
        "site_domain": "jep.centre-mersenne.org",
        "en_only": False,
        "licences": [(2014, "CC-BY-ND 4.0"), (2019, "CC-BY 4.0")],
    },
    "smai": {
        "site_id": 9,
        "collection_pid": "SMAI-JCM",
        "site_domain": "smai-jcm.centre-mersenne.org",
        "en_only": True,
        "licences": [(2017, "CC-BY-NC-ND 4.0"), (2023, "CC-BY 4.0")],
    },
    "gdml": {
        "site_id": 10,
        "collection_pid": "ALL",
        "site_domain": "http://dml.mathdoc.fr",
        "en_only": False,
    },
    "cml": {
        "site_id": 11,
        "collection_pid": "CML",
        "site_domain": "cml.centre-mersenne.org",
        "en_only": False,
        "licences": [(2017, "CC-BY-NC-ND 4.0")],
    },
    "pmb": {
        "site_id": 13,
        "collection_pid": "PMB",
        "site_domain": "pmb.centre-mersenne.org",
        "en_only": False,
        "licences": [(2017, "CC-BY-ND 4.0")],
    },
    "afst": {
        "site_id": 14,
        "collection_pid": "AFST",
        "site_domain": "afst.centre-mersenne.org",
        "default_lang": "en",
        "en_only": False,
        "licences": [(2017, "CC-BY 4.0")],
    },
    "jtnb": {
        "site_id": 15,
        "collection_pid": "JTNB",
        "site_domain": "jtnb.centre-mersenne.org",
        "en_only": False,
        "licences": [(2017, "CC-BY-ND 4.0")],
    },
    "ambp": {
        "site_id": 16,
        "collection_pid": "AMBP",
        "site_domain": "ambp.centre-mersenne.org",
        "en_only": False,
        "licences": [(2017, "CC-BY 4.0")],
    },
    "msia": {
        "site_id": 17,
        "collection_pid": "MSIA",
        "site_domain": "msia.centre-mersenne.org",
        "en_only": False,
        "licences": [(2017, "CC-BY 4.0")],
    },
    "acirm": {
        "site_id": 18,
        "collection_pid": "ACIRM",
        "site_domain": "acirm.centre-mersenne.org",
        "en_only": False,
    },
    "ccirm": {
        "site_id": 19,
        "collection_pid": "CCIRM",
        "site_domain": "ccirm.centre-mersenne.org",
        "en_only": False,
    },
    "jedp": {
        "site_id": 20,
        "collection_pid": "JEDP",
        "site_domain": "jedp.centre-mersenne.org",
        "en_only": False,
    },
    "tsg": {
        "site_id": 21,
        "collection_pid": "TSG",
        "site_domain": "tsg.centre-mersenne.org",
        "en_only": False,
    },
    "slsedp": {
        "site_id": 22,
        "collection_pid": "SLSEDP",
        "site_domain": "slsedp.centre-mersenne.org",
        "en_only": False,
    },
    "wbln": {
        "site_id": 23,
        "collection_pid": "WBLN",
        "site_domain": "wbln.centre-mersenne.org",
        "en_only": False,
    },
    "mbk": {
        "site_id": 24,
        "collection_pid": "MALSM",
        "site_domain": "books.centre-mersenne.org",
        "en_only": False,
    },
    "ojmo": {
        "site_id": 25,
        "collection_pid": "OJMO",
        "site_domain": "ojmo.centre-mersenne.org",
        "en_only": True,
        "licences": [(2019, "CC-BY 4.0")],
    },
    "crmath": {
        "site_id": 26,
        "collection_pid": "CRMATH",
        "site_domain": "comptes-rendus.academie-sciences.fr/mathematique",
        "en_only": False,
        "licences": [(2020, "CC-BY 4.0")],
        "name": "Mathématique",
        "email_from": "no-reply@listes.mathdoc.fr",
    },
    "crchim": {
        "site_id": 27,
        "collection_pid": "CRCHIM",
        "site_domain": "comptes-rendus.academie-sciences.fr/chimie",
        "en_only": False,
        "licences": [(2020, "CC-BY 4.0")],
        "name": "Chimie",
        "email_from": "no-reply@listes.mathdoc.fr",
    },
    "crphys": {
        "site_id": 28,
        "collection_pid": "CRPHYS",
        "site_domain": "comptes-rendus.academie-sciences.fr/physique",
        "en_only": False,
        "licences": [(2020, "CC-BY 4.0")],
        "name": "Physique",
        "email_from": "no-reply@listes.mathdoc.fr",
    },
    "crmeca": {
        "site_id": 29,
        "collection_pid": "CRMECA",
        "site_domain": "comptes-rendus.academie-sciences.fr/mecanique",
        "en_only": False,
        "licences": [(2020, "CC-BY 4.0")],
        "name": "Mécanique",
        "email_from": "no-reply@listes.mathdoc.fr",
    },
    "crbiol": {
        "site_id": 30,
        "collection_pid": "CRBIOL",
        "site_domain": "comptes-rendus.academie-sciences.fr/biologies",
        "en_only": False,
        "licences": [(2020, "CC-BY 4.0")],
        "name": "Biologies",
        "email_from": "no-reply@listes.mathdoc.fr",
    },
    "crgeos": {
        "site_id": 31,
        "collection_pid": "CRGEOS",
        "site_domain": "comptes-rendus.academie-sciences.fr/geoscience",
        "en_only": False,
        "licences": [(2020, "CC-BY 4.0")],
        "name": "Géoscience",
        "email_from": "no-reply@listes.mathdoc.fr",
    },
    "roia": {
        "site_id": 32,
        "collection_pid": "ROIA",
        "site_domain": "roia.centre-mersenne.org",
        "en_only": False,
        "fr_only": True,
        "licences": [(2021, "CC-BY 4.0")],
    },
    "mrr": {
        "site_id": 33,
        "collection_pid": "MRR",
        "site_domain": "mrr.centre-mersenne.org",
        "en_only": True,
        "licences": [(2021, "CC-BY 4.0")],
    },
    "cr": {
        "site_id": 34,
        "collection_pid": "CR",
        "site_domain": "comptes-rendus.academie-sciences.fr",
        "en_only": False,
    },
    "malsm": {
        "site_id": 35,
        "collection_pid": "MBK",
        "site_domain": "books.centre-mersenne.org",
        "en_only": False,
    },
    "pcj": {
        "site_id": 36,
        "collection_pid": "PCJ",
        "site_domain": "pcj.centre-mersenne.org",
        "en_only": True,
        "licences": [(2021, "CC-BY 4.0")],
    },
    "art": {
        "site_id": 37,
        "collection_pid": "ART",
        "site_domain": "art.centre-mersenne.org",
        "en_only": True,
        "licences": [(2018, "CC-BY 4.0")],
    },
    "proceedings": {
        "site_id": 38,
        "collection_pid": "PROCEEDINGS",
        "site_domain": "proceedings.centre-mersenne.org",
        "en_only": False,
    },
    "igt": {
        "site_id": 39,
        "collection_pid": "IGT",
        "site_domain": "igt.centre-mersenne.org",
        "en_only": True,
        "licences": [(2023, "CC-BY 4.0")],
    },
    "xups": {
        "site_id": 40,
        "collection_pid": "XUPS",
        "site_domain": "xups.centre-mersenne.org",
        "en_only": False,
    },
    "sms": {
        "site_id": 100,
        "collection_pid": "SMS",
        "site_domain": "SMS - pour test",
        "en_only": False,
    },
    "crasmath": {
        "site_id": 101,
        "collection_pid": "CRASMATH",
        "site_domain": "crasmath.centre-mersenne.org",
        "en_only": False,
    },
    "craschim": {
        "site_id": 102,
        "collection_pid": "CRASCHIM",
        "site_domain": "craschim.centre-mersenne.org",
        "en_only": False,
    },
    "cg": {
        "site_id": 103,
        "collection_pid": "CG",
        "site_domain": "unknown",
        "en_only": False,
    },
    "editor": {
        "site_id": 104,
        "collection_pid": "PCJ",
        "site_domain": "peercommunityjournal.org/submit",
    },
}

############################################################################
#
#  Tâches à effectuer pour créer un nouveau site:
#
#  - Modifier ce fichier et ajouter une nouvelle entrée. (git push dans la foulée)
#  - Copier un répertoire de site (ex: cd sites; cp -r acirm <new_site>)
#  - Faire un Replace in Path dans sites/<new_site> de acirm par <new_site> (minuscule),
#        et le même replace en majuscule
#  - Mettre à jour le numéro du site dans les fixtures
#  - Modifier les templates (base.html, footer.html, top.html,...)
#  - Créer une config capistrano (cp -r un config/deploy existant et remplacer l'acronyme
#
#  - En local, on peut tester
#        1) py manage.py migrate
#        2) Se créer un répertoire /mathdoc_archive/<new_site>/<new_site>.xml (au minimum)
#           Attention: si ce répertoire n'est pas à la racine (ex: /home/me/mathdoc_archive),
#                      il faut se créer un lien symbolique de /mathdoc_archive vers /home/me/mathdoc_archive
#        3) py manage.py import -pid <NEW_SITE> -folder /mathdoc_archive
#           Attention: le -folder doit être un répertoire racine, sinon l'import échoue
#
#  - Sur mrstest
#        1) créer le lien symbolique /var/www/<new_site>/shared/sites/<new_site>/<new_site>/settings_local.py
#           (vers /var/www/mersenne_shared/settings_local.py),
#        2) sudo chown -R deployer:deployers <new_site>
#        3) copier un conf apache existante (ex: /etc/apache2/sites-available/acirm-ssl.conf),
#           et remplacer acirm par <new_site>  (idem en majuscule)
#        4) sudo a2ensite <new_site>-ssl.conf
#
#  - Dans la branche ptf_tools, modifier si besoin ptf_tools/settings.py (ex: MERSENNE_COLLECTIONS),
#       puis commit/push/deploy
#
#  - Sur ptf-tools
#      A) S'il y a des volumes anciens à récupérer
#         1) Si besoin de récupérer un mathdoc_archive depuis numdam:
#            - Depuis numdam-pre
#            - Modifier settings_local.py er remplacer le 127.0.0.1 de SOLR_URL par numdam.pre.u-ga.fr
#            - py manage.py export -pid <NEW_SITE> -folder /home/me/mathdoc_archive -for_archive -with-binary-files
#            - Restaurer settings_local.py
#            - scp le répertoire sur ptf_tools
#         2) Se créer un /home/<me>/mathdoc_archive/<new_site>/<new_site>.xml (au minimum)
#            Créer un lien symbolique du genre /<me>_mathdoc_archive  (voir remarque ci-dessus pour les tests en local)
#         3) cd /var/www/ptf_tools/current; . venv/bin/activate; cd sites/ptf_tools
#         4) py manage.py import -pid <NEW_SITE> -folder /<me>_mathdoc_archive
#      B) Dans l'interface graphique de ptf_tools, importer des fascicules issus de Cedrics
#      C) Déployer en test
