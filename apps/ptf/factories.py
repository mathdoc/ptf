import datetime

import factory.django

from django.conf import settings

from .model_helpers import get_or_create_site
from .models import Abstract
from .models import Article
from .models import Award
from .models import BibItem
from .models import Collection
from .models import Container
from .models import Contribution
from .models import ExtId
from .models import Kwd
from .models import PtfSite
from .models import Publisher
from .models import Resource
from .models import ResourceCount
from .models import ResourceId
from .models import SiteMembership
from .models import Subj


class ResourceFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Resource


class CollectionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Collection

    pid = "PID"
    title_tex = "Collection1"
    title_html = "Collection1"
    coltype = "journal"
    alive = True
    fyear = 1949
    lyear = 2023


class PublisherFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Publisher

    pub_key = factory.Sequence(lambda n: "%d" % n)
    pub_name = "Publisher1"
    pub_loc = "Grenoble"


class ContainerFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Container

    my_collection = factory.SubFactory(CollectionFactory)
    my_publisher = factory.SubFactory(PublisherFactory)
    pid = "PID_2014__3_"
    title_tex = "Container1"
    lang = "fr"
    ctype = "issue"
    year = "2014"
    number = "3"
    number_int = 3
    vseries_int = 0
    seq = factory.Sequence(lambda n: n)
    last_modified = datetime.datetime(2014, 1, 1, 1, 1, 1, 1, tzinfo=datetime.UTC)


class ArticleFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Article

    my_container = factory.SubFactory(ContainerFactory)
    pid = "pid1"
    doi = "10.1111/site.1"
    seq = factory.Sequence(lambda n: n)
    pseq = factory.Sequence(lambda n: n)
    date_accepted = datetime.datetime(2023, 1, 1, 1, 1, 1, 2, tzinfo=datetime.UTC)
    date_online_first = datetime.datetime(2023, 1, 1, 1, 1, 1, 3, tzinfo=datetime.UTC)
    date_published = datetime.datetime(2023, 1, 1, 1, 1, 1, 4, tzinfo=datetime.UTC)
    # extids = [{'id_type': 'zbl-item-id', 'id_value': 1111.11111}]


class AbstractFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Abstract

    resource = factory.SubFactory(ArticleFactory)
    lang = "en"
    value_tex = "abstract1"
    value_html = ""
    seq = factory.Sequence(lambda n: n)


class AwardFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Award

    resource = factory.SubFactory(ArticleFactory)
    abbrev = "award1"
    award_id = 1
    seq = factory.Sequence(lambda n: n)


class KwdFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Kwd

    resource = factory.SubFactory(ArticleFactory)
    lang = "en"
    type = ""
    value = "kw1"
    seq = factory.Sequence(lambda n: n)


class ExtIdFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ExtId

    resource = factory.SubFactory(ArticleFactory)
    id_type = "zbl-item-id"
    id_value = 1111.11111


class BibItemFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = BibItem

    resource = factory.SubFactory(ResourceFactory)
    sequence = factory.Sequence(lambda n: n)
    label = "label"
    type = "article"
    user_id = "bib1"
    article_title_tex = "Article title"
    source_tex = "Journal"
    publisher_name = "Publisher"
    publisher_loc = "Grenoble"
    institution = "CNRS"
    series = "3"
    volume = "252"
    issue = "12"
    month = "Jan"
    year = "2012"
    comment = "Comment"
    annotation = "Annotation"
    fpage = "2"
    lpage = "5"


class ContributionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Contribution

    resource = factory.SubFactory(ArticleFactory)
    bibitem = factory.SubFactory(BibItemFactory)
    first_name = "John"
    last_name = "Smith"
    role = "author"
    orcid = "9999-9999-9999-999X"
    seq = factory.Sequence(lambda n: n)


class PtfSiteFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = PtfSite

    id = settings.SITE_ID
    domain = settings.SITE_DOMAIN
    name = settings.SITE_NAME
    acro = settings.SITE_NAME

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        obj = get_or_create_site(settings.SITE_ID)
        return obj


class SiteMembershipFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = SiteMembership

    resource = factory.SubFactory(ArticleFactory)
    site = factory.SubFactory(PtfSiteFactory)


class ArticleWithSiteFactory(ArticleFactory):
    membership = factory.RelatedFactory(
        SiteMembershipFactory,
        factory_related_name="resource",
    )


class ContainerWithSiteFactory(ContainerFactory):
    membership = factory.RelatedFactory(
        SiteMembershipFactory,
        factory_related_name="resource",
    )


class CollectionWithSiteFactory(CollectionFactory):
    membership = factory.RelatedFactory(
        SiteMembershipFactory,
        factory_related_name="resource",
    )


class SubjFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Subj

    resource = factory.SubFactory(ArticleFactory)
    lang = "und"
    type = "type"
    value = "subj1"
    seq = factory.Sequence(lambda n: n)


class ResourceIdFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ResourceId

    resource = factory.SubFactory(ResourceFactory)
    id_type = "doi"
    id_value = "10.5802/foo.1"


class ResourceCountFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ResourceCount

    resource = factory.SubFactory(ResourceFactory)
    name = "page-count"
    value = "10"
    seq = 0
