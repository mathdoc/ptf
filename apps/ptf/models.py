import os
import re
from urllib.parse import urljoin
from urllib.parse import urlparse

from django.conf import settings
from django.contrib.sites.models import Site
from django.core.exceptions import MultipleObjectsReturned
from django.core.files.storage import FileSystemStorage
from django.db import models
from django.db.models import Max
from django.db.models import Prefetch
from django.db.models import Q
from django.db.models.signals import pre_delete
from django.dispatch import receiver
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import get_language
from django.utils.translation import gettext_lazy as _

from ptf import exceptions
from ptf.bibtex import append_in_latex
from ptf.bibtex import get_bibtex_id
from ptf.bibtex import get_bibtex_names
from ptf.cmds.xml import xml_utils
from ptf.display import resolver
from ptf.utils import get_display_name
from ptf.utils import volume_display

CONTRIB_TYPE_AUTHOR = "author"
CONTRIB_TYPE_EDITOR = "editor"
CONTRIB_TYPE_CONTRIBUTOR = "contributor"
CONTRIB_TYPE_REDAKTOR = "redaktor"
CONTRIB_TYPE_ORGANIZER = "organizer"
CONTRIB_TYPE_PRESENTER = "presenter"
EDITED_BOOK_TYPE = "book-edited-book"


# http://stackoverflow.com/questions/929029/
# how-do-i-access-the-child-classes-of-an-object-in-django-without-knowing-the-nam
# http://djangosnippets.org/snippets/1031/


class UnknownRelationType(Exception):
    pass


class Identifier:
    """
    descripteur
    """

    def __init__(self, id_type):
        self.id_type = id_type

    def __get__(self, obj, objtype):
        value = ""
        idobj_qs = obj.resourceid_set.filter(id_type=self.id_type)
        if idobj_qs.count() > 0:
            value = idobj_qs.first().id_value

        return value

    def __set__(self, obj, value):
        raise NotImplementedError("Operation not implemented")


class PtfSite(Site):
    """Site hébergé"""

    acro = models.CharField(max_length=32, unique=True)
    public = models.OneToOneField(
        "self", null=True, related_name="test_site", on_delete=models.CASCADE
    )
    prev_pub_date = models.DateField(null=True)
    last_pub_date = models.DateField(null=True)


class ResourceQuerySet(models.QuerySet):
    def prefetch_contributors(self):
        return self.prefetch_related("contributions", "contributions__contribaddress_set")

    def prefetch_references(self):
        sorted_ids = BibItemId.objects.filter(
            bibitem__resource__pk__in=self.values("pk")
        ).order_by("id_type")
        return self.prefetch_related(
            Prefetch("bibitem_set__bibitemid_set", queryset=sorted_ids),
            "bibitem_set__contributions",
            "bibitem_set__contributions__contribaddress_set",
        )

    def prefetch_work(self):
        return self.prefetch_related(
            "resourceid_set",
            "extid_set",
            "abstract_set",
            "kwd_set",
            "subj_set",
            "datastream_set",
            "relatedobject_set",
            "extlink_set",
            "resourcecount_set",
            "subject_of",
            "object_of",
            "award_set",
            "frontmatter",
        )

    def prefetch_all(self):
        return self.prefetch_references().prefetch_contributors().prefetch_work()


class Resource(models.Model):
    classname = models.CharField(max_length=32, editable=False, db_index=True)
    ##
    # dans SiteMembership
    provider = models.ForeignKey("Provider", null=True, on_delete=models.CASCADE)
    ##
    # provider id
    # pas unique globalement, mais unique par provider
    # et par site hébergé
    pid = models.CharField(max_length=80, db_index=True, blank=True, default="")
    ##
    # surrogate provider id -- unique par provider
    sid = models.CharField(max_length=64, db_index=True, blank=True, null=True)
    doi = models.CharField(max_length=64, unique=True, null=True, blank=True)
    right_resources_related_to_me = models.ManyToManyField(
        "self",
        symmetrical=False,
        through="Relationship",
        related_name="left_resources_related_to_me",
    )
    sites = models.ManyToManyField(PtfSite, symmetrical=False, through="SiteMembership")

    title_xml = models.TextField(default="")

    lang = models.CharField(max_length=3, default="und")
    title_tex = models.TextField(default="")
    title_html = models.TextField(default="")

    trans_lang = models.CharField(max_length=3, default="und")
    trans_title_tex = models.TextField(default="")
    trans_title_html = models.TextField(default="")

    abbrev = models.CharField(max_length=128, blank=True, db_index=True)
    funding_statement_html = models.TextField(default="")
    funding_statement_xml = models.TextField(default="")
    footnotes_html = models.TextField(default="")
    footnotes_xml = models.TextField(default="")

    body_html = models.TextField(default="")
    body_tex = models.TextField(default="")
    body_xml = models.TextField(default="")

    objects = ResourceQuerySet.as_manager()

    class Meta:
        unique_together = ("provider", "pid")

    def __str__(self):
        return self.pid

    def get_absolute_url(self):
        """
        @warning : return an absolute path, not an URL
        @return: absolute path without scheme or domain name
        """

        return reverse("item_id", kwargs={"pid": self.pid})

    def get_url_absolute(self):
        if settings.SITE_NAME == "numdam":
            domain = "http://numdam.org/"
        else:
            col = self.get_collection()
            try:
                website_extlink = col.extlink_set.get(rel="website", metadata="website")
                # add ending / for urljoin
                domain = website_extlink.location + "/"
            except ExtLink.DoesNotExist:
                domain = "/"

        # remove beginning / for urljoin
        resource_path = re.sub(r"^/*", "", self.get_absolute_url())
        return urljoin(domain, resource_path)

    def save(self, *args, **kwargs):
        if not self.id:
            self.classname = self.__class__.__name__
        super().save(*args, **kwargs)

    def cast(self):
        base_obj = self
        if not hasattr(self, self.classname.lower()):
            base_obj = self.article
        return base_obj.__getattribute__(self.classname.lower())

    def get_collection(self):
        return None

    def get_top_collection(self):
        return None

    def get_container(self):
        return None

    def is_deployed(self, site):
        try:
            site = self.sites.get(id=site.id)
        except Site.DoesNotExist:
            return False
        else:
            return True

    def deploy(self, site, deployed_date=None):
        """
        Warning: As of July 2018, only 1 site id is stored in a SolR document
        Although the SolR schema is already OK to store multiple sites ("sites" is an array)
        no Solr commands have been written to add/remove sites
        We only have add commands.
        Search only works if the Solr instance is meant for individual or ALL sites

        :param site:
        :param deployed_date:
        :return:
        """
        if deployed_date is None:
            deployed_date = timezone.now()
        try:
            membership = SiteMembership.objects.get(resource=self, site=site)
            membership.deployed = deployed_date
            membership.save()
        except SiteMembership.DoesNotExist:
            membership = SiteMembership(resource=self, site=site, deployed=deployed_date)
            membership.save()

    def undeploy(self, site):
        try:
            membership = SiteMembership.objects.get(resource=self, site=site)
        except SiteMembership.DoesNotExist:
            pass
        else:
            membership.delete()

    def date_time_deployed(self, site):
        try:
            membership = SiteMembership.objects.get(resource=self, site=site)
        except SiteMembership.DoesNotExist:
            return None
        return membership.deployed

    def deployed_date(self, site=None):
        if site is None and settings.SITE_NAME == "ptf_tools":
            # on est sur ptf-tools et dans un template on fait appel à deployed_date
            # si le site lié à la collection est créé on renvoie la date de déploiement sur ce site
            # sinon None
            from ptf import model_helpers

            site = model_helpers.get_site_mersenne(self.get_collection().pid)
            return self.date_time_deployed(site)
        if not site:
            site = Site.objects.get_current()

        return self.date_time_deployed(site)

    def get_id_value(self, id_type):
        try:
            rid = self.resourceid_set.get(id_type=id_type)
        except ResourceId.DoesNotExist:
            return None
        else:
            return rid.id_value

    # TODO : doi is in ResourceId and in Resource ? maybe use only one ...
    def get_doi_href(self):
        href = None
        if self.doi:
            href = resolver.get_doi_url(self.doi)
        return href

    def get_link(self, link_type):
        href = None
        for link in self.extlink_set.all():
            if link.rel == link_type:
                href = link.get_href()

        return href

    def website(self):
        return self.get_link("website")

    def test_website(self):
        return self.get_link("test_website")

    def icon(self):
        return self.get_link("icon")

    def small_icon(self):
        return self.get_link("small_icon")

    # def relation_names(self):
    #     names = set()
    #     for rel in self.subject_of.select_related('rel_info').all():
    #         name = rel.rel_info.left
    #         names.add(name)
    #     for rel in self.object_of.select_related('rel_info').all():
    #         name = rel.rel_info.right
    #         names.add(name)
    #     return names
    #
    # def get_related(self, rel_type, count_only=True):
    #     is_subject = False
    #     try:
    #         rel = RelationName.objects.get(left=rel_type)
    #     except RelationName.DoesNotExist:
    #         try:
    #             rel = RelationName.objects.get(right=rel_type)
    #         except RelationName.DoesNotExist:
    #             raise UnknownRelationType(rel_type)
    #         else:
    #             pass
    #     else:
    #         is_subject = True
    #     if is_subject:
    #         qs = self.subject_of.filter(rel_info=rel)
    #         if count_only:
    #             return qs.count()
    #         result = [x.related.cast() for x in qs]
    #     else:
    #         qs = self.object_of.filter(rel_info=rel)
    #         if count_only:
    #             return qs.count()
    #         result = [x.resource.cast() for x in qs]
    #     return result

    def is_edited_book(self):
        return False

    def get_document_type(self):
        """
        Is used to classify the resources in the
         - (TODO) SOLR Facet Document Type
         - Geodesic menu (Journals/Books/Seminars...)
        """
        return ""

        collection = self.get_top_collection()
        if collection.coltype == "journal":
            document_type = "Article de revue"
        elif collection.coltype == "acta":
            document_type = "Acte de séminaire"
        elif collection.coltype == "thesis":
            self.data["classname"] = "Thèse"
            document_type = "Thèse"
        elif collection.coltype == "lecture-notes":
            self.data["classname"] = "Notes de cours"
            document_type = "Notes de cours"
        elif collection.coltype == "proceeding":
            self.data["classname"] = "Acte de rencontre"
            document_type = "Acte de rencontre"
        else:
            self.data["classname"] = "Livre"
            document_type = "Livre"

        return document_type

    # NOTE - 12/08/2017 - Basile
    # utilisé nul part à delete ?
    # def has_errata(self):
    #     return self.get_related('corrected-by')

    # def is_erratum_to(self):
    #     return self.get_related('corrects', count_only=False)

    # def errata(self):
    #     return self.get_related('corrected-by', count_only=False)

    # def erratum(self):
    #     errata = self.get_related('corrected-by', count_only=False)
    #     if len(errata) == 1:
    #         return errata[0]
    #     return None

    # def questions(self):
    #     return self.get_related('resolves', count_only=False)

    # def solutions(self):
    #     return self.get_related('resolved-by', count_only=False)

    # def complements(self):
    #     return self.get_related('complements', count_only=False)

    # def completed(self):
    #     return self.get_related('complemented-by', count_only=False)

    # def follows(self):
    #     followed = self.get_related('follows', count_only=False)
    #     if len(followed) == 1:
    #         return followed[0]
    #     return 0

    # def followed(self):
    #     follows = self.get_related('followed-by', count_only=False)
    #     if len(follows) == 1:
    #         return follows[0]
    #     return 0

    # def citations_count(self):
    #     if not self.pid:
    #         return 0
    #     qs = BibItemId.objects.select_related().filter(
    #         id_type=self.provider.pid_type,
    #         id_value=self.pid,
    #         bibitem__resource__sites__id=settings.SITE_ID
    #     )
    #     return qs.count()

    def citations(self):
        if not self.pid or not self.provider:
            return []

        qs = BibItemId.objects.select_related().filter(
            id_type=self.provider.pid_type,
            id_value=self.pid,
            bibitem__resource__sites__id=settings.SITE_ID,
        )
        # on ne peut pas trier par date sur la requete car
        # on peut avoir soit des containers soit des articles
        # comme resource et year est sur le container uniquement
        result = [bid.bibitem.resource.cast() for bid in qs]
        result.sort(key=lambda item: item.get_year(), reverse=True)
        return result

    def get_contributions(self, role):
        # prefetch probably has already queried the database for the contributions
        # Using filter on self.contributions would result in a separate SQL query
        return [
            contribution
            for contribution in self.contributions.all()
            if contribution.role.find(role) == 0
        ]

    def get_author_contributions(self, strict=True):
        authors = self.get_contributions("author")
        if not strict and len(authors) == 0:
            authors = self.get_editors()
        return authors

    def get_authors_short(self):
        authors = self.get_contributions("author")
        if len(authors) > 2:
            authors = "; ".join([str(author) for author in authors[:2]])
            authors += " <i>et al.</i>"
            return authors
        return "; ".join([str(author) for author in authors])

    def get_editors(self):
        return self.get_contributions("editor")

    def get_contributors(self):
        return self.get_contributions("contributor")

    def get_redaktors(self):
        return self.get_contributions("redaktor")

    def get_organizers(self):
        return self.get_contributions("organizer")

    def get_presenters(self):
        return self.get_contributions("presenter")

    def get_kwds_by_type(self):
        msc = [kwd for kwd in self.kwd_set.all() if kwd.type == "msc"]
        kwds = [kwd for kwd in self.kwd_set.all() if kwd.type != "msc" and kwd.lang == self.lang]
        trans_kwds = [
            kwd for kwd in self.kwd_set.all() if kwd.type != "msc" and kwd.lang != self.lang
        ]
        return msc, kwds, trans_kwds

    def get_subjs_by_type_and_lang(self):
        subjs = {}
        for subj in self.subj_set.all():
            if subj.type in subjs:
                if subj.lang in subjs[subj.type]:
                    subjs[subj.type][subj.lang].append(subj)
                else:
                    subjs[subj.type][subj.lang] = [subj]
            else:
                subjs[subj.type] = {subj.lang: [subj]}

        return subjs

    def self_uris_no_xml(self):
        """
        Returns a list of links to the datastream of the resource (= pdf/djvu of the resource)
        This function is only used to export an xml (oai)
        HTML templates use get_binary_files_href (see below)
        """
        links = [
            {
                "mimetype": link.mimetype,
                "full_path": self.get_binary_file_href_full_path(
                    "self", link.mimetype, link.location
                ),
                "link": link.text,
            }
            for link in self.datastream_set.all()
        ]
        return links

    @staticmethod
    def append_href_to_binary_files(binary_files, key, mimetype, href):
        if mimetype == "application/pdf":
            if key in binary_files:
                binary_files[key]["pdf"] = href
            else:
                binary_files[key] = {"pdf": href}
        elif mimetype == "image/x.djvu":
            if key in binary_files:
                binary_files[key]["djvu"] = href
            else:
                binary_files[key] = {"djvu": href}
        elif mimetype == "application/x-tex":
            if key in binary_files:
                binary_files[key]["tex"] = href
            else:
                binary_files[key] = {"tex": href}
        elif mimetype == "video":
            if key in binary_files:
                binary_files[key]["video"] = href
            else:
                binary_files[key] = {"video": href}

    def get_binary_files_location(self):
        binary_files = []

        for obj in self.extlink_set.all():
            if obj.rel == "icon" or obj.rel == "small_icon":
                binary_files.append(obj.location)

        for obj in self.relatedobject_set.all():
            if obj.rel != "video" and obj.rel != "html-image":
                binary_files.append(obj.location)

        for obj in self.datastream_set.all():
            binary_files.append(obj.location)

        if hasattr(self, "translations"):
            for translated_article in self.translations.all():
                binary_files.extend(translated_article.get_binary_files_location())

        return binary_files

    def get_binary_files_href(self):
        """
        Get all the HREFs (without http://site) of the binary files (pdf/djvu) related to the resource
        Result:  { 'self': {'pdf':href, 'djvu':href, 'tex': href},
                   'toc':{'pdf':href, 'djvu':href},
                   'frontmatter':{'pdf':href, 'djvu':href},
                   'backmatter':{'pdf':href, 'djvu':href},
                   'translations': {<lang>: {'pdf':href,...}}  }
        result['self'] is the main pdf/djvu of the resource (article pdf,
        full volume pdf, book part pdf)
            The information come from the DataStream
            The other results come from RelatedObject
        """
        binary_files = {}

        for related_object in self.relatedobject_set.all():
            key = related_object.rel
            mimetype = related_object.mimetype
            location = related_object.location
            href = self.get_binary_file_href_full_path(key, mimetype, location)

            self.append_href_to_binary_files(binary_files, key, mimetype, href)

        allow_local_pdf = not hasattr(settings, "ALLOW_LOCAL_PDF") or settings.ALLOW_LOCAL_PDF

        for stream in self.datastream_set.all():
            key = "self"
            mimetype = stream.mimetype
            location = stream.location

            if allow_local_pdf or mimetype != "application/pdf":
                if location.find("http") == 0:
                    href = location
                else:
                    href = self.get_binary_file_href_full_path("self", mimetype, location)

                self.append_href_to_binary_files(binary_files, key, mimetype, href)

        if not allow_local_pdf:
            qs = self.extlink_set.filter(rel="article-pdf")
            if qs:
                extlink = qs.first()
                href = extlink.location
                key = "self"
                mimetype = "application/pdf"

                self.append_href_to_binary_files(binary_files, key, mimetype, href)

        translations = {}
        if hasattr(self, "translations"):
            for trans_article in self.translations.all():
                result = trans_article.get_binary_files_href()
                if "self" in result:
                    translations[trans_article.lang] = result["self"]

        binary_files["translations"] = translations

        return binary_files

    def get_binary_file_href_full_path(self, doctype, mimetype, location):
        """
        Returns an encoded URL to a pdf/djvu
        URLs in HTML pages do not return the exact path to a file (for safety reason)
        An encode URL is used instead
        Ex: URL to an article pdf:  article/pid.pdf
            URL to an issue full pdf: issue/pid.pdf
            URL to an issue toc djvu: issue/toc/pid.pdf
            URL to an issue frontmatter: issue/frontmatter.pid.djvu
        When you click on such a link, ptf/views.py will decode the URL in get_pdf
        and use the DataStream/RelatedObject true location to return the file
        Input: doctype: 'self', 'toc', 'frontmatter', 'backmatter'
               mimetype: 'application/pdf', 'image/x.djvu'

        Ex: /article/ALCO_2018__1_1_23_0.pdf
            /issue/MSMF_1978__55-56__1_0.pdf
            /issue/toc/CSHM_1980__1_.pdf
            /article/ALCO_2018__1_1_23_0/tex/src/tex/ALCO_Thiem_31.tex

        """
        if self.embargo():
            return ""

        pid = self.pid
        doi = getattr(self, "doi")
        if doi is not None:
            pid = doi

        prefix = doctype if doctype != "self" else ""

        if "annexe" in location:
            # 04/08/2021. Not used anymore ?

            # Ex: /article/SMAI-JCM_2015__1__83_0/attach/Allaire-Dapogny-supp.pdf
            href = reverse(
                "annexe-pdf", kwargs={"pid": pid, "relative_path": location.split("/")[-1]}
            )

        elif mimetype in ["application/pdf", "image/x.djvu"] and doctype not in [
            "review",
            "supplementary-material",
        ]:
            extension = "pdf" if mimetype == "application/pdf" else "djvu"
            if len(prefix) > 0:
                href = reverse(
                    "issue-relatedobject-pdf",
                    kwargs={"pid": pid, "extension": extension, "binary_file_type": prefix},
                )
            else:
                href = reverse("item-pdf", kwargs={"pid": pid, "extension": extension})

        elif mimetype == "application/x-tex":
            to_find = "/src/tex/"
            i = location.find(to_find)
            if i > 0:
                location = location[i + 1 :]

            href = reverse("article-binary-files", kwargs={"pid": pid, "relative_path": location})

        else:
            # All other attachments (videos, zip, etc...) :

            to_find = "/attach/"
            i = location.find(to_find)
            if i > 0:
                location = location[i + 1 :]

            # Ex: /article/ALCO_2018__1_1_23_0/file/review_history.pdf
            href = reverse("article-binary-files", kwargs={"pid": pid, "relative_path": location})

        return href

    def get_binary_disk_location(self, doctype, mimetype, relativepath):
        """
        Returns the path of a binary file (pdf/djvu) on the file server
        This function is called when you click on a link like issue/frontmatter/pid.pdf

        URLs in HTML pages do not return the exact path to a file (for safety reason)
        An encoded URL is used instead (see get_binary_file_href_full_path above)
          Ex:  URL to an issue toc djvu: issue/toc/pid.pdf

        When you click on such a link, ptf/views.py will decode the URL in get_pdf
        before calling get_binary_disk_location to get the exact pdf/djvu disk location
        based on the DataStream/RelatedObject location

        Input: doctype: 'self', 'toc', 'frontmatter', 'backmatter'
               mimetype: 'application/pdf', 'image/x.djvu'
               relativepath: Ex: 'src/tex/ALCO_Thiem_31.tex'

        Returns value: path or None if there is an embargo
        May raise exceptions.ResourceDoesNotExist if the resource
        (RelatedObject/DataStream) does not exist
        """
        if self.embargo():
            return None

        filename = None

        if (
            doctype not in ["self", "toc", "frontmatter", "backmatter"]
            and len(doctype) == 2
            and hasattr(self, "translations")
        ):
            for translated_article in self.translations.all():
                if translated_article.lang == doctype:
                    filename = translated_article.get_binary_disk_location(
                        "self", mimetype, relativepath
                    )
        elif relativepath:
            # relative path are used with supplementary materials or TeX Source file
            filename = os.path.join(self.get_relative_folder(), relativepath)
        else:
            if doctype != "self":
                try:
                    obj = self.relatedobject_set.filter(mimetype=mimetype, rel=doctype).get()

                except RelatedObject.DoesNotExist:
                    # status = 404
                    raise exceptions.ResourceDoesNotExist("The binary file does not exist")
            else:
                try:
                    obj = self.datastream_set.get(mimetype=mimetype)

                except DataStream.DoesNotExist:
                    # status = 404
                    raise exceptions.ResourceDoesNotExist("The binary file does not exist")
            filename = obj.location

        return filename

    def get_abstracts(self):
        return self.abstract_set.filter(tag__endswith="abstract")

    def get_avertissements(self):
        return self.abstract_set.filter(tag="avertissement")

    def get_notes(self):
        return self.abstract_set.filter(tag="note")

    def get_descriptions(self):
        return self.abstract_set.filter(tag="description")

    def get_editorial_intros(self):
        return self.abstract_set.filter(tag__endswith="intro")

    def get_toc(self):
        return self.abstract_set.filter(tag__endswith="toc")

    def get_biblio(self):
        return self.abstract_set.filter(tag="biblio")

    def accept(self, visitor):
        return visitor.visit(self.cast())

    def natural_key(self):
        return (self.pid, self.provider.id)

    def get_solr_body(self, field):
        from ptf.cmds import solr_cmds

        body = None
        cmd = solr_cmds.solrGetDocumentByPidCmd({"pid": self.pid})
        result = cmd.do()
        if result:
            if field in result:
                body = result[field]

        return body

    def get_body(self):
        return self.get_solr_body("body")

    def volume_string(self):
        return ""

    def update_bibtex_with_commons(self, bibtex, container, hostname, scheme, indent):
        # TODO chapter, howpublished?
        # TODO institution
        # if self.institution:
        #    append_in_latex(bibtex, indent + 'institution = {' + self.institution + '},' )

        to_appear = container.to_appear()
        is_cr = container.is_cr()

        # pages = self.pages()
        publisher = container.my_publisher

        if publisher is not None:
            if publisher.pub_name:
                append_in_latex(bibtex, indent + "publisher = {" + publisher.pub_name + "},")
            if publisher.pub_loc:
                append_in_latex(bibtex, indent + "address = {" + publisher.pub_loc + "},")

        if not to_appear:
            if container.volume:
                append_in_latex(bibtex, indent + "volume = {" + self.volume_string() + "},")
            if container.number and not (is_cr and container.number[0] == "G"):
                append_in_latex(bibtex, indent + "number = {" + container.number + "},")

        if container.year != "0":
            append_in_latex(bibtex, indent + "year = {" + container.year + "},")
        elif hasattr(self, "date_online_first") and self.date_online_first is not None:
            year = self.date_online_first.strftime("%Y")
            append_in_latex(bibtex, indent + "year = {" + year + "},")

        if self.doi:
            append_in_latex(bibtex, indent + "doi = {" + self.doi + "},")

        ext_type = {"zbl-item-id": "zbl", "mr-item-id": "mrnumber"}
        for extid in self.extid_set.filter(id_type__in=["zbl-item-id", "mr-item-id"]):
            append_in_latex(
                bibtex, indent + ext_type.get(extid.id_type) + " = {" + extid.id_value + "},"
            )

        if self.lang and self.lang != "und":
            append_in_latex(bibtex, indent + "language = {" + self.lang + "},")

        if to_appear:
            append_in_latex(bibtex, indent + "note = {Online first},")
        elif not is_cr and len(hostname) > 0:
            # No latex encoding, so append directly in the array
            url = f"{scheme}://{hostname}{self.get_absolute_url()}"
            bibtex.append("{}url = {}{}{}".format(indent, "{", url, "}"))

    def update_ris_with_commons(self, items, container, hostname, scheme, sep):
        to_appear = container.to_appear()
        is_cr = container.is_cr()

        if container.year != "0":
            items.append("PY" + sep + container.year)
        elif hasattr(self, "date_online_first") and self.date_online_first is not None:
            year = self.date_online_first.strftime("%Y")
            items.append("PY" + sep + year)

        if not to_appear:
            if hasattr(self, "pages") and callable(self.pages) and self.pages():
                pages = self.pages(for_bibtex=False).split("-")
                items.append("SP" + sep + pages[0])
                if len(pages) > 1:
                    items.append("EP" + sep + pages[1])
            if container.volume:
                items.append("VL" + sep + container.volume)
            if container.number and not (is_cr and container.number[0] == "G"):
                items.append("IS" + sep + container.number)

        publisher = container.my_publisher
        if publisher is not None:
            if publisher.pub_name:
                items.append("PB" + sep + publisher.pub_name)
            if publisher.pub_loc:
                items.append("PP" + sep + publisher.pub_loc)

        if to_appear:
            items.append("N1" + sep + "Online first")
        elif not is_cr and len(hostname) > 0:
            url = f"{scheme}://{hostname}{self.get_absolute_url()}"
            items.append("UR" + sep + url)

        if self.doi:
            items.append("DO" + sep + self.doi)

        if self.lang and self.lang != "und":
            items.append("LA" + sep + self.lang)

        if self.pid:
            items.append("ID" + sep + self.pid)
        items.append("ER" + sep)

    def update_endnote_with_commons(self, items, container, hostname, scheme, sep):
        to_appear = container.to_appear()
        is_cr = container.is_cr()

        if container.year != "0":
            items.append("%D" + sep + container.year)
        elif hasattr(self, "date_online_first") and self.date_online_first is not None:
            year = self.date_online_first.strftime("%Y")
            items.append("%D" + sep + year)

        if not to_appear:
            if hasattr(self, "pages") and callable(self.pages) and self.pages():
                pages = self.pages(for_bibtex=False)
                items.append("%P" + sep + pages)
            if container.volume:
                items.append("%V" + sep + container.volume)
            if container.number and not (is_cr and container.number[0] == "G"):
                items.append("%N" + sep + container.number)

        publisher = container.my_publisher
        if publisher is not None:
            if publisher.pub_name:
                items.append("%I" + sep + publisher.pub_name)
            if publisher.pub_loc:
                items.append("%C" + sep + publisher.pub_loc)

        if to_appear:
            items.append("%Z" + sep + "Online first")
        elif not is_cr and len(hostname) > 0:
            url = f"{scheme}://{hostname}{self.get_absolute_url()}"
            items.append("%U" + sep + url)

        if self.doi:
            items.append("%R" + sep + self.doi)

        if self.lang and self.lang != "und":
            items.append("%G" + sep + self.lang)

        if self.pid:
            items.append("%F" + sep + self.pid)

    def _next_in_qs(self, qs):
        next_item = None

        if qs.count() > 1:
            ready_for_next = False
            for item in qs:
                if ready_for_next:
                    next_item = item
                    ready_for_next = False
                if item.pid == self.pid:
                    ready_for_next = True
        return next_item

    def get_next_resource(self):
        return None

    def get_previous_resource(self):
        return None


class PublisherQuerySet(models.QuerySet):
    def get_by_natural_key(self, pub_key, pub_name):
        return self.get(pub_key=pub_key)


class Publisher(Resource):
    """
    les classes Publisher, EventSeries et Event sont un peu à part:
    a priori pas de pid ni sid et même pas d'identificateur du tout
    d'où les slugs
    On peut les sortir de la hiérarchie resource -- les y laisser
    permet de leur attache des meta suppléméntaires. Voir Provider
    pour la possibilité inverse :-)
    """

    pub_key = models.CharField(max_length=128, unique=True)
    pub_name = models.CharField(max_length=256, db_index=True)
    pub_loc = models.CharField(max_length=128, db_index=True)

    # 2016-05-18: Publisher is only used by Container: ManyToOne relation
    # publishes = models.ManyToManyField(Resource, related_name='Publisher')

    def __str__(self):
        return self.pub_key

    @staticmethod
    def get_collection():
        return None

    @staticmethod
    def get_top_collection():
        return None

    @staticmethod
    def get_container():
        return None

    def natural_key(self):
        return (
            self.pub_key,
            self.pub_name,
        )

    objects = PublisherQuerySet.as_manager()


class CollectionQuerySet(models.QuerySet):
    def order_by_date(self):
        return self.annotate(year=Max("content__year")).order_by("year")

    def get_by_natural_key(self, pid, provider):
        return self.get(pid=pid, provider=provider)


class Collection(Resource):
    # journal, acta, book-series, these, lectures
    coltype = models.CharField(max_length=32, db_index=True)
    title_sort = models.CharField(max_length=128, db_index=True)  # sort key, not displayed
    issn = Identifier("issn")
    e_issn = Identifier("e-issn")
    wall = models.IntegerField(default=5)
    alive = models.BooleanField(default=True)
    # First/Last year of a collection. Comes from its containers. Is typically
    # updated when a container is imported.
    fyear = models.IntegerField(default=0)
    lyear = models.IntegerField(default=0)
    last_doi = models.IntegerField(default=0)

    # Ancestors means journals that existed before the creation of the Collection: time-based relation
    # (the name 'ancestor' does not fit with 'parent' as 'parent' refers to a tree structure)
    # We don't really create a tree.
    # Ex: AFST is the root journal. AFST-0996-0481 is the original journal that became AFST-0240-2955 that became AFST.
    # We create a top node (AFST) and 2 ancestors (AFST-0996-0481, AFST-0240-2955)
    parent = models.ForeignKey(
        "self", on_delete=models.CASCADE, null=True, blank=True, related_name="ancestors"
    )
    objects = CollectionQuerySet.as_manager()

    class Meta:
        ordering = ["title_sort"]

    # This function is used by Django: https://docs.djangoproject.com/fr/3.0/ref/models/instances/
    # Unfortunately, the name is wrong: get_absolute_url must return a relative path !?!
    # => hence the get_url_absolute below that returns an absolute URL
    # def get_absolute_url(self):
    #     if self.coltype == "thesis":
    #         url = reverse("pretty_thesis", args=[f'"{self.title_html}"-p'])
    #     elif self.coltype == "book-series":
    #         url = reverse("pretty-series", args=[f'"{self.title_html}"-p'])
    #     elif self.coltype == "lectures" or self.coltype == "lecture-notes":
    #         url = reverse("pretty-lectures", args=[f'"{self.title_html}"-p'])
    #     else:
    #         url = reverse(self.coltype + "-issues", kwargs={"jid": self.pid})
    #     return url

    def bloodline(self):
        # returns parent + siblings, ordered by date (of the last volume published)
        if self.parent or self.ancestors.exists():
            parent_pid = self.parent.pid if self.parent else self.pid
            return Collection.objects.filter(
                Q(pid=parent_pid) | Q(parent__pid=parent_pid)
            ).order_by_date()
        return Collection.objects.none()

    def preceding_journal(self):
        # returns my ancestor (timed-based) = the Collection that was published just before me
        bloodline = self.bloodline()
        for index, collection in enumerate(bloodline):
            if collection == self and index:
                return bloodline[index - 1]
        return Collection.objects.none()

    def get_wall(self):
        return self.wall

    def get_collection(self):
        return self

    def get_top_collection(self):
        return self.parent if self.parent else self

    def get_document_type(self):
        """
        Is used to classify the resources in the
         - (TODO) SOLR Facet Document Type
         - Geodesic menu (Journals/Books/Seminars...)
        """

        if self.coltype == "journal":
            document_type = "Revue"
        elif self.coltype == "acta":
            document_type = "Séminaire"
        elif self.coltype == "thesis":
            document_type = "Thèse"
        elif self.coltype == "lecture-notes":
            document_type = "Notes de cours"
        elif self.coltype == "proceeding":
            self.data["classname"] = "Acte de rencontre"
            document_type = "Actes de rencontres"
        else:
            document_type = "Livre"

        return document_type

    def deployed_date(self, site=None):
        # return the last date of metadata deployed date for all of containers
        containers = self.content.all()
        date = None
        if containers:
            site = Site.objects.get_current()
            sms = (
                SiteMembership.objects.filter(resource__in=containers)
                .filter(site=site)
                .order_by("-deployed")
            )
            date = sms.first().deployed
        return date

    def get_relative_folder(self):
        return resolver.get_relative_folder(self.get_top_collection().pid)


class ContainerQuerySet(ResourceQuerySet):
    def prefetch_all(self):
        return super().prefetch_all().select_related("my_collection", "my_publisher")

    def prefetch_for_toc(self):
        return (
            self.prefetch_contributors()
            .prefetch_work()
            .prefetch_related(
                "article_set__datastream_set",
                "article_set__subj_set",
                "article_set__extlink_set",
                "article_set__resourcecount_set",
                "article_set__contributions",
            )
            .select_related("my_collection", "my_publisher")
        )


class Container(Resource):
    """
    mappe issue et book (on pourrait faire deux classes) ou une hiérarchie
    container <--- issue
              <--- book
    """

    # issue, book-monograph, book-edited-book (multiple authors, with editors), lecture-notes
    ctype = models.CharField(max_length=32, db_index=True)
    year = models.CharField(max_length=32, db_index=True)

    last_modified = models.DateTimeField(null=False, blank=False)
    ##
    # if ctype == issue
    number = models.CharField(max_length=32, db_index=True)  # issue number
    ##
    # data for relation with enclosing serial, if any
    my_collection = models.ForeignKey(
        Collection, null=True, related_name="content", on_delete=models.CASCADE
    )
    vseries = models.CharField(max_length=32, db_index=True)
    volume = models.CharField(max_length=64, db_index=True)
    # la même chose pour le tri
    vseries_int = models.IntegerField(default=0, db_index=True)
    volume_int = models.IntegerField(default=0, db_index=True)
    number_int = models.IntegerField(default=0, db_index=True)
    seq = models.IntegerField(db_index=True)
    with_online_first = models.BooleanField(default=False)  # Used by ptf-tools only

    my_publisher = models.ForeignKey(
        Publisher, related_name="publishes", null=True, on_delete=models.CASCADE
    )

    # Initially, a container could only be in 1 collection.
    # In 2018, we extended the model so that a container can be
    # in multiple collections (Bourbaki and Asterisque)
    #
    # This is an exception, most containers are in only 1 collection.
    # my_collection is kept to store the main collection, the one used in the "how to cite" field.
    # my_other_collections stores the additional collections.
    #
    # vseries/volume/number/seq of the main collection are kept in Container.
    # As a result, there is no need to fetch the CollectionMembership to get
    # these info for the main collection.
    my_other_collections = models.ManyToManyField(
        Collection, symmetrical=False, through="CollectionMembership"
    )

    objects = ContainerQuerySet.as_manager()

    class Meta:
        ordering = ["seq"]
        get_latest_by = ["year", "vseries_int", "volume_int", "number_int"]

    def allow_crossref(self):
        # we need at least a doi or an issn to allow crossref record
        result = self.my_collection.doi or self.my_collection.issn or self.my_collection.e_issn

        # if there are unpublished articles in the volume, we block crossref record
        if self.article_set.filter(
            date_published__isnull=True, date_online_first__isnull=True
        ).exists():
            result = False

        return result

    def all_doi_are_registered(self):
        if self.article_set.count() > 0:
            # il y a des articles, on vérifie qu'ils sont tous enregistrés
            return (
                self.article_set.filter(doibatch__status="Enregistré").count()
                == self.article_set.count()
            )
        if self.doi and self.doibatch is not None:
            return self.doibatch.status == "Enregistré"
        # aucun doi associé aux articles ou au container
        return False

    def registered_in_doaj(self):
        query = Q(date_published__isnull=True, date_online_first__isnull=True) | Q(
            do_not_publish__isnull=True
        )
        unpublished = self.article_set.filter(query).count()
        registered = self.article_set.filter(doajbatch__status="registered").count()
        all_registered = True if registered == self.article_set.count() - unpublished else False
        if not all_registered and hasattr(self, "doajbatch"):
            self.doajbatch.status = "unregistered"
            self.doajbatch.save()

    def are_all_articles_published(self):
        from ptf import model_helpers

        result = True

        for article in self.article_set.all():
            year = article.get_year()
            fyear, lyear = model_helpers.get_first_last_years(year)
            try:
                fyear = int(fyear)
            except ValueError:
                fyear = 0

            if fyear > 2017:
                if not article.date_published:
                    result = False
            elif fyear == 0:
                result = False

        return result

    def get_wall(self):
        return self.my_collection.get_wall()

    def previous(self):
        if self.get_top_collection().pid not in settings.COLLECTIONS_SEQUENCED:
            return None

        issue = None
        qs = self.my_collection.content.filter(seq=self.seq - 1)
        if qs.count() > 0:
            issue = qs.first()
        return issue

    def next(self):
        if self.get_top_collection().pid not in settings.COLLECTIONS_SEQUENCED:
            return None

        issue = None
        qs = self.my_collection.content.filter(seq=self.seq + 1)
        if qs.count() > 0:
            issue = qs.first()
        return issue

    def get_next_resource(self):
        # No Next/Previous for CRAS Special Issues or "Volume articles"
        colid = self.get_top_collection().pid
        if colid in settings.CRAS_COLLECTIONS:
            year = int(self.year)
            if (
                self.title_html
                or (colid != "CRBIOL" and year > 2020)
                or (colid == "CRBIOL" and year > 2022)
            ):
                return None

        collection = self.get_top_collection()
        if collection.pid in settings.COLLECTIONS_SEQUENCED:
            qs = collection.content.order_by("seq")
        else:
            qs = collection.content.order_by("vseries_int", "year", "volume_int", "number_int")

        next_issue = self._next_in_qs(qs)
        return next_issue

    def get_previous_resource(self):
        # No Next/Previous for CRAS Special Issues or "Volume articles"
        colid = self.get_top_collection().pid
        if colid in settings.CRAS_COLLECTIONS:
            year = int(self.year)
            if (
                self.title_html
                or (colid != "CRBIOL" and year > 2020)
                or (colid == "CRBIOL" and year > 2022)
            ):
                return None

        collection = self.get_top_collection()
        if collection.pid in settings.COLLECTIONS_SEQUENCED:
            qs = collection.content.order_by("-seq")
        else:
            qs = collection.content.order_by("-vseries_int", "-year", "-volume_int", "-number_int")
        next_issue = self._next_in_qs(qs)
        return next_issue

    # TODO container in multiple collections
    def get_collection(self):
        return self.my_collection

    def get_top_collection(self):
        return self.my_collection.get_top_collection()

    def get_other_collections(self):
        return self.my_other_collections.all()

    def get_container(self):
        return self

    def get_document_type(self):
        """
        Is used to classify the resources in the
         - (TODO) SOLR Facet Document Type
         - Geodesic menu (Journals/Books/Seminars...)
        """
        return self.get_top_collection().get_document_type()

    def get_volume(self):
        return self.volume

    def get_number(self):
        return self.number

    def embargo(self):
        return resolver.embargo(self.get_wall(), self.year)

    def to_appear(self):
        return self.with_online_first or (
            hasattr(settings, "ISSUE_TO_APPEAR_PID") and settings.ISSUE_TO_APPEAR_PID == self.pid
        )

    def is_cr(self):
        return (
            hasattr(settings, "SITE_NAME")
            and len(settings.SITE_NAME) == 6
            and settings.SITE_NAME[0:2] == "cr"
        )

    @staticmethod
    def get_base_url():
        return resolver.get_issue_base_url()

    def get_relative_folder(self):
        collection = self.get_top_collection()
        return resolver.get_relative_folder(collection.pid, self.pid)

    def get_vid(self):
        """
        08/09/2022: support of Collection ancestors
        The collection.pid might no longer be the top_collection.pid
        => The volume URL would change if we were to keep the same vid
        To keep URLs relatively similar, we use the pid of the first_issue of the volume

        VolumeDetailView (ptf/views.py) handles the vid differently and no longer decrypts the vid
        """
        # vid = f"{self.my_collection.pid}_{self.year}_{self.vseries}_{self.volume}"
        vid = self.pid
        return vid

    def get_year(self):
        return self.year

    def is_edited_book(self):
        return self.ctype == EDITED_BOOK_TYPE

    def get_citation(self, request):
        citation = ""

        author_names = get_names(self, "author")
        authors = ""
        if author_names:
            authors = "; ".join(author_names)

        if not author_names or authors == "Collectif":
            author_names = get_names(self, "editor")
            if author_names:
                authors = "; ".join(author_names) + " (" + str(_("éd.")) + ")"
            else:
                authors = ""

        if authors != "Collectif":
            citation += authors

        if citation:
            if not citation.endswith("."):
                citation += "."
            citation += " "

        citation += self.title_tex + ". "
        citation += self.my_collection.title_tex

        if self.vseries:
            citation += f", {str(_('Série'))} {self.vseries}"

        if self.volume:
            citation += ", " + str(volume_display()) + " " + self.volume + " (" + self.year + ") "

            if self.number:
                citation += "no. " + self.number + ", "
        elif self.number:
            citation += ", no. " + self.number + " (" + self.year + "), "
        else:
            citation += " (" + self.year + "), "

        redactors = self.get_redaktors()
        if len(redactors) > 0:
            redactors_str = "; ".join(get_names(self, "redaktor"))
            citation += f"{redactors_str} (red.), "

        for pages in self.resourcecount_set.all():
            citation += pages.value + " p."

        for resourceid in self.resourceid_set.all():
            if resourceid.id_type == "doi":
                citation += " doi : " + resourceid.id_value + "."

        citation += " " + self.get_url_absolute()

        return citation

    def has_detailed_info(self):
        # Ignore citations here.

        result = False

        if self.extid_set.exists():
            result = True
        elif self.kwd_set.exists():
            result = True
        else:
            for resourceid in self.resourceid_set.all():
                if resourceid.id_type != "numdam-prod-id":
                    result = True

        return result

    def get_bibtex(self, request):
        """

        :param self:
        :return: a string encoded in latex (with latexcodec)
        """
        bibtex = []
        indent = "     "

        collection = self.get_collection()

        is_phdthesis = False

        # no bibtex for an issue, only for a book (book, these)
        if self.ctype == "issue":
            return bibtex

        if collection is not None and collection.coltype == "thesis":
            is_phdthesis = True

        author_names = get_bibtex_names(self, "author")
        editor_names = get_bibtex_names(self, "editor")

        type_ = "book"
        if is_phdthesis:
            type_ = "phdthesis"

        # Numdam meeting: Use the resource pid as the bibtex id => No latex encoding to keep the '_'
        id_ = self.pid
        bibtex.append("@" + type_ + "{" + id_ + ",")

        if author_names:
            append_in_latex(bibtex, indent + author_names)
        if editor_names:
            append_in_latex(bibtex, indent + editor_names)
        append_in_latex(bibtex, indent + "title = {" + self.title_tex + "},", is_title=True)

        if collection is not None:
            append_in_latex(bibtex, indent + "series = {" + collection.title_tex + "},")

        self.update_bibtex_with_commons(bibtex, self, request.get_host(), request.scheme, indent)

        append_in_latex(bibtex, "}")
        return "\n".join(bibtex)

    def get_ris(self, request):
        """

        :param self:
        :return: a string
        """
        items = []
        sep = "  - "

        collection = self.get_collection()

        is_phdthesis = False

        # no citation for an issue, only for a book (book, these)
        if self.ctype == "issue":
            return ""

        if collection is not None and collection.coltype == "these":
            is_phdthesis = True

        if is_phdthesis:
            items.append("TY" + sep + "THES")
        else:
            items.append("TY" + sep + "BOOK")

        author_names = get_names(self, CONTRIB_TYPE_AUTHOR)
        for author in author_names:
            items.append("AU" + sep + author)

        editor_names = get_names(self, CONTRIB_TYPE_EDITOR)
        for editor in editor_names:
            items.append("ED" + sep + editor)

        items.append("TI" + sep + self.title_tex)

        if collection is not None:
            items.append("T3" + sep + collection.title_tex)

        self.update_ris_with_commons(items, self, request.get_host(), request.scheme, sep)
        return "\r\n".join(items)

    def get_endnote(self, request):
        """

        :param self:
        :return: a string
        """
        items = []
        sep = " "

        collection = self.get_collection()

        is_phdthesis = False

        # no citation for an issue, only for a book (book, these)
        if self.ctype == "issue":
            return ""

        if collection is not None and collection.coltype == "these":
            is_phdthesis = True

        if is_phdthesis:
            items.append("%0" + sep + "Thesis")
        else:
            items.append("%0" + sep + "Book")

        author_names = get_names(self, CONTRIB_TYPE_AUTHOR)
        for author in author_names:
            items.append("%A" + sep + author)

        editor_names = get_names(self, CONTRIB_TYPE_EDITOR)
        for editor in editor_names:
            items.append("%E" + sep + editor)

        items.append("%T" + sep + self.title_tex)

        if collection is not None:
            items.append("%S" + sep + collection.title_tex)

        self.update_endnote_with_commons(items, self, request.get_host(), request.scheme, sep)
        return "\r\n".join(items)

    def has_articles_excluded_from_publication(self):
        result = self.article_set.filter(do_not_publish=True).count() > 0
        return result


class EventSeries(Resource):
    """to do: clé fabriquée voir _manager.make_key done"""

    slug = models.CharField(max_length=128, unique=True)
    event_type = models.CharField(max_length=32, db_index=True)
    acro = models.CharField(max_length=32, db_index=True)
    title_sort = models.CharField(max_length=128, db_index=True)
    short_title = models.CharField(max_length=64, db_index=True)


class Event(Resource):
    """to do: clé fabriquée voir _manager.make_key done"""

    slug = models.CharField(max_length=128, unique=True)
    event_type = models.CharField(max_length=32, db_index=True)
    title_sort = models.CharField(max_length=128, db_index=True)
    string_event = models.CharField(max_length=128, db_index=True)
    year = models.CharField(max_length=32, db_index=True)
    acro = models.CharField(max_length=32, db_index=True)
    number = models.CharField(max_length=4, db_index=True)
    loc = models.CharField(max_length=64, db_index=True)
    theme = models.CharField(max_length=64, db_index=True)
    contrib = models.TextField()
    proceedings = models.ManyToManyField(Resource, related_name="Events", symmetrical=False)
    series = models.ForeignKey(EventSeries, null=True, on_delete=models.CASCADE)

    class Meta:
        ordering = ["year"]  # seq (int)


class ArticleQuerySet(ResourceQuerySet):
    def order_by_published_date(self):
        return self.order_by("-date_published", "-seq")

    def order_by_sequence(self):
        return self.order_by("seq")

    def prefetch_for_toc(self):
        return (
            self.prefetch_contributors()
            .prefetch_work()
            .select_related("my_container", "my_container__my_collection")
        )


class Article(Resource):
    """mappe journal article, book-part"""

    atype = models.CharField(max_length=32, db_index=True)
    fpage = models.CharField(max_length=32, db_index=True)
    lpage = models.CharField(max_length=32, db_index=True)
    page_range = models.CharField(max_length=32, db_index=True)
    page_type = models.CharField(max_length=64)
    elocation = models.CharField(max_length=32, db_index=True)
    article_number = models.CharField(max_length=32)
    talk_number = models.CharField(max_length=32)
    date_received = models.DateTimeField(null=True, blank=True)
    date_accepted = models.DateTimeField(null=True, blank=True)
    date_revised = models.DateTimeField(null=True, blank=True)
    date_online_first = models.DateTimeField(null=True, blank=True)
    date_published = models.DateTimeField(null=True, blank=True)
    date_pre_published = models.DateTimeField(
        null=True, blank=True
    )  # Used by ptf-tools only to measure delays
    coi_statement = models.TextField(null=True, blank=True)  # Conflict of interest
    show_body = models.BooleanField(
        default=True
    )  # Used by ptf-tools only (to export or not the body)
    do_not_publish = models.BooleanField(
        default=False
    )  # Used by ptf-tools only (to export or not the article)

    ##
    # container
    my_container = models.ForeignKey(Container, null=True, on_delete=models.CASCADE)
    seq = models.IntegerField()
    ##
    # parent part
    parent = models.ForeignKey(
        "self", null=True, related_name="children", on_delete=models.CASCADE
    )
    pseq = models.IntegerField()
    objects = ArticleQuerySet.as_manager()

    class Meta:
        ordering = ["seq", "fpage"]

    def __str__(self):
        return self.pid

    @staticmethod
    def get_base_url():
        return resolver.get_article_base_url()

    def get_absolute_url(self):
        if self.doi is not None:
            return reverse("article", kwargs={"aid": self.doi})
        else:
            return reverse("item_id", kwargs={"pid": self.pid})

    def get_relative_folder(self):
        collection = self.get_top_collection()
        return resolver.get_relative_folder(collection.pid, self.my_container.pid, self.pid)

    def embargo(self):
        if self.my_container is None:
            return False

        return self.my_container.embargo()

    def get_wall(self):
        if self.my_container is None:
            return 0
        return self.my_container.get_wall()

    def get_collection(self):
        return self.my_container.get_collection()

    def get_top_collection(self):
        return self.my_container.get_top_collection()

    def get_container(self):
        return self.my_container

    def get_document_type(self):
        """
        Is used to classify the resources in the
         - (TODO) SOLR Facet Document Type
         - Geodesic menu (Journals/Books/Seminars...)
        """

        collection = self.get_top_collection()
        if collection.coltype == "journal":
            document_type = "Article de revue"
        elif collection.coltype == "acta":
            document_type = "Acte de séminaire"
        elif collection.coltype == "thesis":
            document_type = "Thèse"
        elif collection.coltype == "lecture-notes":
            document_type = "Notes de cours"
        elif collection.coltype == "proceeding":
            document_type = "Acte de rencontre"
        else:
            document_type = "Chapitre de livre"

        return document_type

    def get_volume(self):
        return self.my_container.get_volume()

    def get_number(self):
        return self.my_container.get_number()

    def get_page_count(self):
        page_count = None
        for resourcecount in self.resourcecount_set.all():
            if resourcecount.name == "page-count":
                page_count = resourcecount.value

        return page_count

    def get_article_page_count(self):
        try:
            page_count = self.get_page_count() or 0
            page_count = int(page_count)
        except ValueError:
            page_count = 0

        lpage = fpage = 0
        try:
            fpage = int(self.fpage)
        except ValueError:
            pass
        try:
            lpage = int(self.lpage)
        except ValueError:
            pass
        if lpage > 0 and fpage > 0:
            page_count = lpage - fpage + 1
        return page_count

    def pages(self, for_bibtex=False):
        """
        Returns a string with the article pages.
        It is used for the different exports (BibTeX, EndNote, RIS)

        typically "{fpage}-{lpage}"
        For BibTex, 2 '-' are used, ie "{fpage}--{lpage}"

        Some articles have a page_range. Use this field instead of the fpage/lpage in this case.
        """
        if self.page_range:
            return self.page_range
        if self.fpage or self.lpage:
            if self.lpage:
                return (
                    f"{self.fpage}--{self.lpage}" if for_bibtex else f"{self.fpage}-{self.lpage}"
                )
            else:
                return self.fpage
        return None

    def volume_series(self):
        # Use only for latex
        if not self.my_container.vseries:
            return ""
        if self.lang == "fr":
            return self.my_container.vseries + "e s{\\'e}rie, "
        return f"Ser. {self.my_container.vseries}, "

    def volume_string(self):
        return self.volume_series() + self.my_container.volume

    def get_page_text(self, use_pp=False):
        if (self.talk_number or self.article_number) and self.get_page_count():
            return self.get_page_count() + " p."

        if not (self.lpage or self.fpage):
            return ""

        if self.lpage == self.fpage:
            return f"p. {self.lpage}"

        text = ""
        if not self.page_range:
            if self.fpage and self.lpage and use_pp:
                text = "pp. "
            else:
                text = "p. "
            text += self.fpage
            if self.fpage and self.lpage:
                text += "-"
            if self.lpage:
                text += self.lpage
        elif self.page_range[0] != "p":
            text = "p. " + self.page_range

        return text

    def get_summary_page_text(self):
        text = ""
        if self.talk_number:
            text += str(_("Exposé")) + " no. " + str(self.talk_number) + ", "
        if self.article_number:
            text += "article no. " + str(self.article_number) + ", "

        text += self.get_page_text()

        return text

    def get_breadcrumb_page_text(self):
        text = ""
        if self.my_container.with_online_first:
            if self.doi is not None:
                text = self.doi
            else:
                text = str(_("Première publication"))
        elif self.talk_number:
            text += str(_("Exposé")) + " no. " + str(self.talk_number)
        elif self.article_number:
            text += "article no. " + str(self.article_number)
        else:
            text += self.get_page_text()

        return text

    def get_year(self):
        return self.my_container.year

    def previous(self):
        try:
            return self.my_container.article_set.get(seq=(self.seq - 1))
        except (Article.DoesNotExist, MultipleObjectsReturned):
            return None

    def next(self):
        try:
            return self.my_container.article_set.get(seq=(self.seq + 1))
        except (Article.DoesNotExist, MultipleObjectsReturned):
            return None

    def get_next_resource(self):
        # TODO: rename the next function defined just above by this function
        next_article = None

        try:
            next_article = self.my_container.article_set.get(seq=(self.seq + 1))
        except (Article.DoesNotExist, MultipleObjectsReturned):
            pass

        if next_article is None and not self.my_container.title_html and self.my_container.volume:
            qs = Container.objects.filter(volume=self.my_container.volume)
            if qs.count() > 1:
                qs = qs.order_by("number_int")
                next_issue = self._next_in_qs(qs)
                if next_issue:
                    qs = next_issue.article_set.order_by("seq")
                    if qs.exists():
                        next_article = qs.first()

        return next_article

    def get_previous_resource(self):
        previous_article = None

        try:
            previous_article = self.my_container.article_set.get(seq=(self.seq - 1))
        except (Article.DoesNotExist, MultipleObjectsReturned):
            pass

        if (
            previous_article is None
            and not self.my_container.title_html
            and self.my_container.volume
        ):
            qs = Container.objects.filter(volume=self.my_container.volume)
            if qs.count() > 1:
                qs = qs.order_by("-number_int")
                previous_issue = self._next_in_qs(qs)
                if previous_issue:
                    qs = previous_issue.article_set.order_by("-seq")
                    if qs.exists():
                        previous_article = qs.first()

        return previous_article

    def get_citation_base(self, year=None, page_text=None):
        citation = ""
        if year is None:
            year = self.my_container.year

        to_appear = self.my_container.to_appear()
        is_cr = self.my_container.is_cr()

        if to_appear and year != "0":
            citation += f", Online first ({year})"
        elif to_appear:
            citation += ", Online first"
        else:
            if self.my_container.vseries:
                citation += f", {str(_('Série'))} {self.my_container.vseries}"

            if self.my_container.volume:
                citation += f", {str(volume_display())} {self.my_container.volume} ({year})"

                if self.my_container.number and not (is_cr and self.my_container.number[0] == "G"):
                    citation += f" no. {self.my_container.number}"
            elif self.my_container.number:
                citation += f", no. {self.my_container.number} ({year})"
            else:
                citation += f" ({year})"

            if self.talk_number:
                citation += f", {str(_('Exposé'))} no. {str(self.talk_number)}"
            if self.article_number:
                citation += f", article  no. {str(self.article_number)}"

        if page_text is None:
            page_text = self.get_page_text(True)
        if len(page_text) > 0:
            citation += f", {page_text}"

        if citation[-1] != ".":
            citation += "."

        if not to_appear:
            if self.page_type == "volant":
                citation += f" ({str(_('Pages volantes'))})"
            elif self.page_type == "supplement":
                citation += f" ({str(_('Pages supplémentaires'))})"
            elif self.page_type == "preliminaire":
                citation += f" ({str(_('Pages préliminaires'))})"
            elif self.page_type == "special":
                citation += f" ({str(_('Pages spéciales'))})"

        return citation

    def get_citation(self, request=None, with_formatting=False):
        citation = ""

        author_names = get_names(self, "author")
        if author_names:
            authors = "; ".join(author_names)
        else:
            author_names = get_names(self, "editor")
            if author_names:
                authors = "; ".join(author_names) + " (" + str(_("éd.")) + ")"
            else:
                authors = ""

        if authors != "Collectif":
            citation += authors

        if citation:
            if not citation.endswith("."):
                citation += "."
            citation += " "

        if with_formatting:
            citation += f"<strong>{self.title_tex}</strong>"
        else:
            citation += self.title_tex
        if self.my_container.ctype != "issue":
            citation += ", "
            citation += str(_("dans")) + " <em>" + self.my_container.title_tex + "</em>, "
        else:
            citation += ". "
        citation += self.my_container.my_collection.title_tex

        citation += self.get_citation_base()

        to_appear = self.my_container.to_appear()
        is_cr = self.my_container.is_cr()

        if not to_appear or is_cr:
            if self.doi is not None:
                citation += " doi : " + self.doi + "."

            if not to_appear and request is not None:
                url = f"{request.scheme}://{request.get_host()}{self.get_absolute_url()}"
                citation += " " + url

        return citation

    def has_detailed_info(self):
        # Ignore citations here.

        result = False

        if (
            self.date_received
            or self.date_revised
            or self.date_accepted
            or self.date_published
            or self.date_online_first
        ):
            result = True
        elif self.extid_set.exists():
            result = True
        elif self.kwd_set.exists():
            result = True
        elif self.doi is not None:
            result = True
        else:
            for resourceid in self.resourceid_set.all():
                if resourceid.id_type != "numdam-prod-id":
                    result = True

        return result

    def get_ojs_id(self):
        ojs_id = ""
        qs = self.resourceid_set.filter(id_type="ojs-id")
        if qs.count() > 0:
            resourceid = qs.first()
            ojs_id = resourceid.id_value
        pos = ojs_id.find("$$")
        if pos > 0:
            ojs_id = ojs_id[0:pos]
        return ojs_id

    def update_bibtex_with_book_contributors(
        self, bibtex, collection, container, indent, author_names, editor_names
    ):
        if container is not None:
            append_in_latex(bibtex, indent + "booktitle = {" + container.title_tex + "},")

            # @incollection (edited-books): add the book editors
            book_author_names = get_bibtex_names(container, "author")
            book_editor_names = get_bibtex_names(container, "editor")

            if not author_names and book_author_names:
                append_in_latex(bibtex, indent + book_author_names)
            if not editor_names and book_editor_names:
                append_in_latex(bibtex, indent + book_editor_names)

        if collection is not None:
            append_in_latex(bibtex, indent + "series = {" + collection.title_tex + "},")

    def get_bibtex(self, request=None, is_title=True):
        """

        :param self:
        :return: string encoded in latex (with latexcodec)
        """
        bibtex = []
        indent = "     "

        container = self.my_container
        collection = self.get_collection()

        is_article = True
        is_incollection = False
        is_inbook = False
        is_phdthesis = False

        if container is not None and container.ctype != "issue":
            is_article = False

            if collection is not None and collection.coltype == "these":
                is_phdthesis = True
            elif container.ctype == "book-monograph":
                is_inbook = True
            elif container.ctype in ["book-edited-book", "lecture-notes"]:
                is_incollection = True
        elif collection.coltype == "proceeding":
            is_incollection = True

        to_appear = container.to_appear()
        is_cr = container.is_cr()

        # No bibtex at the article level for a these
        if is_phdthesis:
            return ""

        author_names = get_bibtex_names(self, "author")
        editor_names = get_bibtex_names(self, "editor")

        type_ = "article"
        if to_appear and not is_cr:
            type_ = "unpublished"
        elif is_inbook:
            type_ = "inbook"
        elif is_incollection:
            type_ = "incollection"
        elif len(author_names) == 0 and len(editor_names) == 0:
            type_ = "misc"

        # Numdam meeting: Use the resource pid as the bibtex id => No latex encoding to keep the '_'
        bibtex.append("@" + type_ + "{" + self.pid + ",")

        if author_names:
            append_in_latex(bibtex, indent + author_names)
        if editor_names:
            append_in_latex(bibtex, indent + editor_names)

        title = xml_utils.normalise_span(self.title_tex)
        append_in_latex(bibtex, indent + "title = {" + title + "},", is_title=is_title)

        if is_article and not is_incollection:
            title = xml_utils.normalise_span(collection.title_tex)
            append_in_latex(bibtex, indent + "journal = {" + title + "},")
        elif is_article:
            title = xml_utils.normalise_span(container.title_tex)
            append_in_latex(bibtex, indent + "booktitle = {" + title + "},")
            title = xml_utils.normalise_span(collection.title_tex)
            append_in_latex(bibtex, indent + "series = {" + title + "},")
        else:
            self.update_bibtex_with_book_contributors(
                bibtex, collection, container, indent, author_names, editor_names
            )

        if not to_appear:
            if self.talk_number:
                append_in_latex(bibtex, indent + "note = {talk:" + self.talk_number + "},")
            if self.article_number:
                append_in_latex(bibtex, indent + "eid = {" + self.article_number + "},")
            if self.pages():
                append_in_latex(bibtex, indent + "pages = {" + self.pages(for_bibtex=True) + "},")

        hostname = request.get_host() if request is not None else ""
        scheme = request.scheme if request is not None else "https"
        self.update_bibtex_with_commons(bibtex, container, hostname, scheme, indent)

        append_in_latex(bibtex, "}")

        return "\n".join(bibtex)

    def get_ris(self, request=None):
        """

        :param self:
        :return: string
        """
        items = []
        sep = "  - "

        container = self.my_container
        collection = self.get_collection()

        is_article = True
        is_incollection = False
        is_inbook = False
        is_phdthesis = False

        if container is not None and container.ctype != "issue":
            is_article = False

            if collection is not None and collection.coltype == "these":
                is_phdthesis = True
            elif container.ctype == "book-monograph":
                is_inbook = True
            elif container.ctype == "book-edited-book":
                is_incollection = True
            elif container.ctype == "lecture-notes":
                is_incollection = True

        to_appear = container.to_appear()
        is_cr = container.is_cr()

        # no citation at the article level for a these
        if is_phdthesis:
            return ""

        type_ = "JOUR"  # "article"
        if to_appear and not is_cr:
            type_ = "UNPB"  # "unpublished"
        elif is_inbook:
            type_ = "CHAP"
        elif is_incollection:
            type_ = "CHAP"
        items.append("TY" + sep + type_)

        author_names = get_names(self, CONTRIB_TYPE_AUTHOR)
        for author in author_names:
            items.append("AU" + sep + author)

        editor_names = get_names(self, CONTRIB_TYPE_EDITOR)
        for editor in editor_names:
            items.append("ED" + sep + editor)

        title = xml_utils.remove_html(self.title_tex)
        items.append("TI" + sep + title)

        collection_title = xml_utils.remove_html(collection.title_tex)
        if collection is not None and is_article:
            items.append("JO" + sep + collection_title)
        else:
            if container is not None:
                items.append("BT" + sep + container.title_tex)
                author_names = get_names(container, CONTRIB_TYPE_AUTHOR)
                for author in author_names:
                    items.append("AU" + sep + author)

                editor_names = get_names(container, CONTRIB_TYPE_EDITOR)
                for editor in editor_names:
                    items.append("ED" + sep + editor)
            if collection is not None:
                items.append("T3" + sep + collection_title)

        if not to_appear:
            if self.talk_number:
                items.append("N1" + sep + "talk:" + self.talk_number)
            # if self.article_number:
            #     items.append("M1" + sep + "eid = " + self.article_number)

        hostname = request.get_host() if request is not None else ""
        scheme = request.scheme if request is not None else "https"
        self.update_ris_with_commons(items, container, hostname, scheme, sep)
        return "\r\n".join(items)

    def get_endnote(self, request=None):
        """

        :param self:
        :return: string
        """
        items = []
        sep = " "

        container = self.my_container
        collection = self.get_collection()

        is_article = True
        is_incollection = False
        is_inbook = False
        is_phdthesis = False

        if container is not None and container.ctype != "issue":
            is_article = False

            if collection is not None and collection.coltype == "these":
                is_phdthesis = True
            elif container.ctype == "book-monograph":
                is_inbook = True
            elif container.ctype == "book-edited-book":
                is_incollection = True
            elif container.ctype == "lecture-notes":
                is_incollection = True

        to_appear = container.to_appear()
        is_cr = container.is_cr()

        # no citation at the article level for a these
        if is_phdthesis:
            return ""

        type_ = "Journal Article"  # "article"
        if to_appear and not is_cr:
            type_ = "Unpublished Work"  # "unpublished"
        elif is_inbook:
            type_ = "Book Section"
        elif is_incollection:
            type_ = "Book Section"
        items.append("%0" + sep + type_)

        author_names = get_names(self, CONTRIB_TYPE_AUTHOR)
        for author in author_names:
            items.append("%A" + sep + author)

        editor_names = get_names(self, CONTRIB_TYPE_EDITOR)
        for editor in editor_names:
            items.append("%E" + sep + editor)

        title = xml_utils.remove_html(self.title_tex)
        items.append("%T" + sep + title)

        collection_title = xml_utils.remove_html(collection.title_tex)
        if collection is not None and is_article:
            items.append("%J" + sep + collection_title)
        else:
            if container is not None:
                items.append("%B" + sep + container.title_tex)
                author_names = get_names(container, CONTRIB_TYPE_AUTHOR)
                for author in author_names:
                    items.append("%A" + sep + author)

                editor_names = get_names(container, CONTRIB_TYPE_EDITOR)
                for editor in editor_names:
                    items.append("%E" + sep + editor)
            if collection is not None:
                items.append("%S" + sep + collection_title)

        if not to_appear:
            if self.talk_number:
                items.append("%Z" + sep + "talk:" + self.talk_number)
            # if self.article_number:
            #     items.append("%1" + sep + "eid = " + self.article_number)

        hostname = request.get_host() if request is not None else ""
        scheme = request.scheme if request is not None else "https"
        self.update_endnote_with_commons(items, container, hostname, scheme, sep)
        return "\r\n".join(items)

    def get_conference(self):
        text = ""

        subjs = []
        for subj in self.subj_set.all():
            if subj.type == "conference":
                subjs.append(subj.value)

        text = ", ".join(subjs)

        return text

    def get_topics(self):
        text = ""

        subjs = []
        for subj in self.subj_set.all():
            if subj.type == "topic":
                subjs.append(subj.value)

        text = ", ".join(subjs)

        return text

    def get_subj_text(self):
        text = ""
        lang = get_language()

        subj_types = ["subject", "type", "pci", "heading", "conference"]
        if self.my_container.my_collection.pid == "CRMATH":
            subj_types = ["subject", "heading"]

        subj_groups = self.get_subjs_by_type_and_lang()
        for type_ in subj_types:
            if type_ in subj_groups:
                sg_type_langs = subj_groups[type_]
                if type_ == "pci":
                    subtext = ", ".join(
                        list(
                            [
                                resolver.get_pci(subj.value)
                                for lang_ in sg_type_langs
                                for subj in sg_type_langs[lang_]
                            ]
                        )
                    )
                else:
                    if lang in sg_type_langs:
                        subtext = ", ".join(
                            list([subj.value for subj in subj_groups[type_][lang]])
                        )
                    else:
                        subtext = ", ".join(
                            list(
                                [
                                    subj.value
                                    for lang_ in sg_type_langs
                                    if lang_ != lang
                                    for subj in sg_type_langs[lang_]
                                ]
                            )
                        )

                if text:
                    text += " - "
                text += subtext

        return text

    def get_pci_section(self):
        pci = ""
        for subj in self.subj_set.all():
            if subj.type == "pci":
                pci = subj.value

        return pci

    def get_pci_value(self):
        return resolver.get_pci(self.get_pci_section())

    def is_uga_pci(self):
        return self.get_pci_section() in resolver.PCJ_UGA_SECTION

    def allow_crossref(self):
        # we need at least a doi or an issn to allow crossref record
        doi = self.my_container.my_collection.doi
        issn = self.my_container.my_collection.issn
        e_issn = self.my_container.my_collection.e_issn
        result = bool(doi) or bool(issn) or bool(e_issn)

        # and we need a published date (online_first or final)
        result = result and (self.date_published is not None or self.date_online_first is not None)

        return result

    def get_illustrations(self):
        return GraphicalAbstract.objects.filter(resource=self).first()

    def has_graphical_abstract(self):
        collections = ["CRCHIM"]
        return True if self.my_container.my_collection.pid in collections else False


class ResourceCategory(models.Model):
    """non utilisé"""

    category = models.CharField(max_length=32, db_index=True)


class Provider(models.Model):
    """
    en faire une resource permettrait d'attacher des metadonnées
    supplémentaires -- à voir
    """

    name = models.CharField(max_length=32, unique=True)
    pid_type = models.CharField(max_length=32, unique=True)
    sid_type = models.CharField(max_length=64, unique=True, null=True, blank=True)

    def __str__(self):
        return self.name


class SiteMembership(models.Model):
    """
    Warning: As of July 3018, only 1 site id is stored in a SolR document
        Although the SolR schema is already OK to store multiple sites ("sites" is an array)
        no Solr commands have been written to add/remove sites
        We only have add commands.
        Search only works if the Solr instance is meant for individual or ALL sites
    """

    resource = models.ForeignKey(Resource, on_delete=models.CASCADE)
    deployed = models.DateTimeField(null=True)
    online = models.DateTimeField(db_index=True, null=True)  # pour cedram
    site = models.ForeignKey(PtfSite, on_delete=models.CASCADE)

    class Meta:
        unique_together = (
            "resource",
            "site",
        )


class CollectionMembership(models.Model):
    collection = models.ForeignKey(Collection, on_delete=models.CASCADE)
    container = models.ForeignKey(Container, on_delete=models.CASCADE)

    vseries = models.CharField(max_length=32, db_index=True)
    volume = models.CharField(max_length=64, db_index=True)
    number = models.CharField(max_length=32, db_index=True)  # issue number
    # la même chose pour le tri
    vseries_int = models.IntegerField(default=0, db_index=True)
    volume_int = models.IntegerField(default=0, db_index=True)
    number_int = models.IntegerField(default=0, db_index=True)

    seq = models.IntegerField(db_index=True)

    class Meta:
        unique_together = (
            "collection",
            "container",
        )

    def __str__(self):
        return f"{self.collection} - {self.container}"


class RelationName(models.Model):
    """
    Triple store ;-)
    """

    left = models.CharField(max_length=32, unique=True)
    right = models.CharField(max_length=32, unique=True)
    gauche = models.CharField(max_length=64, unique=True)
    droite = models.CharField(max_length=64, unique=True)


class Relationship(models.Model):
    resource = models.ForeignKey(
        Resource, null=True, related_name="subject_of", on_delete=models.CASCADE
    )
    related = models.ForeignKey(
        Resource, null=True, related_name="object_of", on_delete=models.CASCADE
    )
    subject_pid = models.CharField(max_length=64, db_index=True)
    object_pid = models.CharField(max_length=64, db_index=True)
    rel_info = models.ForeignKey(RelationName, null=True, on_delete=models.CASCADE)


class ExtRelationship(models.Model):
    """
    Triple store (resources externes)
    """

    resource = models.ForeignKey(Resource, related_name="subject", on_delete=models.CASCADE)
    ext_object = models.CharField(max_length=256)  # uri
    predicate = models.CharField(max_length=256, db_index=True)  # predicate uri


class XmlBase(models.Model):
    base = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.base


class DataStream(models.Model):
    resource = models.ForeignKey(Resource, on_delete=models.CASCADE)
    rel = models.CharField(max_length=32)
    mimetype = models.CharField(max_length=32)
    base = models.ForeignKey(XmlBase, blank=True, null=True, on_delete=models.CASCADE)
    location = models.URLField()
    text = models.CharField(max_length=32, default="Link")
    seq = models.IntegerField(db_index=True)

    class Meta:
        ordering = ["seq"]

    def __str__(self):
        return " - ".join([self.resource.pid, self.mimetype])


class ExtLink(models.Model):
    resource = models.ForeignKey(Resource, on_delete=models.CASCADE)
    rel = models.CharField(max_length=50)
    mimetype = models.CharField(max_length=32)
    base = models.ForeignKey(XmlBase, blank=True, null=True, on_delete=models.CASCADE)
    location = models.CharField(max_length=300)
    metadata = models.TextField()
    seq = models.IntegerField(db_index=True)

    class Meta:
        ordering = ["seq"]

    def __str__(self):
        return f"{self.rel}: {self.get_href()}"

    def save(self, *args, **kwargs):
        if "website" in self.rel:
            self.metadata = "website"
        if not self.seq:
            self.seq = self.generate_seq()
        super().save(*args, **kwargs)

    def generate_seq(self):
        max_seq = ExtLink.objects.filter(resource=self.resource).aggregate(Max("seq"))["seq__max"]
        if max_seq:
            return max_seq + 1
        else:
            return 1

    def get_href(self):
        if self.rel in ("small_icon", "icon"):
            # construction du chemin vers l'icone
            # filename = os.path.basename(self.location)
            resource_pid = self.resource.pid
            href = resolver.get_icon_url(resource_pid, self.location)
            return href
        return self.location


class RelatedObject(models.Model):
    """
    Related Objects are used to store pdf/djvu related to an issue (tdm, preliminary pages)
    """

    resource = models.ForeignKey(Resource, on_delete=models.CASCADE)
    rel = models.CharField(max_length=32)
    mimetype = models.CharField(max_length=32)
    base = models.ForeignKey(XmlBase, blank=True, null=True, on_delete=models.CASCADE)
    location = models.CharField(max_length=200)
    metadata = models.TextField()
    seq = models.IntegerField(db_index=True)

    class Meta:
        ordering = ["seq"]

    def __str__(self):
        return " - ".join([self.resource.pid, self.mimetype])

    def get_href(self):
        the_dynamic_object = self.resource.cast()
        href = the_dynamic_object.get_binary_file_href_full_path(
            self.rel, self.mimetype, self.location
        )
        return href


class SupplementaryMaterial(RelatedObject):
    caption = models.TextField()

    def __str__(self):
        return self.location.split("/")[-1]

    def embeded_link(self):
        if "youtube" in self.location:
            video_id = urlparse(self.location).query.split("=")[1]
            return f"https://www.youtube-nocookie.com/embed/{video_id}"
        return self.get_href()


class MetaDataPart(models.Model):
    """
    stockage des metadonnées
    qui ne sont pas aiilleurs (!)
    non utilisé - à voir
    3 classes qui font sans doute doublon:
    MetadataPart, ResourceAttribute, CustomMeta -- à voir
    """

    resource = models.ForeignKey(Resource, on_delete=models.CASCADE)
    name = models.CharField(max_length=32, db_index=True)
    seq = models.IntegerField(db_index=True)
    data = models.TextField()

    class Meta:
        ordering = ["seq"]


class ResourceAttribute(models.Model):
    """
    not used
    """

    resource = models.ForeignKey(Resource, on_delete=models.CASCADE)
    name = models.CharField(max_length=32, db_index=True)
    value = models.TextField()


class CustomMeta(models.Model):
    resource = models.ForeignKey(Resource, on_delete=models.CASCADE)
    name = models.CharField(max_length=32, db_index=True)
    value = models.CharField(max_length=128, db_index=True)


class ResourceId(models.Model):
    resource = models.ForeignKey(Resource, on_delete=models.CASCADE)
    id_type = models.CharField(max_length=32, db_index=True)
    id_value = models.CharField(max_length=64, db_index=True)

    class Meta:
        unique_together = ("id_type", "id_value")

    def __str__(self):
        return f"{self.id_type} {self.id_value}"

    def get_href(self):
        href = ""
        if self.id_type == "doi":
            href = resolver.get_doi_url(self.id_value)
        return href


class ExtId(models.Model):
    """
    zbl, mr, jfm, etc..
    mis à part car non uniques
    """

    resource = models.ForeignKey(Resource, on_delete=models.CASCADE)
    id_type = models.CharField(max_length=32, db_index=True)
    id_value = models.CharField(max_length=64, db_index=True)
    checked = models.BooleanField(default=True)
    false_positive = models.BooleanField(default=False)

    class Meta:
        unique_together = ["resource", "id_type"]

    def __str__(self):
        return f"{self.resource} - {self.id_type}:{self.id_value}"

    def get_href(self):
        return resolver.resolve_id(self.id_type, self.id_value)


class Abstract(models.Model):
    resource = models.ForeignKey(Resource, on_delete=models.CASCADE)
    lang = models.CharField(max_length=3, default="und")
    tag = models.CharField(max_length=32, db_index=True)
    # specific use, content_type, label ?
    seq = models.IntegerField(db_index=True)
    value_xml = models.TextField(default="")
    value_tex = models.TextField(default="")
    value_html = models.TextField(default="")

    class Meta:
        ordering = ["seq"]

    def __str__(self):
        return f"{self.resource} - {self.tag}"


class Kwd(models.Model):
    resource = models.ForeignKey(Resource, on_delete=models.CASCADE)
    lang = models.CharField(max_length=3, default="und")
    type = models.CharField(max_length=32, db_index=True)
    value = models.TextField(default="")
    seq = models.IntegerField(db_index=True)

    class Meta:
        ordering = ["seq"]

    def __str__(self):
        return f"{self.type} - {self.lang} - {self.value}"


class Subj(models.Model):
    resource = models.ForeignKey(Resource, on_delete=models.CASCADE)
    lang = models.CharField(max_length=3, default="und")
    type = models.CharField(max_length=32, db_index=True)
    value = models.TextField(default="")
    seq = models.IntegerField(db_index=True)

    class Meta:
        ordering = ["seq"]

    def __str__(self):
        return f"{self.type} - {self.lang} - {self.value}"


class Award(models.Model):
    resource = models.ForeignKey(Resource, on_delete=models.CASCADE)
    abbrev = models.CharField(max_length=600, db_index=True)
    award_id = models.CharField(max_length=600, db_index=True)
    seq = models.IntegerField(db_index=True)

    class Meta:
        ordering = ["seq"]


class BibItem(models.Model):
    resource = models.ForeignKey(Resource, on_delete=models.CASCADE)
    sequence = models.PositiveIntegerField(db_index=True)
    label = models.CharField(max_length=128, default="")
    citation_xml = models.TextField(default="")
    citation_tex = models.TextField(default="")
    citation_html = models.TextField(default="")

    type = models.CharField(max_length=32, default="", db_index=True)
    user_id = models.TextField(default="")

    # article_title, chapter_title and source can contain formulas. Save the tex version.
    # artile_title, chapter_title and source were created from title, booktitle and chapter.
    # We need to do reverse engineering when exporting the bibtex (see
    # get_bibtex)
    article_title_tex = models.TextField(default="")
    chapter_title_tex = models.TextField(default="")
    source_tex = models.TextField(default="")

    publisher_name = models.TextField(default="")
    publisher_loc = models.TextField(default="")
    institution = models.TextField(default="")
    series = models.TextField(default="")
    volume = models.TextField(default="")
    issue = models.TextField(default="")
    month = models.CharField(max_length=32, default="")
    year = models.TextField(default="")
    comment = models.TextField(default="")
    annotation = models.CharField(max_length=255, default="")
    fpage = models.TextField(default="")
    lpage = models.TextField(default="")
    page_range = models.TextField(default="")
    size = models.TextField(default="")

    class Meta:
        ordering = ["sequence"]

    def __str__(self):
        return f"{self.resource} - {self.label}"

    def get_ref_id(self):
        ref_id = ""
        # self.resource.body_html is for PCJ case when SHOW_BODY is at false but we have a body html to display
        if (hasattr(settings, "SHOW_BODY") and settings.SHOW_BODY) or self.resource.body_html:
            ref_id = "r" + str(self.sequence)
        else:
            ref_id = self.user_id
        return ref_id

    def get_doi(self):
        bibitemId_doi = BibItemId.objects.filter(bibitem=self, id_type="doi")
        if bibitemId_doi:
            return bibitemId_doi.get().id_value
        return None

    def get_bibtex(self):
        """

        :param self:
        :return: an array of strings encoded in latex (with latexcodec)
        """
        bibtex = []
        indent = "     "

        author_names = get_bibtex_names(self, "author")
        editor_names = get_bibtex_names(self, "editor")

        if self.user_id:
            id_ = self.user_id
        else:
            id_ = get_bibtex_id(self, self.year)

        append_in_latex(bibtex, "@" + self.type + "{" + id_ + ",")

        if author_names:
            append_in_latex(bibtex, indent + author_names)
        if editor_names:
            append_in_latex(bibtex, indent + editor_names)

        # From numdam+/ptf-xsl/cedram/structured-biblio.xsl
        # < xsl:template match = "bib_entry[@doctype='article']/title|
        #                         bib_entry[@doctype='misc']/title|
        #                         bib_entry[@doctype='inproceedings']/title|
        #                         bib_entry[@doctype='booklet']/title|
        #                         bib_entry[@doctype='conference']/title" >
        # => All article_title in JATS become title in bibtex
        if self.article_title_tex:
            title = xml_utils.normalise_span(self.article_title_tex)
            append_in_latex(bibtex, indent + "title = {" + title + "},", is_title=True)

        # <chapter-title> in JATS becomes title or chapter according to the type
        # From numdam+/ptf-xsl/cedram/structured-biblio.xsl
        # <xsl:template match="bib_entry[@doctype='incollection']/title|bibentry/chapter">

        if self.chapter_title_tex:
            keyword = ""
            if self.type == "incollection":
                keyword += "title"
            else:
                keyword += "chapter"
            title = xml_utils.normalise_span(self.chapter_title_tex)
            append_in_latex(bibtex, indent + keyword + " = {" + title + "},")

        # <source> in JATS becomes title, journal, booktitle, or howpublished according to the type
        # From numdam+/ptf-xsl/cedram/structured-biblio.xsl
        # <xsl:template match="bib_entry[@doctype='article']/journal|
        #                      bib_entry[@doctype='incollection']/booktitle|
        #                      bib_entry[@doctype='inproceedings']/booktitle|
        #                      bib_entry[@doctype='conference']/booktitle|
        #                      bib_entry[@doctype='misc']/howpublished|
        #                      bib_entry[@doctype='booklet']/howpublished|
        #                      bib_entry[@doctype='book']/title|
        #                      bib_entry[@doctype='unpublished']/title|
        #                      bib_entry[@doctype='inbook']/title|
        #                      bib_entry[@doctype='phdthesis']/title|
        #                      bib_entry[@doctype='mastersthesis']/title|
        #                      bib_entry[@doctype='manual']/title|
        #                      bib_entry[@doctype='techreport']/title|
        #                      bib_entry[@doctype='masterthesis']/title|
        #                      bib_entry[@doctype='coursenotes']/title|
        #                      bib_entry[@doctype='proceedings']/title">
        # < xsl:template match = "bib_entry[@doctype='misc']/booktitle" > : Unable to reverse correctly ! 2 choices

        if self.source_tex:
            keyword = ""
            if self.type == "article":
                keyword += "journal"
            elif (
                self.type == "incollection"
                or self.type == "inproceedings"
                or self.type == "conference"
            ):
                keyword += "booktitle"
            elif self.type == "misc" or self.type == "booklet":
                keyword += "howpublished"
            else:
                keyword += "title"
            title = xml_utils.normalise_span(self.source_tex)
            append_in_latex(bibtex, indent + keyword + " = {" + title + "},")

        if self.publisher_name:
            append_in_latex(bibtex, indent + "publisher = {" + self.publisher_name + "},")
        if self.publisher_loc:
            append_in_latex(bibtex, indent + "address = {" + self.publisher_loc + "},")
        if self.institution:
            append_in_latex(bibtex, indent + "institution = {" + self.institution + "},")
        if self.series:
            append_in_latex(bibtex, indent + "series = {" + self.series + "},")
        if self.volume:
            append_in_latex(bibtex, indent + "volume = {" + self.volume + "},")
        if self.issue:
            append_in_latex(bibtex, indent + "number = {" + self.issue + "},")
        if self.year:
            append_in_latex(bibtex, indent + "year = {" + self.year + "},")

        if self.page_range:
            append_in_latex(bibtex, indent + "pages = {" + self.page_range + "},")
        elif self.fpage or self.lpage:
            text = self.fpage
            if self.fpage and self.lpage:
                text += "--"
            if self.lpage:
                text += self.lpage
            append_in_latex(bibtex, indent + "pages = {" + text + "},")

        if self.size:
            append_in_latex(bibtex, indent + "pagetotal = {" + self.size + "},")

        if self.comment:
            append_in_latex(bibtex, indent + "note = {" + self.comment + "},")

        for extid in BibItemId.objects.filter(bibitem=self):
            type_ = ""
            if extid.id_type == "zbl-item-id":
                type_ = "zbl"
            elif extid.id_type == "mr-item-id":
                type_ = "mrnumber"
            elif extid.id_type == "doi":
                type_ = "doi"
            elif extid.id_type == "eid":
                type_ = "eid"

            if type_:
                append_in_latex(bibtex, indent + type_ + " = {" + extid.id_value + "},")

        append_in_latex(bibtex, "}")

        return bibtex


class BibItemId(models.Model):
    bibitem = models.ForeignKey(BibItem, on_delete=models.CASCADE)
    id_type = models.CharField(max_length=32, db_index=True)
    id_value = models.CharField(max_length=256, db_index=True)
    checked = models.BooleanField(default=True)
    false_positive = models.BooleanField(default=False)

    class Meta:
        unique_together = ["bibitem", "id_type"]

    def __str__(self):
        return f"{self.bibitem} - {self.id_type}:{self.id_value}"

    def get_href_display(self):
        value = "Article"
        if settings.SITE_ID == 3:
            value = "Numdam"
        else:
            if self.id_type in ["numdam-id", "mathdoc-id"]:
                value = "Numdam"

        return value

    def get_href(self):
        force_numdam = False
        if self.id_type in ["numdam-id", "mathdoc-id"] and settings.SITE_ID != 3:
            force_numdam = True

        return resolver.resolve_id(self.id_type, self.id_value, force_numdam)


class ContribGroup(models.Model):
    resource = models.ForeignKey(Resource, blank=True, null=True, on_delete=models.CASCADE)
    bibitem = models.ForeignKey(BibItem, blank=True, null=True, on_delete=models.CASCADE)
    content_type = models.CharField(max_length=32, db_index=True)
    # specific_use ?!
    seq = models.IntegerField()

    class Meta:
        ordering = ["seq"]


class Contrib(models.Model):
    group = models.ForeignKey(ContribGroup, on_delete=models.CASCADE)
    contrib_type = models.CharField(max_length=32, db_index=True)

    last_name = models.CharField(max_length=128, db_index=True)
    first_name = models.CharField(max_length=128, db_index=True)
    prefix = models.CharField(max_length=32)
    suffix = models.CharField(max_length=32)
    string_name = models.CharField(max_length=256, db_index=True)
    reference_name = models.CharField(max_length=256, db_index=True)
    deceased = models.BooleanField(default=False)
    orcid = models.CharField(max_length=64, db_index=True, blank=True, default="")
    equal_contrib = models.BooleanField(default=True)
    email = models.EmailField(max_length=254, db_index=True, blank=True, default="")

    # Could be used to export the contrib
    contrib_xml = models.TextField()

    seq = models.IntegerField()

    class Meta:
        ordering = ["seq"]

    @classmethod
    def get_fields_list(cls):
        return [
            item.attname
            for item in cls._meta.get_fields()
            if not item.auto_created
            and not item.many_to_many
            and not item.many_to_one
            and not item.one_to_many
            and not item.one_to_one
        ]

    def __str__(self):
        return "{} {} / {} / {}".format(
            self.last_name, self.first_name, self.reference_name, self.contrib_type or "None"
        )

    def get_absolute_url(self):
        from ptf.templatetags.helpers import pretty_search

        return pretty_search("search", "c", f'"{self.reference_name()}"')

    def display_name(self):
        display_name = self.string_name

        if self.is_etal():
            display_name = "et al."
        elif getattr(settings, "DISPLAY_FIRST_NAME_FIRST", False) and (
            len(str(self.first_name)) > 0 or len(str(self.last_name)) > 0
        ):
            display_name = f"{self.first_name} {self.last_name}"

        return display_name

    def orcid_href(self):
        return resolver.resolve_id("orcid", self.orcid)

    def idref_href(self):
        return resolver.resolve_id("idref", self.idref)

    def get_addresses(self):
        return self.contribaddress_set.all()  # .order_by('address')

    def is_etal(self):
        return self.contrib_xml.startswith("<etal")


class Author(models.Model):
    """
    Count the number of documents (articles,books...) written by an author
    """

    name = models.CharField(max_length=200, db_index=True, unique=True)
    first_letter = models.CharField(max_length=1, db_index=True)
    count = models.IntegerField()


class FrontMatter(models.Model):
    resource = models.OneToOneField(Resource, on_delete=models.CASCADE)

    value_xml = models.TextField(default="")
    value_html = models.TextField(default="")
    foreword_html = models.TextField(default="")


class LangTable(models.Model):
    code = models.CharField(max_length=3, db_index=True)
    name = models.CharField(max_length=64)


class ResourceCount(models.Model):
    resource = models.ForeignKey(Resource, on_delete=models.CASCADE)
    name = models.CharField(max_length=32, db_index=True)
    value = models.CharField(max_length=32, db_index=True)
    seq = models.IntegerField()

    class Meta:
        ordering = ["seq"]


class Stats(models.Model):
    name = models.CharField(max_length=128, db_index=True, unique=True)
    value = models.IntegerField()


class History(models.Model):
    """pas utilisé pour l'heure"""

    resource = models.ForeignKey(Resource, on_delete=models.CASCADE)
    date_type = models.CharField(max_length=32, db_index=True)
    date_value = models.DateTimeField()
    comment = models.TextField()


class SerialHistory(models.Model):
    """à revoir"""

    serial = models.ForeignKey("Collection", on_delete=models.CASCADE)
    date_deployed = models.DateField(auto_now_add=True)
    first_year = models.CharField(max_length=32)
    last_year = models.CharField(max_length=32)


# def matches(id_type, id_value):
#     if id_type in ('mr-item-id', 'zbl-item-id', 'jfm-item-id'):
#         qs = ExtId.objects.filter(
#             id_type=id_type,
#             id_value=id_value,
#         ).select_related('resource', 'resource__resourceid')
#         counter = 0
#         match_list = []
#         for extid in qs:
#             resource = extid.resource
#             for idext in resource.extid_set.all():
#                 match_list.append({'type': idext.id_type, 'value': idext.id_value})
#             for rid in resource.resourceid_set.all():
#                 match_list.append({'type': rid.id_type, 'value': rid.id_value})
#             counter += 1
#         return {
#             'id': {'type': id_type, 'value': id_value},
#             'count': counter,
#             'ids': match_list,
#         }
#     resource = Resource.objects.get(resourceid__id_type=id_type,
#                                     resourceid__id_value=id_value)
#     counter = 0
#     match_list = []
#     for idext in resource.extid_set.all():
#         match_list.append({'type': idext.id_type, 'value': idext.id_value})
#     for rid in resource.resourceid_set.all():
#         match_list.append({'type': rid.id_type, 'value': rid.id_value})
#     counter += 1
#     return {
#         'id': {'type': id_type, 'value': id_value},
#         'count': counter,
#         'ids': match_list,
#     }


def parse_page_count(page_count):
    """
    page-count is not an integer but a string to be displayed.
    page_count may sometimes mix roman and arabic values. Ex "iv-121"
    """
    if "-" in page_count:
        result = 0
        values = page_count.split("-")
        for value in values:
            try:
                result += int(value)
            except ValueError:
                pass
    else:
        result = int(page_count)

    return result


@receiver(pre_delete, sender=ResourceCount)
def delete_resourcecount_handler(sender, instance, **kwargs):
    """
    pre_delete ResourceCount signal

    Stats are added manually during a addResourceCountDatabaseCmd
    but ResourceCount are deleted automatically by Django when its
    related resource is deleted
    (resource = models.ForeignKey(Resource) of ResourceCount)
    To update Stats, we use the Django signal mechanism
    """
    if instance and instance.name == "page-count" and instance.resource.classname == "Container":
        # page-count may sometimes mix roman and arabic values. Ex "iv-121"
        value = parse_page_count(instance.value)

        total = Stats.objects.get(name=instance.name)
        total.value -= value
        total.save()


# class Volume:
#
#     def get_absolute_url(self):
#         return reverse('volume-items', kwargs={'vid': self.id_})
#
#     def __init__(self, id_):
#         self.id_ = id_
#         try:
#             journal_pid, year_id, self.vseries_id, self.volume_id = id_.split(
#                 '_')
#         except ValueError:
#             raise self.DoesNotExist(_('Volume {} does not exist').format(id_))
#         try:
#             self.collection = Collection.objects.get(
#                 pid=journal_pid, sites__id=settings.SITE_ID)
#         except Collection.DoesNotExist:
#             self.issues = None
#         try:
#             self.issues = Container.objects.filter(
#                 my_collection__pid=journal_pid,
#                 year=year_id,
#                 vseries=self.vseries_id,
#                 volume=self.volume_id,
#             ).order_by('number_int').all()
#         except Container.DoesNotExist:
#             self.issues = None
#
#     class DoesNotExist(Exception):
#         pass


class ContribAddress(models.Model):
    contrib = models.ForeignKey("Contrib", blank=True, null=True, on_delete=models.CASCADE)
    contribution = models.ForeignKey(
        "Contribution", blank=True, null=True, on_delete=models.CASCADE
    )
    address = models.TextField(null=True, blank=True)

    class Meta:
        ordering = ["pk"]


def cmp_container_base(a, b):
    return (
        a.year < b.year
        or (a.year == b.year and a.vseries_int < b.vseries_int)
        or (a.year == b.year and a.vseries_int == b.vseries_int and a.volume_int < b.volume_int)
        or (
            a.year == b.year
            and a.vseries_int == b.vseries_int
            and a.volume_int == b.volume_int
            and a.number_int < b.number_int
        )
    )


class PersonManager(models.Manager):
    @staticmethod
    def clean():
        pass
        # Person.objects.filter(contributions=None).delete()


class Person(models.Model):
    """
    A Person is a contributor (author/editor...) of a Resource (Article/Book) or a BibItem.
    A Person can appear with different names (ex: "John Smith", "J. Smith") and we want to preserve the differences,
    in particular with print papers that are digitized.
    A Person is unique for a person (a Person is basically the key).
    A Person has one or many PersonInfo which stores the different names

    TODO: signal similar to delete_contrib_handler
    """

    # blank=True and null=True allow unique=True with null values (on multiple Persons)
    orcid = models.CharField(max_length=20, blank=True, null=True)
    idref = models.CharField(max_length=10, blank=True, null=True)
    # mid (Mathdoc id) is the key set by numdam-plus
    mid = models.CharField(max_length=256, blank=True, null=True)

    last_name = models.CharField(max_length=128, blank=True, default="")
    first_name = models.CharField(max_length=128, blank=True, default="")
    prefix = models.CharField(max_length=32, blank=True, default="")
    suffix = models.CharField(max_length=32, blank=True, default="")
    first_letter = models.CharField(max_length=1, blank=True, default="")

    # Used when a person is not fully tagged in the XML
    string_name = models.CharField(max_length=256, blank=True, default="")

    objects = PersonManager()

    @classmethod
    def get_fields_list(cls):
        return [
            item.attname
            for item in cls._meta.get_fields()
            if not item.auto_created
            and not item.many_to_many
            and not item.many_to_one
            and not item.one_to_many
            and not item.one_to_one
        ]

    def __str__(self):
        return get_display_name(
            self.prefix, self.first_name, self.last_name, self.suffix, self.string_name
        )


class PersonInfoManager(models.Manager):
    @staticmethod
    def clean():
        PersonInfo.objects.filter(contributions=None).delete()
        Person.objects.filter(personinfo=None).delete()


class PersonInfo(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE)

    last_name = models.CharField(max_length=128)
    first_name = models.CharField(max_length=128)
    prefix = models.CharField(max_length=32)
    suffix = models.CharField(max_length=32)

    # Used when a person is not fully tagged in the XML
    string_name = models.CharField(max_length=256, blank=True, default="")

    objects = PersonInfoManager()

    @classmethod
    def get_fields_list(cls):
        return [
            item.attname
            for item in cls._meta.get_fields()
            if not item.auto_created
            and not item.many_to_many
            and not item.many_to_one
            and not item.one_to_many
            and not item.one_to_one
        ]

    def __str__(self):
        return get_display_name(
            self.prefix, self.first_name, self.last_name, self.suffix, self.string_name
        )


class Contribution(models.Model):
    resource = models.ForeignKey(
        Resource, blank=True, null=True, on_delete=models.CASCADE, related_name="contributions"
    )
    bibitem = models.ForeignKey(
        BibItem, blank=True, null=True, on_delete=models.CASCADE, related_name="contributions"
    )

    # blank=True and null=True allow unique=True with null values (on multiple Persons)
    orcid = models.CharField(max_length=20, blank=True, null=True)
    idref = models.CharField(max_length=10, blank=True, null=True)
    # mid (Mathdoc id) is the key set by numdam-plus
    mid = models.CharField(max_length=256, blank=True, null=True)

    last_name = models.CharField(max_length=128, blank=True, default="")
    first_name = models.CharField(max_length=128, blank=True, default="")
    prefix = models.CharField(max_length=32, blank=True, default="")
    suffix = models.CharField(max_length=32, blank=True, default="")
    first_letter = models.CharField(max_length=1, blank=True, default="")

    # Used when a person is not fully tagged in the XML
    string_name = models.CharField(max_length=256, blank=True, default="")

    role = models.CharField(max_length=64)
    email = models.EmailField(max_length=254, blank=True, default="")
    deceased_before_publication = models.BooleanField(default=False)
    equal_contrib = models.BooleanField(default=True)
    corresponding = models.BooleanField(default=False)

    # Used to export the contribution
    contrib_xml = models.TextField()

    seq = models.IntegerField()

    class Meta:
        ordering = ["seq"]

    @classmethod
    def get_fields_list(cls):
        return [
            item.attname
            for item in cls._meta.get_fields()
            if not item.auto_created
            and not item.many_to_many
            and not item.many_to_one
            and not item.one_to_many
            and not item.one_to_one
        ]

    def __str__(self):
        return self.display_name()

    def is_etal(self):
        return self.contrib_xml.startswith("<etal")

    def display_name(self):
        return (
            "et al."
            if self.is_etal()
            else get_display_name(
                self.prefix, self.first_name, self.last_name, self.suffix, self.string_name
            )
        )

    def get_absolute_url(self):
        from ptf.templatetags.helpers import pretty_search

        return pretty_search("search", "c", f'"{self.display_name()}"')

    def orcid_href(self):
        return resolver.resolve_id("orcid", self.orcid)

    def idref_href(self):
        return resolver.resolve_id("idref", self.idref)

    def is_equal(self, contribution):
        """
        return True if the contribution is identical, based on orcif/idref or homonimy
        TODO: override the __eq__ operator ? Not sure since homonimy is not bullet proof
        """
        equal = False
        if self.orcid and self.orcid == contribution.orcid:
            equal = True
        elif self.idref and self.idref == contribution.idref:
            equal = True
        else:
            equal = self.display_name() == contribution.display_name()

        return True
        return equal


@receiver(pre_delete, sender=Contribution)
def delete_contrib_handler(sender, instance, **kwargs):
    """
    pre_delete Contrib signal

    Contrib and Author are added manually during a
    addResourceDatabaseCmd (mainly addArticleDatabaseCmd)
    but Contrib are deleted automatically by Django when its
    related resource is deleted
    (resource = models.ForeignKey(Resource) of ContribGroup)
    To update Author, we use the Django signal mechanism
    """
    if instance and instance.role == "author":
        try:
            ref_name = instance.mid if instance.mid else str(instance)
            author = Author.objects.get(name=ref_name)
            author.count -= 1
            if author.count == 0:
                author.delete()
            else:
                author.save()
        except Author.DoesNotExist:
            pass


def get_names(item, role):
    """
    item: resource or bibitem
    """
    return [
        str(contribution) for contribution in item.contributions.all() if contribution.role == role
    ]


def are_all_equal_contrib(contributions):
    if len(contributions) == 0:
        return False

    are_all_equal = True
    for contribution in contributions:
        are_all_equal = are_all_equal and contribution.equal_contrib

    return are_all_equal


def are_all_false_equal_contrib(contributions):
    are_all_equal = True
    for contribution in contributions:
        are_all_equal = are_all_equal and not contribution.equal_contrib

    return are_all_equal


class ResourceInSpecialIssue(models.Model):
    my_container = models.ForeignKey(
        Container, null=False, related_name="resources_in_special_issue", on_delete=models.CASCADE
    )
    resource = models.ForeignKey(
        Resource, null=True, related_name="used_in_special_issues", on_delete=models.CASCADE
    )
    resource_doi = models.CharField(max_length=64, unique=True)
    seq = models.IntegerField(db_index=True)
    citation = models.TextField(null=True, blank=True)

    def __str__(self):
        if self.resource and hasattr(self.resource, "doi"):
            return f"{self.resource.doi}, {self.my_container}"


class RelatedArticles(models.Model):
    resource = models.ForeignKey(Resource, null=True, blank=True, on_delete=models.CASCADE)
    # Used during reimport/redeploy to find back the RelatedArticles
    resource_doi = models.CharField(max_length=64, unique=True, null=True, blank=True)
    date_modified = models.DateTimeField(null=True, blank=True)
    doi_list = models.TextField(null=True, blank=True)
    exclusion_list = models.TextField(null=True, blank=True)
    automatic_list = models.BooleanField(default=True)

    def __str__(self):
        if self.resource and hasattr(self.resource, "doi"):
            return f"{self.resource.doi}"


def image_path_graphical_abstract(instance, filename):
    path = "images"
    return os.path.join(path, str(instance.id), "graphical_abstract", filename)


def image_path_illustration(instance, filename):
    path = "images"
    return os.path.join(path, str(instance.id), "illustration", filename)


class OverwriteStorage(FileSystemStorage):
    def get_available_name(self, name, max_length=None):
        if self.exists(name):
            os.remove(os.path.join(settings.MEDIA_ROOT, name))
        return name


class GraphicalAbstract(models.Model):
    resource = models.ForeignKey(Resource, null=True, blank=True, on_delete=models.CASCADE)
    # Used during reimport/redeploy to find back the GraphicalAbstracts
    resource_doi = models.CharField(max_length=64, unique=True, null=True, blank=True)
    date_modified = models.DateTimeField(null=True, blank=True)
    graphical_abstract = models.ImageField(
        upload_to=image_path_graphical_abstract, blank=True, null=True, storage=OverwriteStorage
    )
    illustration = models.ImageField(
        upload_to=image_path_illustration, blank=True, null=True, storage=OverwriteStorage
    )

    def __str__(self):
        if self.resource and hasattr(self.resource, "doi"):
            return f"{self.resource.doi}"


def backup_obj_not_in_metadata(article):
    """
    When you addArticleXmlCmd in the PTF, the XML does not list objects like GraphicalAbstract or RelatedArticles.
    Since addArticleXmlCmd deletes/re-creates the article, we need to "backup" these objects.
    To do so, we delete the relation between article and the object,
    so that the object is not deleted when the article is deleted.

    You need to call restore_obj_not_in_metadata after the re-creation of the article.
    """
    for class_name in ["GraphicalAbstract", "RelatedArticles", "ResourceInSpecialIssue"]:
        qs = globals()[class_name].objects.filter(resource=article)
        if qs.exists():
            obj = qs.first()
            obj.resource_doi = article.doi
            obj.resource = None
            obj.save()


def backup_translation(article):
    """
    Translated articles are the article JATS XML, but not in the Cedrics XML
    We need to "backup" existing translations when importing a Cedrics Article.
    To do so, we delete the relation between article and the translation,
    so that the translation is not deleted when the article is deleted.

    You need to call restore_translation after the re-creation of the article
    """
    qs = TranslatedArticle.objects.filter(original_article=article)
    if qs.exists():
        obj = qs.first()
        obj.original_article_doi = article.doi
        obj.original_article = None
        obj.save()


def restore_obj_not_in_metadata(article):
    for class_name in ["GraphicalAbstract", "RelatedArticles", "ResourceInSpecialIssue"]:
        qs = globals()[class_name].objects.filter(resource_doi=article.doi, resource__isnull=True)
        if qs.exists():
            obj = qs.first()
            obj.resource = article
            obj.save()


def restore_translation(article):
    qs = TranslatedArticle.objects.filter(
        original_article_doi=article.doi, original_article__isnull=True
    )
    if qs.exists():
        obj = qs.first()
        obj.original_article = article
        obj.save()


class TranslatedArticle(Article):
    original_article = models.ForeignKey(
        Article, null=True, blank=True, on_delete=models.CASCADE, related_name="translations"
    )
    # Used during reimport/redeploy in Trammel to find back the TranslatedArticle
    original_article_doi = models.CharField(max_length=64, unique=True, null=True, blank=True)

    def get_year(self):
        return self.original_article.get_year()

    def get_ojs_id(self):
        return self.original_article.get_ojs_id()

    def get_binary_file_href_full_path(self, doctype, mimetype, location):
        """
        Returns an encoded URL to the pdf of a translated article

        Input: doctype: language (ex: 'fr', 'en'
               mimetype: 'application/pdf'
               location is ignored

        Ex: /article/fr/10.5802/crmath.100.pdf
        """

        if mimetype != "application/pdf":
            return ""

        pid = (
            self.original_article.doi
            if self.original_article.doi is not None
            else self.original_article.pid
        )
        extension = "pdf"
        href = reverse(
            "article-translated-pdf",
            kwargs={"pid": pid, "extension": extension, "binary_file_type": self.lang},
        )

        return href

    def get_citation(self, request=None):
        citation = ""

        translator_names = get_names(self, "translator")
        citation += "; ".join(translator_names)

        if citation:
            if not citation.endswith("."):
                citation += ". "
            citation += " "

        if self.lang == self.original_article.trans_lang:
            citation += self.original_article.trans_title_tex
        else:
            citation += self.title_tex

        year = self.date_published.strftime("%Y") if self.date_published is not None else "YYYY"
        citation += " (" + year + ")"

        if self.doi is not None:
            citation += " doi : " + self.doi

        # page_text = self.get_page_text(True)
        # citation += self.original_article.get_citation_base(year, page_text)

        citation += " (" + self.original_article.get_citation()[0:-1] + ")"

        # author_names = get_names(self.original_article, "author")
        # if author_names:
        #     authors = '; '.join(author_names)
        # else:
        #     author_names = get_names(self.original_article, "editor")
        #     if author_names:
        #         authors = '; '.join(author_names) + ' (' + str(
        #             _("éd.")) + ')'
        #     else:
        #         authors = ''
        # citation += authors
        #
        # if citation:
        #     if not citation.endswith('.'):
        #         citation += '.'
        #     citation += ' '
        #
        # citation += self.original_article.title_tex
        #
        # if self.lang == self.original_article.trans_lang:
        #     citation += f" [{self.original_article.trans_title_tex}]"
        # else:
        #     citation += f" [{self.title_tex}]"
        #
        # citation += ' (' + str(_('Traduit par')) + ' '
        #
        # citation += '). '
        #
        # citation += self.original_article.my_container.my_collection.title_tex

        # if self.doi is not None:
        #     citation += ' doi : ' + self.doi + '.'
        #
        # if request is not None:
        #     url = "{}://{}{}".format(request.scheme, request.get_host(), self.original_article.get_absolute_url())
        #     citation += ' ' + url
        #
        # citation += " (" + str(_('Article original publié en ')) + self.original_article.my_container.year + ')'

        return citation
