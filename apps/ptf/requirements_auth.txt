django-allauth==0.58.2
django-braces==1.15.0
django-cors-headers==4.3.1
python3-openid==3.2.0
requests-oauthlib==1.3.1
