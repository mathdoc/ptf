def paginate(rows, page, numfound):
    numpages = numfound // rows
    if numfound % rows:
        numpages += 1

    # if page % 10 == 0:
    #     fp = page - 10 + 1
    # else:
    #     fp = (page/10) * 10 + 1
    # lp = fp + min(10, numpages - fp + 1)
    # next_slice = lp + ( numpages - lp ) / 2
    # previous_slice = fp / 2

    if numpages < 10:
        fp = 1
        lp = numpages
        previous_slice = next_slice = 0
    elif page < 5:
        fp = 1
        lp = min(numpages, 9)
        previous_slice = 0
        next_slice = lp + ((numpages - lp) // 2)
    else:
        fp = page - 4
        lp = min(numpages, page + 4)
        previous_slice = fp // 2
        next_slice = lp + ((numpages - lp) // 2)

    template_data = {
        "curpage": page,
        "pagerange": list(range(fp, lp + 1)),
        "previous_slice": previous_slice,
        "next_slice": next_slice,
        "numpages": numpages,
    }
    return template_data


def paginate_from_request(request, params, numfound):
    """
    prepare le context pour la pagination
    et supprime la pagination si il y a en a une sur la query
    @return: context
    """
    rows = params["rows"]
    page = params["page"]
    context = paginate(rows, page, numfound)

    context["page_path"] = params["path"]
    return context
