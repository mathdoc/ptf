import os
import shutil
import time

from django.conf import settings

from ptf.cmds.xml import xml_utils

NOW = time.gmtime()[0]

extids_hrefs = {
    "doi": "https://doi.org/{0}",
    "zbl-item-id": "https://zbmath.org/?q=an:{0}",
    "jfm-item-id": "https://zbmath.org/?q=an:{0}",
    "mr-item-id": "https://mathscinet.ams.org/mathscinet-getitem?mr={0}",
    "nmid": "/item/{0}",
    "numdam-id": "/item/{0}",
    "mathdoc-id": "/item/{0}",
    "eudml-item-id": "https://eudml.org/doc/{0}",
    "sps-id": "http://sites.mathdoc.fr/cgi-bin/spitem?id={0}",
    "arxiv": "https://arxiv.org/abs/{0}",
    "hal": "https://hal.archives-ouvertes.fr/{0}",
    "tel": "https://tel.archives-ouvertes.fr/{0}",
    "theses.fr": "https://theses.fr/{0}",
    "orcid": "https://orcid.org/{0}",
    "idref": "https://www.idref.fr/{0}",
    "semantic-scholar": "https://www.semanticscholar.org/paper/{0}",
    "pmid": "https://pubmed.ncbi.nlm.nih.gov/{0}",
    "ark": "http://ark.bnf.fr/{0}",
}


def resolve_id(id_type: str, id_value: str, force_numdam=False):
    if id_type == "mr-item-id" and "#" in id_value:
        id_value = id_value.replace(" #", ":")
    elif id_type == "nmid" and force_numdam:
        return f"http://www.numdam.org/item/{id_value}"
    elif id_type == "eudml-item-id":
        values = id_value.split(":")
        if len(values) > 0:
            id_value = values[-1]

    if id_type in extids_hrefs:
        return extids_hrefs[id_type].format(id_value)
    return ""


def find_id_type(id):
    id_type = None
    if id.find("10.") == 0:
        id_type = "doi"
    elif id.find("hal-") == 0:
        id_type = "hal"
    elif id.lower().find("arxiv:") == 0:
        id_type = "arxiv"

        # if (len(id) == 9 or len(id) == 10) and id.find(".") == 5:
        #     year = id[0:1]
        #     month = id[2:3]
        #     sequence = id[5:]
        #     if year.is_numeric() and month.is_numeric() and 1 < int(month) < 13 and sequence.is_numeric():
        #         id_type = "arxiv"

    return id_type


def get_mimetype(filename):
    type_extension = {
        "pdf": "application/pdf",
        "djvu": "image/x.djvu",
        "tex": "application/x-tex",
        "png": "image/png",
        "jpg": "image/jpeg",
    }

    basename = os.path.basename(filename)
    lower_basename = basename.lower()
    extension = os.path.splitext(lower_basename)[1][1:]
    mimetype = type_extension.get(extension, "")
    return mimetype


def get_article_base_url():
    return settings.ARTICLE_BASE_URL


def get_issue_base_url():
    return settings.ISSUE_BASE_URL


def get_icon_base_url():
    return settings.ICON_BASE_URL


def get_icon_url(id_, filename):
    href = get_icon_base_url() + filename
    # path = get_relative_file_path(id, filename)
    # if os.path.isabs(path):
    #     path = path[1:]
    # href = os.path.join(get_icon_base_url(), path)
    return href


def get_doi_url(doi):
    href = settings.DOI_BASE_URL + doi
    return href


def get_relative_folder(collection_id, container_id=None, article_id=None):
    folder = collection_id
    if container_id:
        folder += "/" + container_id
    if article_id:
        folder += "/" + article_id
    return folder


def embargo(wall, year):
    result = False
    y = NOW

    if wall:
        try:
            y = int(year.split("-")[0])
        except BaseException:
            pass

        result = NOW - y <= wall

    return result


# Iterate a folder with a collection
# The folder must look like @COL/@ISSUE/@ISSUE.XML


def iterate_collection_folder(folder, pid, first_issue=""):
    root_folder = os.path.join(folder, pid)

    start = len(first_issue) == 0
    # first_issue = 'CRMATH_2008__346_1-2'
    for item in sorted(os.listdir(root_folder)):
        if not start and item == first_issue:
            start = True
        if start:  # and item != 'CRMATH_2015__353_2':
            dir = os.path.join(root_folder, item)
            if os.path.isdir(dir):
                file = os.path.join(root_folder, item, item + ".xml")
                if os.path.isfile(file):
                    yield item, file
                file = os.path.join(root_folder, item, item + "-cdrxml.xml")
                if os.path.isfile(file):
                    yield item, file


def create_folder(folder):
    try:
        os.makedirs(folder)
    except BaseException:
        pass

    if not os.path.isdir(folder):
        raise RuntimeError("Unable to create " + folder)


def copy_folder(from_dir, to_dir):
    if os.path.isdir(from_dir):
        create_folder(to_dir)

    for f in os.listdir(from_dir):
        from_path = os.path.join(from_dir, f)
        if os.path.isfile(from_path):
            copy_file(from_path, to_dir)
        if os.path.isdir(from_path):
            copy_folder(from_path, os.path.join(to_dir, f))


def copy_file(from_path, to_path):
    if os.path.isfile(from_path):
        if os.path.isdir(to_path):
            to_path = os.path.join(to_path, os.path.basename(from_path))
        if to_path.startswith(settings.MATHDOC_ARCHIVE_FOLDER):
            # copy2 attempts to preserve all file metadata
            # on /mathdoc_archive, we don't want to preserve the mode, just the dates
            shutil.copyfile(from_path, to_path)
            shutil.copystat(from_path, to_path)
        else:
            shutil.copy2(from_path, to_path)


def copy_html_images_in_folder(colid, issue, article_to_copy, from_folder, to_folder, folder_name):
    # copy depuis archive, directement tout le répertoire contenant les images
    dest_folder = os.path.join(
        to_folder,
        get_relative_folder(colid, issue.pid, article_to_copy.pid),
        folder_name,
    )
    if os.path.isdir(dest_folder):
        try:
            shutil.rmtree(dest_folder)
        except OSError:
            message = "Unable to remove " + dest_folder
            raise RuntimeError(message)

    src_folder = os.path.join(
        from_folder,
        get_relative_folder(colid, issue.pid, article_to_copy.pid),
        folder_name,
    )
    if os.path.isdir(src_folder):
        copy_folder(src_folder, dest_folder)


def copy_html_images(resource, to_folder, from_folder):
    """
    Copy the figures associated with the HTML body of an article
    if from_archive:
    Images are in settings.MATHDOC/@colid/@issue_id/@a_id/src/tex/figures/
    if from_cedram:
    Images are in settings.CEDRAM_TEX_FOLDER/@colid/@issue_id/@tex_aid/Fulltext/figures/

    @param resource:
    @param to_folder:
    @param from_folder:
    @return: nothing
    """

    if resource.classname != "Article":
        return

    article_to_copy = resource
    issue = article_to_copy.my_container
    colid = article_to_copy.get_collection().pid

    if from_folder == settings.CEDRAM_XML_FOLDER:
        tex_src_folder = get_cedram_issue_tex_folder(colid, issue.pid)
        tex_folders, _ = get_cedram_tex_folders(colid, issue.pid)

        if len(tex_folders) > 0:
            i = 0
            for article in issue.article_set.all():
                if article_to_copy.pid == article.pid:
                    # l'ordre d'enregistrement des articles dans la bdd est important : l'ordre du tex est SENSE correspondre au xml de l'issue

                    dest_folder = os.path.join(
                        to_folder,
                        get_relative_folder(colid, issue.pid, article.pid),
                        "src/tex/figures",
                    )

                    if os.path.isdir(dest_folder):
                        try:
                            shutil.rmtree(dest_folder)
                        except OSError:
                            message = "Unable to remove " + dest_folder
                            raise RuntimeError(message)

                    src_folder = os.path.join(
                        tex_src_folder, tex_folders[i], "FullText", "figures"
                    )
                    qs = article.relatedobject_set.filter(rel="html-image")
                    if qs.count() > 0:
                        create_folder(dest_folder)

                    for related_obj in qs:
                        img_file = os.path.join(src_folder, os.path.basename(related_obj.location))
                        copy_file(img_file, dest_folder)

                i += 1

    else:
        # copy depuis archive, directement tout le répertoire contenant les images
        copy_html_images_in_folder(
            colid, issue, article_to_copy, from_folder, to_folder, "src/tex/figures"
        )
        copy_html_images_in_folder(
            colid, issue, article_to_copy, from_folder, to_folder, "src/media"
        )


def copy_file_obj_to_article_folder(
    file_obj, colid, issue_pid, article_pid, is_image=False, article_container_pid=None, path=""
):
    if not is_image:
        name, extension = os.path.splitext(file_obj.name)
        relative_folder = get_relative_folder(colid, issue_pid, article_pid)
        folder = os.path.join(settings.RESOURCES_ROOT, relative_folder)
        create_folder(folder)
        full_file_name = os.path.join(folder, article_pid + extension)
        relative_file_name = os.path.join(relative_folder, article_pid + extension)

        with open(full_file_name, "wb+") as destination:
            for chunk in file_obj.chunks():
                destination.write(chunk)

    else:
        name, extension = os.path.splitext(file_obj.name)
        relative_folder = get_relative_folder(colid, issue_pid, article_pid)
        if path:
            folder = os.path.join(settings.RESOURCES_ROOT, relative_folder, path)
        else:
            folder = os.path.join(settings.RESOURCES_ROOT, relative_folder + "/src/media")
        create_folder(folder)
        full_file_name = os.path.join(folder, name + extension)
        with open(full_file_name, "wb+") as destination:
            for chunk in file_obj.chunks():
                destination.write(chunk)

        relative_file_name = os.path.join(relative_folder, article_pid + extension)

    return relative_file_name


def copy_binary_files(resource, from_folder, to_folder, binary_files=None):
    if to_folder is not None and not from_folder == to_folder:
        if binary_files is None:
            copy_html_images(resource, to_folder, from_folder)
            binary_files = resource.get_binary_files_location()

        for file in binary_files:
            to_path = os.path.join(to_folder, file)
            dest_folder = os.path.dirname(to_path)

            os.makedirs(dest_folder, exist_ok=True)
            skip_copy = False

            if "http" in file:
                skip_copy = True
            from_path = os.path.join(from_folder, file)

            if not skip_copy and os.path.isfile(from_path):
                copy_file(from_path, to_path)


def delete_object_folder(object_folder, to_folder):
    folder = os.path.join(to_folder, object_folder)

    # pas de sécurité car pour garder le mode CASCADE de la db, on supprime le rép sans s'occuper de ce qu'il y a dedans
    # si on veut vérifier, décommenter :
    # for entry in os.listdir(folder):
    #     if entry.startswith(colid) and os.path.isdir(os.path.join(folder, entry)):
    #         print(entry)
    #         os.path.join(folder, entry)
    #         raise Exception('Le répertoire a supprimer : ' + folder + ' semble encore contenir des articles/containers')
    #
    # if verify == True:
    #     for root, dirs, files in os.walk(folder):
    #         if len(files) > 0:
    #             raise Exception('Le répertoire a supprimer : ' + folder + ' semble encore contenir des objects')

    folder = os.path.normpath(folder)
    # garde fous :)
    if folder in [
        "/mersenne_prod_data",
        "/mersenne_test_data",
        "/mathdoc_archive",
    ] or folder.startswith("/cedram_dev"):
        raise Exception("Attention, pb avec la suppression de " + folder)

    if os.path.isdir(folder):
        shutil.rmtree(folder)


def delete_file(path):
    if os.path.isfile(path):
        os.remove(path)


def get_disk_location(
    root_folder, collection_id, ext, container_id=None, article_id=None, do_create_folder=False
):
    if do_create_folder:
        folder = os.path.join(root_folder, collection_id)
        create_folder(folder)

        if container_id:
            folder = os.path.join(root_folder, collection_id, container_id)
            create_folder(folder)

        if article_id:
            folder = os.path.join(root_folder, collection_id, container_id, article_id)
            create_folder(folder)

    last_id = collection_id
    filename = os.path.join(root_folder, collection_id)
    if container_id:
        filename = os.path.join(filename, container_id)
        last_id = container_id
    if article_id:
        filename = os.path.join(filename, article_id)
        last_id = article_id

    filename = os.path.join(filename, last_id + "." + ext)

    return filename


def get_body(filename):
    with open(filename, encoding="utf-8") as file_:
        body = file_.read()
    return body


def get_archive_filename(root_folder, colid, pid, ext, do_create_folder=False, article_pid=None):
    """

    :param root_folder: root_folder of the archive. Ex: /mathdoc_archive
    :param colid: collection id
    :param pid:   issue id
    :param ext:   filename extension ("xml" or "json")
    :param create_folder: option to recursively create sub folders
    :return:
    """

    # TODO: call get_disk_location(root_folder, colid, ext, pid, None, do_create_folder)

    if do_create_folder:
        folder = os.path.join(root_folder, colid)
        create_folder(folder)

        if pid:
            folder = os.path.join(root_folder, colid, pid)
            create_folder(folder)

        if article_pid:
            folder = os.path.join(folder, article_pid)
            create_folder(folder)

    if pid and article_pid:
        filename = os.path.join(root_folder, colid, pid, article_pid, article_pid + "." + ext)
    elif pid:
        filename = os.path.join(root_folder, colid, pid, pid + "." + ext)
    else:
        filename = os.path.join(root_folder, colid, colid + "." + ext)

    return filename


# Read the XML of an issue/collection within an archive folder
# The folder must look like @COL/@ISSUE/@ISSUE.XML
#                           @COL/@COL.XML


def get_archive_body(root_folder, colid, pid):
    filename = get_archive_filename(root_folder, colid, pid, "xml")
    return get_body(filename)


def is_tex_comment(text, i):
    is_comment = False
    while i > 0 and text[i] == " ":
        i -= 1

    if i >= 0 and text[i] == "%":
        is_comment = True
    elif i > 0 and text[i] == "~" and text[i - 1] == "%":
        is_comment = True

    return is_comment


def is_tex_def(text, i):
    is_def = False

    if text[i - 5 : i - 1] == "\\def":
        is_def = True

    return is_def


def is_tex_newcommand(text, i):
    is_newcommand = False

    if text[i - 12 : i - 1] == "\\newcommand":
        is_newcommand = True

    return is_newcommand


def get_cedram_issue_tex_folder(colid, issue_id):
    return os.path.join(settings.CEDRAM_TEX_FOLDER, colid, issue_id)


def get_cedram_tex_folders(colid, issue_id):
    """
    return article filenames in cedram tex issue folder and corresponding doi if present, extracted from issue tex file
    @param colid:
    @param issue_id:
    @return:  list of filename, list of doi
    """
    filenames = []
    dois = []

    body = ""
    issue_filename = os.path.join(get_cedram_issue_tex_folder(colid, issue_id), issue_id + ".tex")
    if os.path.isfile(issue_filename):
        try:
            with open(issue_filename, encoding="utf-8") as f:
                body = f.read()
        except UnicodeDecodeError:
            with open(issue_filename, encoding="iso-8859-1") as f:
                body = f.read()

        lower_body = body.lower()

        li = []
        j = body.find("includearticle")
        if j >= 0:
            li.append(j)
        j = body.find("includeprearticle")
        if j >= 0:
            li.append(j)
        j = lower_body.find("includepreface")
        if j >= 0:
            li.append(j)
        i = min(li) if len(li) > 0 else -1

        while i >= 0:
            if (
                i > 1
                and not is_tex_comment(body, i - 2)
                and not is_tex_def(body, i)
                and not is_tex_newcommand(body, i)
            ):
                doi = None
                while body[i] != "{":
                    if len(body) > i + 4 and body[i : i + 4] == "doi=":
                        j = i + 4
                        while body[i] != "," and body[i] != "]":
                            i += 1
                        doi = xml_utils.normalize_space(body[j:i])
                    i += 1
                i += 1
                filename = ""
                while body[i] != "}":
                    filename += body[i]
                    i += 1
                if len(filename) > 0:
                    filenames.append(filename)
                    dois.append(doi)
            else:
                i += 1

            li = []
            j = body.find("includearticle", i)
            if j >= 0:
                li.append(j)
            j = body.find("includeprearticle", i)
            if j >= 0:
                li.append(j)
            j = lower_body.find("includepreface", i)
            if j >= 0:
                li.append(j)
            i = min(li) if len(li) > 0 else -1

    return filenames, dois


def get_bibtex_from_tex(tex_filename):
    bibtex_filename = ""

    body = ""
    if os.path.isfile(tex_filename):
        try:
            with open(tex_filename, encoding="utf-8") as f:
                body = f.read()
        except UnicodeDecodeError:
            with open(tex_filename, encoding="iso-8859-1") as f:
                body = f.read()

        i = body.find("\\bibliography")
        while i >= 0:
            if i > 1 and not is_tex_comment(body, i - 2):
                while body[i] != "{":
                    i += 1
                i += 1
                while body[i] != "}":
                    bibtex_filename += body[i]
                    i += 1
            else:
                i += 1

            i = body.find("\\bibliography", i)

    return bibtex_filename


PCJ_SECTIONS = {
    "animsci": "Animal Science",
    "archaeo": "Archaeology",
    "ecology": "Ecology",
    "ecotoxenvchem": "Ecotoxicology & Environmental Chemistry",
    "evolbiol": "Evolutionary Biology",
    "forestwoodsci": "Forest & Wood Sciences",
    "genomics": "Genomics",
    "healthmovsci": "Health & Movement Sciences",
    "infections": "Infections",
    "mcb": "Mathematical & Computational Biology",
    "microbiol": "Microbiology",
    "networksci": "Network Science",
    "neuro": "Neuroscience",
    "orgstudies": "Organization Studies",
    "paleo": "Paleontology",
    "rr": "Registered Reports",
    "zool": "Zoology",
}

PCJ_UGA_SECTION = ["healthmovsci", "rr", "orgstudies"]
PCJ_CONFERENCES = ["Euring 2023"]
PCJ_MANDATORY_TOPICS = {
    "ecology": "Ecology",
    "evolbiol": "Evolution",
    "genomics": "Genetics/genomics",
    "paleo": "Paleontology",
    "archaeo": "Archaeology",
    "microbiol": "Microbiology",
    "neuro": "Neuroscience",
}


def get_pci(value):
    if value in PCJ_SECTIONS:
        return PCJ_SECTIONS[value]
    return ""


ARTICLE_TYPES = {
    "biographical-note": "Notice biographique",
    "book-review": "Recension d’ouvrage",
    "clarification": "Mise au point",
    "congress": "Intervention en colloque",
    "corrigendum": "Corrigendum",
    "editorial": "Éditorial",
    "erratum": "Erratum",
    "expression-of-concern": "Avertissement des éditeurs",
    "foreword": "Avant-propos",
    "guest-editors": "Rédacteurs invités",
    "historical-commentary": "Commentaire historique",
    "history-of-sciences": "Histoire des sciences et des idées",
    "letter": "Commentaire et réponse",
    "news": "C'est apparu dans la presse",
    "opinion": "Opinion / Perspective",
    "preliminary-communication": "Communication préliminaire",
    "research-article": "Article de recherche",
    "retraction": "Rétractation",
    "review": "Article de synthèse",
    "software-tool": "Outil logiciel",
}

# From : apps/ptf/templates/common/externalid_detail.html
extids_formats: dict[str, str] = {
    "arxiv": "arXiv",
    "tel": "TEL",
    "hal": "HAL",
    "theses.fr": "theses.fr",
    "doi": "DOI",
    "numdam-id": "Numdam",
    "sps-id": "SPS",
    "mr-item-id": "MR",
    "zbl-item-id": "Zbl",
    "jfm-item-id": "JFM",
    "eudml-item-id": "EuDML",
    "pmid": "PMID",
    "ark": "Gallica",
}
