import csv
import html
import json
import os
import re
import string
import urllib.parse
from itertools import chain
from operator import itemgetter

from bs4 import BeautifulSoup
from django_sendfile import sendfile
from munch import Munch
from requests.exceptions import RequestException
from requests.exceptions import Timeout

from django.conf import settings
from django.contrib.syndication.views import Feed
from django.core.cache import cache
from django.core.exceptions import SuspiciousOperation
from django.core.files.temp import NamedTemporaryFile
from django.core.paginator import EmptyPage
from django.core.paginator import Paginator
from django.db.models import F
from django.db.models import Q
from django.db.models import Value
from django.db.models.functions import Greatest
from django.http import Http404
from django.http import HttpResponse
from django.http import HttpResponseBadRequest
from django.http import HttpResponseRedirect
from django.http import HttpResponseServerError
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.shortcuts import render
from django.urls import reverse
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.utils.translation import get_language
from django.utils.translation import gettext_lazy as _
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from django.views.generic import TemplateView
from django.views.generic import View

from comments_api.constants import PARAM_PREVIEW
from matching import crossref
from matching import matching
from ptf import citedby
from ptf import exceptions
from ptf import model_data
from ptf import model_data_converter
from ptf import model_helpers
from ptf import models
from ptf.cmds import ptf_cmds
from ptf.cmds import solr_cmds
from ptf.cmds import xml_cmds
from ptf.cmds.xml import xml_utils
from ptf.cmds.xml.ckeditor.ckeditor_parser import CkeditorParser
from ptf.cmds.xml.jats.builder.issue import get_title_xml
from ptf.cmds.xml.jats.jats_parser import get_tex_from_xml
from ptf.display import resolver
from ptf.display import utils
from ptf.model_data_converter import jats_from_abstract
from ptf.models import Article
from ptf.solr.search_helpers import CleanSearchURL
from ptf.templatetags.helpers import decode_query_string
from ptf.url_utils import format_url_with_params
from ptf.utils import highlight_diff
from ptf.views.components import breadcrumb

# from django.utils.crypto import md5
# from django.utils import cache
#
# def ptf_generate_cache_key(request, method, headerlist, key_prefix):
#     """Return a cache key from the headers given in the header list."""
#     url = md5(request.build_absolute_uri().encode("ascii"), usedforsecurity=False)
#     cache_key = "views.decorators.cache.cache_page.%s.%s.%s" % (
#         key_prefix,
#         method,
#         url.hexdigest(),
#     )
#     return cache_key
#
#
# def ptf_generate_cache_header_key(key_prefix, request):
#     """Return a cache key for the header cache."""
#     url = md5(request.build_absolute_uri().encode("ascii"), usedforsecurity=False)
#     cache_key = "views.decorators.cache.cache_header.%s.%s" % (
#         key_prefix,
#         url.hexdigest(),
#     )
#     return cache_key
#
#
# cache._generate_cache_key = ptf_generate_cache_key
# cache._generate_cache_header_key = ptf_generate_cache_header_key


def update_context_with_desc(context):
    online_first_cr = context.get("online_first_cr", False)
    context[
        "online_first_desc_fr"
    ] = """<p>Les articles en « Première publication » ont été évalués par les pairs, acceptés et édités. Ils ont été mis en page et finalisés avec les corrections des auteurs et des relecteurs.</p>"""
    context[
        "online_first_desc_fr"
    ] += """<p>Ces articles sont donc publiés dans leur forme finale et ont reçu un DOI (digital object identifier). Par conséquent, ils ne peuvent plus être modifiés après leur publication électronique. Toute correction qui pourrait être nécessaire doit être effectuée ultérieurement dans un erratum.</p>"""
    if not online_first_cr:
        context[
            "online_first_desc_fr"
        ] += """<p>Dès que les articles sont affectés à un numéro de la revue, ils sont retirés de cette page.</p>"""

    context[
        "online_first_desc_en"
    ] = """<p>Online First articles have been peer-reviewed, accepted and edited. They have been formatted and finalized with corrections from authors and proofreaders.</p>"""
    context[
        "online_first_desc_en"
    ] += """<p>These articles appear in their final form and have been assigned a digital object identifier (DOI). Therefore, papers cannot be changed after electronic publication. Any corrections that might be necessary have to be made in an erratum.</p>"""
    if not online_first_cr:
        context[
            "online_first_desc_en"
        ] += """<p>When the articles are assigned to an issue, they will be removed from this page.</p>"""


@require_http_methods(["POST"])
def set_formula_display(request):
    next = request.headers.get("referer")
    # if formula-display is not in POST, then mathml is choosen
    formula_display = request.POST.get("formula-display", "mathml")
    request.session["formula_display"] = formula_display
    # redis session needs to save item in state
    request.session.save()

    # to read session with redis-django-sessions
    # rdata = request.session.load()
    return HttpResponseRedirect(next)


# def views_get_collection(request, collection, api=False, *args, **kwargs):
#     if not collection:
#         raise Http404
#
#     if collection.coltype in ['journal', 'acta']:
#         return IssuesView.as_view()(request, jid=collection.pid)
#     elif collection.coltype == 'these':
#         return thesis(request)
#     else:
#         # en theorie: coltype=book-series TODO: check lectures
#         url = ('series/"%s"/' % collection.title_html) + "p"
#         path, queryDict = CleanSearchURL.decode(url)
#         request.GET = queryDict
#         request.path = path
#         request.path_info = path
#         return sorted_books(request)


# @require_http_methods(["GET"])
# def container(request, pid, api=False):
#     cont = model_helpers.get_container(pid)
#     return views_get_container(request, cont, api)


def views_update_container_context(request, container, context, display_lang):
    if container is None:
        raise Http404

    articles_to_appear = is_cr = False
    if hasattr(settings, "ISSUE_TO_APPEAR_PID") and settings.ISSUE_TO_APPEAR_PID == container.pid:
        context["articles_to_appear"] = articles_to_appear = True

    if (
        hasattr(settings, "SITE_NAME")
        and len(settings.SITE_NAME) == 6
        and settings.SITE_NAME[0:2] == "cr"
    ):
        context["is_cr"] = is_cr = True

    context["online_first_cr"] = False
    if container.with_online_first and is_cr:
        context["articles_to_appear"] = articles_to_appear = True
        context["online_first_cr"] = True

    collection = container.my_collection
    context["citing_articles"] = container.citations()
    context["source"] = request.GET.get("source", None)

    context["breadcrumb"] = breadcrumb.get_breadcrumb(container)
    context["btn_show_tex"] = settings.SHOW_TEX if hasattr(settings, "SHOW_TEX") else False

    if container.ctype.startswith("book") or container.ctype == "lecture-notes":
        book_parts = container.article_set.filter(sites__id=settings.SITE_ID).all().order_by("seq")

        references = False
        if container.ctype == "book-monograph":
            # on regarde si il y a au moins une bibliographie
            for art in container.article_set.all():
                if art.bibitem_set.count() > 0:
                    references = True
        context["references"] = references

        context["book"] = container
        context["book_parts"] = list(book_parts)
        context["template"] = "blocks/book.html"

    else:
        if articles_to_appear and is_cr:
            articles = container.article_set.all().order_by(
                F("date_online_first").desc(nulls_last=True), "-seq"
            )

        else:
            articles = container.article_set.all().order_by("seq")
        # TODO next_issue, previous_issue

        context["issue"] = container
        context["articles"] = articles
        context["template"] = "blocks/issue-items.html"

    # commun article ?
    context["coltype"] = collection.coltype
    context["supplementary_materials"] = models.SupplementaryMaterial.objects.filter(
        relatedobject_ptr__resource__pk=container.pk,
    ).filter(rel="supplementary-material")
    context["reviews"] = models.SupplementaryMaterial.objects.filter(
        relatedobject_ptr__resource__pk=container.pk,
    ).filter(rel="review")
    context["bibtex"] = container.get_bibtex(request)
    context["ris"] = container.get_ris(request)
    context["enw"] = container.get_endnote(request)
    context["howtocite"] = container.get_citation(request)
    # context['bibtex'] = article.get_bibtex(request.get_host()) pas la meme signature que pour article ?
    # context['howtocite'] = article.get_citation(request)

    if collection.pid == "ART":
        qs = collection.content.filter(sites__id=settings.SITE_ID).order_by(
            "-vseries_int", "-year", "-volume_int", "number_int"
        )
        if qs.exists():
            last_issue = qs.first()
            if container.pid == last_issue.pid:
                context["last_issue"] = True

    context["bs_version"] = (
        settings.BOOTSTRAP_VERSION if hasattr(settings, "BOOTSTRAP_VERSION") else 3
    )
    if container.ctype == "issue_special" and container.resources_in_special_issue:
        context["resource_in_special_issue"] = [
            r.resource for r in container.resources_in_special_issue.all()
        ]

    update_context_with_desc(context)


# def views_get_container(request, container, api=False, *args, **kwargs):
#     context = {}
#     views_update_container_context(request, container, context)
#     template = context['template']
#     return render(request, template, context)


# class ContainerView(TemplateView):
#     template_name = ''
#
#     def get_context_data(self, **kwargs):
#         context = super(ContainerView, self).get_context_data(**kwargs)
#
#         if 'pid' in kwargs:
#             pid = kwargs['pid']
#         else:
#             pid = self.kwargs.get('pid')
#         container = model_helpers.get_container(pid)
#         if pid == getattr(settings, 'ISSUE_TO_APPEAR_PID', 'XXX') and not container:
#             self.template_name = 'blocks/issue_detail_empty.html'
#         else:
#             views_update_container_context(self.request, container, context)
#             self.template_name = context['template']
#
#         return context


# def views_get_article(
#         request: HttpRequest,
#         article: Optional[Article],
#         api=False,
#         lang=None,
#         *args,
#         **kwargs
#     ):
#
#
#     return render(request, template, context)


# @require_http_methods(["GET"])
# def article(request: HttpRequest, aid: str, api=False):
#     return ItemView.as_view()(request)
#
#
# @require_http_methods(["GET"])
# def article_lang(request, lang, aid, api=False):
#     return ItemView.as_view()(request)


class ItemView(TemplateView):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.template_name = ""
        self.obj = None
        self.pid = None
        self.display_lang = None

    def get_obj(self, **kwargs):
        self.pid = self.kwargs.get("aid", None)
        if self.pid is None:
            self.pid = self.kwargs.get("pid", None)
        if self.pid is None:
            self.pid = self.request.GET.get("id", "").replace("/", "")
        if "\x00" in self.pid:
            raise Http404

        if "/" in self.pid:
            # If someone types /articles/10.5802/crgeos.150-fr, we set the display lang and get 10.5802/crgeos.150
            if not self.display_lang and self.pid[-3] == "-" and not self.pid[-2:].isdigit():
                self.display_lang = self.pid[-2:]
                self.pid = self.pid[:-3]

            obj = model_helpers.get_article_by_doi(self.pid, prefetch=True)
        else:
            obj = model_helpers.get_resource(self.pid, prefetch=True)
            if obj is not None:
                obj = obj.cast()
                fct = getattr(model_helpers, "get_" + obj.classname.lower())
                if fct and callable(fct):
                    obj = fct(self.pid, prefetch=True)

        if obj is None and self.pid != getattr(settings, "ISSUE_TO_APPEAR_PID", "XXX"):
            raise Http404

        self.obj = obj

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["journal"] = self.obj.get_collection()
        context["obj"] = self.obj
        return context

    def get(self, request, *args, **kwargs):
        if "obj" in kwargs:
            self.obj = kwargs["obj"]
        if "lang" in kwargs:
            self.display_lang = kwargs["lang"]

        if (
            self.obj is not None
        ):  # ItemView may call ArticleView. Avoid infinite loop is self.obj is set
            return super().get(request, *args, **kwargs)

        self.get_obj(**kwargs)
        if not self.display_lang:
            self.display_lang = self.kwargs.get("lang", None)
        view_cls = ItemViewClassFactory().get_resource_view(self.obj.classname)
        if view_cls is not None:
            kwargs["obj"] = self.obj
            if self.display_lang is not None:
                kwargs["lang"] = self.display_lang
            return view_cls.as_view()(request, *args, **kwargs)
        return super().get(request, *args, **kwargs)


class ArticleView(ItemView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        request = self.request
        display_lang = self.display_lang
        article = self.obj

        article.title_tex = article.title_tex.strip()
        source = request.GET.get("source", None)
        container = article.my_container
        collection = container.my_collection
        citing_articles = article.citations()

        with_tex = (
            "formula_display" in request.session and request.session["formula_display"] == "tex"
        )

        if display_lang is not None and len(display_lang) == 2:
            context["display_lang"] = display_lang
        else:
            display_lang = context["display_lang"] = article.lang

        context["collection"] = collection
        context["journal"] = collection
        context["is_translation"] = False
        context["full_translation"] = False
        context["needs_translation"] = True
        context["start_translation_url"] = os.path.join(
            settings.TRANSLATION_URL, f"translation/new?doi={article.doi}"
        )
        langs = [article.lang] if (article.lang and article.lang != "und") else []
        # if article.trans_lang and article.trans_lang != 'und':
        #     langs.append(article.trans_lang)
        for translated_article in article.translations.all():
            if translated_article.lang not in langs:
                langs.append(translated_article.lang)
            if display_lang == translated_article.lang:
                context["is_translation"] = True
                context["needs_translation"] = False
                context["translated_article"] = translated_article
                context["body_html"] = translated_article.body_html

        context["languages"] = langs

        if display_lang == article.lang or (
            not context["is_translation"] and display_lang != article.trans_lang
        ):
            context["article_title"] = article.title_tex if with_tex else article.title_html
            context["body_html"] = article.body_html
            if display_lang == article.lang:
                context["needs_translation"] = False
            for abstract in article.get_abstracts():
                if abstract.lang == display_lang:
                    context["abstract"] = abstract
        elif display_lang == article.trans_lang or not context["is_translation"]:
            context["article_title"] = (
                article.trans_title_tex if with_tex else article.trans_title_html
            )
            context["article_original_title"] = (
                article.title_tex if with_tex else article.title_html
            )
            if "body_html" not in context or not context["body_html"]:
                # The full text is not available in the display_lang. Use the original version.
                context["body_html"] = article.body_html
            for abstract in article.get_abstracts():
                if abstract.lang == display_lang:
                    context["abstract"] = abstract
        else:
            context["full_translation"] = True
            context["article_original_title"] = (
                article.title_tex if with_tex else article.title_html
            )
            for translated_article in article.translations.all():
                if display_lang == translated_article.lang:
                    context["article_title"] = (
                        translated_article.title_tex if with_tex else translated_article.title_html
                    )
                    context["abstract"] = translated_article.get_abstracts().first()

        if context["is_translation"]:
            context["howtocite"] = translated_article.get_citation(request)
        else:
            context["howtocite"] = article.get_citation(request)

        if (
            hasattr(settings, "ISSUE_TO_APPEAR_PID")
            and settings.ISSUE_TO_APPEAR_PID == container.pid
        ):
            context["articles_to_appear"] = True

        # Format comment URL if comments are activated
        if getattr(settings, "COMMENTS_VIEWS_ARTICLE_COMMENTS", False) is True:
            current_url = f"{request.build_absolute_uri()}#article-comments-section"
            query_params = {"redirect_url": current_url}
            # Forward some query parameters if present
            preview_id = request.GET.get(PARAM_PREVIEW)
            if preview_id:
                query_params[PARAM_PREVIEW] = preview_id
            comment_reply_to = request.GET.get("commentReplyTo")
            if comment_reply_to:
                query_params["commentReplyTo"] = comment_reply_to

            context["comments_url"] = format_url_with_params(
                reverse("article_comments", kwargs={"doi": article.doi}), query_params
            )

        is_cr = False
        if (
            hasattr(settings, "SITE_NAME")
            and len(settings.SITE_NAME) == 6
            and settings.SITE_NAME[0:2] == "cr"
        ):
            context["is_cr"] = is_cr = True
            context["related_articles"] = get_suggested_articles(article)

        context["online_first_cr"] = False
        if container.with_online_first and is_cr:
            context["articles_to_appear"] = True
            context["online_first_cr"] = True

        if hasattr(settings, "SHOW_BODY") and settings.SHOW_BODY:
            context["article_show_body"] = True

        context["breadcrumb"] = breadcrumb.get_breadcrumb(article)
        context["btn_show_tex"] = settings.SHOW_TEX if hasattr(settings, "SHOW_TEX") else False

        # pour les livres du centre Mersenne, on remplace la collection principale par collectionmembership
        if collection.pid == "MBK":
            for collection_membership in container.collectionmembership_set.all():
                collection = collection_membership.collection
                container.my_collection = collection

        if container.ctype == "issue":
            context.update(
                {
                    "article": article,
                    "citing_articles": citing_articles,
                    "source": source,
                }
            )
            context["template"] = "blocks/article.html"
        else:
            context.update(
                {"book_part": article, "citing_articles": citing_articles, "source": source}
            )
            context["template"] = "blocks/book-part.html"

        if hasattr(settings, "SITE_REGISTER"):
            site_entry = settings.SITE_REGISTER[settings.SITE_NAME]

            licence = None
            try:
                year = int(article.my_container.year)
            except ValueError:
                year = 0

            if "licences" in site_entry:
                licences = site_entry["licences"]
                i = len(licences) - 1
                while not licence and i >= 0:
                    if year >= licences[i][0]:
                        licence = licences[i][1]
                    i -= 1
            context["licence"] = licence

        context["coltype"] = collection.coltype
        context["supplementary_materials"] = models.SupplementaryMaterial.objects.filter(
            relatedobject_ptr__resource__pk=article.pk,
        ).filter(rel="supplementary-material")
        context["reviews"] = models.SupplementaryMaterial.objects.filter(
            relatedobject_ptr__resource__pk=article.pk,
        ).filter(rel="review")
        context["bibtex"] = article.get_bibtex(request)
        context["ris"] = article.get_ris(request)
        context["enw"] = article.get_endnote(request)
        link = model_helpers.get_extlink(resource=collection, rel="test_website")
        context["test_website"] = link.location if link else None
        link = model_helpers.get_extlink(resource=collection, rel="website")
        context["prod_website"] = link.location if link else None
        context["bs_version"] = (
            settings.BOOTSTRAP_VERSION if hasattr(settings, "BOOTSTRAP_VERSION") else 3
        )
        context["recommendations"] = article.extid_set.filter(id_type="rdoi")
        if article.used_in_special_issues.all():
            context["special_issues"] = [
                si.my_container for si in article.used_in_special_issues.all()
            ]
        # context["virtual_issues"] = container
        self.template_name = context["template"]

        return context


class ContainerView(ItemView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if self.pid == getattr(settings, "ISSUE_TO_APPEAR_PID", "XXX") and not self.obj:
            self.template_name = "blocks/issue_detail_empty.html"
        else:
            container = self.obj
            if container is None:
                raise Http404

            views_update_container_context(self.request, container, context, self.display_lang)

            self.template_name = context["template"]

        return context


class CollectionView(ItemView):
    def get(self, request, *args, **kwargs):
        if "obj" in kwargs:
            self.obj = kwargs["obj"]

        collection = self.obj
        if collection.coltype in ["journal", "acta"]:
            return IssuesView.as_view()(request, jid=collection.pid)
        elif collection.coltype in ["lecture-notes"]:
            request.META["QUERY_STRING"] = '"' + collection.title_html + '"-p'
            return sorted_lectures_detailed_view(request)
        elif collection.coltype == "proceeding":
            url = reverse("proceeding-issues", kwargs={"jid": collection.pid})
            return HttpResponseRedirect(url)
            # return proceedings(request, jid=collection.pid)
        else:
            # en theorie: coltype=book-series TODO: check lectures
            request.META["QUERY_STRING"] = '"' + collection.title_html + '"-p'
            return sorted_books(request)


class VolumeDetailView(TemplateView):
    template_name = "blocks/volume-items.html"

    def get_context_data(self, **kwargs):
        # TODO next_volume, previous_volume
        context = super().get_context_data(**kwargs)

        context["group_issues"] = False
        is_cr = (
            hasattr(settings, "SITE_NAME")
            and len(settings.SITE_NAME) == 6
            and settings.SITE_NAME[0:2] == "cr"
        )
        context["is_cr"] = is_cr

        issues_articles, collection = model_helpers.get_issues_in_volume(kwargs.get("vid"), is_cr)
        year = int(issues_articles[0]["issue"].year.split("-")[0])
        context["year"] = year

        if is_cr:
            context["group_issues"] = (settings.SITE_NAME != "crbiol" and year > 2020) or (
                settings.SITE_NAME == "crbiol" and year > 2022
            )

            with_thematic = (
                len(issues_articles) > 1 and len(issues_articles[1]["issue"].title_html) > 0
            )
            context["with_thematic"] = with_thematic

        # Olivier 7/30/2020. Le code suivant crash lorsque fpage n'est pas un entier mais un chiffre romain
        # Il faut rediscuter des specs. Ordonner par 'seq' semble plus simple.
        # Il faut peut-être juste s'assurer que 'seq' soit bien mis à l'import ?

        #  Ex: /volume/WBLN_2018__5/
        # issues_articles = []
        # for issue in issues:
        #     articles = issue.article_set.all().annotate(
        #         fpage_int=Cast(
        #             Case(When(~Q(fpage=''), then='fpage'), default=Value('0')),
        #             IntegerField()
        #         )
        #     ).order_by('seq', 'fpage_int')
        #
        #     issues_articles.append({'issue': issue, 'articles': articles})

        context["issues_articles"] = issues_articles
        context["collection"] = collection
        context["journal"] = collection
        context["coltype"] = collection.coltype
        context["breadcrumb"] = breadcrumb.get_breadcrumb(
            issues_articles[-1].get("issue"), is_volume=True
        )
        context["btn_show_tex"] = settings.SHOW_TEX if hasattr(settings, "SHOW_TEX") else False

        if collection.pid == "ART":
            now = timezone.now()
            curyear = now.year
            context["volume_in_progress"] = year == curyear

        return context


class VolumeGeneralDetailView(TemplateView):
    template_name = "blocks/volume-general-items.html"

    def get_context_data(self, **kwargs):
        # TODO next_volume, previous_volume
        context = super().get_context_data(**kwargs)

        context["group_issues"] = False
        is_cr = (
            hasattr(settings, "SITE_NAME")
            and len(settings.SITE_NAME) == 6
            and settings.SITE_NAME[0:2] == "cr"
        )
        context["is_cr"] = is_cr

        issues_articles, collection = model_helpers.get_issues_in_volume(
            kwargs.get("vid"), is_cr, general_articles=True
        )

        if is_cr:
            year = int(issues_articles[0]["issue"].year)
            context["group_issues"] = (settings.SITE_NAME != "crbiol" and year > 2020) or (
                settings.SITE_NAME == "crbiol" and year > 2022
            )

            with_thematic = (
                len(issues_articles) > 1 and len(issues_articles[1]["issue"].title_html) > 0
            )
            context["with_thematic"] = with_thematic

        context["issues_articles"] = issues_articles
        context["collection"] = collection
        context["journal"] = collection
        context["coltype"] = collection.coltype
        context["breadcrumb"] = breadcrumb.get_breadcrumb(issues_articles[-1].get("issue"))
        context["btn_show_tex"] = settings.SHOW_TEX if hasattr(settings, "SHOW_TEX") else False
        context["vid"] = kwargs.get("vid")
        # for article in issues_articles:
        #     for a in article["articles"]:
        #         special = a.used_in_special_issues.all()
        #         for s in special:
        #             context["in_special"] = vars(s)
        #             pass
        #         pass
        context["request"] = self.request

        return context


class ItemViewClassFactory:
    views = {
        "article": ArticleView,
        "container": ContainerView,
        "collection": CollectionView,
        "volume": VolumeDetailView,
    }

    def get_resource_view(self, classname):
        classname = classname.lower()
        if classname in self.views:
            return self.views[classname]
        return None


@require_http_methods(["GET"])
def journals(request, api=False):
    journals = model_helpers.get_journals().filter(parent=None).order_by("title_tex")

    context = {"journals": journals, "coltype": "journal"}
    context["journal"] = model_helpers.get_collection(settings.COLLECTION_PID)

    return render(request, "blocks/journal-list.html", context)


@require_http_methods(["GET"])
def actas(request, api=False):
    journals = model_helpers.get_actas().filter(parent=None)
    context = {"journals": journals, "coltype": "acta"}

    return render(request, "blocks/journal-list.html", context)


@require_http_methods(["GET"])
def books(request, api=False):
    books = model_helpers.get_collection_of_books().order_by("title_tex")
    context = {"journals": books, "coltype": "book"}

    return render(request, "blocks/journal-list.html", context)


@require_http_methods(["GET"])
def proceedings(request):
    proceedings = model_helpers.get_proceedings().order_by("title_tex")
    context = {"journals": proceedings, "coltype": "proceeding"}

    return render(request, "blocks/journal-list.html", context)


@require_http_methods(["GET"])
def thesis(request):
    if request.GET:  # i.e. /thesis?<search_term>
        return thesis_detailed_view(request)
    else:  # /thesis
        thesis = model_helpers.get_collection_of_thesis().order_by("title_tex")
        context = {"journals": thesis, "coltype": "thesis"}

        return render(request, "blocks/journal-list.html", context)


@require_http_methods(["GET"])
def sorted_lectures(request):  # i.e. /lectures?<search_term>
    if request.GET:
        return sorted_lectures_detailed_view(request)
    else:  # /lectures
        lectures = model_helpers.get_lectures().order_by("title_tex")
        context = {"journals": lectures, "coltype": "lectures"}

        return render(request, "blocks/journal-list.html", context)


@require_http_methods(["GET"])
def thesis_detailed_view(request, sorted_by="fau", order_by="asc", api=False):
    # request.path like : thesis?C-"Thèses de l'entre-deux-guerres"-rp/

    decoded_query_string = decode_query_string(request)

    path, queryDict = CleanSearchURL.decode(decoded_query_string, "/thesis")

    request.GET = queryDict
    request.path = path
    request.path_info = path

    filters = []
    filters = request.GET.getlist("f")

    path = request.path
    search_path = path + "?"

    try:
        page = int(request.GET.get("page", 1))
    except ValueError:
        page = 1

    params = {
        "q": '(classname:"Thèse")',
        "sorted_by": sorted_by,
        "order_by": order_by,
        "page": page,
        "filters": filters,
        "path": path,
        "search_path": search_path,
        "facet_fields": ["firstLetter", "year_facet", "collection_title_facet"],
        "query": decoded_query_string,
    }

    return generic_books(request, "thesis", params)


@require_http_methods(["GET"])
def sorted_lectures_detailed_view(
    request, collection=None, sorted_by="fau title_sort_key", order_by="asc", api=False
):
    decoded_query_string = decode_query_string(request)

    path, queryDict = CleanSearchURL.decode(decoded_query_string, "/lectures")

    request.GET = queryDict
    request.path = path
    request.path_info = path

    filters = []
    filters = request.GET.getlist("f")

    collection = model_helpers.get_collection(collection)

    path = request.path
    search_path = path + "?"

    try:
        page = int(request.GET.get("page", 1))
    except ValueError:
        page = 1

    params = {
        "q": '(classname:"Notes de cours")',
        "sorted_by": sorted_by,
        "order_by": order_by,
        "page": page,
        "filters": filters,
        "path": path,
        "search_path": search_path,
        "facet_fields": ["firstLetter", "year_facet", "collection_title_facet"],
        "query": decoded_query_string,
    }
    return generic_books(request, "lectures", params)


class IssuesView(TemplateView):
    template_name = "blocks/issue-list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        journal = model_helpers.get_collection(self.kwargs.get("jid"), sites="")

        if journal is None:
            raise Http404
        try:
            current_edition = journal.extlink_set.get(rel="website").location
            if "numdam.org" in current_edition:
                current_edition = None
        except models.ExtLink.DoesNotExist:
            current_edition = None

        result = model_helpers.get_volumes_in_collection(journal)
        volume_count = result["volume_count"]
        collections = []
        for ancestor in journal.ancestors.all():
            item = model_helpers.get_volumes_in_collection(ancestor)
            volume_count = max(0, volume_count)
            item.update({"journal": ancestor})
            collections.append(item)

        # add the parent collection to its children list and sort it by date
        result.update({"journal": journal})
        collections.append(result)
        if len(collections) > 1:
            collections.sort(
                key=lambda ancestor: ancestor["sorted_issues"][0]["volumes"][0]["lyear"],
                reverse=True,
            )

        is_cr = False
        if (
            hasattr(settings, "SITE_NAME")
            and len(settings.SITE_NAME) == 6
            and settings.SITE_NAME[0:2] == "cr"
        ):
            is_cr = True

        display_with_titles = True
        i = 0
        while display_with_titles and i < len(collections):
            col = collections[i]
            if len(col["sorted_issues"]) != 1:
                display_with_titles = False
            else:
                j = 0
                volumes = col["sorted_issues"][0]["volumes"]
                while display_with_titles and j < len(volumes):
                    vol = volumes[j]
                    display_with_titles = len(vol["issues"]) == 1
                    if display_with_titles:
                        issue = vol["issues"][0]
                        display_with_titles = issue.title_tex != ""
                    j += 1
            i += 1
        # for vserie in result["sorted_issues"]:
        #     for volume in vserie["volumes"]:
        #         print(volume["issues"][0])
        #         if volume["issues"][0].ctype == "issue_special":
        #             volume["issues"].reverse()
        #             continue

        context.update(
            {
                "obj": journal,
                "journal": journal,
                "sorted_issues": result["sorted_issues"],
                "coltype": journal.coltype,
                "volume_count": volume_count,
                "max_width": result["max_width"],
                "to_appear": models.Article.objects.filter(pid__startswith=f"{journal.pid}_0"),
                "show_issues": True,
                "is_cr": is_cr,
                "current_edition": current_edition,
                "collections": collections,
                "display_with_titles": display_with_titles,
            }
        )
        update_context_with_desc(context)

        return context


def generic_books(request, coltype, params):
    rows = 20
    start = (params["page"] - 1) * rows

    params["rows"] = 20
    params["start"] = start
    if " " in params["sorted_by"]:
        sorts = params["sorted_by"].split()
        params["sort"] = ",".join([f"{item} {params['order_by']}" for item in sorts])
    else:
        params["sort"] = params["sorted_by"] + " " + params["order_by"]

    cmd = solr_cmds.solrInternalSearchCmd(params)
    results = cmd.do()
    # si len(results.facets['collection_title_facets']) == 1,
    # on est dans le cas où une collection est choisie

    context = {"results": results, "coltype": coltype}

    if results is not None:
        if results.facets["collection_title_facets"]:
            has_one_collection_filter = False
            collection_title_filter = ""
            if "filters" in params:
                for filter_ in params["filters"]:
                    if filter_.find("collection_title_facet:") > -1:
                        has_one_collection_filter = (
                            True if not has_one_collection_filter else False
                        )
                        collection_title_filter = filter_.split('"')[1]

            if has_one_collection_filter:
                obj = model_helpers.get_resource(results.docs[0]["pid"])
                if obj:
                    book = obj.cast()
                    container = book.get_container()
                    collection = container.get_collection()
                    if collection.title_tex == collection_title_filter:
                        context["collection"] = collection
                    else:
                        for other_collection in container.my_other_collections.all():
                            if other_collection.title_tex == collection_title_filter:
                                context["collection"] = other_collection

        context.update(utils.paginate_from_request(request, params, results.hits))

    context["query"] = params["query"]
    if settings.SITE_NAME == "numdam":
        context["numdam"] = True
    return render(request, "blocks/sorted-books.html", context)


@require_http_methods(["GET"])
def sorted_books(request, sorted_by="fau title_sort_key", order_by="asc", api=False):
    decoded_query_string = decode_query_string(request)
    path, queryDict = CleanSearchURL.decode(decoded_query_string, "/series")

    request.GET = queryDict
    request.path = path
    request.path_info = path

    filters = []
    filters = request.GET.getlist("f")

    path = request.path
    search_path = path + "?"

    try:
        page = int(request.GET.get("page", 1))
    except ValueError:
        page = 1

    params = {
        "q": '(classname:"Livre")',
        "sorted_by": sorted_by,
        "order_by": order_by,
        "page": page,
        "filters": filters,
        "path": path,
        "search_path": search_path,
        "facet_fields": ["firstLetter", "year_facet", "collection_title_facet"],
        "query": decoded_query_string,
    }
    return generic_books(request, "book-series", params)


@method_decorator(csrf_exempt, name="dispatch")
class SearchView(TemplateView):
    template_name = "blocks/search-results.html"
    GET = None
    path = None

    def get(self, request, *args, **kwargs):
        decoded_query_string = decode_query_string(request)
        path = self.kwargs.get("path")
        # query like test-"aitre, -ert"-qa
        path, queryDict = CleanSearchURL.decode(decoded_query_string, path)
        self.GET = queryDict
        self.path = path
        try:
            result = super().get(request, *args, **kwargs)
        except SuspiciousOperation:
            result = HttpResponseBadRequest("Bad request")

        return result

    def post(self, request, *args, **kwargs):
        if request.POST.get("q0", "") == "":
            return HttpResponseRedirect(reverse("help"))

        show_eprint = request.POST.get("show_eprint", False)
        show_eprint = True if show_eprint == "on" else show_eprint
        path = CleanSearchURL.encode(request.POST, request.path)
        path += "&" + urllib.parse.urlencode({"eprint": show_eprint})

        return HttpResponseRedirect(path)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        raw_query_string = self.request.META.get("QUERY_STRING", "")
        if raw_query_string:
            decoded_query_string = urllib.parse.unquote(raw_query_string)
            parse = urllib.parse.parse_qs(decoded_query_string)
            context["query"] = decoded_query_string
        eprint = False

        try:
            if "eprint" in parse:
                eprint = eval(parse["eprint"][0])
                cache.set("eprint", eprint, 60 * 60 * 24 * 2)
        except:
            value = cache.get("eprint")
            eprint = False if not value else value
        finally:
            cache.close()

        filters = self.GET.getlist("f")

        if eprint is False:
            filters.append('!dt:"e-print"')

        search_path = self.path + "?"
        qs = []

        keep_qs_in_display = True
        i = 0
        qti = self.GET.get("qt" + str(i), None)

        while qti:
            if i > 0:
                search_path += "&"
            search_path += "qt" + str(i) + "=" + qti

            if qti == "date":
                qfi = self.GET.get("q-f-" + str(i), None)
                qli = self.GET.get("q-l-" + str(i), None)

                if qfi or qli:
                    qs.append({"name": qti, "first": qfi, "last": qli, "value": "", "not": False})

                    search_path += "&q-f-" + str(i) + "=" + qfi
                    search_path += "&q-l-" + str(i) + "=" + qli
            else:
                if qti == "author_ref":
                    keep_qs_in_display = False

                qi = self.GET.get("q" + str(i), None)

                if qi or i == 0:
                    noti = self.GET.get("not" + str(i), None)
                    if noti == "on":
                        noti = True
                    else:
                        noti = False

                    qs.append({"name": qti, "value": qi, "not": noti, "first": "", "last": ""})

                    search_path += "&q" + str(i) + "=" + qi

                    if noti:
                        search_path += "&not" + str(i) + "=on"

            i += 1
            qti = self.GET.get("qt" + str(i), None)

        try:
            page = int(self.GET.get("page", 1))
        except ValueError:
            page = 1

        if len(qs) < 1:
            # 400.html is used only if DEBUG is False
            # (see get_response in django.core.handlers.base)
            raise SuspiciousOperation("Bad request")

        rows = 20
        start = (page - 1) * rows

        params = {
            "filters": filters,
            "qs": qs,
            "page": page,
            "start": start,
            "rows": rows,
            "search_path": search_path,
            "path": self.path,
        }

        sort = self.GET.get("order_by")
        if sort:
            params["sort"] = sort

        cmd = solr_cmds.solrSearchCmd(params)
        results = cmd.do()

        if not keep_qs_in_display:
            qs = []

        context.update({"results": results, "qs": qs, "eprint": eprint})

        if results is not None:
            context.update(utils.paginate_from_request(self, params, results.hits))

        context["journal"] = model_helpers.get_collection(settings.COLLECTION_PID)

        return context


@require_http_methods(["GET"])
def authors(request, api=False):
    """
    @param request: like /authors?q=M+r
    @param api:
    @return:
    """

    decoded_query_string = decode_query_string(request)
    path, queryDict = CleanSearchURL.decode(decoded_query_string, "/authors")

    request.GET = queryDict
    request.path = path
    request.path_info = path

    # to set default value on letter
    letter = request.GET.get("letter", "A")[0]
    filters = request.GET.getlist("f")
    for filter_ in filters:
        if filter_.startswith("{!tag=firstletter}firstNameFacetLetter:"):
            letter = filter_[39]

    try:
        page = int(request.GET.get("page", 1))
    except ValueError:
        page = 1

    rows = 60

    all_authors = model_helpers.get_authors_by_letter(letter)
    paginator = Paginator(all_authors, rows)
    try:
        authors = paginator.page(page)
    except EmptyPage:
        raise Http404

    context = {
        "authors": authors,
        "letters": string.ascii_uppercase,
        "letter_active": letter,
        "query": decoded_query_string,
    }

    params = {"rows": rows, "page": page, "path": path}
    context.update(utils.paginate_from_request(request, params, paginator.count))

    return render(request, "blocks/authors.html", context)


@require_http_methods(["GET"])
def citations(request, aid, api=False):
    resource = model_helpers.get_resource(aid)
    if resource is None or (resource.classname != "Article" and resource.classname != "Container"):
        raise Http404
    # a priori les citations ne sont que sur les articles/book
    # resource.classname != 'Article'-part : et ben non !
    item = resource.cast()
    citing = item.citations()
    context = {"resource": item}
    context["citing"] = citing
    return render(request, "blocks/resource-citations.html", context)


@require_http_methods(["GET"])
def get_pdf(request, pid, binary_file_type, extension, relative_path):
    """
    return PDF file of an Resource or of a RelatedObject for Container if embargo not present
    si l'item n'existe pas : 404
    si il est protege par barriere mobile : alors on renvoie vers
    une page qui redirige apres un delai vers la notice de l'objet
    """

    return get_binary_file(request, pid, binary_file_type, extension, relative_path)


@require_http_methods(["GET"])
def get_binary_file(request, pid, binary_file_type, extension, relative_path):
    """
    return tex file of a Resource if embargo not present
    return 404 if the file or the resource does not exist
    If there is an embargo redirect to the resource page
    """

    if len(relative_path) > 0:
        extension = relative_path.split(".")[-1]

    type_extension = {
        "pdf": "application/pdf",
        "djvu": "image/x.djvu",
        "tex": "application/x-tex",
        "png": "image/png",
        "jpg": "image/jpeg",
        "html": "text/html",
    }
    mimetype = type_extension.get(extension, "")

    # binary_file_type: 'toc', 'frontmatter', 'backmatter', or 'self' for
    # the article/issue binary file
    if not binary_file_type:
        binary_file_type = "self"

    if "/" in pid:
        resource = model_helpers.get_resource_by_doi(pid)
    else:
        resource = model_helpers.get_resource(pid)

    if resource is not None:
        resource = resource.cast()

    filename, status = get_binary_filename(resource, relative_path, binary_file_type, mimetype)
    return render_binary_file(request, resource, status, filename)


def get_binary_filename(resource, relative_path, binary_file_type, mimetype):
    """
    get the filename from the database
    returns the filename and the status 200, 404 (not found) or 403 (embargo)
    """
    if resource is None:
        return None, 404

    allow_local_pdf = not hasattr(settings, "ALLOW_LOCAL_PDF") or settings.ALLOW_LOCAL_PDF
    if not allow_local_pdf:
        return None, 404

    filename = None
    status = 200

    try:
        # May return None if there is an embargo
        filename = resource.get_binary_disk_location(binary_file_type, mimetype, relative_path)
    except exceptions.ResourceDoesNotExist:
        status = 404

    # File is protected
    if filename is None:
        status = 403

    return filename, status


def render_binary_file(request, resource, status, filename):
    if status == 404:
        raise Http404
    elif status == 403:
        template_name = "403withRedirect.html"
        response = render(request, template_name, {"pid": resource.pid}, status=403)
    else:
        full_filename = os.path.join(settings.RESOURCES_ROOT, filename)

        try:
            file_ = open(full_filename, "rb")
        except OSError:
            print("cannot open ", full_filename)
            raise Http404
        else:
            response = sendfile(request, full_filename)
            response["Accept-Ranges"] = "bytes"
            file_.close()
    return response


##########################################################################
#
# Views that update model objects (besides import/upload)
#
##########################################################################
class APILookupView(View):
    def get(self, request):
        title = request.GET["title"]
        year = request.GET.get("year", False)
        authors = request.GET.get("authors", False)
        params = {
            "qs": [
                {"name": "title", "value": title, "not": False, "first": "", "last": ""},
            ],
        }
        if year:
            params["qs"].append(
                {"name": "date", "value": None, "not": False, "first": year, "last": year},
            )
        if authors:
            params["qs"].append(
                {"name": "author", "value": authors, "not": False, "first": "", "last": ""},
            )
        cmd = solr_cmds.solrSearchCmd(params)
        results = cmd.do()
        if len(results.docs):
            data = {"pid": results.docs[0]["pid"]}
        else:
            data = {}
        return JsonResponse(data)


class APIFetchView(View):
    def get(self, request):
        data = ""
        pid = request.GET["pid"]
        if pid:
            article = get_object_or_404(models.Article, pid=pid)
            data = article.get_citation(with_formatting=True)
        return HttpResponse(data)


class ArticlesAPIView(View):
    def get(self, request):
        # values_list returns queryset in format [('id',) ('id',)]
        nested_article_pids = models.Article.objects.values_list("pid")
        # We flatten it to a normal list ['id', 'id']
        article_pids = list(chain.from_iterable(nested_article_pids))
        return JsonResponse(
            {"articles": {"ids": article_pids, "total": nested_article_pids.count()}}
        )


class ArticleDumpAPIView(View):
    def get(self, request, *args, **kwargs):
        pid = kwargs.get("pid", None)

        if "/" in pid:
            article = model_helpers.get_article_by_doi(pid, prefetch=True)
        else:
            article = model_helpers.get_article(pid, prefetch=True)
        if not article:
            raise Http404

        date_accepted = date_published = date_online_first = None
        if article.date_accepted:
            date_accepted = article.date_accepted.strftime("%Y-%m-%d")
        if article.date_online_first:
            date_online_first = article.date_online_first.strftime("%Y-%m-%d")
        if article.date_published:
            date_published = article.date_published.strftime("%Y-%m-%d")
        date_received = (
            article.date_received.strftime("%Y-%m-%d") if article.date_received else None
        )
        date_revised = article.date_revised.strftime("%Y-%m-%d") if article.date_revised else None

        author_names = models.get_names(article, "author")
        if author_names:
            authors = "; ".join(author_names)
        else:
            author_names = models.get_names(article, "editor")
            if author_names:
                authors = "; ".join(author_names) + " (" + str(_("éd.")) + ")"
            else:
                authors = None

        page_count = article.get_article_page_count()

        result = {
            "title_tex": article.title_tex,
            "title_html": article.title_html,
            "trans_title_tex": article.trans_title_tex,
            "trans_title_html": article.trans_title_html,
            "lang": article.lang,
            "doi": article.doi or None,
            "pid": article.pid,
            "authors": authors,
            "issue_pid": article.my_container.pid,
            "colid": article.my_container.my_collection.pid,
            "volume": article.my_container.volume,
            "number": article.my_container.number,
            "year": article.my_container.year,
            "issue_title": article.my_container.title_tex,
            "citation": article.get_citation(request),
            "date_accepted": date_accepted,
            "date_online_first": date_online_first,
            "date_published": date_published,
            "date_received": date_received,
            "date_revised": date_revised,
            "page_count": page_count,
            "body_html": article.body_html,
            "pci_section": article.get_pci_section(),
        }

        extids = []
        for extid in article.extid_set.all():
            extids.append([extid.id_type, extid.id_value])
        result["extids"] = extids

        result["kwds"] = [
            {"type": kwd.type, "lang": kwd.lang, "value": kwd.value}
            for kwd in article.kwd_set.all()
        ]

        awards = []
        for award in article.award_set.all():
            awards.append([award.abbrev, award.award_id])
        result["awards"] = awards

        abstracts = []
        for abstract in article.abstract_set.all():
            abstracts.append(
                {
                    "lang": abstract.lang,
                    "value_tex": abstract.value_tex,
                    "value_html": abstract.value_html,
                }
            )
        result["abstracts"] = abstracts

        bibitems = []
        for bib in article.bibitem_set.all():
            bibitems.append(bib.citation_html)
        result["bibitems"] = bibitems

        return JsonResponse(result)


class BookDumpAPIView(View):
    def get(self, request, *args, **kwargs):
        pid = kwargs.get("pid", None)

        book = model_helpers.get_container(pid)
        if not book:
            raise Http404

        result = {
            "title_tex": book.title_tex,
            "title_html": book.title_html,
            "doi": book.doi or "",
            "citation": book.get_citation(request),
        }

        extids = []
        for extid in book.extid_set.all():
            extids.append([extid.id_type, extid.id_value])
        result["extids"] = extids

        result["kwds"] = [
            {"type": kwd.type, "lang": kwd.lang, "value": kwd.value} for kwd in book.kwd_set.all()
        ]

        abstracts = []
        for abstract in book.abstract_set.all():
            abstracts.append(
                {
                    "lang": abstract.lang,
                    "value_tex": abstract.value_tex,
                    "value_html": abstract.value_html,
                }
            )
        result["abstracts"] = abstracts

        bibitems = []
        for bib in book.bibitem_set.all():
            bibitems.append(bib.citation_html)
        result["bibitems"] = bibitems

        return JsonResponse(result)


class AllIssuesAPIView(View):
    def get(self, request):
        issues_pids = list(models.Container.objects.values_list("pid", flat=True).order_by("pid"))
        return JsonResponse({"issues": issues_pids})


class CollectionIssnAPIView(View):
    def get(self, request, *args, **kwargs):
        collection = get_object_or_404(
            models.Collection,
            resourceid__id_value=kwargs.get("issn"),
        )
        if collection.parent:
            url = (
                ""
                if collection.parent.pid == collection.pid
                else collection.parent.get_absolute_url()
            )
            url += f"#{collection.pid}"
            return JsonResponse({"url": url})
        return JsonResponse({"url": collection.get_absolute_url()})


class CollectionsAPIView(View):
    def get(self, request):
        collections_pids = list(
            models.Collection.objects.filter(parent__isnull=True).values_list("pid", flat=True)
        )
        return JsonResponse({"collections": collections_pids})


class CollectionExportCSV(View):
    def get(self, request, *args, **kwargs):
        colid = kwargs.get("colid")
        collection = get_object_or_404(models.Collection, pid=colid)

        response = HttpResponse(content_type="text/csv")
        response["Content-Disposition"] = f'attachment; filename="{collection.pid}.csv"'

        csv_header = [
            "doi",
            "title",
            "date_accepted",
            "date_first_publication",
        ]

        writer = csv.writer(response, delimiter="\t")
        writer.writerow(csv_header)

        for article in models.Article.objects.filter(
            my_container__my_collection__pid=colid
        ).order_by("-date_accepted"):
            if article.date_published is not None or article.date_online_first is not None:
                first_online = (
                    article.date_online_first
                    if (article.date_online_first is not None and colid.lower()[0:2] == "cr")
                    else article.date_published
                )
                writer.writerow(
                    [
                        f'=LIEN.HYPERTEXTE("https://doi.org/{article.doi}"; "{article.doi}")',
                        xml_utils.normalise_span(article.title_tex),
                        article.date_accepted.strftime("%Y-%m-%d")
                        if article.date_accepted is not None
                        else "",
                        first_online.strftime("%Y-%m-%d"),
                    ]
                )

        return response


class IssuesAPIView(View):
    def get(self, request, *args, **kwargs):
        colid = kwargs.get("colid", None)
        issues_pids = list(
            models.Container.objects.filter(
                Q(my_collection__pid=colid) | Q(my_collection__parent__pid=colid)
            )
            .values_list("pid", flat=True)
            .order_by("pid")
        )
        return JsonResponse({"issues": issues_pids})


class IssueListAPIView(View):
    def get(self, request, *args, **kwargs):
        pid = kwargs.get("pid", None)
        articles_pids = list(
            models.Article.objects.filter(my_container__pid=pid)
            .values_list("pid", flat=True)
            .order_by("pid")
        )
        return JsonResponse({"articles": articles_pids})


class ItemXMLView(View):
    def get(self, request, *args, **kwargs):
        pid = kwargs.get("pid", None)
        full_xml = self.request.GET.get("full_xml", "1")

        full_xml = False if full_xml == "0" or full_xml == "false" else True
        if pid.find(".") > 0:
            # The id given is a DOI
            article = model_helpers.get_article_by_doi(pid)
            if article:
                pid = article.pid
            else:
                raise Http404

        xml_body = ptf_cmds.exportPtfCmd(
            {
                "pid": pid,
                "with_internal_data": False,
                "with_binary_files": False,
                "for_archive": True,
                "full_xml": full_xml,
            }
        ).do()

        return HttpResponse(xml_body, content_type="application/xml")


class ItemFileListAPIView(View):
    def get(self, request, *args, **kwargs):
        pid = kwargs.get("pid", None)

        resource = model_helpers.get_resource(pid)
        if not resource:
            raise Http404

        obj = resource.cast()
        binary_files = obj.get_binary_files_location()

        result = {"files": binary_files}

        if obj.classname == "Container":
            articles = []
            for article in obj.article_set.all():
                article_files = article.get_binary_files_location()
                articles.append({"pid": article.pid, "files": article_files})
            result["articles"] = articles

        return JsonResponse(result)


class APIMatchOneView(View):
    def get(self, request, *args, **kwargs):
        pid = kwargs.get("pid", None)
        seq = kwargs.get("seq", 0)
        what = kwargs.get("what", "")

        resource = model_helpers.get_resource(pid)
        if not resource:
            raise Http404

        if what not in ["zbl-item-id", "mr-item-id", "doi", "numdam-id", "pmid"]:
            return HttpResponseBadRequest(
                "Bad request: 'what' has to be one"
                "of {zbl-item-id, mr-item-id, doi, numdam-id, pmid}"
            )
        result = ""

        status = 200
        message = ""

        try:
            obj = resource.cast()
            seq = int(seq)
            if seq == 0:
                # Article, Book or Book part match
                if obj.classname.lower() == "article":
                    result = matching.match_article(obj, what)
                else:
                    # TODO match book
                    pass
            else:
                bibitem = model_helpers.get_bibitem_by_seq(obj, seq)
                if not bibitem:
                    raise Http404
                result = matching.match_bibitem(bibitem, what)

            message = pid + " " + str(seq) + " " + what + " : " + result
        except Timeout as exception:
            return HttpResponse(exception, status=408)
        except Exception as exception:
            return HttpResponseServerError(exception)

        data = {"message": message, "status": status}
        return JsonResponse(data)


class APIMatchAllView(View):
    @staticmethod
    def get_existing_ids(pid, what, force):
        query_bibitemids = (
            models.BibItemId.objects.filter(
                bibitem__resource__pid__contains=pid,
                id_type__in=what,
            )
            .select_related(
                "bibitem",
                "bibitem__resource",
            )
            .only("bibitem__resource__pid", "bibitem__sequence", "id_type")
        )
        query_extids = models.ExtId.objects.filter(
            resource__pid__contains=pid,
            id_type__in=what,
        ).select_related("resource")
        if force == "1":
            query_bibitemids = query_bibitemids.exclude(checked=False, false_positive=False)
            query_extids = query_extids.exclude(checked=False, false_positive=False)
        if force == "2":
            query_bibitemids = models.BibItemId.objects.none()
            query_extids = models.ExtId.objects.none()
        bibitemids = {
            (bibitemid.bibitem.resource.pid, bibitemid.bibitem.sequence, bibitemid.id_type)
            for bibitemid in query_bibitemids
        }
        extids = {(extid.resource.pid, 0, extid.id_type) for extid in query_extids}
        return bibitemids.union(extids)

    @staticmethod
    def get_possible_ids(pid, what):
        bibitems = models.BibItem.objects.filter(resource__pid__contains=pid).select_related(
            "resource"
        )
        articles = models.Article.objects.filter(pid__contains=pid).exclude(
            classname="TranslatedArticle"
        )
        bibitemids = {
            (item.resource.pid, item.sequence, type_)
            for type_ in what
            for item in bibitems
            if type_ != "pmid"
        }
        # we remove doi from possible extids
        if "doi" in what:
            what.remove("doi")
        extids = {(article.pid, 0, type_) for type_ in what for article in articles}
        return bibitemids.union(extids)

    @staticmethod
    def delete_ids_if_necessary(pid, what, force):
        if force == "1":
            models.BibItemId.objects.filter(
                bibitem__resource__pid__contains=pid,
                id_type__in=what,
                checked=False,
                false_positive=False,
            ).delete()
            models.ExtId.objects.filter(
                resource__pid__contains=pid,
                id_type__in=what,
                checked=False,
                false_positive=False,
            ).delete()
        if force == "2":
            models.BibItemId.objects.filter(
                bibitem__resource__pid__contains=pid, id_type__in=what
            ).delete()
            models.ExtId.objects.filter(resource__pid__contains=pid, id_type__in=what).delete()

    def get(self, request, *args, **kwargs):
        pid = kwargs.get("pid", None)
        what = kwargs.get("what", "all")
        force = kwargs.get("force", "0")
        # if what contains several targets, they are separated with an underscore
        what = what.split("_")
        if force != "0":
            self.delete_ids_if_necessary(pid, what, force)
        existing = self.get_existing_ids(pid, what, force)
        possible = self.get_possible_ids(pid, what)
        ids = list(possible - existing)
        ids.sort(key=itemgetter(0, 1, 2))
        final = [f"{pid}/{number}/{what}" for pid, number, what in ids]
        return JsonResponse({"ids": final})


class UpdateMatchingView(View):
    resource = None

    def post_update(self):
        model_helpers.post_resource_updated(self.resource)

    def update_obj(self, action):
        if action == "delete":
            models.ExtId.objects.filter(resource=self.resource).delete()
            models.BibItemId.objects.filter(bibitem__resource=self.resource).delete()
        elif action == "mark-checked":
            models.ExtId.objects.filter(resource=self.resource).update(checked=True)
            models.BibItemId.objects.filter(bibitem__resource=self.resource).update(checked=True)
        elif action == "mark-unchecked":
            models.ExtId.objects.filter(resource=self.resource).update(checked=False)
            models.BibItemId.objects.filter(bibitem__resource=self.resource).update(checked=False)

        for bibitem in models.BibItem.objects.filter(resource=self.resource):
            cmd = xml_cmds.updateBibitemCitationXmlCmd()
            cmd.set_bibitem(bibitem)
            cmd.do()

        self.post_update()

    def get(self, request, *args, **kwargs):
        pid = kwargs.get("pid", None)
        action = kwargs.get("action", None)

        self.resource = model_helpers.get_resource(pid)
        if not self.resource:
            raise Http404

        self.update_obj(action)

        url = reverse("article", kwargs={"aid": self.resource.pid})
        return HttpResponseRedirect(url)


class UpdateExtIdView(View):
    obj = None
    resource = None
    parent = None

    def get_obj(self, pk):
        try:
            extid = models.ExtId.objects.get(pk=pk)
        except models.ExtId.DoesNotExist:
            raise Http404

        self.obj = extid
        self.resource = extid.resource

    def post_update(self):
        model_helpers.post_resource_updated(self.resource)

    def update_obj(self, action):
        if not self.obj:
            raise Http404

        if action == "delete":
            self.obj.delete()
        elif action == "toggle-checked":
            self.obj.checked = False if self.obj.checked else True
            self.obj.save()
        elif action == "toggle-false-positive":
            self.obj.false_positive = False if self.obj.false_positive else True
            self.obj.save()

        self.post_update()

    def get(self, request, *args, **kwargs):
        pk = kwargs.get("pk", None)
        action = kwargs.get("action", None)

        self.get_obj(pk)
        self.update_obj(action)

        if action == "delete":
            url = reverse("article", kwargs={"aid": self.resource.pid})
            return HttpResponseRedirect(url)
        return JsonResponse({})


class UpdateBibItemIdView(UpdateExtIdView):
    def get_obj(self, pk):
        try:
            bibitemid = models.BibItemId.objects.get(pk=pk)
        except models.BibItemId.DoesNotExist:
            raise Http404

        self.obj = bibitemid
        self.parent = bibitemid.bibitem
        self.resource = bibitemid.bibitem.resource

    def post_update(self):
        cmd = xml_cmds.updateBibitemCitationXmlCmd()
        cmd.set_bibitem(self.parent)
        cmd.do()

        model_helpers.post_resource_updated(self.resource)


class APIFetchId(View):
    def get(self, request, *args, **kwargs):
        id_ = kwargs.get("id", None)
        what = kwargs.get("what", None)
        pk = kwargs.get("pk")
        resource = kwargs["resource"]

        if what == "pmid":
            data = {"result": "", "status": 200}
            return JsonResponse(data)

        if not id_:
            return HttpResponseBadRequest("Bad request: 'id' is none")

        if what not in ["zbl-item-id", "mr-item-id", "doi", "numdam-id"]:
            return HttpResponseBadRequest(
                "Bad request: 'what' has to be one" "of {zbl-item-id, mr-item-id, doi, numdam-id}"
            )

        try:
            result = matching.fetch_id(id_, what)
        except (IndexError, RequestException) as exception:
            return HttpResponseServerError(exception)

        if resource == "bibitemid":
            bibitem = models.BibItem.objects.get(pk=pk)
            raw = bibitem.citation_html
        else:
            article = models.Article.objects.get(pk=pk)
            raw = article.get_citation(request)
        result = highlight_diff(raw, result)

        data = {"result": result, "status": 200}
        return JsonResponse(data)


class APIFetchAllView(View):
    def get(self, request, *args, **kwargs):
        pid = kwargs.get("pid", None)

        article = model_helpers.get_article(pid)
        if not article:
            raise Http404

        ids = matching.get_all_fetch_ids(article)

        data = {"ids": ids}
        return JsonResponse(data)


@require_http_methods(["GET"])
def malsm_books(request, pid="MALSM", api=False):
    template = "malsm.html"
    context = {
        "journal": model_helpers.get_collection(pid),
        "coltype": "book",
        "template": template,
    }

    return render(request, template, context)


class LatestArticlesFeed(Feed):
    link = ""
    ttl = 120

    def get_feed(self, obj, request):
        feed = super().get_feed(obj, request)
        feed.feed["language"] = get_language()
        return feed

    def get_object(self, request, name, jid=""):
        """
        Select the site whose RSS feed is requested.
        It is annotated with an optional `requested_col_id` for sites with multiple collections (ex: proceedings).
        """
        self.request = request
        return models.Site.objects.annotate(requested_col_id=Value(jid)).get(name=name)

    def title(self, obj):
        if obj.requested_col_id:
            collection = get_object_or_404(
                models.Collection, sites__name=obj.name, pid=obj.requested_col_id
            )
        else:
            collection = get_object_or_404(models.Collection, sites__name=obj.name)
        return collection.title_sort

    def description(self):
        return _("Flux RSS des derniers articles parus")

    def items(self, obj):
        qs = models.Article.objects.filter(sites=obj.pk)
        if obj.requested_col_id:
            qs = qs.filter(my_container__my_collection__pid=obj.requested_col_id)
        return qs.exclude(classname="TranslatedArticle").order_by(
            Greatest("date_online_first", "date_published").desc(nulls_last=True), "-seq"
        )[:20]

    def item_title(self, item):
        return f"{item.get_authors_short()} - {item.title_html}"

    def item_description(self, item):
        language = get_language()
        abstracts = item.get_abstracts()
        if not abstracts:
            return self.item_title(item)
        try:
            return abstracts.get(lang=language).value_html
        except models.Abstract.DoesNotExist:
            return abstracts.first().value_html

    def item_pubdate(self, item):
        if item.date_published:
            return item.date_published
        return item.date_online_first

    def item_link(self, item):
        if item.doi:
            return f"{settings.DOI_BASE_URL}{item.doi}"
        return item.get_absolute_url()


class LatestIssue(ItemView):
    def get(self, request, *args, **kwargs):
        pid = kwargs.get("pid")
        if pid is None:
            return HttpResponse(status=404)

        container_issues = (
            models.Container.objects.filter(my_collection__pid=pid)
            .all()
            .order_by("-vseries_int", "-year", "-volume_int", "-number_int")
        )
        if container_issues is None:
            return HttpResponse(status=404)

        container = container_issues.first()
        url = container.get_absolute_url()
        return HttpResponseRedirect(url)


class RSSTemplate(TemplateView):
    template_name = "blocks/syndication_feed.html"

    def dispatch(self, request, *args, **kwargs):
        # Numdam does not have an rss feed for now
        if settings.SITE_ID == 3:
            raise Http404
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["journal"] = model_helpers.get_collection(settings.COLLECTION_PID)

        return context


class ArticleEditAPIView(View):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.doi = None
        self.colid = None
        self.edit_all_fields = False
        self.fields_to_update = (
            []
        )  # Must be used to specify the fields to update (title, authors, references...)

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def save_data(self, data_article):
        pass

    def restore_data(self, article):
        pass

    def convert_data_for_editor(self, data_article):
        data_article.trans_title_formulas = []
        data_article.title_formulas = []
        data_article.abstract_formulas = []
        data_article.trans_abstract_formulas = []

        data_article.title_tex, data_article.trans_title_tex = get_tex_from_xml(
            data_article.title_xml, "title", add_span_around_tex_formula=True
        )

        for contrib in data_article.contributors:
            contrib["address_text"] = "\n".join([address for address in contrib["addresses"]])

        if len(data_article.abstracts) == 0:
            data_article.abstracts = [
                {
                    "tag": "abstract",
                    "lang": data_article.lang,
                    "value_html": "",
                    "value_tex": "",
                    "value_xml": "",
                }
            ]

        for abstract in data_article.abstracts:
            if abstract["value_xml"]:
                value_tex = get_tex_from_xml(
                    abstract["value_xml"], "abstract", add_span_around_tex_formula=True
                )
            else:
                value_tex = ""
            abstract["value_tex"] = value_tex

        data_article.conference = ", ".join(
            [subj["value"] for subj in data_article.subjs if subj["type"] == "conference"]
        )
        data_article.topics = [
            subj["value"] for subj in data_article.subjs if subj["type"] == "topic"
        ]

        data_article.pci_section = "".join(
            [subj["value"] for subj in data_article.subjs if subj["type"] == "pci"]
        )

        data_article.subjs = [
            subj
            for subj in data_article.subjs
            if subj["type"] not in ["conference", "topic", "pci"]
        ]

        with_ordered_label = True
        for i, ref in enumerate(data_article.bibitems, start=1):
            model_data_converter.convert_refdata_for_editor(ref)
            if ref.label != f"[{str(i)}]":
                with_ordered_label = False
        data_article.bibitems_with_ordered_label = with_ordered_label

        ext_link = model_data.get_extlink(data_article, "icon")
        data_article.icon_url = None
        if ext_link:
            data_article.icon_url = resolver.get_icon_url(None, ext_link["location"])

    def convert_data_from_editor(self, data_article):
        xtitle = CkeditorParser(
            html_value=data_article.title_tex,
            mml_formulas=data_article.title_formulas,
            ignore_p=True,
        )
        data_article.title_html = xtitle.value_html
        data_article.title_tex = xtitle.value_tex

        trans_title_xml = ""
        if data_article.trans_title_tex:
            xtranstitle = CkeditorParser(
                html_value=data_article.trans_title_tex,
                mml_formulas=data_article.trans_title_formulas,
                ignore_p=True,
            )
            data_article.trans_title_html = xtranstitle.value_html
            data_article.trans_title_tex = xtranstitle.value_tex
            trans_title_xml = xtranstitle.value_xml

        data_article.title_xml = get_title_xml(
            xtitle.value_xml, trans_title_xml, data_article.trans_lang, with_tex_values=False
        )

        for contrib in data_article.contributors:
            contrib["addresses"] = []
            if "address_text" in contrib:
                contrib["addresses"] = contrib["address_text"].split("\n")

        for i, abstract in enumerate(data_article.abstracts):
            if i > 0:
                xabstract = CkeditorParser(
                    html_value=abstract["value_tex"],
                    mml_formulas=data_article.trans_abstract_formulas,
                )
            else:
                xabstract = CkeditorParser(
                    html_value=abstract["value_tex"], mml_formulas=data_article.abstract_formulas
                )
            abstract["value_html"] = xabstract.value_html
            abstract["value_tex"] = xabstract.value_tex
            abstract["value_xml"] = jats_from_abstract(
                abstract["lang"], data_article.lang, xabstract
            )

        data_article.subjs = [
            subj for subj in data_article.subjs if subj["type"] not in ["conference", "topic"]
        ]
        if data_article.topics:
            data_article.subjs.extend(
                [{"type": "topic", "lang": "en", "value": topic} for topic in data_article.topics]
            )
        if data_article.conference:
            data_article.subjs.append(
                {"type": "conference", "lang": "en", "value": data_article.conference}
            )
        if data_article.pci_section:
            data_article.subjs.append(
                {"type": "pci", "lang": "en", "value": data_article.pci_section}
            )

        if self.edit_all_fields or "bibitems" in self.fields_to_update:
            for i, ref in enumerate(data_article.bibitems, start=1):
                if data_article.bibitems_with_ordered_label and ref["type"] != "unknown":
                    ref["label"] = f"[{str(i)}]"
                if "doi" in ref and len(ref["doi"]) > 0:
                    doi = xml_utils.clean_doi(ref["doi"])
                    ref["doi"] = doi
                    ref["extids"] = [["doi", doi]]
                else:
                    ref["extids"] = []
                    # URLs are in <comment>
                    # if 'url' in ref and len(ref['url']) > 0:
                    #     ref['ext_links'] = [{'rel': '',
                    #                          'mimetype': '',
                    #                          'location': ref['url'],
                    #                          'base': '',
                    #                          'metadata': ''}]
                    # else:
                    #     ref['ext_links'] = []

                author_array = ref["contribs_text"].split("\n")
                contribs = []
                for author_txt in author_array:
                    if author_txt:
                        lastname = firstname = ""
                        pos = author_txt.find(", ")
                        if pos > 0:
                            lastname = author_txt[0:pos]
                            firstname = author_txt[pos + 2 :]
                        else:
                            lastname = author_txt

                        contrib = model_data.create_contributor()
                        contrib["first_name"] = firstname
                        contrib["last_name"] = lastname
                        contrib["role"] = "author"
                        contrib["contrib_xml"] = xml_utils.get_contrib_xml(contrib)
                        contribs.append(contrib)
                ref["contributors"] = contribs

    def replace_p(self, obj, key):
        text = obj[key].replace("</p>", "")
        pos = text.find("<p>")
        if pos > -1:
            text = text[0:pos] + text[pos + 3 :]
            text = text.replace("<p>", "\n")
        obj[key] = text

    def replace_return(self, obj, key):
        text_array = obj[key].split("\n")
        text = "<br>".join([s for s in text_array])
        obj[key] = text

    def get(self, request, *args, **kwargs):
        doi = kwargs.get("doi", None)
        # colid = kwargs.get("colid", None)

        article = model_helpers.get_article_by_doi(doi, prefetch=True)
        if not article:
            raise Http404

        data_article = model_data_converter.db_to_article_data(article)
        self.convert_data_for_editor(data_article)

        def obj_to_dict(obj):
            return obj.__dict__

        dump = json.dumps(data_article, default=obj_to_dict)

        return HttpResponse(dump, content_type="application/json")

    def update_article(self, article_data, icon_file):
        self.save_data(article_data)

        collection = model_helpers.get_collection(self.colid)

        model_data_converter.update_data_for_jats(article_data)

        issue = None
        existing_article = model_helpers.get_article_by_doi(article_data.doi)
        new_data_article = None

        if existing_article is not None:
            issue = existing_article.my_container
            new_data_article = model_data_converter.db_to_article_data(existing_article)
        else:
            new_data_article = model_data.create_articledata()
            new_data_article.pid = article_data.pid
            new_data_article.doi = article_data.doi

        if self.edit_all_fields:
            new_data_article = article_data
        else:
            for field in self.fields_to_update:
                value = getattr(article_data, field)
                setattr(new_data_article, field, value)

        if self.edit_all_fields or "ext_links" in self.fields_to_update:
            # New icon
            if icon_file and issue:
                relative_file_name = resolver.copy_file_obj_to_article_folder(
                    icon_file, self.colid, issue.pid, article_data.pid
                )
                ext_link = model_data.get_extlink(new_data_article, "icon")
                if ext_link is None:
                    ext_link = model_data.create_extlink()
                    ext_link["rel"] = "icon"
                    ext_link["location"] = relative_file_name
                new_data_article.ext_links.append(ext_link)

            # No or removed icon
            if not icon_file and hasattr(article_data, "icon_url") and not article_data.icon_url:
                new_data_article.ext_links = [
                    e for e in new_data_article.ext_links if e["rel"] != "icon"
                ]

        cmd = xml_cmds.addArticleXmlCmd(
            {"xarticle": new_data_article, "use_body": False, "issue": issue, "standalone": True}
        )
        cmd.set_collection(collection)
        article = cmd.do()

        self.restore_data(article)

    def post(self, request, *args, **kwargs):
        self.colid = kwargs.get("colid", None)
        self.doi = kwargs.get("doi", None)

        body_unicode = request.POST.get("data", "")
        body = json.loads(body_unicode)

        article_data = Munch(body)
        new_bibitems = []
        for bib in article_data.bibitems:
            new_bibitems.append(Munch(bib))
        article_data.bibitems = new_bibitems

        new_relations = []
        for relation in article_data.relations:
            new_relations.append(Munch(relation))
        article_data.relations = new_relations

        new_translations = []
        for translation in article_data.translations:
            new_translations.append(Munch(translation))
        article_data.translations = new_translations

        self.convert_data_from_editor(article_data)

        icon_file = None
        if "icon" in request.FILES:
            icon_file = request.FILES["icon"]

        self.update_article(article_data, icon_file)

        return JsonResponse({"message": "OK"})


class CollectionEditAPIView(View):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.colid = None
        self.collection = None
        self.edit_all_fields = True
        self.fields_to_update = (
            []
        )  # Must be used to specify the fields to update (title, wall, periods...)

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def save_data(self, data):
        pass

    def restore_data(self, collection):
        pass

    def convert_data_for_editor(self, data):
        data.title, data.trans_title = get_tex_from_xml(
            data.title_xml, "title", add_span_around_tex_formula=True
        )
        # TODO: move periods inside GDML
        data.periods = []

    def convert_data_from_editor(self, data):
        xtitle = CkeditorParser(html_value=data.title_tex, mml_formulas=[], ignore_p=True)
        data.title_html = xtitle.value_html
        data.title_tex = xtitle.value_tex

        # TODO: get_title_xml for Collections
        data.title_xml = get_title_xml(xtitle.value_xml, with_tex_values=False)

        ids = []
        if data.issn:
            ids.append(["issn", data.issn])
        if data.e_issn:
            ids.append(["e-issn", data.e_issn])
        data.ids = ids

    def replace_p(self, obj, key):
        text = obj[key].replace("</p>", "")
        pos = text.find("<p>")
        if pos > -1:
            text = text[0:pos] + text[pos + 3 :]
            text = text.replace("<p>", "\n")
        obj[key] = text

    def replace_return(self, obj, key):
        text_array = obj[key].split("\n")
        text = "<br>".join([s for s in text_array])
        obj[key] = text

    def get(self, request, *args, **kwargs):
        colid = kwargs.get("colid", None)

        self.collection = collection = model_helpers.get_collection(colid, sites=False)
        if not collection:
            raise Http404

        data = model_data_converter.db_to_publication_data(collection)
        self.convert_data_for_editor(data)

        def obj_to_dict(obj):
            return obj.__dict__

        dump = json.dumps(data, default=obj_to_dict)

        return HttpResponse(dump, content_type="application/json")

    def update_collection(self, data):
        self.save_data(data)

        existing_collection = model_helpers.get_collection(self.colid, sites=False)
        if existing_collection is None:
            cls = ptf_cmds.addCollectionPtfCmd
            new_data = data
        else:
            cls = ptf_cmds.updateCollectionPtfCmd

            if self.edit_all_fields:
                new_data = data
            else:
                new_data = model_data_converter.db_to_publication_data(existing_collection)

                for field in self.fields_to_update:
                    value = getattr(data, field)
                    setattr(new_data, field, value)

        params = {"xobj": new_data, "solr_commit": False}
        cmd = cls(params)
        if new_data.provider:
            provider = model_helpers.get_provider_by_name(new_data.provider)
        else:
            provider = model_helpers.get_provider("mathdoc-id")
        cmd.set_provider(provider)
        collection = cmd.do()

        # TODO: Move xml_cmds add_objects_with_location inside ptf_cmds
        # self.add_objects_with_location(xcol.ext_links, collection, "ExtLink")

        self.restore_data(collection)

        return collection

    def post(self, request, *args, **kwargs):
        self.colid = kwargs.get("colid", None)

        body_unicode = request.body.decode("utf-8")
        # POST.get('data') has to be used with a VueJS FormData
        # body_unicode = request.POST.get('data', '')
        body = json.loads(body_unicode)

        data = Munch(body)
        self.convert_data_from_editor(data)

        self.update_collection(data)

        return JsonResponse({"message": "OK"})


class ArticleCitedByView(View):
    def get(self, request, *args, **kwargs):
        doi = self.kwargs.get("doi", "").strip("/")

        if "/" in doi:
            resource = model_helpers.get_resource_by_doi(doi)
        else:
            resource = model_helpers.get_resource(doi)

        if not resource:
            raise Http404

        citations, sources, for_stats = citedby.get_citations(resource)
        if citations:
            status_code = 200
        else:
            status_code = 204

        data = {
            "result": {"citations": citations, "sources": sources, "doi": doi, "json": for_stats},
            "status": status_code,
        }
        return JsonResponse(data)


def export_citation(request, **kwargs):
    data = ""
    pid = kwargs.get("pid", "")
    ext = kwargs.get("ext", "")

    if "/" in pid:
        resource = model_helpers.get_resource_by_doi(pid)
    else:
        resource = model_helpers.get_resource(pid)

    if not resource:
        return HttpResponse(status=404)

    if hasattr(resource, "article"):
        document = resource.article
    else:
        document = model_helpers.get_container(resource.pid)

    filename = "citation." + ext
    if ext == "bib":
        data = document.get_bibtex(request)
    elif ext == "ris":
        data = document.get_ris(request)
    elif ext == "enw":
        data = document.get_endnote(request)

    if data:
        response = HttpResponse(data, content_type="text/html; charset=UTF-8")
        response["Content-Disposition"] = "attachment; filename=%s" % filename
    else:
        response = HttpResponse(status=204)
    return response


def get_tokenized(body):
    soup = BeautifulSoup(body, "html.parser")
    math_symbols = {}
    i = 0

    for tag in soup.select("span.mathjax-formula"):
        if tag.name == "span":
            math_symbols[i] = tag.decode_contents()
            tag.string = "[math_symbol_" + str(i) + "]"
            i += 1
        # else:
        #     links[j] = tag.decode_contents()
        #     tag.string = '$link_' + str(j) + '$'
        #     j += 1

    body = str(soup)

    return {"body": body, "tokens": {"math": math_symbols}}


def detokenize(body, tokens):
    soup = BeautifulSoup(body, "html.parser")

    for tag in soup.select("span.mathjax-formula"):
        token_name = re.sub(r"\[|\]", "", tag.string)
        id_list = re.findall(r"\d+", token_name)
        if id_list:
            tag_id = int(id_list[0])
            if tag_id in tokens["math"]:
                tag.string = tokens["math"][tag_id]

    body = html.unescape(str(soup))

    return body


class ExportArticleHtml(View):
    def get(self, request, **kwargs):
        doi = kwargs.get("doi", None)
        if doi[-1] == "/":
            doi = doi[:-1]
        tokenize = kwargs.get("tokenize", None)
        a = Article.objects.get(doi=doi)
        body = a.body_html
        pid = a.__str__()

        if tokenize is not None:
            # tokenize = int(tokenize)

            tokens_infos = get_tokenized(body)
            body = tokens_infos["body"]
            # tokens = tokens_infos["tokens"]

        with NamedTemporaryFile("a+", encoding="utf-8") as html_article:
            html_article.write(body)
            html_article.seek(0)
            response = HttpResponse(html_article, content_type="text/html; charset=utf-8")
            response["Content-Disposition"] = "attachment; filename=" + pid + ".html"

            return response


class RecentArticlesPublished(View):
    # La méthode permet de retourner 3 articles récents au format JSON avec pour informations :
    # - titre de l'article
    # - le(s) auteur(s) de l'article
    # - collection de l'article
    # - doi
    # - Infos sur l'article
    # - url

    def get(self, request):
        articles = models.Article.objects.all().order_by(
            Greatest("date_online_first", "date_published").desc(nulls_last=True)
        )[:100]
        articles_infos = []
        container_pids = []
        nb_articles = articles.count()
        i = 0
        while i < nb_articles and len(articles_infos) < 3:
            article = articles[i]
            if article.my_container is not None and article.my_container.pid not in container_pids:
                container_pids.append(article.my_container.pid)
                if article.date_published:
                    item = {
                        "title": article.title_html,
                        "authors": article.get_authors_short(),
                        "collection": article.my_container.my_collection.title_tex,
                        "doi": article.doi,
                        "citation_source": article.get_citation_base(),
                        "url": resolver.get_doi_url(article.doi),
                    }
                    articles_infos.append(item)
            i += 1
        return JsonResponse({"articles": articles_infos})


def get_first_n_authors(authors, n=3):
    if len(authors) > n:
        authors = authors[0:n]
        authors.append("...")
    return "; ".join(authors)


def get_suggested_articles(article):
    documents = []
    obj, created = models.RelatedArticles.objects.get_or_create(resource=article)
    solr_cmds.auto_suggest_doi(obj, article)

    if obj.doi_list:
        exclusion = obj.exclusion_list.split() if obj.exclusion_list else []
        # Filter the articles in the exclusion list and that have a DOI that includes the article DOI (translations)
        dois = [
            doi
            for doi in obj.doi_list.split()
            if doi not in exclusion and doi.find(article.doi) != 0
        ]
        for doi in dois:
            suggest = Article.objects.filter(doi=doi).first()
            doc = {}
            base_url = ""
            if suggest:
                doc = vars(suggest)
                doc["authors"] = models.get_names(suggest, "author")
                if suggest.my_container:
                    collection = suggest.my_container.my_collection
                    base_url = collection.website() or ""
                    doc["year"] = suggest.my_container.year
                    doc["journal_abbrev"] = collection.abbrev
            else:
                try:
                    doc = crossref.crossref_request(doi)
                except:
                    continue
                doc["title_html"] = doc.get("title", "")
                doc["journal_abbrev"] = doc.get("journal", "")
                authors = doc.get("authors", ";").split(";")
                doc["authors"] = [" ".join(au.split(",")).title() for au in authors]

            if doc:
                doc["authors"] = get_first_n_authors(doc.get("authors", []))
                if base_url and suggest:
                    doc["url"] = base_url + "/articles/" + doi
                else:
                    doc["url"] = resolver.get_doi_url(doi)
                documents.append(doc)
    return documents


@csrf_exempt
def update_suggest(request, doi):
    if request.method == "POST":
        article = Article.objects.filter(doi=doi).first()
        if not article:
            return JsonResponse({"message": "No article found"})
        obj, created = models.RelatedArticles.objects.get_or_create(resource=article)
        obj.resource_doi = article.doi
        obj.doi_list = request.POST.get("doi_list")
        obj.date_modified = request.POST.get("date_modified")
        obj.exclusion_list = request.POST.get("exclusion_list")
        obj.automatic_list = request.POST.get("automatic_list", True)
        obj.save()
        return JsonResponse({"message": "OK"})


@csrf_exempt
def graphical_abstract(request, doi):
    article = get_object_or_404(models.Article, doi=doi)
    if request.method == "POST":
        obj, created = models.GraphicalAbstract.objects.get_or_create(resource=article)
        obj.resource_doi = article.doi
        obj.date_modified = request.POST.get("date_modified")
        obj.graphical_abstract = request.FILES.get("graphical_abstract")
        obj.illustration = request.FILES.get("illustration")
        obj.save()
        return JsonResponse({"message": "OK"})
    elif request.method == "DELETE":
        obj = get_object_or_404(models.GraphicalAbstract, resource=article)
        if obj:
            obj.delete()
        return JsonResponse({"message": "OK"})


class MoreLikeThisView(View):
    def get(self, request, *args, **kwargs):
        doi = self.kwargs.get("doi", "")
        article = model_helpers.get_resource_by_doi(doi)
        if not article:
            raise Http404

        results = solr_cmds.research_more_like_this(article)
        return JsonResponse(results)


class HelpView(TemplateView):
    template_name = "help.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["journal"] = model_helpers.get_collection(settings.COLLECTION_PID)

        return context


def home(request):
    context = {}
    return render(request, "help.html", context)
