from __future__ import annotations

from dataclasses import dataclass
from dataclasses import field

from django.conf import settings
from django.urls import reverse
from django.urls import reverse_lazy
from django.utils import timezone
from django.utils.translation import gettext as translate_text
from django.utils.translation import gettext_lazy as _

from ptf.model_helpers import get_issues_in_volume
from ptf.utils import volume_display


@dataclass
class BreadcrumbItem:
    """
    Interface for a breadcrumb item.
    """

    title: str
    url: str
    icon_class: str = field(default="")
    html: bool = field(default=False)

    def __init__(self, title: str, url: str, icon_class: str = "", html=False):
        self.title = title
        self.url = url


@dataclass
class Breadcrumb:
    """
    Interface for Breadcrumb data.
    """

    previousItem: BreadcrumbItem = None
    nextItem: BreadcrumbItem = None
    items: list[BreadcrumbItem] = field(default_factory=list)

    def add_item(self, title: str, url: str, icon_class: str = "", html=False):
        self.items.append(BreadcrumbItem(title=title, url=url, icon_class=icon_class, html=html))


def get_base_breadcrumb() -> Breadcrumb:
    """
    Returns a breadcrumb with the default "Home" breadcrumb item.
    """
    return Breadcrumb(
        items=[
            BreadcrumbItem(
                title=_("Home"), url=reverse_lazy("mesh:home"), icon_class="fa-house-chimney"
            )
        ]
    )


# logging.basicConfig(level=logging.DEBUG)


def get_breadcrumb(resource, is_volume=False):  # noqa: F811 (TODO: remove above function)
    """
    Solution with a visitor
    """
    if settings.SITE_NAME in ["crchim", "crphys", "crmeca", "crbiol", "crgeos", "crmath"]:
        visitor = BreadcrumbVisitorCR()
    else:
        visitor = BreadcrumbVisitor()

    if is_volume:
        resource = Volume(resource)

    crumb = resource.accept(visitor)

    return crumb


#########################################################################################
#
# Other solutions
#
# Solution 1:
#   BCBuilder with functions dynamically added to the Article/Container/Collection
#
# Solution 2:
#   BCBuilder Article/Container/Collection mixin dynamically added the base classes
#   a VolumeBCMixin class is added to handle volumes
#
# Solution 3:
#   A visitor
#
#########################################################################################

'''
class BCBuilder:
    def __init__(self):
        self.crumb = Breadcrumb()

    def add_article_bcitem(self, article):
        display_page = True
        if hasattr(settings, "PAGE_BREADCRUMB"):
            display_page = settings.PAGE_BREADCRUMB
        if display_page:
            href = article.get_absolute_url()
            title = article.get_breadcrumb_page_text()
            self.crumb.add_item(title, href)

    def add_collection_bcitem(self, collection):
        href = collection.get_absolute_url()

        if collection.pid == "MALSM":
            href = reverse("malsm_books")

        # We show the collection title instead of the basic 'Feuilleter' for the sites with several collections:
        # Numdam, Geodesic, Proceedings
        if settings.COLLECTION_PID in ["ALL", "PROCEEDINGS"]:
            title = collection.title_html
        elif collection.pid in settings.CRAS_COLLECTIONS:
            title = _("Consulter")
        else:
            title = _("Feuilleter")

        self.crumb.add_item(title, href)

    def add_volume_bcitem(self, container):
        if container and container.ctype == "issue":
            volume = VolumeBCMixin(container)
            href = volume.get_absolute_url()

            if hasattr(settings, "YEAR_BREADCRUMB"):
                title = str(container.year)
            else:
                title = (
                    "{} {} : ".format(_("Série"), container.vseries) if container.vseries else ""
                )
                if container.volume:
                    title += f"{volume_display()} {container.volume} ({container.year})"
                else:
                    title += "{} {}".format(_("Année"), container.year)
            self.crumb.add_item(title, href)

    def add_container_bcitem(self, container):
        title = ""
        href = ""

        colid = container.get_top_collection().pid
        if colid in settings.CRAS_COLLECTIONS and not container.title_html:
            year = int(container.year)
            if (colid != "CRBIOL" and year > 2020) or (colid == "CRBIOL" and year > 2022):
                now = timezone.now()
                curyear = str(now.year)
                if curyear == container.year:
                    title = translate_text("Articles du volume en cours")
                else:
                    title = translate_text("Articles du volume")
                href = reverse("volume-general-items", kwargs={"vid": container.get_vid()})

        if not title and container.number:
            title = "no." + " " + container.number
            href = container.get_absolute_url()

        if title:
            self.crumb.add_item(title, href)


#############################################################################
#
# Solution 1: BCBuilder with functions
# (not complete, need to extend the resources with get_next and get_previous)
#
#############################################################################


# def article_build_breadcrumb(self):
#     collection = self.get_top_collection()
#     container = self.get_container()
#
#     breadcrumb_builder = BCBuilder()
#
#     breadcrumb_builder.add_collection_bcitem(collection)
#     breadcrumb_builder.add_container_bcitem(container)
#     breadcrumb_builder.add_article_bcitem(self)
#
#     breadcrumb_builder.set_next_previous_article_bc_items(self)
#
#     return breadcrumb_builder
#
#
# def container_build_breadcrumb(self):
#     collection = self.get_top_collection()
#
#     breadcrumb_builder = BCBuilder()
#
#     breadcrumb_builder.add_collection_bcitem(collection)
#     breadcrumb_builder.add_container_bcitem(self)
#
#     breadcrumb_builder.set_next_previous_container_bc_items(self)
#
#     return breadcrumb_builder
#
#
# def collection_build_breadcrumb(self):
#     breadcrumb_builder = BCBuilder()
#
#     breadcrumb_builder.add_collection_bcitem(self)
#
#     return breadcrumb_builder

# Article.build_breadcrumb = article_build_breadcrumb
# Article.get_next = article_get_next
# Container.build_breadcrumb = container_build_breadcrumb
# Container.get_next = container_get_next
# Collection.build_breadcrumb = collection_build_breadcrumb


###########################################
#
# Solution 2: BCBuilder with mixin
#
###########################################


class ResourceBCMixin:
    def get_next(self):
        return None

    def get_previous(self):
        return None

    def _next_in_qs(self, qs):
        next_item = None

        if qs.count() > 1:
            ready_for_next = False
            for item in qs:
                if ready_for_next:
                    next_item = item
                    ready_for_next = False
                if item.pid == self.pid:
                    ready_for_next = True
        return next_item


class ArticleBCMixin(ResourceBCMixin):
    def build_breadcrumb(self):
        collection = self.get_top_collection()
        container = self.get_container()

        breadcrumb_builder = BCBuilder()

        breadcrumb_builder.add_collection_bcitem(collection)
        breadcrumb_builder.add_volume_bcitem(container)
        breadcrumb_builder.add_container_bcitem(container)
        breadcrumb_builder.add_article_bcitem(self)

        self.set_next_previous_bc_items(breadcrumb_builder.crumb)

        return breadcrumb_builder.crumb

    def get_next(self):
        next_article = None

        try:
            next_article = self.my_container.article_set.get(seq=(self.seq + 1))
        except (Article.DoesNotExist, MultipleObjectsReturned):
            pass

        if next_article is None and not self.my_container.title_html and self.my_container.volume:
            qs = Container.objects.filter(volume=self.my_container.volume)
            if qs.count() > 1:
                qs = qs.order_by("number_int")
                next_issue = self._next_in_qs(qs)
                if next_issue:
                    qs = next_issue.article_set.order_by("seq")
                    if qs.exists():
                        next_article = qs.first()

        return next_article

    def get_previous(self):
        previous_article = None

        try:
            previous_article = self.my_container.article_set.get(seq=(self.seq - 1))
        except (Article.DoesNotExist, MultipleObjectsReturned):
            pass

        if (
            previous_article is None
            and not self.my_container.title_html
            and self.my_container.volume
        ):
            qs = Container.objects.filter(volume=self.my_container.volume)
            if qs.count() > 1:
                qs = qs.order_by("-number_int")
                previous_issue = self._next_in_qs(qs)
                if previous_issue:
                    qs = previous_issue.article_set.order_by("-seq")
                    if qs.exists():
                        previous_article = qs.first()

        return previous_article

    def set_next_previous_bc_items(self, crumb):
        next_article = self.get_next()
        if next_article is not None:
            crumb.nextItem = BreadcrumbItem(
                translate_text("Suivant"), next_article.get_absolute_url()
            )

        previous_article = self.get_previous()
        if previous_article is not None:
            crumb.previousItem = BreadcrumbItem(
                translate_text("Précédent"), previous_article.get_absolute_url()
            )

        # Les articles de CRAS des volumes généraux sont mis dans plusieurs containers (G1, G2, ...)
        # On regarde s'il y a un numéro (caché) suivant/précédant pour y prendre le premier/dernier article
        if (next_article is None or previous_article is None) and not self.my_container.title_html:
            issues_article, collection = get_issues_in_volume(self.my_container.pid, True, True)

            if next_article is None:
                return_next = None
                current_found = False
                index = 0
                articles = issues_article[0]["articles"]
                while index < len(articles) and return_next is None:
                    if current_found:
                        return_next = articles[index]
                    if articles[index].pid == self.pid:
                        current_found = True
                    index += 1
                if return_next is not None:
                    crumb.nextItem = BreadcrumbItem(
                        translate_text("Suivant"), return_next.get_absolute_url()
                    )
            if previous_article is None:
                return_prev = None
                current_found = False
                articles = issues_article[0]["articles"]
                index = len(articles) - 1
                while index >= 0 and return_prev is None:
                    if current_found:
                        return_prev = articles[index]
                    if articles[index].pid == self.pid:
                        current_found = True
                    index -= 1
                if return_prev is not None:
                    crumb.previousItem = BreadcrumbItem(
                        translate_text("Précédent"), return_prev.get_absolute_url()
                    )


class ContainerBCMixin(ResourceBCMixin):
    def build_breadcrumb(self):
        collection = self.get_top_collection()

        breadcrumb_builder = BCBuilder()

        breadcrumb_builder.add_collection_bcitem(collection)
        breadcrumb_builder.add_volume_bcitem(self)
        breadcrumb_builder.add_container_bcitem(self)

        self.set_next_previous_bc_items(breadcrumb_builder.crumb)

        return breadcrumb_builder.crumb

    def get_next(self):
        # No Next/Previous for CRAS Special Issues or "Volume articles"
        colid = self.get_top_collection().pid
        if colid in settings.CRAS_COLLECTIONS:
            year = int(self.year)
            if (
                self.title_html
                or (colid != "CRBIOL" and year > 2020)
                or (colid == "CRBIOL" and year > 2022)
            ):
                return None

        collection = self.get_top_collection()
        if collection.pid in settings.COLLECTIONS_SEQUENCED:
            qs = collection.content.order_by("seq")
        else:
            qs = collection.content.order_by("vseries_int", "year", "volume_int", "number_int")

        next_issue = self._next_in_qs(qs)
        return next_issue

    def get_previous(self):
        # No Next/Previous for CRAS Special Issues or "Volume articles"
        colid = self.get_top_collection().pid
        if colid in settings.CRAS_COLLECTIONS:
            year = int(self.year)
            if (
                self.title_html
                or (colid != "CRBIOL" and year > 2020)
                or (colid == "CRBIOL" and year > 2022)
            ):
                return None

        collection = self.get_top_collection()
        if collection.pid in settings.COLLECTIONS_SEQUENCED:
            qs = collection.content.order_by("-seq")
        else:
            qs = collection.content.order_by("-vseries_int", "-year", "-volume_int", "-number_int")
        next_issue = self._next_in_qs(qs)
        return next_issue

    def set_next_previous_bc_items(self, crumb):
        next_container = self.get_next()
        if next_container is not None:
            title = translate_text("Suivant")
            href = next_container.get_absolute_url()
            crumb.nextItem = BreadcrumbItem(title, href)

        previous_container = self.get_previous()
        if previous_container is not None:
            title = translate_text("Précédent")
            href = previous_container.get_absolute_url()
            crumb.previousItem = BreadcrumbItem(title, href)


class VolumeBCMixin(ContainerBCMixin):
    """
    There is no Volume class in the PTF Model.
    VolumeBCMixin is not a mixin but a full class.
    In order to work with get_breadcrumb, we need to add Resource functions such as get_absolute_url
    """

    def __init__(self, container_bc, **kwargs):
        super().__init__(**kwargs)
        self.container_bc = container_bc

    def __getattribute__(self, name):
        try:
            value = super().__getattribute__(name)
        except AttributeError:
            value = self.container_bc.__getattribute__(name)
        return value

    def build_breadcrumb(self):
        collection = self.container_bc.get_top_collection()

        breadcrumb_builder = BCBuilder()

        breadcrumb_builder.add_collection_bcitem(collection)
        breadcrumb_builder.add_volume_bcitem(self)

        self.set_next_previous_bc_items(breadcrumb_builder.crumb)

        return breadcrumb_builder.crumb

    def get_absolute_url(self):
        if hasattr(settings, "YEAR_BREADCRUMB"):
            href = reverse("articles-year", kwargs={"year": self.container_bc.year})
        else:
            href = reverse("volume-items", kwargs={"vid": self.container_bc.get_vid()})

        return href

    def _next_in_volume_qs(self, qs):
        next_item = None

        if qs.count() > 1:
            ready_for_next = False
            for item in qs:
                if ready_for_next and (
                    item.year != self.container_bc.year or item.volume != self.container_bc.volume
                ):
                    next_item = item
                    ready_for_next = False
                if item.pid == self.container_bc.pid:
                    ready_for_next = True
        return next_item

    def get_next(self):
        next_container = None
        collection = self.container_bc.get_top_collection()
        if collection.pid in settings.COLLECTIONS_SEQUENCED:
            next_container = self.container_bc.get_next()
        else:
            qs = collection.content.order_by("vseries_int", "year", "volume_int", "number_int")
            next_container = self._next_in_volume_qs(qs)

        if next_container:
            # Convert to VolumeBCMixin to have access to the "volume" get_absolute_url
            next_container = VolumeBCMixin(next_container)

        return next_container

    def get_previous(self):
        previous_container = None
        collection = self.container_bc.get_top_collection()
        if collection.pid in settings.COLLECTIONS_SEQUENCED:
            previous_container = self.container_bc.get_previous()
        else:
            qs = collection.content.order_by("-vseries_int", "-year", "-volume_int", "-number_int")
            previous_container = self._next_in_volume_qs(qs)

        if previous_container:
            # Convert to VolumeBCMixin to have access to the "volume" get_absolute_url
            previous_container = VolumeBCMixin(previous_container)

        return previous_container


class CollectionBCMixin(ResourceBCMixin):
    def build_breadcrumb(self):
        breadcrumb_builder = BCBuilder()

        breadcrumb_builder.add_collection_bcitem(self)

        return breadcrumb_builder.crumb

'''
###########################################
#
# Solution 3: a visitor
#
###########################################


class BreadcrumbVisitor:
    def __init__(self):
        self.crumb = Breadcrumb()

    def visit(self, resource):
        meth = getattr(self, "visit_" + resource.classname.lower())
        return meth(resource)

    def visit_article(self, article):
        collection = article.get_top_collection()
        container = article.get_container()

        self._add_collection_bcitem(collection)
        self._add_volume_bcitem(container)
        self._add_container_bcitem(container)
        self._add_article_bcitem(article)

        next_article, previous_article = self.set_next_previous_article_bc_items(article)
        if next_article is not None:
            self.crumb.nextItem = BreadcrumbItem(
                translate_text("Suivant"), next_article.get_absolute_url()
            )

        if previous_article is not None:
            self.crumb.previousItem = BreadcrumbItem(
                translate_text("Précédent"), previous_article.get_absolute_url()
            )

        return self.crumb

    def visit_container(self, container):
        collection = container.get_top_collection()

        self._add_collection_bcitem(collection)
        self._add_volume_bcitem(container)
        self._add_container_bcitem(container)

        self.set_next_previous_container_bc_items(container)

        return self.crumb

    def visit_volume(self, volume):
        collection = volume.get_top_collection()

        self._add_collection_bcitem(collection)
        self._add_volume_bcitem(volume)

        self.set_next_previous_container_bc_items(volume)

        return self.crumb

    def visit_collection(self, collection):
        self._add_collection_bcitem(collection)

        return self.crumb

    def _add_article_bcitem(self, article):
        display_page = True
        if hasattr(settings, "PAGE_BREADCRUMB"):
            display_page = settings.PAGE_BREADCRUMB
        if display_page:
            href = article.get_absolute_url()
            title = article.get_breadcrumb_page_text()
            self.crumb.add_item(title, href)

    def _add_collection_bcitem(self, collection):
        href = collection.get_absolute_url()

        if collection.pid == "MALSM":
            href = reverse("malsm_books")

        # We show the collection title instead of the basic 'Feuilleter' for the sites with several collections:
        # Numdam, Geodesic, Proceedings
        if settings.COLLECTION_PID in ["ALL", "PROCEEDINGS"]:
            title = collection.title_html
        elif collection.pid in settings.CRAS_COLLECTIONS:
            title = _("Consulter")
        else:
            title = _("Feuilleter")

        self.crumb.add_item(title, href)

    def _add_volume_bcitem(self, container):
        if container and container.ctype == "issue":
            volume = Volume(container)
            href = volume.get_absolute_url()

            if hasattr(settings, "YEAR_BREADCRUMB"):
                title = str(container.year)
            else:
                title = (
                    "{} {} : ".format(_("Série"), container.vseries) if container.vseries else ""
                )
                if container.volume:
                    title += f"{volume_display()} {container.volume} ({container.year})"
                else:
                    title += "{} {}".format(_("Année"), container.year)
            self.crumb.add_item(title, href)

    def _add_container_bcitem(self, container):
        title = ""
        href = ""

        colid = container.get_top_collection().pid
        if colid in settings.CRAS_COLLECTIONS and not container.title_html:
            year = int(container.year)
            if (colid != "CRBIOL" and year > 2020) or (colid == "CRBIOL" and year > 2022):
                now = timezone.now()
                curyear = str(now.year)
                if curyear == container.year:
                    title = translate_text("Articles du volume en cours")
                else:
                    title = translate_text("Articles du volume")
                href = reverse("volume-general-items", kwargs={"vid": container.get_vid()})

        if not title and container.number:
            title = "no." + " " + container.number
            href = container.get_absolute_url()

        if title:
            self.crumb.add_item(title, href)

    def set_next_previous_container_bc_items(self, container):
        next_container = container.get_next_resource()
        if next_container is not None:
            title = translate_text("Suivant")
            href = next_container.get_absolute_url()
            self.crumb.nextItem = BreadcrumbItem(title, href)

        previous_container = container.get_previous_resource()
        if previous_container is not None:
            title = translate_text("Précédent")
            href = previous_container.get_absolute_url()
            self.crumb.previousItem = BreadcrumbItem(title, href)

    def set_next_previous_article_bc_items(self, article):
        next_article = article.get_next_resource()
        if next_article is not None:
            self.crumb.nextItem = BreadcrumbItem(
                translate_text("Suivant"), next_article.get_absolute_url()
            )

        previous_article = article.get_previous_resource()
        if previous_article is not None:
            self.crumb.previousItem = BreadcrumbItem(
                translate_text("Précédent"), previous_article.get_absolute_url()
            )
        return next_article, previous_article


class BreadcrumbVisitorCR(BreadcrumbVisitor):
    def set_next_previous_article_bc_items(self, article):
        next_article, previous_article = super().set_next_previous_article_bc_items(article)
        # Les articles de CRAS des volumes généraux sont mis dans plusieurs containers (G1, G2, ...)
        # On regarde s'il y a un numéro (caché) suivant/précédant pour y prendre le premier/dernier article
        if not article.my_container.title_html:
            issues_article, collection = get_issues_in_volume(article.my_container.pid, True, True)

            if next_article is None:
                return_next = None
                current_found = False
                index = 0
                articles = issues_article[0]["articles"]
                while index < len(articles) and return_next is None:
                    if current_found:
                        return_next = articles[index]
                    if articles[index].pid == article.pid:
                        current_found = True
                    index += 1
                if return_next is not None:
                    self.crumb.nextItem = BreadcrumbItem(
                        translate_text("Suivant"), return_next.get_absolute_url()
                    )
            if previous_article is None:
                return_prev = None
                current_found = False
                articles = issues_article[0]["articles"]
                index = len(articles) - 1
                while index >= 0 and return_prev is None:
                    if current_found:
                        return_prev = articles[index]
                    if articles[index].pid == article.pid:
                        current_found = True
                    index -= 1
                if return_prev is not None:
                    self.crumb.previousItem = BreadcrumbItem(
                        translate_text("Précédent"), return_prev.get_absolute_url()
                    )
        return next_article, previous_article


class Volume:
    """
    There is no Volume class in the PTF Model.
    Add it here to handle breadcrumb code for volumes
    """

    def __init__(self, container, **kwargs):
        super().__init__(**kwargs)
        self.container = container
        self.classname = "Volume"

    def __getattribute__(self, name):
        try:
            value = super().__getattribute__(name)
        except AttributeError:
            value = self.container.__getattribute__(name)
        return value

    def cast(self):
        return self

    def accept(self, visitor):
        """
        We need to copy the resource:accept function here.
        If we do not, the call to volume.accept() will look for accept.
        The __get__attribute__ function will find the accept function of the encapsulated container function
        => accept will be called for the encapsulated container
        """
        return visitor.visit(self)

    def get_absolute_url(self):
        if hasattr(settings, "YEAR_BREADCRUMB"):
            href = reverse("articles-year", kwargs={"year": self.container.year})
        else:
            href = reverse("volume-items", kwargs={"vid": self.container.get_vid()})

        return href

    def _next_in_volume_qs(self, qs):
        next_item = None

        if qs.count() > 1:
            ready_for_next = False
            for item in qs:
                if ready_for_next and (
                    item.year != self.container.year or item.volume != self.container.volume
                ):
                    next_item = item
                    ready_for_next = False
                if item.pid == self.container.pid:
                    ready_for_next = True
        return next_item

    def get_next_resource(self):
        next_container = None
        collection = self.container.get_top_collection()
        if collection.pid in settings.COLLECTIONS_SEQUENCED:
            next_container = self.container.get_next_resource()
        else:
            qs = collection.content.order_by("vseries_int", "year", "volume_int", "number_int")
            next_container = self._next_in_volume_qs(qs)

        if next_container:
            # Convert to VolumeBCMixin to have access to the "volume" get_absolute_url
            next_container = Volume(next_container)

        return next_container

    def get_previous_resource(self):
        previous_container = None
        collection = self.container.get_top_collection()
        if collection.pid in settings.COLLECTIONS_SEQUENCED:
            previous_container = self.container.get_previous_resource()
        else:
            qs = collection.content.order_by("-vseries_int", "-year", "-volume_int", "-number_int")
            previous_container = self._next_in_volume_qs(qs)

        if previous_container:
            # Convert to VolumeBCMixin to have access to the "volume" get_absolute_url
            previous_container = Volume(previous_container)

        return previous_container
