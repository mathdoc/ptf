from django.db import migrations
from django.conf import settings
from ptf.models import *


def insert_sites(apps, schema_editor):
    site = PtfSite(
        id=settings.SITE_ID, domain=settings.SITE_DOMAIN, name=settings.SITE_NAME, acro=settings.SITE_NAME
    )
    site.save()


class Migration(migrations.Migration):

    dependencies = [
        ('ptf', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(insert_sites)
    ]
