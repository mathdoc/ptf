# Generated by Django 2.2.24 on 2021-12-03 10:26

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0004_alter_site_options'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='site',
            options={'ordering': ('domain',), 'verbose_name': 'site', 'verbose_name_plural': 'sites'},
        ),
    ]
