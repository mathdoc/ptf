from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
        ('sites', '0002_initialize_sites'),
    ]

    operations = []
