class ResourceExists(Exception):
    pass


class ResourceDoesNotExist(Exception):
    pass


class ResourceHasNoDoi(Exception):
    pass


class ServerUnderMaintenance(Exception):
    pass


class PDFException(Exception):
    """
    Any problem while compiling a PDF
    """


class DOIException(Exception):
    """
    Any problem while recording a DOI
    """
