# Generated by Django 2.2.13 on 2020-06-22 15:30

import django.contrib.sites.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ptf', '0036_auto_20200506_1043'),
    ]

    operations = [
        migrations.AlterModelManagers(
            name='ptfsite',
            managers=[
                ('objects', django.contrib.sites.models.SiteManager()),
            ],
        ),
        migrations.AlterField(
            model_name='resource',
            name='abbrev',
            field=models.CharField(blank=True, db_index=True, max_length=128),
        ),
    ]
