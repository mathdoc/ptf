# Generated by Django 4.2.16 on 2024-09-06 13:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ptf', '0081_alter_article_options'),
    ]

    operations = [
        migrations.AlterField(
            model_name='extlink',
            name='location',
            field=models.CharField(max_length=300),
        ),
    ]
