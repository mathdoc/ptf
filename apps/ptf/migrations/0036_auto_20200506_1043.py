# -*- coding: utf-8 -*-
# Generated by Django 1.11.29 on 2020-06-29 13:19
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ptf', '0035_auto_20200506_1041'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bibitem',
            name='annotation',
            field=models.CharField(default='', max_length=255),
        ),
        migrations.AlterField(
            model_name='bibitem',
            name='month',
            field=models.CharField(default='', max_length=16),
        ),
    ]
