# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2019-05-06 09:58
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ptf', '0029_auto_20190503_1356'),
        ('ptf', '0027_supplementarymaterial'),
    ]

    operations = [
    ]
