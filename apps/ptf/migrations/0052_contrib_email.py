# Generated by Django 2.2.20 on 2021-10-15 14:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ptf', '0051_auto_20210906_0953'),
    ]

    operations = [
        migrations.AddField(
            model_name='contrib',
            name='email',
            field=models.EmailField(blank=True, db_index=True, default='', max_length=254),
        ),
    ]
