# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-03-02 09:32
from __future__ import unicode_literals

from django.db import migrations

from django.core.management import call_command

from ptf.cmds import ptf_cmds

fixture = 'initial_data'

def load_fixture(apps, schema_editor):
    call_command('loaddata', fixture, app_label='ptf')

    #cmd = ptf_cmds.addSitePtfCmd( { "name":"numdam", "acro":"numdam", "domain": "numdam" } )


def unload_fixture(apps, schema_editor):
    "Brutally deleting all entries for this model..."

    MyModel = apps.get_model("ptf", "PtfSite")
    MyModel.objects.all().delete()

class Migration(migrations.Migration):

    dependencies = [
        ('ptf', '0001_initial'),
        ('ptf', '0002_relationname_migration')
    ]

    operations = [
        migrations.RunPython(load_fixture, reverse_code=unload_fixture),
    ]
