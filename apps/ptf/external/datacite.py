import requests

from ptf import model_data


def get_datacite_journal_url(publisher):
    publisher = publisher.replace(" ", "%20")
    #    return f"https://api.datacite.org/dois?prefix={prefix}&fields[dois]=id,publisher

    return f"https://api.datacite.org/dois?query=publisher:%22{publisher}%22&page[size]=1000"


def get_datacite_doi_url(doi):
    return f"https://api.datacite.org/dois/{doi}"


def get_datacite_articles_in_journal(publisher, what):
    url = get_datacite_journal_url(publisher)
    response = requests.get(url)
    response.raise_for_status()
    data = response.json()

    dois_to_ignore = ["10.15781/md28-ws10"]

    article_datas = []
    for datacite_data in data["data"]:
        if datacite_data["id"] not in dois_to_ignore:
            xarticle = parse_datacide_data(datacite_data["attributes"], what)
            if xarticle is not None:
                article_datas.append(xarticle)

    return article_datas

    # dois_to_ignore = ["10.15781/8c5z-fs65", "10.15781/z70n-ed29", "10.15781/md28-ws10", "10.15781/7187-xq59"]
    # dois = [item["id"] for item in data["data"] if item["id"] not in dois_to_ignore ]
    #
    # for doi in dois:
    #     xarticle = get_datacite_doi(doi, what)
    #     if xarticle is not None:
    #         article_datas.append(xarticle)

    return article_datas


def get_datacite_doi(doi, what):
    url = get_datacite_doi_url(doi)
    response = requests.get(url)
    response.raise_for_status()
    response.encoding = "utf-8"
    data = response.json()["data"]["attributes"]

    xarticle = parse_datacide_data(data, what)
    return xarticle


def parse_datacide_data(data, what):
    xarticle = model_data.create_articledata()
    doi = data["doi"]
    xarticle.doi = doi
    xarticle.pid = doi.replace("/", "_").replace(".", "_").replace("-", "_")
    xarticle.url = f"https://doi.org/{doi}"

    if "year" in what:
        xarticle.year = data["publicationYear"]

    if "published" in what:
        xarticle.date_published_iso_8601_date_str = data["created"]

    if "primary_url" in what:
        # DataCite does not give the primary url, returns the DOI url instead
        xarticle.url = f"https://doi.org/{doi}"

    return xarticle
