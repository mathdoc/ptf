from datetime import timedelta

import feedparser
from requests_cache import CachedSession
from requests_cache import FileCache

from django.conf import settings

from ptf import model_data
from ptf.model_data import AbstractDict
from ptf.model_data import create_contributor

session = CachedSession(
    backend=FileCache(
        getattr(settings, "REQUESTS_CACHE_LOCATION", None) or "/tmp/ptf_requests_cache",
        decode_content=False,
    ),
    headers={
        "User-Agent": getattr(settings, "REQUESTS_USER_AGENT", None) or "Mathdoc/1.0.0",
        "From": getattr(settings, "REQUESTS_EMAIL", None) or "accueil@listes.mathdoc.fr",
    },
    expire_after=timedelta(days=2),
)


def get_arxiv_url(id):
    return f"http://export.arxiv.org/api/query?id_list={id}"


def get_arxiv_article(id):
    url = get_arxiv_url(id)

    # http = urllib3.PoolManager(cert_reqs="CERT_NONE")
    # urllib3.util.make_headers(keep_alive=None, accept_encoding="utf-8")
    headers = {"accept_encoding": "utf-8"}

    # For SSL Errors, use verify=False kwarg
    response = session.get(url=url, headers=headers)

    # parse the response using feedparser
    feed = feedparser.parse(response.text)
    if len(feed.entries) == 0:
        return None

    entry = feed.entries[0]

    article_data = model_data.create_articledata()

    # TITLE
    article_data.title_tex = entry.title

    # AUTHORS
    for author_entry in entry.authors:
        author = create_contributor()
        author["role"] = "author"
        author["string_name"] = author_entry.name

        article_data.contributors.append(author)

    # ABSTRACT
    xabstract: AbstractDict = {
        "tag": "abstract",
        "value_html": "",
        "value_tex": entry.summary,
        "value_xml": "",
        "lang": "en",
    }
    article_data.abstracts.append(xabstract)

    # PDF
    for link in entry.links:
        if link["type"] == "application/pdf":
            article_data.pdf_url = link["href"]

    return article_data
