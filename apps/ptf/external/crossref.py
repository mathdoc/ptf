from datetime import timedelta

from requests_cache import CachedSession
from requests_cache import FileCache

from django.conf import settings

from ptf.model_data import create_articledata

session = CachedSession(
    backend=FileCache(
        getattr(settings, "REQUESTS_CACHE_LOCATION", None) or "/tmp/ptf_requests_cache",
        decode_content=False,
    ),
    headers={
        "User-Agent": getattr(settings, "REQUESTS_USER_AGENT", None) or "Mathdoc/1.0.0",
        "From": getattr(settings, "REQUESTS_EMAIL", None) or "accueil@listes.mathdoc.fr",
    },
    expire_after=timedelta(days=30),
)


def get_crossref_journal_url(issn):
    issn = issn.replace("-", "")
    return f"https://api.crossref.org/journals/{issn}/works?select=DOI&sort=published&rows=1000"


def get_crossref_doi_url(doi):
    return f"https://api.crossref.org/works/{doi}"


def get_crossref_articles_in_journal(issn, what):
    url = get_crossref_journal_url(issn)
    response = session.get(url)
    response.raise_for_status()
    data = response.json()["message"]
    dois = [item["DOI"] for item in data["items"]]

    article_datas = []

    for doi in dois:
        xarticle = crossref_request(doi, type="article", what=what)
        if xarticle is not None:
            article_datas.append(xarticle)

    return article_datas


def crossref_request(doi, type="article", what=[]):
    """
    :param doi: DOI of the resource to request
    :param type: type of the expected returned value. Can be "article"
    :param what: list of metadata to request.
       ["published", "year", "primary_url"]
    :return: an ArticleData
    """

    url = get_crossref_doi_url(doi)
    response = session.get(url)
    response.raise_for_status()
    data = response.json()["message"]

    # FIXME : is doi used somewhere in create_articledata ?
    ptf_data = create_articledata(doi=doi)
    ptf_data.doi = doi
    ptf_data.pid = doi.replace("/", "_").replace(".", "_").replace("-", "_")

    if "year" and data.get("published") is None:
        return None

    if "year" and data.get("published") is not None:
        ptf_data.year = data["published"]["date-parts"][0][0]

    if "published" in what and data.get("published") is not None:
        date_parts = data["published"]["date-parts"][0]

        if len(date_parts) == 3:
            year = str(date_parts[0])
            month = str(date_parts[1]).zfill(2)
            day = str(date_parts[2]).zfill(2)
            date_str = f"{year}-{month}-{day}"
        elif len(date_parts) == 2:
            year = str(date_parts[0])
            month = str(date_parts[1]).zfill(2)
            date_str = f"{year}-{month}"
        else:
            date_str = str(date_parts[0])

        ptf_data.date_published_iso_8601_date_str = date_str

    if (
        "primary_url" in what
        and data.get("resource") is not None
        and data["resource"].get("primary") is not None
    ):
        ptf_data.url = data["resource"]["primary"].get("URL")

    return ptf_data
