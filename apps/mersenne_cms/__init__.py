import os

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
LOCALE_PATHS = ("%s/locale/" % BASE_DIR,)
