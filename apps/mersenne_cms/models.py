import json

from ckeditor_uploader.fields import RichTextUploadingField

from django.conf import settings
from django.contrib.auth.models import User
from django.core import serializers
from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _

from ptf import model_helpers

STATE_DISABLED = "disabled"
STATE_TEAM_ONLY = "team_only"
STATE_DRAFT = "draft"
STATE_PUBLISHED = "published"
PAGE_STATES = (
    (STATE_DISABLED, _("Disabled")),
    (STATE_TEAM_ONLY, _("Team only")),
    (STATE_DRAFT, _("Draft")),
    (STATE_PUBLISHED, _("Published")),
)

TYPE_ARTICLE = "article"
PAGE_TYPES = ((TYPE_ARTICLE, _("Article")),)

PAGE_POSITION_TOP = "top"
PAGE_POSITION_BOTTOM = "bottom"
PAGE_POSITIONS = ((PAGE_POSITION_TOP, _("Top")), (PAGE_POSITION_BOTTOM, _("Bottom")))

MERSENNE_ID_HOMEPAGE = "homepage"
MERSENNE_ID_MENU_ISSUES = "issues"
MERSENNE_ID_NEXT_ARTICLES = "next_articles"
MERSENNE_ID_VOLUMES = "volumes"
MERSENNE_ID_CONTACT = "contact"
MERSENNE_ID_GTU = "gtu"
MERSENNE_ID_LEGAL = "legal"
MERSENNE_ID_VIRTUAL_ISSUES = "virtual_issues"

NO_HOMEPAGE_IDS = [10, 35, 36, 100, 103]


class PageQuerySet(models.QuerySet):
    def delete(self, *args, **kwargs):
        for obj in self:
            if obj.mersenne_id:
                raise Exception(f"You cannot delete page {obj}")
        super().delete(*args, **kwargs)


class AdminPageManager(models.Manager):
    def get_by_natural_key(self, slug_value, site_id):
        if hasattr(self, "slug_en"):
            return self.get(slug_en=slug_value, site_id=site_id)
        elif hasattr(self, "slug_fr"):
            return self.get(slug_fr=slug_value, site_id=site_id)
        else:
            return self.get(slug=slug_value, site_id=site_id)

    def available_pages(self, request_user):
        states = [STATE_PUBLISHED]
        if Team.current.has_user(request_user) or request_user.is_superuser:
            states.append(STATE_TEAM_ONLY)
        return self.get_queryset().filter(state__in=states)


class PageManager(AdminPageManager):
    def get_queryset(self):
        if settings.SITE_ID == 2:
            return PageQuerySet(self.model, using=self._db).all()
        else:
            return PageQuerySet(self.model, using=self._db).filter(site_id=settings.SITE_ID)

    def home_page(self):
        return self.get_queryset().filter(home_page=True).first()

    def filter_all_slug(self, slug):
        q_quey = models.Q()
        for language_code, _x in settings.LANGUAGES:
            language_code = language_code.replace("-", "_")
            q_quey |= models.Q(**{f"slug_{language_code}": slug})

        return self.get_queryset().filter(q_quey)


class Page(models.Model):
    menu_title = models.CharField(_("Menu title"), max_length=255, default="", blank=False)  # noqa
    menu_fa = models.CharField(_("Menu fa"), max_length=255, default="", blank=True)  # noqa
    parent_page = models.ForeignKey(
        "Page",
        on_delete=models.SET_NULL,
        related_name="sub_pages",
        null=True,
        blank=True,
        verbose_name=_("Parent page"),
    )  # noqa
    slug = models.CharField(
        _("URL"),
        max_length=258,
        default="",
        blank=True,
        help_text=_('Exemple: "about", make sure you have a leading and trailing slash'),
    )  # noqa
    site_id = models.IntegerField()
    state = models.CharField(
        _("State"), default=STATE_DRAFT, choices=PAGE_STATES, blank=False, max_length=255
    )  # noqa
    _type = models.CharField(
        _("Type"), default=TYPE_ARTICLE, choices=PAGE_TYPES, blank=False, max_length=255
    )  # noqa
    mersenne_id = models.CharField(
        default="", blank=True, help_text="Identifiant static à usage interne", max_length=50
    )  # noqa
    editable = models.BooleanField(default=True)
    content = RichTextUploadingField(
        _("Content"), default="", blank=True, null=False, config_name="editor_pages"
    )  # noqa
    menu_order = models.IntegerField(_("Menu order"), default=0)
    _weight = models.IntegerField(default=0)
    position = models.CharField(
        _("Position"),
        default=PAGE_POSITION_TOP,
        choices=PAGE_POSITIONS,
        blank=False,
        max_length=255,
    )  # noqa

    sudo_objects = AdminPageManager()
    objects = PageManager()

    class Meta:
        verbose_name = _("Page")
        verbose_name_plural = _("Pages")
        ordering = ["_weight"]

    def __str__(self):
        return self.menu_title

    def save(self, weight_update=False, *args, **kwargs):
        if weight_update:
            return super().save(*args, **kwargs)

        if not self.pk and not self.site_id:
            self.site_id = settings.SITE_ID

        self._automatic_menu_order()

        for language_code, _x in settings.LANGUAGES:
            language_code = language_code.replace("-", "_")
            slug_attribute = f"slug_{language_code}"
            value = self.get_default_value(language_code)
            if value is not None and not getattr(self, slug_attribute, None):
                setattr(self, slug_attribute, slugify(value + f"_{language_code}"))

        self.slug = slugify(self.slug)

        return super().save(*args, **kwargs)

    def get_absolute_url(self):
        if not self.is_link():
            return "#"
        return reverse("page_detail", kwargs={"slug": self.slug})

    def _automatic_menu_order(self):
        queryset = self._meta.model.objects.filter(
            site_id=self.site_id, parent_page__isnull=True
        ).order_by("menu_order")

        if not self.menu_order:
            if self.parent_page is None:
                self.menu_order = queryset.count() + 1
            else:
                self.menu_order = self.parent_page.sub_pages.count() + 1

        if self.parent_page is None:
            order = self.menu_order
            sub_order = 0
            self._set_weight_as_parent()
            if self.id:
                for sub_page in self.sub_pages.all():
                    sub_page._set_weight_as_child(self)
                    sub_page.save(weight_update=True)
        else:
            order = 0
            sub_order = self.menu_order
            self._set_weight_as_child(self.parent_page)

        for page in queryset:
            if page.menu_order == order and page != self:
                order += 1
                page.menu_order = order
            page._set_weight_as_parent()
            page.save(weight_update=True)
            if page == self:
                continue
            for sub_page in page.sub_pages.order_by("menu_order").all():
                if (
                    page == self.parent_page
                    and sub_page.menu_order == sub_order
                    and (sub_page != self or sub_page.menu_order == 0)
                ):
                    sub_order += 1
                    sub_page.menu_order = sub_order
                sub_page._set_weight_as_child(page)
                sub_page.save(weight_update=True)

    def _set_weight_as_parent(self):
        self._weight = self.menu_order * 1000

    def _set_weight_as_child(self, parent_page):
        self._weight = self.menu_order + parent_page._weight

    def get_default_value(self, language_code):
        i18n_value = getattr(self, f"menu_title_{language_code}")
        if i18n_value:
            return i18n_value

        return getattr(
            self,
            "menu_title_{}".format(settings.MODELTRANSLATION_DEFAULT_LANGUAGE.replace("-", "_")),
        )

    def is_link(self):
        if self.mersenne_id == MERSENNE_ID_MENU_ISSUES:
            return False

        if not (self.content or self.mersenne_id):
            return False

        return True

    def full_menu_order(self):
        if self.parent_page:
            return f"{self.parent_page.menu_order} - {self.menu_order}"
        return self.menu_order

    full_menu_order.short_description = "Menu order"
    full_menu_order.admin_order_field = "_weight"

    def admin_css_class(self):
        if self.parent_page:
            return "child-page"
        return "parent-page"

    def natural_key(self):
        default_lang = model_helpers.get_site_default_language(self.site_id)
        if default_lang:
            if default_lang == "en":
                slug_key = self.slug_en if hasattr(self, "slug_en") else self.slug_fr
            else:
                slug_key = self.slug_fr if hasattr(self, "slug_fr") else self.slug_en
        else:
            if model_helpers.is_site_en_only(self.site_id):
                slug_key = self.slug_en if hasattr(self, "slug_en") else self.slug_fr
            else:
                slug_key = self.slug_fr if hasattr(self, "slug_fr") else self.slug_en

        return (slug_key, self.site_id)

    def is_published(self):
        return STATE_PUBLISHED == self.state


class DefaultTeamManager(models.Manager):
    def get_by_natural_key(self, site_id, name):
        return self.get(site_id=site_id, name=name)


class TeamManager(DefaultTeamManager):
    def get_queryset(self):
        return super().get_queryset().filter(site_id=settings.SITE_ID)

    def has_user(self, user):
        if user.is_anonymous:
            return False
        return self.get_queryset().filter(users=user).exists()


class Team(models.Model):
    name = models.CharField(max_length=250, default="")
    users = models.ManyToManyField(User, through="Member")
    site_id = models.IntegerField(unique=True)

    objects = DefaultTeamManager()
    current = TeamManager()

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.pk:
            self.site_id = settings.SITE_ID

        return super().save(*args, **kwargs)


class Member(models.Model):
    user = models.OneToOneField(
        User, unique=True, null=True, on_delete=models.CASCADE, verbose_name=_("User")
    )  # noqa
    team = models.ForeignKey(
        Team, verbose_name=_("Team"), blank=True, null=True, on_delete=models.CASCADE
    )  # noqa

    class Meta:
        verbose_name = _("Member")
        verbose_name_plural = _("Members")

    def __str__(self):
        return f"{self.user.username} - {self.team.name}"


class Service(models.Model):
    title = models.CharField(
        _("Title"), max_length=255, default="", blank=True, null=False
    )  # noqa
    description = RichTextUploadingField(_("Content"), default="", blank=True, null=False)  # noqa
    full_description = RichTextUploadingField(
        _("Full content"), default="", blank=True, null=False
    )  # noqa
    order = models.IntegerField(default=0, null=False, blank=False)
    slug = models.SlugField(default="", max_length=100, blank=False)

    class Meta:
        ordering = ["order"]

    def __str__(self):
        return self.title


class NewsManager(models.Manager):
    def get_by_natural_key(self, site_id):
        return self.get(site_id=site_id)

    def get_queryset(self):
        if settings.SITE_ID == 2:
            return models.QuerySet(self.model, using=self._db).all()
        else:
            return models.QuerySet(self.model, using=self._db).filter(site_id=settings.SITE_ID)


class News(models.Model):
    title_en = models.CharField(
        "Title (en)", max_length=255, default="", blank=True, null=False
    )  # noqa
    title_fr = models.CharField(
        "Titre (fr)", max_length=255, default="", blank=True, null=False
    )  # noqa
    content_en = RichTextUploadingField("Content (en)", default="", blank=True, null=False)  # noqa
    content_fr = RichTextUploadingField("Contenu (fr)", default="", blank=True, null=False)  # noqa
    # created = models.DateTimeField(auto_now_add=True) on souhaite récupérer created date lors de l'import
    # #https://docs.djangoproject.com/en/2.1/ref/models/fields/#django.db.models.DateField.auto_now_add
    created = models.DateTimeField(default=timezone.now)
    site_id = models.IntegerField(blank=True, null=True)

    objects = NewsManager()

    class Meta:
        verbose_name = "news"
        verbose_name_plural = "news"

    def __str__(self):
        return self.title_en


def get_pages_content(colid=None):
    if colid is None:
        pages = serializers.serialize(
            "json",
            Page.objects.all(),
            use_natural_foreign_keys=True,
            use_natural_primary_keys=True,
        )
    else:
        site_id = model_helpers.get_site_id(colid)
        json_string = serializers.serialize(
            "json",
            Page.objects.filter(site_id=site_id),
            use_natural_foreign_keys=True,
            use_natural_primary_keys=True,
        )
        data = json.loads(json_string)
        if model_helpers.is_site_en_only(site_id):
            for d in data:
                d["fields"] = {k: v for k, v in d["fields"].items() if not k.endswith("_fr")}
        elif model_helpers.is_site_fr_only(site_id):
            for d in data:
                d["fields"] = {k: v for k, v in d["fields"].items() if not k.endswith("_en")}
        pages = json.dumps(data)

    return pages


def get_news_content(colid=None):
    if colid is None:
        news = serializers.serialize(
            "json",
            News.objects.all(),
            use_natural_foreign_keys=True,
            use_natural_primary_keys=True,
        )
    else:
        site_id = model_helpers.get_site_id(colid)
        news = serializers.serialize(
            "json",
            News.objects.filter(site_id=site_id),
            use_natural_foreign_keys=True,
            use_natural_primary_keys=True,
        )

    return news


def import_pages(pages, colid=None):
    ids = [
        item["fields"]["mersenne_id"]
        for item in pages
        if "mersenne_id" in item["fields"] and item["fields"]["mersenne_id"] != ""
    ]

    site_id = settings.SITE_ID if colid is None else model_helpers.get_site_id(colid)

    if site_id not in NO_HOMEPAGE_IDS and MERSENNE_ID_HOMEPAGE not in ids:
        raise ValueError("The MERSENNE_ID_HOMEPAGE page is missing")

    if len(ids) != len(set(ids)):
        raise ValueError("Duplicates mersenne_id found")

    Page.sudo_objects.filter(site_id=site_id).delete()
    save_after = list()
    detail = ""
    count = 0
    pages_content = json.dumps(pages)
    for deserialize_obj in serializers.deserialize("json", pages_content):
        obj = deserialize_obj.object
        if obj.site_id != site_id:
            raise ValueError(
                f"Wrong website: page.site_id = {obj.site_id} and site_id = {site_id}"
            )

        if obj.parent_page:
            # To avoid saving an object before its parent
            save_after.append(obj)
        else:
            obj.pk = None
            obj.save()
            count += 1
            detail += f"{obj} saved with pk: {obj.pk}</br>"

    for obj in save_after:
        obj.pk = None
        obj.save()
        count += 1
        detail += f"{obj} saved with pk: {obj.pk}</br>"


def import_news(news, colid=None):
    site_id = settings.SITE_ID if colid is None else model_helpers.get_site_id(colid)

    News.objects.filter(site_id=site_id).delete()
    news_content = json.dumps(news)

    for deserialize_obj in serializers.deserialize("json", news_content):
        obj = deserialize_obj.object

        if obj.site_id != site_id:
            raise ValueError(
                f"Wrong website: page.site_id = {obj.site_id} and site_id = {site_id}"
            )

        obj.pk = None
        obj.save()
