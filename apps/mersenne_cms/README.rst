.. raw:: html

    <style> .red {color:red} </style>

.. role:: red

============
MERSENNE CMS
============


Mettre en place la plateforme
=============================

1 - Création de la base de données
----------------------------------

:red:`A ne faire que lors de la création complète de la plateforme`

::

    sudo su postgres
    psql
    CREATE database mersenne;CREATE user mersenne_user with password 'mersenne_pwd';GRANT ALL ON DATABASE mersenne to mersenne_user;
    \q
    exit

2 - Démarrage d'un nouveau projet
---------------------------------

Pour démarrer un projet mersenne il suffit d'utiliser le script ``start_mersenne_cms.py``.

Trois paramètres sont obligatoires à son exécution:

-i         Site id qui sera utilisé comme clé étrangère de toutes les pages et ressources.  
-n         Site name qui sera le nom du dossier contenant le projet.  
-c         Collection pid ou acronyme de la revue qui sera utilisé comme composante des identifiants ressouce.  

Optionel:

--verbose  Par defaut 0 n'affiche que les erreurs, à 1 affiche également les actions en cours.

**exemple**

>>> python path/to/ptf/mersenne/start_mersenne_site.py -i 2 -c AMPA -n nom_du_site_1 


3 - Connexion à la base de données
----------------------------------

Dans le fichier local_settings.py se trouvant dans le dossier``path/to/ptf/sites/site_name/site_name`` modifier les clés **NAME**, **USER** et **PASSWORD**, correspondant aux informations que vous avez défini à la création de la base de données.


4 - Virtualenv
--------------


``start_mersenne_cms`` crée automatiquement le virtualenv dans le dossier du projet.

Pour l'activer:

>>> source path/to/ptf/sites/site_name/env/bin/activate 


5 - Schéma de la base de données
--------------------------------

:red:`A ne faire que lors de la création complète de la plateforme`

>>> python source path/to/ptf/sites/site_name/manage.py migrate


6 - Lancement du serveur
------------------------

>>> python source path/to/ptf/sites/site_name/manage.py runserver


Register
========

Dans le dossier ``mersenne`` se trouve le fichier ``site_register.json``, ce fichier est mis à jour à chaque utilisation du script start_mersenne.
Si vous supprimez un projet manuellement vous devrez également mettre à jour ce fichier si vous desirez réutiliser **Site id, Site name ou Collection pid**.


Static page
===========

Quatres pages sont générés par défauts pour un site mersenne 'Acceuil', 'Notre revue', 'Articles à Venir', 'Tous les articles'.
Il est possible de les désactiver via le booléen ``ENABLE_STATIC_PAGES`` dans les settings.
