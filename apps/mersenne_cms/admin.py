from modeltranslation.admin import TranslationAdmin

from django.conf import settings
from django.contrib import admin
from django.contrib import messages
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import Group
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from mersenne_cms.forms import PageForm
from mersenne_cms.models import MERSENNE_ID_VIRTUAL_ISSUES
from mersenne_cms.models import Member
from mersenne_cms.models import News
from mersenne_cms.models import Page
from mersenne_cms.models import Service
from mersenne_cms.models import Team
from ptf.model_helpers import get_collection_id
from ptf.model_helpers import get_site_id
from ptf.site_register import SITE_REGISTER


class MersenneAdminSite(admin.AdminSite):
    site_header = "Centre Mersenne administration"
    site_title = "Centre Mersenne site admin"


class PageAdmin(TranslationAdmin):
    form = PageForm
    actions = None
    view_on_site = False

    super_admin_list_display = [
        "__str__",
        "state",
        "parent_page",
        "collection",
        "site_id",
        "full_menu_order",
        "_weight",
    ]

    list_display = [
        "__str__",
        "collection",
        "state",
        "parent_page",
        "slug",
    ]

    fields = [
        "site_id",
        "collection",
        "menu_title",
        "parent_page",
        "content",
        "state",
        "slug",
        "menu_order",
        "position",
    ]

    def save_model(self, request, obj, form, change):
        obj.site_id = form.cleaned_data["collection"]
        super().save_model(request, obj, form, change)

    def get_form(self, request, obj=None, **kwargs):
        if request.user.is_superuser and "mersenne_id" not in self.fields:
            self.fields.append("mersenne_id")
        form = super().get_form(request, obj, **kwargs)
        field = form.base_fields["site_id"]
        field.widget = field.hidden_widget()

        colids = [v["collection_pid"] for v in SITE_REGISTER.values()]
        choices = list((get_site_id(colid), colid) for colid in colids if colid)
        form.base_fields["collection"].choices = choices
        site_id = obj.site_id if obj else choices[0][0] if choices and choices[0] else 2
        form.base_fields["site_id"].initial = site_id
        form.base_fields["collection"].initial = site_id

        qs = Page.objects.filter(parent_page__isnull=True)
        if obj:
            qs = qs.exclude(pk=obj.pk)
        form.base_fields["parent_page"].queryset = qs.all()

        return form

    def get_queryset(self, request):
        queryset = super().get_queryset(request)

        # virtual issues must be saved only with the dedicated API to avoid parsing errors later
        vi = Page.objects.filter(mersenne_id=MERSENNE_ID_VIRTUAL_ISSUES).first()
        if vi:
            queryset = queryset.exclude(parent_page=vi)

        if request.user.is_superuser:
            return queryset
        return queryset.filter(site_id=settings.SITE_ID)

    def get_list_display(self, request):
        if request.user.is_superuser:
            return self.super_admin_list_display
        return self.list_display

    def _delete_view(self, request, object_id, extra_context):
        try:
            return super()._delete_view(request, object_id, extra_context)
        except Exception as e:
            opts = self.model._meta
            index_url = reverse(f"admin:{opts.app_label}_{opts.model_name}_changelist")
            self.message_user(
                request,
                e.message,
                messages.WARNING,
            )
            return HttpResponseRedirect(index_url)

    @admin.display(ordering="site_id")
    def collection(self, obj):
        return get_collection_id(obj.site_id)


class UserAdmin(BaseUserAdmin):
    list_display = ("username", "email")

    min_fielsets = (
        (None, {"fields": ("username", "password")}),
        (_("Personal info"), {"fields": ("first_name", "last_name", "email")}),
    )

    def get_fieldsets(self, request, obj=None):
        if obj and not request.user.is_superuser:
            return self.min_fielsets
        return super().get_fieldsets(request, obj)

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if request.user.is_superuser:
            return queryset

        return queryset.filter(member__team=request.user.team_set.first())

    def save_model(self, request, obj, form, change):
        obj.is_staff = True
        super().save_model(request, obj, form, change)
        if not obj.team_set.exists():
            if request.user.team_set.exists():
                Member.objects.create(team=request.user.team_set.get(), user=obj)
            else:
                Member.objects.create(team=Team.objects.get(site_id=settings.SITE_ID), user=obj)
        Group.objects.get(name="cms-writer").user_set.add(obj)


class ServiceAdmin(TranslationAdmin):
    list_display = [
        "__str__",
        "slug",
    ]

    fields = ["title", "description", "full_description", "slug", "order"]

    prepopulated_fields = (
        {"slug": ("title_fr",)} if settings.LANGUAGE_CODE == "fr" else {"slug": ("title_en",)}
    )


mersenne_admin = MersenneAdminSite(name="admin")
mersenne_admin.register(Page, PageAdmin)
mersenne_admin.register(Member)
mersenne_admin.register(Team)
# mersenne_admin.site.unregister(User)
mersenne_admin.register(User, UserAdmin)
mersenne_admin.register(Group)
mersenne_admin.register(Service, ServiceAdmin)
mersenne_admin.register(News)
mersenne_admin.register(Site)
