from ckeditor_uploader import urls as ckeditor_uploader_urls

from django.contrib.admin.views.decorators import staff_member_required
from django.urls import include
from django.urls import path
from django.urls import re_path
from django.views.generic import RedirectView

from mersenne_cms.views import ExportCMSView
from mersenne_cms.views import FileDeleteView
from mersenne_cms.views import HomeView
from mersenne_cms.views import ImportCMSView
from mersenne_cms.views import PageByMersenneIdDetailView
from mersenne_cms.views import PageDetailView

urlpatterns_staff = [
    re_path(r'^ckeditor/delete/', FileDeleteView.as_view(), name='ckeditor_delete'),
    re_path(r'^spage/(?P<mersenne_id>[\w-]+)/$', PageByMersenneIdDetailView.as_view(), name='page_by_mersenne_id'),  # noqa
]

urlpatterns_editors = [
    re_path(r'^page/(?P<slug>[\w-]+)/$', PageDetailView.as_view(), name='page_detail'),
    path('import_cms/', ImportCMSView.as_view(), name='import_cms'),
    path('export_cms/', ExportCMSView.as_view(), name='export_cms'),
]

urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('legal/', RedirectView.as_view(url='/page/legal-notice_en/'), name='legal'),
    path('ckeditor/', include(ckeditor_uploader_urls)),
]

for urlpattern in urlpatterns_staff:
    urlpattern.callback = staff_member_required(urlpattern.callback)
    urlpatterns.append(urlpattern)

urlpatterns += urlpatterns_editors
