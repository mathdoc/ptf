import json
import os

from ckeditor_uploader.utils import get_thumb_filename
from ckeditor_uploader.views import is_valid_image_extension

from django.conf import settings
from django.core.files.storage import default_storage
from django.http import Http404
from django.http import HttpResponse
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import DetailView
from django.views.generic import TemplateView
from django.views.generic import View

from mersenne_cms.models import MERSENNE_ID_HOMEPAGE
from mersenne_cms.models import MERSENNE_ID_MENU_ISSUES
from mersenne_cms.models import MERSENNE_ID_NEXT_ARTICLES
from mersenne_cms.models import News
from mersenne_cms.models import Page
from mersenne_cms.models import get_news_content
from mersenne_cms.models import get_pages_content
from mersenne_cms.models import import_news
from mersenne_cms.models import import_pages
from ptf.models import Article
from ptf.models import Collection
from ptf.models import Container
from ptf.views import ContainerView
from ptf.views import IssuesView
from ptf.views import SearchView as BaseSearchView


class HomeView(TemplateView):
    template_name = getattr(settings, "CMS_HOME_TEMPLATE", "mersenne_cms/page_detail.html")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        qs = Collection.objects.filter(pid=settings.COLLECTION_PID, sites__id=settings.SITE_ID)
        if qs.exists():
            context["journal"] = qs.first()

        context["page"] = Page.objects.get(mersenne_id=MERSENNE_ID_HOMEPAGE)
        context["news"] = News.objects.all().order_by("-created")[:3]
        if settings.DISPLAY_LATEST_ARTICLES:
            last_articles = (
                Article.objects.filter(sites__id=settings.SITE_ID)
                .filter(date_published__isnull=False)
                .exclude(classname="TranslatedArticle")
                .order_by("-date_published")
            )

            context["latest_articles"] = list()
            last_issue = None
            for article in last_articles:
                pid = article.my_container.pid[article.my_container.pid.find("_") :]
                if pid != "_0__0_0":
                    context["latest_articles"].append(article)
                    if not last_issue:
                        last_issue = article.my_container
                if len(context["latest_articles"]) > 10:
                    break

            context["issue"] = last_issue
        else:
            last_issue = None
            containers = Container.objects.filter(sites__id=settings.SITE_ID).order_by(
                "-year", "-vseries_int", "-volume_int", "-number_int"
            )
            if containers:
                last_issue = containers.first()
            context["issue"] = last_issue

        context["display_new_articles"] = settings.DISPLAY_LATEST_ARTICLES
        context["last_articles_msg"] = "Dernier Fascicule"
        return context


class NextVolumeView(ContainerView):
    def get(self, request, *args, **kwargs):
        if hasattr(settings, "ISSUE_TO_APPEAR_PID"):
            self.kwargs["pid"] = settings.ISSUE_TO_APPEAR_PID

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        if hasattr(settings, "ISSUE_TO_APPEAR_PID"):
            kwargs["pid"] = settings.ISSUE_TO_APPEAR_PID
        context = super().get_context_data(**kwargs)
        try:
            context["page"] = Page.objects.get(mersenne_id=MERSENNE_ID_NEXT_ARTICLES)
        except Page.DoesNotExist:
            pass

        return context


class PageDetailView(DetailView):
    template_name = "mersenne_cms/page_detail.html"
    model = Page
    init_slug = None

    def get_queryset(self):
        return Page.objects.available_pages(self.request.user)

    def get_object(self, queryset=None):
        if self.init_slug:
            self.kwargs["slug"] = self.init_slug

        if getattr(self, "object", None):
            return self.object

        if queryset is None:
            queryset = self.get_queryset()

        slug_queryset = Page.objects.filter_all_slug(self.kwargs.get(self.slug_url_kwarg))

        if queryset and slug_queryset:
            queryset = queryset & slug_queryset

        if queryset:
            obj = queryset.first()
        else:
            raise Http404()

        self.object = obj
        return obj

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["current_page"] = self.get_object()
        qs = Collection.objects.filter(pid=settings.COLLECTION_PID, sites__id=settings.SITE_ID)
        if qs.exists():
            context["journal"] = qs.first()
        return context

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.object.mersenne_id and settings.ENABLE_STATIC_PAGES:
            View = STATIC_PAGE_VIEWS.get(self.object.mersenne_id, None)
            if View:
                return View.as_view()(request)
            elif hasattr(self.object, "slug_en") and self.object.slug_en == "all-volumes":
                return IssuesView.as_view()(request, jid=settings.COLLECTION_PID)
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)


class PrettySearchView(BaseSearchView):
    template_name = "mersenne_cms/search-results.html"


STATIC_PAGE_VIEWS = {
    MERSENNE_ID_HOMEPAGE: HomeView,
    MERSENNE_ID_MENU_ISSUES: None,
    MERSENNE_ID_NEXT_ARTICLES: NextVolumeView,
}


class ExportCMSView(View):
    def get(self, request):
        pages = get_pages_content()
        news = get_news_content()

        # services = serializers.serialize(
        #     "json",
        #     Service.objects.all(),
        #     use_natural_foreign_keys=True,
        #     use_natural_primary_keys=True,
        # )
        return JsonResponse(
            {
                "pages": json.loads(pages),
                "news": json.loads(news),
                # "services": json.loads(services),
            }
        )


@method_decorator([csrf_exempt], name="dispatch")
class ImportCMSView(View):
    """
    Import Pages and optionally News in the journal web site
    """

    def check_lock(self):
        return hasattr(settings, "LOCK_FILE") and os.path.isfile(settings.LOCK_FILE)

    def put(self, request):
        if self.check_lock():
            return HttpResponse(
                "The journal website is under maintenance",
                status=503,
                headers={"Retry-After": 180},
            )

        try:
            # json_content = json.dumps(json.loads(request.body.decode("utf-8")).get("pages"))
            json_data = json.loads(request.body.decode("utf-8"))

            # pages_content = json.dumps(json_data.get('pages'))
            pages = json_data.get("pages")
            import_pages(pages)

            if "news" in json_data:
                news = json_data.get("news")
                import_news(news)

        except ValueError:
            return HttpResponse("Json is not valid", status=400)

        return HttpResponse("OK", status=201)
        # return JsonResponse({"count": count, "detail": detail}, status=201)


class PageByMersenneIdDetailView(PageDetailView):
    def get_object(self, queryset=None):
        if not queryset:
            queryset = self.get_queryset()
        queryset = queryset.filter(mersenne_id=self.kwargs.get("mersenne_id", "nomersenneid"))
        try:
            obj = queryset.get()
        except queryset.model.DoesNotExist:
            raise Http404(
                ("No %(verbose_name)s found matching the query")
                % {"verbose_name": queryset.model._meta.verbose_name}
            )
        return obj


class FileDeleteView(View):
    http_method_names = ["delete"]

    def delete(self, request, **kwargs):
        file_to_de_deleted = request.GET["path"]

        if is_valid_image_extension(file_to_de_deleted):
            thumbnail_filename_path = get_thumb_filename(file_to_de_deleted)
            default_storage.delete(thumbnail_filename_path)

        default_storage.delete(file_to_de_deleted)
        return JsonResponse({"success": 1})
