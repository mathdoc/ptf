from modeltranslation.translator import TranslationOptions
from modeltranslation.translator import translator

from mersenne_cms.models import Page
from mersenne_cms.models import Service


class PageTranslationOptions(TranslationOptions):
    fields = ("menu_title", "slug", "content")


class ServiceTranslationOptions(TranslationOptions):
    fields = (
        "title",
        "description",
        "full_description",
    )


translator.register(Page, PageTranslationOptions)
translator.register(Service, ServiceTranslationOptions)
