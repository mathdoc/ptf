# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-08-02 15:18
from __future__ import unicode_literals

import ckeditor_uploader.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mersenne_cms', '0003_auto_20170717_1514'),
    ]

    operations = [
        # migrations.RemoveField(
        #     model_name='page',
        #     name='published_content',
        # ),
        # migrations.RemoveField(
        #     model_name='page',
        #     name='published_content_en_gb',
        # ),
        # migrations.RemoveField(
        #     model_name='page',
        #     name='published_content_fr_fr',
        # ),
        migrations.AddField(
            model_name='page',
            name='content',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, default='', verbose_name='Content'),
        ),
        migrations.AddField(
            model_name='page',
            name='content_en_gb',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, default='', null=True, verbose_name='Content'),
        ),
        migrations.AddField(
            model_name='page',
            name='content_fr_fr',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, default='', null=True, verbose_name='Content'),
        ),
    ]
