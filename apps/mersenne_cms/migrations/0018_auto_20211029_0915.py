# Generated by Django 2.2.24 on 2021-10-29 09:15

from django.db import migrations
import django.db.models.manager


class Migration(migrations.Migration):

    dependencies = [
        ('mersenne_cms', '0017_auto_20200513_0910'),
    ]

    operations = [
        migrations.AlterModelManagers(
            name='page',
            managers=[
                ('sudo_objects', django.db.models.manager.Manager()),
            ],
        ),
    ]
