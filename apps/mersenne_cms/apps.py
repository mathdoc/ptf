from django.apps import AppConfig


class MersenneCmsConfig(AppConfig):
    name = "mersenne_cms"
