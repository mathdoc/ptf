LANGUAGES = (("fr", "French"), ("en", "English"))


INSTALLED_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.sites",
    "django_extensions",
    "modeltranslation.apps.ModeltranslationConfig",
    "mersenne_cms",
    "ptf",
    "oai",
    "ckeditor",
    "ckeditor_uploader",
    "upload",
    "django.contrib.admin",
]

# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

CKEDITOR_IMAGE_BACKEND = "ckeditor_uploader.backends.PillowBackend"
CKEDITOR_UPLOAD_PATH = "uploads/"
CKEDITOR_ALLOW_NONIMAGE_FILES = True
CKEDITOR_CONFIGS = {
    "default": {
        "skin": "moono-lisa",
        "toolbar_Basic": [["Source", "-", "Bold", "Italic"]],
        "toolbar_Full": [
            [
                "Styles",
                "Format",
                "Bold",
                "Italic",
                "Underline",
                "Strike",
                "SpellChecker",
                "Undo",
                "Redo",
            ],
            ["Link", "Unlink", "Anchor"],
            ["Image", "Table", "HorizontalRule", "BulletedList", "NumberedList"],
            ["TextColor", "BGColor"],
            ["SpecialChar"],
            ["Source"],
        ],
        "toolbar": "Full",
        "height": 291,
        "width": 835,
        "filebrowserWindowWidth": 940,
        "filebrowserWindowHeight": 725,
        "versionCheck": False,
    },
    "editor_pages": {
        "toolbar": [
            {"name": "document", "items": ["Source", "-", "Preview", "-", "NewPage"]},
            {
                "name": "clipboard",
                "items": [
                    "Cut",
                    "Copy",
                    "Paste",
                    "PasteText",
                    "PasteFromWord",
                    "-",
                    "Undo",
                    "Redo",
                ],
            },
            {"name": "editing", "items": ["Find", "Replace", "-", "SelectAll", "-"]},
            {"name": "styles", "items": ["Styles", "Format", "Font", "FontSize"]},
            {"name": "colors", "items": ["TextColor", "BGColor"]},
            {"name": "mathjax", "items": ["Mathjax"]},
            "/",
            {
                "name": "basicstyles",
                "items": [
                    "Bold",
                    "Italic",
                    "Underline",
                    "Strike",
                    "Subscript",
                    "Superscript",
                    "-",
                    "CopyFormatting",
                    "RemoveFormat",
                ],
            },
            {
                "name": "paragraph",
                "items": [
                    "NumberedList",
                    "BulletedList",
                    "-",
                    "Outdent",
                    "Indent",
                    "-",
                    "CreateDiv",
                    "-",
                    "JustifyLeft",
                    "JustifyCenter",
                    "JustifyRight",
                    "JustifyBlock",
                    "-",
                    "BidiLtr",
                    "BidiRtl",
                ],
            },
            {"name": "links", "items": ["Link", "Unlink", "Anchor"]},
            {
                "name": "insert",
                "items": ["Image", "Flash", "Table", "HorizontalRule", "SpecialChar"],
            },
            {"name": "tools", "items": ["Maximize", "ShowBlocks", "CreateDiv"]},
        ],
        "height": 500,
        "width": 1200,
        "removePlugins": ["exportpdf"],
        "extraAllowedContent": "div(*);span(*);em(*);figure(*);p(*);hr(*);sub(*);sup(*)",
        "extraPlugins": ["div", "mathjax"],
        "mathJaxLib": "https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/latest.js?config=TeX-AMS_HTML",
        "mathJaxClass": "mathjax-formula",
        "versionCheck": False,
    },
}


MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    # 'ptf.middleware.ForceDefaultLanguageMiddleware',
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "django.contrib.sites.middleware.CurrentSiteMiddleware",
]


MODELTRANSLATION_TRANSLATION_FILES = ("mersenne_cms.translations",)
MODELTRANSLATION_DEFAULT_LANGUAGE = "en"
