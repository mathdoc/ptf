$(document).ready(function () {
    /////////////////////////////////////////////////////////////
    // Search Input and Search Clear Behaviour
    /////////////////////////////////////////////////////////////

    // At load time, if search input is not empty, display clear search button
    $(".searchinput").each(function () {
        if ($(this).val().length) {
            $(this).next().children().first().removeClass("hidden");
        }
    });

    // Display clear search button if search input is not empty
    $(".searchinput").keyup(function () {
        if ($(this).val().length) {
            $(this).next().children().first().removeClass("hidden");
        } else {
            $(this).next().children().first().addClass("hidden");
        }
    });

    // Clear search input
    $(".searchclear").on("click", function () {
        $(this).parent().prev().val("").focus();
        $(this).addClass("hidden");
    });

    /////////////////////////////////////////////////////////////
    // Search Category Selection Behaviour
    /////////////////////////////////////////////////////////////
    $("#search-bar").on("click", ".dropdown-menu a", function () {
        // New value
        var selection = $(this).html();
        hidden_input = $(this).closest(".input-group").find("input[type='hidden']").first();

        // Change the button text
        $(this)
            .parent()
            .parent()
            .prev()
            .html(selection + ' <span class="caret"></span>');

        if (selection == "Tout" || selection == "All") {
            value = "all";
        } else if (selection == "Titre" || selection == "Title") {
            value = "title";
        } else if (selection == "Auteur" || selection == "Author") {
            value = "author";
        } else if (selection == "Date") {
            value = "date";
        } else if (selection == "Résumé" || selection == "Abstract") {
            value = "abstract";
        } else if (selection == "Plein texte" || selection == "Full text") {
            value = "body";
        } else if (selection == "Bibliographie" || selection == "References") {
            value = "references";
        }

        if (hidden_input.attr("id") != "qt0") {
            var input_group = $(this).parent().parent().parent().parent().parent();

            // Display 2 inputs if date is selected, else 1
            if (value == "date") {
                input_group.children().slice(0, 4).removeClass("hidden");
                input_group.children().slice(4, 6).addClass("hidden");
            } else {
                input_group.children().slice(0, 4).addClass("hidden");
                input_group.children().slice(4, 6).removeClass("hidden");
            }
        }

        // Update the hidden input
        hidden_input.val(value);
    });

    /////////////////////////////////////////////////////////////
    // Add/Remove line to the search form
    /////////////////////////////////////////////////////////////

    // Add a new line to the search form
    $("#add-search-field").click(function () {
        var i = $("#search-bar form .input-group").length;

        // A prototype was added by the Django template
        // Clone it to create the new line
        input_template = $("#template-search-form").html();
        input_template = input_template.replace(/__INPUT_NUMBER__/g, i);

        var form_root = $("#main-search");
        form_root.append(input_template);
        form_root.find(".searchinput").last().focus();
    });

    $("#search-bar").on("click", ".remove-search-field", function () {
        var div_to_remove = $(this).parent().parent();
        var form_root = div_to_remove.parent();

        div_to_remove.remove();
        form_root.find(".searchinput").last().focus();
    });
});
