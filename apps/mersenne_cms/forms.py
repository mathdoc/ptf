from django import forms
from django.conf import settings
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from mersenne_cms.models import Page


class PageForm(forms.ModelForm):
    collection = forms.ChoiceField(label="Collection", required=True)

    class Meta:
        model = Page
        fields = [
            "menu_title",
            "collection",
            "parent_page",
            "content",
            "state",
            "slug",
            "menu_order",
            "position",
            # 'mersenne_id'
        ]

    def clean(self):
        cleaned_data = super().clean()

        for language_code, _x in settings.LANGUAGES:
            slug_attr = "slug_{}".format(language_code.replace("-", "_"))

            if slug_attr not in cleaned_data:
                continue

            slug = self.cleaned_data.get(slug_attr)
            page = Page.objects.filter_all_slug(slug).first()
            if page and page.id != self.instance.id:
                raise ValidationError(
                    _(f'Invalid slug: "{slug}" already in use'),
                    params={"slug": slug},
                )

    def clean_parent_page(self):
        parent_page = self.cleaned_data["parent_page"]
        if parent_page and self.instance.sub_pages.exists():
            raise ValidationError(_("You cannot add a parent page to pages with subpages"))
        return parent_page


class PageAdminForm(PageForm):
    class Meta:
        model = Page
        fields = [
            "menu_title",
            "parent_page",
            "content",
            "state",
            "slug",
            "menu_order",
            "position",
            "mersenne_id",
        ]
