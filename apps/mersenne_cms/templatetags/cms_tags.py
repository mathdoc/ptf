from django import template
from django.urls import reverse

from mersenne_cms.models import MERSENNE_ID_CONTACT
from mersenne_cms.models import MERSENNE_ID_GTU
from mersenne_cms.models import MERSENNE_ID_HOMEPAGE
from mersenne_cms.models import MERSENNE_ID_LEGAL
from mersenne_cms.models import Page

register = template.Library()


OUTSITE_MAIN_MENU_PAGES = [
    MERSENNE_ID_HOMEPAGE,
    MERSENNE_ID_GTU,
    MERSENNE_ID_LEGAL,
    MERSENNE_ID_CONTACT,
]

MAIN_MENU_PAGES_TO_EXCLUDE = [
    MERSENNE_ID_GTU,
    MERSENNE_ID_LEGAL,
    MERSENNE_ID_CONTACT,
]


@register.simple_tag
def get_all_pages(request, position, with_home=False):
    path = request.path

    exclude_ids = MAIN_MENU_PAGES_TO_EXCLUDE if with_home else OUTSITE_MAIN_MENU_PAGES

    page_list = list()
    pages = (
        Page.objects.available_pages(request.user)
        .filter(parent_page__isnull=True, position=position)
        .exclude(mersenne_id__in=exclude_ids)
        .all()
    )
    for page in pages.all():
        subpages = list(page.sub_pages.available_pages(request.user).all())

        is_active = False
        url = reverse("page_detail", kwargs={"slug": page.slug})
        if path == url:
            is_active = True

        if subpages and not is_active:
            for subpage in subpages:
                url = reverse("page_detail", kwargs={"slug": subpage.slug})
                if path == url:
                    is_active = True

        page_list.append([page, subpages, is_active])

    return page_list
