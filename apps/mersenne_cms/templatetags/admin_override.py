from django.contrib.admin.templatetags.admin_list import ResultList
from django.contrib.admin.templatetags.admin_list import items_for_result
from django.contrib.admin.templatetags.admin_list import result_headers
from django.contrib.admin.templatetags.admin_list import result_hidden_fields
from django.template import Library

register = Library()


def results(cl):
    if cl.formset:
        for res, form in zip(cl.result_list, cl.formset.forms):
            yield ResultList(form, items_for_result(cl, res, form))
    else:
        for res in cl.result_list:
            yield {
                "class": res.admin_css_class,
                "items": ResultList(None, items_for_result(cl, res, None)),
            }


@register.inclusion_tag("admin/change_list_results_with_class.html")
def result_list_with_class(cl):
    """
    Displays the headers and data list together
    """
    headers = list(result_headers(cl))
    num_sorted_fields = 0
    for header in headers:
        if header["sortable"] and header["sorted"]:
            num_sorted_fields += 1
    return {
        "cl": cl,
        "result_hidden_fields": list(result_hidden_fields(cl)),
        "result_headers": headers,
        "num_sorted_fields": num_sorted_fields,
        "results": list(results(cl)),
    }
