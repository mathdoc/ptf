"""
MESH filters.
This offer a generic implementation of filters for any dataset.
This was made expecting a queryset but any iterable dataset can be filtered.

The filters are generated via a specific config (cf FilterSet & Filter docs).

This should be used with the `filter_set.html` template and it requires a bit of
Javascript located in `mesh.js` (this can be externalized for a full component behavior).

Usage:
    -   Create a FilterSet instance with as many Filter instance as required.
    -   Initialize the filters by calling `filter_set.init_filters(dataset, request.GET)`
    -   Filter your data by calling `filtered_data = filter_set.filter(dataset)`
"""
from collections.abc import Callable
from collections.abc import Collection
from collections.abc import Iterable
from dataclasses import dataclass
from dataclasses import field
from typing import Any
from typing import Literal

from django.http import QueryDict

FILTER_QUERY_PARAMETER_PREFIX = "_filter_"


def is_collection(value: Any) -> bool:
    """
    Whether the given object is a "collection": a Collection other than a string.

    What we actually want to check is whether the value is a single value or
    values such as a list, set, dict, QuerySet, ...
    TODO: this might fail for lazy strings ?
    """
    return isinstance(value, Collection) and not isinstance(value, str)


## Type aliases
IdValue = int | str
# Available filter types. Any other type will be handled as the default "string" type.
FilterType = Literal["string", "int", "model"]


@dataclass
class FilterValue:
    """
    Interface for a filter value.
    """

    # Actual filter value ~~ ID
    value: IdValue
    # Name to display for the filter value
    name: str
    # Whether the filter value is active (ie. selected).
    active: bool = field(default=False)


@dataclass
class FieldGetter:
    """
    Getter object.

    Data & logic on how to retrieve a value from an item.
    Only one of `attr` or `func` should be given.
    """

    # The attribute name to look for in the given item.
    attr: str = field(default="")
    # Whether the attribute is a callable. TODO: Automate this ? By checking if callable
    callable: bool = field(default=False)
    # A function with one parameter (the item) to be called to get the value.
    func: Callable | None = field(default=None)

    def get_value(self, item: Any) -> Any:
        """
        Get the given item's value.
        """
        if self.attr:
            value = getattr(item, self.attr)
            if self.callable:
                value = value()
        elif self.func:
            value = self.func(item)
        else:
            value = None
        return value


@dataclass
class Filter:
    """
    Interface for a filter.

    About the retrieval of the filter value & name for an item:
    -   Default:            the filter ID is the direct attribute of the item
                            (not callable) that holds the value. Same value is used for
                            the value associated name.
    -   `getter`:           Use this field to indicate a different behavior to retrieve
                            the value. This is used for both the value and its
                            associated name unless one of the below is defined.
                            Cf. FieldGetter for how to define a getter.
    -   `value_getter`:     Override the `getter`, if any, for retrieving the value
                            only.
    -   `name_getter`:      Override the `getter`, if any, for retrieving the value's
                            associated name only.

    The item value and its associated name can be a collection
    (ex: `list` or `QuerySet`).

    Specific type:
    -   "model":            When the filter is a Django model. The item value should be
                            a model instance. The default associated name is the casting
                            of the instance to string (`__str__` method).
    """

    # Filter ID
    id: str
    # Filter name for display
    name: str
    # Common getter function for both value and name
    getter: FieldGetter | None = field(default=None)
    # Getter function only for the value
    value_getter: FieldGetter | None = field(default=None)
    # Getter function only for the name
    name_getter: FieldGetter | None = field(default=None)
    # Filter type - Impacts the handling of an item's value
    type: FilterType = field(default="string")
    # The available values of the Filter
    values: list[FilterValue] = field(default_factory=list)

    @property
    def value_field(self) -> FieldGetter:
        """
        Return the config indicating how to retrieve an item value for this filter.
        """
        return self.value_getter or self.getter or FieldGetter(attr=self.id)

    @property
    def name_field(self) -> FieldGetter:
        """
        Return the config indicating how to retrieve an item value associated name.
        """
        return self.name_getter or self.getter or FieldGetter(attr=self.id)

    def set_active_value(self, value_list: list[IdValue] | list[str]) -> None:
        """
        Set the active value on the filter.

        Casts to correct type according to the filter type.
        The matching is made on the filter value (ID).
        """
        for v in value_list:
            try:
                if self.type in ["model", "int"]:
                    value = int(v)
                else:
                    value = v
            except Exception:
                continue

            # Flag the matching value as active
            match_value = next((v for v in self.values if v.value == value), None)
            if match_value:
                match_value.active = True

    @property
    def active(self) -> bool:
        """
        Whether the filter is active (= at least 1 active value).
        """
        return next((True for v in self.values if v.active), False)

    @staticmethod
    def get_item_value(filter: "Filter", item: Any) -> list[IdValue] | IdValue | None:
        """
        Get the item value corresponding to the given filter config.
        """
        item_value = filter.value_field.get_value(item)

        # Handle "model" type
        if filter.type == "model":
            if is_collection(item_value):
                return [i.pk for i in item_value]
            return item_value.pk

        return item_value

    @staticmethod
    def get_item_name(filter: "Filter", item: Any) -> list[str] | str:
        """
        Get the item value's name corresponding to the given filter config.
        """
        item_name = filter.name_field.get_value(item)

        # Handle collections.
        if is_collection(item_name):
            return [str(n) for n in item_name]

        return str(item_name)

    @property
    def active_values(self) -> list[IdValue]:
        """
        The list of active values (as `IdValue`, ie. `str` or `int`)
        """
        return [v.value for v in self.values if v.active]

    @staticmethod
    def filter(filter: "Filter", item: Any) -> bool:
        """
        Whether the given item matches the given filter values.
        """
        try:
            # Get the item value
            item_value = Filter.get_item_value(filter, item)

            # Check that the value is within the applied values
            # Case of a colletion
            if is_collection(item_value):
                return any(v in filter.active_values for v in item_value)  # type:ignore

            # Base case
            return item_value in filter.active_values
        except Exception:
            return False

    def value_exists(self, value: IdValue) -> bool:
        """
        Whether the given value is already present.
        """
        return next((True for v in self.values if v.value == value), False)

    def _add_single_value(self, item_value: Any, item_name: str) -> None:
        """
        Add a single value to the filter's values if not already present.
        """
        if item_value not in [None, 0, ""] and not self.value_exists(item_value):
            self.values.append(FilterValue(value=item_value, name=item_name))

    def add_value(self, item_value: Any, item_name: Any) -> None:
        """
        Add the given value and associated name to the filter values.

        The value and the name can be collections.
        """
        # Vectorize if the value is a collection
        if is_collection(item_value):
            # Check that the item_name is also an iterable, otherwise cast to `list`
            if not is_collection(item_name):
                item_name = [item_name]

            # If both iterable are not on same length, we will miss some values.
            # Nothing to do here, just fix the getting of value & name so that
            # their length match.
            for v, n in zip(item_value, item_name):
                self._add_single_value(v, n)

            return

        self._add_single_value(item_value, item_name)

    def get_query_param(self) -> str:
        """
        Returns the query param corresponding to this filter.
        """
        return f"{FILTER_QUERY_PARAMETER_PREFIX}{self.id}"

    def sort_values(self) -> None:
        """
        Sort alphabetically the filter values and place active values first.
        """
        self.values.sort(key=lambda v: (not v.active, v.name))


@dataclass
class FilterSet:
    """
    FilterSet interface - Set of filters.

    Filters together act as an AND condition.
    Multiple values of 1 filter act as an OR condition.

    Using dataclasses enables exporting the filters config easily with the asdict
    built-in method.

    WARNING: Watch performance. We use standard Python for loops everywhere to populate
    and filter the data. This is probably OK if the involved datasets are < 1K entries.
    If this becomes problematic, we might want to translate it to much faster JavaScript
    (but quite a bummer to implement without a framework).
    """

    # FieldSet name for display
    id: str
    name: str = field(default="")
    filters: list[Filter] = field(default_factory=list)

    def init_filters(self, query_dict: QueryDict, *args: Iterable):
        """
        Initialize the filter set with the given dataset and query dictionnary:
            -   populate all filters from the given data
            -   get the active filters from the query dict

        Params:
            -   `query_dict`    The query dictionnary
            -   `args`          An arbitrary number of datasets used to populate
                                the data.
        """
        for dataset in args:
            self.populate_filters(dataset)
        self.parse_query_filters(query_dict)
        self.sort_filters()

    def get_filter(self, filter_id: str, prefix: str = "") -> Filter | None:
        """
        Retrieve the filter corresponding to the given filter ID.
        """
        if prefix:
            if not filter_id.startswith(prefix):
                return None
            filter_id = filter_id.removeprefix(prefix)
        return next((f for f in self.filters if f.id == filter_id), None)

    def populate_filters(self, dataset: Iterable) -> None:
        """
        Populate the filter values from the given dataset.

        The item value can be an iterable. In that case we create one FilterValue
        for each item value.
        """
        for filter in self.filters:
            for item in dataset:
                try:
                    filter.add_value(
                        Filter.get_item_value(filter, item), Filter.get_item_name(filter, item)
                    )

                except Exception:
                    continue

    def parse_query_filters(self, query_dict: QueryDict) -> None:
        """
        Parse and fill the active filters from the given query dictionnary.
        """
        for key, values_list in query_dict.lists():
            filter = self.get_filter(key, prefix=FILTER_QUERY_PARAMETER_PREFIX)
            if not filter:
                continue
            filter.set_active_value(values_list)

    def sort_filters(self) -> None:
        """
        Sort the filters by name and sort each filter's values.
        """
        self.filters.sort(key=lambda f: f.name)
        for filter in self.filters:
            filter.sort_values()

    @property
    def applied_filters(self) -> list[Filter]:
        """
        The list of applied/active filters.
        """
        return [f for f in self.filters if f.active]

    @property
    def active(self) -> bool:
        """
        Whether the FilterSet is active (= at least 1 applied/active filter).
        """
        return len(self.applied_filters) > 0

    def filter(self, dataset: Iterable) -> Iterable:
        """
        Filter the given dataset with the current active filters.

        TODO: Do it together with the populate_filters method ? To end with
        only 1 iteration over the full dataset.
        OR cache the result tuple (item_value, item_value_name) for each filter
        directly in the dataset to avoid "computing" the values multiple times.
        """
        filtered_dataset = []
        applied_filters = self.applied_filters
        if not applied_filters:
            return [d for d in dataset]

        for item in dataset:
            item_filtered = True

            for filter in applied_filters:
                if not Filter.filter(filter, item):
                    item_filtered = False
                    break

            if item_filtered:
                filtered_dataset.append(item)

        return filtered_dataset
