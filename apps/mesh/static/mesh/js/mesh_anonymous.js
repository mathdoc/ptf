/**
 * Observer to toggle the class `show` on elements intersecting the viewport
 * for the first time.
 * Used to animate on scroll or on initial display.
 */
const observer = new IntersectionObserver((entries) => {
    entries.forEach(entry => {
        if (entry.isIntersecting) {
            entry.target.classList.add("show")
        }
    })
})

let targetSelectors = [".allauth-form .form-input"]
targetSelectors.forEach(selector => {
    document.querySelectorAll(selector).forEach(el => observer.observe(el))
})
