var clickLikeKeys = [" ", "Spacebar", "Enter"]
/**
 * Array of query parameters to remove from the URL on init.
 * They should never persist in the URL because they correspond to backend one-time actions.
 */
var paramsToDiscard = ["_auth_token", "_role_switched"]

/**
 * Clean the given URL of unnecessary elements and replace the current URL with the result.
 * @param {string} url_string      The url to clean
 */
function cleanAndReplaceUrl(url_string) {
    let url = new URL(url_string)
    let searchParams = new URLSearchParams(url.searchParams)
    for (let param of paramsToDiscard) {
        searchParams.delete(param)
    }
    url.search = searchParams.toString()
    history.replaceState(null, null, url.toString())
}

/**
 * Return whether the given UIEvent should be consider a "click-like" action.
 * For example: space and enter keydown are considered click-like actions.
 *
 * @param {UIEvent} event
 * @returns {boolean}
 */
function isClickLikeAction(event) {
    if (event.type === "click") {
        return true
    }
    if (event.type === "keydown") {
        return clickLikeKeys.includes(event.key)
    }
    return false
}

/**
 * Adds the necessary event listeners to execute the provided callback when a click-like action is
 * performed on the element.
 *
 * @param {HTMLElement} element     The element to add the event listener to.
 * @param {*} callback              The callback to execute. It must takes 2 arguments: the element
 *                                  and the event.
 * @returns
 */
function addClickEventListener(element, callback) {
    if (!element) {
        return
    }
    element.addEventListener("click", (event) => callback(element, event))
    element.addEventListener("keydown", (event) => {
        if (!isClickLikeAction(event)) {
            return
        }
        callback(element, event)
    })
}


/**
 * Toggles mesh sidebar.
 */
function toggleSidebar(_, event) {
    const sidebar = document.getElementById("mesh-sidebar")
    if (!sidebar) {
        console.warning("No sidebar to toggle")
        return
    }
    sidebar.classList.toggle("active")

    const sidebarBackground = document.getElementById("mesh-sidebar-background")
    sidebarBackground.classList.toggle("active")
}


/**
 * Reset our custom file input.
 * @param {UIEvent} event
 */
function fileInputClear(button, event) {
    event.stopImmediatePropagation()
    let inputWidget = event.target?.closest(".form-input-widget")
    let input = inputWidget?.querySelector("input[type='file']")
    if (input) {
        // Reset hidden input value
        input.value = null

        // Reset displayed text
        let fileNameSpan = inputWidget.querySelector(".form-input-file-name")
        if (fileNameSpan) {
            fileNameSpan.innerHTML = input.dataset.initial ?? ""
            fileNameSpan.closest("label").setAttribute("title", "")
        }
        button.classList.add("display-none")
    }
}

/**
 * Init the given jQuery element as a DataTable.
 * Fetches additional config from the element ".table-config"
 *
 * @param {*} jElement  A jQuery object to be muted into a dataTable (ex: $("#my-table"))
 */
function initDataTable(jElement) {
    let dataTableConfig = {}
    let table = jElement[0]
    if (!table) {
        return
    }
    let tableConfig = table.closest(".mesh-table-wrapper")?.querySelector(".table-config")
    if (tableConfig) {
        let languageConfig = tableConfig.querySelector(".table-language-config")?.dataset
        if (languageConfig) {
            dataTableConfig["language"] = languageConfig
        }
    }
    jElement.DataTable(dataTableConfig)
}


/**
 * Toggle the given filter.
 * Adapt the current query string with the filter value and redirect to the new URL.
 *
 * @param {HTMLElement} filter
 */
function toggleFilter(filter) {
    let param = filter.dataset.filterParam
    let value = filter.dataset.filterValue
    if (!param) {
        console.log(`Error: filter without data-filter-param property`)
        return
    }
    if (!value) {
        console.log(`Error: filter ${param} without data-filter-value property`)
        return
    }

    let url = new URL(window.location.href)
    let params = new URLSearchParams(url.search)
    if (params.has(param, value)) {
        params.delete(param, value)
    }
    else {
        params.append(param, value)
    }
    params.set("filters_opened", "true")
    url.search = `?${params.toString()}`
    window.location.replace(url.toString())
}

/**
 * Remove all query parameters linked to the filters in the given filterset.
 *
 * @param {HTMLElement} element     A filtersetreset element
 * @param {Event} event             A click-like event
 */
function resetFilters(element, event) {
    event.stopImmediatePropagation()
    const filterSet = element.closest(".mesh-filter-set")
    if (!filterSet) {
        return
    }
    const filters = Array.from(filterSet.getElementsByClassName("mesh-filter"))
    if (!filters) {
        return
    }

    let url = new URL(window.location.href)
    let params = new URLSearchParams(url.search)
    let change = false
    for (const filter of filters) {
        let param = filter.dataset.filterParam
        if (param && params.has(param)) {
            change = true
            params.delete(param)
        }
    }

    if (!change) {
        return
    }
    params.set("filters_opened", "true")

    url.search = `?${params.toString()}`
    window.location.replace(url.toString())
}

/**
 * Toggle a collapsible element.
 * It toggles the "show" class to display or hide the collapsible element.
 *
 * @param {HTMLElement} element     The toggle element
 * @param {Event} _
 */
function toggleCollapse(element, _) {
    let toToggle = document.getElementById(element.dataset.collapseToggle)
    if (!toToggle) {
        return
    }
    toToggle.classList.toggle("show")

    let expanded = element.getAttribute("aria-expanded")
    if (expanded == "true") {
        element.setAttribute("aria-expanded", "false")
    }
    else {
        element.setAttribute("aria-expanded", "true")
    }
}

function main() {
    cleanAndReplaceUrl(window.location.href)

    // File form field
    Array.from(document.querySelectorAll(".custom-form input.form-input-file")).forEach((input) => {
        input.addEventListener("change", (event) => {
            let fileName = null
            if (input.files.length > 1) {
                fileName = `${input.files.length} files selected`
            }
            else {
                fileName = input.files[0].name
            }
            let tooltipText = Array.from(input.files).map((file) => file.name).join("\n")
            let label = input.parentElement.querySelector("label")
            label.setAttribute("title", tooltipText)
            let fileNameSpan = label.querySelector(".form-input-file-name")
            if (fileName) {
                fileNameSpan.innerHTML = fileName.split("\\").pop()
            }
            // Display the clear button
            label.parentElement.querySelector(".form-input-file-clear")?.classList.remove("display-none")
        })
    })

    // File form field - Clear input
    Array.from(document.querySelectorAll(".custom-form .form-input-file-clear")).forEach((button) => {
        addClickEventListener(button, fileInputClear)
    })

    // Sidebar stuff
    const sidebarToggle = document.querySelector("#mesh-sidebar-toggle")
    addClickEventListener(sidebarToggle, toggleSidebar)

    const sidebarBackground = document.querySelector("#mesh-sidebar-background")
    if (sidebarBackground) {
        addClickEventListener(sidebarBackground, toggleSidebar)
    }

    // Submission list tables
    const dataTables = [".mesh-dataTable"]
    dataTables.forEach((selector) => {
        document.querySelectorAll(selector).forEach(element => {
            initDataTable($(element))
        })
    })

    // Filters
    Array.from(document.getElementsByClassName("mesh-filter-input")).forEach((filter) => {
        filter.addEventListener("input", (_) => toggleFilter(filter))
    })
    Array.from(document.getElementsByClassName("mesh-filter-set-reset")).forEach((filterSetReset) => {
        addClickEventListener(filterSetReset, resetFilters)
    })

    // Collapsible elements
    Array.from(document.querySelectorAll("*[data-collapse-toggle]")).forEach((toggle) => {
        // Initialize the aria-expanded attribute
        let collapsibleEl = document.getElementById(toggle.dataset.collapseToggle)
        if (!collapsibleEl || !collapsibleEl.classList.contains("custom-collapse")) {
            return
        }
        let ariaExpanded = collapsibleEl.classList.contains("show") ? "true" : "false"
        toggle.setAttribute("aria-expanded", ariaExpanded)

        if (!toggle.getAttribute("tabindex")) {
            toggle.setAttribute("tabindex", "0")
        }

        addClickEventListener(toggle, toggleCollapse)
    })
}

main()
