from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.forms import BaseForm
from django.http import HttpRequest
from django.utils.translation import gettext_lazy as _

from .models.base_models import BaseChangeTrackingModel
from .models.editorial_models import EditorialDecision
from .models.editorial_models import EditorialDecisionFile
from .models.editorial_models import EditorSectionRight
from .models.editorial_models import EditorSubmissionRight
from .models.file_models import BaseFileWrapperModel
from .models.journal_models import JournalSection
from .models.review_models import Review
from .models.review_models import ReviewAdditionalFile
from .models.submission_models import Submission
from .models.submission_models import SubmissionAdditionalFile
from .models.submission_models import SubmissionAuthor
from .models.submission_models import SubmissionMainFile
from .models.submission_models import SubmissionVersion
from .models.user_models import User
from .models.user_models import UserToken


def get_file_wrapper_inline(
    model_class: type[BaseFileWrapperModel],
    name_plural: str = "Files",
    extra_fields: list[str] = [],
    read_only_fields: list[str] = [],
) -> type[admin.TabularInline]:
    """
    Returns a `TalubarInline` to display related `BaseFileWrapperModel` instances.
    """

    class FileWrapperInlineAdmin(admin.TabularInline):
        model = model_class
        extra = 0
        verbose_name_plural = name_plural
        fields = ["file", "name", *extra_fields]
        readonly_fields = read_only_fields

    return FileWrapperInlineAdmin


class BaseUserChangeTrackingAdmin(admin.ModelAdmin):
    """
    Base admin model for models inheriting from BaseChangeTrackingModel.
    Attach the request to the object to be saved.
    """

    readonly_fields: list[str] = [
        "date_created",
        "created_by",
        "date_last_modified",
        "last_modified_by",
    ]

    def save_model(
        self, request: HttpRequest, obj: BaseChangeTrackingModel, form: BaseForm, change: bool
    ) -> None:
        obj._user = request.user  # type:ignore
        # change = whether the object is being created or updated
        if change and not form.has_changed():
            obj.do_not_update = True

        super().save_model(request, obj, form, change)


class UserTokenInline(admin.StackedInline):
    model = UserToken
    fk_name = "user"
    can_delete = False
    fields = ["key", "date_refreshed", "date_created"]
    readonly_fields = ["date_created"]


@admin.register(User)
class UserAdmin(DjangoUserAdmin):
    """Define admin model for our custom User model having no email field."""

    inlines = (UserTokenInline,)
    fieldsets = (
        (None, {"fields": ("email", "password")}),
        (_("Personal info"), {"fields": ("first_name", "last_name")}),
        (_("For staff"), {"fields": ["current_role", "journal_manager"]}),
        (
            _("Permissions"),
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                )
            },
        ),
        (_("Important dates"), {"fields": ("last_login", "date_joined")}),
    )
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": (
                    "email",
                    "password1",
                    "password2",
                    "first_name",
                    "last_name",
                ),
            },
        ),
    )
    list_display = (
        "email",
        "first_name",
        "last_name",
        "current_role",
        "is_staff",
        "is_superuser",
        "journal_manager",
    )
    search_fields = ("email", "first_name", "last_name")
    ordering = ("email",)
    readonly_fields = ["current_role", "last_login", "date_joined"]


@admin.register(JournalSection)
class JournalSectionAdmin(BaseUserChangeTrackingAdmin):
    fieldsets = (
        (None, {"fields": ["name", "parent"]}),
        (_("Object metadata"), {"fields": ["date_created", "created_by"]}),
    )
    list_display = ["name", "parent", "date_created", "created_by"]


class EditorSubmissionRightInline(admin.TabularInline):
    extra = 0
    verbose_name_plural = _("Assigned Editors")
    model = EditorSubmissionRight


@admin.register(Submission)
class SubmissionAdmin(BaseUserChangeTrackingAdmin):
    inlines = (EditorSubmissionRightInline,)
    fieldsets = (
        (_("Metadata"), {"fields": ["created_by", "journal_section", "state", "date_created"]}),
        (_("Submission data"), {"fields": ["name", "abstract"]}),
    )
    readonly_fields = ["name", "abstract"]
    list_display = ["created_by", "date_created", "journal_section", "name", "state"]

    def get_readonly_fields(self, request: HttpRequest, obj) -> set[str]:
        return set(list(BaseUserChangeTrackingAdmin.readonly_fields) + list(self.readonly_fields))


@admin.register(SubmissionVersion)
class SubmissionVersionAdmin(BaseUserChangeTrackingAdmin):
    inlines = (
        get_file_wrapper_inline(SubmissionMainFile),
        get_file_wrapper_inline(SubmissionAdditionalFile),
    )
    fieldsets = [
        (
            _("Submission version"),
            {"fields": ["submission", "number", "submitted", "date_submitted"]},
        )
    ]
    readonly_fields = ["submission", "number", "submitted", "date_submitted"]
    list_display = ["submission", "number", "date_created", "submitted", "date_submitted"]

    def get_readonly_fields(self, request: HttpRequest, obj) -> set[str]:
        return set(list(BaseUserChangeTrackingAdmin.readonly_fields) + list(self.readonly_fields))


@admin.register(SubmissionAuthor)
class SubmissionAuthorAdmin(BaseUserChangeTrackingAdmin):
    fieldsets = [
        (
            None,
            {
                "fields": [
                    "first_name",
                    "last_name",
                    "email",
                    "corresponding",
                    "date_created",
                    "created_by",
                ]
            },
        )
    ]
    list_display = [
        "first_name",
        "last_name",
        "email",
        "corresponding",
        "date_created",
        "created_by",
    ]


@admin.register(EditorSectionRight)
class EditorSectionRightAdmin(BaseUserChangeTrackingAdmin):
    fieldsets = [(None, {"fields": ["user", "journal_section", "date_created", "created_by"]})]
    list_display = ["user", "journal_section", "date_created", "created_by"]


@admin.register(EditorSubmissionRight)
class EditorSubmissionRightAdmin(BaseUserChangeTrackingAdmin):
    fieldsets = [(None, {"fields": ["user", "submission", "date_created", "created_by"]})]
    list_display = ["user", "submission", "date_created", "created_by"]


@admin.register(Review)
class ReviewAdmin(BaseUserChangeTrackingAdmin):
    inlines = (get_file_wrapper_inline(ReviewAdditionalFile, extra_fields=["author_access"]),)
    fieldsets = [
        (
            None,
            {
                "fields": [
                    "version",
                    "reviewer",
                    "state",
                    "date_response_due",
                    "date_review_due",
                    "request_email",
                    "accepted",
                    "date_accepted",
                    "accept_comment",
                    "recommendation",
                    "comment",
                    "date_created",
                    "created_by",
                ]
            },
        )
    ]
    list_display = [
        "reviewer",
        "date_response_due",
        "accepted",
        "date_review_due",
        "recommendation",
        "date_created",
        "created_by",
    ]

    readonly_fields = ["version", "accepted", "date_accepted"]

    def get_readonly_fields(self, request: HttpRequest, obj) -> set[str]:
        return set(list(BaseUserChangeTrackingAdmin.readonly_fields) + list(self.readonly_fields))


@admin.register(EditorialDecision)
class EditorialDecisionAdmin(BaseUserChangeTrackingAdmin):
    inlines = (get_file_wrapper_inline(EditorialDecisionFile),)
    fieldsets = [
        (
            None,
            {
                "fields": [
                    "submission",
                    "value",
                    "date_created",
                    "created_by",
                    "comment",
                    "last_modified_by",
                    "date_last_modified",
                ]
            },
        )
    ]

    list_display = ["submission", "value", "date_created", "created_by", "comment"]
    readonly_fields = ["submission"]

    def get_readonly_fields(self, request: HttpRequest, obj) -> set[str]:
        return set(list(BaseUserChangeTrackingAdmin.readonly_fields) + list(self.readonly_fields))
