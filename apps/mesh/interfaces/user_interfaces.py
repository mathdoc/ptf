from dataclasses import asdict
from dataclasses import dataclass
from dataclasses import field
from datetime import datetime
from typing import ClassVar
from typing import Self

from django.contrib.sessions.backends.base import SessionBase

from ..models.user_models import User


@dataclass
class UserInfo:
    pk: int
    first_name: str
    last_name: str
    email: str

    def __str__(self) -> str:
        return f"{self.first_name} {self.last_name} ({self.email})"

    @classmethod
    def from_user(cls, user: User) -> Self:
        return cls(
            pk=user.pk, first_name=user.first_name, last_name=user.last_name, email=user.email
        )


@dataclass
class ImpersonateData:
    """
    Interface for storing impersonation data.
    """

    _SESSION_KEY: ClassVar = "impersonate_data"
    # Max session duration in seconds
    _MAX_SESSION_DURATION: ClassVar = 10800  # 3 * 3600
    source: UserInfo
    target_id: int
    # Timestamp in seconds (use default datetime.timestamp())s
    timestamp_start: float | None = field(default=None)
    target_role: str | None = field(default=None)

    def __post_init__(self):
        """
        Dataclasses do not provide an effective fromdict method to deserialize
        a dataclass (JSON to python dataclass object).

        This enables to effectively deserialize a JSON into a ImpersonateData object,
        by replacing the nested dict by their actual dataclass representation.
        Beware this might not work well with typing (?)
        """
        source = self.source
        if source and not isinstance(source, UserInfo):
            self.source = UserInfo(**source)

        if self.timestamp_start is None:
            self.timestamp_start = datetime.utcnow().timestamp()

    @classmethod
    def from_session(cls, session: SessionBase) -> Self | None:
        """
        Returns the impersonate data from the given session.
        """
        data = session.get(cls._SESSION_KEY, None)
        if data:
            # In case the impersonate data is somehow corrupted.
            try:
                return cls(**data)
            except Exception:
                cls.clean_session(session)
        return None

    @classmethod
    def clean_session(cls, session: SessionBase):
        """
        Discards the impersonate data in the given session.
        """
        if cls._SESSION_KEY in session:
            del session[cls._SESSION_KEY]
            session.save()

    def serialize(self, session: SessionBase):
        """
        Serializes the ImpersonateData in the given session.
        """
        session[self._SESSION_KEY] = asdict(self)

    def is_valid(self) -> bool:
        """
        Check if the impersonate data is still valid.
        """
        return (
            datetime.utcnow().timestamp()
            < self.timestamp_start + self._MAX_SESSION_DURATION  # type:ignore
        )
