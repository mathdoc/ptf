from __future__ import annotations

from dataclasses import dataclass
from dataclasses import field
from enum import Enum
from enum import unique
from typing import TYPE_CHECKING
from typing import Literal

# from django.urls import reverse_lazy
# from django.utils.translation import gettext_lazy as _

# from ..views.components.button import Button

if TYPE_CHECKING:
    from ..models.submission_models import Submission
    from ..views.components.button import Button


@unique
class SubmissionStatus(Enum):
    """
    The submission status according to the user role.
    """

    # The user role must take action.
    TODO = "todo"
    # The user role is not required to take action and the submission is not archived.
    # Usually waiting for some other user to take action.
    WAITING = "waiting"
    # The submission is closed (accepted or declined).
    ARCHIVED = "archived"
    # Error somewhere - Unsolvable status or the submission should not be accessible.
    ERROR = "error"


SubmissionStatusType = Literal[
    SubmissionStatus.TODO,
    SubmissionStatus.WAITING,
    SubmissionStatus.ARCHIVED,
    SubmissionStatus.ERROR,
]


@dataclass
class SubmissionListConfig:
    """
    Config used by the submission_list view.

    The available submissions are grouped in different tables according to the
    submission status wrt. the user role.
    This object defines rules about how to display/handle one of these submission group and
    holds the submission data.
    """

    key: SubmissionStatusType
    title: str
    # All the submissions for this submission status
    all_submissions: list = field(default_factory=list)
    # The active submissions to be displayed (for ex. the filtered submissions)
    submissions: list = field(default_factory=list)
    # Whether to display the corresponding submission table
    display: bool = field(default=True)
    # HTML fragment to display when `display=False`
    display_html: str = field(default="")
    # Whether to use these submissions with filters ie. whether to use them
    # to populate the filters & whether to filter them.
    in_filters: bool = field(default=True)
    # String of classes (space separated) to be appended to the submission table
    html_classes: str = field(default="")

    @property
    def id(self) -> str:
        return f"submission-table-{self.key.value}"


@dataclass
class SubmissionStatusData:
    """
    Data about the submission status relative to an user role.
    """

    submission: Submission
    status: SubmissionStatusType
    # Precise description of the submission status
    description: str
    # Actions regarding the submissiosn accessible from the submission list.
    # shortcut_actions: list[Button] = field(default_factory=list)

    # def get_shortcut_actions(self) -> list[Button]:
    #     """
    #     Wraps self.shortcut_actions:
    #     Return a link button to the submission if there are no shortcut actions,
    #     otherwise the list of shortcut actions.
    #     """
    #     if self.shortcut_actions:
    #         return self.shortcut_actions
    #     return []
    #     # return [self.default_action_button]

    # @property
    # def default_action_button(self) -> Button:
    #     return Button(
    #         id=f"submission-link-{self.submission.pk}",
    #         title=_("View Submission"),
    #         icon_class="fa-eye",
    #         attrs={
    #             "href": [
    #                 reverse_lazy("mesh:submission_details", kwargs={"pk": self.submission.pk})
    #             ],
    #             "class": ["as-button button-highlight"],
    #             "data-tooltip": [_("Navigate to the submission page.")],
    #         },
    #     )


@dataclass
class SubmissionActionList:
    title: str
    actions: list[Button] = field(default_factory=list)
