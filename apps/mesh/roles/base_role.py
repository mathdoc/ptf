from __future__ import annotations

from abc import ABC
from abc import abstractmethod
from dataclasses import asdict
from dataclasses import dataclass
from typing import TYPE_CHECKING
from typing import ClassVar

from django.db.models import QuerySet
from django.utils.translation import gettext
from django.utils.translation import gettext_lazy as _

from ..interfaces.submission_interfaces import SubmissionListConfig
from ..interfaces.submission_interfaces import SubmissionStatus
from ..models.editorial_models import EditorialDecision
from ..models.review_models import Review
from ..models.review_models import ReviewAdditionalFile
from ..models.submission_models import Submission
from ..models.submission_models import SubmissionVersion
from ..models.user_models import User

if TYPE_CHECKING:
    from ..interfaces.submission_interfaces import SubmissionStatusData


class RoleRights(ABC):
    """
    Base interface for rights management.
    Contains the actual data used to figure out the rights over a given object/entity.
    """

    user: User

    def __init__(self, user: User) -> None:
        self.user = user

    @property
    @abstractmethod
    def submissions(self) -> QuerySet[Submission]:
        """
        Returns the queryset of submissions the user has access to.
        """
        pass

    def get_current_open_review(self, submission: Submission) -> Review | None:
        """
        Returns the current open review for the given submission, if any.
        Current review = Round not closed + review not submitted.
        """
        return None

    @abstractmethod
    def get_submission_status(self, submission: Submission) -> SubmissionStatusData:
        """
        Returns the submission status according to the user role + an optional string
        describing the submission status.
        Ex:     (WAITING, "X reports missing") for an editor+
                (WAITING, "Under review") for the author
                (TODO, "Reports due for {{date}}") for a reviewer
        """
        pass

    def get_submission_list_config(self) -> list[SubmissionListConfig]:
        """
        Returns the config to display the submissions for the user role.
        """
        return [
            SubmissionListConfig(
                key=SubmissionStatus.TODO, title=_("Requires action"), html_classes="todo"
            ),
            SubmissionListConfig(
                key=SubmissionStatus.WAITING,
                title=_("Waiting for other's input"),
                html_classes="waiting",
            ),
            SubmissionListConfig(
                key=SubmissionStatus.ARCHIVED,
                title=_("Closed / Archived"),
                html_classes="archived",
            ),
        ]

    def get_archived_submission_list_config(self) -> list[SubmissionListConfig]:
        """
        Returns the config to display only the Archived submissions.
        """
        return self.get_archived_submission_list_config()

    def can_create_submission(self) -> bool:
        """
        Wether the user role has rights to create a new submission.
        """
        return False

    def can_access_submission(self, submission: Submission) -> bool:
        """
        Wether the user role can access the given submission.
        """
        return False

    def can_edit_submission(self, submission: Submission) -> bool:
        """
        Whether the user role can edit the given submission.
        """
        return True

    def can_submit_submission(self, submission: Submission) -> bool:
        """
        Whether the user role can submit the given submission.
        It doesn't check whether the submission has the required fields to be submitted.
        """
        return self.can_edit_submission(submission) and submission.is_draft

    def can_create_version(self, submission: Submission) -> bool:
        """
        Whether the user role can submit a new version for the given submission
        """
        return False

    def can_edit_version(self, version: SubmissionVersion) -> bool:
        """
        Whether the user role can edit the given submission version.
        """
        return False

    def can_access_version(self, version: SubmissionVersion) -> bool:
        """
        Whether the user role can view the data of the given submission version.
        """
        return False

    def can_start_review_process(self, submission: Submission) -> bool:
        """
        Whether the user role can send the submission into the review process.
        """
        return False

    def can_create_editorial_decision(self, submission: Submission) -> bool:
        """
        Whether the user role can create an editorial decision for the given submission.
        """
        return False

    def can_edit_editorial_decision(self, decision: EditorialDecision) -> bool:
        """
        Whether the user role can edit the given editorial decision.
        """
        return False

    def can_access_reviews(self, version: SubmissionVersion) -> bool:
        """
        Whether the user role can view the reviews section of the given submission
        version.
        """
        return False

    def can_access_review(self, review: Review) -> bool:
        """
        Whether the user role can view the data of the given review.
        """
        return False

    def can_edit_review(self, review: Review) -> bool:
        """
        Whether the user role can edit the given review.
        """
        return False

    def can_submit_review(self, review: Review) -> bool:
        """
        Whether the user role can submit the given review.
        """
        return False

    def can_access_review_author(self, review: Review) -> bool:
        """
        Whether the user role can view the review's author name
        """
        return False

    def can_access_review_file(self, file: ReviewAdditionalFile) -> bool:
        """
        Whether the user role can access the given review's file.
        """
        return False

    def can_access_review_details(self, review: Review) -> bool:
        """
        Whether the user role can access the review details.
        """
        return False

    def can_invite_reviewer(self, version: SubmissionVersion) -> bool:
        """
        Whether the user role can invite reviewer for the given submission version.
        """
        return False

    def can_access_submission_author(self, submission: Submission) -> bool:
        """
        Wether the user role can access the author name of the given submission.
        """
        return False

    def can_impersonate(self) -> bool:
        """
        Whether the user role can impersonate other users.
        """
        return False

    def can_access_submission_log(self, submission: Submission) -> bool:
        """
        Whether the user role can view the submission log.
        """
        return False

    def can_assign_editor(self, submission: Submission) -> bool:
        """
        Whether the user role can assign an editor to the given submission.
        """
        return False

    def can_filter_submissions(self) -> bool:
        """
        Whether the user role can use filters on the submission list dashboard.
        """
        return False

    def can_access_journal_sections(self) -> bool:
        """
        Whether the user can access the submission journal_sections views.
        """
        return False

    def can_edit_journal_sections(self) -> bool:
        """
        Whether the user can edit the submission journal_sections.
        """
        return False

    def can_edit_review_file_right(self, review: Review) -> bool:
        """
        Whether the user can edit the review file access right.
        """
        return False

    def can_access_last_activity(self) -> bool:
        """
        Whether the user role can view the last activity.
        """
        return True

    def can_access_shortcut_actions(self) -> bool:
        return False


@dataclass
class RoleSummary:
    code: str
    name: str
    icon_class: str
    submission_list_title: str

    def serialize(self) -> dict:
        return asdict(self)


class Role(ABC):
    """
    Base interface for a role object.
    TODO: Is the split Role & RoleRights logically relevant ? Wouldn't it be simpler
    to merge them in a single object ?
    -----> Yes it is. It's the basis that will enable doing something like OJS with
    both role and permission level: any number of app-dependent role can be created but
    the available permission levels are the implemented RoleRights.
    https://docs.pkp.sfu.ca/learning-ojs/en/users-and-roles#permissions-and-roles
    This will require quite some work to enable freely creating role with our
    role handling system.
    """

    # Role code - Stored in user table keep track of the user current role.
    _CODE: ClassVar[str]
    # Role name
    _NAME: ClassVar[str]
    # Font-awesome 6 icon class (ex: "fa-user") used to represent the role.
    _ICON_CLASS: ClassVar[str]
    # Title for the "mesh:submission_list" view
    _SUBMISSION_LIST_TITLE: ClassVar[str] = gettext("My submissions")
    user: User
    rights: RoleRights

    def __init__(self, user: User) -> None:
        self.user = user
        self.rights = self.get_rights()

    @property
    @abstractmethod
    def active(self) -> bool:
        """
        Wether the role is active for the user.
        """
        return False

    @classmethod
    def code(cls) -> str:
        """
        Returns the role's code.
        """
        return cls._CODE

    @classmethod
    def name(cls) -> str:
        """
        Returns the role's display name.
        """
        return cls._NAME

    @classmethod
    def icon_class(cls) -> str:
        """
        Returns the role's icon HTML tag (it uses font awesome 6).
        """
        return cls._ICON_CLASS

    @classmethod
    def submissions_list_title(cls) -> str:
        return cls._SUBMISSION_LIST_TITLE

    @classmethod
    def summary(cls) -> RoleSummary:
        return RoleSummary(
            code=cls.code(),
            name=cls.name(),
            icon_class=cls.icon_class(),
            submission_list_title=cls.submissions_list_title(),
        )

    @abstractmethod
    def get_rights(self) -> RoleRights:
        """
        Returns the rights object associated to the role.
        """
        pass

    def accept(self, visitor, submission):
        return visitor.visit(self, submission)
