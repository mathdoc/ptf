from functools import cached_property
from typing import ClassVar

from django.db.models import QuerySet
from django.utils.translation import gettext as _

from ..models.submission_models import Submission
from ..models.submission_models import SubmissionState
from ..models.user_models import User
from .base_role import Role
from .editor import EditorRights


class JournalManagerRights(EditorRights):
    """
    Rights implementation for Journal manager role.
    """

    @cached_property
    def managed_submissions(self) -> QuerySet[Submission]:
        """
        Block access to non-submitted submissions.
        """
        return Submission.objects.get_submissions().exclude(state=SubmissionState.OPENED.value)

    @cached_property
    def submissions(self) -> QuerySet[Submission]:
        """
        Block access to non-submitted submissions.
        """
        return self.managed_submissions

    @cached_property
    def managed_users(self) -> QuerySet[User]:
        """
        Can impersonate anyone except admin users.
        """
        return (
            User.objects.exclude(pk=self.user.pk)
            .exclude(is_superuser=True)
            .exclude(journal_manager=True)
        )

    def can_edit_journal_sections(self) -> bool:
        return True

    def can_access_shortcut_actions(self) -> bool:
        return True


class JournalManager(Role):
    """
    Journal manager role.
    """

    _CODE: ClassVar[str] = "journal_manager"
    _NAME: ClassVar[str] = _("Journal manager")
    _ICON_CLASS: ClassVar[str] = "fa-newspaper"
    _SUBMISSION_LIST_TITLE: ClassVar[str] = _("All submissions")
    rights: JournalManagerRights

    def __init__(self, user: User) -> None:
        super().__init__(user)

    @property
    def active(self) -> bool:
        return self.user.journal_manager

    def get_rights(self) -> JournalManagerRights:
        return JournalManagerRights(self.user)
