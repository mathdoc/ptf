from functools import cached_property
from typing import ClassVar

from django.db.models import QuerySet

# from django.urls import reverse
# from django.urls import reverse_lazy
from django.utils.translation import gettext as _

from ..app_settings import BlindMode
from ..app_settings import app_settings
from ..interfaces.submission_interfaces import SubmissionStatus
from ..interfaces.submission_interfaces import SubmissionStatusData
from ..models.review_models import Review
from ..models.review_models import ReviewAdditionalFile
from ..models.submission_models import Submission
from ..models.submission_models import SubmissionState
from ..models.submission_models import SubmissionVersion
from ..models.user_models import User

# from ..views.components.button import Button
from .base_role import Role
from .base_role import RoleRights

# from ptf.url_utils import add_query_parameters_to_url


class ReviewerRights(RoleRights):
    """
    Rights implementation for reviewer role.
    """

    @cached_property
    def reviews(self) -> QuerySet[Review]:
        return Review.objects.filter(reviewer=self.user)

    @cached_property
    def submissions(self) -> QuerySet[Submission]:
        return (
            Submission.objects.get_submissions()
            .filter(versions__reviews__in=self.reviews)
            .distinct()
        )

    def can_access_submission(self, submission: Submission) -> bool:
        return submission in self.submissions

    def can_access_reviews(self, version: SubmissionVersion) -> bool:
        return self.can_access_version(version)

    def can_access_review(self, review: Review) -> bool:
        return review in self.reviews

    def can_access_review_author(self, review: Review) -> bool:
        return self.can_access_review(review)

    def can_access_review_file(self, file: ReviewAdditionalFile) -> bool:
        return self.can_access_review(file.attached_to)

    def can_access_review_details(self, review: Review) -> bool:
        return self.can_access_review(review)

    def get_current_open_review(self, version: SubmissionVersion | None) -> Review | None:
        if not version:
            return None

        current_reviews = [
            r
            for r in self.reviews
            if r.version == version and r.version.review_open and not r.submitted
        ]
        if current_reviews:
            return current_reviews[0]
        return None

    def can_edit_review(self, review: Review) -> bool:
        return review.is_editable and self.can_access_review(review)

    def can_submit_review(self, review: Review) -> bool:
        return review.is_submittable and self.can_access_review(review)

    def can_access_submission_author(self, submission: Submission) -> bool:
        return (
            self.can_access_submission(submission)
            and app_settings.BLIND_MODE != BlindMode.DOUBLE_BLIND
        )

    def can_access_version(self, version: SubmissionVersion) -> bool:
        return version in [r.version for r in self.reviews]

    def get_submission_status(self, submission: Submission) -> SubmissionStatusData:
        """
        For a reviewer, the submission status depends on the submission state and the
        (optional) review for the submission's current version.
        """
        if not self.can_access_submission(submission):
            return SubmissionStatusData(
                submission=submission, status=SubmissionStatus.ERROR, description=""
            )
        if submission.state in [SubmissionState.ACCEPTED.value, SubmissionState.REJECTED.value]:
            return SubmissionStatusData(
                submission=submission, status=SubmissionStatus.ARCHIVED, description=""
            )
        # Get the first review attached to the submission's current version.
        # There is max. 1 review per user per version.
        review = next((r for r in self.reviews if r.version == submission.current_version), None)
        if not review or not submission.current_version.review_open:  # type:ignore
            return SubmissionStatusData(
                submission=submission,
                status=SubmissionStatus.WAITING,
                description=_("The round for your review is closed."),
            )
        elif review.submitted:
            return SubmissionStatusData(
                submission=submission,
                status=SubmissionStatus.WAITING,
                description=_("Review submitted"),
            )
        elif review.accepted is False:
            return SubmissionStatusData(
                submission=submission,
                status=SubmissionStatus.ARCHIVED,
                description=_("Review declined"),
            )
        elif review.accepted is None:
            status_data = SubmissionStatusData(
                submission=submission,
                status=SubmissionStatus.TODO,
                description=_("Accept or decline the review request.")
                + " "
                + _("Due date")
                + f": {review.date_response_due.strftime('%Y-%m-%d')}",
                # shortcut_actions=[
                #     Button(
                #         id=f"review-accept-{review.pk}",
                #         title=_("Accept"),
                #         icon_class="fa-check",
                #         attrs={
                #             "href": [
                #                 add_query_parameters_to_url(
                #                     reverse("mesh:review_accept", kwargs={"pk": review.pk}),
                #                     {"accepted": ["true"]},
                #                 )
                #             ],
                #             "class": ["as-button", "button-success"],
                #         },
                #     ),
                #     Button(
                #         id=f"review-accept-{review.pk}",
                #         title=_("Decline"),
                #         icon_class="fa-xmark",
                #         attrs={
                #             "href": [
                #                 add_query_parameters_to_url(
                #                     reverse("mesh:review_accept", kwargs={"pk": review.pk}),
                #                     {"accepted": ["false"]},
                #                 )
                #             ],
                #             "class": ["as-button", "button-error"],
                #         },
                #     ),
                # ],
            )
            # status_data.shortcut_actions.append(status_data.default_action_button)
            return status_data
        # The review is accepted but not submitted yet;
        else:
            return SubmissionStatusData(
                submission=submission,
                status=SubmissionStatus.TODO,
                description=_("Submit your review report.")
                + " "
                + _("Due date")
                + f": {review.date_review_due.strftime('%Y-%m-%d')}",
                # shortcut_actions=[
                #     Button(
                #         id=f"review-submit-{review.pk}",
                #         title=_("Submit review"),
                #         icon_class="fa-pen-to-square",
                #         attrs={
                #             "href": [reverse_lazy("mesh:review_update", kwargs={"pk": review.pk})],
                #             "class": ["as-button"],
                #         },
                #     )
                # ],
            )


class Reviewer(Role):
    """
    Reviewer role.
    """

    _CODE: ClassVar[str] = "reviewer"
    _NAME: ClassVar[str] = _("Reviewer")
    _ICON_CLASS: ClassVar[str] = "fa-user-group"
    _SUBMISSION_LIST_TITLE: ClassVar[str] = _("My assignments")
    rights: ReviewerRights

    def __init__(self, user: User) -> None:
        super().__init__(user)

    @property
    def active(self) -> bool:
        return len(self.rights.reviews) > 0

    def get_rights(self) -> ReviewerRights:
        return ReviewerRights(self.user)
