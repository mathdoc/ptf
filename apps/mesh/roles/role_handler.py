from dataclasses import dataclass
from typing import Any
from typing import Self

from django.core.exceptions import ImproperlyConfigured
from django.http import HttpRequest

from ..exceptions import RoleException
from ..interfaces.user_interfaces import ImpersonateData
from ..models.user_models import User
from .author import Author
from .base_role import Role
from .base_role import RoleRights
from .editor import Editor
from .journal_manager import JournalManager
from .reviewer import Reviewer

# Cache of the RoleHandler at request level. The RoleHandler might be derived
# at middleware level.
ROLE_HANDLER_CACHE = "role_handler_cache"
ROLE_CLASSES: list[type[Role]] = [Author, Reviewer, Editor, JournalManager]

if len({role_cls.code() for role_cls in ROLE_CLASSES}) != len(ROLE_CLASSES):
    data_str = " - ".join(f"{role_cls}: {role_cls.code()}" for role_cls in ROLE_CLASSES)
    raise ImproperlyConfigured(f"At least 2 distinct roles have the same code: {data_str}")


def get_role_class_from_code(code: str | None) -> type[Role] | None:
    if code is None:
        return None

    for role_cls in ROLE_CLASSES:
        if role_cls.code() == code:
            return role_cls
    return None


@dataclass
class RoleData:
    """
    Contains all the roles and related data accessible to an user.
    The attribute name much match the `_CODE` of the role class (which should also
    match the python file name for consistency).
    """

    author: Author
    reviewer: Reviewer
    editor: Editor
    journal_manager: JournalManager

    def get_roles(self) -> list[Role]:
        """
        Get the list of all roles for an user.
        The order in the list is the priority order to get the user's default role.
        """
        return [self.journal_manager, self.editor, self.reviewer, self.author]

    def __getitem__(self, key) -> Role | None:
        return getattr(self, key, None)

    def default_role(self) -> Role:
        """
        Get the user's default role.
        A role is available to the user if `role.active = True`
        """
        for role in self.get_roles():
            if role.active:
                return role
        raise RoleException("There is no default role for this user. Should not happen.")

    def get_active_roles(self) -> list[Role]:
        """
        Returns the list of active roles for the current user.
        """
        return [role for role in self.get_roles() if role.active]

    @classmethod
    def from_user(cls, user: User) -> Self:
        return cls(
            author=Author(user),
            reviewer=Reviewer(user),
            editor=Editor(user),
            journal_manager=JournalManager(user),
        )


class RoleHandler:
    """
    Handler for the role logic & data of an user.
    """

    user: User
    current_role: Role
    # Just a shortcut for `current_role.rights`
    rights: RoleRights
    role_data: RoleData
    # Whether the `complete_init` method has been called.
    init_complete = False
    # The RoleHandler is usually instantiated on each request for the current user
    # and is used to derive the user's current role.
    # We keep a reference of the enclosing request here.
    # The value can remain `None`, for ex. if the role handler is instantiated just to
    # check some global rights for a given user.
    request: HttpRequest | None = None

    @property
    def impersonate_data(self) -> ImpersonateData | None:
        if self.request is None:
            return None
        return ImpersonateData.from_session(self.request.session)

    def __init__(
        self, user: User, request: HttpRequest | None = None, partial_init: bool = False
    ) -> None:
        """
        The initialization is split in 2 steps:
            -   `init_user_roles`   Populates the given user role data.
            -   `complete_init`     Process the given request to derive request
                                    dependent attributes (ex. impersonate data)
                                    and derive the user current role and related
                                    variables

        This is useful when we only want an user's role data without any impact on the
        database (for ex: see `TokenBackend`).
        Params:
            -   `user`          The user for which the roles & corresponding rights
                                should be derived.
            -   `request`       A reference to the enclosing HTTP request (Optional).
            -   `partial_init`  Whether to perform the second step `complete_init`.
                                Default to `False`.
        """
        self.user = user
        self.init_user_roles()
        self.request = request

        if not partial_init:
            self.complete_init()

    def complete_init(self):
        """
        Finalize the initialization of the role_handler. It consists mainly in deriving
        the user's current role and setting some related variables.
        """
        if self.init_complete:
            return

        role = self.get_current_active_role()

        self.set_current_role(role)

        # Set the impersonation boolean.
        # Current impersonator cannot further impersonate another user (we prevent
        # nested impersonated users, would be too messy).
        if self.request:
            self.request.session[
                "can_impersonate"
            ] = self.impersonate_data is None and self.check_global_rights("can_impersonate")
        self.init_complete = True

    def get_current_active_role(self) -> Role:
        """
        Derive the user's current active role.
        It depends on:
            - The current role stored in DB
            - The impersonate data if the user is currently being impersonated.
            - The user's active roles.
        """
        current_role_code = self.user.current_role

        # In impersonate context, the current_role can be set from the impersonate data.
        impersonate_data = self.impersonate_data
        if impersonate_data and impersonate_data.target_role:
            current_role_code = impersonate_data.target_role
        elif current_role_code is None:
            current_role_code = self.role_data.default_role().code()

        current_role_class = get_role_class_from_code(current_role_code)
        if current_role_class is None:
            current_role_class = self.role_data.default_role().__class__

        # Check that the selected role is active
        role = self.role_data[current_role_class.code()]
        if role is None or not role.active:
            role = self.role_data.default_role()

        return role

    def get_active_roles(self):
        """
        Return all the roles that are active (ie: that a user can select)
        """
        return self.role_data.get_active_roles()

    def set_current_role(self, role: Role):
        """
        Sets the user current role.

        IMPORTANT: In case of a current user impersonator, don't update the current
        role in the database as it would affect the actual impersonated user.
        Stores the new role in session, to be used when negotiating
        the user current role in `get_current_active_role`.
        """
        self.user.current_role = role.code()

        impersonate_data = self.impersonate_data
        if impersonate_data is None:
            self.user.save()
        else:
            impersonate_data.target_role = role.code()
            impersonate_data.serialize(self.request.session)  # type:ignore

        self.current_role = role
        self.rights = role.rights
        if self.request:
            self.request.session["user_role"] = role.summary().serialize()
            self.request.session["available_roles"] = [
                role.summary().serialize() for role in self.role_data.get_active_roles()
            ]

    def init_user_roles(self) -> None:
        """
        Initialize the role data of the given user.
        """
        self.role_data = RoleData.from_user(self.user)

    def switch_role(self, code: str) -> Role:
        """
        Switch the user's current role to the provided code.
        Return the selected user role.
        """
        role = self.role_data[code]
        if role is None:
            raise RoleException(f"The role code '{code}' does not exist.")
        elif not role.active:
            raise RoleException(
                f"The role '{role.name()}' is not available to user '{self.user}'."
            )

        self.set_current_role(role)

        return role

    def execute_rights_function(
        self, rights: RoleRights, function_name: str, *args, **kwargs
    ) -> Any:
        """
        Execute the given function with the provided arguments for the current role.
        """
        if not hasattr(rights, function_name):
            raise RoleException(
                f"The right function `{function_name}` is not implemented "
                f"for the role rights '{rights.__class__}'"
            )
        function = getattr(rights, function_name)
        if not callable(function):
            raise RoleException(
                f"The right function `{function_name}` is not callable "
                f"for the role rights '{rights.__class__}'"
            )

        return function(*args, **kwargs)

    def check_rights(self, function_name: str, *args, **kwargs) -> bool:
        """
        Execute the given function with the provided arguments for the current role.
        This should only be used for right functions returning boolean values.
        """
        return self.execute_rights_function(self.rights, function_name, *args, **kwargs)

    def check_global_rights(self, function_name: str, *args, **kwargs) -> bool:
        """
        Execute the given function with the provided arguments for every active role
        of the current user. Returns True if at least one role has the given right,
        else False.
        """
        for role in self.role_data.get_active_roles():
            result = self.execute_rights_function(role.rights, function_name, *args, **kwargs)
            if result:
                return True
        return False

    def get_from_rights(self, function_name: str, *args, **kwargs) -> Any:
        """
        Execute the given function with the provided arguments for the current role.
        This should only be used for rights functions with a return type other than
        `bool`.
        """
        return self.execute_rights_function(self.rights, function_name, *args, **kwargs)

    def get_attribute(self, attribute_name: str) -> Any:
        if not hasattr(self.rights, attribute_name):
            raise RoleException(
                f"The attribute `{attribute_name}` is not implemented "
                f"for the role '{self.current_role.code()}'"
            )

        return getattr(self.rights, attribute_name)

    def token_authentication_allowed(self) -> bool:
        """
        Token authentication is forbidden for user with active Editor+ roles.
        """
        roles_token_forbidden = [Editor.code(), JournalManager.code()]
        return not any(
            role.code() in roles_token_forbidden for role in self.role_data.get_active_roles()
        )


class RoleVisitor:
    def __init__(self, role_handler, *args, **kwargs):
        self.role_handler = role_handler
        super().__init__(*args, **kwargs)

    def visit(self, role, submission):
        function_name = f"visit_{role.code()}"
        ftor = getattr(self, function_name, None)
        if callable(ftor):
            return ftor(submission)

    def visit_author(self, submission):
        pass

    def visit_editor(self, submission):
        pass

    def visit_journal_manager(self, submission):
        pass

    def visit_reviewer(self, submission):
        pass
