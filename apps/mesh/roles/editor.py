from collections.abc import Collection
from functools import cached_property
from itertools import chain
from typing import ClassVar

from django.db.models import Q
from django.db.models import QuerySet
from django.urls import reverse_lazy
from django.utils.translation import gettext as _

from ..interfaces.submission_interfaces import SubmissionListConfig
from ..interfaces.submission_interfaces import SubmissionStatus
from ..interfaces.submission_interfaces import SubmissionStatusData
from ..models.editorial_models import EditorialDecision
from ..models.journal_models import JournalSection
from ..models.review_models import Review
from ..models.review_models import ReviewAdditionalFile
from ..models.submission_models import Submission
from ..models.submission_models import SubmissionState
from ..models.submission_models import SubmissionVersion
from ..models.user_models import User
from .base_role import Role
from .base_role import RoleRights


class EditorRights(RoleRights):
    """
    Base rights for every editor derived class.
    Basically they would have the same rights over a submission. The only difference is
    the available list of submissions.
    """

    @cached_property
    def journal_sections(self) -> list[JournalSection]:
        """
        Recursively gets ALL the submission journal_sections the user has access to.
        That means all directly assigned journal_sections and all their children.
        """
        direct_journal_sections = JournalSection.objects.filter(editors__user=self.user)
        if not direct_journal_sections:
            return []

        base_journal_sections = chain.from_iterable(
            JournalSection.objects.get_children_recursive(c) for c in direct_journal_sections
        )

        return [*direct_journal_sections, *base_journal_sections]

    @cached_property
    def managed_submissions(self) -> QuerySet[Submission]:
        """
        Editors have edit access to all submissions in their assigned sections that
        they did not author + all directly assigned submissions.
        """
        submissions = Submission.objects.get_submissions().filter(editors__user=self.user)
        if self.journal_sections:
            submissions = submissions.union(
                Submission.objects.get_submissions()
                .filter(journal_section__in=self.journal_sections)
                .exclude(Q(created_by=self.user) | Q(state=SubmissionState.OPENED.value))
            )
        return submissions

    @cached_property
    def submissions(self) -> QuerySet[Submission]:
        """
        Editors have access to all submissions and associated data (TBD)
        """
        return self.managed_submissions

    def can_access_submission(self, submission: Submission) -> bool:
        return submission in self.submissions

    def can_manage_submission(self, submission: Submission) -> bool:
        return submission in self.managed_submissions

    def can_access_reviews(self, version: SubmissionVersion) -> bool:
        return self.can_access_submission(version.submission)

    def can_access_review(self, review: Review) -> bool:
        return self.can_access_submission(review.version.submission)

    def can_access_review_author(self, review: Review) -> bool:
        return self.can_manage_submission(review.version.submission)

    def can_access_review_file(self, file: ReviewAdditionalFile) -> bool:
        return self.can_access_review(file.attached_to)

    def can_access_review_details(self, review: Review) -> bool:
        return self.can_manage_submission(review.version.submission)

    def can_invite_reviewer(self, version: SubmissionVersion) -> bool:
        """
        A reviewer can be invited only if the version is submitted by the author and
        opened for review.
        """
        return (
            version.submitted
            and version.review_open
            and self.can_manage_submission(version.submission)
        )

    def can_access_submission_author(self, submission: Submission) -> bool:
        return self.can_access_submission(submission)

    def can_access_version(self, version: SubmissionVersion) -> bool:
        """
        Non-submitted versions are not available to anyone except the author.
        """
        return version.submitted and self.can_access_submission(version.submission)

    @cached_property
    def managed_users(self) -> QuerySet[User]:
        """
        Restrict to non-editor users.

        TODO: Restrict actions to manageable_submissions ?
        """
        return (
            User.objects.exclude(pk=self.user.pk)
            .exclude(journal_manager=True)
            .exclude(editor_sections__isnull=False)
            .exclude(editor_submissions__isnull=False)
            .exclude(is_superuser=True)
        )

    def can_impersonate(self) -> bool:
        return True

    def can_access_submission_log(self, submission: Submission) -> bool:
        return self.can_access_submission(submission)

    def can_create_editorial_decision(self, submission: Submission) -> bool:
        return self.can_manage_submission(submission)

    def can_edit_editorial_decision(self, decision: EditorialDecision) -> bool:
        return self.can_manage_submission(decision.submission)

    def can_start_review_process(self, submission: Submission) -> bool:
        return self.can_manage_submission(submission) and submission.is_reviewable

    def can_assign_editor(self, submission: Submission) -> bool:
        return self.can_manage_submission(submission)

    def get_submission_status(self, submission: Submission) -> SubmissionStatusData:
        """
        For an editor+ role, the status highly depends on the submission state.
        -   Submission/Revisions submitted -> The editor must take action
        -   Revisions required -> Waiting for author to submit
        -   Submission accepted/declined -> Nothing to do
        -   Under review -> Tricky. Depends on the existing reviews (or not) state.
        """
        if not self.can_access_submission(submission) or submission.is_draft:
            return SubmissionStatusData(
                submission=submission, status=SubmissionStatus.ERROR, description=""
            )

        # buttons = []
        # if self.can_create_editorial_decision(submission):
        #     buttons.append(
        #         Button(
        #             id=f"submission-editorial-decision-{submission.pk}",
        #             title=_("Editorial decision"),
        #             icon_class="fa-plus",
        #             attrs={
        #                 "href": [
        #                     reverse_lazy(
        #                         "mesh:editorial_decision_create",
        #                         kwargs={"submission_pk": submission.pk},
        #                     )
        #                 ],
        #                 "class": ["as-button"],
        #             },
        #         )
        #     )

        if self.can_assign_editor(submission) and not submission.all_assigned_editors:
            pass
            # buttons.append(
            #     Button(
            #         id=f"submission-assign-editor-{submission.pk}",
            #         title=_("Assign editor"),
            #         icon_class="fa-user-tie",
            #         attrs={
            #             "href": [
            #                 reverse_lazy("mesh:submission_editors", kwargs={"pk": submission.pk})
            #             ],
            #             "class": ["as-button"],
            #         },
            #     )
            # )

        if submission.state in [
            SubmissionState.SUBMITTED.value,
            SubmissionState.REVISION_SUBMITTED.value,
        ]:
            # if self.can_start_review_process(submission):
            #     buttons.append(
            #         Button(
            #             id=f"submission-send-to-review-{submission.pk}",
            #             title=_("Start review process"),
            #             icon_class="fa-file-import",
            #             form=StartReviewProcessForm(initial={"process": True}),
            #             attrs={
            #                 "href": [
            #                     reverse_lazy(
            #                         "mesh:submission_start_review_process",
            #                         kwargs={"pk": submission.pk},
            #                     )
            #                 ]
            #             },
            #         )
            #     )
            return SubmissionStatusData(
                submission=submission,
                status=SubmissionStatus.TODO,
                description=_("Requires an editorial action."),
                # shortcut_actions=buttons,
            )

        elif submission.state in [
            SubmissionState.OPENED.value,
            SubmissionState.REVISION_REQUESTED.value,
        ]:
            return SubmissionStatusData(
                submission=submission,
                status=SubmissionStatus.WAITING,
                description=_("Waiting for a new version"),
                # shortcut_actions=buttons,
            )

        elif submission.state in [
            SubmissionState.ACCEPTED.value,
            SubmissionState.REJECTED.value,
        ]:
            return SubmissionStatusData(
                submission=submission,
                status=SubmissionStatus.ARCHIVED,
                description=_("Submission") + " " + submission.get_state_display(),  # type:ignore
            )

        # Case under review
        version: SubmissionVersion = submission.current_version  # type:ignore
        # if self.can_invite_reviewer(version):
        #     buttons.append(
        #         Button(
        #             id=f"submission-referee-request-{submission.pk}",
        #             title=_("Request review"),
        #             icon_class="fa-user-group",
        #             attrs={
        #                 "href": [
        #                     reverse_lazy("mesh:review_create", kwargs={"version_pk": version.pk})
        #                 ],
        #                 "class": ["as-button"],
        #             },
        #         )
        #     )
        reviews: QuerySet[Review] = version.reviews.all()  # type:ignore
        if not reviews:
            return SubmissionStatusData(
                submission=submission,
                status=SubmissionStatus.TODO,
                description=_("No reviewers for the current round."),
                # shortcut_actions=buttons,
            )

        # If some review requests are not answered and answer date is overdued
        if any(r.is_response_overdue for r in reviews):
            # if self.can_manage_submission(submission):
            #     buttons.append(
            #         Button(
            #             id=f"submission-inspect-review-{submission.pk}",
            #             title=_("Inspect review"),
            #             icon_class="fa-magnifying-glass",
            #             attrs={
            #                 "href": [
            #                     reverse_lazy(
            #                         "mesh:submission_details", kwargs={"pk": submission.pk}
            #                     )
            #                 ],
            #                 "class": ["as-button", "button-light"],
            #             },
            #         )
            #     )
            return SubmissionStatusData(
                submission=submission,
                status=SubmissionStatus.TODO,
                description=_("At least 1 request answer is overdue."),
                # shortcut_actions=buttons,
            )

        # If some reports are not submitted and report date is overdued
        elif any(r.is_report_overdue for r in reviews):
            # if self.can_manage_submission(submission):
            #     buttons.append(
            #         Button(
            #             id=f"submission-inspect-review-{submission.pk}",
            #             title=_("Inspect review"),
            #             icon_class="fa-magnifying-glass",
            #             attrs={
            #                 "href": [
            #                     reverse_lazy(
            #                         "mesh:submission_details", kwargs={"pk": submission.pk}
            #                     )
            #                 ],
            #                 "class": ["as-button", "button-light"],
            #             },
            #         )
            #     )
            return SubmissionStatusData(
                submission=submission,
                status=SubmissionStatus.TODO,
                description=_("At least 1 report is overdue."),
                # shortcut_actions=buttons,
            )

        # All reviews submitted
        elif all(r.is_completed for r in reviews):
            return SubmissionStatusData(
                submission=submission,
                status=SubmissionStatus.TODO,
                description=_("All reviews have been declined or submitted."),
                # shortcut_actions=buttons,
            )

        # Pending reviews, nothing to do ?
        return SubmissionStatusData(
            submission=submission,
            status=SubmissionStatus.WAITING,
            description=_("Waiting for review(s)."),
            # shortcut_actions=buttons,
        )

    def get_submission_list_config(self) -> list[SubmissionListConfig]:
        """
        Returns the config to display the submissions for the user role.
        """
        return [
            SubmissionListConfig(
                key=SubmissionStatus.TODO, title=_("Requires action"), html_classes="todo"
            ),
            SubmissionListConfig(
                key=SubmissionStatus.WAITING,
                title=_("Waiting for other's input"),
                html_classes="waiting",
            ),
            SubmissionListConfig(
                key=SubmissionStatus.ARCHIVED,
                title=_("Closed / Archived"),
                html_classes="archived",
                in_filters=False,
                display=False,
                display_html=f"""<p>Please visit <a href="{reverse_lazy("mesh:submission_list_archived")}">this page</a> to consult all archived submissions.</p>""",
            ),
        ]

    def get_archived_submission_list_config(self) -> list[SubmissionListConfig]:
        """
        Returns the config to display only the Archived submissions.
        """
        return [
            SubmissionListConfig(
                key=SubmissionStatus.ARCHIVED,
                title=_("Closed / Archived"),
                html_classes="archived",
            ),
        ]

    def can_filter_submissions(self) -> bool:
        return True

    def can_access_journal_sections(self) -> bool:
        return True

    def can_edit_review_file_right(self, review: Review) -> bool:
        return self.can_manage_submission(review.version.submission)


class Editor(Role):
    """
    Editor role.
    """

    _CODE: ClassVar[str] = "editor"
    _NAME: ClassVar[str] = _("Editor")
    _ICON_CLASS: ClassVar[str] = "fa-user-tie"
    _SUBMISSION_LIST_TITLE: ClassVar[str] = _("My assignments")
    rights: EditorRights

    def __init__(self, user: User):
        super().__init__(user)

    @property
    def active(self) -> bool:
        """
        Editor role is active if the user has rights over any
        `JournalSection` or `Submission`.
        """
        return len(self.rights.journal_sections) > 0 or len(self.rights.managed_submissions) > 0

    def get_rights(self) -> EditorRights:
        return EditorRights(self.user)


def get_section_editors(submission: Submission) -> Collection[User]:
    """
    Get the editors with access to the given submission from the EditorSectionRights.
    """
    # 1 - Get all journal_sections above the given submission
    if not submission.journal_section:
        return []

    journal_sections = [
        submission.journal_section,
        *JournalSection.objects.get_parents_recursive(submission.journal_section),
    ]

    # 2 - Get all users with rights over one of the above journal_sections
    return User.objects.filter(editor_sections__journal_section__in=journal_sections)
