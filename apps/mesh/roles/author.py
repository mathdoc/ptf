from functools import cached_property
from typing import ClassVar

from django.db.models import QuerySet

# from django.urls import reverse_lazy
from django.utils.translation import gettext as _

from ..app_settings import BlindMode
from ..app_settings import app_settings
from ..interfaces.submission_interfaces import SubmissionStatus
from ..interfaces.submission_interfaces import SubmissionStatusData
from ..models.review_models import Review
from ..models.review_models import ReviewAdditionalFile
from ..models.submission_models import Submission
from ..models.submission_models import SubmissionState
from ..models.submission_models import SubmissionVersion
from ..models.user_models import User

# from ..views.components.button import Button
from .base_role import Role
from .base_role import RoleRights


class AuthorRights(RoleRights):
    """
    Rights implementation for author role.
    """

    @cached_property
    def submissions(self) -> QuerySet[Submission]:
        return Submission.objects.get_submissions().filter(created_by=self.user)

    def can_create_submission(self) -> bool:
        return True

    def can_access_submission(self, submission: Submission) -> bool:
        return submission in self.submissions

    def can_edit_submission(self, submission: Submission) -> bool:
        result = self.can_access_submission(submission)
        result = (
            result
            and submission.is_draft
            or submission.state == SubmissionState.REVISION_REQUESTED.value
        )
        return result

    def can_create_version(self, submission: Submission) -> bool:
        if not self.can_access_submission(submission):
            return False
        current_version = submission.current_version
        # The author must have entered at least 1 submission author to proceed
        # with the submission version.
        if submission.is_draft and current_version is None:
            return submission.authors.exists()  # type:ignore
        return (
            submission.state == SubmissionState.REVISION_REQUESTED.value
            and current_version is not None
            and current_version.submitted
        )

    def can_access_version(self, version: SubmissionVersion) -> bool:
        return self.can_access_submission(version.submission)

    def can_edit_version(self, version: SubmissionVersion) -> bool:
        return not version.submitted and self.can_access_submission(version.submission)

    def can_access_submission_author(self, submission: Submission) -> bool:
        return self.can_access_submission(submission)

    def can_access_reviews(self, version: SubmissionVersion) -> bool:
        """
        An author can access reviews when the review process is closed.
        """
        return not version.review_open and self.can_access_submission(version.submission)

    def can_access_review(self, review: Review) -> bool:
        """
        An author can access review with at least one accessible file.
        """
        return (
            self.can_access_reviews(review.version)
            and review.submitted
            and review.recommendation is not None
            and any(
                self.can_access_review_file(f)
                for f in review.additional_files.all()  # type:ignore
            )
        )

    def can_access_review_author(self, review: Review) -> bool:
        return (
            app_settings.BLIND_MODE == BlindMode.NO_BLIND
            and review.version.submission in self.submissions
        )

    def can_access_review_file(self, file: ReviewAdditionalFile) -> bool:
        return file.author_access

    def get_submission_status(self, submission: Submission) -> SubmissionStatusData:
        """
        For an author, the submission status is one of the following:
        -   "Submission is still in draft", True - when STATUS_OPENED
        -   "A new version must be submitted", True - when STATUS_REVISIONS_REQUESTED
        -   "Submission accepted", False - when STATUS_ACCEPTED
        -   "Submission declined", False - when STATUS_DECLINED
        -   "Waiting for editorial actions" - when STATUS_SUBMITTED or STATUS_REVISIONS_SUBMITTED
        -   "Under review", False - For other state
        """
        # Should not happen ?
        if not self.can_access_submission(submission):
            return SubmissionStatusData(
                submission=submission, status=SubmissionStatus.ERROR, description=""
            )

        if submission.state in [
            SubmissionState.SUBMITTED.value,
            SubmissionState.REVISION_SUBMITTED.value,
        ]:
            return SubmissionStatusData(
                submission=submission,
                status=SubmissionStatus.WAITING,
                description=_("Under review"),
            )
        elif submission.state == SubmissionState.OPENED.value:
            status_data = SubmissionStatusData(
                submission=submission,
                status=SubmissionStatus.TODO,
                description=_("Complete your submission"),
            )
            return status_data

        elif submission.state == SubmissionState.REVISION_REQUESTED.value:
            status_data = SubmissionStatusData(
                submission=submission,
                status=SubmissionStatus.TODO,
                description=_("Submit a new version"),
            )
            # if submission.state == SubmissionState.OPENED.value and self.can_submit_submission(
            #     submission
            # ):
            #     status_data.shortcut_actions.append(
            #         Button(
            #             id=f"submission-submit-{submission.pk}",
            #             title=_("Resume submission"),
            #             icon_class="fa-list-check",
            #             attrs={
            #                 "href": [
            #                     reverse_lazy(
            #                         "mesh:submission_resume", kwargs={"pk": submission.pk}
            #                     )
            #                 ],
            #                 "class": ["as-button"],
            #             },
            #         )
            #     )
            # elif (
            #     submission.state == SubmissionState.REVISION_REQUESTED.value
            #     and self.can_create_version(submission)
            # ):
            #     status_data.shortcut_actions.append(
            #         Button(
            #             id=f"submission-submit-{submission.pk}",
            #             title=_("Submit revisions"),
            #             icon_class="fa-plus",
            #             attrs={
            #                 "href": [
            #                     reverse_lazy(
            #                         "mesh:submission_version_create",
            #                         kwargs={"submission_pk": submission.pk},
            #                     )
            #                 ],
            #                 "class": ["as-button"],
            #             },
            #         )
            #     )
            # elif (
            #     submission.state == SubmissionState.REVISION_REQUESTED.value
            #     and submission.current_version
            #     and not submission.current_version.submitted
            # ):
            #     status_data.shortcut_actions.append(
            #         Button(
            #             id=f"submission-submit-{submission.pk}",
            #             title=_("Resume revisions"),
            #             icon_class="fa-list-check",
            #             attrs={
            #                 "href": [
            #                     reverse_lazy(
            #                         "mesh:submission_version_update",
            #                         kwargs={"pk": submission.current_version.pk},
            #                     )
            #                 ],
            #                 "class": ["as-button"],
            #             },
            #         )
            #     )
            return status_data

        elif submission.state == SubmissionState.ON_REVIEW.value:
            return SubmissionStatusData(
                submission=submission,
                status=SubmissionStatus.WAITING,
                description=_("Submission is under review"),
            )
        elif submission.state in [
            SubmissionState.ACCEPTED.value,
            SubmissionState.REJECTED.value,
        ]:
            return SubmissionStatusData(
                submission=submission,
                status=SubmissionStatus.ARCHIVED,
                description=_("Submission") + " " + submission.get_state_display(),  # type:ignore
            )

        return SubmissionStatusData(
            submission=submission, status=SubmissionStatus.ERROR, description=""
        )

    def can_access_last_activity(self) -> bool:
        return False


class Author(Role):
    """
    Author role.
    This is the base role of mesh app, always active.
    """

    _CODE: ClassVar[str] = "author"
    _NAME: ClassVar[str] = _("Author")
    _ICON_CLASS: ClassVar[str] = "fa-pen-nib"
    _SUBMISSION_LIST_TITLE: ClassVar[str] = _("My submissions")
    rights: AuthorRights

    def __init__(self, user: User) -> None:
        super().__init__(user)

    @property
    def active(self) -> bool:
        return True

    def get_rights(self) -> AuthorRights:
        return AuthorRights(self.user)
