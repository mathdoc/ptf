# MESH

This Django application implements a custom journal/editorial management software.

It is strictly speaking independent from the main `ptf` app but it's planned to be integrated together at some point. There are only a few calls to `ptf.utils` and `ptf.url_utils` modules as of November 23rd, 2023 and the link to `ptf` requirements file to align the apps dependencies.

The site `mesh_site` implements an instance website of the application. A live test version
is available at [mesh-test.centre-mersenne.org](https://mesh-test.centre-mersenne.org)

__NB1:__ Language support is activated and most places use `gettext` or the `{% translate %}` tag when it should. However no translations are offered as of November 23rd 2023.

__NB2:__ A large number of tests are located in the test folder.

---------------------------------------

[[_TOC_]]

---------------------------------------

# [Introduction] `mesh_site` quick setup

- Install system dependencies (python, postgresql):

    ```shell
    sudo apt install python3-dev python3-venv libpq-dev postgresql
    ```

- Create a python virtual environment and activate it:

    ```shell
    cd /sites/mesh_site
    python3 -m venv my_venv_name
    source my_venv_name/bin/activate
    ```

- Install dependencies in the virtual environment:

    ```shell
    pip install -r requirements.txt --upgrade
    ```

- Create a `settings_local.py` in from the `settings_local.sample.py`

    ```shell
    cd mesh_site
    cp settings_local.sample.py settings_local.py
    ```

- Configure the different settings, cf. section [__III.Django settings__](#iii-django-settings), mainly create a Postgres database (if you want a different database engine you'll need to add them to the app settings) :

    ```shell
    sudo su postgres
    createuser -P mesh_base_user
    createdb mesh_base -O mesh_base_user
    exit
    ```

- Setup web server to serve the website or run django dev server locally:

    ```shell
    python manage.py runserver 8000 # run dev server on port 8000
    ```


# I. Models

## I.1 UML Diagram

See the below diagram to get a quick overview of the application models.

Diagram obtained with the command:

```shell
python manage.py graph_models mesh --disable-abstract-fields -x created_by,last_modified_by -X AbstractUser -o mesh_models.png
```

It requires the following dependencies:

```shell
sudo apt-get install python3-dev graphviz libgraphviz-dev pkg-config
sudo pip install pygraphviz
```

The `created_by` and `last_modified_by` of the BaseChangeTrackingModel are foreign keys to the User model.
The graph becomes unreadable if we keep them because these abstract inherited relationships are drawn for every inheriting model by the tool.

![MESH models UML diagram](./doc/mesh_models.png)

## I.2 Base models

We introduce a number of _"Base"_ models to implement useful features shared across different models.

### I.2.a Basic change tracking

All models inherit from [`BaseChangeTrackingModel`](./models/base_models.py).

It contains 4 fields to track the creation and last modification of the model: `created_by` (User FK), `date_created` (datetime), `last_modified_by` (User FK), `date_last_modified` (datetime). \

One must set the `_user` attribute of the model before saving it to have an automatic update of the above fields.

### I.2.b File handling

We use a custom model wrapper [`BaseFileWrapperModel`](./models/file_models.py) to enhance the basic Django `FileField` model field. This gives us flexibility and an uniform way to deal with all our files.
For example, we currently use it to:

- store the initial name of the file (the file might be stored under another name)
- implement validators at the model level
- define the upload path elsewhere than in the FileField declaration
- auto-delete the file when the model is deleted or when the file is changed.

Thus one should not use the `FileField` directly all files should be declared as a separate model either in
a OneToOne/ManyToOne relationship, inheriting from our custom model wrapper.

__WARNING__: The auto-delete feature might become critical if a same file can be referenced by multiple `BaseFileWrapperModel`. This is currently not the case.


We use a custom storage for the user files distinct from the `MEDIA_ROOT` (see `FILES_DIRECTORY` setting.)

We use custom handlers everywhere to automatically handle files with our file wrapper. See:

- Custom form field - Possible to handle multiple files (see [FileField](./forms/fields.py))
- Custom widget - see [FileInput](./forms/widgets.py)
- Custom ModelForm handling these fields (see [FileModelForm](./forms/base_forms.py))
- Custom route to serve file -> Enable to check rights before serving a file (see [FileServingView](./views/file_views.py))
- Custom logic to delete a file [`post_delete_model_file`](./file_helpers.py)s

### I.2.c Submittable model

- A model mixin with `submitted` and `date_submitted` fields.
- The `submitted` boolean indicates whether the model has been effectively submitted by the user.
- It should be used together with `SubmittableModelForm` and `SubmittableModelFormMixin`.
- This mixin is used in 2 ways:
  - It enables a kind of "draft" state where the workflow logic is not executed until the model is actually submitted.
  - We prompt a confirmation form when the user submits the form. The default is to prompt the same form, deactivated. One can display a different view for confirmation by using the `submit_url` method.

## I.3 Submission object

### I.3.a Submission Workflow

<ol>
    <li>
        The author "submits" a submission.
    </li>
    <li>
        <i>[OPTIONAL]</i> An editor is assigned to the submission.
    </li>
    <li>
        The assigned editor Starts the review process or makes an editorial decision:  Accept submission, Decline submission, Request Revisions
        <br>
        If the editor choses to start the review process, proceed to next step.
        <br>
        If the editor chose to request revisions, proceed to step 7.
        <br>
        Else proceed to step 9.
    </li>
    <li>
        Case <b><i>Send to review -</i></b> The editor invites reviewers to perform a peer-review of the submission.
    </li>
    <li>
        Reviewers accept or decline the reviews... other reviewers can be invited ... Some reviews are performed.
    </li>
    <li>
        The editor makes an editorial decision (this can be done anytime): Request revisions, Accept submission, Decline submission.
        If Request revisions, proceed to next step, else proceed to step 9
    </li>
    <li>
        Case <b><i>Request revisions -</i></b> The author is invited to submit a new version of his submission, that means performing the requested revisions and submitting back all his submission files (the revised ones and the other non-modified ones).
    </li>
    <li>
        Go back to step <b>3</b>.
    </li>
    <li>
        Case <b><i>Accept/Decline submission -</i></b> The review process is now over and the submission should be sent to copyediting which is outside the scope of MESH v1.
    </li>
</ol>



The following schema illustrates the available statuses of a submission and the user actions leading to a status change ([mesh_submissions_statuses.jpg](./doc/mesh_submission_statuses.svg)):

![Mesh submission states](./doc/mesh_submission_statuses.jpg)

### 1.3.b Submission models

#### Submission model

- This only contains the submission's metadata.
- This is the central model of the application: all other models described here are somehow related to a submission.

#### SubmissionVersion model

- A submission can have an inifinite number of versions.
- Versions contain all the submission files.
- A new version must be submitted by the author when revisions are requested by the editor.
- The author must include all the files of his submission when submitting a new version, even the non-modified ones.

#### Review model

- Reviews are directly linked to a submission version, not a submission.
- We assume there can be max. 1 round of review per submission version. Thus there's no Round model, everything is handled at the Submission version level. And a version number also determines the review round of the submission.
- A review's availability is managed by the boolean `review_open` of the related submission version: if `review_open is False`, a review cannot be interacted with.
- A submission version is automatically set to `review_open = False` by default and when any editorial decision is made but __Send to review__ (which purposes is exactly to set `review_open = True`).

#### EditorialDecision model

- An editorial decision is one of the following:
    - Request revisions
    - Action that closes the submission: Accept submissio | Decline submission | Re-submit elsewhere | ...
- Editors to attach data to it, either a comment or a file to explain the made decision.

#### SubmissionLog model

- This implements a log of all relevant events for a submission.
- It logs a message in French & English. \
    *__TODO__*: Adapt it so that we store a message code to be translated when it's actually consulted. OR really store French + English (right now it uses gettext which might result in en + en).
- It logs the user that performed the action resulting in the log.
- It also logs whether the user was impersonated by someone else while performing the action (there is code linked to the __Impersonate mode__).
- The log is used to compute the "last activity" on a submission. It's computed as the last log flagged as `significant=True`.


### 1.3.c Submission models & business logic

I tried to stricly separate the "business" logic from the view code when possible and with the amount of available time.

I think this is a very important approach when considering a to-grow application. The view code should be minimal and be restricted to selecting the object(s) queried, applying business logic function to it and then preparing the result for display/presentation. \
The main benefit from this is clearer code & ease in testing (you don't have to always test a full view).

I tend to relate to the fat models/querysets django paragidm where most of the logic is stored in a model or its queryset, and the code interfacing with many different components is located in helper functions.

As exemples, one can check the `submit()`, `start_review_process()` and `apply_editorial_decision()` methods of the [`Submission`](./models/submission_models.py) model. They are methods responsible for updating the submission or its current version, and adding a log entry.


Additionally, the code has been logically splitted everytime needed to ease development and stick to a maximum of ~1000 lines of code per file (must do!). For example, models, views and forms aren't a single file but a full-fledged folder/package so that the logic is correctly splitted between separate files...


# II. Noteworthy  features
## II.1 Roles & Rights

- An user can have multiple roles (author, reviewer, journal manager)
- Active/available roles are dynamically computed for each HTTP request going through our [`RoleMixin`](./mixins.py), using the [`RoleHandler`](./roles/role_handler.py) object.
- An user always have 1 and only 1 current role, that is stored in the User table. The user's current role doesn't change except on explicit (switch role form clicked in the UI) or implicit (see [`RoleMixin`](./mixins.py)) role change.
- The UI is highly determined from the user current role.
- All rights are handled outside of Django permission system and completely role dependent. They are implemented at the role level (see [`Role` and `RoleRights` classes](./roles/base_role.py)).


### RoleMixin - ~ Role middleware

- Used as a middleware for almost every request going through a mesh route.
- Handles all the logic involving navigation and role restriction:
    - Initialize the [RoleHandler](./roles/role_handler.py) for the HTTP request.
    - Restrict and/or force the user role according to the view.


### Roles

Each role is a distinct file in the [roles](./roles/) folder, that implements the [`Role`](./roles/base_role.py) abstract interface. Each role has an attached [`RoleRights`](./roles/base_role.py) object obviously implementing the role rights.

The implemented roles are:

- __Author__ - Implements the author role and rights.
- __Reviewer__ - Implements the reviewer role and rights.
- __Editor__ - Implements the Editor role and rights. \
    An editor can manage a submission when the editor is in charge of the submission's section OR when the editor is assigned to the submission.
- __Journal manager__ - Implements the Journal manager role and rights. \
    The journal manager and the editor have the same rights over a submission. The only distinction is that the journal manager has access to all submissions when the editor has restricted access to its available submissions.

The current role can be switched anytime using the user menu (only if multiple roles are "active").


### Role Rights

A [`RoleRights`](./roles/base_role.py) is primarily a set of methods containing the logic about the role. These methods are used to:

- Retrieve role dependent data - Ex: the list of available submissions to the current role, the config of the submission list page for the current role, ...
- Check a specific right - These right functions are all named in the form `can_{something}` and return a boolean. They are used to limit the user role's access throughout the application.

A role right method should usually be implemented by all role (most methods are used to restrict access to a view so they can be called by any role.).

Basically, all these methods have usually default in the abstract class `RoleRights` and only the specific implementation is implemented in the dedicated role rights.

### Model interfaces

- See [models_interface.py](./roles/models_interface.py)
- Defines an "interface" for a model when some properties of the instance require to be adapted accorded to the user role rights.
- To be used for template display - it adapts a Model instance to the current user role rights in a seemless way (all attributes are adapted to the user role rights)


## II.2 Impersonating (Login as)

- User with sufficient rights can impersonate (login as) a specific set of users.
- Impersonating is done by a custom middleware replacing the user in the Django HTTP request right after the authentication middleware (see [`middleware.py`](./middleware.py)).
- All actions available to the impersonated user are available to the user using the impersonate mode.
- An impersonate session is valid for a short period of time (currently 3 hours).
- History/Log actions also log the actual user using the impersonate mode when active.
- __IMPORTANT -__ Switching role while in impersonate mode does not change the target user's role in DB. This would otherwise result in really buggy behavior for the impersonated user.

## II.3 Token based authentification

- There is a custom authentication backend along with a dedicated view to login with a token in the URL's querystring. See `UserToken` (model), `TokenBackend` (logic) and `TokenLoginView` (initiates token authentication).
- Token authentication is restricted according to the user role data.
- Unfortunately, we cannot use django-allauth rate limit mechanism to protect the view from basic brute force attacks because it's designed only for _POST_ requests... See django-ratelimit if a rate-limit mechanism is required (although it uses a restrictive subset of available cache systems..).


## II.4 User invitation & management (TODO)

### II.4.a User accounts

The application requires development regarding inviting a new user and managing a user account.

A lot of logic is available from django-allauth.

### II.4.b User invitation

One side note regarding invitations: I strongly recommend developing a custom invitation system and not using the django-invitations library.
This library comes with an Invitation model to invite new users and to enable them to signup using the django-allauth signup view. \
__However__, this is quite limited because inviting an user using the library does not create an User object in DB to work with.

The current dirty way to enable "custom" invitations is the following:
- We can easily create a new user and related e-mail address manually (see the `create_new_user` method of [`views.utils.py`](./views/utils.py)).
- The created user cannot login using credentials as long as
    - 1. No password have been set -> _A password can be set using the reset password view or the set/change password view in the user menu._
    - 2. The e-mail address has not been verified -> _An e-mail is automatically sent the first time an user tries to login with correct credentials._

A desirable solution would be to create a system that generates a custom link to ease the process of the 1. and 2. above steps (just as implemented in the invitation module using an invite link).
It's not very difficult - it mainly implies generating a custom route with a robust unique token to be linked to an e-mail address - but there are security concerns.


## II.5 View components

I tried to create UI "components" in a front-end framework fashion. One component is an HTML template, a python file with a `dataclass` representing the component and an optional CSS file.

The goal is to provide an interface for the object to be rendered in the template so that it's easy to re-employ it anywhere in the application.
As an example, the [`Button`](./views/components/button.py) component (to be rendered with [button.html](./templates/mesh/components/button.html)) is used in many places throughout the app

Please note that the rendering is not automated, ie. one must include explicitely the HTML template with the correct context.
We could imagine providing an auto-rendering system where a `Button` instance is automatically rendered with the `button.html` when writing something like `{{ button }}` in a template, in the same way widgets are handled.


## II.6 Files handling (TODO)

As stated before, there has been quite some work about the file handling. There still misses quite a fair amount of development to develop an efficient file widget and form field with its related javascript.


## II.7. Generic filters

Automatic filtering system developped based on:

- Filters config - A JSON like object (dict) specifying for each filter how to retrieve the value to filter on.
- Query parameters - Activating a filter adds a query parameter with selected value to the URL and navigate to the obtained URL. The filtering system gets the filters' state by parsing the provided `QueryDict`.
- Handled filter types - String, int, model.

The code is located in the [`filters.py`](./filters.py) file. The only use (as of 23-11-2023) is for the submission list view ([`SubmissionListView`](./views/submission_views.py)).


## II.8 E-mail templates (TODO)

There is no such thing as an e-mail template system as of 23-11-2023. Nevertheless the referee request e-mail can be freely edited before being sent ([`referee_request.html` file](./templates/mesh/emails/review/referee_request.html)).

The main issue is that there is data that should be hidden from the user or that are only available after the submit of the form. The trick is to render the e-mail twice:

- The e-mail is rendered a first time to display to the user in a CKEditor. The first rendered e-mail will still contain placeholders to be filled by variables in the templating language syntax (usually `{{ MY_VARIABLE }}` ). This is directly achieved by using the `{% verbatim %}{{ MY_VARIABLE }}{% endverbatim %}` tag.
- When the form is submitted, the e-mail is re-rendered using django engine to fill those placeholders.


At least 2 unanswered issues:

- How to handle conditionnal content that should be rendered in the last rendering, not the first. Maybe this should not be allowed.
- A safety system so that user entered strings like `{{ GOT_YOU }}` doesn't get treated during the re-rendering ?


# III. Django settings

The Django application has the following settings (their list & code is available in [`app_settings.py`](./app_settings.py)). They should be prefixed with `MESH_` when declaring them in `settings.py`.

- `ENABLED_ROLES` - List of role codes enabled for the website. Defaults to `["author", "reviewer", "editor", "journal_manager"]`.

- `BLIND_MODE` - The blind mode of the application. To be selected among `no_blind`, `single_blind` and `double_blind`. Defaults to `single_blind`.

- `FILES_DIRECTORY` [MANDATORY] - Directory used to store all user files. Files are served using django-sendfile package (XSendFile with Apache). \
This should lie outside of Django directory. \
The directory is automatically created on startup if required.

- `USER_TOKEN_EXPIRATION_DAYS` - Expiration time (in days) for an user token from its last refresh date.

- `EMAIL_PREFIX` - Prefix for every e-mail manually sent by the MESH application. Defaults to `[MESH] `.
