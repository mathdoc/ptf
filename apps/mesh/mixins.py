from typing import Any

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import Http404
from django.http import HttpRequest
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _

from .exceptions import RoleException

# from .forms.role_forms import RoleSelectForm
from .interfaces.user_interfaces import ImpersonateData
from .roles.base_role import Role
from .roles.role_handler import ROLE_HANDLER_CACHE
from .roles.role_handler import RoleHandler

ROLE_SWITCH_QUERY_PARAM = "_role_switched"


class BaseRoleMixin:
    """
    Mixin responsible for instantiating a RoleHandler for the current view.
    It should not been used directly. Use the `RoleMixin` instead.

    Additionally, it contains the logic involving navigation and role restriction:
        - force the user role if the route is role protected.
        - restrict dispatch on hookable conditions (cf. `RoleMixin.restrict_dispatch`).
    """

    role_handler: RoleHandler
    fail_redirect_uri = reverse_lazy("mesh:home")
    request: HttpRequest
    access_restricted_message = _(
        "Your are not allowed to access the requested content with your current rights."
    )
    message_on_restrict = True
    # Used to restrict the view to users with at least one active role in the list.
    restricted_roles: list[type[Role]] = []

    def dispatch(self, request: HttpRequest, *args, **kwargs):
        assert (
            request.user.is_authenticated
        ), "ERROR: `BaseRoleMixin` should not be used directly. Use `RoleMixin` instead."

        self.request = request

        # The `role_handler` object might have already been derived by a middleware.
        # See `TokenBackend` for more info.
        role_handler_cache = getattr(request, ROLE_HANDLER_CACHE, None)

        if role_handler_cache is None or role_handler_cache.user.pk != request.user.pk:
            self.role_handler = RoleHandler(request.user, request)  # type:ignore
        else:
            self.role_handler: RoleHandler = role_handler_cache
            if self.role_handler.request is None:
                self.role_handler.request = request

        # The obtained role_handler might not be fully initialized.
        self.role_handler.complete_init()

        # Force the user role if the view is role-restricted.
        # /!\ Do not force role if we're performing a role switch as it could be re-changed
        # instantaneously.
        role_switch = request.GET.get(ROLE_SWITCH_QUERY_PARAM, None) == "true"
        if not role_switch:
            self.force_role()

        self.request.user.impersonate_data = ImpersonateData.from_session(self.request.session)

        if self.restrict_dispatch(request, *args, **kwargs):
            # If the role the user is switching to is not allowed access, simply
            # redirect to the redirect uri without adding a message
            # By default, the role switch redirects to the same page but the new role
            # might not have the required rights to access.
            response = HttpResponseRedirect(self.get_fail_redirect_uri())
            if role_switch:
                return response

            # Otherwise the user tried to access unallowed URL.
            if self.message_on_restrict:
                messages.warning(request, self.get_access_restricted_message())
            return response

        return super().dispatch(request, *args, **kwargs)  # type:ignore

    def restrict_dispatch(self, request: HttpRequest, *args, **kwargs) -> bool:
        """
        Custom hook to restrict access to the current view. \\
        When `True`, redirects to `self.get_fail_redirect_uri()`.

        TBD: Maybe we should raise a HTTP 404 error when access is restricted instead of
        redirecting to home.
        """
        return False

    def force_role(self) -> None:
        """
        Switch the current user's role to the first active role matching the view's
        `restricted_roles`.

        Raise an 404 error if there's no matching active role.
        """
        if (
            self.restricted_roles
            and self.role_handler.current_role.__class__ not in self.restricted_roles
        ):
            for role in self.restricted_roles:
                try:
                    self.role_handler.switch_role(role.code())
                except RoleException:
                    continue
                else:
                    return
            raise Http404
        return

    def get_fail_redirect_uri(self) -> str:
        return self.fail_redirect_uri

    def get_context_data(self, *args, **kwargs) -> dict[str, Any]:
        """
        Overloads the default context data with the role handler object.
        """
        if hasattr(super(), "get_context_data"):
            context = super().get_context_data(*args, **kwargs)  # type:ignore
        else:
            context = {}

        context["role_handler"] = self.role_handler

        active_roles = self.role_handler.get_active_roles()
        # choices = [(role.code(), role.name()) for role in active_roles]
        # initials = {"role_code": choices}
        context["available_roles"] = active_roles

        return context

    def get_access_restricted_message(self) -> str:
        return (
            self.access_restricted_message
            + "<br>"
            + f"Current role: {self.role_handler.current_role.name()}"
        )


class RoleMixin(LoginRequiredMixin, BaseRoleMixin):
    """
    Mixin to be used when handling role-related content.

    BaseRoleMixin is irrelevant if the user is not logged in.
    """

    pass
