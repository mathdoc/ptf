from collections.abc import Callable

from django.contrib import messages
from django.core.exceptions import ImproperlyConfigured
from django.http import HttpRequest
from django.http import HttpResponse
from django.utils.translation import gettext_lazy as _

from .interfaces.user_interfaces import ImpersonateData
from .models.user_models import User


def process_impersonate_session(request: HttpRequest) -> None:
    if not isinstance(request.user, User):
        return

    data = ImpersonateData.from_session(request.session)
    if not data:
        return

    if not data.is_valid():
        ImpersonateData.clean_session(request.session)
        messages.warning(
            request,
            _(
                "Your impersonate session has expired. You can restart one using the 'Impersonate user' link from the user menu."
            ),
        )
        return

    try:
        # We don't check the user rights to impersonate here. This is done when
        # initializing the impersonate session.
        # There's a max. duration for the impersonate session that is sufficient to
        # prevent security issues.
        target_user = User.objects.get(pk=data.target_id)

        # TODO: Check the source user has the right to impersonate the target user?
        # We don't bother doing it

        # Replace user objects by the impersonate target
        request.user = target_user

    # Corrupted impersonate data
    except User.DoesNotExist:
        ImpersonateData.clean_session(request.session)


class ImpersonateMiddleware:
    """
    This middleware enables impersonation of users.
    This should be called after the SessionMiddleware and all authentication related
    middlewares.

    This replaces the user object attached to the HTTP request by the targeted user.
    """

    def __init__(self, get_response: Callable[[HttpRequest], HttpResponse]):
        self.get_response = get_response

    def __call__(self, request: HttpRequest) -> HttpResponse:
        if not hasattr(request, "session"):
            raise ImproperlyConfigured(
                "The ImpersonateMiddleware requires session middleware to be installed."
            )
        if not hasattr(request, "user"):
            raise ImproperlyConfigured(
                "The ImpersonateMiddleware requires authentication middleware " "to be installed."
            )
        if not hasattr(request, "_messages"):
            raise ImproperlyConfigured(
                "The ImpersonateMiddleware requires the message middleware " "to be installed."
            )

        process_impersonate_session(request)

        return self.get_response(request)
