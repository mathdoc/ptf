# Generated by Django 4.2.6 on 2023-11-06 10:59

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('mesh', '0005_alter_review_recommendation_usertoken'),
    ]

    operations = [
        migrations.AlterField(
            model_name='submission',
            name='created_by',
            field=models.ForeignKey(blank=True, editable=False, help_text='Automatically filled on save.', on_delete=django.db.models.deletion.PROTECT, related_name='+', to=settings.AUTH_USER_MODEL, verbose_name='Created by'),
        ),
    ]
