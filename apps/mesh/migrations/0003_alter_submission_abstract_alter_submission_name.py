# Generated by Django 4.2.6 on 2023-10-20 15:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mesh', '0002_alter_submission_container_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='submission',
            name='abstract',
            field=models.TextField(verbose_name='Abstract'),
        ),
        migrations.AlterField(
            model_name='submission',
            name='name',
            field=models.TextField(verbose_name='Title'),
        ),
    ]
