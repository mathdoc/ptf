from __future__ import annotations

from unittest.mock import Mock
from unittest.mock import patch

from ...filters import Filter
from ...filters import FilterSet
from ...interfaces.submission_interfaces import SubmissionListConfig
from ...interfaces.submission_interfaces import SubmissionStatus
from ...models.factories import JournalSectionFactory
from ...models.factories import ReviewFactory
from ...models.factories import SubmissionFactory
from ...models.factories import SubmissionVersionFactory
from ...models.factories import UserFactory
from ...models.journal_models import JournalSection
from ...models.submission_models import Submission
from ...models.submission_models import SubmissionState
from ...models.user_models import User
from ...roles.journal_manager import JournalManagerRights
from ...roles.role_handler import RoleHandler
from ...views import submission_views
from ...views.components.review_summary import CountWithTotal
from ...views.components.review_summary import ReviewSummary
from ...views.components.review_summary import build_review_summary

# from ...views.submission_views import group_submissions_per_status
# from ...views.submission_views import prepare_submissions_lists
from ..base_test_case import BaseTestCase

submission_todo_1: Submission | None = None
submission_todo_2: Submission | None = None
submission_waiting_1: Submission | None = None
submission_waiting_2: Submission | None = None
submission_waiting_3: Submission | None = None
submission_archived_1: Submission | None = None
submission_error_1: Submission | None = None
journal_section: JournalSection | None = None


def create_global_submissions():
    global submission_todo_1
    global submission_todo_2
    global submission_waiting_1
    global submission_waiting_2
    global submission_waiting_3
    global submission_archived_1
    global submission_error_1
    global journal_section
    journal_section = JournalSectionFactory.create()
    submission_todo_1 = SubmissionFactory.create(state=SubmissionState.ON_REVIEW.value)
    submission_todo_2 = SubmissionFactory.create(
        state=SubmissionState.REVISION_REQUESTED.value, journal_section=journal_section
    )
    submission_waiting_1 = SubmissionFactory.create(state=SubmissionState.ON_REVIEW.value)
    submission_waiting_2 = SubmissionFactory.create(state=SubmissionState.SUBMITTED)
    submission_waiting_3 = SubmissionFactory.create(state=SubmissionState.ON_REVIEW.value)
    submission_archived_1 = SubmissionFactory.create(
        state=SubmissionState.ON_REVIEW.value, journal_section=journal_section
    )
    submission_error_1 = SubmissionFactory.create(state=SubmissionState.REJECTED.value)

    SubmissionVersionFactory.create(submission=submission_todo_1, submitted=True)
    SubmissionVersionFactory.create(submission=submission_todo_2, submitted=True)
    SubmissionVersionFactory.create(submission=submission_waiting_1, submitted=True)
    SubmissionVersionFactory.create(submission=submission_waiting_2, submitted=True)
    SubmissionVersionFactory.create(submission=submission_waiting_3, submitted=True)
    SubmissionVersionFactory.create(submission=submission_archived_1, submitted=True)
    SubmissionVersionFactory.create(submission=submission_error_1, submitted=True)


def grouped_submissions_per_status(submissions, role_handler):
    return {
        SubmissionStatus.TODO: [submission_todo_1, submission_todo_2],
        SubmissionStatus.WAITING: [
            submission_waiting_1,
            submission_waiting_2,
            submission_waiting_3,
        ],
        SubmissionStatus.ARCHIVED: [submission_archived_1],
        SubmissionStatus.ERROR: [submission_error_1],
    }


def base_submission_list_config():
    return [
        SubmissionListConfig(
            key=SubmissionStatus.TODO, title="Requires action", html_classes="todo"
        ),
        SubmissionListConfig(
            key=SubmissionStatus.WAITING,
            title="Waiting for other's input",
            html_classes="waiting",
        ),
        SubmissionListConfig(
            key=SubmissionStatus.ARCHIVED, title="Closed / Archived", html_classes="archived"
        ),
    ]


class SubmissionViewsTestCase(BaseTestCase):
    class_used_models = [Submission, JournalSection, User]

    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.user = UserFactory.create(journal_manager=True)
        create_global_submissions()

    def test_group_submissions_per_status(self):
        submissions = Submission.objects.get_submissions().all()
        role_handler = RoleHandler(self.user)

        grouped_submissions = submission_views.group_submissions_per_status(
            submissions, role_handler
        )
        self.assertEqual(len(grouped_submissions), 3)

        group = grouped_submissions[SubmissionStatus.TODO]
        self.assertEqual(len(group), 5)

        group = grouped_submissions[SubmissionStatus.WAITING]
        self.assertEqual(len(group), 1)

        group = grouped_submissions[SubmissionStatus.ARCHIVED]
        self.assertEqual(len(group), 1)

    @patch.object(submission_views, "group_submissions_per_status", grouped_submissions_per_status)
    @patch.object(
        JournalManagerRights,
        "get_submission_list_config",
        Mock(return_value=base_submission_list_config()),
    )
    def test_submissions_list_grouping(self):
        role_handler = RoleHandler(self.user)
        request = self.dummy_request("/")

        context = submission_views.prepare_submissions_lists(role_handler, request)

        submissions_config = context["submissions_config"]
        self.assertEqual(len(submissions_config), 3)

        config: SubmissionListConfig = submissions_config[0]
        self.assertEqual(config.key, SubmissionStatus.TODO)
        self.assertEqual(len(config.all_submissions), 2)
        self.assertEqual(len(config.submissions), 2)

        config: SubmissionListConfig = submissions_config[1]
        self.assertEqual(config.key, SubmissionStatus.WAITING)
        self.assertEqual(len(config.all_submissions), 3)
        self.assertEqual(len(config.submissions), 3)

        config: SubmissionListConfig = submissions_config[2]
        self.assertEqual(config.key, SubmissionStatus.ARCHIVED)
        self.assertEqual(len(config.all_submissions), 1)
        self.assertEqual(len(config.submissions), 1)

        submission_count = context["submission_count"]
        self.assertEqual(submission_count, CountWithTotal(value=6, total=6))

    @patch.object(submission_views, "group_submissions_per_status", grouped_submissions_per_status)
    @patch.object(
        JournalManagerRights,
        "get_submission_list_config",
        Mock(return_value=base_submission_list_config()),
    )
    def test_submissions_list_filtering(self):
        role_handler = RoleHandler(self.user)
        # Test single filter
        query_params = {"_filter_state": [SubmissionState.ON_REVIEW.value]}
        request = self.request_factory.get("/", data=query_params)
        context = submission_views.prepare_submissions_lists(role_handler, request)

        submissions_config = context["submissions_config"]
        self.assertEqual(len(submissions_config), 3)

        config: SubmissionListConfig = submissions_config[0]
        self.assertEqual(config.key, SubmissionStatus.TODO)
        self.assertEqual(len(config.all_submissions), 2)
        self.assertEqual(len(config.submissions), 1)

        config: SubmissionListConfig = submissions_config[1]
        self.assertEqual(config.key, SubmissionStatus.WAITING)
        self.assertEqual(len(config.all_submissions), 3)
        self.assertEqual(len(config.submissions), 2)

        config: SubmissionListConfig = submissions_config[2]
        self.assertEqual(config.key, SubmissionStatus.ARCHIVED)
        self.assertEqual(len(config.all_submissions), 1)
        self.assertEqual(len(config.submissions), 1)

        query_params = {"_filter_state": [SubmissionState.REVISION_REQUESTED.value]}
        request = self.request_factory.get("/", data=query_params)
        context = submission_views.prepare_submissions_lists(role_handler, request)

        submissions_config = context["submissions_config"]
        self.assertEqual(len(submissions_config), 3)

        config: SubmissionListConfig = submissions_config[0]
        self.assertEqual(config.key, SubmissionStatus.TODO)
        self.assertEqual(len(config.all_submissions), 2)
        self.assertEqual(len(config.submissions), 1)

        config: SubmissionListConfig = submissions_config[1]
        self.assertEqual(config.key, SubmissionStatus.WAITING)
        self.assertEqual(len(config.all_submissions), 3)
        self.assertEqual(len(config.submissions), 0)

        config: SubmissionListConfig = submissions_config[2]
        self.assertEqual(config.key, SubmissionStatus.ARCHIVED)
        self.assertEqual(len(config.all_submissions), 1)
        self.assertEqual(len(config.submissions), 0)

        # Test single filter with multiple values
        query_params = {
            "_filter_state": [
                SubmissionState.REVISION_REQUESTED.value,
                SubmissionState.ON_REVIEW.value,
            ]
        }
        request = self.request_factory.get("/", data=query_params)
        context = submission_views.prepare_submissions_lists(role_handler, request)

        submissions_config = context["submissions_config"]
        self.assertEqual(len(submissions_config), 3)

        config: SubmissionListConfig = submissions_config[0]
        self.assertEqual(config.key, SubmissionStatus.TODO)
        self.assertEqual(len(config.all_submissions), 2)
        self.assertEqual(len(config.submissions), 2)

        config: SubmissionListConfig = submissions_config[1]
        self.assertEqual(config.key, SubmissionStatus.WAITING)
        self.assertEqual(len(config.all_submissions), 3)
        self.assertEqual(len(config.submissions), 2)

        config: SubmissionListConfig = submissions_config[2]
        self.assertEqual(config.key, SubmissionStatus.ARCHIVED)
        self.assertEqual(len(config.all_submissions), 1)
        self.assertEqual(len(config.submissions), 1)

        # Test multiple filters behaviour
        query_params = {
            "_filter_state": [
                SubmissionState.REVISION_REQUESTED.value,
                SubmissionState.ON_REVIEW.value,
            ],
            "_filter_journal_section": [journal_section.pk],  # type:ignore
        }
        request = self.request_factory.get("/", data=query_params)
        context = submission_views.prepare_submissions_lists(role_handler, request)

        submissions_config = context["submissions_config"]
        self.assertEqual(len(submissions_config), 3)

        config: SubmissionListConfig = submissions_config[0]
        self.assertEqual(config.key, SubmissionStatus.TODO)
        self.assertEqual(len(config.all_submissions), 2)
        self.assertEqual(len(config.submissions), 1)

        config: SubmissionListConfig = submissions_config[1]
        self.assertEqual(config.key, SubmissionStatus.WAITING)
        self.assertEqual(len(config.all_submissions), 3)
        self.assertEqual(len(config.submissions), 0)

        config: SubmissionListConfig = submissions_config[2]
        self.assertEqual(config.key, SubmissionStatus.ARCHIVED)
        self.assertEqual(len(config.all_submissions), 1)
        self.assertEqual(len(config.submissions), 1)

        filters: FilterSet = context["filters"]
        filter: Filter = filters.get_filter("state")  # type:ignore
        self.assertEqual(len(filter.values), 3)
        self.assertEqual(len(filter.active_values), 2)

    @patch.object(submission_views, "group_submissions_per_status", grouped_submissions_per_status)
    def test_submissions_list_filtering_settings(self):
        role_handler = RoleHandler(self.user)
        # Test single filter
        query_params = {"_filter_state": [SubmissionState.ON_REVIEW.value]}
        request = self.request_factory.get("/", data=query_params)

        with patch.object(
            JournalManagerRights,
            "get_submission_list_config",
            Mock(return_value=base_submission_list_config()),
        ):
            context = submission_views.prepare_submissions_lists(role_handler, request)

        filters: FilterSet = context["filters"]
        filter: Filter = filters.get_filter("state")  # type:ignore
        self.assertEqual(len(filter.values), 3)

        base_config = base_submission_list_config()
        base_config[0].in_filters = False

        with patch.object(
            JournalManagerRights,
            "get_submission_list_config",
            Mock(return_value=base_config),
        ):
            context = submission_views.prepare_submissions_lists(role_handler, request)

        filters: FilterSet = context["filters"]
        filter: Filter = filters.get_filter("state")  # type:ignore
        self.assertEqual(len(filter.values), 2)

        submissions_config = context["submissions_config"]

        config: SubmissionListConfig = submissions_config[0]
        self.assertEqual(config.key, SubmissionStatus.TODO)
        self.assertEqual(len(config.all_submissions), 2)
        self.assertEqual(len(config.submissions), 2)

        config: SubmissionListConfig = submissions_config[1]
        self.assertEqual(config.key, SubmissionStatus.WAITING)
        self.assertEqual(len(config.all_submissions), 3)
        self.assertEqual(len(config.submissions), 2)

        base_config = base_submission_list_config()
        base_config[0].display = False

        submission_count = context["submission_count"]
        self.assertEqual(submission_count, CountWithTotal(value=3, total=4))

        with patch.object(
            JournalManagerRights,
            "get_submission_list_config",
            Mock(return_value=base_config),
        ):
            context = submission_views.prepare_submissions_lists(role_handler, request)

        filters: FilterSet = context["filters"]
        filter: Filter = filters.get_filter("state")  # type:ignore
        self.assertEqual(len(filter.values), 3)

        submissions_config = context["submissions_config"]

        config: SubmissionListConfig = submissions_config[0]
        self.assertEqual(config.key, SubmissionStatus.TODO)
        self.assertEqual(len(config.all_submissions), 2)
        self.assertEqual(len(config.submissions), 0)

        config: SubmissionListConfig = submissions_config[1]
        self.assertEqual(config.key, SubmissionStatus.WAITING)
        self.assertEqual(len(config.all_submissions), 3)
        self.assertEqual(len(config.submissions), 2)

    def test_review_summary(self):
        submission = SubmissionFactory.create()
        review_summary = build_review_summary(submission)
        self.assertEqual(review_summary.current_version, 0)

        version = SubmissionVersionFactory.create(submission=submission)
        submission = Submission.objects.get(pk=submission.pk)

        review_summary = build_review_summary(submission)
        expected = ReviewSummary(
            current_version=1,
            reviewers_count=CountWithTotal(value=0, total=0),
            reports_count=CountWithTotal(value=0, total=0),
        )
        self.assertEqual(expected, review_summary)

        ReviewFactory.create(version=version)
        submission = Submission.objects.get(pk=submission.pk)

        review_summary = build_review_summary(submission)
        expected = ReviewSummary(
            current_version=1,
            reviewers_count=CountWithTotal(value=0, total=1),
            reports_count=CountWithTotal(value=0, total=0),
        )
        self.assertEqual(expected, review_summary)

        version.submitted = True
        version.save()

        version_2 = SubmissionVersionFactory.create(submission=submission)
        submission = Submission.objects.get(pk=submission.pk)

        review_summary = build_review_summary(submission)
        expected = ReviewSummary(
            current_version=2,
            reviewers_count=CountWithTotal(value=0, total=0),
            reports_count=CountWithTotal(value=0, total=0),
        )
        self.assertEqual(expected, review_summary)

        ReviewFactory.create(version=version_2, accepted=False)
        ReviewFactory.create(version=version_2)
        ReviewFactory.create(version=version_2, accepted=True)
        ReviewFactory.create(version=version_2, accepted=True, submitted=True)
        submission = Submission.objects.get(pk=submission.pk)

        review_summary = build_review_summary(submission)
        expected = ReviewSummary(
            current_version=2,
            reviewers_count=CountWithTotal(value=3, total=4),
            reports_count=CountWithTotal(value=1, total=2),
        )
        self.assertEqual(expected, review_summary)
