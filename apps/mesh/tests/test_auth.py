from unittest.mock import patch

from ..auth import TokenBackend
from ..models.factories import UserFactory
from ..models.user_models import User
from ..models.user_models import UserToken
from ..roles.role_handler import ROLE_HANDLER_CACHE
from ..roles.role_handler import RoleHandler
from .base_test_case import BaseTestCase


class TokenBackendTestCase(BaseTestCase):
    used_models = [UserToken, User]

    def setUp(self) -> None:
        super().setUp()
        self.user_base = UserFactory.create()
        self.user_base_token = UserToken.get_token(self.user_base)
        self.request = self.dummy_request()
        self.backend = TokenBackend()

    def test_no_token(self):
        UserToken.objects.all().delete()
        self.assertFalse(UserToken.objects.exists())

        token_key = "my_token"
        user = self.backend.authenticate(self.request, token=token_key)

        self.assertIsNone(user)

    def test_base_user(self):
        user = self.backend.authenticate(self.request, token=self.user_base_token.key)
        self.assertEqual(user.pk, self.user_base.pk)  # type:ignore

        # Test the caching of the role_handler
        role_handler_cache = getattr(self.request, ROLE_HANDLER_CACHE)
        self.assertTrue(isinstance(role_handler_cache, RoleHandler))
        self.assertEqual(role_handler_cache.user.pk, self.user_base.pk)

    def test_staff_user(self):
        self.user_base.is_staff = True
        self.user_base.save()

        user = self.backend.authenticate(self.request, token=self.user_base_token.key)
        self.assertIsNone(user)

    def test_admin_user(self):
        self.user_base.is_superuser = True
        self.user_base.save()

        user = self.backend.authenticate(self.request, token=self.user_base_token.key)
        self.assertIsNone(user)

    def test_journal_manager_user(self):
        self.user_base.journal_manager = True
        self.user_base.save()

        user = self.backend.authenticate(self.request, token=self.user_base_token.key)
        self.assertIsNone(user)

    def test_role_unauthorized_user(self):
        with patch.object(RoleHandler, "token_authentication_allowed") as mock_method:
            mock_method.return_value = True

            user = self.backend.authenticate(self.request, token=self.user_base_token.key)
            self.assertEqual(user.pk, self.user_base.pk)  # type:ignore

        with patch.object(RoleHandler, "token_authentication_allowed") as mock_method:
            mock_method.return_value = False

            user = self.backend.authenticate(self.request, token=self.user_base_token.key)
            self.assertIsNone(user)
