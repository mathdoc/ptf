from datetime import datetime
from datetime import timedelta

import pytest

from django.contrib.sessions.backends.base import SessionBase
from django.contrib.sessions.middleware import SessionMiddleware

from ...interfaces.user_interfaces import ImpersonateData
from ...interfaces.user_interfaces import UserInfo
from ...models.factories import UserFactory

middleware = SessionMiddleware(lambda x: x)  # type:ignore


def get_dummy_session() -> SessionBase:
    return middleware.SessionStore(None)


def test_user_info():
    user = UserFactory.build()
    user_info = UserInfo.from_user(user)

    assert user.pk == user_info.pk
    assert user.first_name == user_info.first_name
    assert user.last_name == user_info.last_name
    assert user.email == user_info.email


@pytest.mark.django_db  # We need DB for session saving..
def test_impersonate_data():
    session = get_dummy_session()
    data = ImpersonateData.from_session(session)
    assert data is None

    user = UserFactory.build()
    user_info = UserInfo.from_user(user)
    data = ImpersonateData(source=user_info, target_id=10)

    assert data.is_valid() is True
    data.serialize(session)

    data_from_session = ImpersonateData.from_session(session)
    assert data is not None
    assert data == data_from_session
    assert data.source == data_from_session.source  # type:ignore

    ImpersonateData.clean_session(session)
    data_from_session = ImpersonateData.from_session(session)
    assert data_from_session is None

    data.timestamp_start = (datetime.utcnow() - timedelta(days=100)).timestamp()
    assert data.is_valid() is False
