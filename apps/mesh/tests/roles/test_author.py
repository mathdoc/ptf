from ...interfaces.submission_interfaces import SubmissionStatus
from ...models.factories import ReviewFactory
from ...models.factories import SubmissionAuthorFactory
from ...models.factories import SubmissionFactory
from ...models.factories import SubmissionVersionFactory
from ...models.factories import UserFactory
from ...models.review_models import RecommendationValue
from ...models.review_models import Review
from ...models.review_models import ReviewAdditionalFile
from ...models.submission_models import Submission
from ...models.submission_models import SubmissionMainFile
from ...models.submission_models import SubmissionState
from ...models.user_models import User
from ...roles.author import Author
from ...roles.author import AuthorRights
from ..base_test_case import BaseTestCase


class AuthorTestCase(BaseTestCase):
    used_models = [Review, Submission]
    class_used_models = [User]

    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.user_author = UserFactory.create()
        cls.user_reviewer = UserFactory.create()

    def test_always_active(self):
        author = Author(self.user_author)
        self.assertTrue(author.active)

    def test_submissions(self):
        submission_1 = SubmissionFactory.create(created_by=self.user_author)
        submission_2 = SubmissionFactory.create()
        submission_3 = SubmissionFactory.create(created_by=self.user_author)
        author = Author(self.user_author)

        submissions = author.rights.submissions
        self.assertEqual(len(submissions), 2)
        self.assertIn(submission_1, submissions)
        self.assertIn(submission_3, submissions)

        rights = author.rights
        self.assertTrue(rights.can_access_submission(submission_1))
        self.assertFalse(rights.can_access_submission(submission_2))
        self.assertTrue(rights.can_access_submission(submission_3))

    def test_can_create_version(self):
        submission = SubmissionFactory.create(created_by=self.user_author, author_agreement=True)
        rights = AuthorRights(self.user_author)

        # New submission without required author
        self.assertFalse(rights.can_create_version(submission))

        # New submission with 1 author
        SubmissionAuthorFactory.create(submission=submission)
        submission = Submission.objects.get(pk=submission.pk)
        self.assertTrue(rights.can_create_version(submission))

        # New submission with a 1st version
        version = SubmissionVersionFactory.create(submission=submission)
        SubmissionMainFile.objects.create(file=self.dummy_file, attached_to=version)
        submission = Submission.objects.get(pk=submission.pk)
        self.assertFalse(rights.can_create_version(submission))

        # Submitted submission
        request = self.dummy_request()
        request.user = self.user_author
        submission.submit(self.user_author)
        submission = Submission.objects.get(pk=submission.pk)
        self.assertFalse(rights.can_create_version(submission))

        # Under review submission
        submission.state = SubmissionState.ON_REVIEW.value
        submission.save()
        submission = Submission.objects.get(pk=submission.pk)
        self.assertFalse(rights.can_create_version(submission))

        # Revisions requested submission
        submission.state = SubmissionState.REVISION_REQUESTED.value
        submission.save()
        submission = Submission.objects.get(pk=submission.pk)
        self.assertTrue(rights.can_create_version(submission))

        # Revisions requested submission w/ a non-submitted current version
        version_2 = SubmissionVersionFactory.create(submission=submission)
        SubmissionMainFile.objects.create(file=self.dummy_file, attached_to=version_2)
        submission = Submission.objects.get(pk=submission.pk)
        self.assertFalse(rights.can_create_version(submission))

        # Revisions submitted submission
        submission.submit(self.user_author)
        submission = Submission.objects.get(pk=submission.pk)
        self.assertFalse(rights.can_create_version(submission))

    def test_can_access_review(self):
        submission = SubmissionFactory.create(created_by=self.user_author, author_agreement=True)
        SubmissionAuthorFactory.create(submission=submission)
        version = SubmissionVersionFactory.create(submission=submission, review_open=True)
        SubmissionMainFile.objects.create(file=self.dummy_file, attached_to=version)
        submission = Submission.objects.get(pk=submission.pk)
        request = self.dummy_request()
        request.user = self.user_author
        submission.submit(self.user_author)

        rights = AuthorRights(self.user_author)

        submission = Submission.objects.get(pk=submission.pk)
        self.assertFalse(rights.can_access_reviews(submission.current_version))  # type:ignore

        version.review_open = False
        version.save()

        submission = Submission.objects.get(pk=submission.pk)
        self.assertTrue(rights.can_access_reviews(submission.current_version))  # type:ignore

        review = ReviewFactory.create(
            reviewer=self.user_reviewer,
            version=version,
            accepted=True,
            recommendation=RecommendationValue.REVISION_REQUESTED.value,
        )
        review_file = ReviewAdditionalFile.objects.create(file=self.dummy_file, attached_to=review)
        self.assertFalse(rights.can_access_review(review))
        self.assertFalse(rights.can_access_review_file(review_file))

        review.submitted = True
        review.save()
        review = Review.objects.get(pk=review.pk)
        self.assertFalse(rights.can_access_review_file(review_file))
        self.assertFalse(rights.can_access_review(review))

        review_file.author_access = True
        review_file.save()
        review = Review.objects.get(pk=review.pk)
        self.assertTrue(rights.can_access_review_file(review_file))
        self.assertTrue(rights.can_access_review(review))

    def test_submission_status(self):
        """
        The SubmissionStatus only depends on the submission state for the author role.
        """
        submission = SubmissionFactory.create(created_by=self.user_author, author_agreement=True)
        submission_other = SubmissionFactory.create(author_agreement=True)
        rights = AuthorRights(self.user_author)

        status = rights.get_submission_status(submission_other)
        self.assertEqual(status.status, SubmissionStatus.ERROR)

        status = rights.get_submission_status(submission)
        self.assertEqual(status.status, SubmissionStatus.TODO)

        submission.state = SubmissionState.SUBMITTED.value
        status = rights.get_submission_status(submission)
        self.assertEqual(status.status, SubmissionStatus.WAITING)

        submission.state = SubmissionState.ON_REVIEW.value
        status = rights.get_submission_status(submission)
        self.assertEqual(status.status, SubmissionStatus.WAITING)

        submission.state = SubmissionState.REVISION_REQUESTED.value
        status = rights.get_submission_status(submission)
        self.assertEqual(status.status, SubmissionStatus.TODO)

        submission.state = SubmissionState.REVISION_SUBMITTED.value
        status = rights.get_submission_status(submission)
        self.assertEqual(status.status, SubmissionStatus.WAITING)

        submission.state = SubmissionState.ACCEPTED.value
        status = rights.get_submission_status(submission)
        self.assertEqual(status.status, SubmissionStatus.ARCHIVED)

        submission.state = SubmissionState.REJECTED.value
        status = rights.get_submission_status(submission)
        self.assertEqual(status.status, SubmissionStatus.ARCHIVED)
