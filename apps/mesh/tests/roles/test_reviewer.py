from django.test import override_settings

from ...app_settings import BlindMode
from ...interfaces.submission_interfaces import SubmissionStatus
from ...models.factories import ReviewFactory
from ...models.factories import SubmissionFactory
from ...models.factories import SubmissionVersionFactory
from ...models.factories import UserFactory
from ...models.review_models import Review
from ...models.submission_models import Submission
from ...models.submission_models import SubmissionState
from ...models.user_models import User
from ...roles.reviewer import Reviewer
from ...roles.reviewer import ReviewerRights
from ..base_test_case import BaseTestCase


class ReviewerTestCase(BaseTestCase):
    used_models = [Review, Submission]
    class_used_models = [User]

    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.user_author = UserFactory.create()
        cls.user_reviewer = UserFactory.create()

    def test_submissions(self):
        submission_1 = SubmissionFactory.create(created_by=self.user_author)
        version_1 = SubmissionVersionFactory.create(submission=submission_1)
        submission_2 = SubmissionFactory.create(created_by=self.user_author)
        version_2 = SubmissionVersionFactory.create(submission=submission_2)
        submission_3 = SubmissionFactory.create(created_by=self.user_reviewer)
        version_3 = SubmissionVersionFactory.create(submission=submission_3)

        review_1 = ReviewFactory.create(version=version_1, reviewer=self.user_reviewer)
        review_2 = ReviewFactory.create(version=version_2, reviewer=self.user_reviewer)
        review_3 = ReviewFactory.create(version=version_3)

        role = Reviewer(self.user_reviewer)
        self.assertTrue(role.active)

        rights = role.rights
        self.assertEqual(len(rights.submissions), 2)
        self.assertIn(submission_1, rights.submissions)
        self.assertIn(submission_2, rights.submissions)

        self.assertEqual(len(rights.reviews), 2)
        self.assertIn(review_1, rights.reviews)
        self.assertIn(review_2, rights.reviews)

        self.assertTrue(rights.can_access_submission(submission_1))
        self.assertTrue(rights.can_access_submission(submission_2))
        self.assertFalse(rights.can_access_submission(submission_3))

        self.assertTrue(rights.can_access_version(version_1))
        self.assertTrue(rights.can_access_version(version_2))
        self.assertFalse(rights.can_access_version(version_3))

        self.assertTrue(rights.can_access_review(review_1))
        self.assertTrue(rights.can_access_review(review_2))
        self.assertFalse(rights.can_access_review(review_3))

    def test_get_current_open_review(self):
        submission = SubmissionFactory.create(created_by=self.user_author)
        version_1 = SubmissionVersionFactory.create(
            submission=submission, submitted=True, review_open=True
        )

        review_1 = ReviewFactory.create(reviewer=self.user_reviewer, version=version_1)

        rights = ReviewerRights(self.user_reviewer)
        self.assertEqual(review_1, rights.get_current_open_review(version_1))

        version_1.review_open = False
        version_1.save()
        rights = ReviewerRights(self.user_reviewer)
        self.assertIsNone(rights.get_current_open_review(version_1))

        # Refresh cache
        submission = Submission.objects.get(pk=submission.pk)
        version_2 = SubmissionVersionFactory.create(
            submission=submission, submitted=True, review_open=True
        )
        rights = ReviewerRights(self.user_reviewer)
        self.assertIsNone(rights.get_current_open_review(version_1))
        self.assertIsNone(rights.get_current_open_review(version_2))

        review_2 = ReviewFactory.create(reviewer=self.user_reviewer, version=version_2)
        rights = ReviewerRights(self.user_reviewer)
        self.assertIsNone(rights.get_current_open_review(version_1))
        self.assertEqual(review_2, rights.get_current_open_review(version_2))

        review_2.submitted = True
        review_2.save()
        rights = ReviewerRights(self.user_reviewer)
        self.assertIsNone(rights.get_current_open_review(version_1))
        self.assertIsNone(rights.get_current_open_review(version_2))

    def test_can_edit_review(self):
        submission = SubmissionFactory.create(created_by=self.user_author)
        version = SubmissionVersionFactory.create(
            submission=submission, submitted=True, review_open=True
        )

        review = ReviewFactory.create(reviewer=self.user_reviewer, version=version)
        rights = ReviewerRights(self.user_reviewer)

        self.assertTrue(rights.can_edit_review(review))

        version.review_open = False
        version.save()
        rights = ReviewerRights(self.user_reviewer)
        self.assertFalse(rights.can_edit_review(review))

        version.review_open = True
        version.save()
        review.submitted = True
        review.save()
        rights = ReviewerRights(self.user_reviewer)
        self.assertFalse(rights.can_edit_review(review))

    @override_settings(MESH_BLIND_MODE=BlindMode.NO_BLIND.value)
    def test_can_access_submission_author(self):
        submission = SubmissionFactory.create(created_by=self.user_author)
        version = SubmissionVersionFactory.create(
            submission=submission, submitted=True, review_open=True
        )

        ReviewFactory.create(reviewer=self.user_reviewer, version=version)
        rights = ReviewerRights(self.user_reviewer)

        self.assertTrue(rights.can_access_submission_author(submission))

        with override_settings(MESH_BLIND_MODE=BlindMode.SINGLE_BLIND.value):
            self.assertTrue(rights.can_access_submission_author(submission))

        with override_settings(MESH_BLIND_MODE=BlindMode.DOUBLE_BLIND.value):
            self.assertFalse(rights.can_access_submission_author(submission))

    def test_submission_status(self):
        submission = SubmissionFactory.create(created_by=self.user_author)
        version = SubmissionVersionFactory.create(
            submission=submission, submitted=True, review_open=True
        )
        rights = ReviewerRights(self.user_reviewer)

        # No access to the submission
        status = rights.get_submission_status(submission)
        self.assertEqual(status.status, SubmissionStatus.ERROR)

        # A review request for the current version
        review = ReviewFactory.create(reviewer=self.user_reviewer, version=version)
        rights = ReviewerRights(self.user_reviewer)
        submission = Submission.objects.get(pk=submission.pk)
        status = rights.get_submission_status(submission)
        self.assertEqual(status.status, SubmissionStatus.TODO)

        # An accepted review request for the current version
        review.accepted = True
        review.save()
        rights = ReviewerRights(self.user_reviewer)
        submission = Submission.objects.get(pk=submission.pk)
        status = rights.get_submission_status(submission)
        self.assertEqual(status.status, SubmissionStatus.TODO)

        # An accepted review request for the current version with review process closed
        version.review_open = False
        version.save()
        rights = ReviewerRights(self.user_reviewer)
        submission = Submission.objects.get(pk=submission.pk)
        status = rights.get_submission_status(submission)
        self.assertEqual(status.status, SubmissionStatus.WAITING)

        # Re-open review process
        version.review_open = True
        version.save()
        rights = ReviewerRights(self.user_reviewer)
        submission = Submission.objects.get(pk=submission.pk)
        status = rights.get_submission_status(submission)
        self.assertEqual(status.status, SubmissionStatus.TODO)

        # An declined review request for the current version
        review.accepted = False
        review.save()
        rights = ReviewerRights(self.user_reviewer)
        submission = Submission.objects.get(pk=submission.pk)
        status = rights.get_submission_status(submission)
        self.assertEqual(status.status, SubmissionStatus.ARCHIVED)

        # A submitted review for the current version
        review.submitted = True
        review.save()
        rights = ReviewerRights(self.user_reviewer)
        submission = Submission.objects.get(pk=submission.pk)
        status = rights.get_submission_status(submission)
        self.assertEqual(status.status, SubmissionStatus.WAITING)

        # New version with no review attached.
        version.review_open = False
        version.save()
        version = SubmissionVersionFactory.create(
            submission=submission, submitted=True, review_open=True
        )
        rights = ReviewerRights(self.user_reviewer)
        submission = Submission.objects.get(pk=submission.pk)
        status = rights.get_submission_status(submission)
        self.assertEqual(status.status, SubmissionStatus.WAITING)

        # Accepted submission
        submission.state = SubmissionState.ACCEPTED.value
        submission.save()
        rights = ReviewerRights(self.user_reviewer)
        submission = Submission.objects.get(pk=submission.pk)
        status = rights.get_submission_status(submission)
        self.assertEqual(status.status, SubmissionStatus.ARCHIVED)

        # Rejected submission
        submission.state = SubmissionState.REJECTED.value
        submission.save()
        rights = ReviewerRights(self.user_reviewer)
        submission = Submission.objects.get(pk=submission.pk)
        status = rights.get_submission_status(submission)
        self.assertEqual(status.status, SubmissionStatus.ARCHIVED)
