from unittest.mock import patch

from ...exceptions import RoleException
from ...interfaces.user_interfaces import ImpersonateData
from ...interfaces.user_interfaces import UserInfo
from ...models import EditorSubmissionRight
from ...models import Review
from ...models import Submission
from ...models import SubmissionMainFile
from ...models.factories import ReviewFactory
from ...models.factories import SubmissionAuthorFactory
from ...models.factories import SubmissionFactory
from ...models.factories import SubmissionVersionFactory
from ...models.factories import UserFactory
from ...models.user_models import User
from ...roles.author import Author
from ...roles.editor import Editor
from ...roles.journal_manager import JournalManager
from ...roles.reviewer import Reviewer
from ...roles.role_handler import RoleData
from ...roles.role_handler import RoleHandler
from ...views.model_proxy.review import ReviewProxy
from ...views.model_proxy.submission import SubmissionProxy
from ..base_test_case import BaseTestCase


class RoleDataTestCase(BaseTestCase):
    used_models = [User]

    def setUp(self):
        super().setUp()
        self.user_base = UserFactory.create()
        self.role_data = RoleData.from_user(self.user_base)

    def test_default_role(self):
        role = self.role_data.default_role()
        self.assertEqual(role.code(), Author.code())

        with patch.object(Reviewer, "active", True):
            with patch.object(Editor, "active", True):
                with patch.object(JournalManager, "active", True):
                    role = self.role_data.default_role()
                    self.assertEqual(role.code(), JournalManager.code())

                role = self.role_data.default_role()
                self.assertEqual(role.code(), Editor.code())

            role = self.role_data.default_role()
            self.assertEqual(role.code(), Reviewer.code())


class RoleHandlerTestCase(BaseTestCase):
    class_used_models = [Review, Submission, User]

    @classmethod
    def setUpClass(cls) -> None:
        """
        We create all the ref data only once here in `setUpClass` method instead of
        in `setUp` method to save quite some overhead time (it reduces tests plan
        up to 10s on my local machine).
        Note that the actual data status/content may vary from one test function to the
        other as it depends on the previously runned test functions ...
        It should be okay because we don't really play with the data content in
        this test case.
        """
        super().setUpClass()
        cls.user_journal_manager = UserFactory.create(journal_manager=True)
        cls.user_author = UserFactory.create()
        cls.user_reviewer = UserFactory.create()

        cls.submission = SubmissionFactory.create(
            created_by=cls.user_author, author_agreement=True
        )
        submission_version = SubmissionVersionFactory.create(submission=cls.submission)
        SubmissionMainFile.objects.create(attached_to=submission_version, file=cls.dummy_file)
        SubmissionAuthorFactory.create(submission=cls.submission)
        request = cls.dummy_request()
        request.user = cls.user_author
        # Update the submission object to reset cached_property
        cls.submission = Submission.objects.get(pk=cls.submission.pk)
        cls.submission.submit(cls.user_author)
        cls.review = ReviewFactory.create(reviewer=cls.user_reviewer, version=submission_version)

    def setUp(self):
        super().setUp()
        # Reset cached_properties
        self.submission = Submission.objects.get(pk=self.submission.pk)
        self.review = Review.objects.get(pk=self.review.pk)

    def test_init_user_roles(self):
        """
        Test corret init of role_handler.role_data + partial_init boolean.
        """
        user = User.objects.get(pk=self.user_author.pk)
        self.assertIsNone(user.current_role)

        role_handler = RoleHandler(self.user_author, partial_init=True)
        self.assertFalse(role_handler.init_complete)
        self.assertIsNotNone(role_handler.role_data)

        user = User.objects.get(pk=self.user_author.pk)
        self.assertIsNone(user.current_role)

        role_handler = RoleHandler(self.user_author)
        self.assertTrue(role_handler.init_complete)
        self.assertIsNotNone(role_handler.role_data)
        self.assertIsNotNone(role_handler.rights)
        self.assertIsNotNone(role_handler.current_role)
        self.assertEqual(role_handler.current_role.code(), Author.code())

        user = User.objects.get(pk=self.user_author.pk)
        self.assertEqual(user.current_role, Author.code())

    def test_set_current_role_base(self):
        role_handler = RoleHandler(self.user_author)
        user = User.objects.get(pk=self.user_author.pk)
        self.assertEqual(role_handler.current_role.code(), Author.code())
        self.assertEqual(user.current_role, Author.code())

        role_handler.set_current_role(role_handler.role_data.reviewer)
        user = User.objects.get(pk=self.user_author.pk)
        self.assertEqual(role_handler.current_role.code(), Reviewer.code())
        self.assertEqual(user.current_role, Reviewer.code())

    def test_set_current_role_impersonate(self):
        role_handler = RoleHandler(self.user_author)
        user = User.objects.get(pk=self.user_author.pk)
        self.assertEqual(role_handler.current_role.code(), Author.code())
        self.assertEqual(user.current_role, Author.code())

        request = self.dummy_request()
        impersonate_data = ImpersonateData(
            target_id=user.pk, source=UserInfo.from_user(self.user_journal_manager)
        )
        impersonate_data.serialize(request.session)
        role_handler.request = request
        role_handler.set_current_role(role_handler.role_data.reviewer)
        self.assertEqual(role_handler.current_role.code(), Reviewer.code())
        self.assertEqual(user.current_role, Author.code())

        impersonate_data = ImpersonateData.from_session(request.session)
        self.assertEqual(impersonate_data.target_role, Reviewer.code())  # type:ignore

        # Check session data
        self.assertDictEqual(request.session["user_role"], Reviewer.summary().serialize())
        self.assertEqual(len(request.session["available_roles"]), 1)

    def test_get_current_active_role_base(self):
        # Test current role with brand new user
        self.user_reviewer.current_role = None
        self.user_reviewer.save()
        role_handler = RoleHandler(self.user_reviewer, partial_init=True)
        self.assertEqual(len(role_handler.role_data.get_active_roles()), 2)
        self.assertEqual(role_handler.get_current_active_role().code(), Reviewer.code())

        # Test curent role when DB data is corrupted
        user = User.objects.get(pk=self.user_reviewer.pk)
        user.current_role = "__UNKNOWN_ROLE__"
        user.save()
        role_handler = RoleHandler(user, partial_init=True)
        self.assertEqual(role_handler.get_current_active_role().code(), Reviewer.code())

        # Test current role when the DB value is valid
        user.current_role = Author.code()
        user.save()
        role_handler = RoleHandler(user, partial_init=True)
        self.assertEqual(role_handler.get_current_active_role().code(), Author.code())

        # Test current role when the DB value is an inactive role
        user.current_role = Reviewer.code()
        user.save()
        Review.objects.get(pk=self.review.pk).delete()
        role_handler = RoleHandler(user, partial_init=True)
        self.assertEqual(role_handler.get_current_active_role().code(), Author.code())

        # Recreate deleted review for test consistency
        self.review.save()

    def test_get_current_active_role_impersonate(self):
        user = self.user_reviewer
        user.current_role = Reviewer.code()
        user.save()
        request = self.dummy_request()
        impersonate_data = ImpersonateData(
            target_id=user.pk,
            source=UserInfo.from_user(self.user_journal_manager),
            target_role=Author.code(),
        )
        impersonate_data.serialize(request.session)
        role_handler = RoleHandler(user, request=request, partial_init=True)

        self.assertEqual(role_handler.get_current_active_role().code(), Author.code())

        role_handler.request = None
        self.assertEqual(role_handler.get_current_active_role().code(), Reviewer.code())

        # Test corrupted impersonate data
        impersonate_data.target_role = "__UNKNOWN_ROLE__"
        impersonate_data.serialize(request.session)
        role_handler = RoleHandler(user, request=request, partial_init=True)

        self.assertEqual(role_handler.get_current_active_role().code(), Reviewer.code())

        # Test inactive role
        impersonate_data.target_role = Editor.code()
        impersonate_data.serialize(request.session)
        role_handler = RoleHandler(user, request=request, partial_init=True)

        self.assertEqual(role_handler.get_current_active_role().code(), Reviewer.code())

    def test_switch_role(self):
        user = self.user_reviewer
        user.current_role = Reviewer.code()
        user.save()
        role_handler = RoleHandler(user)
        self.assertEqual(role_handler.current_role.code(), Reviewer.code())

        role = role_handler.switch_role(Author.code())
        self.assertEqual(role.code(), Author.code())
        self.assertEqual(role_handler.current_role.code(), Author.code())
        user = User.objects.get(pk=user.pk)
        self.assertEqual(user.current_role, Author.code())

        self.assertRaises(RoleException, role_handler.switch_role, "__UNKNOWN_ROLE__")
        user = User.objects.get(pk=user.pk)
        self.assertEqual(user.current_role, Author.code())

        self.assertRaises(RoleException, role_handler.switch_role, JournalManager.code())
        user = User.objects.get(pk=user.pk)
        self.assertEqual(user.current_role, Author.code())

    def test_execute_rights_function(self):
        role_handler = RoleHandler(self.user_author)

        # Test author access to his own submission
        function_name = "can_access_submission"
        right_result = role_handler.execute_rights_function(
            role_handler.rights, function_name, self.submission
        )
        self.assertTrue(right_result)

        # Test wrong function
        function_name = "can_do_unkwnown_action"
        self.assertRaises(
            RoleException, role_handler.execute_rights_function, role_handler.rights, function_name
        )

        # Test with attribute
        function_name = "submissions"
        self.assertRaises(
            RoleException, role_handler.execute_rights_function, role_handler.rights, function_name
        )

    def test_check_rights(self):
        function_name = "can_access_submission"
        role_handler = RoleHandler(self.user_reviewer)

        role_handler.switch_role(Reviewer.code())
        right_result = role_handler.check_rights(function_name, self.submission)
        self.assertTrue(right_result)

        role_handler.switch_role(Author.code())
        right_result = role_handler.check_rights(function_name, self.submission)
        self.assertFalse(right_result)

    def test_check_global_rights(self):
        function_name = "can_access_submission"
        role_handler = RoleHandler(self.user_reviewer)

        role_handler.switch_role(Reviewer.code())
        right_result = role_handler.check_global_rights(function_name, self.submission)
        self.assertTrue(right_result)

        role_handler.switch_role(Author.code())
        right_result = role_handler.check_global_rights(function_name, self.submission)
        self.assertTrue(right_result)

    def test_get_from_rights(self):
        function_name = "get_submission_status"
        role_handler = RoleHandler(self.user_reviewer)

        role_handler.switch_role(Reviewer.code())
        result = role_handler.get_from_rights(function_name, self.submission)
        self.assertIsNotNone(result)

        role_handler.switch_role(Author.code())
        result = role_handler.get_from_rights(function_name, self.submission)
        self.assertIsNotNone(result)

    def test_get_attribute(self):
        attribute_name = "submissions"
        role_handler = RoleHandler(self.user_reviewer)

        role_handler.switch_role(Reviewer.code())
        result = role_handler.get_attribute(attribute_name)
        self.assertIsNotNone(result)

        role_handler.switch_role(Author.code())
        result = role_handler.get_attribute(attribute_name)
        self.assertIsNotNone(result)

    def test_get_proxy(self):
        role_handler = RoleHandler(self.user_reviewer)

        submission_proxy = SubmissionProxy(self.submission, role_handler)
        self.assertIsInstance(submission_proxy, SubmissionProxy)

        review_proxy = ReviewProxy(self.review, role_handler)
        self.assertIsInstance(review_proxy, ReviewProxy)

    def test_token_authentication_allowed(self):
        role_handler = RoleHandler(self.user_author)
        self.assertTrue(role_handler.token_authentication_allowed())

        role_handler = RoleHandler(self.user_reviewer)
        self.assertTrue(role_handler.token_authentication_allowed())

        # Give editor right to user_reviewer
        EditorSubmissionRight.objects.create(user=self.user_reviewer, submission=self.submission)
        role_handler = RoleHandler(self.user_reviewer)
        self.assertFalse(role_handler.token_authentication_allowed())
        EditorSubmissionRight.objects.all().delete()

        role_handler = RoleHandler(self.user_journal_manager)
        self.assertFalse(role_handler.token_authentication_allowed())
