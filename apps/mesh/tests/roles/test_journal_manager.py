from ...models.editorial_models import EditorSubmissionRight
from ...models.factories import ReviewFactory
from ...models.factories import SubmissionFactory
from ...models.factories import SubmissionVersionFactory
from ...models.factories import UserFactory
from ...models.submission_models import Submission
from ...models.submission_models import SubmissionState
from ...models.user_models import User
from ...roles.journal_manager import JournalManager
from ...roles.journal_manager import JournalManagerRights
from ..base_test_case import BaseTestCase


class JournalManagerTestCase(BaseTestCase):
    used_models = [Submission]
    class_used_models = [User]

    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.user_journal_manager = UserFactory.create(journal_manager=True)

    def test_role_active(self):
        random_user = UserFactory.create()
        role = JournalManager(random_user)
        self.assertFalse(role.active)

        role = JournalManager(self.user_journal_manager)
        self.assertTrue(role.active)

    def test_submissions(self):
        """
        All submissions are available to the journal manager except the OPENED (draft) ones.
        """
        submission_1 = SubmissionFactory.create()
        submission_2 = SubmissionFactory.create(state=SubmissionState.SUBMITTED.value)
        submission_3 = SubmissionFactory.create(state=SubmissionState.ON_REVIEW.value)
        submission_4 = SubmissionFactory.create(state=SubmissionState.REVISION_REQUESTED.value)
        submission_5 = SubmissionFactory.create(state=SubmissionState.REVISION_SUBMITTED.value)
        submission_6 = SubmissionFactory.create(state=SubmissionState.ACCEPTED.value)
        submission_7 = SubmissionFactory.create(state=SubmissionState.REJECTED.value)

        rights = JournalManagerRights(self.user_journal_manager)
        self.assertEqual(len(rights.submissions), 6)
        self.assertNotIn(submission_1, rights.submissions)
        self.assertIn(submission_2, rights.submissions)
        self.assertIn(submission_3, rights.submissions)
        self.assertIn(submission_4, rights.submissions)
        self.assertIn(submission_5, rights.submissions)
        self.assertIn(submission_6, rights.submissions)
        self.assertIn(submission_7, rights.submissions)

    def test_managed_users(self):
        user_author_1 = UserFactory.create()
        user_author_2 = UserFactory.create()
        user_reviewer = UserFactory.create()
        user_editor_1 = UserFactory.create()
        user_editor_2 = UserFactory.create()
        user_admin = UserFactory.create(is_superuser=True)  # noqa
        user_journal_manager = UserFactory.create(journal_manager=True)
        user_other_journal_manager = UserFactory.create(journal_manager=True)  # noqa

        submission_1 = SubmissionFactory.create(created_by=user_author_1)
        submission_2 = SubmissionFactory.create(created_by=user_author_2)  # noqa
        version = SubmissionVersionFactory.create(submission=submission_1)
        ReviewFactory.create(reviewer=user_reviewer, version=version)
        EditorSubmissionRight.objects.create(user=user_editor_1, submission=submission_1)
        EditorSubmissionRight.objects.create(user=user_editor_2, submission=submission_1)

        rights = JournalManagerRights(user_journal_manager)
        users = rights.managed_users
        self.assertEqual(len(users), 5)
        self.assertIn(user_author_1, rights.managed_users)
        self.assertIn(user_author_2, rights.managed_users)
        self.assertIn(user_reviewer, rights.managed_users)
        self.assertIn(user_editor_1, rights.managed_users)
        self.assertIn(user_editor_2, rights.managed_users)

    def test_can_edit_journal_sections(self):
        rights = JournalManagerRights(self.user_journal_manager)
        self.assertTrue(rights.can_edit_journal_sections())
