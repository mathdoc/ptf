from datetime import datetime
from datetime import timedelta
from unittest.mock import patch

from ...interfaces.submission_interfaces import SubmissionStatus
from ...models.editorial_models import EditorSectionRight
from ...models.editorial_models import EditorSubmissionRight
from ...models.factories import JournalSectionFactory
from ...models.factories import ReviewFactory
from ...models.factories import SubmissionAuthorFactory
from ...models.factories import SubmissionFactory
from ...models.factories import SubmissionVersionFactory
from ...models.factories import UserFactory
from ...models.journal_models import JournalSection
from ...models.submission_models import Submission
from ...models.submission_models import SubmissionMainFile
from ...models.submission_models import SubmissionState
from ...models.user_models import User
from ...roles.editor import Editor
from ...roles.editor import EditorRights
from ...roles.editor import get_section_editors
from ..base_test_case import BaseTestCase


class EditorTestCase(BaseTestCase):
    used_models = [
        EditorSubmissionRight,
        EditorSectionRight,
        Submission,
        JournalSection,
    ]
    class_used_models = [User]

    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.user_author = UserFactory.create()
        cls.user_editor = UserFactory.create()

    def test_submissions_from_direct_right(self):
        submission_1 = SubmissionFactory.create()
        submission_2 = SubmissionFactory.create()  # noqa

        role = Editor(self.user_editor)
        self.assertFalse(role.active)

        EditorSubmissionRight.objects.create(user=self.user_editor, submission=submission_1)
        role = Editor(self.user_editor)
        self.assertTrue(role.active)
        rights = role.rights

        self.assertEqual(len(rights.submissions), 1)
        self.assertEqual(rights.submissions[0], submission_1)

        submission_1.created_by = self.user_editor
        submission_1.save()
        role = Editor(self.user_editor)
        self.assertTrue(role.active)
        rights = role.rights
        self.assertEqual(len(rights.submissions), 1)
        self.assertEqual(rights.submissions[0], submission_1)

    def test_submissions_from_journal_sections(self):
        journal_section_1 = JournalSectionFactory.create()
        journal_section_2 = JournalSectionFactory.create(parent=journal_section_1)
        journal_section_3 = JournalSectionFactory.create()

        submission_1 = SubmissionFactory.create(
            journal_section=journal_section_1, state=SubmissionState.SUBMITTED.value
        )
        submission_2 = SubmissionFactory.create(
            journal_section=journal_section_2, state=SubmissionState.SUBMITTED.value
        )
        submission_3 = SubmissionFactory.create(  # noqa
            journal_section=journal_section_3, state=SubmissionState.SUBMITTED.value
        )

        EditorSectionRight.objects.create(user=self.user_editor, journal_section=journal_section_1)
        role = Editor(self.user_editor)
        self.assertTrue(role.active)
        rights = role.rights

        self.assertEqual(len(rights.submissions), 2)
        self.assertIn(submission_1, rights.submissions)
        self.assertIn(submission_2, rights.submissions)

        # Self-made submission are not available to the editor.
        submission_1.created_by = self.user_editor
        submission_1.save()
        role = Editor(self.user_editor)
        self.assertTrue(role.active)
        rights = role.rights
        self.assertEqual(len(rights.submissions), 1)
        self.assertIn(submission_2, rights.submissions)

        # OPENED submissions are not availabe to anyone but to its author.
        submission_2.state = SubmissionState.OPENED.value
        submission_2.save()
        role = Editor(self.user_editor)
        # Editor role is actve if it has rights over any journal_section or any submissions
        self.assertTrue(role.active)
        rights = role.rights
        self.assertEqual(len(rights.submissions), 0)

    def test_all_submissions(self):
        journal_section_1 = JournalSectionFactory.create()
        journal_section_2 = JournalSectionFactory.create(parent=journal_section_1)
        journal_section_3 = JournalSectionFactory.create()

        submission_1 = SubmissionFactory.create(
            journal_section=journal_section_1, state=SubmissionState.SUBMITTED.value
        )
        submission_2 = SubmissionFactory.create(
            journal_section=journal_section_2, state=SubmissionState.SUBMITTED.value
        )
        submission_3 = SubmissionFactory.create(  # noqa
            journal_section=journal_section_3, state=SubmissionState.SUBMITTED.value
        )
        submission_4 = SubmissionFactory.create(state=SubmissionState.SUBMITTED.value)
        submission_5 = SubmissionFactory.create(  # noqa
            journal_section=journal_section_1,
            state=SubmissionState.SUBMITTED.value,
            created_by=self.user_editor,
        )

        EditorSectionRight.objects.create(user=self.user_editor, journal_section=journal_section_1)
        EditorSubmissionRight.objects.create(user=self.user_editor, submission=submission_4)

        rights = EditorRights(self.user_editor)
        self.assertEqual(len(rights.submissions), 3)
        self.assertIn(submission_1, rights.submissions)
        self.assertIn(submission_2, rights.submissions)
        self.assertIn(submission_4, rights.submissions)

    # All the following tests are rights exclusive to the Editor+ roles.
    # We just ensure they resolve to True when the editor has rights over the submission.
    def test_can_invite_reviewer(self):
        submission = SubmissionFactory.create(state=SubmissionState.SUBMITTED.value)
        version = SubmissionVersionFactory.create(submission=submission, submitted=True)
        EditorSubmissionRight.objects.create(user=self.user_editor, submission=submission)

        rights = EditorRights(self.user_editor)
        self.assertFalse(rights.can_invite_reviewer(version))

        version.review_open = True
        self.assertTrue(rights.can_invite_reviewer(version))

    def test_can_access_version(self):
        submission = SubmissionFactory.create(state=SubmissionState.SUBMITTED.value)
        version = SubmissionVersionFactory.create(submission=submission)
        EditorSubmissionRight.objects.create(user=self.user_editor, submission=submission)

        rights = EditorRights(self.user_editor)
        self.assertFalse(rights.can_access_version(version))

        version.submitted = True
        self.assertTrue(rights.can_access_version(version))

    def test_managed_users(self):
        User.objects.all().delete()
        user_editor = UserFactory.create()
        user_author_1 = UserFactory.create()
        user_author_2 = UserFactory.create()
        user_reviewer = UserFactory.create()
        user_editor_2 = UserFactory.create()
        user_editor_3 = UserFactory.create()
        user_journal_manager = UserFactory.create(journal_manager=True)
        user_admin = UserFactory.create(is_superuser=True)  # noqa

        submission_1 = SubmissionFactory.create(
            created_by=user_author_1, state=SubmissionState.SUBMITTED.value
        )
        version_1 = SubmissionVersionFactory(submission=submission_1, submitted=True)
        submission_2 = SubmissionFactory.create(
            created_by=user_author_2, state=SubmissionState.SUBMITTED.value
        )
        review = ReviewFactory.create(reviewer=user_reviewer, version=version_1)  # noqa

        EditorSubmissionRight.objects.create(user=user_editor, submission=submission_1)
        EditorSubmissionRight.objects.create(user=user_editor_2, submission=submission_2)

        journal_section = JournalSectionFactory.create(created_by=user_journal_manager)
        EditorSectionRight.objects.create(journal_section=journal_section, user=user_editor_3)

        rights = EditorRights(user_editor)
        self.assertEqual(len(rights.managed_users), 3)
        self.assertIn(user_author_1, rights.managed_users)
        self.assertIn(user_author_2, rights.managed_users)
        self.assertIn(user_reviewer, rights.managed_users)

    def test_can_impersonate(self):
        rights = EditorRights(self.user_editor)
        self.assertTrue(rights.can_impersonate())

    def test_can_access_submission_log(self):
        submission = SubmissionFactory.create(state=SubmissionState.SUBMITTED.value)
        rights = EditorRights(self.user_editor)
        self.assertFalse(rights.can_access_submission_log(submission))

        EditorSubmissionRight.objects.create(user=self.user_editor, submission=submission)
        rights = EditorRights(self.user_editor)
        self.assertTrue(rights.can_access_submission_log(submission))

    def test_can_create_editorial_decision(self):
        submission = SubmissionFactory.create(state=SubmissionState.SUBMITTED.value)
        rights = EditorRights(self.user_editor)
        self.assertFalse(rights.can_create_editorial_decision(submission))

        EditorSubmissionRight.objects.create(user=self.user_editor, submission=submission)
        rights = EditorRights(self.user_editor)
        self.assertTrue(rights.can_create_editorial_decision(submission))

        submission.state = SubmissionState.ON_REVIEW.value
        submission.save()
        rights = EditorRights(self.user_editor)
        self.assertTrue(rights.can_create_editorial_decision(submission))

        submission.state = SubmissionState.REVISION_REQUESTED.value
        submission.save()
        rights = EditorRights(self.user_editor)
        self.assertTrue(rights.can_create_editorial_decision(submission))

        submission.state = SubmissionState.REVISION_SUBMITTED.value
        submission.save()
        rights = EditorRights(self.user_editor)
        self.assertTrue(rights.can_create_editorial_decision(submission))

        submission.state = SubmissionState.ACCEPTED.value
        submission.save()
        rights = EditorRights(self.user_editor)
        self.assertTrue(rights.can_create_editorial_decision(submission))

        submission.state = SubmissionState.REJECTED.value
        submission.save()
        rights = EditorRights(self.user_editor)
        self.assertTrue(rights.can_create_editorial_decision(submission))

    def test_can_start_review_process(self):
        submission = SubmissionFactory.create()

        rights = EditorRights(self.user_editor)
        with patch.object(Submission, "is_reviewable", True):
            self.assertFalse(rights.can_start_review_process(submission))

        with patch.object(Submission, "is_reviewable", False):
            self.assertFalse(rights.can_start_review_process(submission))

        EditorSubmissionRight.objects.create(user=self.user_editor, submission=submission)
        rights = EditorRights(self.user_editor)
        with patch.object(Submission, "is_reviewable", True):
            self.assertTrue(rights.can_start_review_process(submission))

        with patch.object(Submission, "is_reviewable", False):
            self.assertFalse(rights.can_start_review_process(submission))

    def test_can_assign_editor(self):
        submission = SubmissionFactory.create(state=SubmissionState.SUBMITTED.value)
        rights = EditorRights(self.user_editor)
        self.assertFalse(rights.can_assign_editor(submission))

        EditorSubmissionRight.objects.create(user=self.user_editor, submission=submission)
        rights = EditorRights(self.user_editor)
        self.assertTrue(rights.can_assign_editor(submission))

    def test_can_filter_submissions(self):
        rights = EditorRights(self.user_editor)
        self.assertTrue(rights.can_filter_submissions())

    def test_can_access_journal_sections(self):
        rights = EditorRights(self.user_editor)
        self.assertTrue(rights.can_access_journal_sections())

    def test_can_edit_review_file_right(self):
        submission = SubmissionFactory.create(state=SubmissionState.SUBMITTED.value)
        version = SubmissionVersionFactory.create(submission=submission, submitted=True)
        review = ReviewFactory.create(version=version)
        rights = EditorRights(self.user_editor)
        self.assertFalse(rights.can_edit_review_file_right(review))

        EditorSubmissionRight.objects.create(user=self.user_editor, submission=submission)
        rights = EditorRights(self.user_editor)
        self.assertTrue(rights.can_edit_review_file_right(review))

    def test_editor_submission_status(self):
        """
        Test the editor status wrt. the whole submission workflow
        """
        submission = SubmissionFactory.create(author_agreement=True)
        version = SubmissionVersionFactory.create(
            submission=submission, submitted=False, review_open=False
        )
        SubmissionAuthorFactory.create(submission=submission)
        SubmissionMainFile.objects.create(file=self.dummy_file, attached_to=version)

        # No rights over the submission
        submission = Submission.objects.get(pk=submission.pk)
        rights = EditorRights(self.user_editor)
        status = rights.get_submission_status(submission)
        self.assertEqual(status.status, SubmissionStatus.ERROR)

        # OPENED submission
        EditorSubmissionRight.objects.create(user=self.user_editor, submission=submission)
        rights = EditorRights(self.user_editor)
        status = rights.get_submission_status(submission)
        self.assertEqual(status.status, SubmissionStatus.ERROR)

        # SUBMITTED submission
        request = self.dummy_request()
        request.user = submission.created_by
        submission.submit(submission.created_by)
        submission = Submission.objects.get(pk=submission.pk)
        status = rights.get_submission_status(submission)
        self.assertEqual(status.status, SubmissionStatus.TODO)

        # UNDER REVIEW submission with no reviews
        request.user = self.user_editor
        submission.start_review_process(self.user_editor)
        submission = Submission.objects.get(pk=submission.pk)
        status = rights.get_submission_status(submission)
        self.assertEqual(status.status, SubmissionStatus.TODO)

        # UNDER REVIEW with pending answer
        review = ReviewFactory.create(version=version)
        submission = Submission.objects.get(pk=submission.pk)
        status = rights.get_submission_status(submission)
        self.assertEqual(status.status, SubmissionStatus.WAITING)

        # UNDER REVIEW with 1 pending review & 1 overdue review
        yesterday = (datetime.utcnow() - timedelta(days=1)).date()
        review_overdue = ReviewFactory.create(version=version, date_response_due=yesterday)
        submission = Submission.objects.get(pk=submission.pk)
        status = rights.get_submission_status(submission)
        self.assertEqual(status.status, SubmissionStatus.TODO)

        # UNDER REVIEW with 1 pending review, 1 pending answer
        review_overdue.date_response_due = (datetime.utcnow() + timedelta(days=1)).date()
        review_overdue.accepted = True
        review_overdue.save()
        submission = Submission.objects.get(pk=submission.pk)
        status = rights.get_submission_status(submission)
        self.assertEqual(status.status, SubmissionStatus.WAITING)

        # UNDER REVIEW with all reviews submitted or declined
        review.accepted = False
        review.save()
        review_overdue.submitted = True
        review_overdue.save()
        submission = Submission.objects.get(pk=submission.pk)
        status = rights.get_submission_status(submission)
        self.assertEqual(status.status, SubmissionStatus.TODO)

        # REVISIIONS REQUESTED submission
        submission.state = SubmissionState.REVISION_REQUESTED.value
        submission.save()
        submission = Submission.objects.get(pk=submission.pk)
        status = rights.get_submission_status(submission)
        self.assertEqual(status.status, SubmissionStatus.WAITING)

        # REVISIIONS SUBMITTED submission
        version_2 = SubmissionVersionFactory.create(
            submission=submission, submitted=False, review_open=False
        )
        SubmissionMainFile.objects.create(file=self.dummy_file, attached_to=version_2)
        submission = Submission.objects.get(pk=submission.pk)
        request.user = self.user_author
        submission.submit(self.user_author)
        submission = Submission.objects.get(pk=submission.pk)
        status = rights.get_submission_status(submission)
        self.assertEqual(status.status, SubmissionStatus.TODO)

        # ACCEPTED submission
        submission.state = SubmissionState.ACCEPTED.value
        submission.save()
        submission = Submission.objects.get(pk=submission.pk)
        status = rights.get_submission_status(submission)
        self.assertEqual(status.status, SubmissionStatus.ARCHIVED)

        # REJECTED submission
        submission.state = SubmissionState.REJECTED.value
        submission.save()
        submission = Submission.objects.get(pk=submission.pk)
        status = rights.get_submission_status(submission)
        self.assertEqual(status.status, SubmissionStatus.ARCHIVED)


class EditorUtilsTestCase(BaseTestCase):
    def test_section_editors(self):
        journal_section_1 = JournalSectionFactory.create()
        journal_section_2 = JournalSectionFactory.create(parent=journal_section_1)
        journal_section_3 = JournalSectionFactory.create(parent=journal_section_1)
        journal_section_4 = JournalSectionFactory.create(parent=journal_section_2)

        submission = SubmissionFactory.create(journal_section=journal_section_4)

        user_editor_1 = UserFactory.create()
        user_editor_2 = UserFactory.create()
        user_editor_3 = UserFactory.create()
        user_editor_4 = UserFactory.create()

        EditorSectionRight.objects.create(journal_section=journal_section_1, user=user_editor_1)
        EditorSectionRight.objects.create(journal_section=journal_section_2, user=user_editor_2)
        EditorSectionRight.objects.create(journal_section=journal_section_3, user=user_editor_3)
        EditorSectionRight.objects.create(journal_section=journal_section_4, user=user_editor_4)

        EditorSubmissionRight.objects.create(submission=submission, user=user_editor_3)

        section_editors = get_section_editors(submission)
        self.assertEqual(len(section_editors), 3)
        self.assertIn(user_editor_1, section_editors)
        self.assertIn(user_editor_2, section_editors)
        self.assertIn(user_editor_4, section_editors)
