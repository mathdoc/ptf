import os
import shutil

from django.contrib.messages.middleware import MessageMiddleware
from django.contrib.sessions.middleware import SessionMiddleware
from django.core.files.uploadedfile import SimpleUploadedFile
from django.db.models import Model
from django.http import HttpRequest
from django.http import HttpResponse
from django.test import RequestFactory
from django.test import TestCase

from ..app_settings import app_settings


def reset_user_files_directory():
    """
    Removes everything in the (test !!!) directory used to store MESH user files.
    """
    base = app_settings.FILES_DIRECTORY
    for filename in os.listdir(base):
        file_path = os.path.join(base, filename)
        if os.path.isfile(file_path) or os.path.islink(file_path):
            os.unlink(file_path)
        elif os.path.isdir(file_path):
            shutil.rmtree(file_path)


class BaseTestCase(TestCase):
    """
    Base test case offering basic commodities such has:
        - WSGI request forgery with session & message
        - Automatic clean up of tables used by the whole test case or individual test functions
        ...
    """

    dummy_file = SimpleUploadedFile(
        "my_submission.pdf", b"My submission", content_type="application/pdf"
    )
    # List of used models to be deleted after each test function.
    used_models: list[type[Model]] = []
    # List of used models to be deleted after the full test case.
    class_used_models: list[type[Model]] = []

    request_factory = RequestFactory()
    session_middleware = SessionMiddleware(lambda x: HttpResponse())
    message_middleware = MessageMiddleware(lambda x: HttpResponse())

    @classmethod
    def dummy_request(cls, route: str = "/", *args, **kwargs) -> HttpRequest:
        request = cls.request_factory.get(route, *args, **kwargs)
        cls.session_middleware.process_request(request)
        cls.message_middleware.process_request(request)
        return request

    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.clear_tables(*cls.used_models)
        cls.clear_tables(*cls.class_used_models)

    @classmethod
    def tearDownClass(cls) -> None:
        cls.reset_user_files_directory()
        cls.clear_tables(*cls.class_used_models)

    @classmethod
    def clear_tables(cls, *args: type[Model]):
        """
        Empty the given tables.
        """
        for model in args:
            model.objects.all().delete()

    def tearDown(self):
        """Clear all the model tables listed in `self.used_models`"""
        super().tearDown()
        self.clear_tables(*self.used_models)

    @classmethod
    def reset_user_files_directory(cls):
        """
        Removes everything in the (test !!!) directory used to store MESH user files.
        """
        reset_user_files_directory()
