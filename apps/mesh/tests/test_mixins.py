from unittest.mock import patch

from django.contrib.auth.models import AnonymousUser
from django.http import Http404
from django.http import HttpResponse
from django.urls import reverse

from ..mixins import ROLE_SWITCH_QUERY_PARAM
from ..mixins import BaseRoleMixin
from ..mixins import RoleMixin
from ..models.factories import ReviewFactory
from ..models.factories import SubmissionFactory
from ..models.factories import SubmissionVersionFactory
from ..models.factories import UserFactory
from ..models.submission_models import Submission
from ..models.user_models import User
from ..roles.author import Author
from ..roles.editor import Editor
from ..roles.reviewer import Reviewer
from ..roles.role_handler import ROLE_HANDLER_CACHE
from ..roles.role_handler import RoleHandler
from .base_test_case import BaseTestCase

TEST_RESPONSE = HttpResponse("NAIVE RESPONSE")


class NaiveDispatcher:
    """
    Naive dispatcher to be used for test purposes.
    """

    def dispatch(self, *args, **kwargs):
        return TEST_RESPONSE


class TestMixin(RoleMixin, NaiveDispatcher):
    pass


class TestRoleMixin(BaseTestCase):
    class_used_models = [Submission, User]

    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.user_author = UserFactory.create()
        submission = SubmissionFactory.create(created_by=cls.user_author)
        version = SubmissionVersionFactory.create(submission=submission)
        cls.user_reviewer = UserFactory.create()
        review = ReviewFactory.create(version=version, reviewer=cls.user_reviewer)  # noqa

    def test_dispatch_no_authenticated_user(self):
        mixin = TestMixin()
        request = self.dummy_request("/")
        request.user = AnonymousUser()
        mixin.request = request
        response = mixin.dispatch(request)

        self.assertRedirects(response, "/accounts/login/?next=/", fetch_redirect_response=False)

        mixin = BaseRoleMixin()
        self.assertRaises(AssertionError, mixin.dispatch, request)

    def test_base_dispatch(self):
        """
        Test the instantiation of the role_handler and the request attributes.
        """
        mixin = TestMixin()
        request = self.dummy_request()
        request.user = self.user_author
        self.assertFalse(hasattr(mixin, "role_handler"))
        self.assertFalse(hasattr(mixin, "request"))

        response = mixin.dispatch(request)
        self.assertIs(response, TEST_RESPONSE)
        self.assertIsInstance(mixin.role_handler, RoleHandler)
        self.assertIs(mixin.role_handler.user, self.user_author)
        self.assertTrue(mixin.role_handler.init_complete)
        self.assertIs(mixin.request, request)

    def test_cached_role_handler(self):
        # Test when the role_handler is cahced for the correct user
        mixin = TestMixin()
        request = self.dummy_request()
        request.user = self.user_author
        role_handler = RoleHandler(self.user_author, partial_init=True)
        setattr(request, ROLE_HANDLER_CACHE, role_handler)

        self.assertIsNone(role_handler.request)
        self.assertFalse(role_handler.init_complete)

        response = mixin.dispatch(request)
        self.assertIs(response, TEST_RESPONSE)
        self.assertIs(mixin.role_handler, role_handler)
        self.assertIs(role_handler.request, request)
        self.assertIs(role_handler.user, self.user_author)
        self.assertTrue(role_handler.init_complete)

        # Test role_handler for another user
        mixin = TestMixin()
        request = self.dummy_request()
        request.user = self.user_author
        role_handler = RoleHandler(self.user_reviewer, partial_init=True)
        setattr(request, ROLE_HANDLER_CACHE, role_handler)

        self.assertIsNone(role_handler.request)
        self.assertFalse(role_handler.init_complete)

        response = mixin.dispatch(request)
        self.assertIs(response, TEST_RESPONSE)
        self.assertIsNot(mixin.role_handler, role_handler)
        self.assertIsNone(role_handler.request)
        self.assertIs(mixin.role_handler.user, self.user_author)
        self.assertIs(role_handler.user, self.user_reviewer)
        self.assertTrue(mixin.role_handler.init_complete)

    def test_restrict_dispatch(self):
        mixin = TestMixin()
        request = self.dummy_request()
        request.user = self.user_author
        with patch.object(RoleMixin, "restrict_dispatch", return_value=False):
            response = mixin.dispatch(request)

        self.assertIs(response, TEST_RESPONSE)

        mixin = TestMixin()
        request = self.dummy_request()
        request.user = self.user_author
        with patch.object(RoleMixin, "restrict_dispatch", return_value=True):
            response = mixin.dispatch(request)

        self.assertRedirects(response, reverse("mesh:home"), fetch_redirect_response=False)

        redirect_uri = "/you_failed/dummy/"
        mixin.fail_redirect_uri = redirect_uri
        with patch.object(RoleMixin, "restrict_dispatch", return_value=True):
            response = mixin.dispatch(request)

        self.assertRedirects(response, redirect_uri, fetch_redirect_response=False)

    def test_force_role(self):
        role_handler = RoleHandler(self.user_reviewer)
        role_handler.switch_role(Author.code())

        user = User.objects.get(pk=self.user_reviewer.pk)
        self.assertEqual(user.current_role, Author.code())

        # Test forcing active reviewer role
        mixin = TestMixin()
        mixin.restricted_roles = [Reviewer]
        request = self.dummy_request()
        request.user = self.user_reviewer

        response = mixin.dispatch(request)
        self.assertIs(response, TEST_RESPONSE)
        self.assertEqual(mixin.role_handler.current_role.code(), Reviewer.code())
        user = User.objects.get(pk=self.user_reviewer.pk)
        self.assertEqual(user.current_role, Reviewer.code())

        # Test forcing current role again
        mixin = TestMixin()
        mixin.restricted_roles = [Reviewer]
        request = self.dummy_request()
        request.user = self.user_reviewer

        response = mixin.dispatch(request)
        self.assertIs(response, TEST_RESPONSE)
        self.assertEqual(mixin.role_handler.current_role.code(), Reviewer.code())
        user = User.objects.get(pk=self.user_reviewer.pk)
        self.assertEqual(user.current_role, Reviewer.code())

        # Test forcing inactive role
        mixin = TestMixin()
        mixin.restricted_roles = [Editor]

        self.assertRaises(Http404, mixin.dispatch, request)
        user = User.objects.get(pk=self.user_reviewer.pk)
        self.assertEqual(user.current_role, Reviewer.code())

    def test_force_role_when_switching(self):
        role_handler = RoleHandler(self.user_reviewer)
        role_handler.switch_role(Author.code())

        user = User.objects.get(pk=self.user_reviewer.pk)
        self.assertEqual(user.current_role, Author.code())

        query_params = {ROLE_SWITCH_QUERY_PARAM: "true"}
        request = self.dummy_request("/restricted_route", data=query_params)
        request.user = self.user_reviewer

        mixin = TestMixin()
        mixin.restricted_roles = [Reviewer]
        response = mixin.dispatch(request)
        self.assertIs(response, TEST_RESPONSE)
        user = User.objects.get(pk=self.user_reviewer.pk)
        self.assertEqual(user.current_role, Author.code())
