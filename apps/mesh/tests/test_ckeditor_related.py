from django.test import override_settings

from ..ckeditor_config import CKEditorConfig
from ..ckeditor_config import sanitize_html_input
from ..forms.fields import CKEditorFormField

tags = {"a", "span"}
tag_attributes = {"*": {"id", "class", "style"}, "a": {"href"}}
tag_attribute_values = {"*": {"style": {"text-align: center"}}}
url_schemes = {"https"}
styles = {"*": {"text-align"}}

sanitizer_tests = [
    {
        "html": """<a id="span" class="span" style="text-align: center" href="https://link.test" rel="noopener noreferrer">My a</a>""",
        "expected": """<a id="span" class="span" style="text-align: center" href="https://link.test" rel="noopener noreferrer">My a</a>""",
    },
    {
        "html": """<a id="a" class="a" style="text-align: center;" href="javascript:get_link()" rel="noopener noreferrer" onchange="Evil()">My a</a>""",
        "expected": """<a id="a" class="a" style="text-align: center;" rel="noopener noreferrer">My a</a>""",
    },
    {
        "html": """  &nbsp;\n   <a rel="noopener noreferrer"><span class="my-span">My link</span><script src="https://evil.test"></script></a>  &nbsp;\n """,
        "expected": """<a rel="noopener noreferrer"><span class="my-span">My link</span></a>""",
    },
]


def test_sanitize_html_input():
    """
    Basic testing of the HTML sanitizer.
    """
    for test in sanitizer_tests:
        assert (
            sanitize_html_input(
                test["html"], tags, tag_attributes, tag_attribute_values, url_schemes
            )
            == test["expected"]
        )


def test_ckeditor_sanitizer():
    """
    Test correct forwarding of editor config to the sanitizing function.
    """
    ckeditor_config = CKEditorConfig(
        id="mesh",
        allowed_tags=tags,
        allowed_attributes=tag_attributes,
        allowed_attributes_values=tag_attribute_values,
        allowed_url_schemes=url_schemes,
    )

    for test in sanitizer_tests:
        assert ckeditor_config.sanitize_value(test["html"]) == test["expected"]


def test_ckeditor_allowed_content():
    """
    Test the correct generation of the allowed content used by the CKEditor library.
    `tag [attrs]{styles}(classes)`
    """
    ckeditor_config = CKEditorConfig(
        id="mesh",
        allowed_tags=tags,
        allowed_attributes=tag_attributes,
        allowed_attributes_values=tag_attribute_values,
        allowed_url_schemes=url_schemes,
        allowed_styles=styles,
    )

    expected = "a[class,href,id,style]{text-align}(*);span[class,id,style]{text-align}(*)"
    assert ckeditor_config.allowed_content() == expected

    assert ckeditor_config.javascript_config()["allowedContent"] == expected

    ckeditor_config.allowed_tags = {"a", "b", "span"}
    expected = "a[class,href,id,style]{text-align}(*);b[class,id,style]{text-align}(*);span[class,id,style]{text-align}(*)"
    assert ckeditor_config.allowed_content() == expected


def test_ckeditor_form_field():
    """
    Test the auto-sanitization of the form field.
    """
    ckeditor_config = CKEditorConfig(
        id="mesh",
        allowed_tags=tags,
        allowed_attributes=tag_attributes,
        allowed_attributes_values=tag_attribute_values,
        allowed_url_schemes=url_schemes,
    )

    with override_settings(CKEDITOR_CONFIGS={"mesh": ckeditor_config.javascript_config()}):
        field = CKEditorFormField(editor_config=ckeditor_config)
        for test in sanitizer_tests:
            assert field.clean(test["html"]) == test["expected"]
