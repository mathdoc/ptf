from datetime import date
from datetime import timedelta
from unittest.mock import patch

from django.db import transaction
from django.db.utils import IntegrityError

from ...exceptions import ReviewStateError
from ...models.factories import ReviewFactory
from ...models.factories import UserFactory
from ...models.review_models import Review
from ...models.review_models import ReviewState
from ...models.submission_models import Submission
from ...models.submission_models import SubmissionLog
from ...models.user_models import User
from ..base_test_case import BaseTestCase


class ReviewTestCase(BaseTestCase):
    used_models = [Submission]
    class_used_models = [User]

    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.user_reviewer = UserFactory.create()

    def test_unique_reviewer_per_round(self):
        review = ReviewFactory.create()
        self.assertEqual(review.state, ReviewState.AWAITING_ACCEPTANCE.value)

        # We need to encapsulate the database exception in a transaction, otherwise
        # successive DB operations are not permitted.
        with transaction.atomic():
            self.assertRaises(
                IntegrityError,
                ReviewFactory.create,
                reviewer=review.reviewer,
                version=review.version,
            )

    def test_is_response_overdue(self):
        review = ReviewFactory.create()
        review.date_response_due = date.today()
        self.assertFalse(review.is_response_overdue)

        review.date_response_due -= timedelta(days=1)
        self.assertTrue(review.is_response_overdue)

        review.accepted = True
        self.assertFalse(review.is_response_overdue)

    def test_is_report_overdue(self):
        review = ReviewFactory.create()
        review.date_review_due = date.today()
        review.accepted = False
        review.submitted = False

        with patch.object(Review, "is_response_overdue", False):
            self.assertFalse(review.is_report_overdue)

            review.date_review_due -= timedelta(days=1)
            self.assertFalse(review.is_report_overdue)

            review.accepted = True
            self.assertTrue(review.is_report_overdue)

            review.submitted = True
            self.assertFalse(review.is_report_overdue)

        with patch.object(Review, "is_response_overdue", True):
            self.assertTrue(review.is_report_overdue)

    def test_is_completed(self):
        review = ReviewFactory.create()
        review.accepted = None
        review.submitted = False
        self.assertFalse(review.is_completed)

        review.accepted = True
        self.assertFalse(review.is_completed)

        review.submitted = True
        self.assertTrue(review.is_completed)

        review.submitted = False
        review.accepted = False
        self.assertTrue(review.is_completed)

    def test_is_editable(self):
        review = ReviewFactory.create()
        review.submitted = True
        review.version.review_open = False

        self.assertFalse(review.is_editable)

        review.submitted = False
        self.assertFalse(review.is_editable)

        review.version.review_open = True
        self.assertTrue(review.is_editable)

    def test_accept(self):
        review = ReviewFactory.create()
        accept_value = True
        accept_comment = "<p>My comment</p>"
        request = self.dummy_request()
        request.user = review.reviewer

        self.assertIsNone(review.accepted)
        self.assertEqual(
            len(SubmissionLog.objects.filter(attached_to=review.version.submission)), 0
        )

        with patch.object(Review, "is_editable", False):
            self.assertRaises(
                ReviewStateError, review.accept, accept_value, accept_comment, review.reviewer
            )
        self.assertEqual(
            len(SubmissionLog.objects.filter(attached_to=review.version.submission)), 0
        )
        review = Review.objects.get(pk=review.pk)
        self.assertEqual(review.state, ReviewState.AWAITING_ACCEPTANCE.value)
        self.assertIsNone(review.accepted)

        with patch.object(Review, "is_editable", True):
            review.accept(accept_value, accept_comment, review.reviewer)
        self.assertEqual(
            len(SubmissionLog.objects.filter(attached_to=review.version.submission)), 1
        )
        review = Review.objects.get(pk=review.pk)
        self.assertEqual(review.state, ReviewState.PENDING.value)
        self.assertTrue(review.accepted)
        self.assertEqual(review.accept_comment, accept_comment)

        review = ReviewFactory.create()
        accept_value = False
        accept_comment = "<p>My comment</p>"
        request = self.dummy_request()
        request.user = review.reviewer

        with patch.object(Review, "is_editable", True):
            review.accept(accept_value, accept_comment, review.reviewer)
        self.assertEqual(
            len(SubmissionLog.objects.filter(attached_to=review.version.submission)), 1
        )
        review = Review.objects.get(pk=review.pk)
        self.assertEqual(review.state, ReviewState.DECLINED.value)
        self.assertFalse(review.accepted)

    def test_submit(self):
        review = ReviewFactory.create(accepted=True, state=ReviewState.PENDING.value)
        request = self.dummy_request()
        request.user = review.reviewer

        self.assertFalse(review.submitted)
        self.assertEqual(review.state, ReviewState.PENDING.value)
        self.assertEqual(
            len(SubmissionLog.objects.filter(attached_to=review.version.submission)), 0
        )

        with patch.object(Review, "is_submittable", False):
            self.assertRaises(ReviewStateError, review.submit, review.reviewer)
        self.assertFalse(review.submitted)
        self.assertEqual(review.state, ReviewState.PENDING.value)
        self.assertEqual(
            len(SubmissionLog.objects.filter(attached_to=review.version.submission)), 0
        )

        with patch.object(Review, "is_submittable", True):
            review.submit(review.reviewer)
        self.assertTrue(review.submitted)
        self.assertEqual(review.state, ReviewState.SUBMITTED.value)
        self.assertEqual(
            len(SubmissionLog.objects.filter(attached_to=review.version.submission)), 1
        )
