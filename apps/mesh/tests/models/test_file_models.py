import os
from unittest.mock import patch

from django.core.exceptions import ValidationError
from django.core.files.uploadedfile import SimpleUploadedFile
from django.db import DatabaseError

from ...models.factories import SubmissionVersionFactory
from ...models.file_models import get_upload_path_from_model
from ...models.submission_models import Submission
from ...models.submission_models import SubmissionAdditionalFile
from ..base_test_case import BaseTestCase


class BaseFileWrapperTestCase(BaseTestCase):
    """
    Tests for `BaseFileWrapperModel` class.
    We use the `SubmissionAdditionalFile` class in the tests because
    `BaseFileWrapperModel` is an abstract model.
    """

    used_models = [Submission]

    def setUp(self):
        super().setUp()
        self.reset_user_files_directory()
        self.version = SubmissionVersionFactory.create()

    def test_file_path(self):
        file_wrapper = SubmissionAdditionalFile()
        file_wrapper.attached_to = self.version
        file_wrapper.file = self.dummy_file  # type:ignore
        file_wrapper.save()

        # Check that the original name is saved
        self.assertEqual(file_wrapper.name, self.dummy_file.name)

        # Check that the upload path is the one from get_upload_path
        self.assertEqual(
            SubmissionAdditionalFile.get_upload_path(file_wrapper, self.dummy_file.name),
            file_wrapper.file.name,
        )

        # Check creating a file with the same name
        file_wrapper_2 = SubmissionAdditionalFile()
        file_wrapper_2.attached_to = self.version
        file_wrapper_2.file = self.dummy_file  # type:ignore
        file_wrapper_2.save()

        # Check that the original name is saved
        self.assertEqual(file_wrapper_2.name, self.dummy_file.name)

        # The actual file path/name should be different: the second file having the
        # same initial path and name as the first one, it should have been renamed correctly.
        self.assertNotEqual(
            SubmissionAdditionalFile.get_upload_path(file_wrapper_2, self.dummy_file.name),
            file_wrapper_2.file.name,
        )

    def test_validate_file_extension(self):
        SubmissionAdditionalFile.file_extensions = [".pdf"]
        SubmissionAdditionalFile.validate_file_extension(self.dummy_file)

        SubmissionAdditionalFile.file_extensions = []
        SubmissionAdditionalFile.validate_file_extension(self.dummy_file)

        SubmissionAdditionalFile.file_extensions = [".docx"]
        self.assertRaises(
            ValidationError, SubmissionAdditionalFile.validate_file_extension, self.dummy_file
        )

    def test_validate_file_size(self):
        SubmissionAdditionalFile.file_max_size = 10**6
        SubmissionAdditionalFile.validate_file_size(self.dummy_file)

        SubmissionAdditionalFile.file_max_size = 1
        self.assertRaises(
            ValidationError, SubmissionAdditionalFile.validate_file_size, self.dummy_file
        )

    def test_validate_file_name_length(self):
        SubmissionAdditionalFile.file_name_max_length = 100
        SubmissionAdditionalFile.validate_file_name_length(self.dummy_file)

        self.dummy_file.name = "".join("a" for a in range(97)) + ".pdf"
        self.assertRaises(
            ValidationError, SubmissionAdditionalFile.validate_file_name_length, self.dummy_file
        )

    def test_base_run_file_validators(self):
        SubmissionAdditionalFile.file_extensions = [".pdf"]
        SubmissionAdditionalFile.file_max_size = 10**6
        SubmissionAdditionalFile.file_name_max_length = 100
        SubmissionAdditionalFile.run_file_validators(self.dummy_file)

        SubmissionAdditionalFile.file_extensions = [".docx"]
        self.assertRaises(
            ValidationError, SubmissionAdditionalFile.run_file_validators, self.dummy_file
        )

        SubmissionAdditionalFile.file_extensions = [".pdf"]
        SubmissionAdditionalFile.run_file_validators(self.dummy_file)
        SubmissionAdditionalFile.file_max_size = 1
        self.assertRaises(
            ValidationError, SubmissionAdditionalFile.run_file_validators, self.dummy_file
        )

        SubmissionAdditionalFile.file_max_size = 10**6
        SubmissionAdditionalFile.run_file_validators(self.dummy_file)
        SubmissionAdditionalFile.file_name_max_length = 1
        self.assertRaises(
            ValidationError, SubmissionAdditionalFile.run_file_validators, self.dummy_file
        )

    def test_reverse_file_path(self):
        """
        SHOULD BE TESTED FOR EVERY CLASS inheriting the BaseFileWrapperModel.
        One should test saving a file, then ensuring that reverse_file_path returns
        the correct file when supplied with the file path.
        """
        file_wrapper = SubmissionAdditionalFile()
        file_wrapper.attached_to = self.version
        file_wrapper.file = self.dummy_file  # type:ignore
        file_wrapper.save()

        file_wrapper_request = SubmissionAdditionalFile.reverse_file_path(file_wrapper.file.name)
        self.assertEqual(file_wrapper.pk, file_wrapper_request.pk)  # type:ignore

        path = SubmissionAdditionalFile.get_upload_path(file_wrapper, "UNKNOWN.pdf")
        self.assertIsNone(SubmissionAdditionalFile.reverse_file_path(path))

        self.assertIsNone(SubmissionAdditionalFile.reverse_file_path("my_path/UNKNOWN.pdf"))

    def test_file_update(self):
        file_wrapper = SubmissionAdditionalFile()
        file_wrapper.attached_to = self.version
        file_wrapper.file = self.dummy_file  # type:ignore
        file_wrapper.save()

        path: str = file_wrapper.file.path  # type:ignore
        self.assertTrue(os.path.isfile(path))

        new_file = SimpleUploadedFile(
            "my_new_file.pdf", b"My new contentes", content_type="application/pdf"
        )
        file_wrapper.file = new_file  # type:ignore

        # DB save fails -> the original file should not have been deleted
        with patch("django.db.models.Model.save", side_effect=DatabaseError("Error")):
            self.assertRaises(DatabaseError, file_wrapper.save)
            self.assertTrue(os.path.isfile(path))

        # DB save succeeds -> the original file should have been deleted from
        # the filesystem.
        file_wrapper.save()
        self.assertFalse(os.path.exists(path))
        self.assertTrue(os.path.isfile(file_wrapper.file.path))  # type:ignore

    def test_delete(self):
        file_wrapper = SubmissionAdditionalFile()
        file_wrapper.attached_to = self.version
        file_wrapper.file = self.dummy_file  # type:ignore
        file_wrapper.save()

        path: str = file_wrapper.file.path  # type:ignore
        self.assertTrue(os.path.isfile(path))

        file_wrapper.delete()
        self.assertFalse(os.path.exists(path))

        # Ensure the file is not deleted if the DB deletion fails
        with patch("django.db.models.Model.delete", side_effect=DatabaseError("Error")):
            file_wrapper = SubmissionAdditionalFile()
            file_wrapper.attached_to = self.version
            file_wrapper.file = self.dummy_file  # type:ignore
            file_wrapper.save()

            path: str = file_wrapper.file.path  # type:ignore
            self.assertTrue(os.path.isfile(path))

            self.assertRaises(DatabaseError, file_wrapper.delete)
            self.assertTrue(os.path.isfile(path))

    def test_get_upload_path_from_model(self):
        file_wrapper = SubmissionAdditionalFile()
        file_wrapper.attached_to = self.version
        filename = "my_file.pdf"

        self.assertEqual(
            get_upload_path_from_model(file_wrapper, filename),
            SubmissionAdditionalFile.get_upload_path(file_wrapper, filename),
        )
