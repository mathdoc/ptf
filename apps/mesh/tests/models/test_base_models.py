# from django.test import TestCase

import pytest

from ...models.base_models import BaseChangeTrackingModel
from ...models.factories import UserFactory
from ...models.journal_models import JournalSection


@pytest.mark.django_db
def test_base_change_tracking_model():
    # Test object creation without user attached
    base_model: BaseChangeTrackingModel = JournalSection.objects.create(name="my journal_section")
    assert base_model.created_by is None
    assert base_model.date_created is not None
    assert base_model.last_modified_by is None
    assert base_model.date_last_modified is not None

    # Test update with user attached
    date_created = base_model.date_created
    date_last_modified_1 = base_model.date_last_modified
    user_1 = UserFactory.create()
    base_model._user = user_1
    base_model.save()

    base_model = JournalSection.objects.get(pk=base_model.pk)
    assert base_model.created_by is None
    assert base_model.date_created == date_created
    assert base_model.last_modified_by.pk == user_1.pk  # type:ignore
    assert base_model.date_last_modified > date_last_modified_1  # type:ignore

    # Test update with user attached but do_not_update = True
    date_last_modified_2 = base_model.date_last_modified
    user_2 = UserFactory.create()
    base_model._user = user_2
    base_model.do_not_update = True
    base_model.save()

    base_model = JournalSection.objects.get(pk=base_model.pk)
    assert base_model.created_by is None
    assert base_model.date_created == date_created
    assert base_model.last_modified_by.pk == user_1.pk  # type:ignore
    assert base_model.date_last_modified == date_last_modified_2

    # Test creation of object with user attached
    base_model = JournalSection(name="my other journal_section")
    base_model._user = user_1
    base_model.save()

    base_model = JournalSection.objects.get(pk=base_model.pk)
    assert base_model.created_by.pk == user_1.pk  # type:ignore
    assert base_model.date_created is not None
    assert base_model.last_modified_by.pk == user_1.pk  # type:ignore
    assert base_model.date_last_modified is not None
