from unittest.mock import patch

from django.db import transaction
from django.db.utils import IntegrityError
from django.utils import timezone

from ...exceptions import SubmissionStateError
from ...models.editorial_models import EditorSubmissionRight

# from ...models.factories import ReviewFactory
from ...models.factories import EditorialDecisionFactory
from ...models.factories import SubmissionAuthorFactory
from ...models.factories import SubmissionFactory
from ...models.factories import SubmissionVersionFactory
from ...models.factories import UserFactory
from ...models.submission_models import Submission
from ...models.submission_models import SubmissionAuthor
from ...models.submission_models import SubmissionLog
from ...models.submission_models import SubmissionMainFile
from ...models.submission_models import SubmissionState
from ...models.submission_models import SubmissionVersion
from ...models.user_models import User
from ..base_test_case import BaseTestCase


class SubmissionTestCase(BaseTestCase):
    # Emptying the Submission table will delete all other models thanks to their
    # cascade relationship. All models are related to a Submission somehow, except
    # JournalSection and user models.
    used_models = [Submission]
    class_used_models = [User]

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user_author = UserFactory.create()

    def test_unique_submission_name_per_user(self):
        submission = SubmissionFactory.create(created_by=self.user_author)
        # We need to encapsulate the database exception in a transaction, otherwise
        # successive DB operations are not permitted.
        with transaction.atomic():
            self.assertRaises(
                IntegrityError,
                SubmissionFactory.create,
                name=submission.name,
                created_by=self.user_author,
            )

    def test_date_submission(self):
        submission = SubmissionFactory.create(created_by=self.user_author)
        self.assertNotEqual(submission.date_created, submission.date_first_version)
        self.assertEqual(submission.date_submission, submission.date_created)

        submission.date_first_version = timezone.now()
        submission.save()

        self.assertNotEqual(submission.date_created, submission.date_first_version)
        self.assertEqual(submission.date_submission, submission.date_first_version)

    def test_is_draft(self):
        submission = SubmissionFactory.create(created_by=self.user_author)
        self.assertTrue(submission.is_draft)

        submission.state = SubmissionState.SUBMITTED.value
        self.assertFalse(submission.is_draft)

    def test_version_attributes(self):
        """
        This tests the submission attributes `all_versions` and `current_version`
        along with the `unique_draft_submission_version` constraint
        """
        submission = SubmissionFactory.create(created_by=self.user_author)
        self.assertEqual(len(submission.all_versions), 0)
        self.assertIsNone(submission.current_version)

        version_1 = SubmissionVersion(submission=submission, created_by=self.user_author)
        version_1.save()

        submission = Submission.objects.get(pk=submission.pk)
        self.assertEqual(len(submission.all_versions), 1)
        self.assertEqual(submission.all_versions[0].pk, version_1.pk)
        self.assertEqual(submission.current_version.pk, version_1.pk)  # type:ignore

        version_2 = SubmissionVersion(submission=submission, created_by=self.user_author)
        # Can't save because version_1 is not submitted yet.
        with transaction.atomic():
            self.assertRaises(
                IntegrityError,
                version_2.save,
            )

        version_1.submitted = True
        version_1.save()
        version_2.save()
        submission = Submission.objects.get(pk=submission.pk)
        self.assertEqual(len(submission.all_versions), 2)
        self.assertEqual(submission.all_versions[0].pk, version_2.pk)
        self.assertEqual(submission.all_versions[1].pk, version_1.pk)
        self.assertEqual(submission.current_version.pk, version_2.pk)  # type:ignore

    def test_all_authors(self):
        submission = SubmissionFactory.create()
        self.assertEqual(len(submission.all_authors), 0)

        author_1 = SubmissionAuthorFactory.create(submission=submission, first_name="B")
        author_2 = SubmissionAuthorFactory.create(submission=submission, first_name="A")
        submission = Submission.objects.get(pk=submission.pk)
        self.assertEqual(len(submission.all_authors), 2)
        self.assertEqual(submission.all_authors[0].pk, author_2.pk)
        self.assertEqual(submission.all_authors[1].pk, author_1.pk)

        # Test unique constraint
        with transaction.atomic():
            self.assertRaises(
                IntegrityError,
                SubmissionAuthorFactory.create,
                submission=submission,
                email=author_1.email,
            )

    def test_all_assigned_editors(self):
        submission = SubmissionFactory.create()
        editor_1 = UserFactory.create(first_name="Jean")
        editor_2 = UserFactory.create(first_name="Amélie")
        _ = EditorSubmissionRight.objects.create(user=editor_1, submission=submission)
        _ = EditorSubmissionRight.objects.create(user=editor_2, submission=submission)

        assigned_editors = submission.all_assigned_editors
        self.assertEqual(len(assigned_editors), 2)
        self.assertEqual(assigned_editors[0].pk, editor_2.pk)
        self.assertEqual(assigned_editors[1].pk, editor_1.pk)


class SubmissionWorkflowTestCase(BaseTestCase):
    used_models = [Submission]

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user_author = UserFactory.create()
        cls.user_reviewer = UserFactory.create()
        cls.user_editor = UserFactory.create()
        cls.user_journal_manager = UserFactory.create(journal_manager=True)
        cls.user_admin = UserFactory.create(is_superuser=True)

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        User.objects.all().delete()

    def setUp(self):
        super().setUp()

    def test_is_submittable(self):
        submission = SubmissionFactory.create(created_by=self.user_author)
        self.assertFalse(submission.is_submittable)

        submission.state = SubmissionState.OPENED.value
        submission.author_agreement = True
        submission.save()
        submission_version = SubmissionVersionFactory.create(submission=submission)
        SubmissionMainFile.objects.create(attached_to=submission_version, file=self.dummy_file)
        SubmissionAuthorFactory.create(submission=submission)
        submission = Submission.objects.get(pk=submission.pk)
        self.assertTrue(submission.is_submittable)

        # Test each condition separately
        submission.state = SubmissionState.SUBMITTED.value
        self.assertFalse(submission.is_submittable)

        submission.state = SubmissionState.OPENED.value
        self.assertTrue(submission.is_submittable)
        SubmissionAuthor.objects.all().delete()
        submission = Submission.objects.get(pk=submission.pk)
        self.assertFalse(submission.is_submittable)

        SubmissionAuthorFactory.create(submission=submission)
        submission = Submission.objects.get(pk=submission.pk)
        self.assertTrue(submission.is_submittable)
        submission_version.submitted = True
        submission_version.save()
        submission = Submission.objects.get(pk=submission.pk)
        self.assertFalse(submission.is_submittable)

        submission_version.submitted = False
        submission_version.save()
        submission = Submission.objects.get(pk=submission.pk)
        self.assertTrue(submission.is_submittable)
        SubmissionMainFile.objects.all().delete()
        submission = Submission.objects.get(pk=submission.pk)
        self.assertFalse(submission.is_submittable)

    def test_submit(self):
        submission = SubmissionFactory.create(created_by=self.user_author, author_agreement=True)
        submission_version = SubmissionVersionFactory.create(submission=submission)
        submission = Submission.objects.get(pk=submission.pk)
        self.assertFalse(submission_version.submitted)
        self.assertIsNone(submission_version.date_submitted)
        self.assertEqual(submission.state, SubmissionState.OPENED.value)
        self.assertFalse(SubmissionLog.objects.filter(attached_to=submission).exists())
        request = self.dummy_request()
        request.user = self.user_author
        date_last_activity = submission.date_last_activity  # type:ignore
        self.assertIsNone(date_last_activity)

        # Test submitting non-submittable submission
        with patch.object(Submission, "is_submittable", False):
            self.assertRaises(SubmissionStateError, submission.submit, self.user_author)

        submission = Submission.objects.get(pk=submission.pk)
        self.assertEqual(submission.state, SubmissionState.OPENED.value)
        submission_version: SubmissionVersion = submission.current_version  # type:ignore
        self.assertFalse(submission_version.submitted)
        self.assertFalse(SubmissionLog.objects.filter(attached_to=submission).exists())
        date_last_activity = submission.date_last_activity  # type:ignore
        self.assertIsNone(date_last_activity)

        # Test submiting a submittable OPENED submission
        with patch.object(Submission, "is_submittable", True):
            submission.submit(self.user_author)

        submission = Submission.objects.get(pk=submission.pk)
        self.assertEqual(submission.state, SubmissionState.SUBMITTED.value)
        submission_version: SubmissionVersion = submission.current_version  # type:ignore
        self.assertTrue(submission_version.submitted)
        self.assertIsNotNone(submission_version.date_submitted)
        self.assertEqual(len(SubmissionLog.objects.filter(attached_to=submission).all()), 1)
        date_last_activity = submission.date_last_activity  # type:ignore
        self.assertIsNotNone(date_last_activity)

        # Test submiting a 2nd version of the REVISION_REQUESTED submission
        submission_version.review_open = False
        submission_version.save()
        submission_version_2 = SubmissionVersionFactory.create(submission=submission)
        self.assertFalse(submission_version_2.submitted)
        submission.state = SubmissionState.REVISION_REQUESTED.value
        submission = Submission.objects.get(pk=submission.pk)

        with patch.object(Submission, "is_submittable", True):
            submission.submit(self.user_author)

        submission = Submission.objects.get(pk=submission.pk)
        self.assertEqual(submission.state, SubmissionState.REVISION_SUBMITTED.value)
        current_version: SubmissionVersion = submission.current_version  # type:ignore
        self.assertEqual(current_version.pk, submission_version_2.pk)
        self.assertTrue(current_version.submitted)
        self.assertEqual(len(SubmissionLog.objects.filter(attached_to=submission).all()), 2)
        date_last_activity_2 = submission.date_last_activity  # type:ignore
        self.assertIsNotNone(date_last_activity_2)
        self.assertGreater(date_last_activity_2, date_last_activity)

    def test_is_reviewable(self):
        submission = SubmissionFactory.create(state=SubmissionState.SUBMITTED.value)
        version = SubmissionVersionFactory.create(
            submission=submission, submitted=True, review_open=False
        )
        # Not reviewable if the version has no main file
        submission = Submission.objects.get(pk=submission.pk)
        self.assertFalse(submission.is_reviewable)

        SubmissionMainFile.objects.create(file=self.dummy_file, attached_to=version)
        submission = Submission.objects.get(pk=submission.pk)
        self.assertTrue(submission.is_reviewable)

        # Test all submission state
        submission.state = SubmissionState.REVISION_SUBMITTED.value
        submission.save()
        self.assertTrue(submission.is_reviewable)

        submission.state = SubmissionState.OPENED.value
        submission.save()
        self.assertFalse(submission.is_reviewable)

        submission.state = SubmissionState.ON_REVIEW.value
        submission.save()
        self.assertFalse(submission.is_reviewable)

        submission.state = SubmissionState.REVISION_REQUESTED.value
        submission.save()
        self.assertFalse(submission.is_reviewable)

        submission.state = SubmissionState.ACCEPTED.value
        submission.save()
        self.assertFalse(submission.is_reviewable)

        submission.state = SubmissionState.REJECTED.value
        submission.save()
        self.assertFalse(submission.is_reviewable)

        # Test version parameters
        # Already on review
        submission.state = SubmissionState.SUBMITTED.value
        submission.save()
        self.assertTrue(submission.is_reviewable)
        version.review_open = True
        version.save()
        submission = Submission.objects.get(pk=submission.pk)
        self.assertFalse(submission.is_reviewable)

        version.review_open = False
        version.save()
        submission = Submission.objects.get(pk=submission.pk)
        self.assertTrue(submission.is_reviewable)
        version.submitted = False
        version.save()
        submission = Submission.objects.get(pk=submission.pk)
        self.assertFalse(submission.is_reviewable)

    def test_start_review_process(self):
        submission = SubmissionFactory.create(
            created_by=self.user_author, state=SubmissionState.SUBMITTED.value
        )
        version = SubmissionVersionFactory.create(
            submission=submission, submitted=True, review_open=False
        )
        SubmissionMainFile.objects.create(file=self.dummy_file, attached_to=version)

        submission = Submission.objects.get(pk=submission.pk)

        self.assertFalse(SubmissionLog.objects.filter(attached_to=submission).exists())
        self.assertEqual(submission.state, SubmissionState.SUBMITTED.value)

        request = self.dummy_request()
        request.user = self.user_editor
        with patch.object(Submission, "is_reviewable", False):
            self.assertRaises(
                SubmissionStateError, submission.start_review_process, self.user_editor
            )

        submission = Submission.objects.get(pk=submission.pk)
        version.refresh_from_db()
        self.assertFalse(SubmissionLog.objects.filter(attached_to=submission).exists())
        self.assertEqual(submission.state, SubmissionState.SUBMITTED.value)
        self.assertFalse(version.review_open)

        with patch.object(Submission, "is_reviewable", True):
            submission.start_review_process(self.user_editor)

        submission = Submission.objects.get(pk=submission.pk)
        version.refresh_from_db()
        self.assertEqual(len(SubmissionLog.objects.filter(attached_to=submission)), 1)
        self.assertEqual(submission.state, SubmissionState.ON_REVIEW.value)
        self.assertTrue(version.review_open)

    def test_apply_editorial_decision(self):
        submission = SubmissionFactory.create(
            created_by=self.user_author, state=SubmissionState.ON_REVIEW.value
        )
        version = SubmissionVersionFactory.create(
            submission=submission, submitted=True, review_open=True
        )
        SubmissionMainFile.objects.create(file=self.dummy_file, attached_to=version)

        submission = Submission.objects.get(pk=submission.pk)
        log_count = 0
        self.assertEqual(len(SubmissionLog.objects.filter(attached_to=submission)), log_count)
        self.assertEqual(submission.state, SubmissionState.ON_REVIEW.value)
        self.assertTrue(version.review_open)

        decision = EditorialDecisionFactory.create(
            submission=submission, value=SubmissionState.REVISION_REQUESTED.value
        )
        request = self.dummy_request()
        request.user = self.user_editor
        with patch.object(Submission, "is_draft", True):
            self.assertRaises(
                SubmissionStateError,
                submission.apply_editorial_decision,
                decision,
                self.user_editor,
            )

        submission = Submission.objects.get(pk=submission.pk)
        version.refresh_from_db()
        self.assertEqual(len(SubmissionLog.objects.filter(attached_to=submission)), log_count)
        self.assertEqual(submission.state, SubmissionState.ON_REVIEW.value)
        self.assertTrue(version.review_open)

        with patch.object(Submission, "is_draft", False):
            submission.apply_editorial_decision(decision, self.user_editor)

        submission = Submission.objects.get(pk=submission.pk)
        version.refresh_from_db()
        log_count += 1
        self.assertEqual(len(SubmissionLog.objects.filter(attached_to=submission)), log_count)
        self.assertEqual(submission.state, SubmissionState.REVISION_REQUESTED.value)
        self.assertFalse(version.review_open)

        # Test that an editorial decision is possible from all submisison state
        # SUBMITTED
        submission.state = SubmissionState.SUBMITTED.value
        submission.save()
        with patch.object(Submission, "is_draft", False):
            submission.apply_editorial_decision(decision, self.user_editor)

        submission = Submission.objects.get(pk=submission.pk)
        version.refresh_from_db()
        log_count += 1
        self.assertEqual(len(SubmissionLog.objects.filter(attached_to=submission)), log_count)
        self.assertEqual(submission.state, SubmissionState.REVISION_REQUESTED.value)
        self.assertFalse(version.review_open)

        # REVISION_REQUESTED
        submission.state = SubmissionState.REVISION_REQUESTED.value
        submission.save()
        decision.value = SubmissionState.ACCEPTED.value
        decision.save()
        with patch.object(Submission, "is_draft", False):
            submission.apply_editorial_decision(decision, self.user_editor)

        submission = Submission.objects.get(pk=submission.pk)
        version.refresh_from_db()
        log_count += 1
        self.assertEqual(len(SubmissionLog.objects.filter(attached_to=submission)), log_count)
        self.assertEqual(submission.state, SubmissionState.ACCEPTED.value)
        self.assertFalse(version.review_open)

        # REVISION_SUBMITTED
        submission.state = SubmissionState.REVISION_SUBMITTED.value
        submission.save()
        with patch.object(Submission, "is_draft", False):
            submission.apply_editorial_decision(decision, self.user_editor)

        submission = Submission.objects.get(pk=submission.pk)
        version.refresh_from_db()
        log_count += 1
        self.assertEqual(len(SubmissionLog.objects.filter(attached_to=submission)), log_count)
        self.assertEqual(submission.state, SubmissionState.ACCEPTED.value)
        self.assertFalse(version.review_open)

        # REJECTED
        submission.state = SubmissionState.REJECTED.value
        submission.save()
        with patch.object(Submission, "is_draft", False):
            submission.apply_editorial_decision(decision, self.user_editor)

        submission = Submission.objects.get(pk=submission.pk)
        version.refresh_from_db()
        log_count += 1
        self.assertEqual(len(SubmissionLog.objects.filter(attached_to=submission)), log_count)
        self.assertEqual(submission.state, SubmissionState.ACCEPTED.value)
        self.assertFalse(version.review_open)

        # ACCEPTED
        submission.state = SubmissionState.ACCEPTED.value
        submission.save()
        decision.value = SubmissionState.REJECTED.value
        decision.save()
        with patch.object(Submission, "is_draft", False):
            submission.apply_editorial_decision(decision, self.user_editor)

        submission = Submission.objects.get(pk=submission.pk)
        version.refresh_from_db()
        log_count += 1
        self.assertEqual(len(SubmissionLog.objects.filter(attached_to=submission)), log_count)
        self.assertEqual(submission.state, SubmissionState.REJECTED.value)
        self.assertFalse(version.review_open)
