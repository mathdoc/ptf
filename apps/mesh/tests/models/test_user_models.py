from datetime import timedelta

from django.test import override_settings

from ...models.factories import UserFactory
from ...models.user_models import User
from ...models.user_models import UserToken
from ..base_test_case import BaseTestCase


class UserTestCase(BaseTestCase):
    used_models = [User]

    def test_token_authentication_allowed(self):
        base_user = UserFactory.create()
        self.assertTrue(base_user.is_token_authentication_allowed)

        staff_user = UserFactory.create(is_staff=True)
        self.assertFalse(staff_user.is_token_authentication_allowed)

        admin_user = UserFactory.create(is_staff=True)
        self.assertFalse(admin_user.is_token_authentication_allowed)

        journal_manager_user = UserFactory.create(is_staff=True)
        self.assertFalse(journal_manager_user.is_token_authentication_allowed)


class UserTokenTestCase(BaseTestCase):
    used_models = [UserToken, User]

    def setUp(self):
        super().setUp()
        self.user_base = UserFactory.create()

    @override_settings(MESH_USER_TOKEN_EXPIRATION_DAYS=0)
    def test_expiration_date(self):
        token = UserToken.get_token(self.user_base)
        self.assertIsNotNone(token.date_refreshed)
        self.assertFalse(token.is_expired)

        token.date_refreshed -= timedelta(days=1)
        self.assertTrue(token.is_expired)

    def test_get_token(self):
        self.assertFalse(UserToken.objects.all().exists())
        token = UserToken.get_token(self.user_base)

        self.assertEqual(len(UserToken.objects.all()), 1)
        self.assertEqual(token.user.pk, self.user_base.pk)

        same_token = UserToken.get_token(self.user_base)
        self.assertEqual(same_token.pk, token.pk)
        self.assertEqual(len(UserToken.objects.all()), 1)
