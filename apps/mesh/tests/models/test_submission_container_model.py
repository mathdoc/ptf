from ...models.factories import SubmissionFactory
from ...models.journal_models import JournalSection
from ...models.submission_models import Submission
from ...views.components.tree_node import build_tree_recursive
from ..base_test_case import BaseTestCase


class JournalSectionTestCase(BaseTestCase):
    used_models = [Submission, JournalSection]

    def sort_journal_section_list(
        self, journal_section_list: list[JournalSection]
    ) -> list[JournalSection]:
        """
        Sort a list of submission journal_section by primary key.
        """
        return sorted(journal_section_list, key=lambda c: c.pk)

    def setUp(self):
        """
        JournalSection tree:
        Science
            Maths
            Physics
                Relativity
                Laser
        Arts
        """
        self.journal_section_1 = JournalSection.objects.create(id=1, name="Science")
        self.journal_section_2 = JournalSection.objects.create(id=2, name="Arts")
        self.journal_section_3 = JournalSection.objects.create(
            id=3, name="Physics", parent=self.journal_section_1
        )
        self.journal_section_4 = JournalSection.objects.create(
            id=4, name="Relativity", parent=self.journal_section_3
        )
        self.journal_section_5 = JournalSection.objects.create(
            id=5, name="Maths", parent=self.journal_section_1
        )
        self.journal_section_6 = JournalSection.objects.create(
            id=6, name="Laser", parent=self.journal_section_3
        )

    def test_all_journal_sections(self):
        journal_sections = JournalSection.objects.all_journal_sections()
        self.assertEqual(len(journal_sections), 6)
        for ind, c in enumerate(journal_sections):
            self.assertEqual(c.pk, ind + 1)

    def test_all_journal_sections_children(self):
        journal_sections_dict = JournalSection.objects.all_journal_sections_children()
        expected = {
            None: [self.journal_section_1, self.journal_section_2],
            1: [self.journal_section_3, self.journal_section_5],
            2: [],
            3: [self.journal_section_4, self.journal_section_6],
            4: [],
            5: [],
            6: [],
        }
        self.assertDictEqual(journal_sections_dict, expected)

        self.journal_section_3.delete()
        journal_sections_dict = JournalSection.objects.all_journal_sections_children()
        expected = {
            None: [self.journal_section_1, self.journal_section_2],
            1: [self.journal_section_4, self.journal_section_5, self.journal_section_6],
            2: [],
            4: [],
            5: [],
            6: [],
        }

    def test_all_journal_sections_parent(self):
        journal_sections_dict = JournalSection.objects.all_journal_sections_parents()
        expected = {
            1: None,
            2: None,
            3: self.journal_section_1,
            4: self.journal_section_3,
            5: self.journal_section_1,
            6: self.journal_section_3,
        }
        self.assertDictEqual(journal_sections_dict, expected)

        self.journal_section_3.delete()
        journal_sections_dict = JournalSection.objects.all_journal_sections_parents()
        expected = {
            1: None,
            2: None,
            4: self.journal_section_1,
            5: self.journal_section_1,
            6: self.journal_section_1,
        }
        self.assertDictEqual(journal_sections_dict, expected)

    def test_get_children_recursive(self):
        children = JournalSection.objects.get_children_recursive(None)
        expected = [
            self.journal_section_1,
            self.journal_section_2,
            self.journal_section_3,
            self.journal_section_4,
            self.journal_section_5,
            self.journal_section_6,
        ]
        self.assertListEqual(
            self.sort_journal_section_list(children), self.sort_journal_section_list(expected)
        )

        children = JournalSection.objects.get_children_recursive(self.journal_section_1)
        expected = [
            self.journal_section_3,
            self.journal_section_4,
            self.journal_section_5,
            self.journal_section_6,
        ]
        self.assertListEqual(
            self.sort_journal_section_list(children), self.sort_journal_section_list(expected)
        )

    def test_get_parents_recursive(self):
        parents = JournalSection.objects.get_parents_recursive(None)
        expected = []
        self.assertListEqual(
            self.sort_journal_section_list(parents), self.sort_journal_section_list(expected)
        )

        parents = JournalSection.objects.get_parents_recursive(self.journal_section_6)
        expected = [self.journal_section_1, self.journal_section_3]
        self.assertListEqual(
            self.sort_journal_section_list(parents), self.sort_journal_section_list(expected)
        )

    def test_build_tree_recursive(self):
        # Full tree
        tree = build_tree_recursive(JournalSection.objects)
        self.assertIsNone(tree.item, None)
        self.assertEqual(len(tree.children), 2)

        self.assertEqual(tree.children[0].item, self.journal_section_1)
        self.assertEqual(tree.children[1].item, self.journal_section_2)

        science_tree = tree.children[0]
        self.assertEqual(len(science_tree.children), 2)
        self.assertEqual(science_tree.children[0].item, self.journal_section_3)
        self.assertEqual(science_tree.children[1].item, self.journal_section_5)

        physics_tree = science_tree.children[0]
        self.assertEqual(len(physics_tree.children), 2)
        self.assertEqual(physics_tree.children[0].item, self.journal_section_4)
        self.assertEqual(physics_tree.children[1].item, self.journal_section_6)

        # Partial tree
        physics_tree = build_tree_recursive(JournalSection.objects, self.journal_section_3)
        self.assertEqual(len(physics_tree.children), 2)
        self.assertEqual(physics_tree.children[0].item, self.journal_section_4)
        self.assertEqual(physics_tree.children[1].item, self.journal_section_6)

    def test_clean_cache(self):
        _ = JournalSection.objects.all_journal_sections()
        _ = JournalSection.objects.all_journal_sections_children()
        _ = JournalSection.objects.all_journal_sections_parents()

        self.assertIsNotNone(JournalSection.objects._all_journal_sections)
        self.assertIsNotNone(JournalSection.objects._all_journal_sections_children)
        self.assertIsNotNone(JournalSection.objects._all_journal_sections_parents)

        JournalSection.objects.clean_cache()
        self.assertIsNone(JournalSection.objects._all_journal_sections)
        self.assertIsNone(JournalSection.objects._all_journal_sections_children)
        self.assertIsNone(JournalSection.objects._all_journal_sections_parents)

    def test_update_submission_journal_section(self):
        _ = JournalSection.objects.all_journal_sections()
        _ = JournalSection.objects.all_journal_sections_children()
        _ = JournalSection.objects.all_journal_sections_parents()

        self.journal_section_3.parent = None
        self.journal_section_3.save()

        # Test clean cache
        self.assertIsNone(JournalSection.objects._all_journal_sections)
        self.assertIsNone(JournalSection.objects._all_journal_sections_children)
        self.assertIsNone(JournalSection.objects._all_journal_sections_parents)

        self.journal_section_3.parent = self.journal_section_3
        self.assertRaises(ValueError, self.journal_section_3.save)

        self.journal_section_3.parent = self.journal_section_4
        self.assertRaises(ValueError, self.journal_section_3.save)

        self.journal_section_3.parent = self.journal_section_2
        self.journal_section_3.save()

    def test_delete_submission_journal_section(self):
        submission = SubmissionFactory(journal_section=self.journal_section_3)

        self.assertEqual(submission.journal_section, self.journal_section_3)  # type:ignore

        self.journal_section_3.delete()
        submission = Submission.objects.get(pk=submission.pk)  # type:ignore
        self.assertEqual(submission.journal_section, self.journal_section_1)
