from django.contrib.auth.models import AnonymousUser

from ..interfaces.user_interfaces import ImpersonateData
from ..interfaces.user_interfaces import UserInfo
from ..middleware import process_impersonate_session
from ..models.factories import UserFactory
from ..models.user_models import User
from .base_test_case import BaseTestCase


class ImpersonateMiddlewareTestCase(BaseTestCase):
    used_models = [User]

    def setUp(self):
        super().setUp()
        self.request = self.dummy_request()
        self.request.user = AnonymousUser()

        self.user_admin = UserFactory.create()
        self.user_to_impersonate = UserFactory.create()

        # Serialize the data into the base request session
        self.impersonate_data = ImpersonateData(
            source=UserInfo.from_user(self.user_admin), target_id=self.user_to_impersonate.pk
        )
        self.impersonate_data.serialize(self.request.session)

    def test_processing_impersonate_data_anonymous_user(self):
        process_impersonate_session(self.request)
        self.assertTrue(self.request.user.is_anonymous)

    def test_processing_impersonate_data_normal_use(self):
        self.request.user = self.user_admin
        self.assertEqual(self.request.user.pk, self.user_admin.pk)
        process_impersonate_session(self.request)

        self.assertEqual(self.request.user.pk, self.user_to_impersonate.pk)
        data_from_session = ImpersonateData.from_session(self.request.session)
        self.assertIsNotNone(data_from_session)
        self.assertEqual(data_from_session, self.impersonate_data)

    def test_processing_impersonate_data_user_not_found(self):
        self.user_to_impersonate.delete()
        self.request.user = self.user_admin
        self.assertEqual(self.request.user.pk, self.user_admin.pk)
        process_impersonate_session(self.request)

        self.assertEqual(self.request.user.pk, self.user_admin.pk)
        self.assertIsNone(ImpersonateData.from_session(self.request.session))

    def test_processing_impersonate_data_invalid(self):
        self.impersonate_data.timestamp_start -= 10**6  # type:ignore
        self.impersonate_data.serialize(self.request.session)

        self.request.user = self.user_admin
        self.assertEqual(self.request.user.pk, self.user_admin.pk)
        process_impersonate_session(self.request)

        self.assertEqual(self.request.user.pk, self.user_admin.pk)
        self.assertIsNone(ImpersonateData.from_session(self.request.session))
