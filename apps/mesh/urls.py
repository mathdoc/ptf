from django.urls import path
from django.urls import re_path

from .views.editorial_views import AssignEditorView
from .views.editorial_views import EditorialDecisionCreateView
from .views.editorial_views import EditorialDecisionUpdateView
from .views.editorial_views import SendToReviewView
from .views.file_views import FileServingView
from .views.home import HomeView
from .views.journal_section_views import JournalSectionEditView
from .views.journal_section_views import JournalSectionListView
from .views.review_views import ReviewAcceptView
from .views.review_views import ReviewConfirmView
from .views.review_views import ReviewCreateView
from .views.review_views import ReviewDetails
from .views.review_views import ReviewFileAccessUpdate
from .views.review_views import ReviewSubmitView
from .views.role_views import RoleSwitchView
from .views.submission_edit_views import SubmissionAuthorView
from .views.submission_edit_views import SubmissionConfirmView
from .views.submission_edit_views import SubmissionCreateView
from .views.submission_edit_views import SubmissionResumeView
from .views.submission_edit_views import SubmissionUpdateView
from .views.submission_edit_views import SubmissionVersionCreateView
from .views.submission_edit_views import SubmissionVersionUpdateView
from .views.submission_views import ArchivedSubmissionListView
from .views.submission_views import SubmissionDetailsView
from .views.submission_views import SubmissionListView
from .views.submission_views import SubmissionLogView
from .views.user_views import CloseImpersonateSessionView
from .views.user_views import InitImpersonateSessionView
from .views.user_views import TokenLoginView

urlpatterns = [
    path("", HomeView.as_view(), name="home"),
    # Submission views
    path("submissions/", SubmissionListView.as_view(), name="submission_list"),
    path(
        "submissions/archived/",
        ArchivedSubmissionListView.as_view(),
        name="submission_list_archived",
    ),
    path("submissions/new/", SubmissionCreateView.as_view(), name="submission_create"),
    path("submissions/<int:pk>/update/", SubmissionUpdateView.as_view(), name="submission_update"),
    path("submissions/<int:pk>/resume/", SubmissionResumeView.as_view(), name="submission_resume"),
    path(
        "submission/<int:pk>/submit/confirm/",
        SubmissionConfirmView.as_view(),
        name="submission_confirm",
    ),
    path(
        "submissions/<int:pk>/authors/", SubmissionAuthorView.as_view(), name="submission_authors"
    ),
    path("submissions/<int:pk>/", SubmissionDetailsView.as_view(), name="submission_details"),
    path(
        "submissions/<int:submission_pk>/version/new/",
        SubmissionVersionCreateView.as_view(),
        name="submission_version_create",
    ),
    path(
        "versions/<int:pk>/",
        SubmissionVersionUpdateView.as_view(),
        name="submission_version_update",
    ),
    path("submissions/<int:pk>/log/", SubmissionLogView.as_view(), name="submission_log"),
    path("submissions/<int:pk>/editors/", AssignEditorView.as_view(), name="submission_editors"),
    # Review views
    path(
        "versions/<int:version_pk>/review/new/", ReviewCreateView.as_view(), name="review_create"
    ),
    path("reviews/<int:pk>/", ReviewDetails.as_view(), name="review_details"),
    path("reviews/<int:pk>/accept/", ReviewAcceptView.as_view(), name="review_accept"),
    path("reviews/<int:pk>/update/", ReviewSubmitView.as_view(), name="review_update"),
    path("reviews/<int:pk>/confirm/", ReviewConfirmView.as_view(), name="review_confirm"),
    path(
        "reviews/<int:pk>/files_access_update/",
        ReviewFileAccessUpdate.as_view(),
        name="review_file_access_update",
    ),
    # Editorial views
    path(
        "submissions/<int:pk>/to-review/",
        SendToReviewView.as_view(),
        name="submission_start_review_process",
    ),
    path(
        "submissions/<int:submission_pk>/decision/new/",
        EditorialDecisionCreateView.as_view(),
        name="editorial_decision_create",
    ),
    path(
        "decisions/<int:pk>/update/",
        EditorialDecisionUpdateView.as_view(),
        name="editorial_decision_update",
    ),
    # Role & user views
    path("role_switch/", RoleSwitchView.as_view(), name="role_switch"),
    path("impersonate/", InitImpersonateSessionView.as_view(), name="impersonate_init"),
    path("impersonate/close/", CloseImpersonateSessionView.as_view(), name="impersonate_close"),
    path("login-token/", TokenLoginView.as_view(), name="token_login"),
    # Submission journal_sections
    path(
        "journal_sections/",
        JournalSectionListView.as_view(),
        name="journal_section_list",
    ),
    path(
        "journal_sections/<int:pk>/update/",
        JournalSectionEditView.as_view(),
        name="submission_journal_section_update",
    ),
    # File views
    re_path(
        r"^files/(?P<file_identifier>[A-z0-9-_=]+)$",
        FileServingView.as_view(),
        name="serve_protected_file",
    ),
]
