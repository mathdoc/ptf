from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.base_user import AbstractBaseUser
from django.http.request import HttpRequest

from .models.user_models import UserToken
from .roles.role_handler import ROLE_HANDLER_CACHE
from .roles.role_handler import RoleHandler


class TokenBackend(ModelBackend):
    """
    Authenticates against `UserToken`.
    Only succeeds if the derived user is allowed to use the token authentication.

    Only the authenticate method is overwritten. The standard get_user from an user ID
    stays the same as the default `ModelBackend`.
    """

    def authenticate(
        self, request: HttpRequest, token: str | None = None
    ) -> AbstractBaseUser | None:
        if token is None:
            return None

        try:
            token_obj = UserToken.objects.select_related("user").get(key=token)
        except UserToken.DoesNotExist:
            return None

        if token_obj.is_expired:
            return None

        user = token_obj.user
        # Token authentication might be forbidden at User object level.
        if not getattr(user, "is_token_authentication_allowed", False):
            return None

        # We need the role handler logic in order to derive whether
        # the user is allowed to authenticate using the token method.
        # As this is quite expensive, we cache the `RoleHandler` in the request
        # to avoid having to repopulate it later.
        role_handler = RoleHandler(user, request, partial_init=True)
        if not role_handler.token_authentication_allowed():
            return None

        setattr(request, ROLE_HANDLER_CACHE, role_handler)
        return token_obj.user
