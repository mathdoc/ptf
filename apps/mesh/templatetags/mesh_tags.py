from django import template
from django.http import HttpRequest
from django.template.defaultfilters import stringfilter

from ..file_helpers import file_name as original_file_name
from ..forms.role_forms import RoleSwitchForm
from ..interfaces.user_interfaces import ImpersonateData
from ..roles.role_handler import RoleHandler
from ..views.components.button import get_shortcut_actions

register = template.Library()


@register.filter
@stringfilter
def file_name(file_path: str) -> str:
    """
    Returns the base name of a file from its path.
    """
    return original_file_name(file_path)


@register.filter
@stringfilter
def role_switch_form(role_code: str) -> RoleSwitchForm:
    """
    Returns a RoleSwitchForm whose POST switches the user current role to
    the provided role code.
    """
    return RoleSwitchForm(initial={"role_code": role_code})


@register.simple_tag
def check_rights(role_handler: RoleHandler, function_name: str, *args, **kwargs) -> bool:
    """
    Wrapper around `RoleHandler.check_rights` method for use in template.
    """
    return role_handler.check_rights(function_name, *args, **kwargs)


@register.filter
def get_impersonate_data(request: HttpRequest) -> ImpersonateData | None:
    return ImpersonateData.from_session(request.session)


@register.filter
def get_shortcut_actions_tag(submission_status_data, rights):
    return get_shortcut_actions(submission_status_data, rights)
