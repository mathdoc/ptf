from django.apps import AppConfig


class MyappConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "mesh"

    # Activate if we make use of signals
    # def ready(self) -> None:
    #     from . import signals
