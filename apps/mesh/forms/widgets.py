from __future__ import annotations

from typing import TYPE_CHECKING
from typing import Any

from django import forms
from django.utils.translation import gettext_lazy as _

from ..file_helpers import DELETE_FILE_PREFIX
from ..file_helpers import file_exists

if TYPE_CHECKING:
    from ..models.file_models import BaseFileWrapperModel


class FileInput(forms.FileInput):
    """
    Custom file widget.
    Simply overrides the default template and adds data to the context for rendering.
    """

    # Whether multiple files are allowed.
    allow_multiple_selected: bool
    template_name = "mesh/forms/widgets/file.html"
    model_class: type[BaseFileWrapperModel] | None
    related_name: str | None
    deletable: bool

    def __init__(
        self,
        *args,
        allow_multiple_selected: bool = False,
        model_class: type[BaseFileWrapperModel] | None = None,
        related_name: str | None = None,
        deletable: bool = True,
        **kwargs,
    ) -> None:
        self.allow_multiple_selected = allow_multiple_selected
        self.model_class = model_class
        self.related_name = related_name
        self.deletable = deletable
        super().__init__(*args, **kwargs)

    def get_context(self, name: str, value, attrs) -> dict[str, Any]:
        """
        Add the file(s) to the context if they're all existing files.
        """
        context = super().get_context(name, value, attrs)
        related_name = self.related_name or name
        context["widget"]["delete_prefix"] = f"{DELETE_FILE_PREFIX}{related_name}"
        context["widget"]["multiple"] = self.allow_multiple_selected
        context["widget"]["initial_text"] = (
            _("Choose file(s)...") if self.allow_multiple_selected else _("Choose a file...")
        )

        # Check the validity of the input value
        if value:
            check_func = file_exists
            if self.model_class and hasattr(self.model_class, "instance_valid_file"):
                check_func = self.model_class.instance_valid_file
            if self.allow_multiple_selected:
                if all([check_func(v) for v in value]):
                    context["widget"]["file_value"] = value
            else:
                if check_func(value):
                    context["widget"]["file_value"] = value

        # Check whether existing files can be deleted
        context["deletable"] = self.deletable and (
            not self.is_required or self.allow_multiple_selected and value and len(value) > 1
        )

        return context
