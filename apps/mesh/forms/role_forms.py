from typing import Any

from django import forms

from ..app_settings import app_settings


class RoleSelectForm(forms.Form):
    role_code = forms.ChoiceField(
        choices=[], widget=forms.Select(attrs={"class": "bootstrap-select"})
    )

    def __init__(self, *args, **kwargs):
        role_choices = kwargs.pop("role_choices")
        super().__init__(*args, **kwargs)
        self.fields["role_code"].choices = role_choices


class RoleSwitchForm(forms.Form):
    role_code = forms.CharField(
        max_length=64, strip=True, required=True, widget=forms.HiddenInput()
    )

    def clean(self) -> dict[str, Any]:
        """
        Chosen role is limited to the activated roles of the application.
        """
        cleaned_data = super().clean()
        role_code = cleaned_data["role_code"]
        if role_code not in app_settings.ENABLED_ROLES:
            raise forms.ValidationError(f"Error: the selected role '{role_code}' is not enabled.")
        return cleaned_data
