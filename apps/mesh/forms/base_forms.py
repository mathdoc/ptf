from collections.abc import Collection
from enum import Enum
from enum import unique

from django import forms
from django.utils.translation import gettext_lazy as _

from ..models.file_models import BaseFileWrapperModel
from ..views.components.button import Button
from .fields import FileField


@unique
class FormAction(Enum):
    DELETE = "_action_delete"
    SAVE = "_action_save"
    SUBMIT = "_action_submit"
    SUBMIT_CONFIRM = "_action_submit_confirm"
    NEXT = "_action_next"
    PREVIOUS = "_action_previous"


SUBMIT_QUERY_PARAMETER = "_submit_confirm"


class FileModelForm(forms.ModelForm):
    """
    `ModelForm` template to automatically handle custom `FileField` fields.
    It correctly initializes the fields from the model instance and saves them
    with the foreign key to the form's instance: `attached_to=self.instance`.

    WARNING: A `FileField` declared in the form must have the same name as
    the model attribute/relation or have the `related_name` kwargs
    set to the model's attribute.
    """

    # If auto_handle is False, only process the fields declared in `custom_file_fields`
    custom_file_fields = []
    auto_handle = True

    def __init__(self, *args, **kwargs):
        """
        Init `FileField` from model data.
        """
        super().__init__(*args, **kwargs)
        # Nothing to do if the form instance has never been
        if self.instance._state.adding or not self.instance.pk:
            return

        for field_name, field in self.fields.items():
            if not isinstance(field, FileField) or (
                not self.auto_handle and field_name not in self.custom_file_fields
            ):
                continue

            related_name = field.related_name or field_name
            instance_field = getattr(self.instance, related_name)
            if not field.allow_multiple_selected and not instance_field:
                continue
            elif not field.allow_multiple_selected and instance_field:
                field.initial = instance_field

            # Handle multiple files
            else:
                files = field.filter_initial(instance_field.all())
                if files:
                    field.initial = files

    def save(self, commit=True):
        """
        Handle new files creation on form save. This requires the attribute
        `model_class` to be set on the form field.
        """
        obj = super().save(commit)

        if not commit:
            return obj

        for field_name, field_value in self.cleaned_data.items():
            if not field_value:
                continue

            form_field = self.fields[field_name]
            model_class: type[BaseFileWrapperModel] | None = getattr(
                form_field, "model_class", None
            )
            if (
                model_class is None
                or not isinstance(form_field, FileField)
                or (not self.auto_handle and field_name not in self.custom_file_fields)
            ):
                continue

            # Cast single value to list
            if not form_field.allow_multiple_selected or not isinstance(field_value, Collection):
                field_value = [field_value]

            for temp_file in field_value:
                file_wrapper: BaseFileWrapperModel = model_class(
                    attached_to=self.instance, file=temp_file, **form_field.save_kwargs
                )
                file_wrapper._user = self.instance._user
                file_wrapper.save()

        return obj


DEFAULT_FORM_BUTTONS = [
    Button(
        id="form_save",
        title=_("Save"),
        icon_class="fa-floppy-disk",
        attrs={"type": ["submit"], "class": ["save-button"], "name": [FormAction.SAVE.value]},
    )
]


class MeshFormMixin:
    """
    Base form mixin to be used by every form rendered using `form_full_page.html`
    template.
    """

    required_css_class = "required"
    # The list of form buttons to display at the bottom of the form.
    buttons = [*DEFAULT_FORM_BUTTONS]


class SubmittableModelForm(MeshFormMixin, forms.ModelForm):
    """
    Base form for a submittable model.

    There are 2 submit buttons (save as draft and submit with distinct names).
    """

    is_submittable = True

    def __init__(self, *args, **kwargs):
        self.submit_confirm = kwargs.pop(SUBMIT_QUERY_PARAMETER, False)
        super().__init__(*args, **kwargs)

        # Disable all fields if this is the submit confirm view
        if self.submit_confirm:
            for field in self.fields.values():
                field.disabled = True
                if isinstance(field, FileField):
                    field.widget.deletable = False  # type:ignore

            # Update the form defaut buttons according to the submit status
            self.buttons = [
                Button(
                    id="form_confirm",
                    title=_("Confirm"),
                    icon_class="fa-file-export",
                    attrs={
                        "type": ["submit"],
                        "class": ["button-highlight"],
                        "name": [FormAction.SUBMIT_CONFIRM.value],
                    },
                )
            ]

        else:
            self.buttons = [
                Button(
                    id="form_save",
                    title=_("Save as draft"),
                    icon_class="fa-floppy-disk",
                    attrs={
                        "type": ["submit"],
                        "class": ["save-button", "light"],
                        "name": [FormAction.SAVE.value],
                    },
                ),
                Button(
                    id="form_submit",
                    title=_("Submit"),
                    icon_class="fa-file-export",
                    attrs={
                        "type": ["submit"],
                        "class": ["save-button", "button-highlight"],
                        "name": [FormAction.SUBMIT.value],
                        "data-tooltip": [
                            _(
                                "Save and submit the form. You will no longer be "
                                + "able to edit this form once submitted."
                            )
                        ],
                    },
                ),
            ]


class HiddenModelChoiceForm(forms.Form):
    """
    Generic form to handle a hidden model choice field.
    Mainly used as a Remove/Delete button throughout the app.
    """

    FORM_ACTION = FormAction.SAVE.value
    instance = forms.ModelChoiceField(queryset=None, required=True, widget=forms.HiddenInput)

    def __init__(self, *args, **kwargs):
        self.FORM_ACTION = kwargs.pop("form_action", None) or self.FORM_ACTION
        queryset = kwargs.pop("_queryset")
        super().__init__(*args, **kwargs)
        self.fields["instance"].queryset = queryset
