from __future__ import annotations

from datetime import date
from typing import TYPE_CHECKING
from typing import Any

from allauth.account.models import EmailAddress

from django import forms
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from ..ckeditor_config import DEFAULT_CKEDITOR_CONFIG
from ..ckeditor_config import EMAIL_CKEDITOR_CONFIG
from ..models.review_models import Review
from ..models.review_models import ReviewAdditionalFile
from .base_forms import FileModelForm
from .base_forms import MeshFormMixin
from .base_forms import SubmittableModelForm
from .fields import CKEditorFormField
from .fields import FileField

if TYPE_CHECKING:
    from django.db.models import QuerySet

    from ..models.user_models import User

UserModel = get_user_model()


class ReviewCreateForm(MeshFormMixin, forms.ModelForm):
    """
    Form used to create a new review, ie. request a new review from another user.
    """

    custom_display = "block"
    request_email = CKEditorFormField(EMAIL_CKEDITOR_CONFIG, required=True, label=_("E-mail"))
    request_email_subject = forms.CharField(
        label=_("E-mail subject"), max_length=256, required=True
    )
    reviewer_first_name = forms.CharField(
        label=_("Reviewer - First name"),
        max_length=150,
        required=False,
        help_text=_("Leave empty for an existing user"),
    )
    reviewer_last_name = forms.CharField(
        label=_("Reviewer - Last name"),
        max_length=150,
        required=False,
        help_text=_("Leave empty for an existing user"),
    )
    reviewer_email = forms.EmailField(
        label=_("Reviewer - E-mail address"),
        required=False,
        help_text=_("Leave empty for an existing user"),
    )

    class Meta:
        model = Review
        fields = [
            "reviewer",
            "reviewer_first_name",
            "reviewer_last_name",
            "reviewer_email",
            "request_email_subject",
            "request_email",
            "date_response_due",
            "date_review_due",
        ]

    def __init__(self, *args, **kwargs):
        reviewers: QuerySet[User] = kwargs.pop("_reviewers")
        super().__init__(*args, **kwargs)
        reviewer_field = self.fields["reviewer"]
        reviewer_field.queryset = reviewers
        reviewer_field.required = False
        reviewer_field.help_text = _("Existing user")
        reviewer_field.separator_after_or = True
        self.fields["reviewer_email"].separator_after = True
        self.fields["date_response_due"].widget.input_type = "date"
        self.fields["date_review_due"].widget.input_type = "date"

    def clean_date_response_due(self) -> date:
        date_input = self.cleaned_data["date_response_due"]
        if date_input < date.today():
            raise ValidationError(_("The response due date must be later than today."))

        return date_input

    def clean_date_review_due(self) -> date:
        date_input = self.cleaned_data["date_review_due"]
        if date_input < date.today():
            raise ValidationError(_("The response due date must be later than today."))

        return date_input

    def clean_reviewer_email(self) -> str:
        email = self.cleaned_data["reviewer_email"]
        if not email:
            return email
        if (
            EmailAddress.objects.filter(email=email).exists()
            or UserModel.objects.filter(email=email).exists()
        ):
            raise ValidationError(_("The given e-mail address is already used."))
        return email

    def clean(self) -> dict[str, Any]:
        cleaned_data = super().clean()
        if self.errors:
            return cleaned_data

        # Check dates validity
        response_date = cleaned_data["date_response_due"]
        review_date = cleaned_data["date_review_due"]
        if response_date > review_date:
            raise ValidationError(
                _("The response due date must be earlier than the review due date.")
            )

        # Check reviewer validity (either existing user or name + email fields)
        reviewer = cleaned_data["reviewer"]
        first_name = cleaned_data["reviewer_first_name"]
        last_name = cleaned_data["reviewer_last_name"]
        email = cleaned_data["reviewer_email"]
        if (
            (reviewer and (first_name or last_name or email))
            or not reviewer
            and not (first_name and last_name and email)
        ):
            raise ValidationError(
                _(
                    "User selection error. You must either pick an existing user OR fill the first name, last name and e-mail fields."
                )
            )

        return cleaned_data


class ReviewAcceptForm(MeshFormMixin, forms.ModelForm):
    """
    Accept/decline form for the reviewer.
    """

    custom_display = "block"

    date_response_due_display = forms.DateField(
        label=_("Response due date"), disabled=True, required=False
    )
    date_review_due_display = forms.DateField(
        label=_("Review due date"), disabled=True, required=False
    )
    accept_comment = CKEditorFormField(DEFAULT_CKEDITOR_CONFIG, required=False, label=_("Comment"))

    class Meta:
        model = Review
        fields = [
            "date_response_due_display",
            "date_review_due_display",
            "accepted",
            "accept_comment",
        ]

    def __init__(self, *args, **kwargs):
        """
        Set date fields input type.
        """
        super().__init__(*args, **kwargs)
        self.fields["date_response_due_display"].widget.input_type = "date"
        # self.fields["date_response_due_display"].custom_display = "inline"
        self.fields["date_review_due_display"].widget.input_type = "date"
        # self.fields["date_review_due_display"].custom_display = "inline"

    def clean(self) -> dict[str, Any]:
        cleaned_data = super().clean()
        if cleaned_data["accepted"] is False and not cleaned_data["accept_comment"]:
            raise ValidationError(
                _("You must fill the comment field when declining the review request.")
            )
        return cleaned_data


class ReviewSubmitForm(SubmittableModelForm, FileModelForm):
    """
    Form used to submit/complete a review.
    """

    custom_display = "block"

    date_review_due_display = forms.DateField(
        label=_("Review due date"), disabled=True, required=False
    )
    additional_files = FileField(
        required=True,
        label=_("Review file(s)"),
        allow_multiple_selected=True,
        model_class=ReviewAdditionalFile,
        related_name="additional_files",
    )
    comment = CKEditorFormField(
        DEFAULT_CKEDITOR_CONFIG, label=_("Comment / Message"), required=False
    )

    class Meta:
        model = Review
        fields = [
            "date_review_due_display",
            "recommendation",
            "additional_files",
            "comment",
        ]

    def __init__(self, *args, **kwargs) -> None:
        """
        Sets the date fields input type.
        """
        super().__init__(*args, **kwargs)
        self.fields["date_review_due_display"].widget.input_type = "date"


class ReviewConfirmForm(MeshFormMixin, forms.Form):
    custom_display = "inline"

    confirm = forms.BooleanField(label=_("Confirm"), required=True)
