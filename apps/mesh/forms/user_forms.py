from allauth.account.forms import SignupForm as BaseSignupForm

from django import forms
from django.utils.translation import gettext_lazy as _

from .base_forms import MeshFormMixin


class UserForm(MeshFormMixin, forms.Form):
    custom_display = "inline"
    user = forms.ModelChoiceField(label=_("Target user"), queryset=None, required=True)

    def __init__(self, *args, **kwargs):
        users = kwargs.pop("_users")
        hide_user = kwargs.pop("_hide_user", False)
        user_label = kwargs.pop("_user_label", None)
        super().__init__(*args, **kwargs)

        self.fields["user"].queryset = users
        if hide_user:
            self.fields["user"].widget = forms.HiddenInput()
        if user_label:
            self.fields["user"].label = user_label


class SignupForm(BaseSignupForm):
    first_name = forms.CharField(
        label=_("First name"),
        max_length=32,
        required=True,
        widget=forms.TextInput(
            attrs={"placeholder": _("First name"), "autocomplete": "given-name"}
        ),
    )
    last_name = forms.CharField(
        label=_("Last name"),
        max_length=32,
        required=True,
        widget=forms.TextInput(
            attrs={"placeholder": _("Last name"), "autocomplete": "family-name"}
        ),
    )

    def clean_email(self):
        """
        Raise a validation error if the email is already used.
        With our setup, allauth silences the validation error
        """
        value = super().clean_email()
        if self.account_already_exists:
            raise forms.ValidationError(f"The provided email, {value}, is already used.")
        return value
