from typing import Any

from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from ..ckeditor_config import DEFAULT_CKEDITOR_CONFIG
from ..models.editorial_models import EditorialDecision
from ..models.editorial_models import EditorialDecisionFile
from .base_forms import FileModelForm
from .base_forms import MeshFormMixin
from .fields import CKEditorFormField
from .fields import FileField


class StartReviewProcessForm(forms.Form):
    process = forms.BooleanField(required=True, widget=forms.HiddenInput)

    def clean_process(self) -> bool:
        value: bool = self.cleaned_data["process"]
        if value is False:
            raise ValidationError("Error in the form, `process` boolean must be True")
        return value


class EditorialDecisionCreateForm(MeshFormMixin, FileModelForm):
    custom_display = "inline"

    comment = CKEditorFormField(DEFAULT_CKEDITOR_CONFIG, required=False)
    additional_files = FileField(
        required=False,
        label=_("File(s)"),
        allow_multiple_selected=True,
        model_class=EditorialDecisionFile,
    )

    class Meta:
        model = EditorialDecision
        fields = ["value", "comment", "additional_files"]

    def clean(self) -> dict[str, Any]:
        cleaned_data = super().clean()
        if self.errors:
            return cleaned_data

        if not cleaned_data["comment"] and not cleaned_data["additional_files"]:
            instance: EditorialDecision | None = getattr(self, "instance")
            if not (
                instance
                and instance._state.adding is False
                and instance.additional_files.exists()  # type:ignore
            ):
                raise ValidationError(
                    _(
                        "You must either fill out the description or upload a file to explain your editorial decision."
                    )
                )

        return cleaned_data


class EditorialDecisionUpdateForm(EditorialDecisionCreateForm):
    by = forms.CharField(label=_("Created by"), max_length=1024, disabled=True, required=False)
    date = forms.DateField(label=_("Creation date"), disabled=True, required=False)
    value_display = forms.CharField(label=_("Decision"), disabled=True, required=False)

    class Meta:
        model = EditorialDecision
        fields = ["by", "date", "value_display", "comment", "additional_files"]

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)

        instance: EditorialDecision | None = getattr(self, "instance")
        if instance:
            self.fields["by"].initial = instance.created_by
            self.fields["date"].initial = instance.date_created
            self.fields["value_display"].initial = instance.get_value_display()  # type:ignore
