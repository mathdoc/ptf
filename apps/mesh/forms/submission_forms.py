from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from ..ckeditor_config import SUBMISSION_ABSTRACT_CKEDITOR_CONFIG
from ..ckeditor_config import SUBMISSION_NAME_CKEDITOR_CONFIG
from ..models.journal_models import JournalSection
from ..models.submission_models import Submission
from ..models.submission_models import SubmissionAdditionalFile
from ..models.submission_models import SubmissionAuthor
from ..models.submission_models import SubmissionMainFile
from ..models.submission_models import SubmissionVersion
from .base_forms import FileModelForm
from .base_forms import MeshFormMixin
from .base_forms import SubmittableModelForm
from .fields import CKEditorFormField
from .fields import FileField


class SubmissionEditForm(MeshFormMixin, forms.ModelForm):
    name = CKEditorFormField(editor_config=SUBMISSION_NAME_CKEDITOR_CONFIG)
    abstract = CKEditorFormField(editor_config=SUBMISSION_ABSTRACT_CKEDITOR_CONFIG)

    def __init__(self, *args, **kwargs):
        """
        Remove the `journal_section` field if there's no JournalSection.
        """
        super().__init__(*args, **kwargs)
        if "journal_section" not in self.fields:
            return

        journal_sections = JournalSection.objects.exists()
        if not journal_sections:
            del self.fields["journal_section"]


class SubmissionCreateForm(SubmissionEditForm):
    class Meta:
        model = Submission
        fields = ["journal_section", "name", "abstract", "author_agreement"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields["author_agreement"].required = True
        self.fields["name"].label = "Title"


class SubmissionUpdateForm(SubmissionEditForm):
    class Meta:
        model = Submission
        fields = ["journal_section", "name", "abstract"]


class SubmissionVersionForm(SubmittableModelForm, FileModelForm):
    custom_display = "inline"

    main_file = FileField(required=True, label=_("Main file"), model_class=SubmissionMainFile)

    additional_files = FileField(
        required=False,
        label=_("Additional files"),
        allow_multiple_selected=True,
        model_class=SubmissionAdditionalFile,
    )

    class Meta:
        model = SubmissionVersion
        fields = ["main_file", "additional_files"]


class SubmissionAuthorForm(MeshFormMixin, forms.ModelForm):
    custom_display = "inline"
    submission: Submission

    class Meta:
        model = SubmissionAuthor
        fields = ["first_name", "last_name", "email", "corresponding"]

    def __init__(self, *args, **kwargs):
        self.submission = kwargs.pop("submission")
        super().__init__(*args, **kwargs)

    def clean_email(self) -> str:
        """
        Enforce unique author e-mail per submission.
        """
        email = self.cleaned_data["email"]
        if self.submission.authors.filter(email=email).exists():  # type:ignore
            raise ValidationError(_("There's already an author with the provided e-mail."))
        return email


class SubmissionConfirmForm(MeshFormMixin, forms.Form):
    custom_display = "inline"

    confirm = forms.BooleanField(label=_("Confirm"), required=True)


class JournalSectionForm(MeshFormMixin, forms.ModelForm):
    class Meta:
        model = JournalSection
        fields = ["name", "parent"]

    def __init__(self, *args, **kwargs):
        parents = kwargs.pop("_parents")
        super().__init__(*args, **kwargs)
        self.fields["parent"].queryset = parents
