from __future__ import annotations

from collections.abc import Callable
from collections.abc import Iterable
from typing import TYPE_CHECKING

from ckeditor.fields import RichTextFormField

from django import forms
from django.db.models import QuerySet

from .widgets import FileInput

if TYPE_CHECKING:
    from ..ckeditor_config import CKEditorConfig
    from ..models.file_models import BaseFileWrapperModel


class FileField(forms.FileField):
    """
    Custom `FileField` for forms.
    Should be used only for `BaseFileWrapperModel` objects.

    Our logic for file handling is the following:
    -   A file field is always empty when displaying a form. The initial data, if any,
        is stored in the widget `file_value` field.
    -   The deletion of a file by the user should be handled in the view treating the
        form POST. The deletion process is triggered by a specific input with syntax
        `_delete_file_{field_name}[_{pk}]`.
    """

    allow_multiple_selected: bool
    widget = FileInput
    model_class: type[BaseFileWrapperModel] | None
    save_kwargs: dict
    filter_initial: Callable[[QuerySet], QuerySet]
    related_name: str | None
    deletable: bool

    def __init__(
        self,
        *args,
        allow_multiple_selected=False,
        related_name: str | None = None,
        model_class: type[BaseFileWrapperModel] | None = None,
        save_kwargs: dict = {},
        filter_initial: Callable[[QuerySet], QuerySet] = lambda x: x,
        deletable: bool = True,
        **kwargs,
    ):
        """
        Setup various additional field and widget attributes from the attached model
        class.
        Params:
            - `allow_multiple_selected`  Whether the user can select multiple files.
            - `model_class`     The BaseFileWrapperModel class the field will use to
                                automatically save the submitted data
            - `save_kwargs`     kwargs to be passed to the model_class constructor
            - `filter_initial`  Callable that takes a queryset as parameter and returns
                                a potentially filtered queryset. Used to filter initial
                                data provided to the field.
            - `related_name`    The field's related name to the model class.
            - `deletable`       Whether the file is deletable.
        """
        self.allow_multiple_selected = allow_multiple_selected
        self.model_class = model_class
        self.related_name = related_name
        self.save_kwargs = save_kwargs
        self.filter_initial = filter_initial
        self.deletable = deletable

        kwargs.setdefault(
            "widget",
            self.widget(
                model_class=model_class,  # type:ignore
                related_name=related_name,  # type:ignore
                deletable=self.deletable,  # type:ignore
                allow_multiple_selected=self.allow_multiple_selected,  # type:ignore
            ),
        )
        super().__init__(*args, **kwargs)
        if model_class:
            # Get the file validators from the attached class
            model_validators = getattr(model_class, "file_validators", lambda: [])
            self.validators = [*self.validators, *model_validators()]
            # Get the help text from the attached class
            help_text = getattr(model_class, "get_help_text", lambda: None)()
            if help_text:
                self.help_text = help_text

    def bound_data(self, data, initial):
        """
        The bound data is always the initial data.
        The file field is always empty when displaying a form.
        """
        return initial

    def clean(self, data, initial=None):
        """
        Returns `None` when no file should be created, ie. when there's no input from
        the user.

        If we don't do this, the FileField pipeline will fail because it will get
        our custom data (BaseFileWrapper) instead of a FieldFile.
        """
        if not data or self.disabled:
            return None
        if not self.allow_multiple_selected:
            return super().clean(data, initial)

        single_file_clean = super().clean
        if isinstance(data, Iterable):
            result = [single_file_clean(d, initial) for d in data]
        else:
            result = single_file_clean(data, initial)
        return result

    def has_changed(self, initial, data):
        return not self.disabled and data


class CKEditorFormField(RichTextFormField):
    """
    Variant of the default `RichTextFormField` that automatically sanitizes
    the input value according to its CKEditor config.
    """

    def __init__(self, editor_config: CKEditorConfig, *args, **kwargs):
        self.editor_config = editor_config
        kwargs["config_name"] = editor_config.id
        super().__init__(*args, **kwargs)

    def clean(self, value: str):
        """
        Sanitize the input value before the beginning the cleaning process.
        """
        if value:
            value = self.editor_config.sanitize_value(value)
        return super().clean(value)
