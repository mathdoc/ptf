from typing import Any
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from enum import Enum, unique
from datetime import timedelta
import os


@unique
class BlindMode(Enum):
    NO_BLIND = "no_blind"
    SINGLE_BLIND = "single_blind"
    DOUBLE_BLIND = "double_blind"


class AppSettings:
    """Wrapper for django settings related to this application."""

    REQUIRED_SETTINGS = ["FILES_DIRECTORY"]

    def __init__(self):
        self.prefix = "MESH_"
        for setting in self.REQUIRED_SETTINGS:
            if self._setting(setting, "__NOT_CONFIGURED__") == "__NOT_CONFIGURED__":
                raise ImproperlyConfigured(
                    f"`mesh` - You must set the following setting(s): {setting}"
                )
        if not os.path.isdir(self.FILES_DIRECTORY):
            try:
                os.makedirs(self.FILES_DIRECTORY, mode=511)
            except OSError:
                raise ImproperlyConfigured(
                    f"`mesh` - The provided FILES_DIRECTORY is not an accessible directory and cannot be created automatically: {self.FILES_DIRECTORY}"
                )

    def _setting(self, name: str, default: Any = None):
        """
        Gets the provided setting with the app prefix (see `self.prefix`).

        Params:
            - `name`:       The name of the setting.
            - `default`:    Fallback if the settings is not defined.
        """
        return getattr(settings, self.prefix + name, default)

    @property
    def ENABLED_ROLES(self) -> list[str]:
        """
        List of role codes enabled for this application.
        Defaults to `["author", "journal_manager"]`.
        """
        return self._setting("ENABLED_ROLES", ["author", "reviewer", "editor", "journal_manager"])

    @property
    def BLIND_MODE(self) -> BlindMode:
        """
        Return the blind mode of the application.
        """
        blind_mode = self._setting("BLIND_MODE", BlindMode.SINGLE_BLIND.value)
        try:
            return BlindMode(blind_mode)
        except ValueError:
            raise ImproperlyConfigured(
                f"Invalid setting value for BLIND_MODE: {blind_mode}. "
                f"Available values are: {[b.value for b in BlindMode]}."
            )

    @property
    def FILES_DIRECTORY(self) -> str:
        """
        Directory used to store all user files.
        Files are served using django-sendfile package (XSendFile with Apache).
        This should lie outside of Django directory.
        The directory is automatically created on startup if required.
        """
        return self._setting("FILES_DIRECTORY")

    @property
    def USER_TOKEN_EXPIRATION_DAYS(self) -> timedelta:
        """
        Expiration time (in days) when an user token is delivered.
        """
        return timedelta(days=self._setting("USER_TOKEN_EXPIRATION_DAYS", 180))

    @property
    def EMAIL_PREFIX(self) -> str:
        """
        Prefix for every e-mail manually sent by the MESH application.
        Defaults to `[MESH] `.
        """
        prefix = self._setting("EMAIL_PREFIX", "[MESH] ")
        if prefix[-1] != " ":
            prefix = f"{prefix} "
        return prefix

    @property
    def JOURNAL_EMAIL_CONTACT(self) -> str:
        return self._setting("JOURNAL_EMAIL_CONTACT", "")


app_settings = AppSettings()
