from __future__ import annotations

import os
import re
from typing import TYPE_CHECKING

from django.contrib import messages
from django.core.files.storage import FileSystemStorage
from django.db.models import Model
from django.db.models import QuerySet
from django.db.models.fields.files import FieldFile
from django.http import HttpRequest
from django.utils.translation import gettext_lazy as _

from .app_settings import app_settings

if TYPE_CHECKING:
    from .roles.role_handler import RoleHandler


DELETE_FILE_PREFIX = "_delete_file_"

FILE_NAME_MAX_LENGTH = 1024
FILE_DELETE_DEFAULT_ERROR = _("The requested field can not be deleted.")


def file_name(file_path: str | None) -> str:
    """
    Returns the base name of a file from its path.
    """
    if file_path is None:
        return ""
    return os.path.basename(file_path)


def file_exists(f: FieldFile) -> bool:
    """
    Returns whether the FieldFile represents an actual file.
    """
    return bool(f and getattr(f, "url", False))


class MeshFileSystemStorage(FileSystemStorage):
    """
    The base location of the files corresponds to the setting FILES_DIRECTORY.
    We make use of Django built-in get_available_name to generate an available
    name for the file to be saved.
    """

    def __init__(self, **kwargs):
        """
        Overrides the default storage location from MEDIA_ROOT to FILES_DIRECTORY
        setting.
        """
        kwargs["location"] = app_settings.FILES_DIRECTORY
        super().__init__(**kwargs)

    def get_available_name(self, name, max_length=None):
        """
        Built-in get_available_name with our max length constant.
        """
        max_length = max_length or FILE_NAME_MAX_LENGTH
        return super().get_available_name(name, max_length=max_length)


def post_delete_model_file(
    instance: Model, request: HttpRequest, role_handler: RoleHandler, add_message=True
) -> tuple[bool, bool]:
    """
    Checks the given POST request for specific input used for file deletion.
    Deletes the requested file, if any, if the field is deletable and not required.

    This handles direct FileFields and attached models in a ManyToOne or OneToOne
    relationship.

    Returns:
        - `bool`  whether a file deletion was requested
        - `bool`  whether the file or model was successfully deleted or not.
    """
    # Check for specific delete entry in the post request
    deletion_requested = False
    deletion_performed = False

    delete_regex = rf"{re.escape(DELETE_FILE_PREFIX)}(?P<field_details>.+)$"

    matched = False
    match = None
    for a in request.POST.keys():
        match = re.match(delete_regex, a)
        if match:
            matched = True
            break

    if not matched:
        return deletion_requested, deletion_performed

    deletion_requested = True

    # Additionally split the field details:
    # It's either just the field name or the field name with a pk: `{field_name}_{pk}`
    field_details = match.group("field_details")  # type:ignore
    field_name = field_details
    field_pk = None
    detail_match = re.match(r"(?P<field_name>.+?)_(?P<field_pk>[0-9]+)", field_details)
    if detail_match:
        field_name = detail_match.group("field_name")
        field_pk = int(detail_match.group("field_pk"))

    # Check that the requested field can be deleted
    can_delete_file = getattr(
        instance, "can_delete_file", lambda x: (False, FILE_DELETE_DEFAULT_ERROR)
    )
    result, error_msg = can_delete_file(field_name)
    if result is False:
        messages.error(request, error_msg)
        return deletion_requested, deletion_performed

    # Get the requested entity to delete
    model_to_delete: Model | None = None

    # If field_pk is not None, we are requested to delete a related model in
    # a ManyToOne relationship.
    if field_pk:
        try:
            # Check if the field is required from the model custom attribute
            required_fields = getattr(instance, "file_fields_required", [])
            entities: QuerySet[Model] = getattr(instance, field_name).all()
            if field_name in required_fields:
                # If the field is required, check that there are more than one related
                # entities. Otherwise abort.
                if len(entities) < 2:
                    messages.error(
                        request,
                        "This field is required. Add another file, save the form, "
                        "then come back to delete this file.",
                    )
            model_to_delete = [entity for entity in entities if entity.pk == field_pk][0]
        except Exception:
            if add_message:
                messages.error(request, "Something went wrong.")

    # Else, we are requested to delete the file which is either a FileField
    # or a Model in a OneToOne relationship.
    else:
        try:
            # Check if the field is required from the model custom attribute
            required_fields = getattr(instance, "file_fields_required", [])
            if field_details in required_fields:
                if add_message:
                    messages.error(
                        request,
                        "This field is required. You can not delete the file. "
                        "Try replacing it using the file picker.",
                    )
                return deletion_requested, deletion_performed
            file = getattr(instance, field_details)

            # FileField - Handle directly
            if isinstance(file, FieldFile):
                if file.field.null is False:
                    messages.error(request, "This field is required. You can not delete the file.")
                    return deletion_requested, deletion_performed
                file.delete(save=False)
                setattr(instance, field_details, None)
                instance.save()
                deletion_performed = True

            # Attached model - Handled later
            elif isinstance(file, Model):
                model_to_delete = file

        except Exception:
            if add_message:
                messages.error(request, "Something went wrong.")

    # Delete the requested Model
    if model_to_delete:
        # Check if there's a restriction to delete the model
        check_access_right = getattr(model_to_delete, "check_access_right", None)
        if check_access_right and not check_access_right(role_handler, "delete"):
            messages.error(request, "You don't have the right to delete the requested file.")
            return deletion_requested, deletion_performed

        model_to_delete.delete()
        deletion_performed = True

    if deletion_performed and add_message:
        messages.success(request, "The file was successfully deleted.")

    return deletion_requested, deletion_performed
