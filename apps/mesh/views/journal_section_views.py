from typing import Any

from django.contrib import messages
from django.http import HttpRequest
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic.edit import CreateView
from django.views.generic.edit import UpdateView

from ..forms.base_forms import DEFAULT_FORM_BUTTONS
from ..forms.base_forms import FormAction
from ..forms.submission_forms import JournalSectionForm
from ..mixins import RoleMixin
from ..models.journal_models import JournalSection
from .components.breadcrumb import get_base_breadcrumb
from .components.button import Button
from .components.tree_node import build_tree_recursive


class JournalSectionListView(RoleMixin, CreateView):
    """
    View to display all existing sections of the journal.
    """

    model = JournalSection
    form_class = JournalSectionForm
    template_name = "mesh/journal_section_list.html"

    def get_form(self, form_class=None) -> JournalSectionForm | None:
        if not self.role_handler.check_rights("can_edit_journal_sections"):
            return None
        return super().get_form(form_class)  # type:ignore

    def get_form_kwargs(self) -> dict[str, Any]:
        kwargs = super().get_form_kwargs()
        kwargs["_parents"] = JournalSection.objects.all_journal_sections()
        return kwargs

    def restrict_dispatch(self, request: HttpRequest, *args, **kwargs) -> bool:
        if not self.role_handler.check_rights("can_access_journal_sections"):
            return True

        if request.method == "POST" and not self.role_handler.check_rights(
            "can_edit_journal_sections"
        ):
            return True

        return False

    def get_success_url(self) -> str:
        return reverse_lazy("mesh:journal_section_list")

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)

        context["journal_sections_tree"] = build_tree_recursive(JournalSection.objects)
        context["page_title"] = _("Journal sections")

        breadcrumb = get_base_breadcrumb()
        breadcrumb.add_item(
            title=_("Journal sections"), url=reverse_lazy("mesh:journal_section_list")
        )
        context["breadcrumb"] = breadcrumb

        return context

    def form_valid(self, form) -> HttpResponse:
        form.instance._user = self.request.user
        return super().form_valid(form)


class JournalSectionEditView(RoleMixin, UpdateView):
    model = JournalSection
    form_class = JournalSectionForm
    journal_section: JournalSection
    template_name = "mesh/forms/submission_journal_section_update.html"

    def restrict_dispatch(self, request: HttpRequest, *args, **kwargs) -> bool:
        if not self.role_handler.check_rights("can_edit_journal_sections"):
            return True
        self.journal_section = get_object_or_404(JournalSection, pk=kwargs["pk"])
        return False

    def get_success_url(self) -> str:
        return reverse_lazy("mesh:journal_section_list")

    def get_form(self, form_class=None) -> JournalSectionForm:
        form: JournalSectionForm = super().get_form(form_class)  # type:ignore
        form.buttons = [
            *DEFAULT_FORM_BUTTONS,
            Button(
                id="form_delete",
                title=_("Delete"),
                icon_class="fa-trash",
                attrs={
                    "type": ["submit"],
                    "class": ["button-error"],
                    "name": [FormAction.DELETE.value],
                },
            ),
        ]
        return form

    def get_form_kwargs(self) -> dict[str, Any]:
        kwargs = super().get_form_kwargs()
        kwargs["_parents"] = JournalSection.objects.all_journal_sections().exclude(
            pk__in=[*[c.pk for c in self.journal_section.all_children()], self.journal_section.pk]
        )
        return kwargs

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)

        context["journal_section"] = self.journal_section
        context["journal_sections_tree"] = build_tree_recursive(
            JournalSection.objects, self.journal_section
        )

        parent = self.journal_section.parent
        if parent is None:
            title_button = Button(
                id="title-back-to",
                title=_("Back to index"),
                icon_class="fa-folder-tree",
                attrs={
                    "href": [reverse_lazy("mesh:journal_section_list")],
                    "class": ["as-button"],
                },
            )
        else:
            title_button = Button(
                id="title-back-to",
                title=_("Back to") + f" {parent.name}",
                icon_class="fa-arrow-right-long",
                attrs={
                    "href": [
                        reverse_lazy(
                            "mesh:submission_journal_section_update", kwargs={"pk": parent.pk}
                        )
                    ],
                    "class": ["as-button"],
                },
            )
        context["title_buttons"] = [title_button]

        # Generate breadcrumb
        breadcrumb = get_base_breadcrumb()
        breadcrumb.add_item(
            title=_("Journal sections"), url=reverse_lazy("mesh:journal_section_list")
        )
        breadcrumb.add_item(
            title=self.journal_section.name,
            url=reverse_lazy(
                "mesh:submission_journal_section_update", kwargs={"pk": self.journal_section.pk}
            ),
        )
        context["breadcrumb"] = breadcrumb
        return context

    def post(self, request: HttpRequest, *args, **kwargs) -> HttpResponse:
        if FormAction.DELETE.value in request.POST:
            self.journal_section.delete()
            messages.success(
                request,
                _(f"The journal_section {self.journal_section.name} was successfully deleted."),
            )
            return HttpResponseRedirect(self.get_success_url())
        return super().post(request, *args, **kwargs)

    def form_valid(self, form: JournalSectionForm):
        form.instance._user = self.request.user
        return super().form_valid(form)
