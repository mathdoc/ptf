import re
from typing import Any

from django.contrib import messages
from django.db.models import QuerySet
from django.http import HttpRequest
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import FormView
from django.views.generic import TemplateView
from django.views.generic import View
from django.views.generic.edit import CreateView
from django.views.generic.edit import UpdateView

from ptf.url_utils import format_url_with_params

from ..file_helpers import post_delete_model_file
from ..forms.base_forms import FormAction
from ..forms.review_forms import ReviewAcceptForm
from ..forms.review_forms import ReviewConfirmForm
from ..forms.review_forms import ReviewCreateForm
from ..forms.review_forms import ReviewSubmitForm
from ..mixins import RoleMixin
from ..models.review_models import Review
from ..models.review_models import ReviewAdditionalFile
from ..models.review_models import ReviewState
from ..models.submission_models import SubmissionLog
from ..models.submission_models import SubmissionVersion
from ..models.user_models import User
from ..roles.editor import Editor
from ..roles.journal_manager import JournalManager
from ..roles.reviewer import Reviewer
from .base_views import SubmittableModelFormMixin
from .components.breadcrumb import get_submission_breadcrumb
from .components.button import Button
from .model_proxy import ReviewProxy
from .utils import create_new_user
from .utils import get_review_request_email
from .utils import send_review_request_email


class ReviewCreateView(RoleMixin, CreateView):
    """
    View to invite a new reviewer for a given round.
    The review form is filled with the submission version from the URL.
    """

    model = Review
    form_class = ReviewCreateForm
    template_name = "mesh/forms/form_full_page.html"
    version: SubmissionVersion
    restricted_roles = [JournalManager, Editor]

    def restrict_dispatch(self, request: HttpRequest, *args, **kwargs):
        round_pk = kwargs["version_pk"]
        self.version = get_object_or_404(SubmissionVersion, pk=round_pk)

        return not self.role_handler.check_rights("can_invite_reviewer", self.version)

    def get_form_kwargs(self) -> dict[str, Any]:
        kwargs = super().get_form_kwargs()
        current_reviewers = list(
            Review.objects.filter(version=self.version).values_list("reviewer__pk", flat=True)
        )
        kwargs["_reviewers"] = User.objects.exclude(pk__in=current_reviewers)
        return kwargs

    def get_initial(self) -> dict[str, Any]:
        return {
            "request_email": get_review_request_email(
                self.version.submission, self.request.build_absolute_uri("/")
            ),
            "request_email_subject": _("New referee request"),
        }

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["page_title"] = "Request review"

        submission = self.version.submission
        submission_url = reverse_lazy("mesh:submission_details", kwargs={"pk": submission.pk})
        description = "Please fill the form below to request an additional review for "
        description += f"round #{self.version.number} of submission "
        description += f"<a href='{submission_url}'><i>{submission.name}</i></a>."
        context["form_description"] = [description]

        # Generate breadcrumb data
        breadcrumb = get_submission_breadcrumb(submission)
        breadcrumb.add_item(
            title=_("Request review"),
            url=reverse_lazy("mesh:review_create", kwargs={"version_pk": self.version.pk}),
        )
        context["breadcrumb"] = breadcrumb

        return context

    def form_valid(self, form: ReviewCreateForm) -> HttpResponse:
        """
        Inject required data to the form instance before saving model.
        """
        form.instance.version = self.version
        form.instance._user = self.request.user

        # Check if the review is attached to an existing user or if we should
        # create a new one.
        if not form.cleaned_data["reviewer"]:
            first_name = form.cleaned_data["reviewer_first_name"]
            last_name = form.cleaned_data["reviewer_last_name"]
            email = form.cleaned_data["reviewer_email"]

            reviewer = create_new_user(email, first_name, last_name)
            form.instance.reviewer = reviewer

        response = super().form_valid(form)

        review: Review = self.object  # type:ignore
        send_review_request_email(
            review,
            form.cleaned_data["request_email"],
            form.cleaned_data["request_email_subject"],
            self.request.build_absolute_uri("/"),
        )

        messages.success(
            self.request, _(f"Referee request successfully submitted to {review.reviewer}")
        )

        SubmissionLog.add_message(
            self.version.submission,
            content=_("Referee request sent to") + f" {review.reviewer}",
            content_en=f"Referee request sent to {review.reviewer}",
            user=self.request.user,
            significant=True,
        )

        return response

    def get_success_url(self) -> str:
        return reverse_lazy("mesh:submission_details", kwargs={"pk": self.version.submission.pk})


class ReviewEditBaseView(RoleMixin, UpdateView):
    """
    Base view for a reviewer to edit a review.
    """

    model = Review
    template_name = "mesh/forms/form_full_page.html"
    review: Review
    restricted_roles = [Reviewer]

    def restrict_dispatch(self, request: HttpRequest, *args, **kwargs):
        pk = kwargs["pk"]
        self.review = get_object_or_404(Review, pk=pk)

        return not self.role_handler.check_rights("can_edit_review", self.review)

    def get_object(self, *args, **kwargs) -> Review:
        """
        Override `get_object` built-in method to avoid an additional look-up when
        we already have the object loaded.
        """
        return self.review

    def get_initial(self) -> dict[str, Any]:
        return {
            "date_response_due_display": self.review.date_response_due,
            "date_review_due_display": self.review.date_review_due,
        }


class ReviewAcceptView(ReviewEditBaseView):
    """
    Preliminary view for a reviewer to accept/decline a requested review.
    """

    form_class = ReviewAcceptForm

    def get_initial(self) -> dict[str, Any]:
        initial = super().get_initial()
        accept_initial = self.request.GET.get("accepted", None)
        if accept_initial == "true":
            initial["accepted"] = True
        elif accept_initial == "false":
            initial["accepted"] = False
        return initial

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["page_title"] = "Accept/decline review request"

        submission = self.review.version.submission
        submission_url = reverse_lazy("mesh:submission_details", kwargs={"pk": submission.pk})
        # TODO: Create a custom template instead of passing HTML in variable ?
        description_1 = f"{self.review.created_by} requested your review for the submission "
        description_1 += f"<a href='{submission_url}'><i>{submission.name}</i></a>."
        description_2 = "Please accept or decline the review request by filling the below form."
        description_3 = "You must accept the request before proceeding with the actual review."
        context["form_description"] = [description_1, description_2, description_3]

        # Generate breadcrumb data
        breadcrumb = get_submission_breadcrumb(submission)
        breadcrumb.add_item(
            title=_("Accept review"),
            url=reverse_lazy("mesh:review_accept", kwargs={"pk": self.review.pk}),
        )
        context["breadcrumb"] = breadcrumb

        return context

    def get_success_url(self) -> str:
        if self.review.state == ReviewState.PENDING.value:
            return reverse_lazy("mesh:review_update", kwargs={"pk": self.review.pk})

        return reverse_lazy(
            "mesh:submission_details", kwargs={"pk": self.review.version.submission.pk}
        )

    def form_valid(self, form: ReviewAcceptForm) -> HttpResponse:
        accept_value = form.cleaned_data["accepted"]
        accept_comment = form.cleaned_data["accept_comment"]

        self.review.accept(accept_value, accept_comment, user=self.request)

        messages.success(
            self.request,
            _("Review request successfully accepted")
            if accept_value
            else _("Review request successfully declined"),
        )

        return HttpResponseRedirect(self.get_success_url())


class ReviewSubmitView(SubmittableModelFormMixin, ReviewEditBaseView):
    """
    View for performing a review on a sumbission version.
    """

    form_class = ReviewSubmitForm
    add_confirm_message = False

    def get(self, request: HttpRequest, *args, **kwargs) -> HttpResponse:
        """
        Redirect to the accept form if the review has not been accepted yet.
        """
        if self.review.state in [
            ReviewState.AWAITING_ACCEPTANCE.value,
            ReviewState.DECLINED.value,
        ]:
            # Forward all query parameters
            return HttpResponseRedirect(
                format_url_with_params(
                    reverse("mesh:review_accept", kwargs={"pk": self.review.pk}),
                    {k: v for k, v in request.GET.lists()},
                )
            )
        return super().get(request, *args, **kwargs)

    def get_success_url(self) -> str:
        if FormAction.SUBMIT.value in self.request.POST:
            return self.submit_url()
        return reverse_lazy(
            "mesh:submission_details", kwargs={"pk": self.review.version.submission.pk}
        )

    def submit_url(self) -> str:
        return reverse_lazy("mesh:review_confirm", kwargs={"pk": self.review.pk})

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["page_title"] = "Submit review"

        submission = self.review.version.submission
        submission_url = reverse_lazy("mesh:submission_details", kwargs={"pk": submission.pk})
        # TODO: Create a custom template instead of passing HTML in variable
        description = f"Please submit your review for round #{self.review.version.number} "
        description += "of submission "
        description += f"<a href='{submission_url}'><i>{submission.name}</i></a> "
        description += "by filling the below form."
        context["form_description"] = [description]

        # Generate breadcrumb data
        breadcrumb = get_submission_breadcrumb(submission)
        breadcrumb.add_item(
            title=_("Submit review"),
            url=reverse_lazy("mesh:review_update", kwargs={"pk": self.review.pk}),
        )
        context["breadcrumb"] = breadcrumb

        return context

    def form_pre_save(self, form: ReviewSubmitForm) -> None:
        form.instance._user = self.request.user

    def post(self, request: HttpRequest, *args, **kwargs) -> HttpResponse:
        """
        View for updating the review or deleting one of the associated files.
        """
        deletion_requested, _ = post_delete_model_file(self.review, request, self.role_handler)

        if deletion_requested:
            self.review = get_object_or_404(Review, pk=self.review.pk)
            return self.get(request, *args, **kwargs)

        return super().post(request, *args, **kwargs)


class ReviewConfirmView(RoleMixin, FormView):
    """
    View to confirm the submission of a review.
    """

    review: Review
    template_name = "mesh/review/review_confirm.html"
    form_class = ReviewConfirmForm

    def restrict_dispatch(self, request: HttpRequest, *args, **kwargs):
        pk = kwargs["pk"]
        self.review = get_object_or_404(Review, pk=pk)

        return not self.role_handler.check_rights("can_submit_review", self.review)

    def get_success_url(self):
        return reverse_lazy(
            "mesh:submission_details", kwargs={"pk": self.review.version.submission.pk}
        )

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)

        context["form"].buttons = [
            Button(
                id="form_save",
                title=_("Submit"),
                icon_class="fa-check",
                attrs={"type": ["submit"], "class": ["save-button"]},
            )
        ]

        context["review_proxy"] = ReviewProxy(self.review, self.role_handler)

        files = []
        for file_wrapper in self.review.additional_files.all():  # type:ignore
            files.append({"file_wrapper": file_wrapper, "type": "Review file"})
        context["review_files"] = files

        # Breadcrumb
        breadcrumb = get_submission_breadcrumb(self.review.version.submission)
        breadcrumb.add_item(
            title=_("Submit review"),
            url=reverse_lazy("mesh:review_update", kwargs={"pk": self.review.pk}),
        )
        context["breadcrumb"] = breadcrumb

        return context

    def form_valid(self, form: ReviewConfirmForm) -> HttpResponse:
        self.review.submit(self.request.user)

        messages.success(self.request, _("Your review has been successfully submitted."))

        return HttpResponseRedirect(self.get_success_url())


REVIEW_FILE_INPUT_PREFIX = "review_file_"
REVIEW_FILE_INPUT_VALUE = "true"


class ReviewDetails(RoleMixin, TemplateView):
    """
    Details view for a review.
    """

    template_name = "mesh/review/review_details.html"
    review: Review

    def restrict_dispatch(self, request: HttpRequest, *args, **kwargs) -> bool:
        self.review = get_object_or_404(Review, pk=kwargs["pk"])

        return not self.role_handler.check_rights("can_access_review", self.review)

    def get_context_data(self, *args, **kwargs) -> dict[str, Any]:
        context = super().get_context_data(*args, **kwargs)
        context["review_proxy"] = ReviewProxy(self.review, self.role_handler)
        context["input_prefix"] = REVIEW_FILE_INPUT_PREFIX
        context["input_value"] = REVIEW_FILE_INPUT_VALUE

        breadcrumb = get_submission_breadcrumb(self.review.version.submission)
        breadcrumb.add_item(
            title=_("Review details") + f" - {self.review.reviewer}",
            url=reverse_lazy("mesh:review_details", kwargs={"pk": self.review.pk}),
        )
        context["breadcrumb"] = breadcrumb
        return context


class ReviewFileAccessUpdate(RoleMixin, View):
    """
    View to edit the author access to a review's files.
    """

    review: Review
    form_cache: str

    def restrict_dispatch(self, request: HttpRequest, *args, **kwargs) -> bool:
        self.review = get_object_or_404(Review, pk=kwargs["pk"])

        return not self.role_handler.check_rights("can_edit_review_file_right", self.review)

    def post(self, request: HttpRequest, *args, **kwargs) -> HttpResponse:
        response = HttpResponseRedirect(
            reverse_lazy("mesh:review_details", kwargs={"pk": self.review.pk})
        )
        files: QuerySet[ReviewAdditionalFile] = self.review.additional_files.all()  # type:ignore
        if not files:
            return response
        files: dict[int, ReviewAdditionalFile] = {f.pk: f for f in files}  # type:ignore

        # Check the POST data and look for the file inputs. Update data accordingly
        field_regex = re.compile(f"{REVIEW_FILE_INPUT_PREFIX}(?P<pk>[0-9]+)$")
        files_handled: list[int] = []
        files_changed: list[ReviewAdditionalFile] = []
        for field, values in request.POST.lists():
            field_match = field_regex.match(field)
            if not field_match:
                continue
            if values[0] != REVIEW_FILE_INPUT_VALUE:
                continue
            file_pk = int(field_match.group("pk"))
            file = files.get(file_pk)
            if file is None:
                continue
            # The boolean already has the correct value
            if file.author_access:
                files_handled.append(file_pk)
                continue
            file.author_access = True
            file.save()
            files_handled.append(file_pk)
            files_changed.append(file)

        # There is no input in the POST data for unchecked checkboxes
        for file_pk, file in files.items():
            if file_pk not in files_handled:
                if not file.author_access:
                    continue
                file.author_access = False
                file.save()
                files_changed.append(file)

        messages.success(request, _("Successfully updated the author access right."))

        changed_files_str = ", ".join([f.name for f in files_changed])
        if changed_files_str:
            message = f"Changed author access right to Round #{ self.review.version.number }"
            message += f" files: { changed_files_str }"
            SubmissionLog.add_message(
                self.review.version.submission,
                content=message,
                content_en=message,
                user=self.request.user,
                significant=True,
            )

        return response
