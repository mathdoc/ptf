from collections.abc import Iterable
from typing import TYPE_CHECKING
from typing import Any

from django.http import HttpRequest
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import TemplateView

from ptf.url_utils import format_url_with_params

from ..app_settings import app_settings
from ..filters import FieldGetter
from ..filters import Filter
from ..filters import FilterSet
from ..forms.editorial_forms import StartReviewProcessForm
from ..mixins import RoleMixin
from ..models.journal_models import JournalSection
from ..models.submission_models import Submission
from ..models.submission_models import SubmissionLog
from ..roles.editor import Editor
from ..roles.journal_manager import JournalManager
from ..roles.role_handler import RoleHandler
from .components.breadcrumb import get_base_breadcrumb
from .components.breadcrumb import get_submission_breadcrumb
from .components.button import build_submission_actions
from .components.review_summary import CountWithTotal
from .model_proxy import BuildSubmissionProxyVisitor
from .utils import group_by

if TYPE_CHECKING:
    from ..interfaces.submission_interfaces import SubmissionListConfig


def get_submission_message_if_no_actions(submission, role_handler):
    """
    An author can not do anything if the submission is under review.
    Instead, we display a "thank you" message
    """

    html_message = ""
    if not role_handler.check_rights("can_edit_submission", submission):
        contact_email = app_settings.JOURNAL_EMAIL_CONTACT
        html_message = f"""
<p>Thank you for submitting your article.</p>
We will review your submission and get back to you. You can contact us at {contact_email}"""

    return html_message


class SubmissionDetailsView(RoleMixin, TemplateView):
    """
    View for a single submission.

    The context data is populated with all the available actions to the user
    regarding the submission.
    """

    template_name = "mesh/submission/submission_details.html"
    submission: Submission

    def restrict_dispatch(self, request: HttpRequest, *args, **kwargs):
        # TODO: Should we prefetch all data for an unique instance ?
        # It would result in the same amount of queries but, with prefetch, they would
        # all be made at the same time.
        self.submission = get_object_or_404(Submission, pk=kwargs["pk"])
        return not self.role_handler.check_rights("can_access_submission", self.submission)

    def get_context_data(self, **kwargs) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)

        builder = BuildSubmissionProxyVisitor(self.role_handler)
        context["submission_proxy"] = self.role_handler.current_role.accept(
            builder, self.submission
        )

        editorial_decisions = self.submission.editorial_decisions.all().order_by(  # type:ignore
            "-date_created"
        )

        context["editorial_decisions"] = editorial_decisions

        review_form = StartReviewProcessForm(initial={"process": True})
        context["action_lists"] = build_submission_actions(
            self.submission, self.role_handler, review_form
        )
        context["message_if_no_actions"] = get_submission_message_if_no_actions(
            self.submission, self.role_handler
        )

        # # Generate breadcrumb data
        # context["breadcrumb"] = get_submission_breadcrumb(self.submission)
        return context


def submission_list_filters() -> FilterSet:
    """
    Instantiate a fresh FilterSet for the submission list view.
    """
    return FilterSet(
        id="submission-filters",
        name="Filters",
        filters=[
            Filter(id="created_by", name=_("Author"), type="model"),
            Filter(
                id="state",
                name=_("State"),
                type="string",
                name_getter=FieldGetter(attr="get_state_display", callable=True),
            ),
            Filter(id="journal_section", name=_("Section"), type="model"),
            Filter(id="all_assigned_editors", name=_("Assigned editors"), type="model"),
        ],
    )


def group_submissions_per_status(
    submissions: Iterable[Submission], role_handler: RoleHandler
) -> dict[Any, list[Submission]]:
    return group_by(
        submissions, lambda s: role_handler.get_from_rights("get_submission_status", s).status
    )


def prepare_submissions_lists(
    role_handler: RoleHandler, request: HttpRequest, config_key="get_submission_list_config"
) -> dict[str, Any]:
    """
    Retrieve, group and filter the user's submissions to show according to:
        - the user's role submission list config
        - the applied filters (contained in the HTTP request)

    Params:
        -   `role_handler`  The role handler
        -   `request`       The underlying HTTP request
        -   `config_key`    The right function to be called to get the SubmissionListConfig(s)
                            to be used. This config defines which submissions to display along
                            with filtering behavior, etc.
    """
    context: dict[str, Any] = {}

    # The obtained submissions should have prefetched their related data.
    # It might be used extensively in the filtering and rendering processes.
    all_submissions = role_handler.get_attribute("submissions")
    # The submissions are displayed in different tables grouped by status. Precise
    # behavior is configured per role with SubmissionListConfig objects.
    grouped_submissions = group_submissions_per_status(all_submissions, role_handler)

    submission_list_configs: list[SubmissionListConfig] = role_handler.get_from_rights(config_key)
    for config in submission_list_configs:
        config.all_submissions = grouped_submissions.get(config.key, [])

    builder = BuildSubmissionProxyVisitor(role_handler)

    # Handle the filters per submission list
    if role_handler.check_rights("can_filter_submissions"):
        # Init filters
        filters = submission_list_filters()
        filters.init_filters(
            request.GET,
            *(config.all_submissions for config in submission_list_configs if config.in_filters),
        )
        context["filters"] = filters
        # Filter all submission lists
        for config in submission_list_configs:
            if not config.display:
                continue
            if not config.in_filters:
                config.submissions = [
                    role_handler.current_role.accept(builder, s) for s in config.all_submissions
                ]
                continue
            config.submissions = [
                role_handler.current_role.accept(builder, s)
                for s in filters.filter(config.all_submissions)
            ]

    else:
        for config in submission_list_configs:
            if not config.display:
                continue
            config.submissions = [
                role_handler.current_role.accept(builder, s) for s in config.all_submissions
            ]

    context["submissions_config"] = submission_list_configs
    context["all_submissions_count"] = sum(
        len(config.all_submissions) for config in submission_list_configs
    )
    context["submission_count"] = CountWithTotal(
        value=sum(
            len(config.submissions)
            for config in submission_list_configs
            if config.display and config.in_filters
        ),
        total=sum(
            len(config.all_submissions)
            for config in submission_list_configs
            if config.display and config.in_filters
        ),
    )

    return context


def all_role_submissions_count(role_handler: RoleHandler) -> list[dict]:
    """
    Returns the total number of submissions per active role.
    """
    all_role_submissions = []

    for role in role_handler.role_data.get_active_roles():
        role_submissions = {
            "role": role.summary().serialize(),
            "submissions": len(role.rights.submissions),
        }
        if role == role_handler.current_role:
            role_submissions["active"] = True
        all_role_submissions.append(role_submissions)

    return all_role_submissions


class SubmissionListView(RoleMixin, TemplateView):
    """
    View for the list of all submissions currently accessible to the user role.
    """

    template_name = "mesh/submission/submission_list_page.html"

    def get_context_data(self, **kwargs) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)

        context.update(prepare_submissions_lists(self.role_handler, self.request))
        all_role_submissions = all_role_submissions_count(self.role_handler)
        if len(all_role_submissions) > 1:
            context["all_role_submissions"] = all_role_submissions

        context["with_journal_sections"] = JournalSection.objects.all_journal_sections().exists()

        # # Generate breadcrumb data
        # breadcrumb = get_base_breadcrumb()
        # breadcrumb.add_item(title=_("My submissions"), url=reverse_lazy("mesh:submission_list"))
        # context["breadcrumb"] = breadcrumb
        return context


class ArchivedSubmissionListView(RoleMixin, TemplateView):
    restricted_roles = [JournalManager, Editor]
    template_name = "mesh/submission/submission_list_page.html"

    def get_context_data(self, *args, **kwargs) -> dict[str, Any]:
        context = super().get_context_data(*args, **kwargs)
        context.update(
            prepare_submissions_lists(
                self.role_handler, self.request, config_key="get_archived_submission_list_config"
            )
        )
        context["archived"] = True

        # Generate breadcrumb data
        breadcrumb = get_base_breadcrumb()
        breadcrumb.add_item(
            title=_("Archived submissions"), url=reverse_lazy("mesh:submission_list_archived")
        )
        context["breadcrumb"] = breadcrumb
        return context


class SubmissionLogView(RoleMixin, TemplateView):
    """
    View to display the `SubmissionLog` of a submission.
    """

    template_name = "mesh/log.html"
    submission: Submission

    def restrict_dispatch(self, request: HttpRequest, *args, **kwargs):
        pk = kwargs["pk"]
        self.submission = get_object_or_404(Submission, pk=pk)

        return not self.role_handler.check_rights("can_access_submission_log", self.submission)

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)

        logs = SubmissionLog.objects.filter(attached_to=self.submission)
        order = self.request.GET.get("order")
        context["order"] = order
        if order == "asc":
            logs = logs.order_by("date_created")
        else:
            logs = logs.order_by("-date_created")
        context["logs"] = logs

        query_params = {"order": "desc" if order == "asc" else "asc"}
        sort_url = reverse("mesh:submission_log", kwargs={"pk": self.submission.pk})
        context["sort_url"] = format_url_with_params(sort_url, query_params)

        context["page_title"] = _("Submission history")

        # Generate breadcrumb data
        breadcrumb = get_submission_breadcrumb(self.submission)
        breadcrumb.add_item(
            title=_("Submission history"),
            url=reverse_lazy("mesh:submission_log", kwargs={"pk": self.submission.pk}),
        )
        context["breadcrumb"] = breadcrumb
        return context
