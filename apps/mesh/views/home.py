from typing import Any

from django.http import HttpRequest
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import TemplateView

from ..mixins import RoleMixin


class HomeView(RoleMixin, TemplateView):
    template_name = "mesh/mesh_home.html"

    def get(self, request: HttpRequest, *args: Any, **kwargs: Any) -> HttpResponseRedirect:
        return HttpResponseRedirect(reverse_lazy("mesh:submission_list"))
