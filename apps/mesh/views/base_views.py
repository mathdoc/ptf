from typing import Any

from django.contrib import messages
from django.http import HttpRequest
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from ptf.url_utils import add_query_parameters_to_url

from ..forms.base_forms import SUBMIT_QUERY_PARAMETER
from ..forms.base_forms import FormAction
from ..forms.base_forms import SubmittableModelForm


class SubmittableModelFormMixin:
    """
    View mixin for submittable model forms.
    To be used together with:
        - RoleMixin
        - TemplateResponseMixin
        - FormMixin - The form class must inherit SubmittableModelForm

    This uses the same view for saving the form (and model), submitting the form
    and confirming the submission of the form.
    The status is known through the booleans `_submit` and `_submit_confirm`.
    The `form_pre_save` and `form_post_save` hooks enable additional processing.

    Normal workflow:
    1.  The user saves the form any number of times (draft).
    2.  When ready, the user clicks the "Submit" button
    3.  The user is redirected to a recap of the form to be submitted (default: same
        route & view with the disabled form).
    4.  The user confirms the submission of the form.
    5.  The underlying model gets updated: `submitted=True` & `date_submitted=now`

    Steps 4 and 5 have a simple implementation in this view mixin.
    These steps can be externalized to a distinct view. In that case, don't forget
    to update the model's `submitted` and `date_submitted` fields !
    """

    # Whether the request (POST) is the submission of the form/model.
    _submit = False
    # Whether the request (GET or POST) is the confirmation of the submission
    # of the form/model.
    _submit_confirm = False
    # Whether to add a message when redirecting to the confirmation URL.
    add_confirm_message = True
    # Only for typing. This attribute should be set somewhere else (usually by the
    # RoleMixin)
    request: HttpRequest

    def submit_url(self) -> str:
        """
        URL to redirect to to when the user submits the form.

        The default is the current URL with an added query parameter indicating
        the confirmation request.
        """
        return add_query_parameters_to_url(
            self.request.build_absolute_uri(), {SUBMIT_QUERY_PARAMETER: ["true"]}
        )

    def form_pre_save(self, form: SubmittableModelForm) -> None:
        """
        Hook called before `form_valid` (wich saves the underlying model to DB)
        when posting the form.
        """
        pass

    def form_post_save(
        self, form: SubmittableModelForm, original_response: HttpResponse
    ) -> HttpResponse:
        """
        Hook called after `form_valid` (wich saves the underlying model to DB)
        when posting the form.

        This must return an HTTP response.
        """
        return original_response

    def form_valid(self, form: SubmittableModelForm):
        """
        Checks whether the POST request is a simple save, a submit or
        a submit confirmation.
        """
        if FormAction.SUBMIT.value in self.request.POST:
            self._submit = True
        elif (
            FormAction.SUBMIT_CONFIRM.value in self.request.POST
            and form.instance.submitted is False
        ):
            self._submit_confirm = True
            form.instance.submitted = True
            if form.instance.date_submitted is None:
                form.instance.date_submitted = timezone.now()

        self.form_pre_save(form)

        resp = super().form_valid(form)  # type:ignore

        if self._submit:
            resp = HttpResponseRedirect(self.submit_url())
            if self.add_confirm_message:
                messages.info(self.request, _("Please confirm the submission of the form."))

        resp = self.form_post_save(form, resp)

        return resp

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)  # type:ignore
        context["submit_confirm"] = self._submit_confirm
        return context

    def get(self, request, *args, **kwargs):
        """
        Overloads the default get to catch whether it's a submit confirmation
        request.
        """
        self._submit_confirm = self.request.GET.get(SUBMIT_QUERY_PARAMETER, None) == "true"
        return super().get(request, *args, **kwargs)  # type:ignore

    def get_form_kwargs(self) -> dict[str, Any]:
        kwargs = super().get_form_kwargs()  # type:ignore
        if self.request.GET.get(SUBMIT_QUERY_PARAMETER, None) == "true":
            kwargs[SUBMIT_QUERY_PARAMETER] = True
        return kwargs
