from __future__ import annotations

from dataclasses import dataclass
from dataclasses import field
from typing import Generic
from typing import TypeVar

from django.urls import reverse_lazy

from .button import Button

T = TypeVar("T")


@dataclass
class TreeNode(Generic[T]):
    # class TreeNode[T]:    NEW SYNTAX (python 3.12) that discards the use of both TypeVar and Generic
    """
    Represents a tree item.
    """
    item: T | None
    href: str = field(default="")
    buttons: list[Button] = field(default_factory=list)
    children: list[TreeNode[T]] = field(default_factory=list)


def build_tree_recursive(journal_section_manager, journal_section=None):
    """
    Return the tree of JournalSection, starting from the given journal_section.
    """
    tree_node = TreeNode(item=journal_section)
    if journal_section:
        tree_node.href = reverse_lazy(
            "mesh:submission_journal_section_update", kwargs={"pk": journal_section.pk}
        )

    children_dict = journal_section_manager.all_journal_sections_children()
    key = None if journal_section is None else journal_section.pk
    tree_node.children = [
        build_tree_recursive(journal_section_manager, c) for c in children_dict[key]
    ]
    return tree_node
