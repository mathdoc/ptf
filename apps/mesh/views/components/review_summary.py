from __future__ import annotations

from dataclasses import dataclass
from dataclasses import field


@dataclass
class CountWithTotal:
    value: int
    total: int

    def __post_init__(self):
        if self.value > self.total:
            raise ValueError("CountWithTotal: a value greater than the total has been set.")


@dataclass
class ReviewSummary:
    current_version: int
    reviewers_count: CountWithTotal | None = field(default=None)
    reports_count: CountWithTotal | None = field(default=None)


def build_review_summary(submission):
    """
    Returns the submission review summary:
    -   Current version / round
    -   Nb of answered/pending reviewer requests
    -   Nb of review reports out of accepted requests
    """

    summary = ReviewSummary(
        current_version=submission.current_version.number if submission.current_version else 0,
        reviewers_count=CountWithTotal(value=0, total=0),
        reports_count=CountWithTotal(value=0, total=0),
    )

    if submission.current_version:
        reviews = submission.current_version.reviews.all()
        if reviews:
            answered_reviews = [r for r in reviews if r.accepted is not None]
            summary.reviewers_count = CountWithTotal(
                value=len(answered_reviews), total=len(reviews)
            )

            accepted_reviews = [r for r in answered_reviews if r.accepted]
            completed_reviews = sum(1 for r in accepted_reviews if r.submitted)
            summary.reports_count = CountWithTotal(
                value=completed_reviews, total=len(accepted_reviews)
            )

    return summary
