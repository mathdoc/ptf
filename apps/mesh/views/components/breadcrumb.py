from __future__ import annotations

from typing import TYPE_CHECKING

from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _

from ptf.views.components.breadcrumb import Breadcrumb
from ptf.views.components.breadcrumb import BreadcrumbItem

if TYPE_CHECKING:
    from ...models.submission_models import Submission


def get_base_breadcrumb() -> Breadcrumb:
    """
    Returns a breadcrumb with the default "Home" breadcrumb item.
    """
    return Breadcrumb(
        items=[
            BreadcrumbItem(
                title=_("Home"), url=reverse_lazy("mesh:home"), icon_class="fa-house-chimney"
            )
        ]
    )


def get_submission_breadcrumb(submission: Submission):
    """
    Returns a breadcrumb with the default items for a page related to a submission.

    Base items -> Submission list -> Submission details
    """
    breadcrumb = get_base_breadcrumb()
    breadcrumb.add_item(title=_("My submissions"), url=reverse_lazy("mesh:submission_list"))
    breadcrumb.add_item(
        title=submission.name,
        url=reverse_lazy("mesh:submission_details", kwargs={"pk": submission.pk}),
        html=True,
    )
    return breadcrumb
