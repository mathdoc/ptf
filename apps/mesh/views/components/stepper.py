from __future__ import annotations

from dataclasses import dataclass
from dataclasses import field
from typing import TYPE_CHECKING

from mesh.forms.base_forms import FormAction

from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _

from .button import Button

if TYPE_CHECKING:
    from ...models.submission_models import Submission


@dataclass
class StepConfig:
    id: str
    title: str
    href: str = field(default="")
    description: str = field(default="")
    tooltip: str = field(default="")
    # Whether the step has already been completed
    completed: bool = field(default=False)
    # Whether this is the currently displayed/selected step.
    active: bool = field(default=False)
    # Whether the step config is navigable.
    # Usually it should be equals to `previous_step.completed`
    can_navigate: bool = field(default=False)
    # For some steps, the Next button is the form submit button
    # For others (use_href_for_next=True), the Next button is a simple link
    use_href_for_next_button: bool = field(default=False)


@dataclass
class StepperConfig:
    steps: list[StepConfig] = field(default_factory=list)
    title: str = field(default="")
    # Id of the active step
    active: str = field(default="")

    def get_step(self, step_id: str) -> StepConfig | None:
        return next((s for s in self.steps if s.id == step_id), None)

    def set_active_step(self, step_id: str):
        step = self.get_step(step_id)
        if step is None:
            return

        for s in self.steps:
            s.active = False
        step.active = True
        self.active = step_id

    def get_next_step(self, step_id):
        next_step = None
        i = 0
        while next_step is None and i < len(self.steps):
            if self.steps[i].id == step_id:
                if i + 1 < len(self.steps):
                    next_step = self.steps[i + 1]
            i += 1
        return next_step

    def get_previous_step(self, step_id):
        previous_step = None
        i = 0
        while previous_step is None and i < len(self.steps):
            if self.steps[i].id == step_id:
                if i - 1 > -1:
                    previous_step = self.steps[i - 1]
            i += 1
        return previous_step

    def set_completed_steps(self, step_ids: list[str]):
        for step in self.steps:
            step.completed = step.id in step_ids

    def get_next_button(self):
        step_id = self.active
        button = None
        next_step = self.get_next_step(step_id)
        if next_step is not None:
            current_step = self.get_step(step_id)
            href = next_step.href if current_step.use_href_for_next_button else None
            button = add_stepper_button(
                id="next",
                title="Next",
                icon_class="fa-right-long",
                name=FormAction.NEXT.value,
                href=href,
                btn_classes=["button-highlight"],
            )
        return button

    def get_previous_button(self):
        step_id = self.active
        button = None
        previous_step = self.get_previous_step(step_id)
        if previous_step is not None:
            button = add_stepper_button(
                id="previous",
                title="Previous",
                icon_class="fa-left-long",
                name=FormAction.PREVIOUS.value,
                href=previous_step.href,
            )
        return button


def get_submission_stepper(submission: Submission | None) -> StepperConfig:
    """
    Returns the submission stepper config adapted with the given submission.
    The active step is not set.
    """
    metadata_step = StepConfig(id="metadata", title=_("Article metadata"))
    authors_step = StepConfig(
        id="authors",
        title=_("Authors"),
        use_href_for_next_button=True,
        # description="I have something very very VERY long to say. Please make some space in the layout to render this description properly."
    )
    version_step = StepConfig(id="version", title=_("Files"))
    confirm_step = StepConfig(id="confirm", title=_("Confirm"))

    if submission:
        metadata_step.href = reverse_lazy("mesh:submission_update", kwargs={"pk": submission.pk})
        metadata_step.completed = True
        metadata_step.can_navigate = True

        authors_step.href = reverse_lazy("mesh:submission_authors", kwargs={"pk": submission.pk})
        authors_step.completed = submission.authors.exists()  # type:ignore
        authors_step.can_navigate = True

        version = submission.current_version
        version_step.href = (
            reverse_lazy("mesh:submission_version_update", kwargs={"pk": version.pk})
            if version
            else reverse_lazy(
                "mesh:submission_version_create", kwargs={"submission_pk": submission.pk}
            )
        )
        version_step.completed = version is not None
        version_step.can_navigate = authors_step.completed

        if version:
            confirm_step.href = reverse_lazy(
                "mesh:submission_confirm", kwargs={"pk": submission.pk}
            )
            confirm_step.can_navigate = True

    return StepperConfig(
        steps=[metadata_step, authors_step, version_step, confirm_step], title=_("Submit process")
    )


def add_stepper_button(id, title, icon_class, name, href=None, btn_classes=[]):
    attrs = {"name": [name]}
    if href is None:
        attrs["type"] = ["submit"]
        attrs["form"] = ["id-form-fullpage"]
        btn_classes.append("save-button")
        attrs["class"] = btn_classes
    else:
        attrs["href"] = [href]
        btn_classes.extend(["as-button", "save-button"])
        attrs["class"] = btn_classes

    return Button(
        id=id,
        title=_(title),
        icon_class=icon_class,
        # attrs={"href": [author_step.href], "class": ["as-button"]},
        attrs=attrs,
    )
