from dataclasses import dataclass
from dataclasses import field

from django import forms
from django.urls import reverse
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _

from ptf.url_utils import add_query_parameters_to_url

from ...interfaces.submission_interfaces import SubmissionActionList
from ...models.review_models import Review
from ...models.review_models import ReviewState
from ...models.submission_models import Submission
from ...roles.role_handler import RoleHandler


@dataclass
class Button:
    """
    Interface for a button component.
    """

    # Unique ID for the button - not used for now
    id: str
    # Title to display as text in the button
    title: str
    # Optional font-awesome 6 icon code/class to be added next to the title.
    icon_class: str = field(default="")
    # Additional HTML attributes (ex: `{"type": ["submit"]})`
    attrs: dict[str, list[str]] = field(default_factory=dict)
    # A Django form - If not None, the button will be rendered as a full form
    # with this unique button. You must provide the "href" HTML attribute for the form
    # to be considered as one.
    form: forms.Form | None = field(default=None)

    def is_form(self) -> bool:
        """
        Whether the button should be displayed as a form.
        """
        return bool(self.form) and bool(self.attrs.get("href", False))

    def is_link(self) -> bool:
        """
        Whether the button is a link.
        """
        return bool(self.attrs.get("href", False))

    def add_attr(self, attr: str, values: list[str]):
        """
        Add the given value to the given attribute values array.

        Ex:
            `button.add_attr("class", ["btn", "btn-primary"])`
            `button.add_attr("class", ["btn"])`
        """
        if attr not in self.attrs:
            self.attrs[attr] = []

        for value in values:
            if value in self.attrs[attr]:
                continue
            self.attrs[attr].append(value)

    def set_attr(self, attr: str, values: list[str]):
        """
        Set the given value for the given attribute.

        Ex:
            `button.set_attr("class", ["btn", "btn-primary"])`
            `button.set_attr("href", ["http://myserver"])`
        """
        self.attrs[attr] = values

    def remove_attr(self, attr: str):
        """
        Remove the given attribute from the attributes array.
        """
        if attr in self.attrs:
            del self.attrs[attr]


def build_submission_actions(
    submission: Submission, role_handler: RoleHandler, review_form
) -> list[SubmissionActionList]:
    """
    Returns the list of available actions to the current user as Button components, gathered
    in distinct groups (SubmissionActionList).

    TODO: There is a kind of duplicate between the Buttons defined here and the ones defined
    in each role's `get_submission_status`. It could be good to harmonize both the condition to
    retrieve the button and the button config itself.
    The paining aspect is that there might be additional conditions for the button visibility
    in `get_submission_status` (we usually only show a subset of the available actions.)
    We could just flag them with something like `for_shortcut=True/False`.
    """
    submission_actions: list[Button] = []
    manage_actions: list[Button] = []
    #### [BEGIN] Author actions ####
    # Resume submit process
    can_submit_submission = role_handler.check_rights("can_submit_submission", submission)
    if can_submit_submission:
        submission_actions.append(
            Button(
                id=f"submission-submit-{submission.pk}",
                title=_("RESUME SUBMISSION"),
                icon_class="fa-list-check",
                attrs={
                    "href": [reverse_lazy("mesh:submission_resume", kwargs={"pk": submission.pk})]
                },
            )
        )

    # Edit submission metadata & authors
    elif role_handler.check_rights("can_edit_submission", submission):
        submission_actions.append(
            Button(
                id=f"submission-edit-metadata-{submission.pk}",
                title=_("EDIT METADATA"),
                icon_class="fa-pen-to-square",
                attrs={
                    "href": [reverse_lazy("mesh:submission_update", kwargs={"pk": submission.pk})]
                },
            )
        )
        submission_actions.append(
            Button(
                id=f"submission-edit-authors-{submission.pk}",
                title=_("EDIT AUTHOR"),
                icon_class="fa-pen-to-square",
                attrs={
                    "href": [reverse_lazy("mesh:submission_authors", kwargs={"pk": submission.pk})]
                },
            )
        )

    # Create a new submission version
    if not can_submit_submission and role_handler.check_rights("can_create_version", submission):
        submission_actions.append(
            Button(
                id=f"submission-create-version-{submission.pk}",
                title=_("SUBMIT REVISIONS"),
                icon_class="fa-plus",
                attrs={
                    "href": [
                        reverse_lazy(
                            "mesh:submission_version_create",
                            kwargs={"submission_pk": submission.pk},
                        )
                    ]
                },
            )
        )

    # Edit the current submission version
    current_version = submission.current_version
    if (
        not can_submit_submission
        and current_version
        and role_handler.check_rights("can_edit_version", current_version)
    ):
        submission_actions.append(
            Button(
                id=f"submission-edit-version-{submission.pk}",
                title=_("RESUME REVISIONS"),
                icon_class="fa-list-check",
                attrs={
                    "href": [
                        reverse_lazy(
                            "mesh:submission_version_update", kwargs={"pk": current_version.pk}
                        )
                    ]
                },
            )
        )
    #### [END] Author actions ####

    #### [BEGIN] Editor actions ####
    if role_handler.check_rights("can_assign_editor", submission):
        """
        <a id="assigned-editors-edit" class="as-button" href="{% url "mesh:submission_editors" pk=submission.pk%}">
           <i class="fa-solid fa-plus"></i>
           {% translate "Assign editor" %}
         </a>
        """
        manage_actions.append(
            Button(
                id=f"assigned-editors-edit-{submission.pk}",
                title=_("ASSIGN EDITOR"),
                icon_class="fa-plus",
                attrs={
                    "href": [reverse_lazy("mesh:submission_editors", kwargs={"pk": submission.pk})]
                },
            )
        )

    # Send the submission version to review
    if role_handler.check_rights("can_start_review_process", submission):
        manage_actions.append(
            Button(
                id=f"submission-send-to-review-{submission.pk}",
                title=_("SEND TO REVIEW"),
                icon_class="fa-file-import",
                form=review_form,
                attrs={
                    "href": [
                        reverse_lazy(
                            "mesh:submission_start_review_process", kwargs={"pk": submission.pk}
                        )
                    ],
                    "class": ["as-link"],
                },
            )
        )

    # Make an editorial decision
    if role_handler.check_rights("can_create_editorial_decision", submission):
        manage_actions.append(
            Button(
                id=f"submission-editorial-decision-{submission.pk}",
                title=_("EDITORIAL DECISION"),
                icon_class="fa-plus",
                attrs={
                    "href": [
                        reverse_lazy(
                            "mesh:editorial_decision_create",
                            kwargs={"submission_pk": submission.pk},
                        )
                    ]
                },
            )
        )

    # Request a new review
    if current_version and role_handler.check_rights("can_invite_reviewer", current_version):
        manage_actions.append(
            Button(
                id=f"submission-referee-request-{submission.pk}",
                title=_("REQUEST REVIEW"),
                icon_class="fa-user-group",
                attrs={
                    "href": [
                        reverse_lazy(
                            "mesh:review_create",
                            kwargs={"version_pk": submission.current_version.pk},  # type:ignore
                        )
                    ]
                },
            )
        )

    if role_handler.check_rights("can_access_submission_log", submission):
        manage_actions.append(
            Button(
                id=f"submission-log-{submission.pk}",
                title=_("VIEW HISTORY"),
                icon_class="fa-clock-rotate-left",
                attrs={
                    "href": [reverse_lazy("mesh:submission_log", kwargs={"pk": submission.pk})]
                },
            )
        )
    #### [END] Editor actions ####

    #### [BEGIN] Reviewer actions ####
    # Submit / Edit the user current review
    current_review: Review | None = role_handler.get_from_rights(
        "get_current_open_review", current_version
    )
    if current_review is not None and current_review.accepted is None:
        # Display 2 buttons: Accept & Decline if the review has not been accepted yet.
        manage_actions.extend(
            [
                Button(
                    id=f"review-accept-{current_review.pk}",
                    title=_("Accept review"),
                    icon_class="fa-check",
                    attrs={
                        "href": [
                            add_query_parameters_to_url(
                                reverse("mesh:review_accept", kwargs={"pk": current_review.pk}),
                                {"accepted": ["true"]},
                            )
                        ],
                        "class": ["as-button", "button-success"],
                    },
                ),
                Button(
                    id=f"review-accept-{current_review.pk}",
                    title=_("Decline review"),
                    icon_class="fa-xmark",
                    attrs={
                        "href": [
                            add_query_parameters_to_url(
                                reverse("mesh:review_accept", kwargs={"pk": current_review.pk}),
                                {"accepted": ["false"]},
                            )
                        ],
                        "class": ["as-button", "button-error"],
                    },
                ),
            ]
        )
    elif current_review is not None:
        action_name = _("SUBMIT REVIEW")
        if current_review.state in [ReviewState.SUBMITTED.value, ReviewState.DECLINED.value]:
            action_name = _("EDIT REVIEW")
        manage_actions.append(
            Button(
                id=f"review-{current_review.pk}",
                title=action_name,
                icon_class="fa-pen-to-square",
                attrs={
                    "href": [reverse_lazy("mesh:review_update", kwargs={"pk": current_review.pk})]
                },
            )
        )
    #### [END] Reviewer actions ####
    action_lists = []
    if submission_actions:
        action_lists.append(
            SubmissionActionList(title=_("Submission"), actions=submission_actions)
        )

    if manage_actions:
        action_lists.append(SubmissionActionList(title=_("Manage"), actions=manage_actions))

    return action_lists


def get_shortcut_actions(submission, rights):
    buttons = []
    if rights.can_assign_editor(submission) and not submission.all_assigned_editors:
        buttons.append(
            Button(
                id=f"submission-assign-editor-{submission.pk}",
                title=_("Assign editor"),
                icon_class="fa-user-tie",
                attrs={
                    "href": [
                        reverse_lazy("mesh:submission_editors", kwargs={"pk": submission.pk})
                    ],
                    "class": ["as-button"],
                },
            )
        )

    return buttons
