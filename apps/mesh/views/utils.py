from __future__ import annotations

from collections.abc import Callable
from collections.abc import Iterable
from typing import TYPE_CHECKING
from typing import Any
from typing import TypeVar

from allauth.account.models import EmailAddress

from django.conf import settings
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.template.loader import render_to_string
from django.urls import reverse_lazy

from ptf.url_utils import add_query_parameters_to_url
from ptf.utils import send_email
from ptf.utils import template_from_string

from ..app_settings import app_settings
from ..models.submission_models import Submission
from ..models.user_models import USER_TOKEN_QUERY_PARAM
from ..models.user_models import User
from ..models.user_models import UserToken
from ..roles.role_handler import RoleHandler

if TYPE_CHECKING:
    from ..models.review_models import Review

# The below is correct for python > 3.11 instead of declaring a TypeVar
# def group_by[_T](dataset: Iterable[_T], key: Callable[[_T], Any]) -> dict[Any, list[_T]]:

_T = TypeVar("_T")


def group_by(dataset: Iterable[_T], key: Callable[[_T], Any]) -> dict[Any, list[_T]]:
    """
    Group the given dataset using the provided key function to get the value
    to group on.

    Params:
        -   `dataset`       The dataset to group by.
        -   `key`           The function used to get the grouping value for each item
                            if the dataset.
    Returns:
        -   `dict`          The dictionnary with keys being the group key, and
                            values the list of items in the group.
    """
    grouped_data: dict[Any, list[_T]] = {}
    for item in dataset:
        value = key(item)
        if value not in grouped_data:
            grouped_data[value] = []
        grouped_data[value].append(item)

    return grouped_data


def create_new_user(
    email: str, first_name: str, last_name: str, password: str | None = None
) -> User:
    """
    Create a new User and a new EmailAddress using the given e-mail address.
    No verification are made regarding the provided e-mail address
    """
    user = User.objects.create_user(
        email, password=password, first_name=first_name, last_name=last_name
    )
    EmailAddress.objects.create(user=user, email=email, primary=True, verified=False)

    return user


def get_review_request_email(
    submission: Submission, base_url: str, template_name: str = ""
) -> str:
    """
    Render the default e-mail template for the review request.
    """
    template_name = template_name or "mesh/emails/review/referee_request.html"
    if base_url[-1] == "/":
        base_url = base_url[:-1]
    context = {
        "submission_name": submission.name,
        "submission_abstract": submission.abstract,
        "submission_url": base_url
        + reverse_lazy("mesh:submission_details", kwargs={"pk": submission.pk}),
    }

    return render_to_string(template_name, context)


def send_review_request_email(review: Review, email: str, subject: str, base_url: str):
    """
    Sends the provided email to the review's assigned reviewer.
    Params:
        -   `review`        The review object.
        -   `email`         The e-mail content (HTML).
        -   `subject`       The e-mail subject.
        -   `base_url`      The base URL to use when generating links.
    """
    recipient: User = review.reviewer
    to = [recipient.email]
    if base_url[-1] == "/":
        base_url = base_url[:-1]

    submission = review.version.submission
    context = {"RECIPIENT": recipient.get_full_name()}

    # Generate a token authentication URL if the user is allowed to
    if getattr(recipient, "is_token_authentication_allowed", False):
        role_handler = RoleHandler(recipient, partial_init=True)

        if role_handler.token_authentication_allowed():
            token = UserToken.get_token(recipient)
            context["TOKEN_AUTH_URL"] = add_query_parameters_to_url(
                base_url + reverse_lazy("mesh:token_login"),
                {
                    USER_TOKEN_QUERY_PARAM: [token.key],
                    REDIRECT_FIELD_NAME: [
                        reverse_lazy("mesh:submission_details", kwargs={"pk": submission.pk})
                    ],
                },
            )
    email_template = template_from_string(email)
    rendered_email = email_template.render(context)

    subject = f"{app_settings.EMAIL_PREFIX}{subject}"
    from_email = f"{review.created_by.get_full_name()} via MESH <{settings.DEFAULT_FROM_EMAIL}>"
    reply_to = [review.created_by.email]  # type:ignore

    send_email(rendered_email, subject, to, from_email=from_email, reply_to=reply_to)

    review.request_email = rendered_email
    review.save()
