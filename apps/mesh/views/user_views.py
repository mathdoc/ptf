from typing import Any

from django.contrib import messages
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth import authenticate
from django.contrib.auth import login
from django.contrib.auth import logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import Http404
from django.http import HttpRequest
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import FormView
from django.views.generic import View

from ..forms.user_forms import UserForm
from ..interfaces.user_interfaces import ImpersonateData
from ..interfaces.user_interfaces import UserInfo
from ..mixins import RoleMixin
from ..models.user_models import USER_TOKEN_QUERY_PARAM
from ..roles.editor import Editor
from ..roles.journal_manager import JournalManager
from .components.breadcrumb import get_base_breadcrumb


class InitImpersonateSessionView(RoleMixin, FormView):
    form_class = UserForm
    template_name = "mesh/forms/form_full_page.html"
    restricted_roles = [JournalManager, Editor]

    def restrict_dispatch(self, request: HttpRequest, *args, **kwargs):
        """
        Check that the user is not currently in impersonate mode (deny
        recursively impersonating user, too messy).
        """
        if "impersonate_target_id" in self.request.session:
            return True
        return not self.role_handler.check_rights("can_impersonate")

    def get_form_kwargs(self) -> dict[str, Any]:
        """
        Adds the _users queryset of availabe users for impersonating when instantiating
        the form.
        """
        kwargs = super().get_form_kwargs()
        kwargs["_users"] = self.role_handler.get_attribute("managed_users")

        return kwargs

    def get_success_url(self) -> str:
        return reverse_lazy("mesh:home")

    def form_valid(self, form: UserForm) -> HttpResponse:
        """
        Serializes the impersonate data in the current session.
        """
        user = form.cleaned_data["user"]

        user_info = UserInfo.from_user(self.request.user)  # type:ignore

        data = ImpersonateData(source=user_info, target_id=user.pk)
        data.serialize(self.request.session)

        return super().form_valid(form)

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)

        context["page_title"] = _("Choose user to impersonate")

        # Generate breadcrumb data
        breadcrumb = get_base_breadcrumb()
        breadcrumb.add_item(title=_("Impersonate user"), url=reverse_lazy("mesh:impersonate_init"))
        context["breadcrumb"] = breadcrumb
        return context


class CloseImpersonateSessionView(LoginRequiredMixin, View):
    """
    Cleans the current session of all impersonating related variables.
    """

    def post(self, request: HttpRequest, *args, **kwargs) -> HttpResponse:
        ImpersonateData.clean_session(request.session)
        redirect_url = request.headers.get("referer", reverse_lazy("mesh:home"))
        return HttpResponseRedirect(redirect_url)


# @method_decorator(rate_limit(action="token_authentication"), name="dispatch")
# We would like to protect this view with a rate limit mechanism.
# Unfortunately the django-allauth rate limiter does not support GET requests..
class TokenLoginView(View):
    """
    View to login using an `UserToken`.
    """

    def get(self, request: HttpRequest, *args, **kwargs) -> HttpResponse:
        token = request.GET.get(USER_TOKEN_QUERY_PARAM)
        if not token:
            raise Http404

        response = HttpResponseRedirect(reverse_lazy("mesh:home"))

        user = authenticate(request, token=token)
        if not user:
            messages.warning(
                request,
                _(
                    "The given authentication token is invalid, expired or "
                    "you're not allowed to use token authentication."
                ),
            )
            return response

        # Check for redirection
        redirect_uri = request.GET.get(REDIRECT_FIELD_NAME)
        if redirect_uri:
            response = HttpResponseRedirect(redirect_uri)

        # Handle already populated user instance by the default `AuthenticationMiddleware`
        if hasattr(request, "user"):
            # Same user requesting authentication -> Nothing to do.
            if request.user.is_authenticated and request.user.pk == user.pk:
                return response

            logout(request)

        request.user = user
        login(request, user)

        return response
