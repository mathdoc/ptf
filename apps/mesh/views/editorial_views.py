from typing import Any

from django.contrib import messages
from django.http import HttpRequest
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import CreateView
from django.views.generic import FormView
from django.views.generic import TemplateView
from django.views.generic import UpdateView

from ..file_helpers import post_delete_model_file
from ..forms.editorial_forms import EditorialDecisionCreateForm
from ..forms.editorial_forms import EditorialDecisionUpdateForm
from ..forms.editorial_forms import StartReviewProcessForm
from ..forms.user_forms import UserForm
from ..mixins import RoleMixin
from ..models.editorial_models import EditorialDecision
from ..models.editorial_models import EditorSubmissionRight
from ..models.submission_models import Submission
from ..models.submission_models import SubmissionLog
from ..models.user_models import User
from ..roles.editor import Editor
from ..roles.editor import get_section_editors
from ..roles.journal_manager import JournalManager
from .components.breadcrumb import get_submission_breadcrumb
from .components.button import Button


class SendToReviewView(RoleMixin, FormView):
    """
    View to send a submission with its curent version to the Review process.

    It sets the version's boolean `review_open = True` and switch the submission
    state to `on_review`.
    """

    submission: Submission
    restricted_roles = [Editor, JournalManager]
    form_class = StartReviewProcessForm

    def restrict_dispatch(self, request: HttpRequest, *args, **kwargs) -> bool:
        self.submission = get_object_or_404(Submission, pk=kwargs["pk"])
        return not self.role_handler.check_rights("can_start_review_process", self.submission)

    def get_success_url(self) -> str:
        return reverse_lazy("mesh:submission_details", kwargs={"pk": self.submission.pk})

    def form_valid(self, form: Any) -> HttpResponse:
        self.submission.start_review_process(self.request.user)
        messages.success(self.request, _("The submission has been sent to review."))
        return super().form_valid(form)


class EditorialDecisionCreateView(RoleMixin, CreateView):
    """
    View for creating an editorial decision.
    """

    model = EditorialDecision
    form_class = EditorialDecisionCreateForm
    template_name = "mesh/forms/form_full_page.html"
    restricted_roles = [JournalManager, Editor]
    submission: Submission

    def restrict_dispatch(self, request: HttpRequest, *args, **kwargs) -> bool:
        self.submission = get_object_or_404(Submission, pk=kwargs["submission_pk"])

        return not self.role_handler.check_rights("can_create_editorial_decision", self.submission)

    def get_success_url(self) -> str:
        return reverse_lazy("mesh:submission_details", kwargs={"pk": self.submission.pk})

    def form_valid(self, form: EditorialDecisionCreateForm) -> HttpResponse:
        form.instance._user = self.request.user
        form.instance.submission = self.submission

        response = super().form_valid(form)

        self.submission.apply_editorial_decision(self.object, self.request.user)  # type:ignore

        messages.success(self.request, _("Editorial decision successfully submitted"))

        return response

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)

        context["page_title"] = _("New editorial decision")

        submission_url = reverse_lazy("mesh:submission_details", kwargs={"pk": self.submission.pk})
        description = "Please fill the form below to submit your editorial decision "
        description += (
            f"for the submission <a href='{submission_url}'><i>{self.submission.name}</i></a>."
        )
        context["form_description"] = [description]

        # Breadcrumnb
        breadcrumb = get_submission_breadcrumb(self.submission)
        breadcrumb.add_item(
            title=_("New editorial decision"),
            url=reverse_lazy(
                "mesh:editorial_decision_create", kwargs={"submission_pk": self.submission.pk}
            ),
        )
        context["breadcrumb"] = breadcrumb

        return context


class EditorialDecisionUpdateView(RoleMixin, UpdateView):
    """
    View for updating an existing editorial decision.
    """

    model: type[EditorialDecision] = EditorialDecision
    form_class = EditorialDecisionUpdateForm
    template_name = "mesh/forms/form_full_page.html"
    restricted_roled = [JournalManager, Editor]
    decision: EditorialDecision

    def restrict_dispatch(self, request: HttpRequest, *args, **kwargs) -> bool:
        self.decision = get_object_or_404(self.model, pk=kwargs["pk"])

        return not self.role_handler.check_rights("can_edit_editorial_decision", self.decision)

    def get_success_url(self) -> str:
        return reverse_lazy("mesh:submission_details", kwargs={"pk": self.decision.submission.pk})

    def get_object(self, *args, **kwargs) -> EditorialDecision:
        return self.decision

    def form_valid(self, form: EditorialDecisionUpdateForm) -> HttpResponse:
        form.instance._user = self.request.user
        if not form.changed_data:
            form.instance.do_not_update = True

        response = super().form_valid(form)

        decision_str = self.decision.get_value_display()  # type: ignore

        if form.has_changed():
            SubmissionLog.add_message(
                self.decision.submission,
                _("Editorial decision updated") + f": {decision_str}",
                f"Editorial decision updated: {decision_str}",
                user=self.request.user,
            )
        messages.success(self.request, _("Editorial decision successfully updated"))

        return response

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)

        context["page_title"] = _("Edit editorial decision")

        submission_url = reverse_lazy(
            "mesh:submission_details", kwargs={"pk": self.decision.submission.pk}
        )
        description = "Please fill the form below to edit your editorial decision "
        description += f"for the submission <a href='{submission_url}'><i>{self.decision.submission.name}</i></a>."
        context["form_description"] = [description]

        # Breadcrumnb
        breadcrumb = get_submission_breadcrumb(self.decision.submission)
        breadcrumb.add_item(
            title=_("New editorial decision"),
            url=reverse_lazy(
                "mesh:editorial_decision_create",
                kwargs={"submission_pk": self.decision.submission.pk},
            ),
        )
        context["breadcrumb"] = breadcrumb

        return context

    def post(self, request: HttpRequest, *args: str, **kwargs: Any) -> HttpResponse:
        """
        View for updating the editorial decision or deleting one of
        the associated files.
        """
        deletion_requested, _ = post_delete_model_file(self.decision, request, self.role_handler)

        if deletion_requested:
            # Update the submission object.
            self.decision = get_object_or_404(self.model, pk=self.decision.pk)
            return self.get(request, *args, **kwargs)

        return super().post(request, *args, **kwargs)


class AssignEditorView(RoleMixin, TemplateView):
    """
    View to add/remove editors assigned to a given submission.
    """

    submission: Submission
    template_name = "mesh/forms/assign_editor.html"

    def restrict_dispatch(self, request: HttpRequest, *args, **kwargs) -> bool:
        self.submission = get_object_or_404(Submission, pk=kwargs["pk"])
        return not self.role_handler.check_rights("can_assign_editor", self.submission)

    def get_context_data(self, *args, **kwargs):
        """
        2 models are used to compute editor rights over a submission.
        Section rights & Submission rights.
        This view will display all editors having right over the given submission.
        """
        context = super().get_context_data(*args, **kwargs)
        context["submission"] = self.submission

        direct_editors = User.objects.filter(editor_submissions__submission=self.submission)
        context["assigned_editors"] = [
            {
                "user": editor,
                "buttons": [
                    Button(
                        id=f"remove-editor-{editor.pk}",
                        title=_("Remove"),
                        icon_class="fa-trash",
                        form=UserForm(
                            _users=direct_editors, _hide_user=True, initial={"user": editor}
                        ),
                        attrs={
                            "href": [
                                reverse_lazy(
                                    "mesh:submission_editors", kwargs={"pk": self.submission.pk}
                                )
                            ],
                            "class": ["button-error"],
                            "type": ["submit"],
                            "name": ["_action_remove"],
                        },
                    )
                ],
            }
            for editor in direct_editors
        ]

        excluded_user_pks = [e.pk for e in direct_editors]
        context["form"] = UserForm(
            _users=User.objects.exclude(pk__in=excluded_user_pks), _user_label=_("User")
        )

        section_editors = []
        for editor in get_section_editors(self.submission):
            editor_data: dict[str, Any] = {"user": editor, "buttons": []}
            if editor not in direct_editors:
                editor_data["buttons"].append(
                    Button(
                        id=f"assign-editor-{editor.pk}",
                        title=_("Assign"),
                        icon_class="fa-plus",
                        form=UserForm(
                            _users=direct_editors, _hide_user=True, initial={"user": editor}
                        ),
                        attrs={
                            "href": [
                                reverse_lazy(
                                    "mesh:submission_editors", kwargs={"pk": self.submission.pk}
                                )
                            ],
                            "type": ["submit"],
                        },
                    )
                )
            else:
                editor_data["buttons"].append(
                    Button(
                        id=f"assigned-editor-{editor.pk}",
                        title=_("Assigned"),
                        icon_class="fa-check",
                        attrs={"class": ["inactive"]},
                    )
                )
            section_editors.append(editor_data)
        context["section_editors"] = section_editors

        context["form_save_title"] = _("Add editor")
        context["form_save_icon"] = "fa-plus"
        context["page_title"] = _("Assigned editors")

        # Breadcrumnb
        breadcrumb = get_submission_breadcrumb(self.submission)
        breadcrumb.add_item(
            title=_("Assigned editors"),
            url=reverse_lazy("mesh:submission_editors", kwargs={"pk": self.submission.pk}),
        )
        context["breadcrumb"] = breadcrumb

        return context

    def post(self, request: HttpRequest, *args, **kwargs) -> HttpResponse:
        remove_form = "_action_remove" in request.POST
        if remove_form:
            users = User.objects.filter(editor_submissions__submission=self.submission)
        else:
            users = User.objects.exclude(editor_submissions__submission=self.submission)

        response = HttpResponseRedirect(
            reverse_lazy("mesh:submission_details", kwargs={"pk": self.submission.pk})
        )
        form = UserForm(request.POST, _users=users)
        if not form.is_valid():
            messages.error(request, "ERROR")
            return response

        user = form.cleaned_data["user"]

        if remove_form:
            EditorSubmissionRight.objects.get(submission=self.submission, user=user).delete()
            messages.success(
                request,
                _(f"{user} was successfully removed from the assigned editors of the submission."),
            )
            SubmissionLog.add_message(
                self.submission,
                content=_(f"{user} removed from the assigned editors."),
                content_en=f"{user} removed from the assigned editors.",
                user=self.request.user,
                significant=True,
            )
        else:
            EditorSubmissionRight.objects.create(submission=self.submission, user=user)
            messages.success(
                request, _(f"{user} was successfully assigned as an editor for the submission.")
            )
            SubmissionLog.add_message(
                self.submission,
                content=_(f"{user} assigned as an editor."),
                content_en=f"{user} assigned as an editor.",
                user=self.request.user,
                significant=True,
            )

        return response
