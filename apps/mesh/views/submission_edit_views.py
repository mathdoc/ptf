from functools import cached_property
from typing import Any

from django.contrib import messages
from django.db.models import QuerySet
from django.http import HttpRequest
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import FormView
from django.views.generic import TemplateView
from django.views.generic import View
from django.views.generic.edit import CreateView
from django.views.generic.edit import UpdateView

from ..file_helpers import post_delete_model_file
from ..forms.base_forms import FormAction
from ..forms.base_forms import HiddenModelChoiceForm
from ..forms.submission_forms import SubmissionAuthorForm
from ..forms.submission_forms import SubmissionConfirmForm
from ..forms.submission_forms import SubmissionCreateForm
from ..forms.submission_forms import SubmissionUpdateForm
from ..forms.submission_forms import SubmissionVersionForm
from ..mixins import RoleMixin
from ..models.submission_models import Submission
from ..models.submission_models import SubmissionAuthor
from ..models.submission_models import SubmissionLog
from ..models.submission_models import SubmissionVersion
from ..roles.author import Author
from ..views.base_views import SUBMIT_QUERY_PARAMETER
from ..views.base_views import SubmittableModelFormMixin

# from .components.breadcrumb import get_base_breadcrumb
# from .components.breadcrumb import get_submission_breadcrumb
from .components.button import Button
from .components.stepper import get_submission_stepper
from .model_proxy import SubmissionProxy

# def submission_stepper_form_buttons() -> list[Button]:
#     return [
#         Button(
#             id="form_save",
#             title=_("Save as draft"),
#             icon_class="fa-floppy-disk",
#             attrs={"type": ["submit"], "class": ["save-button", "light"]},
#         ),
#         Button(
#             id="form_next",
#             title=_("Next"),
#             icon_class="fa-right-long",
#             attrs={"type": ["submit"], "class": ["save-button"], "name": [FormAction.NEXT.value]},
#         ),
#     ]


class SubmissionCreateView(RoleMixin, CreateView):
    """
    View for submitting a new submission.
    """

    model = Submission
    form_class = SubmissionCreateForm
    template_name = "mesh/forms/form_full_page.html"
    restricted_roles = [Author]

    def restrict_dispatch(self, request: HttpRequest, *args, **kwargs):
        return not self.role_handler.check_rights("can_create_submission")

    def get_success_url(self) -> str:
        """
        Redirects to the SubmissionAuthor view to add authors to the
        newly created submission.
        """
        if FormAction.NEXT.value in self.request.POST:
            return reverse_lazy(
                "mesh:submission_authors", kwargs={"pk": self.object.pk}  # type:ignore
            )
        return reverse_lazy(
            "mesh:submission_details", kwargs={"pk": self.object.pk}  # type:ignore
        )

    def form_valid(self, form: SubmissionCreateForm) -> HttpResponse:
        """
        Injects the current user in the form instance.
        Adds a message to the submission log.
        """
        form.instance._user = self.request.user

        response = super().form_valid(form)

        SubmissionLog.add_message(
            self.object,  # type:ignore
            content=_("Creation of the submission"),
            content_en="Creation of the submission",
            user=self.request.user,
            significant=True,
        )

        messages.success(self.request, _("New submission successfully created."))
        return response

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["page_title"] = _("New submission")

        # Stepper
        step_id = "metadata"
        stepper = get_submission_stepper(None)
        stepper.set_active_step(step_id)
        context["stepper"] = stepper

        # Form buttons
        # No Save/Next button in the form, they are put in the stepper
        context["form"].buttons = []  # = submission_stepper_form_buttons()

        descriptions = [
            "You are about to start the submit process.",
            'Please read our guidelines <a href="">here</a>',
        ]
        context["form_description"] = descriptions

        # # Generate breadcrumb data
        # breadcrumb = get_base_breadcrumb()
        # breadcrumb.add_item(title=_("New submission"), url=reverse_lazy("mesh:submission_create"))
        # context["breadcrumb"] = breadcrumb
        return context


class SubmissionUpdateView(RoleMixin, UpdateView):
    """
    View for updating a submission (only the metadata).
    """

    model: type[Submission] = Submission
    form_class = SubmissionUpdateForm
    template_name = "mesh/forms/form_full_page.html"
    submission: Submission
    restricted_roles = [Author]
    message_on_restrict = False

    def restrict_dispatch(self, request: HttpRequest, *args, **kwargs):
        pk = kwargs["pk"]
        self.submission = get_object_or_404(self.model, pk=pk)

        return not self.role_handler.check_rights("can_edit_submission", self.submission)

    def get_fail_redirect_uri(self) -> str:
        """
        Return to the submission details page if the user cannot update the submission.
        """
        return self.get_success_url()

    def get_object(self, *args, **kwargs) -> Submission:
        """
        Override `get_object` built-in method to avoid an additional look-up when
        we already have the object loaded.
        """
        return self.submission

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["page_title"] = _("Edit article metadata")

        if self.submission.is_draft:
            step_id = "metadata"
            stepper = get_submission_stepper(self.submission)
            stepper.set_active_step(step_id)
            context["stepper"] = stepper

            # No Save/Next button in the form, they are put in the stepper
            context["form"].buttons = []  # = submission_stepper_form_buttons()

        # Generate breadcrumb data
        # breadcrumb = get_submission_breadcrumb(self.submission)
        # breadcrumb.add_item(
        #     title=_("Edit metadata"),
        #     url=reverse_lazy("mesh:submission_update", kwargs={"pk": self.submission.pk}),
        # )
        # context["breadcrumb"] = breadcrumb

        return context

    def get_success_url(self) -> str:
        if FormAction.NEXT.value in self.request.POST:
            return reverse_lazy("mesh:submission_authors", kwargs={"pk": self.submission.pk})
        return reverse_lazy("mesh:submission_details", kwargs={"pk": self.submission.pk})

    def form_valid(self, form: SubmissionUpdateForm) -> HttpResponse:
        form.instance._user = self.request.user
        if not form.changed_data:
            form.instance.do_not_update = True

        response = super().form_valid(form)

        if form.has_changed():
            SubmissionLog.add_message(
                self.submission,
                content=_("Modification of the submission's metadata"),
                content_en="Modification of the submission's metadata",
                user=self.request.user,
            )
        return response


class SubmissionVersionCreateView(RoleMixin, SubmittableModelFormMixin, CreateView):
    """
    View for creating a new submission version.
    """

    model = SubmissionVersion
    form_class = SubmissionVersionForm
    template_name = "mesh/forms/form_full_page.html"
    submission: Submission
    restricted_roles = [Author]
    save_submission = False
    add_confirm_message = False

    def restrict_dispatch(self, request: HttpRequest, *args, **kwargs):
        submission_pk = kwargs["submission_pk"]
        self.submission = get_object_or_404(Submission, pk=submission_pk)

        return not self.role_handler.check_rights("can_create_version", self.submission)

    def get_success_url(self) -> str:
        if FormAction.NEXT.value in self.request.POST:
            return self.submit_url()
        return reverse_lazy("mesh:submission_details", kwargs={"pk": self.submission.pk})

    def submit_url(self) -> str:
        return reverse_lazy("mesh:submission_confirm", kwargs={"pk": self.submission.pk})

    def form_pre_save(self, form: SubmissionVersionForm):
        form.instance._user = self.request.user
        form.instance.submission = self.submission

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        current_version = self.submission.current_version
        version = current_version.number + 1 if current_version else 1
        context["page_title"] = _("Submission files")
        if not self.submission.is_draft:
            context["page_title"] += f" - Version {version}"

        submission_url = reverse_lazy("mesh:submission_details", kwargs={"pk": self.submission.pk})
        description = "Please fill the form below to submit your files for the submission "
        description += f"<a href='{submission_url}'><i>{self.submission.name}</i></a>.<br>"
        context["form_description"] = [description]

        if self.submission.is_draft:
            stepper = get_submission_stepper(self.submission)
            stepper.set_active_step("version")
            context["stepper"] = stepper
            context["form"].buttons = []

        # # Generate breadcrumb data
        # breadcrumb = get_submission_breadcrumb(self.submission)
        # breadcrumb.add_item(
        #     title=_("New version"),
        #     url=reverse_lazy(
        #         "mesh:submission_version_create", kwargs={"submission_pk": self.submission.pk}
        #     ),
        # )
        # context["breadcrumb"] = breadcrumb
        return context


class SubmissionVersionUpdateView(RoleMixin, SubmittableModelFormMixin, UpdateView):
    """
    View for updating a submission version.

    Submitting a version = submitting the whole submission.
    This requires extra care about checking that all previous steps have been
    taken when this is the first version.
    """

    model: type[SubmissionVersion] = SubmissionVersion
    form_class = SubmissionVersionForm
    template_name = "mesh/forms/form_full_page.html"
    version: SubmissionVersion
    restricted_roles = [Author]
    message_on_restrict = False
    save_submission = False
    add_confirm_message = False

    def restrict_dispatch(self, request: HttpRequest, *args, **kwargs):
        pk = kwargs["pk"]
        self.version = get_object_or_404(self.model, pk=pk)
        return not self.role_handler.check_rights("can_edit_version", self.version)

    def get_fail_redirect_uri(self) -> str:
        """
        Redirects to the submission details page if the user cannot update the version.
        """
        return self.get_success_url()

    def get_object(self, *args, **kwargs) -> SubmissionVersion:
        """
        Returns the already fetched version.
        """
        return self.version

    def get_form_kwargs(self) -> dict[str, Any]:
        kwargs = super().get_form_kwargs()
        if self.request.GET.get(SUBMIT_QUERY_PARAMETER, None) == "true":
            kwargs[SUBMIT_QUERY_PARAMETER] = True
        return kwargs

    def get_success_url(self) -> str:
        if FormAction.NEXT.value in self.request.POST:
            return self.submit_url()
        return reverse_lazy("mesh:submission_details", kwargs={"pk": self.version.submission.pk})

    def submit_url(self) -> str:
        return reverse_lazy("mesh:submission_confirm", kwargs={"pk": self.version.submission.pk})

    def form_pre_save(self, form: SubmissionVersionForm) -> None:
        form.instance._user = self.request.user

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["page_title"] = _(f"Submission files - Version {self.version.number}")

        submission_url = reverse_lazy(
            "mesh:submission_details", kwargs={"pk": self.version.submission.pk}
        )
        description = "Please fill the form below to submit your files for the submission "
        description += f"<a href='{submission_url}'><i>{self.version.submission.name}</i></a>.<br>"
        context["form_description"] = [description]

        if self.version.submission.is_draft:
            stepper = get_submission_stepper(self.version.submission)
            stepper.set_active_step("version")
            context["stepper"] = stepper
            context["form"].buttons = []

        # # Generate breadcrumb data
        # breadcrumb = get_submission_breadcrumb(self.version.submission)
        # breadcrumb.add_item(
        #     title=_("Edit version") + f" #{self.version.number}",
        #     url=reverse_lazy("mesh:submission_version_update", kwargs={"pk": self.version.pk}),
        # )
        # context["breadcrumb"] = breadcrumb
        return context

    def post(self, request: HttpRequest, *args, **kwargs) -> HttpResponse:
        """
        View for updating the submission or deleting one of the associated files.
        """
        deletion_requested, _ = post_delete_model_file(self.version, request, self.role_handler)

        if deletion_requested:
            # Update the submission object.
            self.version = get_object_or_404(self.model, pk=self.version.pk)
            return self.get(request, *args, **kwargs)

        return super().post(request, *args, **kwargs)


class SubmissionResumeView(RoleMixin, View):
    """
    View handling the first submit of a submission. It resumes the submission process
    at the correct edit view according to the missing data.

    Handles the submission stepper. It redirects to the correct step according to
    the submission status.
    -   STEP 1:     Metadata
    -   STEP 2:     Authors
    -   STEP 3:     Files
    -   STEP 4:     Confirmation
    """

    submission: Submission
    restricted_roles = [Author]

    def restrict_dispatch(self, request: HttpRequest, *args, **kwargs) -> bool:
        self.submission = get_object_or_404(Submission, pk=kwargs["pk"])
        return not self.role_handler.check_rights("can_submit_submission", self.submission)

    def get(self, request: HttpRequest, *args, **kwargs) -> HttpResponse:
        stepper = get_submission_stepper(self.submission)

        # Redirects to the last active step
        for i in range(len(stepper.steps)):
            step = stepper.steps[-(i + 1)]
            if step.href and step.can_navigate:
                return HttpResponseRedirect(step.href)

        return HttpResponseRedirect(reverse_lazy("mesh:submission_create"))


class SubmissionAuthorView(RoleMixin, TemplateView):
    """
    View to add/remove a submission author from a given submission.
    """

    submission: Submission
    template_name = "mesh/forms/submission_author.html"
    restricted_roles = [Author]
    init_form: SubmissionAuthorForm | None = None
    _FORM_ACTION_CORRESPONDING = "_action_toggle_corresponding"

    def restrict_dispatch(self, request: HttpRequest, *args, **kwargs) -> bool:
        self.submission = get_object_or_404(Submission, pk=kwargs["pk"])
        return not self.role_handler.check_rights("can_edit_submission", self.submission)

    @cached_property
    def authors(self) -> QuerySet[SubmissionAuthor]:
        return SubmissionAuthor.objects.filter(submission=self.submission)

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)

        context["submission"] = self.submission

        authors = self.authors
        context["authors"] = [
            {
                "author": author,
                "delete_form": HiddenModelChoiceForm(
                    _queryset=authors,
                    form_action=FormAction.DELETE.value,
                    initial={"instance": author},
                ),
                "corresponding_form": HiddenModelChoiceForm(
                    _queryset=authors,
                    form_action="_action_toggle_corresponding",
                    initial={"instance": author},
                ),
                "buttons": [
                    Button(
                        id=f"author-corresponding-{author.pk}",
                        title=_("Corresponding"),
                        icon_class=("fa-toggle-on" if author.corresponding else "fa-toggle-off"),
                        form=HiddenModelChoiceForm(
                            _queryset=authors, initial={"instance": author}
                        ),
                        attrs={
                            "href": [
                                reverse_lazy(
                                    "mesh:submission_authors", kwargs={"pk": self.submission.pk}
                                )
                            ],
                            "type": ["submit"],
                            "class": ["primary" if author.corresponding else "inactive"],
                            "name": [self._FORM_ACTION_CORRESPONDING],
                            "data-tooltip": [
                                _("Click to toggle whether the author is a corresponding contact.")
                            ],
                        },
                    ),
                    Button(
                        id=f"author-delete-{author.pk}",
                        title=_("Remove"),
                        icon_class="fa-trash",
                        form=HiddenModelChoiceForm(
                            _queryset=authors, initial={"instance": author}
                        ),
                        attrs={
                            "href": [
                                reverse_lazy(
                                    "mesh:submission_authors", kwargs={"pk": self.submission.pk}
                                )
                            ],
                            "type": ["submit"],
                            "class": ["button-error"],
                            "name": [FormAction.DELETE.value],
                        },
                    ),
                ],
            }
            for author in authors
        ]

        initial = {}
        if not authors:
            initial.update(
                {
                    "first_name": self.request.user.first_name,  # type:ignore
                    "last_name": self.request.user.last_name,  # type:ignore
                    "email": self.request.user.email,  # type:ignore
                    "primary": True,
                }
            )

        form = self.init_form or SubmissionAuthorForm(
            submission=self.submission, initial=initial or None
        )

        form.buttons = [
            Button(
                id="form_save",
                title=_("Add author"),
                icon_class="fa-plus",
                attrs={"type": ["submit"], "class": ["save-button"]},
            )
        ]

        context["page_title"] = _("Authors")

        if self.submission.is_draft:
            step_id = "authors"
            stepper = get_submission_stepper(self.submission)
            stepper.set_active_step(step_id)
            context["stepper"] = stepper
            # version_step = stepper.get_step("version")
            # if version_step and version_step.can_navigate and version_step.href:
            # button = Button(
            #     id="next",
            #     title=_("Next"),
            #     icon_class="fa-right-long",
            #     attrs={"href": [version_step.href], "class": ["as-button"]},
            # )
            # context["title_buttons"] = [button]
            #
            # form.buttons.append(
            #     Button(
            #         id="form_next",
            #         title=_("Next"),
            #         icon_class="fa-right-long",
            #         attrs={
            #             "href": [version_step.href],
            #             "type": ["submit"],
            #             "class": ["as-button", "save-button"],
            #         },
            #     )
            # )

        context["form"] = form

        # # Generate breadcrumb data
        # breadcrumb = get_submission_breadcrumb(self.submission)
        # breadcrumb.add_item(
        #     title=_("Authors"),
        #     url=reverse_lazy("mesh:submission_authors", kwargs={"pk": self.submission.pk}),
        # )
        # context["breadcrumb"] = breadcrumb

        return context

    def post(self, request: HttpRequest, *args, **kwargs) -> HttpResponse:
        if FormAction.DELETE.value in request.POST:
            return self.remove_author(request)
        elif self._FORM_ACTION_CORRESPONDING in request.POST:
            return self.toggle_primary_author(request)

        return self.add_author(request, *args, **kwargs)

    def add_author(self, request: HttpRequest, *args, **kwargs) -> HttpResponse:
        """
        Add an `SubmissionAuthor` to the submission.
        """
        response = HttpResponseRedirect(
            reverse_lazy("mesh:submission_authors", kwargs={"pk": self.submission.pk})
        )
        form = SubmissionAuthorForm(request.POST, submission=self.submission)
        if not form.is_valid():
            self.init_form = form
            return self.get(request, *args, **kwargs)

        form.instance._user = self.request.user
        form.instance.submission = self.submission
        form.save()

        SubmissionLog.add_message(
            self.submission,
            content=f"Author added: {form.instance.first_name} {form.instance.last_name} ({form.instance.email})",
            content_en=_("Author added")
            + f": {form.instance.first_name} {form.instance.last_name} ({form.instance.email})",
            user=request.user,
            significant=True,
        )
        messages.success(
            request,
            _("Author added")
            + f": {form.instance.first_name} {form.instance.last_name} ({form.instance.email})",
        )

        return response

    def remove_author(self, request: HttpRequest) -> HttpResponse:
        """
        Remove a `SubmissionAuthor` from the submission.
        """
        response = HttpResponseRedirect(
            reverse_lazy("mesh:submission_authors", kwargs={"pk": self.submission.pk})
        )
        form = HiddenModelChoiceForm(request.POST, _queryset=self.authors)
        if not form.is_valid():
            messages.error(request, _("Something went wrong. Please try again."))
            return response

        if len(self.authors) < 2:
            messages.error(
                request, _("There must be at least 1 author attached to the submission.")
            )
            return response

        author: SubmissionAuthor = form.cleaned_data["instance"]
        author_string = str(author)
        author.delete()

        SubmissionLog.add_message(
            self.submission,
            content=f"Author removed: {author_string}",
            content_en=_("Author added") + f": {author_string}",
            request=request.user,
            significant=True,
        )
        messages.success(request, _("The author has been removed."))
        return response

    def toggle_primary_author(self, request: HttpRequest) -> HttpResponse:
        """
        Toggle the `corresponding` boolean of a `SubmissionAuthor` from the submission.
        """
        response = HttpResponseRedirect(
            reverse_lazy("mesh:submission_authors", kwargs={"pk": self.submission.pk})
        )
        form = HiddenModelChoiceForm(request.POST, _queryset=self.authors)
        if not form.is_valid():
            messages.error(request, _("Something went wrong. Please try again."))
            return response

        author: SubmissionAuthor = form.cleaned_data["instance"]
        author.corresponding = not author.corresponding
        author.save()
        word = "marked" if author.corresponding else "unmarked"
        messages.success(
            request, _(f"Author { author } has been { word } as a corresponding contact.")
        )

        return response


class SubmissionConfirmView(RoleMixin, FormView):
    """
    View to confirm the submission the current SubmissionVersion.

    It's used both when submitting a Submission for the first time and when
    submitting a revised SubmissionVersion.
    """

    template_name = "mesh/submission/version_confirm.html"
    submission: Submission
    form_class = SubmissionConfirmForm

    def restrict_dispatch(self, request: HttpRequest, *args, **kwargs) -> bool:
        self.submission = get_object_or_404(Submission, pk=kwargs["pk"])

        if not self.role_handler.check_rights("can_edit_submission", self.submission):
            return True

        return not self.submission.is_submittable

    def get_success_url(self):
        return reverse_lazy("mesh:submission_details", kwargs={"pk": self.submission.pk})

    def get_object(self, queryset=None) -> SubmissionVersion:
        return self.submission.current_version  # type:ignore

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)

        context["form"].buttons = [
            Button(
                id="form_save",
                title=_("Submit"),
                icon_class="fa-check",
                attrs={"type": ["submit"], "class": ["save-button", "button-highlight"]},
            )
        ]

        if self.submission.is_draft:
            stepper = get_submission_stepper(self.submission)
            stepper.set_active_step("confirm")
            context["stepper"] = stepper

        context["submission_proxy"] = SubmissionProxy(self.submission, self.role_handler)

        files = []
        files.append(
            {
                "file_wrapper": self.submission.current_version.main_file,  # type:ignore
                "type": "Main",
            }
        )

        for file_wrapper in self.submission.current_version.additional_files.all():  # type:ignore
            files.append({"file_wrapper": file_wrapper, "type": "Additional"})
        context["submission_files"] = files

        # # Breadcrumb
        # breadcrumb = get_submission_breadcrumb(self.submission)
        # breadcrumb.add_item(
        #     title=_("Authors"),
        #     url=reverse_lazy("mesh:submission_confirm", kwargs={"pk": self.submission.pk}),
        # )
        # context["breadcrumb"] = breadcrumb

        return context

    def form_valid(self, form: SubmissionConfirmForm) -> HttpResponse:
        """
        Submit the submission's current version.
        """
        self.submission.submit(self.request.user)

        if self.submission.current_version.number == 1:  # type:ignore
            messages.success(
                self.request,
                _("Your submission has been successfully saved and confirmed."),
            )
        else:
            messages.success(self.request, _("Your revisions were successfully submitted."))

        return HttpResponseRedirect(self.get_success_url())
