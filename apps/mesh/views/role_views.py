from django.contrib import messages
from django.http import HttpRequest
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import View

from ptf.url_utils import add_query_parameters_to_url

from ..exceptions import RoleException
from ..forms.role_forms import RoleSwitchForm
from ..mixins import ROLE_SWITCH_QUERY_PARAM
from ..mixins import RoleMixin


class RoleSwitchView(RoleMixin, View):
    """
    View used to switch the user's current role.

    Appends the query parameter `ROLE_SWITCH_QUERY_PARAM` to the redirect URL to
    indicate a role switch was requested.
    The info is not stored in the session because it would require the session
    to be always correctly cleaned afterwards.
    On the other hand a query parameter disappears on the next navigation.
    """

    def post(self, request: HttpRequest, *args, **kwargs):
        form = RoleSwitchForm(request.POST)
        redirect_url = request.headers.get("referer", None)
        if not redirect_url:
            redirect_url = reverse_lazy("mesh:home")
        # If there's an URL to redirect to, add the query parameter _role_switch=true
        else:
            redirect_url = add_query_parameters_to_url(
                redirect_url, {ROLE_SWITCH_QUERY_PARAM: ["true"]}
            )

        response = HttpResponseRedirect(redirect_url)
        if not form.is_valid():
            messages.error(request, "Error: Something went wrong.")
            return response

        try:
            role = self.role_handler.switch_role(form.cleaned_data["role_code"])
            messages.success(request, f"Successfully switched to '{role.name()}' view.")
        except RoleException as e:
            messages.error(request, str(e))

        return response
