import os
import re

from django_sendfile import sendfile

from django.http import HttpRequest
from django.http import HttpResponse
from django.http import HttpResponseNotFound
from django.views.generic import View

from ptf.url_utils import decode_from_url

from ..mixins import RoleMixin
from ..models.editorial_models import EditorialDecisionFile
from ..models.file_models import BaseFileWrapperModel
from ..models.review_models import ReviewAdditionalFile
from ..models.submission_models import SubmissionAdditionalFile
from ..models.submission_models import SubmissionMainFile

# Django Models inheriting from our custom BaseFileWrapperModel.
# cf. FileServingView.
FILE_MODELS = [
    SubmissionMainFile,
    SubmissionAdditionalFile,
    ReviewAdditionalFile,
    EditorialDecisionFile,
]


class FileServingView(RoleMixin, View):
    """
    This view is used to check the user rights before serving a file.

    File models protected by this process must be registered in the `FILE_MODELS`
    variable. The requested file is identified by iterating over the registered models
    and asking if the URL match their unique pattern.
    """

    def get(self, request: HttpRequest, *args, **kwargs) -> HttpResponse:
        bad_response = HttpResponseNotFound("The requested file was not found on this server.")

        # Decode the base64 encoded file_identifier
        file_identifier = kwargs["file_identifier"]
        try:
            file_identifier = decode_from_url(file_identifier)
        except Exception:
            return bad_response

        # Loop over the registered models to find the requested file
        for model in FILE_MODELS:
            lookup_func = getattr(model, "reverse_file_path", None)
            if not lookup_func:
                continue

            instance: BaseFileWrapperModel | None = lookup_func(file_identifier)
            if instance is None:
                continue

            # Make sure the file exists
            if not os.path.exists(instance.file.path):
                break

            # Check if the user can access the requested file
            # We need to check the right over the file with all roles
            right_check_func = getattr(instance, "check_access_right", None)
            if not right_check_func or not right_check_func(self.role_handler, "read"):
                break

            response = sendfile(request, instance.file.path)

            # Set the original name of the served file for display to the
            # download client
            if not hasattr(response, "headers"):
                return response

            resp_headers = response.headers
            content_disposition = resp_headers.get("Content-Disposition")
            if not content_disposition:
                return response

            filename_match = re.search(r"(?P<filename>filename=\".+?\")", content_disposition)
            if not filename_match:
                return response

            filename = filename_match.group("filename")
            resp_headers["Content-Disposition"] = content_disposition.replace(
                filename, f'filename="{instance.name}"'
            )
            return response

        return bad_response
