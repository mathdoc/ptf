from __future__ import annotations

from ...models.submission_models import SubmissionVersion
from ...views import model_proxy
from .model_proxy import RoleModelProxy


class SubmissionVersionProxy(RoleModelProxy):
    """
    Proxy for the `SubmissionVersion` model with restricted data according to the user role.
    """

    model = SubmissionVersion
    _instance: SubmissionVersion

    @property
    def created_by(self):
        if self._role_handler.check_rights("can_access_submission_author", self._instance):
            return str(self._instance.submission)

        return "**** ****"

    def get_all_reviews(self) -> list:
        if not self._role_handler.check_rights("can_access_reviews", self._instance):
            return []

        reviews = self._instance.reviews.all()  # type:ignore

        allowed_reviews = []
        for review in reviews:
            if self._role_handler.check_rights("can_access_review", review):
                review_proxy = model_proxy.review.ReviewProxy(review, self._role_handler)
                allowed_reviews.append(review_proxy)

        return allowed_reviews

    @property
    def submission(self):
        if not self._role_handler.check_rights("can_access_submission", self._instance.submission):
            return None

        return model_proxy.submission.SubmissionProxy(
            self._instance.submission, self._role_handler
        )

    @property
    def reviews(self):
        return self.get_all_reviews()
