from __future__ import annotations

from abc import ABC
from typing import TYPE_CHECKING
from typing import Any

from django.db.models import Model

# Require for type hint with __future__.annotations
if TYPE_CHECKING:
    from ...roles.role_handler import RoleHandler


class RoleModelProxy(ABC):
    """
    Defines a proxy for a model class when some properties require to be adapted according to the user role rights.
    To be used only for template display.

    The logic is to overload a model's attribute in the associated interface.
    If the attribute is not found in the interface, it falls back to the instance value.

    Additionally, we also provide a shortcut to calling rights function with
    the current instance as an argument.
    """

    model: type[Model]
    _instance: Model
    _role_handler: RoleHandler

    def __init__(self, instance: Model, role_handler: RoleHandler) -> None:
        self._instance = instance
        self._role_handler = role_handler

    def __getattr__(self, name: str) -> Any:
        """
        Fetches the attribute from the underlying instance.
        This gets called only if the attribute is not found normally.
        https://docs.python.org/3/reference/datamodel.html#object.__getattr__

        If it doesn't exist, we authorize shortcuts to right function check.
        """

        attr = getattr(self._instance, name)
        return attr
