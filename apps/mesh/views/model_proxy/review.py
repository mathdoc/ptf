from __future__ import annotations

from functools import cached_property

from ...models.review_models import Review
from ...views import model_proxy
from .model_proxy import RoleModelProxy


class ReviewProxy(RoleModelProxy):
    """
    Proxy for the `Review` model with restricted data according to the user role.
    """

    model = Review
    _instance: Review

    @property
    def reviewer(self):
        if self._role_handler.check_rights("can_access_review_author", self._instance):
            return str(self._instance.reviewer)

        return "**** ****"

    @property
    def version(self):
        if not self._role_handler.check_rights("can_access_version", self._instance.version):
            return None

        return model_proxy.submission_version.SubmissionVersionProxy(
            self._instance.version, self._role_handler
        )

    @cached_property
    def additional_files(self):
        files = self._instance.additional_files.all()  # type:ignore
        return [f for f in files if self._role_handler.check_rights("can_access_review_file", f)]
