from __future__ import annotations

from functools import cached_property
from typing import TYPE_CHECKING

from ...models.submission_models import Submission
from ...models.submission_models import SubmissionState
from ...roles.role_handler import RoleVisitor
from ...views import model_proxy
from ...views.components.review_summary import build_review_summary
from .model_proxy import RoleModelProxy

# Require for type hint with __future__.annotations
if TYPE_CHECKING:
    from ...interfaces.submission_interfaces import SubmissionStatusData


class SubmissionProxy(RoleModelProxy):
    """
    Proxy for the `Submission` model with restricted data according to the user role.
    """

    model = Submission
    _instance: Submission

    def __eq__(self, other):
        """Overrides the default implementation"""
        if isinstance(other, Submission):
            return self._instance == other
        return self == other

    @property
    def authors(self):
        if not self._role_handler.check_rights("can_access_submission_author", self._instance):
            return None
        return self._instance.all_authors

    @property
    def authors_string(self):
        if not self._role_handler.check_rights("can_access_submission_author", self._instance):
            return "**** ****"

        authors = self._instance.all_authors
        return ", ".join([f"{a.first_name[0]}. {a.last_name}" for a in authors])

    @property
    def created_by(self):
        if self._role_handler.check_rights("can_access_submission_author", self._instance):
            return str(self._instance.created_by)

        return "**** ****"

    @cached_property
    def all_versions(self) -> list[model_proxy.submission_version.SubmissionVersionProxy]:
        versions = self._instance.all_versions

        allowed_versions = []
        for version in versions:
            if self._role_handler.check_rights("can_access_version", version):
                submission_version_proxy = model_proxy.submission_version.SubmissionVersionProxy(
                    version, self._role_handler
                )
                allowed_versions.append(submission_version_proxy)

        return allowed_versions

    @property
    def versions(self):
        return self.all_versions

    @cached_property
    def status(self) -> SubmissionStatusData:
        return self._role_handler.get_from_rights("get_submission_status", self._instance)


class BuildSubmissionProxyVisitor(RoleVisitor):
    """ "
    Returns a SubmissionProxy based on the role
    Augments the interface with widgets to be displayed according to the role (ex: ReviewSummary)
    """

    def visit_author(self, submission):
        submission_proxy = SubmissionProxy(submission, self.role_handler)
        return submission_proxy

    def visit_editor(self, submission):
        submission_proxy = SubmissionProxy(submission, self.role_handler)

        if (
            self.role_handler.check_rights("can_access_submission", submission)
            and submission.current_version
            and submission.state == SubmissionState.ON_REVIEW.value
        ):
            review_summary = build_review_summary(submission)
            submission_proxy.review_summary = review_summary

        return submission_proxy

    def visit_journal_manager(self, submission):
        return self.visit_editor(submission)

    def visit_reviewer(self, submission):
        return SubmissionProxy(submission, self.role_handler)
