from __future__ import annotations

import os
import re
from datetime import date
from enum import Enum
from enum import unique
from typing import TYPE_CHECKING
from typing import Self
from typing import TypeVar

from django.conf import settings
from django.db import models
from django.db.models.query import QuerySet
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from ..exceptions import ReviewStateError
from .base_models import BaseSubmittableModel
from .file_models import BaseFileWrapperModel
from .file_models import BaseModelWithFiles
from .submission_models import SubmissionLog
from .submission_models import SubmissionVersion

if TYPE_CHECKING:
    from ..roles.role_handler import RoleHandler


_T = TypeVar("_T", bound=models.Model)


@unique
class ReviewState(Enum):
    """
    Enum of the review statees.
    """

    # AWAITING ACCEPTANCE       Reviewer invited, waiting for reviewer to accept/decline
    #                           the review request.
    AWAITING_ACCEPTANCE = "awaiting_accept"
    # DECLINED                  Reviewer invited & declined the request.
    DECLINED = "declined"
    # PENDING                   Reviewer invited & accepted the request, waiting for
    #                           the review.
    PENDING = "pending"
    # SUBMITTED                 Reviewer invited, accepted the request and the review
    #                           was submitted.
    SUBMITTED = "submitted"


REVIEW_STATE_CHOICES = [
    (ReviewState.AWAITING_ACCEPTANCE.value, _("Waiting for acceptance")),
    (ReviewState.DECLINED.value, _("Declined")),
    (ReviewState.PENDING.value, _("Pending")),
    (ReviewState.SUBMITTED.value, _("Submitted")),
]


@unique
class RecommendationValue(Enum):
    """
    Enum of the recommendation values.
    Warning: the value of each state is used in CSS.
    """

    ACCEPTED = "accepted"
    REJECTED = "rejected"
    REVISION_REQUESTED = "rev_requested"
    RESUBMIT_SOMEWHERE_ELSE = "resubmit"


RECOMMENDATION_CHOICES = [
    (RecommendationValue.ACCEPTED.value, "Accept submission"),
    (RecommendationValue.REJECTED.value, "Decline submission"),
    (RecommendationValue.REVISION_REQUESTED.value, "Revision(s) required"),
    (RecommendationValue.RESUBMIT_SOMEWHERE_ELSE.value, "Re-submit somewhere else"),
]


class ReviewManager(models.Manager[_T]):
    def get_queryset(self) -> QuerySet[_T]:
        return (
            super()
            .get_queryset()
            .select_related(
                "created_by",
                "last_modified_by",
                "version",
                "version__submission",
                "version__submission__journal_section",
                "reviewer",
            )
            .prefetch_related("additional_files")
        )


class Review(BaseSubmittableModel, BaseModelWithFiles):
    file_fields_required = ["additional_files"]
    file_fields_deletable = ["additional_files"]

    version = models.ForeignKey(
        SubmissionVersion,
        on_delete=models.CASCADE,
        related_name="reviews",
        blank=True,
        help_text=_("Automatically filled on save"),
    )
    state = models.CharField(
        max_length=64,
        choices=REVIEW_STATE_CHOICES,
        default=ReviewState.AWAITING_ACCEPTANCE.value,
    )
    reviewer = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT)
    date_response_due = models.DateField(verbose_name=_("Response due date"))
    date_review_due = models.DateField(verbose_name=_("Review due date"))
    recommendation = models.CharField(
        max_length=64, choices=RECOMMENDATION_CHOICES, blank=False, null=True, default=""
    )
    accepted = models.BooleanField(
        verbose_name=_("Confirmation"),
        choices=[(True, _("Accept")), (False, _("Decline"))],
        null=True,
        blank=False,
    )
    date_accepted = models.DateTimeField(editable=False, null=True)
    # Optional message for the reviewer to be filled by the editor
    # when making a referee request.
    request_email = models.TextField(verbose_name=_("Request e-mail"), null=True, blank=True)
    # Optional message from the reviewer to the editor when accepting/declining the
    # referee request
    accept_comment = models.TextField(verbose_name=_("Accept comment"), null=True, blank=True)
    # Comment/message from the reviewer to the editor when filling the review form.
    comment = models.TextField(verbose_name=_("Review comment"), null=True, blank=True)

    objects: ReviewManager[Self] = ReviewManager()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["version", "reviewer"], name="unique_reviewer_per_round"
            )
        ]

    def __str__(self) -> str:
        return f"Review: {self.reviewer} - {self.version}"

    @property
    def is_response_overdue(self) -> bool:
        return self.accepted is None and self.date_response_due < date.today()

    @property
    def is_report_overdue(self) -> bool:
        return self.is_response_overdue or (
            self.accepted is True
            and self.submitted is False
            and self.date_review_due < date.today()
        )

    @property
    def is_completed(self) -> bool:
        return self.accepted is False or self.submitted is True

    @property
    def is_editable(self) -> bool:
        return not self.submitted and self.version.review_open

    def accept(
        self,
        accept_value: bool,
        accept_comment: str | None,
        user,
        impersonate_data=None,
    ) -> None:
        if not self.is_editable:
            raise ReviewStateError("Trying to accept a non-editable review.")

        self._user = user  # type:ignore
        self.accepted = accept_value
        self.state = ReviewState.PENDING.value if accept_value else ReviewState.DECLINED.value
        self.date_accepted = timezone.now()
        self.accept_comment = accept_comment

        self.save()

        base_message = (
            "Referee request accepted by" if accept_value else "Referee request declined by"
        )

        SubmissionLog.add_message(
            self.version.submission,
            content=_(base_message) + f" {self.reviewer}",
            content_en=f"{base_message} {self.reviewer}",
            user=user,
            # impersonate_data=impersonate_data,
            significant=True,
        )

    @property
    def is_submittable(self) -> bool:
        return not self.is_completed and self.is_editable

    def submit(self, user) -> None:
        if not self.is_submittable:
            raise ReviewStateError("Trying to submit a non-editable review.")

        self._user = user  # type:ignore
        self.state = ReviewState.SUBMITTED.value
        self.submitted = True

        self.save()

        SubmissionLog.add_message(
            self.version.submission,
            content=_(f"Review for round #{self.version.number} submitted by")
            + f" {self.reviewer}",
            content_en=f"Review for round #{self.version.number} submitted by {self.reviewer}",
            user=user,
            # impersonate_data=impersonate_data,
            significant=True,
        )


class ReviewAdditionalFile(BaseFileWrapperModel):
    attached_to: models.ForeignKey[Review] = models.ForeignKey(
        Review, on_delete=models.CASCADE, related_name="additional_files"
    )
    author_access = models.BooleanField(default=False)

    @staticmethod
    def get_upload_path(instance: ReviewAdditionalFile, filename: str) -> str:
        return os.path.join(
            "submissions",
            str(instance.attached_to.version.submission.pk),
            "versions",
            str(instance.attached_to.version.number),
            "review",
            str(instance.attached_to.pk),
            filename,
        )

    @classmethod
    def reverse_file_path(cls, file_path: str) -> ReviewAdditionalFile | None:
        """
        Check first with a regex if the given path might match. This is not necessary
        but it saves us a DB query in case in doesn't match the path structure.
        """
        regex = "/".join(
            [
                "submissions",
                r"(?P<submission_pk>[0-9]+)",
                "versions",
                r"(?P<version_number>[0-9]+)",
                "review",
                r"(?P<review_pk>[0-9]+)",
                r"(?P<file_name>[^\/]+)$",
            ]
        )
        match = re.match(re.compile(regex), file_path)
        if not match:
            return None

        return cls._default_manager.filter(file=file_path).first()

    def check_access_right(self, role_handler: RoleHandler, right_code: str) -> bool:
        """
        This model is only editable by user role with edit rights on the submission.
        """
        if right_code == "read":
            return role_handler.check_global_rights("can_access_review", self.attached_to)

        elif right_code in ["delete"]:
            return role_handler.check_rights("can_edit_review", self.attached_to)

        return False
