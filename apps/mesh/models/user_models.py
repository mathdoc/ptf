from __future__ import annotations

import secrets
from datetime import datetime
from typing import Self
from typing import TypeVar

from django.conf import settings
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import BaseUserManager
from django.db import DatabaseError
from django.db import IntegrityError
from django.db import models
from django.utils.translation import gettext_lazy as _

from ..app_settings import app_settings
from .base_models import BaseChangeTrackingModel

_T = TypeVar("_T", bound=AbstractUser)

USER_TOKEN_QUERY_PARAM = "_auth_token"


class UserManager(BaseUserManager[_T]):
    """
    Defines a model manager for our custom User model having no username field.
    """

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields) -> _T:
        """
        Create and save a User with the given email and password.
        """
        if not email:
            raise ValueError("The given email must be set")
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.password = make_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields) -> _T:
        """
        Create and save a regular User with the given email and password.
        """
        extra_fields.setdefault("is_active", True)
        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields) -> _T:
        """
        Create and save a SuperUser with the given email and password.
        """
        extra_fields.setdefault("is_active", True)
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        return self._create_user(email, password, **extra_fields)


class User(AbstractUser):
    """
    Custom user model without useless username field.
    Login can only be performed with e-mail address.
    """

    username = None
    first_name = models.CharField(_("first name"), max_length=150, blank=False, null=False)
    last_name = models.CharField(_("last name"), max_length=150, blank=False, null=False)
    email = models.EmailField(_("e-mail address"), unique=True)
    journal_manager = models.BooleanField(_("Journal manager"), default=False)
    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    current_role = models.CharField(max_length=32, blank=True, null=True)
    impersonate_data = None

    objects: UserManager[Self] = UserManager()

    def __str__(self) -> str:
        return f"{self.first_name} {self.last_name} ({self.email})"

    @property
    def is_token_authentication_allowed(self) -> bool:
        """
        For security purposes, we prevent token authentication for user with elevated
        rights (staff, admin and journal manager users).
        """
        return not (self.is_superuser or self.is_staff or self.journal_manager)


class UserToken(BaseChangeTrackingModel):
    """
    Token used for alternative authentication.
    """

    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="token"
    )
    key = models.CharField(max_length=256, unique=True)
    date_refreshed = models.DateField(blank=True, help_text="Automatically filled when created.")

    def save(self, *args, **kwargs):
        if self._state.adding:
            self.reset_refreshed_date()
        super().save(*args, **kwargs)

    def reset_refreshed_date(self):
        """
        Reset the user token refreshed date to now.
        """
        self.date_refreshed = datetime.utcnow().date()

    @property
    def is_expired(self) -> bool:
        return (
            datetime.utcnow().date()
            > self.date_refreshed + app_settings.USER_TOKEN_EXPIRATION_DAYS
        )

    @classmethod
    def get_token(cls, user: User, refresh_token: bool = True) -> Self:
        """
        Returns the existing token attached to the given user or creates and returns
        a fresh one.
        If the token already exists, its expiration date is reseted.
        """
        token = None
        try:
            token = cls.objects.get(user=user)
        except cls.DoesNotExist:
            pass

        if token is not None:
            if refresh_token:
                token.reset_refreshed_date()
                token.save()
            return token

        tries = 0
        while token is None and tries < 5:
            try:
                token = cls.objects.create(user=user, key=secrets.token_urlsafe(64))
            except IntegrityError:
                tries += 1
        if not token:
            raise DatabaseError(f"Could not create unique token for user {user}")

        return token
