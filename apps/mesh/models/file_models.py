from __future__ import annotations

import os
from collections.abc import Callable
from typing import TYPE_CHECKING

from django.core.exceptions import ValidationError
from django.core.files.uploadedfile import UploadedFile
from django.db import models
from django.urls import reverse_lazy

from ptf.url_utils import encode_for_url

from ..file_helpers import FILE_DELETE_DEFAULT_ERROR
from ..file_helpers import FILE_NAME_MAX_LENGTH
from ..file_helpers import MeshFileSystemStorage
from ..file_helpers import file_name
from .base_models import BaseChangeTrackingModel

if TYPE_CHECKING:
    from ..roles.role_handler import RoleHandler


MEGABYTE_VALUE = 1048576


class BaseModelWithFiles(BaseChangeTrackingModel):
    """
    Model template for a model with attached files using the below BaseFileWrapperModel
    model.
    """

    # List of the required file fields. We need to mention them somewhere because
    # some "required" file fields can have null=True for other purpose
    file_fields_required = []
    # List of file fields or related file models that can be deleted by an user.
    file_fields_deletable = []

    class Meta:
        abstract = True

    def can_delete_file(self, file_field: str) -> tuple[bool, str]:
        """
        Check function that returns whether the provided file field is deletable.
        Basic implementation only checks if the field is in the deletable fields list.

        Returns:
            -   result: bool        Whether the field can be deleted
            -   error_message: str  An explaining error message when `result=False`
        """
        return file_field in self.file_fields_deletable, FILE_DELETE_DEFAULT_ERROR


def get_upload_path_from_model(instance: models.Model, filename: str) -> str:
    """
    Calls the `get_upload_path` method of the instance's model.
    """
    model_class = instance.__class__
    upload_path_func = getattr(model_class, "get_upload_path", None)
    if not upload_path_func or not callable(upload_path_func):
        raise Exception(
            f"Error: model {model_class} does not have a callable `get_upload_path` " "method."
        )
    return upload_path_func(instance, filename)


class BaseFileWrapperModel(BaseChangeTrackingModel):
    """
    Base model class providing a wrapper around a single file field.
    This should be used instead of creating direct FileField on the models.
    """

    # Allowed file extensions
    file_extensions = [".pdf", ".docx"]
    # Max length for file name - Must be synced with the `name` field and
    # the file name + the prefix path should be less than FILE_NAME_MAX_LENGTH
    # 256 characters should be okay for any file.
    file_name_max_length = 256
    # File's max size (in bytes)
    file_max_size = 10 * MEGABYTE_VALUE

    file = models.FileField(
        max_length=FILE_NAME_MAX_LENGTH,
        upload_to=get_upload_path_from_model,
        storage=MeshFileSystemStorage,
        # validators=[file_validators], No validators - They are handled by forms
        blank=False,
        null=False,
    )
    # Stores the original file name. The file might be stored with a different name
    # on the server. Note that you must update this field when changing the `file`
    # field manually (it doesn't happen, usually a new Wrapper instance is created).
    name = models.CharField(max_length=256, blank=True, null=False)

    # The foreign key when the file is linked to another entity
    # (Should always be the case)
    attached_to: None | models.OneToOneField | models.ForeignKey = None

    class Meta:
        abstract = True

    @classmethod
    def get_help_text(cls) -> dict:
        """ """
        descriptions = []
        if cls.file_extensions:
            descriptions.append(f"Allowed files are {', '.join(cls.file_extensions)}.")
        descriptions.append(f"Maximum size: {cls.file_max_size / MEGABYTE_VALUE:.2f}Mb")
        return {"type": "list", "texts": descriptions}

    @staticmethod
    def get_upload_path(instance: BaseFileWrapperModel, filename: str) -> str:
        """
        Customizable hook to get the file upload path from the model instance.

        Note: We can't use @abstractmethod decorator here because we can't use
        ABCMeta as the metaclass
        """
        raise NotImplementedError

    @classmethod
    def reverse_file_path(cls, file_path: str) -> BaseFileWrapperModel | None:
        """
        Returns the model instance associated to the given file path if the path
        corresponds to a file of the current model.

        Note: We can't use @abstractmethod decorator here because we can't use
        ABCMeta as the metaclass
        """
        raise NotImplementedError

    @staticmethod
    def instance_valid_file(instance: BaseFileWrapperModel):
        return instance and instance.file and getattr(instance.file, "url", False)

    @classmethod
    def run_file_validators(cls, value: UploadedFile):
        for validator in cls.file_validators():
            validator(value)

    @classmethod
    def file_validators(cls) -> list[Callable]:
        """
        Returns all the model's file validators.
        Default is the 3 base validators: file_extension, file_name_length and
        file_size.
        """
        return [cls.validate_file_extension, cls.validate_file_name_length, cls.validate_file_size]

    @classmethod
    def validate_file_extension(cls, value: UploadedFile):
        """
        Validate the file extension wrt. the class requirements.
        All file types are allowed if `file_extensions` is empty.
        """
        if not cls.file_extensions:
            return
        extension = os.path.splitext(value.name)[1]
        if extension.lower() not in cls.file_extensions:
            raise ValidationError(
                f"Unsupported file extension: {extension.lower()}. "
                f"Valid file extension(s) are: {', '.join(cls.file_extensions)}"
            )

    @classmethod
    def validate_file_name_length(cls, value: UploadedFile):
        name = file_name(value.name)
        max_length = min(cls.file_name_max_length, FILE_NAME_MAX_LENGTH)
        if len(name) > max_length:
            raise ValidationError(
                f"The provided file name is too long: {name}. "
                f"The maximum allowed length is {max_length} characters."
            )

    @classmethod
    def validate_file_size(cls, value: UploadedFile):
        if value.size > cls.file_max_size:
            raise ValidationError(
                f"The provided file is too large: {value.size / (MEGABYTE_VALUE):.2f} MB. "
                f"Maximum allowed file size is {cls.file_max_size / (MEGABYTE_VALUE):.2f} MB."
            )

    def save(self, *args, **kwargs) -> None:
        """
        Deletes the old file from the filesystem if the database insertion/update
        succeeds.

        TODO: This is not robust with rollbacks. In case this is called during
        an encapsulating transaction that gets rolled back, this might create
        an inconsistent status : the file is effectively deleted but the database entry
        still points to the deleted file as it was rolled back...
        """
        # Set the file name now if it does not exist - It might be changed later by the storage
        self.name = self.name or file_name(self.file.name)

        original_path = None
        # Check if there's already a instance of this model with the current pk.
        # Store the old file path for potential deletion.
        if self.pk:
            try:
                original_model = self.__class__.objects.get(pk=self.pk)
                original_path = original_model.file.path
            except self.__class__.DoesNotExist:
                pass

        super().save(*args, **kwargs)

        # If the path changed, delete the old file.
        if (
            original_path is not None
            and self.file.path != original_path
            and os.path.exists(original_path)
        ):
            os.remove(original_path)

    def delete(self, *args, **kwargs) -> tuple[int, dict[str, int]]:
        """
        Deletes the file from the filesystem if the database deletion succeeds.
        """
        file_to_delete = self.file

        result = super().delete(*args, **kwargs)

        if file_to_delete:
            file_to_delete.delete(save=False)

        return result

    def check_access_right(self, role_handler: RoleHandler, right_code: str) -> bool:
        """
        Customizable hook to check the given access right.
        Only used when serving a file.
        Default is False.
        """
        return False

    def get_file_url(self) -> str:
        """
        Returns the URL to the model's file.
        """
        if not self.file.url:
            return ""
        file_identifier = encode_for_url(self.file.name)
        return reverse_lazy(
            "mesh:serve_protected_file", kwargs={"file_identifier": file_identifier}
        )
