from datetime import datetime
from datetime import timedelta
from typing import Generic
from typing import TypeVar

import factory
import factory.django

from .editorial_models import EditorialDecision
from .journal_models import JournalSection
from .review_models import Review
from .submission_models import Submission
from .submission_models import SubmissionAuthor
from .submission_models import SubmissionVersion
from .user_models import User

T = TypeVar("T")


class BaseTypingFactory(Generic[T], factory.django.DjangoModelFactory):
    """
    Factory boy does not provide correct type hints of created objects.
    For ex: the type of `sub = SubmissionFactory()` is `SubmissionFactory` when we want
    `Submission`

    This base class enables correct type hints when instantiating new objects
    using the `create()` method.
    Ex: `sub = SubmissionFactory.create()` will have the correct type: `Submission`.
    """

    @classmethod
    def create(cls, **kwargs) -> T:
        return super().create(**kwargs)

    @classmethod
    def build(cls, **kwargs) -> T:
        return super().build(**kwargs)


class UserFactory(BaseTypingFactory[User]):
    class Meta:
        model = User

    first_name = factory.Faker("first_name")
    last_name = factory.Faker("last_name")
    email = factory.Faker("email")
    password = factory.django.Password("my_password")


class JournalSectionFactory(BaseTypingFactory[JournalSection]):
    class Meta:
        model = JournalSection

    created_by = factory.SubFactory(UserFactory)
    name = factory.Faker("name")


class SubmissionFactory(BaseTypingFactory[Submission]):
    class Meta:
        model = Submission

    created_by = factory.SubFactory(UserFactory)
    name = factory.Faker("sentence")
    abstract = factory.Faker("paragraphs")
    author_agreement = True


class SubmissionVersionFactory(BaseTypingFactory[SubmissionVersion]):
    class Meta:
        model = SubmissionVersion

    submission = factory.SubFactory(SubmissionFactory)


class SubmissionAuthorFactory(BaseTypingFactory[SubmissionAuthor]):
    class Meta:
        model = SubmissionAuthor

    submission = factory.SubFactory(SubmissionFactory)
    first_name = factory.Faker("first_name")
    last_name = factory.Faker("last_name")
    email = factory.Faker("email")


class ReviewFactory(BaseTypingFactory[Review]):
    class Meta:
        model = Review

    reviewer = factory.SubFactory(UserFactory)
    version = factory.SubFactory(SubmissionVersionFactory)
    date_response_due = datetime.utcnow().date() + timedelta(days=3)
    date_review_due = datetime.utcnow().date() + timedelta(days=3)


class EditorialDecisionFactory(BaseTypingFactory[EditorialDecision]):
    class Meta:
        model = EditorialDecision

    created_by = factory.SubFactory(UserFactory)
    submission = factory.SubFactory(SubmissionFactory)
    comment = factory.Faker("paragraphs")
