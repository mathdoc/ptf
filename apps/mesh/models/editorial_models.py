from __future__ import annotations

import os
import re
from typing import TYPE_CHECKING

from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _

from ..file_helpers import FILE_DELETE_DEFAULT_ERROR
from .base_models import BaseChangeTrackingModel
from .file_models import BaseFileWrapperModel
from .file_models import BaseModelWithFiles
from .journal_models import JournalSection
from .submission_models import SUBMISSION_STATE_EDITOR_CHOICES
from .submission_models import Submission

if TYPE_CHECKING:
    from ..roles.role_handler import RoleHandler


class EditorSectionRight(BaseChangeTrackingModel):
    """
    ManyToMany table between User & JournalSection.
    Used to track Editor access/right to a submission journal_section (section).
    """

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="editor_sections"
    )
    journal_section = models.ForeignKey(
        JournalSection,
        verbose_name=_("Journal Section"),
        on_delete=models.CASCADE,
        related_name="editors",
    )


class EditorSubmissionRight(BaseModelWithFiles):
    """
    ManyToMany table representing an editor right to a submission, ie. an editor
    "assigned" to a submission.
    """

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="editor_submissions"
    )
    submission = models.ForeignKey(
        Submission, verbose_name=_("Submission"), on_delete=models.CASCADE, related_name="editors"
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["user", "submission"], name="unique_submission_per_editor"
            )
        ]


class EditorialDecision(BaseModelWithFiles):
    """
    Model representing an editorial decision. Edit restricted to editors and
    above rights a priori.
    """

    file_fields_deletable = ["additional_files"]
    submission = models.ForeignKey(
        Submission,
        on_delete=models.CASCADE,
        null=False,
        blank=False,
        editable=False,
        related_name="editorial_decisions",
    )
    value = models.CharField(
        verbose_name=_("Decision"),
        max_length=64,
        choices=SUBMISSION_STATE_EDITOR_CHOICES,
        null=False,
        blank=False,
    )
    comment = models.TextField(verbose_name=_("Description"), blank=True, null=True)

    def can_delete_file(self, file_field: str) -> tuple[bool, str]:
        """
        An additional file cannot be deleted if there's only one and the comment
        is empty.
        """
        if not super().can_delete_file(file_field):
            return False, FILE_DELETE_DEFAULT_ERROR

        if (
            file_field == "additional_files"
            and not self.comment
            and len(self.additional_files.all()) < 2  # type:ignore
        ):
            return False, _(
                "The decision requires either a file or a description. If you want to change the file, add the new one then delete the old one."
            )

        return True, ""


class EditorialDecisionFile(BaseFileWrapperModel):
    file_extensions = [".pdf"]
    attached_to: models.ForeignKey[EditorialDecision] = models.ForeignKey(
        EditorialDecision, on_delete=models.CASCADE, related_name="additional_files"
    )

    @staticmethod
    def get_upload_path(instance: EditorialDecisionFile, filename: str) -> str:
        return os.path.join(
            "submissions",
            str(instance.attached_to.submission.pk),
            "editor_decisions",
            str(instance.attached_to.pk),
            filename,
        )

    @classmethod
    def reverse_file_path(cls, file_path: str) -> EditorialDecisionFile | None:
        regex = "/".join(
            [
                "submissions",
                r"(?P<submission_pk>[0-9]+)",
                "editor_decisions",
                r"(?P<decision_pk>[0-9]+)",
                r"(?P<file_name>[^\/]+)$",
            ]
        )
        match = re.match(re.compile(regex), file_path)
        if not match:
            return None

        return cls._default_manager.filter(file=file_path).first()

    def check_access_right(self, role_handler: RoleHandler, right_code: str) -> bool:
        if right_code == "read":
            return role_handler.check_global_rights(
                "can_access_submission", self.attached_to.submission
            )

        elif right_code in ["delete"]:
            return role_handler.check_rights("can_edit_editorial_decision", self.attached_to)

        return False
