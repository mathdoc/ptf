from __future__ import annotations

import os
import re
from enum import Enum
from enum import unique
from functools import cached_property
from typing import TYPE_CHECKING
from typing import Self
from typing import TypeVar

from django.conf import settings
from django.db import models
from django.db.models import FilteredRelation
from django.db.models import Max
from django.db.models import Q
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from ..exceptions import SubmissionStateError
from .base_models import BaseChangeTrackingModel
from .base_models import BaseSubmittableModel
from .file_models import BaseFileWrapperModel
from .file_models import BaseModelWithFiles
from .log_models import ModelLog

# Used with annotations to enable correct typing without having circular import
if TYPE_CHECKING:
    from datetime import datetime

    from ..roles.role_handler import RoleHandler
    from .editorial_models import EditorialDecision
    from .user_models import User

_T = TypeVar("_T", bound=models.Model)


@unique
class SubmissionState(Enum):
    """
    Enum of the submission statees.
    Warning: the value of each state is used in CSS.
    """

    # OPENED                Submission created by the author but not submitted for
    #                       review yet.
    # Preferably use the boolean `is_draft` when checking for OPENED state
    OPENED = "opened"
    # SUBMITTED             Submission submitted for review. Waiting for editor to
    #                       take action (accept/reject/open review round)
    SUBMITTED = "submitted"
    # ON REVIEW             Open round OR no waiting for editor to take action
    #                       (accept/reject/open review round)
    ON_REVIEW = "review"
    # REVISION REQUESTED    Editor requested revisions.
    #                       Waiting for author to submit new version
    REVISION_REQUESTED = "rev_requested"
    # REVISION SUBMITTED    Author submitted revision. Waiting for editor to
    #                       take action (accept/reject/open review round)
    REVISION_SUBMITTED = "rev_submited"
    # ACCEPTED              Editor accepted the submission as it is.
    #                       Begin Copyediting process.
    ACCEPTED = "accepted"
    # REJECTED              Editor rejected the submission. The submission is closed.
    REJECTED = "rejected"


SUBMISSION_STATE_CHOICES = [
    (SubmissionState.OPENED.value, _("Draft")),
    (SubmissionState.SUBMITTED.value, _("Submitted")),
    (SubmissionState.ON_REVIEW.value, _("Under review")),
    (SubmissionState.REVISION_REQUESTED.value, _("Revision requested")),
    (SubmissionState.REVISION_SUBMITTED.value, _("Revision submitted")),
    (SubmissionState.ACCEPTED.value, _("Accepted")),
    (SubmissionState.REJECTED.value, _("Rejected")),
]

SUBMISSION_STATE_EDITOR_CHOICES = [
    (SubmissionState.REVISION_REQUESTED.value, _("Request revisions")),
    (SubmissionState.ACCEPTED.value, _("Accept submission")),
    (SubmissionState.REJECTED.value, _("Reject submission")),
]


# Typing with the generic here is very important to get correct type hints
# when using any SubmissionManager methods.
class SubmissionQuerySet(models.QuerySet[_T]):
    def annotate_last_activity(self) -> Self:
        """
        Annotate the `date_last_activity` to the queryset = date of the last
        significant log entry.
        """
        return self.annotate(
            log_significant=FilteredRelation(
                "log_messages", condition=Q(log_messages__significant=True)
            )
        ).annotate(date_last_activity=Max("log_significant__date_created"))

    def prefetch_data(self) -> Self:
        """
        Shortcut function to prefetch all related MtoM data.
        """
        return self.prefetch_related(
            "versions",
            "versions__main_file",
            "versions__additional_files",
            "versions__reviews",
            "versions__reviews__additional_files",
            "authors",
            "editors",
            "editors__user",
        )

    def select_data(self) -> Self:
        """
        Shortcut function to select all related FK.
        """
        return self.select_related("created_by", "last_modified_by", "journal_section")


class SubmissionManager(models.Manager[_T]):
    def get_queryset(self) -> SubmissionQuerySet[_T]:
        return (
            SubmissionQuerySet(self.model, using=self._db).annotate_last_activity().select_data()
        )

    def get_submissions(self, prefetch=True) -> SubmissionQuerySet[_T]:
        """
        Return a SubmissionQuerySet with prefetched related data (default).
        """
        if not prefetch:
            return self.get_queryset()
        return self.get_queryset().prefetch_data()


class Submission(BaseChangeTrackingModel):
    # Change the default `created_by` with a protected on_delete behavior.
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_("Created by"),
        on_delete=models.PROTECT,
        blank=True,
        null=False,
        help_text=_("Automatically filled on save."),
        editable=False,
        related_name="+",
    )
    name = models.TextField(verbose_name=_("Title"))
    abstract = models.TextField(verbose_name=_("Abstract"))
    journal_section = models.ForeignKey(
        "JournalSection",
        verbose_name=_("Section (Optional)"),
        on_delete=models.SET_NULL,
        related_name="submissions",
        null=True,
        blank=True,
    )
    state = models.CharField(
        verbose_name=_("state"),
        max_length=64,
        choices=SUBMISSION_STATE_CHOICES,
        default=SubmissionState.OPENED.value,
    )
    date_first_version = models.DateTimeField(
        verbose_name=_("Date of the first submitted version"), null=True, editable=False
    )
    author_agreement = models.BooleanField(
        verbose_name=_("Agreement"),
        help_text=_("I hereby declare that I have read blablabla and I consent to the terms"),
    )

    objects: SubmissionManager[Self] = SubmissionManager()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["created_by", "name"], name="unique_submission_name_per_user"
            )
        ]

    def __str__(self) -> str:
        return f"{self.created_by} - {self.name}"

    @cached_property
    def all_versions(self) -> list[SubmissionVersion]:
        """
        Return all the versions, ordered by descending `number`.
        We do the sort manually to prevent an potential additional query (
        the versions should be already prefetched).
        """
        versions = self.versions.all()  # type:ignore
        return sorted(versions, key=lambda version: version.number, reverse=True)

    @cached_property
    def current_version(self) -> SubmissionVersion | None:
        """
        The current (latest) `SubmissionVersion` of the Submission.
        """
        all_versions = self.all_versions
        if all_versions:
            return all_versions[0]
        return None

    @property
    def date_submission(self) -> datetime | None:
        """
        Submission date of the submission.
        It is the date of the first version completion or the submisison's creation date
        if no versions are submitted yet.
        """
        return self.date_first_version or self.date_created

    @property
    def state_order(self) -> int:
        """
        Returns the integer mapped to the submission state for ordering purpose.
        """
        return [s[0] for s in SUBMISSION_STATE_CHOICES].index(self.state)

    @cached_property
    def all_assigned_editors(self) -> list[User]:
        return sorted(
            (e.user for e in self.editors.all()), key=lambda u: u.first_name  # type:ignore
        )

    @cached_property
    def all_authors(self) -> list[SubmissionAuthor]:
        return sorted(self.authors.all(), key=lambda a: a.first_name)  # type:ignore

    @property
    def is_submittable(self) -> bool:
        """
        Whether the submission is submittable.
        It checks that the required data is correct.
        """
        return (
            self.author_agreement is True
            and self.state
            in [SubmissionState.OPENED.value, SubmissionState.REVISION_REQUESTED.value]
            and self.current_version is not None
            and not self.current_version.submitted
            and hasattr(self.current_version, "main_file")
            and len(self.all_authors) > 0
        )

    @property
    def is_draft(self) -> bool:
        return self.state == SubmissionState.OPENED.value

    def submit(self, user) -> None:
        """
        Submit the submission's current version:
            -   Set the submission's current version to `submitted=True`
            -   Change the submission state to "submitted" or "revisions_submitted"
                according to the current state.
            -   Add an entry to the submission log

        Raise an SubmissionStateError is the submission is not submittable.

        Params:
            -   `request`   The enclosing HTTP request. It's used to derive the
                            current user and potential impersonate data.
        """
        if not self.is_submittable:
            raise SubmissionStateError(_("Trying to submit an non-submittable submission."))
        self.state = (
            SubmissionState.SUBMITTED.value
            if self.is_draft
            else SubmissionState.REVISION_SUBMITTED.value
        )
        self.save()

        version: SubmissionVersion = self.current_version  # type:ignore
        version.submitted = True
        version.date_submitted = timezone.now()
        version._user = user  # type:ignore
        version.save()

        SubmissionLog.add_message(
            self,
            content=_("Submission of version") + f" #{version.number}",
            content_en=f"Submission of version #{version.number}",
            user=user,
            # impersonate_data=impersonate_data,
            significant=True,
        )

    @property
    def is_reviewable(self) -> bool:
        """
        Returns whether the submission can be sent to review.
        """
        return (
            self.state
            in [SubmissionState.SUBMITTED.value, SubmissionState.REVISION_SUBMITTED.value]
            and self.current_version is not None
            and self.current_version.submitted
            and self.current_version.review_open is False
            and hasattr(self.current_version, "main_file")
        )

    def start_review_process(self, user) -> None:
        """
        Start the review process for the current version of the submission:
            - Change the submission state to ON_REVIEW
            - Open the review on the current version
            - Add an entry to the submission log.

        Raise an exception is the submission is not reviewable.

        Params:
            -   `request`   The enclosing HTTP request. It's used to derive the
                            current user and potential impersonate data.
        """
        if not self.is_reviewable:
            raise SubmissionStateError(
                _("Trying to start the review process of an non-reviewable submission.")
            )
        version: SubmissionVersion = self.current_version  # type:ignore
        version.review_open = True
        version.save()

        self.state = SubmissionState.ON_REVIEW.value
        self.save()

        SubmissionLog.add_message(
            self,
            content=f"Submission version #{version.number} sent to review.",
            content_en=f"Submission version #{version.number} sent to review.",
            user=user,
            # impersonate_data=impersonate_data,
            significant=True,
        )

    def apply_editorial_decision(self, decision: EditorialDecision, user) -> None:
        """
        Apply an editorial decision:
            - Changes the submission state to the selected state.
            - Close the review on the current version.
            - Add an entry to the submission log

        Raise an exception if the submission's status does not allow editorial decision.

        Params:
            -   `request`   The enclosing HTTP request. It's used to derive the
                            current user and potential impersonate data.
        """
        if self.is_draft:
            raise SubmissionStateError(
                _("Trying to apply an editorial decision on a draft submission.")
            )
        # Update the submission state with the selected one
        self.state = decision.value
        self.save()

        # Close the review on the current version if any.
        version = self.current_version
        if version:
            version.review_open = False
            version.save()

        # Add message and log entry
        decision_str = decision.get_value_display()  # type:ignore
        SubmissionLog.add_message(
            self,
            content=_("Editorial decision") + f": {decision_str}",
            content_en=f"Editorial decision: {decision_str}",
            user=user,
            significant=True,
        )


class SubmissionVersion(BaseSubmittableModel, BaseModelWithFiles):
    """
    Version of a submission. Only contains files (main + additional files).
    """

    file_fields_required = ["main_file"]
    file_fields_deletable = ["additional_files"]

    submission = models.ForeignKey(
        Submission,
        on_delete=models.CASCADE,
        blank=True,
        null=False,
        editable=False,
        related_name="versions",
    )
    number = models.IntegerField(
        help_text=_("Automatically filled on save"), blank=True, null=False, editable=False
    )

    # Boolean used to track whether the review process is still open for the submission
    # version. A new version is not opened for review by default. Changing its value
    # requires an editorial action.
    review_open = models.BooleanField(
        verbose_name=_("Version opened for review"), default=False, editable=False
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["submission", "number"], name="unique_submission_version_number"
            ),
            models.UniqueConstraint(
                fields=["submission"],
                condition=Q(submitted=False),
                name="unique_draft_submission_version",
            ),
            models.UniqueConstraint(
                fields=["submission"],
                condition=Q(review_open=True),
                name="unique_review_open_submission_version",
            ),
        ]

    def save(self, *args, **kwargs) -> None:
        """
        Fill the version's number for a new instance.
        """
        if self._state.adding:
            current_version = self.submission.current_version
            if current_version:
                self.number = current_version.number + 1
            else:
                self.number = 1

        return super().save(*args, **kwargs)


class SubmissionMainFile(BaseFileWrapperModel):
    file_extensions = [".pdf"]

    attached_to: models.OneToOneField[SubmissionVersion] = models.OneToOneField(
        SubmissionVersion, primary_key=True, on_delete=models.CASCADE, related_name="main_file"
    )

    @staticmethod
    def get_upload_path(instance: SubmissionMainFile, filename: str) -> str:
        return os.path.join(
            "submissions",
            str(instance.attached_to.submission.pk),
            "versions",
            str(instance.attached_to.number),
            filename,
        )

    @classmethod
    def reverse_file_path(cls, file_path: str) -> SubmissionMainFile | None:
        """
        Check first with a regex is the file path might match. This is not necessary
        but it saves us a DB query in case in doesn't match the path structure.

        WARNING: This is not resilient to path change. A file could be moved to another
        location, with the path updated in DB and still be an instance of this model.
        If this becomes an issue, remove the regex check and just make the DB query.
        """
        regex = "/".join(
            [
                "submissions",
                r"(?P<submission_pk>[0-9]+)",
                "versions",
                r"(?P<version_number>[0-9]+)",
                r"(?P<file_name>[^\/]+)$",
            ]
        )
        match = re.match(re.compile(regex), file_path)
        if not match:
            return None

        return cls._default_manager.filter(file=file_path).first()

    def check_access_right(self, role_handler: RoleHandler, right_code: str) -> bool:
        """
        This model is only editable by user role with edit rights on the submission.
        """
        if right_code == "read":
            return role_handler.check_global_rights("can_access_version", self.attached_to)

        return False


class SubmissionAdditionalFile(BaseFileWrapperModel):
    file_extensions = [".pdf", ".docx", ".odt", ".py", ".jpg", ".png", ".ipynb", ".sql", ".tex"]
    attached_to: models.ForeignKey[SubmissionVersion] = models.ForeignKey(
        SubmissionVersion, on_delete=models.CASCADE, related_name="additional_files"
    )

    @staticmethod
    def get_upload_path(instance: SubmissionAdditionalFile, filename: str) -> str:
        return os.path.join(
            "submissions",
            str(instance.attached_to.submission.pk),
            "versions",
            str(instance.attached_to.number),
            "additional",
            filename,
        )

    @classmethod
    def reverse_file_path(cls, file_path: str) -> SubmissionAdditionalFile | None:
        """
        Check first with a regex is the file path might match. This is not necessary
        but it saves us a DB query in case in doesn't match the path structure.
        """
        regex = "/".join(
            [
                "submissions",
                r"(?P<submission_pk>[0-9]+)",
                "versions",
                r"(?P<version_number>[0-9]+)",
                "additional",
                r"(?P<file_name>[^\/]+)$",
            ]
        )
        match = re.match(re.compile(regex), file_path)
        if not match:
            return None

        return cls._default_manager.filter(file=file_path).first()

    def check_access_right(self, role_handler: RoleHandler, right_code: str) -> bool:
        """
        This model is only editable by user role with edit rights on the submission.
        """
        if right_code == "read":
            return role_handler.check_global_rights("can_access_version", self.attached_to)

        elif right_code in ["delete"]:
            return role_handler.check_rights("can_edit_version", self.attached_to)

        return False


class SubmissionLog(ModelLog):
    attached_to = models.ForeignKey(
        Submission, on_delete=models.CASCADE, related_name="log_messages"
    )


class SubmissionAuthor(BaseChangeTrackingModel):
    """
    Model for a submission's author.
    1 submission author is linked to 1 submission only.
    """

    submission = models.ForeignKey(
        Submission,
        on_delete=models.CASCADE,
        blank=True,
        null=False,
        editable=False,
        related_name="authors",
    )
    first_name = models.CharField(
        verbose_name=_("First name"), max_length=150, blank=False, null=False
    )
    last_name = models.CharField(
        verbose_name=_("Last name"), max_length=150, blank=False, null=False
    )
    email = models.EmailField(_("E-mail address"))
    corresponding = models.BooleanField(
        verbose_name=_("Corresponding contact"),
        blank=False,
        null=False,
        default=False,
        help_text=_(
            "If checked, e-mails will be sent to advise of any progress in the submission process."
        ),
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["submission", "email"], name="unique_author_email_per_submission"
            )
        ]

    def __str__(self) -> str:
        return f"{ self.first_name } { self.last_name } ({ self.email })"

    def full_name(self) -> str:
        return f"{ self.first_name } { self.last_name }"
