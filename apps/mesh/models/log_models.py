from enum import Enum
from enum import unique
from typing import Any
from typing import Self

from django.conf import settings
from django.db import models

# from django.http import HttpRequest
from django.utils.translation import gettext_lazy as _

from .base_models import BaseChangeTrackingModel


@unique
class LogType(Enum):
    # Message automatically created following user actions.
    GENERATED = "generated"
    # Message explicitely entered by an user.
    MANUAL = "manual"


LOG_TYPES = [lt.value for lt in LogType]
LOG_TYPE_CHOICES = [
    (LogType.GENERATED.value, _("Auto generated")),
    (LogType.MANUAL.value, _("Manual entry")),
]


class ModelLog(BaseChangeTrackingModel):
    """
    Base class to create a simple log table attached to an entity.

    TODO: Always store the message text ? For auto generated messages (ex: new version),
    store a unique message code which could enable message lookup somewhere else ?
    """

    attached_to: None | models.ForeignKey = None
    # Log content (French)
    content = models.TextField(blank=True, null=True)
    # Translated content (English)
    content_en = models.TextField(blank=True, null=True)
    # Log message code (little helper that can fasten message browsing) - Optional
    code = models.CharField(max_length=128, null=True, blank=True)
    # Log type - Automatically generated or explicite user entry
    type = models.CharField(
        verbose_name=_("Message type"),
        max_length=32,
        choices=LOG_TYPE_CHOICES,
        default=LogType.GENERATED.value,
    )
    impersonated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        editable=False,
        related_name="+",
    )
    significant = models.BooleanField(verbose_name=_("Significant log"), default=False)

    class Meta:
        abstract = True

    @classmethod
    def add_message(
        cls,
        attached_to: models.Model,
        content: str | None = None,
        content_en: str | None = None,
        user=None,
        # impersonate_data=None,
        # request: HttpRequest | None = None,
        type: str = "",
        code: str = "",
        significant: bool = False,
    ) -> Self:
        """
        Adds an entry to the model log.
        Automatically checks if the request is performed with active impersonate mode.
        """
        kwargs: dict[str, Any] = {"attached_to": attached_to, "significant": significant}
        if content:
            kwargs["content"] = content
        if content_en:
            kwargs["content_en"] = content_en
        if type and type in LOG_TYPES:
            kwargs["type"] = type
        if code:
            kwargs["code"] = code

        new_message = cls(**kwargs)

        if user.impersonate_data:
            new_message.impersonated_by_id = user.impersonate_data.source.pk  # type: ignore

        if user.is_authenticated:
            new_message._user = user  # type: ignore
            # if request.user.is_authenticated:
            # new_message._user = request.user  # type: ignore

        new_message.save()

        return new_message
