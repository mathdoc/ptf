from __future__ import annotations

from itertools import chain
from typing import TypeVar

from django.db import models
from django.db.models import QuerySet
from django.utils.translation import gettext_lazy as _

from .base_models import BaseChangeTrackingModel
from .submission_models import Submission

_T = TypeVar("_T", bound=models.Model)


class JournalSectionManager(models.Manager):
    """
    Custom manager for JournalSection used to cache data at the manager level.
    """

    # Variables used to cache data at the class level. The cache must be cleared
    # on every change on a JournalSection (creation, deletion, update).
    _all_journal_sections: QuerySet[JournalSection] | None = None
    _all_journal_sections_children: dict[int | None, list[JournalSection]] | None = None
    _all_journal_sections_parents: dict[int, JournalSection | None] | None = None

    def get_queryset(self) -> QuerySet:
        return (
            super()
            .get_queryset()
            .select_related(
                "created_by",
                "last_modified_by",
                "parent",
            )
        )

    def all_journal_sections(self) -> QuerySet[JournalSection]:
        """
        Returns all registered `JournalSection`.
        """
        if self._all_journal_sections is None:
            self._all_journal_sections = self.get_queryset().all()

        return self._all_journal_sections

    def all_journal_sections_parents(self) -> dict[int, JournalSection | None]:
        """
        Returns the mapping: `{journal_section.pk: parent}` for all registered
        `JournalSection`.
        """
        if self._all_journal_sections_parents is None:
            self._all_journal_sections_parents = {
                c.pk: c.parent for c in self.all_journal_sections()
            }

        return self._all_journal_sections_parents

    def all_journal_sections_children(self) -> dict[int | None, list[JournalSection]]:
        """
        Return the mapping: `{journal_section.pk : list[children]}` for all registered
        JournalSection.

        There's an additional entry to the mapping, `None`, listing all the journal_sections
        without a parent (top-level journal_sections).
        """
        if self._all_journal_sections_children is None:
            journal_sections = self.all_journal_sections()
            processed_journal_sections = {c.pk: [] for c in journal_sections}
            processed_journal_sections[None] = []

            for journal_section in journal_sections:
                parent: JournalSection | None = journal_section.parent
                key: int | None = None if parent is None else parent.pk
                processed_journal_sections[key].append(journal_section)

            self._all_journal_sections_children = processed_journal_sections

        return self._all_journal_sections_children

    def get_children_recursive(
        self, journal_section: JournalSection | None
    ) -> list[JournalSection]:
        """
        Return the flattened list of all children nodes of the given journal_section.
        """
        children_dict = self.all_journal_sections_children()
        key = None if journal_section is None else journal_section.pk
        children = children_dict[key]
        return list(
            # Flatten the input sequences into a single sequence.
            chain.from_iterable(
                ([c, *self.get_children_recursive(c)] for c in children),
            )
        )

    def get_parents_recursive(
        self, journal_section: JournalSection | None
    ) -> list[JournalSection]:
        """
        Return the flattened list of all parent nodes of the given journal_section.
        """
        if journal_section is None:
            return []
        parents_dict = self.all_journal_sections_parents()
        parent = parents_dict.get(journal_section.pk)
        if parent is None:
            return []
        return [parent, *self.get_parents_recursive(parent)]

    def clean_cache(self):
        self._all_journal_sections = None
        self._all_journal_sections_children = None
        self._all_journal_sections_parents = None


class JournalSection(BaseChangeTrackingModel):
    """
    Represents a journal section. Sections can be nested infinitely.

    Sections are mainly used to give editor rights over a whole section.
    """

    name = models.CharField(verbose_name=_("Name"), max_length=128, unique=True)
    parent = models.ForeignKey(
        "self", on_delete=models.SET_NULL, null=True, blank=True, related_name="children"
    )

    objects: JournalSectionManager = JournalSectionManager()

    def __str__(self) -> str:
        return self.name

    def save(self, *args, **kwargs) -> None:
        """
        Need to check that the selected parent journal_section is valid.
        """
        if self.parent and (self.parent == self or self.parent in self.all_children()):
            raise ValueError("The selected parent is invalid (self or child).")
        super().save(*args, **kwargs)
        self.__class__.objects.clean_cache()

    def delete(self, *args, **kwargs):
        """
        Delete the section and additionally move all submissions and sub-sections
        to the parent section.
        """
        parent = self.parent
        children = list(
            self.children.all().values_list("pk", flat=True)  # type:ignore
        )
        submissions_to_update = list(
            Submission.objects.filter(journal_section=self).values_list("pk", flat=True)
        )

        res = super().delete(*args, **kwargs)

        if children:
            JournalSection.objects.filter(pk__in=children).update(parent=parent)
        if submissions_to_update:
            Submission.objects.filter(pk__in=submissions_to_update).update(journal_section=parent)

        self.__class__.objects.clean_cache()
        return res

    def top_level_journal_section(self) -> JournalSection:
        """
        Return the top level parent journal_section (journal).
        """
        # The journal_section is a journal
        if self.parent is None:
            return self

        # Browse up the journal_section arborescence until finding the top level journal_section
        parent_journal_sections = self.__class__.objects.all_journal_sections_parents()

        top_level_journal_section: JournalSection = self.parent
        while top_level_journal_section.parent is not None:
            top_level_journal_section = parent_journal_sections[
                top_level_journal_section.parent.pk
            ]  # type:ignore

        return top_level_journal_section

    def all_children(self) -> list[JournalSection]:
        """
        Get all the `JournalSection` children.
        """
        if self._state.adding:
            return []

        return self.__class__.objects.get_children_recursive(self)
