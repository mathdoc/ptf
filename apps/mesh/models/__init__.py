# ruff: noqa: F401
# Django expects all models to be accessible in this file.
# All existing models should be explicitely imported here.
# It should still work even if this file is empty, according to the Django version.

from .editorial_models import EditorialDecision
from .editorial_models import EditorialDecisionFile
from .editorial_models import EditorSectionRight
from .editorial_models import EditorSubmissionRight
from .journal_models import JournalSection
from .review_models import Review
from .review_models import ReviewAdditionalFile
from .submission_models import Submission
from .submission_models import SubmissionAdditionalFile
from .submission_models import SubmissionAuthor
from .submission_models import SubmissionMainFile
from .submission_models import SubmissionVersion
from .user_models import User
