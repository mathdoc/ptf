from __future__ import annotations

from typing import TYPE_CHECKING

from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

if TYPE_CHECKING:
    from .user_models import User


class BaseChangeTrackingModel(models.Model):
    """
    Base model used to perform simple "change tracking" on models.
    It adds 4 fields to the model:
        - `date_created`        The creation date of the object.
        - `created_by`          The user creating the object.
        - `date_last_modified`  The date of the last modification performed.
        - `last_modified_by`    The user who performed the last modification.

    The data is automatically updated when calling the model `save()` method.
    The code looks in the `_user` attribute to update the tracking fields. The views
    must inject it in the instance before saving it.
    """

    # Instance of the user creating / modifying the model used to automatically fill
    # the `created_by` and `last_modified_by` fields.
    _user: User | None = None
    # Whether to prevent the update of the last modification fields.
    do_not_update = False
    date_created = models.DateTimeField(
        verbose_name=_("Creation date"),
        default=timezone.now,
        blank=True,
        null=True,
        editable=False,
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_("Created by"),
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        help_text=_("Automatically filled on save."),
        editable=False,
        related_name="+",
    )
    date_last_modified = models.DateTimeField(
        verbose_name=_("Last modification date"),
        default=timezone.now,
        blank=True,
        null=True,
        editable=False,
    )
    last_modified_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_("Last modified by"),
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        help_text=_("Automatically filled on save."),
        editable=False,
        related_name="+",
    )

    class Meta:
        abstract = True

    def save(self, *args, **kwargs) -> None:
        """
        Update the tracking data before save:
            - Update the creation data if the object is new
            - Update the last modification data if the `_request` attribute is set
        """
        UserModel = get_user_model()
        if self._state.adding:
            self.date_created = timezone.now()
            if self._user and isinstance(self._user, UserModel):
                self.created_by_id = self._user.pk

        if not self.do_not_update:
            self.date_last_modified = timezone.now()
            if self._user and isinstance(self._user, UserModel):
                self.last_modified_by_id = self._user.pk

        super().save(*args, **kwargs)


class BaseSubmittableModel(models.Model):
    """
    Base model "mixin" for model to be effectively submitted
    (different action from just saving the model).
    """

    submitted = models.BooleanField(
        verbose_name=_("Submitted"), default=False, editable=False, blank=True
    )
    date_submitted = models.DateTimeField(
        verbose_name=_("Submitted on"), editable=False, blank=True, null=True
    )

    class Meta:
        abstract = True

    @property
    def is_submittable(self) -> bool:
        return True
