class RoleException(Exception):
    pass


class SubmissionStateError(Exception):
    pass


class ReviewStateError(Exception):
    pass
