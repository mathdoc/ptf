import os
from datetime import timedelta
from urllib.parse import urlparse

import paramiko
import xmltodict
from requests_cache import CachedSession
from requests_cache import FileCache

from django.conf import settings
from django.http import Http404
from django.template.loader import render_to_string
from django.views.generic import TemplateView

from ptf import model_helpers
from ptf import models
from ptf.display import resolver

session = CachedSession(
    backend=FileCache(
        getattr(settings, "REQUESTS_CACHE_LOCATION", None) or "/tmp/ptf_requests_cache",
        decode_content=False,
    ),
    headers={
        "User-Agent": getattr(settings, "REQUESTS_USER_AGENT", None) or "Mathdoc/1.0.0",
        "From": getattr(settings, "REQUESTS_EMAIL", None) or "accueil@listes.mathdoc.fr",
    },
    expire_after=timedelta(days=30),
)


def get_pubmed_article_type(self):
    article_type = "Journal Article"

    if self.atype != "normal":
        if self.atype == "biographical-note":
            article_type = "Biography"
        elif self.atype == "congress":
            article_type = "Congress"
        elif self.atype == "corrigendum":
            article_type = "Published Erratum"
        elif self.atype == "editorial":
            article_type = "Editorial"
        elif self.atype == "erratum":
            article_type = "Published Erratum"
        elif self.atype == "expression-of-concern":
            article_type = "Expression of Concern"
        elif self.atype == "foreword":
            article_type = "Introductory Journal Article"
        elif self.atype == "guest-editors":
            article_type = "Biography"
        elif self.atype == "historical-commentary":
            article_type = "Classical Article"
        elif self.atype == "history-of-sciences":
            article_type = "Historical Article"
        elif self.atype == "letter":
            article_type = "Comment"
        elif self.atype == "news":
            article_type = "News"
        elif self.atype == "opinion":
            article_type = "News"
        elif self.atype == "preliminary-communication":
            article_type = "Journal Article"
        elif self.atype == "research-article":
            article_type = "Journal Article"
        elif self.atype == "retraction":
            article_type = "Retraction of Publication"
        elif self.atype == "review":
            article_type = "Review"
        elif self.atype == "book-review":
            article_type = "Comment"
        elif self.atype == "clarification":
            article_type = "Review"
    else:
        for subj in self.subj_set.filter(type="type", lang="en"):
            value = subj.value
            if value == "Editorial":
                article_type = "Editorial"
            elif value == "News and Views":
                article_type = "News"
            elif value == "Life of the Academy":
                article_type = "Review"
            elif value == "Prizes of the Academy":
                article_type = "Review"
            elif value == "News and events":
                article_type = "News"
            elif value in ["Biographical Notes", "Biographical Note"]:
                article_type = "Biography"
            elif value in ["Corrigendum", "Erratum"]:
                article_type = "Published Erratum"
            elif value == "Review Article":
                article_type = "Review"
            elif value in ["Opinion / Perspective", "Opinion, Perspective"]:
                article_type = "News"
            elif value == "Intervention in a conference":
                article_type = "Congress"

    return article_type


def get_pubmed_journal_title(self):
    if self.issn == "1631-0691":
        self.title_tex = "C R Biol."

    return self.title_tex


models.Article.get_pubmed_article_type = get_pubmed_article_type
models.Collection.get_pubmed_journal_title = get_pubmed_journal_title


class PubmedIssueView(TemplateView):
    template_name = "pubmed/issue.xml"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if "pid" in kwargs:
            pid = kwargs["pid"]
        else:
            pid = self.kwargs.get("pid")
        issue = model_helpers.get_container(pid)
        if issue is None:
            raise Http404

        context["issue"] = issue
        context["to_appear"] = issue.with_online_first
        articles = issue.article_set.exclude(do_not_publish=True)
        context["articles"] = articles

        return context


def upload_file_to_pubmed(localpath):
    if not hasattr(settings, "PUBMED_SFTP"):
        print("First, please set settings.PUBMED_SFTP and try again.")
        return

    sftp_url = settings.PUBMED_SFTP
    parsed_url = urlparse(sftp_url)
    hostname = str(parsed_url.hostname)
    username = str(parsed_url.username)
    password = str(parsed_url.password)

    with open(os.path.join(settings.LOG_DIR, "cmds_pubmed.log"), "w", encoding="utf-8") as file_:
        file_.write(f"Inside upload_file_to_pubmed with {hostname}\n")

    try:
        transport = paramiko.Transport((hostname, 22))

        with open(
            os.path.join(settings.LOG_DIR, "cmds_pubmed.log"), "a", encoding="utf-8"
        ) as file_:
            file_.write("transport created\n")

        transport.connect(None, username, password)
        with open(
            os.path.join(settings.LOG_DIR, "cmds_pubmed.log"), "a", encoding="utf-8"
        ) as file_:
            file_.write("connection created\n")

        sftp = paramiko.SFTPClient.from_transport(transport)

        if sftp:
            with open(
                os.path.join(settings.LOG_DIR, "cmds_pubmed.log"), "a", encoding="utf-8"
            ) as file_:
                file_.write("sftp created\n")
            remote_path = os.path.basename(localpath)
            sftp.put(localpath, remote_path)
        else:
            with open(
                os.path.join(settings.LOG_DIR, "cmds_pubmed.log"), "a", encoding="utf-8"
            ) as file_:
                file_.write("sftp is None\n")
    except Exception as e:
        with open(
            os.path.join(settings.LOG_DIR, "cmds_pubmed.log"), "a", encoding="utf-8"
        ) as file_:
            file_.write("Error\n")
            file_.write(str(e))

    # Close
    if sftp:
        sftp.close()
    if transport:
        transport.close()


def generate_pubmed_xml(article, update_article=False):
    context = {
        "articles": [article],
        "issue": article.my_container,
        "to_appear": article.my_container.with_online_first,
        "update_article": update_article,
    }

    xml_body = render_to_string("pubmed/issue.xml", context)

    folder = os.path.join(settings.LOG_DIR, article.get_top_collection().pid, "pubmed/")
    resolver.create_folder(folder)

    prefix = f"{article.get_top_collection().pid}v{article.my_container.volume}"
    i = len([filename for filename in os.listdir(folder) if filename.startswith(prefix)]) + 1

    filename = os.path.join(folder, prefix + "-" + str(i) + ".xml")

    with open(filename, "w", encoding="utf-8") as f:
        f.write(xml_body)

    return filename


def recordPubmed(article, force_update=False, updated_articles=[]):
    # force_update is used by the ptf-tools Record PubMed dialog to force the update.
    # But we need to check the status of the article, because in some cases, the update won't work.

    # If the article has no pmid (assumption: the pubmed matching has been done)
    #    we register the article for the first time => update_pubmed = False
    # If the article has a pmid, we would like to update the metadate in Pubmed.
    # 2 cases:
    #   - the article has a date_online_first, no date_published and the issue is finalized,
    #     we can send a new XML with the <Replaces> tag
    #   - else, PubMed does not allow to update the metadata by API !!!
    #     We need to manually go to the Pubmed web site and update the metadata by hand
    update_pubmed = False
    pm_extid = model_helpers.get_extid(article, "pmid")
    # We can't directly use article.date_published because this date may have just been set
    has_new_date = len([art for art in updated_articles if art.doi == article.doi]) > 0
    if (
        has_new_date
        and article.date_online_first is not None
        and not article.my_container.with_online_first
    ):
        update_pubmed = True

    if pm_extid is None or force_update or (update_pubmed and has_new_date):
        filename = generate_pubmed_xml(article, force_update or update_pubmed)
        upload_file_to_pubmed(filename)


def pubmed_lookup(article):
    api_key = getattr(settings, "PUBMED_API_KEY", None)

    if api_key is None:
        return ""

    url = f'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&term="{article.doi}"[aid]'
    use_json = False

    year = int(article.my_container.year)
    number = 0
    if year > 2021:
        pos = article.doi.rfind(".")
        if pos > 0:
            number = int(article.doi[pos + 1 :])

    if number > 56:
        url = f"https://www.ncbi.nlm.nih.gov/pubmed/management/api/citations?doi={article.doi}"
        use_json = True

    headers = {"api-key": api_key}
    response = session.get(url, headers=headers, timeout=2)

    result = ""
    if response.status_code == 200:
        if use_json:
            response_json = response.json()
            if len(response_json) > 0:
                result = response_json[0]["pmid"]
        else:
            data = xmltodict.parse(response.text)
            idlist = data.get("eSearchResult", {}).get("IdList", {})
            if idlist is not None:
                result = idlist.get("Id", "")

    return result
