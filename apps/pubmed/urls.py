from django.urls import path

from pubmed.views import PubmedIssueView

urlpatterns = [
    path('pubmed/issue/<path:pid>/', PubmedIssueView.as_view(), name='pubmed-issue'),
]
