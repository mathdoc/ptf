import json

from django.http import Http404
from django.http import HttpResponse
from django.views.generic import View

from matching import crossref
from ptf import model_data_converter


class FetchRefView(View):
    def get(self, request, *args, **kwargs):
        doi = kwargs.get("doi", None)
        pos = kwargs.get("pos", "0")

        if doi is None:
            raise Http404

        ref_data = crossref.crossref_request_reference(doi)
        model_data_converter.update_ref_data_for_jats(ref_data, int(pos))
        model_data_converter.convert_refdata_for_editor(ref_data)

        def obj_to_dict(obj):
            return obj.__dict__

        dump = json.dumps(ref_data, default=obj_to_dict)

        return HttpResponse(dump, content_type="application/json")
