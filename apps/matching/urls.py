from django.urls import path

from .views import FetchRefView

urlpatterns = [
    path('fetch-ref/<int:pos>/<path:doi>/', FetchRefView.as_view(), name='api-fetch-ref'),
]
