from django.db import models

from ptf.models import BibItem

"""
Used during matching a non structured biblio
In this scenario, we get info from crossref or zbl in order
to try a match on mathschinet or numdam
"""


class TempMatchParams(models.Model):
    bibitem = models.ForeignKey(BibItem, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    journal = models.CharField(max_length=255, blank=True, null=True)
    authors = models.TextField()
    volume = models.CharField(max_length=15, blank=True, null=True)
    issue = models.CharField(max_length=15, blank=True, null=True)
    year = models.IntegerField(null=True)
    first_page = models.IntegerField(null=True)
    last_page = models.IntegerField(null=True)
    issn = models.CharField(max_length=9, blank=True, null=True)

    def __str__(self):
        return "".join(["Temp Params for : ", self.title])
