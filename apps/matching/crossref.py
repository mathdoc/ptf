import warnings

import requests
from bs4 import BeautifulSoup
from bs4.builder import XMLParsedAsHTMLWarning

from django.conf import settings

from matching import fetch_metadata
from ptf import model_data
from ptf import model_data_converter
from ptf.cmds.xml import xml_utils

# Pubmed matching
# See: https://www.ncbi.nlm.nih.gov/pmc/tools/id-converter-api/
# then https://www.ncbi.nlm.nih.gov/books/NBK25500/#chapter1.Downloading_Full_Records


def crossref_request(doi):
    url = settings.CROSSREF_CHECKDOI_URL.format(doi)
    response = requests.get(url)
    response.raise_for_status()
    data = response.json()["message"]
    page_range = data.get("page", None)
    fpage, lpage = xml_utils.handle_pages(page_range)
    authors = data.get("author", [])
    # we drop authors who doesn't have family name
    # it appears that there are used to store institution name sometimes
    authors = [author for author in authors if "family" in author]
    result = {
        "title": data["title"][0] if data["title"] else "",
        "authors": "; ".join(
            ["{},{}".format(author.get("given", ""), author["family"]) for author in authors]
        ),
        "journal": data["container-title"][0] if data.get("container-title") else None,
        "issue": data.get("issue", None),
        "volume": data.get("volume", None),
        "year": data["published"]["date-parts"][0][0] if data.get("published") else None,
        "fpage": fpage,
        "lpage": lpage,
        "page": page_range,
    }
    return result


def parse_crossref_authors(crossref_data):
    crossref_authors = crossref_data.get("author", [])
    authors = []
    for author in crossref_authors:
        # we drop authors who don't have family name
        # it appears that they are used to store institution name sometimes
        orcid = author["ORCID"] if "ORCID" in author else ""
        orcid = orcid.replace("https://orcid.org/", "").replace("http://orcid.org/", "")

        if "family" in author:
            contrib = model_data.create_contributor()
            contrib["first_name"] = author["given"] if "given" in author else ""
            contrib["last_name"] = author["family"] if "family" in author else ""
            contrib["orcid"] = orcid
            contrib["addresses"] = (
                [aff["name"] for aff in author["affiliation"] if "name" in aff]
                if "affiliation" in author
                else []
            )
            contrib["role"] = "author"
            contrib["contrib_xml"] = xml_utils.get_contrib_xml(contrib)
            authors.append(contrib)
    return authors


def parse_crossref_reference(ptf_data, crossref_data):
    t = crossref_data.get("type", "")

    # PTF types:
    # article
    # book
    # incollection inbook
    # thesis phdthesis masterthesis
    # misc unpublished
    # manual techreport coursenotes proceedings

    # Crossref types: (See https://api.crossref.org/v1/types)
    # journal-article proceedings-article
    # book monograph reference-book edited-book
    # book-section book-track book-part book-chapter
    # dissertation
    # reference-entry posted-content
    # standard report peer-review proceedings
    # journal-volume book-set journal-issue
    # dataset component
    # journal book-series report-series standard-series proceedings-series

    if "article" in t:
        ptf_data.type = "article"
    elif t in ["book", "monograph", "reference-book", "edited-book"]:
        ptf_data.type = "book"
    elif t in ["book-section", "book-track", "book-part", "book-chapter"]:
        ptf_data.type = "incollection"
    elif t == "dissertation":
        ptf_data.type = "phdthesis"
    else:
        ptf_data.type = "misc"

    title = crossref_data.get("title")
    title = title[0] if isinstance(title, list) and title else ""
    container_title = crossref_data.get("container-title")
    container_title = (
        container_title[0] if isinstance(container_title, list) and container_title else ""
    )

    if ptf_data.type == "incollection":
        ptf_data.chapter_title_tex = title
        ptf_data.source_tex = container_title
    elif ptf_data.type in ["book", "phdthesis"]:
        ptf_data.source_tex = title
        ptf_data.series = container_title
    else:
        ptf_data.article_title_tex = title
        ptf_data.source_tex = container_title

    ptf_data.issue = str(crossref_data.get("issue", ""))
    ptf_data.volume = str(crossref_data.get("volume", ""))

    ptf_data.year = (
        str(crossref_data["published-online"]["date-parts"][0][0])
        if crossref_data.get("published-online")
        else ""
    )
    ptf_data.year = (
        str(crossref_data["published-print"]["date-parts"][0][0])
        if crossref_data.get("published-print")
        else ptf_data.year
    )
    ptf_data.year = (
        str(crossref_data["published"]["date-parts"][0][0])
        if crossref_data.get("published-print")
        else ptf_data.year
    )

    ptf_data.publisher_name = crossref_data.get("publisher", "")
    ptf_data.publisher_loc = crossref_data.get("publisher-location", "")
    ptf_data.eid = crossref_data.get("article-number", "")

    ptf_data.citation_xml = "<label></label><element-citation></element-citation>"
    ptf_data.citation_html = ptf_data.citation_tex = ""


def parse_crossref_references(crossref_data):
    crossref_refs = crossref_data.get("reference", [])
    refs = []
    for crossref_ref in crossref_refs:
        if "DOI" in crossref_ref:
            doi = crossref_ref["DOI"]
            ref_data = crossref_request_reference(doi)
        else:
            ref_data = model_data.create_refdata(lang="und")

            # TODO: Parse structured citation without DOI

            unstructured = crossref_ref["unstructured"] if "unstructured" in crossref_ref else ""
            label = ""
            if unstructured and unstructured[0] == "[":
                pos = unstructured.find("]")
                if pos > 0:
                    label = unstructured[1:pos]
                pos += 1
                if unstructured[pos] == " ":
                    pos += 1
                unstructured = unstructured[pos:]

            ref_data.label = label
            if unstructured:
                ref_data.citation_html = ref_data.citation_tex = unstructured
                ref_data.citation_xml = f'<label>{xml_utils.escape(label)}</label><mixed-citation xml:space="preserve">{xml_utils.escape(unstructured)}</mixed_citation>'

        refs.append(ref_data)
    return refs


def fix_id(id_):
    pos = id_.find("10.")
    if pos == -1:
        pos = id_.find("hal-")
    if pos == -1:
        pos = id_.lower().find("arxiv:")

    if pos > 0:
        id_ = id_[pos:].strip("/")

    return id_


def parse_crossref_common(ptf_data, crossref_data, all_metadata=True):
    authors = parse_crossref_authors(crossref_data)
    ptf_data.contributors = authors

    title = crossref_data.get("title")
    ptf_data.title_tex = title[0] if isinstance(title, list) and title else ""

    if "relation" in crossref_data and "is-review-of" in crossref_data["relation"]:
        ptf_data.preprint_id = fix_id(crossref_data["relation"]["is-review-of"][0]["id"])
    if "relation" in crossref_data and "is-comment-on" in crossref_data["relation"]:
        ptf_data.preprint_id = fix_id(crossref_data["relation"]["is-comment-on"][0]["id"])

    if all_metadata:
        if "abstract" in crossref_data:
            abstract = crossref_data["abstract"]
            abstract = xml_utils.get_text_from_xml_with_mathml(f"<abstract>{abstract}</abstract>")
            if abstract.lower().find("abstract") == 0:
                abstract = abstract[8:]

            ptf_data.abstracts = [
                {
                    "tag": "abstract",
                    "lang": "en",
                    "value_html": abstract,
                    "value_tex": abstract,
                    "value_xml": f'<abstract xml:lang="en">{abstract}</abstract>',
                }
            ]

    # model_data.ArticleData does not store information about its issue
    # But they are useful for the ptf-tools matching.
    # 'journal': data['container-title'][0] if data.get('container-title') else None,
    # 'issue': data.get('issue', None),
    # 'volume': data.get('volume', None),
    # 'year': data['published-print']['date-parts'][0][0] if data.get('published-print') else None,

    page_range = crossref_data.get("page", None)
    fpage, lpage = xml_utils.handle_pages(page_range)
    ptf_data.fpage = str(fpage) if fpage else ""
    ptf_data.lpage = str(lpage) if lpage else ""
    if fpage is None and page_range is not None:
        if "-" in page_range:
            if ptf_data.type in ["book", "phdthesis"]:
                ptf_data.size = page_range
            else:
                ptf_data.page_range = page_range
        else:
            # An eid is sometimes stored in "page" by crossref members
            ptf_data.extids.append(("eid", page_range))

    if all_metadata:
        refs = parse_crossref_references(crossref_data)
        ptf_data.bibitems = refs


def crossref_request_resource(doi, is_ref=False, with_bib=True):
    url = settings.CROSSREF_CHECKDOI_URL.format(doi)
    response = requests.get(url)
    if response.status_code == 404:
        if is_ref:
            ptf_data = model_data.create_refdata(lang="und", doi=doi)
        else:
            ptf_data = model_data.create_articledata(doi=doi)
        return ptf_data

    response.raise_for_status()
    crossref_data = response.json()["message"]

    ptf_data = None
    if is_ref:
        ptf_data = model_data.create_refdata(lang="und", doi=doi)
        parse_crossref_reference(ptf_data, crossref_data)
    else:
        # See https://api.crossref.org/v1/types
        resource_type = crossref_data.get("type", "")

        ptf_data = model_data.create_articledata(doi=doi)

    if ptf_data:
        parse_crossref_common(ptf_data, crossref_data, all_metadata=with_bib and not is_ref)

    return ptf_data


def crossref_request_article(doi, with_bib=True, create_author_if_empty=False):
    article_data = crossref_request_resource(doi, with_bib=with_bib)
    model_data_converter.update_data_for_jats(
        article_data, create_author_if_empty=create_author_if_empty
    )
    return article_data


def fetch_article(preprint_id, with_bib=True, create_author_if_empty=False):
    article_data = model_data.create_articledata(doi=preprint_id)
    if preprint_id.startswith("10."):
        agency = fetch_metadata.get_agency(preprint_id)
        if agency == "Crossref":
            article_data = crossref_request_article(
                preprint_id, with_bib=with_bib, create_author_if_empty=create_author_if_empty
            )
            if preprint_id.startswith("10.1101"):
                tmp_data = fetch_metadata.get_biorxiv_metadata(preprint_id)
                if tmp_data:
                    tmp_data.bibitems = article_data.bibitems
                    article_data = tmp_data
        elif agency == "DataCite":
            article_data = fetch_metadata.get_datacite_metadata(preprint_id, with_bib=True)
    else:
        article_data = fetch_metadata.no_doi_metadata_article(preprint_id, with_bib=True)
    return article_data


def crossref_request_reference(doi):
    ref_data = crossref_request_resource(doi, is_ref=True)
    return ref_data


def find_string(node, tag, recursive=False):
    if not node:
        return ""
    value = node.find(tag, recursive=recursive, string=True)
    return value.string if value else ""


def get_title(node, recursive=False):
    title = ""
    if node:
        titles = node.find("titles", recursive=recursive)
        if titles:
            title = titles.find("title")
            title = title.get_text(" ", strip=True) if title else ""
    return title


def get_article_id(node):
    article_id = ""
    if node.find("publisher_item"):
        article_id = node.find(item_number_type="article-number")
        if article_id:
            article_id = article_id.string
    return article_id


def crossref_doi_data_to_ref(item):
    ref = model_data.create_refdata(lang="und", doi=find_string(item, "doi"))
    doc_type = item.doi.get("type")
    container = document = None

    if doc_type == "journal_article":
        container = item.find("journal_issue")
        container_title = find_string(item, "full_title", recursive=True)
    elif doc_type in ["conference_title", "conference_paper"]:
        container = item.find(["proceedings_metadata", "proceedings_series_metadata"])
        container_title = find_string(container, "proceedings_title")
    elif doc_type in ["book_title", "book_content"]:
        container = ["book_metadata", "book_series_metadata", "book_set_metadata"]
        container = item.find(container)
        container_title = get_title(container)
    elif doc_type == "dissertation":
        setattr(ref, "type", "thesis")
        container = item.find("dissertation")
        container_title = get_title(container)
    elif doc_type == "component":
        setattr(ref, "type", "misc")
        container = item.find("component")
        container_title = ""
        setattr(ref, "article_title_tex", get_title(container))
    else:
        return ref

    if doc_type == "journal_article":
        setattr(ref, "type", "article")
        document = item.find("journal_article")
        setattr(ref, "article_title_tex", get_title(document))
    elif doc_type == "conference_title":
        setattr(ref, "type", "proceedings")
    elif doc_type == "conference_paper":
        setattr(ref, "type", "inproceedings")
        document = item.find("conference_paper")
    elif doc_type == "book_title":
        setattr(ref, "type", "book")
    elif doc_type == "book_content":
        setattr(ref, "type", "incollection")
        document = item.find("content_item")

    if not document:
        document = container

    if doc_type in ["conference_paper", "book_content"]:
        setattr(ref, "chapter_title_tex", get_title(document))

    if container:
        setattr(ref, "source_tex", container_title)
        series_title = get_title(container.find(["series_metadata", "set_metadata"]))
        volume = find_string(container, "volume", recursive=True)
        issue = find_string(container, "issue")
        setattr(ref, "series", series_title)
        setattr(ref, "volume", volume)
        setattr(ref, "issue", issue)

        publisher = container.find("publisher")
        if publisher:
            setattr(ref, "publisher_name", find_string(publisher, "publisher_name"))
            setattr(ref, "publisher_loc", find_string(publisher, "publisher_place"))

    if document:
        pages = document.find("pages")
        first_page = find_string(pages, "first_page")
        last_page = find_string(pages, "last_page")
        if first_page and last_page:
            if first_page.isnumeric() and last_page.isnumeric():
                setattr(ref, "fpage", first_page)
                setattr(ref, "lpage", last_page)
        elif first_page and not last_page:
            setattr(ref, "eid", first_page)
        elif not first_page and not last_page:
            setattr(ref, "eid", get_article_id(document))

        publication_date = document.find(["publication_date", "approval_date"])
        publication_date_online = document.find("publication_date", media_type="online")
        publication_date_print = document.find("publication_date", media_type="print")
        year = find_string(publication_date, "year") if publication_date else ""
        if publication_date_print:
            year = find_string(publication_date_print, "year")
        if publication_date_online:
            year = find_string(publication_date_online, "year")
        setattr(ref, "year", year)

        authors = []
        contributors = document.find_all("person_name", contributor_role="author")
        for contributor in contributors:
            author = model_data.create_contributor()
            author["role"] = "author"
            author["first_name"] = find_string(contributor, "given_name")
            author["last_name"] = find_string(contributor, "surname")
            authors.append(author)
        setattr(ref, "contributors", authors)
    return ref


def crossref_request_references(dois):
    url = "https://doi.crossref.org/search/doi?"
    user = settings.CROSSREF_USER
    password = settings.CROSSREF_PWD
    params = {"usr": user, "pwd": password, "doi": dois, "format": "unixsd"}
    response = requests.post(url, params=params)
    response.encoding = "utf-8"

    refs = []
    if response.status_code == 200:
        warnings.filterwarnings("ignore", category=XMLParsedAsHTMLWarning)
        soup = BeautifulSoup(response.text, "html.parser")
        querys = soup.find_all("query")
        for query in querys:
            ref = crossref_doi_data_to_ref(query)
            model_data_converter.convert_refdata_for_editor(ref)
            refs.append(ref)
    return refs
