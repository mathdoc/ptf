from datetime import timedelta

import requests
import xmltodict
from bs4 import BeautifulSoup
from requests_cache import CachedSession
from requests_cache import FileCache

from django.conf import settings

from matching import crossref
from ptf.citedby import built_extlinks
from ptf.citedby import citedby_ads
from ptf.citedby import citedby_ads_refs
from ptf.citedby import create_refdata
from ptf.model_data import create_articledata
from ptf.model_data import create_contributor
from ptf.model_data_converter import update_data_for_jats

session = CachedSession(
    backend=FileCache(
        getattr(settings, "REQUESTS_CACHE_LOCATION", None) or "/tmp/ptf_requests_cache",
        decode_content=False,
    ),
    headers={
        "User-Agent": getattr(settings, "REQUESTS_USER_AGENT", None) or "Mathdoc/1.0.0",
        "From": getattr(settings, "REQUESTS_EMAIL", None) or "accueil@listes.mathdoc.fr",
    },
    expire_after=timedelta(days=30),
)

ARXIV_URL = "https://export.arxiv.org/api/query"
BIORXIV_URL = "https://api.biorxiv.org/details/"
DATACITE_URL = "https://api.datacite.org/dois/"
HAL_URL = "https://api.archives-ouvertes.fr/search"


def get_hal_metadata(paper_id):
    if paper_id.startswith("tel-") or paper_id.startswith("hal-"):
        id_type = "halId_s:"
    else:
        id_type = "arxivId_s:"
        paper_id = paper_id.split(":")[-1].split("v")[0]

    headers = {"Content-Type": "application/atom+xml"}
    params = {"fq": id_type + paper_id, "fl": "title_s,abstract_s,keyword_s,label_xml"}
    response = session.get(HAL_URL, params=params, headers=headers, timeout=10.0)
    data = response.json().get("response", {}).get("docs")
    if data and "label_xml" in data[0]:
        data = data[0]
        xml = xmltodict.parse(data["label_xml"]).get("TEI", {}).get("text", {})
    else:
        return None

    meta = {"contributors": [], "provider": "hal"}
    meta["title"] = data["title_s"][0] if "title_s" in data else ""
    meta["abstracts"] = data["abstract_s"][0] if "abstract_s" in data else ""
    meta["keywords"] = data.get("keyword_s", [])

    orgs = {}
    addresses = xml.get("back", {}).get("listOrg", {})
    addresses = addresses[0] if isinstance(addresses, list) else addresses
    for item in addresses.get("org", []):
        address_struct = item.get("desc", {}).get("address", {})
        org_name = item.get("orgName", "")
        orgs[item["@xml:id"]] = {
            "orgName": org_name if isinstance(org_name, list) else [org_name],
            "addrLine": address_struct.get("addrLine", ""),
            "country_key": address_struct.get("country", {}).get("@key", ""),
        }

    authors = xml.get("body", {}).get("listBibl", {}).get("biblFull", {})
    authors = authors.get("titleStmt", {}).get("author", {})
    authors = authors if isinstance(authors, list) else [authors]
    for author in authors:
        contributor = create_contributor()
        forenames = author.get("persName", {}).get("forename", {})
        forenames = forenames if isinstance(forenames, list) else [forenames]
        first_name = [forename.get("#text", "") for forename in forenames]
        contributor["first_name"] = " ".join(first_name)
        contributor["last_name"] = author.get("persName", {}).get("surname", "")

        structs = author.get("affiliation", [])
        structs = structs if isinstance(structs, list) else [structs]
        contributor["addresses"] = []
        for item in structs:
            sid = item.get("@ref", "").replace("#", "")
            labo = orgs.get(sid)
            address = [labo.get("orgName")[0], labo.get("addrLine")]
            address = ", ".join(ad for ad in address if ad)
            contributor["addresses"].append(address)

        idno = author.get("idno", [])
        idno = idno if isinstance(idno, list) else [idno]
        for item in idno:
            if item["@type"] == "ORCID":
                contributor["orcid"] = item.get("#text").split("/")[-1]

        email = author.get("email", [])
        email = email if isinstance(email, list) else [email]
        for item in email:
            if item["@type"] == "domain":
                contributor["email"] = item.get("#text")
        meta["contributors"].append(contributor)
    return meta


def get_arxiv_metadata(arxiv_id):
    headers = {"Content-Type": "application/atom+xml"}
    paper_id = arxiv_id.split(":")[-1]
    params = {"id_list": paper_id, "max_results": 1}
    response = session.get(ARXIV_URL, params=params, headers=headers, timeout=5.0)
    response.encoding = "utf-8"
    if response.status_code != 200:
        return None

    data = xmltodict.parse(response.text)
    entry = data.get("feed", {}).get("entry", {})
    meta = {"contributors": [], "provider": "arxiv"}
    meta["title"] = entry.get("title", "").replace("\r", "").replace("\n", "")
    meta["abstracts"] = entry.get("summary", "")
    authors = entry.get("author", [])
    authors = authors if isinstance(authors, list) else [authors]
    for author in authors:
        result = author.get("name", "").split(" ")
        contributor = create_contributor()
        contributor["first_name"] = " ".join(result[:-1])
        contributor["last_name"] = result[-1]
        meta["contributors"].append(contributor)
    return meta


def get_agency(doi):
    response = session.get("https://doi.org/ra/" + doi, timeout=5.0)
    if response.status_code == 200:
        return response.json()[0].get("RA", "")
    return ""


def get_datacite_metadata(doi, with_bib=True, timeout=30.0):
    try:
        response = session.get(DATACITE_URL + doi, timeout=timeout)
    except requests.exceptions.ReadTimeout:
        return None

    if response.status_code != 200:
        return None

    response.encoding = "utf-8"
    data = response.json()["data"]["attributes"]
    meta = {"contributors": [], "provider": "datacite"}
    meta["title"] = data["titles"][0]["title"]
    meta["keywords"] = [item["subject"] for item in data["subjects"]]
    meta["year"] = data["publicationYear"]
    meta["source"] = data["publisher"]

    for item in data["descriptions"]:
        if item["descriptionType"] == "Abstract":
            meta["abstracts"] = item["description"]
            break

    string_name_set = set()
    for creator in data["creators"] + data["contributors"]:
        first_name = creator["givenName"] if "givenName" in creator else ""
        last_name = creator["familyName"] if "familyName" in creator else ""
        if not last_name and "," not in creator["name"]:
            result = creator["name"].split()
            if len(result) == 2:
                last_name, first_name = result
            else:
                continue
        contributor = create_contributor()
        contributor["first_name"] = first_name
        contributor["last_name"] = last_name
        for aff in creator["affiliation"]:
            contributor["addresses"].append(aff)
        for nid in creator["nameIdentifiers"]:
            if nid["nameIdentifierScheme"] == "ORCID":
                contributor["orcid"] = nid["nameIdentifier"].split("/")[-1]
        if contributor["string_name"] not in string_name_set:
            string_name_set.add(contributor["string_name"])
            meta["contributors"].append(contributor)

    if with_bib:
        meta["bibitems"] = []
        for rid in data["relatedIdentifiers"]:
            is_doi = rid["relatedIdentifierType"] in ["DOI", "arXiv", "ISBN"]
            if is_doi and rid["relationType"] == "Cites":
                doi = rid["relatedIdentifier"]
                agency = get_agency(doi)
                if agency == "Crossref":
                    ref = crossref.crossref_request_reference(doi)
                    meta["bibitems"].append(ref)
                elif agency == "DataCite":
                    data = get_datacite_metadata(doi, with_bib=False, timeout=2.0)
                    if not data:
                        continue
                    ref = create_refdata()
                    ref.doi = doi
                    ref.type = "article"
                    ref.source_tex = data.get("source", "")
                    ref.article_title_tex = data.get("title", "")
                    ref.year = str(data.get("year", ""))
                    ref.contributors = data["contributors"]
                    meta["bibitems"].append(ref)
        return create_ref_from_metadata(meta)
    return meta


def find_string(node, tag, recursive=False):
    if not node:
        return ""
    value = node.find(tag, recursive=recursive, string=True)
    return value.string if value else ""


def get_contributor(contrib):
    contributor = create_contributor()
    contributor["first_name"] = find_string(contrib, "given-names", recursive=True)
    contributor["last_name"] = find_string(contrib, "surname", recursive=True)
    return contributor


def parse_biorxiv_references(xml):
    references = []
    ref_lists = xml.find_all("ref-list")
    for ref_list in ref_lists:
        for item in ref_list.find_all("citation"):
            ref = {}
            doctype = item.get("publication-type", "").replace("journal", "article")
            ref["type"] = doctype if doctype in ["article", "book"] else "misc"
            ref["source_tex"] = find_string(item, "source")
            ref["article_title_tex"] = find_string(item, "article-title")
            ref["volume"] = find_string(item, "volume")
            ref["issue"] = find_string(item, "issue")
            ref["year"] = find_string(item, "year")
            ref["fpage"] = find_string(item, "fpage")
            ref["lpage"] = find_string(item, "lpage")

            doi = find_string(item, "ext-link").split("10.")
            ref["doi"] = "10." + doi[1] if len(doi) == 2 else ""

            contribs = item.find_all("string-name")
            ref["contributors"] = [get_contributor(contrib) for contrib in contribs]

            bibitem = create_refdata()
            for key, val in ref.items():
                setattr(bibitem, key, val)
            references.append(bibitem)
    return references


def parse_biorxiv_metadata(server, xml):
    meta = {"contributors": [], "provider": server}
    article = xml.find("article-meta")
    abstract = article.find("abstract")
    abstract = abstract.find("p") if abstract else ""
    meta["abstracts"] = abstract.text if abstract else ""

    kwd_group = article.find("kwd-group")
    if kwd_group:
        meta["keywords"] = [k.string for k in kwd_group.find_all("kwd", string=True)]
    meta["title"] = find_string(article, "article-title", recursive=True)

    affs = {}
    contrib_group = article.find("contrib-group")
    for lab in contrib_group.find_all("aff"):
        label = lab.find("label")
        label.clear() if label else None
        if lab.get("id"):
            affs[lab["id"]] = lab.text

    contribs = contrib_group.find_all("contrib")
    for contrib in contribs:
        if contrib["contrib-type"] == "author":
            contributor = get_contributor(contrib)
            if contributor["last_name"]:
                for xref in contrib.find_all("xref"):
                    rid = xref.get("rid")
                    if rid and affs.get(rid):
                        contributor["addresses"].append(affs.get(rid))
                for cid in contrib.find_all("contrib-id", string=True):
                    if cid.get("contrib-id-type", "") == "orcid":
                        contributor["orcid"] = cid.string.split("/")[-1]
                meta["contributors"].append(contributor)

    meta["bibitems"] = parse_biorxiv_references(xml)
    return create_ref_from_metadata(meta)


def get_biorxiv_metadata(doi):
    for server in ["biorxiv", "medrxiv"]:
        response = session.get(BIORXIV_URL + server + "/" + doi, timeout=10.0)
        response.encoding = "utf-8"
        if response.status_code == 200 and response.json()["collection"]:
            xmlurl = response.json()["collection"][-1]["jatsxml"]
            response = session.get(xmlurl, timeout=10.0)
            response.encoding = "utf-8"
            if response.status_code == 200:
                soup = BeautifulSoup(response.content, "lxml")
                return parse_biorxiv_metadata(server, soup)


def create_ref_from_metadata(meta, paper_id="", with_bib=True):
    ref = create_articledata()
    setattr(ref, "provider", meta["provider"])
    setattr(ref, "title_tex", meta["title"])
    abstracts = {"lang": "en", "tag": "", "value_tex": meta["abstracts"]}
    setattr(ref, "abstracts", [abstracts])
    for contrib in meta["contributors"]:
        contrib["role"] = "author"
    setattr(ref, "contributors", meta["contributors"])
    if "keywords" in meta:
        kwds = [{"lang": "en", "type": "", "value": kwd} for kwd in meta["keywords"]]
        setattr(ref, "kwds", kwds)
    if meta.get("bibitems"):
        setattr(ref, "bibitems", meta["bibitems"])

    if with_bib:
        if paper_id.lower().startswith("arxiv:"):
            metadata = {"arxiv_id": paper_id}
            citations = citedby_ads(metadata, by_doi=False, citedby=False)
            ref.bibitems = citedby_ads_refs(citations)
        for bib in ref.bibitems:
            built_extlinks(bib)

    update_data_for_jats(ref)
    return ref


def no_doi_metadata_article(paper_id, with_bib=True):
    meta = get_hal_metadata(paper_id)
    if not meta:
        meta = get_arxiv_metadata(paper_id)

    if not meta:
        ref = create_articledata()
    else:
        ref = create_ref_from_metadata(meta, paper_id, with_bib=with_bib)
    return ref
