import os

from lxml import html

from django.conf import settings

from ptf import model_data
from ptf import model_helpers


def request_article(doi, pii=None):
    article_data = model_data.create_articledata(doi=doi)

    if pii is None:
        url = f"https://doi.org/{doi}"
    else:
        url = f"https://www.sciencedirect.com/science/article/pii/{pii}"

    import random

    import cfscrape

    user_agents_list = [
        "Mozilla/5.0 (iPad; CPU OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.83 Safari/537.36",
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36",
    ]

    headers = {
        "User-Agent": random.choice(user_agents_list),
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
        "Accept-Language": "en-US,en;q=0.5",
        "Accept-Encoding": "gzip, deflate",
        "Connection": "keep-alive",
        "Upgrade-Insecure-Requests": "1",
        "Sec-Fetch-Dest": "document",
        "Sec-Fetch-Mode": "navigate",
        "Sec-Fetch-Site": "none",
        "Sec-Fetch-User": "?1",
        "Cache-Control": "max-age=0",
    }

    scraper = cfscrape.create_scraper(delay=10)
    response = scraper.get(url, headers=headers)

    if response.status_code == 404:
        return article_data

    response.raise_for_status()
    tree = html.fromstring(response.content)

    date_published = None
    metas = tree.xpath("/html/head/meta")
    if len(metas) > 0:
        for m in metas:
            if m.get("name") in ["citation_publication_date", "citation_online_date"]:
                d = model_helpers.parse_date_str(m.get("content"))
                if date_published is None or d < date_published:
                    date_published = d
            elif m.get("name") == "citation_doi":
                article_data.doi = m.get("content")

    article_data.date_published_iso_8601_date_str = date_published.strftime("%Y-%m-%d")

    return article_data


def fetch_article(doi, pii=None, pii_doi_equivalence=False):
    # pii <-> DOI equivalence is stored in a file to avoid multiple requests to Elsevier (and avoid to be banned)
    filename = os.path.join(settings.LOG_DIR, "elsevier_ids.log")

    if pii_doi_equivalence:
        ids = {}
        if os.path.isfile(filename):
            file_ = open(filename)
            ids = file_.readlines()
            file_.close()
            ids = {key: value for (key, value) in [x.split() for x in ids]}

        if pii in ids:
            doi = ids[pii]
        else:
            article_data = request_article(doi, pii)
            doi = article_data.doi

            with open(
                os.path.join(settings.LOG_DIR, "elsevier_ids.log"), "a", encoding="utf-8"
            ) as file_:
                file_.write(pii + " " + doi + "\n")

        return doi

    article_data = request_article(doi, pii)
    return article_data
