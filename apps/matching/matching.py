import json
import re
from datetime import timedelta

from requests_cache import CachedSession
from requests_cache import FileCache

from django.conf import settings
from django.urls import reverse

from ptf.cmds.xml import xml_utils
from ptf.model_helpers import add_or_update_bibitemid
from ptf.model_helpers import add_or_update_extid
from ptf.model_helpers import get_bibitemid
from ptf.model_helpers import get_extid
from ptf.models import get_names
from ptf.utils import strip_markup
from pubmed.views import pubmed_lookup

from .crossref import crossref_request

# Pubmed matching
# See: https://www.ncbi.nlm.nih.gov/pmc/tools/id-converter-api/
# then https://www.ncbi.nlm.nih.gov/books/NBK25500/#chapter1.Downloading_Full_Records

session = CachedSession(
    backend=FileCache(
        getattr(settings, "REQUESTS_CACHE_LOCATION", None) or "/tmp/ptf_requests_cache",
        decode_content=False,
    ),
    headers={
        "User-Agent": getattr(settings, "REQUESTS_USER_AGENT", None) or "Mathdoc/1.0.0",
        "From": getattr(settings, "REQUESTS_EMAIL", None) or "accueil@listes.mathdoc.fr",
    },
    expire_after=timedelta(days=30),
)


def mr_lookup(parameters):
    """
    query = '0373-0956|Ann. Inst. Fourier (Grenoble)|Collin, Pascal; Rosenberg, Harold|60|7||2010||||Asymptotic values of minimal graphs in a disc'
    ISSN | JOUR | AUTH | VID | IID | PID | YNO | TYP | KEY | MRID | TTL
        where:

        ISSN = ISSN
        JOUR = Journal title or abbreviation
        AUTH = Author name(s)
        VID = Volume number
        IID = Issue ID number
        PID = Initial Page number
        YNO = Year
        TYP = Resource type (for compatibility with Crossref; in Batch MR Lookup this value is ignored)
        KEY = User supplied key
        MRID = Identifier (Mathematical Reviews Number)
        TTL = Item title

    :param params:
    :return:
    """
    parameters["authors"] = "; ".join(parameters["authors"])
    params = {
        "api": "xref",
        "qdata": "|{journal}|{authors}|{volume}|{issue}||{year}||||{title}".format(**parameters),
    }
    url = "https://mathscinet.ams.org/batchmrlookup"
    response = session.get(url, params=params, timeout=2)
    response.raise_for_status()
    data = response.text.split("|")
    if len(data) > 9:
        result = data[9]
    return result


def numdam_lookup(params):
    url = f"{settings.NUMDAM_MATCHING_URL}/api/lookup/"
    # Ici, on ne prend que le nom des auteur
    # Il semble que les ponctuations et l'initiale
    # du prénom embrouillent solr
    params["authors"] = " ".join([author.split(",")[0] for author in params["authors"]])
    response = session.get(url, params, timeout=2)
    response.raise_for_status()
    data = response.json()
    if "pid" in data:
        result = data["pid"]
    else:
        result = ""
    return result


def zbmath_lookup(params):
    # 't' : title (article/chapter)
    # 'a' : authors
    # 'j' : journal/book title, citation source or conf-name
    # 'v' : volume
    # 'i' : issue
    # 'p' : fpage - lpage
    # 'y' : year
    # 'q' : query. For badly formatted references. (unstructured biblio)

    result = ""
    if "unstructured" in params:
        query = {
            "queries": [
                {
                    "q": params["citation"],
                }
            ]
        }
    else:
        query = {
            "queries": [
                {
                    "a": params["authors"],
                    "t": params["title"],
                    "j": params["journal"],
                    "v": params["volume"],
                    "i": params["issue"],
                    "y": params["year"],
                    "p": params["pages"],
                }
            ]
        }

    url = "https://zbmath.org/citationmatching/match"
    headers = {"Content-Type": "application/json"}
    response = session.post(url, json=query, headers=headers, timeout=2)
    response.raise_for_status()
    # We get the key zbl_item of the first result
    try:
        result = response.json()["results"][0][0]["zbl_id"]
    except (IndexError, KeyError):
        # No results
        pass

    return result


def crossref_lookup(params):
    if "unstructured" in params:
        url = settings.CROSSREF_CHECKDOI_URL.format("")
        query = {
            "query.bibliographic": params["citation"],
            "select": "DOI",
            "rows": "1",
            "mailto": settings.CROSSREF_MAIL,
        }
        response = session.get(url, params=query)
        response.raise_for_status()
        result = response.json()["message"]["items"][0]["DOI"]
    else:
        # We strip journal from all xml based markup
        # Sometimes, some mathml may be present here
        params["journal"] = strip_markup(params["journal"])

        xml = '<?xml version="1.0"?><query_batch version="2.0" xsi:schemaLocation="http://www.crossref.org/qschema/2.0 http://www.crossref.org/qschema/crossref_query_input2.0.xsd" xmlns="http://www.crossref.org/qschema/2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><head><email_address>support@crossref.org</email_address><doi_batch_id>ABC_123_fff</doi_batch_id></head><body>'
        xml += '<query key="1178517" enable-multiple-hits="false" forward-match="false">'
        if params["issn"]:
            xml += '<issn match="optional">' + params["issn"] + "</issn>"
        if params["journal"]:
            xml += "<journal_title>" + params["journal"] + "</journal_title>"
        if params["authors"]:
            xml += (
                '<author match="fuzzy" search-all-authors="false">'
                + params["authors"][0]
                + "</author>"
            )
        if params["volume"]:
            xml += "<volume>" + params["volume"] + "</volume>"
        if params["issue"]:
            xml += "<issue>" + params["issue"] + "</issue>"
        if params["fpage"]:
            xml += "<first_page>" + params["fpage"] + "</first_page>"
        if params["year"]:
            xml += "<year>" + params["year"] + "</year>"
        if params["title"]:
            # Here, we limit the size of the title to 255 characters
            # Crossref doesn't support more
            xml += "<article_title>" + params["title"][:255] + "</article_title>"
        # 'volume_title': $issue - meta / issue_title,
        # 'edition_number': $citation // edition[1],
        xml += "</query></body></query_batch>"

        # We strip xml from all '&'s.
        # Their presence seems to confuse crossref. It seems to be a bug on their part
        # need to be URL encoded ? https://www.crossref.org/documentation/retrieve-metadata/xml-api/using-https-to-query/

        xml = xml.replace("&", "")

        params = {
            "usr": settings.CROSSREF_USER_5802,
            "pwd": settings.CROSSREF_PWD_5802,
            "qdata": xml,
        }

        url = settings.CROSSREF_BASEURL + "/servlet/query"
        response = session.get(url, params=params)
        response.raise_for_status()
        body = response.text.rstrip()
        data = body.split("|")

        result = ""
        if len(data) > 9:
            result = data[9]
        if "10." not in result and len(data) > 11:
            result = data[11]

    return result


def numdam_fetch(pid):
    url = f"{settings.NUMDAM_MATCHING_URL}/api/fetch/"
    params = {"pid": pid}
    response = session.get(url, params, timeout=1)
    response.raise_for_status()
    result = response.text
    return result


def mr_request(mrid):
    url = f"https://mathscinet.ams.org/mathscinet/api/public/articles/{mrid}"
    response = session.get(url, params={"mr": mrid})
    response.raise_for_status()

    result = {
        "title": "",
        "authors": "",
        "journal": "",
        "issue": "",
        "volume": "",
        "year": "",
        "fpage": "",
        "lpage": "",
        "page": "",
        "raw_source": "",
    }

    if hasattr(response, "text"):
        json_data = json.loads(response.text)
        result["title"] = json_data["titles"]["title"]
        result["authors"] = "; ".join(
            [x["name"] for x in json_data["authorsAndMentions"]["authors"]]
        )
        issue = json_data["issue"]["issue"]
        result["journal"] = issue["journal"]["title"]
        result["issue"] = issue["issNum"]
        result["volume"] = issue["volume"]
        result["year"] = issue["pubYear"]
        result["page"] = json_data["pagings"]["paging"]["text"]

    # url = 'https://mathscinet.ams.org/mathscinet-getitem'
    # response = requests.get(url, params={'mr': mrid})
    # response.raise_for_status()
    # xml_data = lxml.html.fromstring(response.text)
    # data = xml_data.find_class('title')[0].getparent()
    # source = data.xpath('em')[0].tail
    # try:
    #     page = re.findall(r', (\d+–\d+).', source)[0]
    #     issue = re.findall(r'no. (\d+),', source)[0]
    #     volume = re.findall(r'(\d+)  \(\d{4}\)', source)[0]
    #     year = re.findall(r'\((\d{4})\)', source)[0]
    #     fpage, lpage = page.split('–')
    #     raw_source = ''
    # except IndexError:
    #     page, issue, fpage, lpage, year, volume = [None, None, None, None, None, None]
    #     source = data.xpath('em')
    #     raw_source = source[0].tail
    #     if len(source) > 1:
    #         raw_source += ''.join([source[1].text, source[1].tail])
    # result = {
    #     'title': data.xpath('span')[0].text,
    #     'authors': data.xpath('br')[0].tail.strip(),
    #     'journal': data.xpath('em')[0].text,
    #     'issue': issue,
    #     'volume': volume,
    #     'year': year,
    #     'fpage': fpage,
    #     'lpage': lpage,
    #     'page': page,
    #     'raw_source': raw_source,
    # }
    return result


def zbmath_request(zblid):
    url = "https://zbmath.org/citationmatching/eudml/lookup"
    response = session.get(url, params={"zblid": zblid})
    response.raise_for_status()
    data = response.json()["result"]
    page_range = data.get("pagination", None)
    fpage, lpage = xml_utils.handle_pages(page_range)
    serial = data.get("serial", None)
    result = {
        "title": data["title"],
        "authors": "; ".join([author for author in data["authors"]]),
        "year": data.get("year", None),
        "fpage": fpage,
        "lpage": lpage,
        "page": page_range,
        "journal": serial.get("short-title", None) if serial else None,
        "issue": serial.get("issue", None) if serial else None,
        "volume": serial.get("volume", None) if serial else None,
    }
    return result


def build_citation(data):
    authors = data["authors"].replace(",", " ")
    title = " {}".format(data["title"])
    journal = ", {}".format(data["journal"]) if data["journal"] else ""
    volume = ", Volume {}".format(data["volume"]) if data["volume"] else ""
    year = " ({})".format(data["year"]) if data["year"] else ""
    issue = " no. {}".format(data["issue"]) if data["issue"] else ""
    page = ", pp. {}".format(data["page"]) if data["page"] else ""
    raw_source = data.get("raw_source", "")

    return "".join([authors, title, journal, volume, year, issue, page, raw_source])


def mr_fetch(mrid):
    data = mr_request(mrid)
    return build_citation(data)


def zbmath_fetch(zblid):
    data = zbmath_request(zblid)
    return build_citation(data)


def crossref_fetch(doi):
    data = crossref_request(doi)
    return build_citation(data)


def get_article_params(article):
    if not article:
        return

    container = article.my_container

    params = {
        "title": article.title_tex,
        "journal": article.get_collection().title_tex,
    }

    issn = article.get_collection().issn
    params["issn"] = issn

    authors = article.get_author_contributions(strict=False)
    authors = [str(author) for author in authors]
    params["authors"] = authors

    volume = container.volume
    params["volume"] = volume

    issue_number = container.number
    params["issue"] = issue_number

    year = container.year
    params["year"] = year

    params["pages"] = article.page_range

    params["fpage"] = article.fpage

    params["lpage"] = article.lpage

    return params


def get_bibitem_params(bibitem):
    if not bibitem:
        return

    # bibitem.article_title_tex stores the article title for an article
    # bibitem.chapter_title_tex stores the chapter title for an incollection

    # bibitem.source_tex stores
    #   - the journal name for an article
    #   - the booktitle for an incollection/inproceedings/conference
    #   - "howpublished" for a misc or a booklet
    #   - the title for the other types : NOT TRUE FOR CRCHIM so source_tex is the journal_title

    params = {}

    if bibitem.article_title_tex:
        params["title"] = strip_markup(bibitem.article_title_tex)
        params["journal"] = bibitem.source_tex
    elif bibitem.chapter_title_tex:
        params["title"] = strip_markup(bibitem.chapter_title_tex)
        params["journal"] = bibitem.source_tex
    else:
        params["title"] = ""
        params["journal"] = strip_markup(bibitem.source_tex)

    params["authors"] = get_names(bibitem, "author")
    params["volume"] = bibitem.volume
    params["issue"] = bibitem.issue
    params["year"] = bibitem.year
    params["pages"] = bibitem.page_range
    params["fpage"] = bibitem.fpage
    params["lpage"] = bibitem.lpage

    params["issn"] = ""

    # if no title nor authors are provided, we can almost safely assume that
    # this is an unstructured citation
    if not params["title"] and not params["authors"]:
        params["unstructured"] = True
        html_stripped = strip_markup(bibitem.citation_html)
        label_stripped = re.sub(r"\[\w+\]", "", html_stripped)
        params["citation"] = label_stripped
    return params


def delete_bibitemid(article, id_type):
    if article and id_type:
        extid = get_extid(article, id_type)
        if extid:
            extid.delete()


def match_article(article, what):
    params = get_article_params(article)
    result = ""

    if what in ["mr-item-id", "zbl-item-id", "numdam-id", "pmid"]:
        if what == "mr-item-id":
            result = mr_lookup(params)
        elif what == "numdam-id":
            result = numdam_lookup(params)
        elif what == "pmid":
            result = pubmed_lookup(article)
        else:
            result = zbmath_lookup(params)

        extid = get_extid(article, what)
        checked = what == "pmid"

        if not (extid and extid.false_positive and extid.id_value == result):
            add_or_update_extid(article, what, result, checked, False)
    elif what == "doi":
        result = crossref_lookup(params)
        if result:
            # TODO: cmds to modify
            article.doi = result
            article.save()

    return result


def create_temp_params(result, what, bibitem):
    if result and what in ["doi", "zbl-item-id"] and not bibitem.tempmatchparams_set.exists():
        if what == "doi":
            data = crossref_request(result)
        else:
            data = zbmath_request(result)
        bibitem.tempmatchparams_set.create(
            title=data["title"][:254],
            journal=data["journal"],
            authors=data["authors"],
            volume=data["volume"],
            issue=data["issue"],
            year=data["year"],
            first_page=data["fpage"],
            last_page=data["lpage"],
            issn=None,
        )


def get_unstructured_bibitem_params(bibitem):
    temp_params = bibitem.tempmatchparams_set.first()
    params = {
        "title": temp_params.title,
        "journal": temp_params.journal if temp_params.journal else "",
        "authors": temp_params.authors,
        "volume": temp_params.volume if temp_params.volume else "",
        "issue": temp_params.issue if temp_params.issue else "",
        "year": str(temp_params.year) if temp_params.year else "",
        "pages": f"{temp_params.first_page}-{temp_params.last_page}",
        "fpage": str(temp_params.first_page),
        "lpage": str(temp_params.last_page),
        "issn": "",
    }
    return params


def match_bibitem(bibitem, what):
    params = get_bibitem_params(bibitem)
    result = ""

    function_name = ""
    if what == "mr-item-id":
        function_name = "mr_lookup"
    elif what == "zbl-item-id":
        function_name = "zbmath_lookup"
    elif what == "doi":
        function_name = "crossref_lookup"
    elif what == "numdam-id":
        function_name = "numdam_lookup"

    if (
        what in ["mr-item-id", "numdam-id"]
        and params.get("unstructured")
        and bibitem.tempmatchparams_set.exists()
    ):
        params = get_unstructured_bibitem_params(bibitem)

    if function_name:
        method = globals().get(function_name)
        result = method(params)
        if "unstructured" in params:
            create_temp_params(result, what, bibitem)

        bibitemid = get_bibitemid(bibitem, what)
        # If the id is verified (checked) or marked as false positive, do not update
        if not (
            bibitemid
            and bibitemid.id_value == result
            and (bibitemid.checked or bibitemid.false_positive)
        ):
            add_or_update_bibitemid(bibitem, what, result, False, False)

    return result


def fetch_id(id_, what):
    result = ""
    if what == "mr-item-id":
        result = mr_fetch(id_)
    elif what == "zbl-item-id":
        result = zbmath_fetch(id_)
    elif what == "doi":
        result = crossref_fetch(id_)
    elif what == "numdam-id":
        result = numdam_fetch(id_)

    return result


def get_all_fetch_ids(article):
    urls = []
    for extid in article.extid_set.all():
        url = reverse(
            "api-fetch-id",
            kwargs={
                "id": extid.id_value,
                "what": extid.id_type,
                "pk": article.pk,
                "resource": "extid",
            },
        )
        url = url[10:]  # remove /fetch-id/
        urls.append(url)

    for bibitem in article.bibitem_set.all():
        for bibitemid in bibitem.bibitemid_set.all():
            url = reverse(
                "api-fetch-id",
                kwargs={
                    "id": bibitemid.id_value,
                    "what": bibitemid.id_type,
                    "pk": bibitem.pk,
                    "resource": "bibitemid",
                },
            )
            url = url[10:]  # remove /fetch-id/
            urls.append(url)

    return urls


if __name__ == "__main__":
    PARAMS = {
        "issn": "0373-0956",
        "journal": "Annales de l'institut Fourier",
        "authors": ["Collin, Pascal", "Rosenberg, Harold"],
        "volume": "60",
        "issue": "7",
        "year": "2010",
        "title": "Asymptotic values of minimal graphs in a disc",
        "pages": "2357-2372",
        "fpage": "2357",
        "lpage": "2372",
    }
    print(mr_lookup(PARAMS))
    print(zbmath_lookup(PARAMS))
    print(crossref_lookup(PARAMS))
