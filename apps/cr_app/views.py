from abc import abstractmethod

from django.conf import settings
from django.db.models.functions import Greatest

from mersenne_cms.views import HomeView
from ptf.models import Article


class CRHomeView(HomeView):
    class LastArticle:
        title: str
        author: str
        date_publication: any
        absolute_url: str
        site: str

        def __init__(self, title, author, date_publication, url):
            self.title = title
            self.author = author
            self.date_publication = date_publication
            self.absolute_url = url

    @property
    @abstractmethod
    def _sites(self):
        raise NotImplementedError

    def _get_class(self):
        raise NotImplementedError

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Specialize the CRE Home view
        context["latest_articles"] = self._get_last_articles()
        context["last_articles_msg"] = "Derniers articles"
        context["display_new_CR_articles"] = True
        context["display_new_articles"] = False
        return context

    def _get_last_articles(self) -> list[LastArticle]:
        qs = Article.objects.filter(sites__in=self._sites)
        qs = qs.exclude(classname="TranslatedArticle").order_by(
            Greatest("date_online_first", "date_published").desc(nulls_last=True), "-seq"
        )[:5]
        list_article = []
        for article in qs:
            list_article.append(self._build_article(article))
        return list_article

    def _build_article(self, article: Article) -> LastArticle:
        date: str
        if article.date_published:
            date = article.date_published
        else:
            date = article.date_online_first
        title: str
        if self.request.LANGUAGE_CODE != article.lang and article.trans_lang != "und":
            title = article.trans_title_html
        else:
            title = article.title_html
        if settings.SITE_NAME == "cr":
            site = article.sites.first()
            suffix = site.domain.split("/", 1)
            href = suffix[1] + article.get_absolute_url()
        else:
            href = article.get_absolute_url()
        return CRHomeView.LastArticle(title, article.get_authors_short(), date, href)
