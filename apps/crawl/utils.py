import csv
import json

# TODO: add capistrano task to download in a given directory ?
# nltk.download("crubadan")
import os
import re
from datetime import datetime

import nltk
from langcodes import Language
from langcodes import standardize_tag
from unidecode import unidecode


def write_to_csv(file, data):
    with open(file, "a") as fto:
        writer = csv.writer(fto)
        writer.writerow(data)


def write_to_log(logger, type, message, target=None, subj=None, type_error=None):
    subj_log = ""

    log = "\n target: "
    log += str(target) + "\n" + " created on: "
    log += str(datetime.now()) + "\n"
    log += str(message)
    log += "\n type_error : "
    log += str(type_error)
    log += "\n"

    # set log level
    # set flag Metadata

    if type == "error":
        if subj in ["authors", "pages", "year"]:
            subj_log = "\n " + "metadata: - " + subj + ":"

        else:
            subj_log = "\n " + "metadata:"
        log_text = subj_log + log
        logger.error(log_text)

    if type == "info":
        subj_log = "\n " + "metadata:  " + subj + ":"
        log_text = subj_log + log
        logger.info(log_text)


def convert_collections_items_to_csv():
    """@json path input"""
    with open(
        os.path.dirname(os.path.abspath(__file__)) + "/mathcollector/collections_eudml.json",
        encoding="utf8",
    ) as file:
        data = json.load(file)
        file_out = "collections_eudml.csv"
        rows = []
        with open(
            os.path.dirname(os.path.abspath(__file__)) + "/" + file_out, "w"
        ) as collections_csv:
            fieldnames = ["title", "url", "pid", "comments", "exclude", "set", "count"]
            writer = csv.DictWriter(collections_csv, fieldnames=fieldnames)
            writer.writeheader()
            for elem in data:
                elem["title"] = elem["title"].replace("\n", "").strip()
                elem["title"] = elem["title"]
                rows.append(elem)
            writer.writerows(rows)

            # for collection in elem["collection"]:
            # collection["title"]=collection["title"].replace("\n","").rstrip()


def extract_pid_numdam(
    file_in="mathcollector/global_numdam.csv", file_of="mathcollector/cols_eudml.csv"
):
    """

    @param file_in:
    @param file_of:
    @return:
    """
    file_out = os.path.dirname(os.path.abspath(__file__)) + "/mathcollector/cols_eudml_2.csv"

    fieldnames = ["title", "url", "pid", "Commentaire", "exclude", "incomplet"]

    with open(file_out, "w") as list_csv:
        writer = csv.DictWriter(list_csv, fieldnames=fieldnames)
        writer.writeheader()
        with open(os.path.dirname(os.path.abspath(__file__)) + "/" + file_in) as file:
            with open(
                os.path.dirname(os.path.abspath(__file__)) + "/" + file_of, newline=""
            ) as cols_file:
                csvreader = csv.DictReader(cols_file)
                csv_numdam = csv.DictReader(file)
                data = list(csv_numdam)
                rows = []
                rows_pid = []
                for index, row in enumerate(csvreader):
                    title = row["title"]
                    pid = row["pid"]
                    rows_pid.append(pid)

                    for index_item, row_item in enumerate(data):
                        if index_item > 0:
                            print(row_item["publication_title"])
                            if title == row_item["publication_title"]:
                                row["pid"] = row_item["title_id"]

                    rows.append(row)

                wr = csv.DictWriter(list_csv, fieldnames=fieldnames, delimiter=",")
                wr.writeheader()
                wr.writerows(rows)


def merge_json():
    rows = []
    with open(
        os.path.dirname(os.path.abspath(__file__)) + "/mathcollector/itemscollections.json"
    ) as _f:
        data = json.load(_f)
        for index, item in enumerate(data):
            if not any(d["set"] == item["set"] for d in rows):
                item = {"set": item["set"], "count": int(item["count"])}
                if item["count"] > 0:
                    rows.append(item)
            else:
                for entry in rows:
                    if entry["set"] == item["set"]:
                        entry["count"] += int(item["count"])

        print(rows)


def compare_pid_on_files(
    file_of="mathcollector/cols_eudml.csv", file_to="mathcollector/eudml_import.log"
):
    with open(os.path.dirname(os.path.abspath(__file__)) + "/" + file_of, newline="") as cols_file:
        csvreader = csv.DictReader(cols_file)
        log_file = os.path.dirname(os.path.abspath(__file__)) + "/" + file_to

        rows = []
        rows_pid = []
        for index, row in enumerate(csvreader):
            pid = row["pid"]
            rows_pid.append(pid)
            with open(log_file) as file:
                lines = file.readlines()
                for line in lines:
                    if re.compile("col:").search(line):
                        pid_import = line.split("col:")
                        pid_import = pid_import[1].replace("\n", "")
                        if pid == pid_import.strip():
                            rows.append(pid)


def convert_csv_json(
    file_in="mathcollector/data/cols_eudml.csv",
    file_to="mathcollector/data/collections_eudml.json",
):
    list_items = []

    with open(os.path.dirname(os.path.abspath(__file__)) + "/" + file_in) as f:
        reader = csv.DictReader(f, delimiter=",")
        rows = list(reader)
        for row in rows:
            row = dict(row)
            list_items.append(row)
    with open(
        os.path.dirname(os.path.abspath(__file__)) + "/" + file_to, "w", encoding="utf8"
    ) as list_json:
        items = json.dumps(list_items, ensure_ascii=False)
        list_json.write(
            items,
        )


def export_json_csv(file_in="mathcollector/issn_numdam.json", file_out="numdam_issn.csv"):
    print(os.path.dirname(os.path.abspath(__file__)) + "/" + file_in)
    fieldnames = []
    with open(os.path.dirname(os.path.abspath(__file__)) + "/" + file_out, "w") as list_csv:
        # wr = csv.DictWriter(list_csv, fieldnames=fieldnames, delimiter=',', quoting=csv.QUOTE_ALL)
        with open(os.path.dirname(os.path.abspath(__file__)) + "/" + file_in) as file:
            data = json.load(file)
            if "collections" in data:
                if len(data["collections"]) > 0:
                    fieldnames.append("publication_tilte", "pid")
                    wr = csv.DictWriter(list_csv, fieldnames=fieldnames, delimiter="    ")
                    wr.writeheader()

                    for elem in data["collections"]:
                        row = {"publication_title": "", "pid": elem}
                        print(row)
                        wr.writerow(row)
            else:
                keys = data[0].keys()
                for key in keys:
                    fieldnames.append(key)
                wr = csv.DictWriter(list_csv, fieldnames=fieldnames, delimiter=" ")
                wr.writeheader()
                for elem in data:
                    wr.writerow(elem)


def strip_chars_from_json(file_output):
    with open(file_output) as file:
        data = json.load(file)

        for elem in data:
            elem["title"] = elem["title"].replace("\n", "").lstrip()
            elem["title"] = re.sub(r"[\"]", "", elem["title"])

    with open(file_output, "w") as f:
        json.dump(data, f, ensure_ascii=False)


def get_pid_from_title(title, pids):
    """
    Get the PID from a title.
    The PID is built with the initials of the words

    Ex: Annals of Mathematics => AM
        Πανελλήνιο Συνέδριο Μαθηματικής Παιδείας => PSMP

    Only the 4 letters max are used
    Common words are ignored (ex: "un, le, la" in French, "the" in English)
    """
    title = title.lower()

    try:
        # Remove common words of the title (ex: "un, le, la" in French, "the" in English)

        # First, guess the title language
        tc = nltk.classify.textcat.TextCat()
        guessed_lang = tc.guess_language(title).strip()

        # guessed_lang is in alpha3 but langcodes.Language needs the lang code in alpha2
        guessed_lang_alpha2 = standardize_tag(guessed_lang)
        guessed_lang_name = Language.get(guessed_lang_alpha2).display_name().lower()

        # Get the stopwords to filter in the title language (ex: "un, le, la, ..." in French, "the..." in English)
        words_to_remove = set(nltk.corpus.stopwords.words(guessed_lang_name))

        # Remove the stopwords
        title = " ".join([w for w in title.split() if w not in words_to_remove])
    except Exception:
        # The attempt to get the stopwords failed
        # (most likely because nltk stopwords does not know the guessed language)
        # Ignore the error and proceed
        pass

    pid = ""
    for word in title.split()[0:4]:
        letter = word[0]
        # ASCII representation of the letter
        letter = unidecode(letter)
        if len(letter) > 1:
            letter = letter[0]
        letter = letter.upper()
        if letter == "?":
            letter = "Z"
        pid = pid + letter

    base_pid = pid
    i = 2
    while pid in pids:
        pid = base_pid + str(i)
        i += 1

    pids.append(pid)
    return pid


if __name__ == "__main__":
    # print(get_pid_from_title("Annals of Mathematics"))
    # print(get_pid_from_title("Abhandlungen der Gesellschaft der Wissenschaften in Göttingen, Mathematisch-Physikalische Klasse"))
    # print(get_pid_from_title("Πανελλήνιο Συνέδριο Μαθηματικής Παιδείας"))
    # print(get_pid_from_title("Zpravodaj Československého sdružení uživatelů TeXu"))
    # print(get_pid_from_title("Annales de l'institut Fourier"))
    # print(get_pid_from_title("Séminaire L. de Broglie. Théories physiques"))
    # print(get_pid_from_title("Annals of Mathematics", ["AM"]))

    merge_json()
