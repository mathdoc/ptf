### Crawl application

- Processus pour récupérer les collections de différentes sources



1) Organisation
2) Importer des collections
3) Paramètrage des processus d'import


## Organisation

La librairie Scrapy est utilisée pour lancer des processus d'import.
La classe Collection Crawler étendue permet de parser les pages HTML d'une source.

package scrapy:

    ```
      /mathcollector
        /spiders
    ```


## Importer des collections
Tests:

commandes d'import

   > /management
>
   >       / commands


importer des collections depuis le shell:

Les données correspondantes aux périodes à importer en test se situent sur le fichier **list.py**


```
crawl -pid AM
```


Une période est instanciée dans une classe héritée de **CollectionCrawler**

- import d'un ensemble de collection
___


Etapes :

> 1. Récuperation des collections et enregistrement PTF
>
>    **`crawl -prepare_collections -eudml -username @username`**
>
>    username est l'utilisateur django qui va effectuer les tâches.
>
>
> 2. Import des collections
>
>   **`crawl -eudml -username @username`**
>
>
> 3. **[ou depuis un service supervisor](#configuration-serveur)**

 sortie paramètrable dans settings_local.py :
>
> LOG_EUDML = "/var/log/gdml/eudml_import.log"
>
### Progression
Pour suivre la progression d'avancement, des clés/valeurs redis sont mises à jour

> redis_client.exec_cmd(r.hset, "last_task_import", mapping={
    'id': self.uuid,
    'username': self.username,
    'collection': collection.pid,
    'progress': self.bar.numerator
    })
>
### Fonctionnement
![alt image](mathcollector/schema/fonctionnement_crawl_process.jpg "processus moissonnage")

## Paramètrage des processus d'import

> Un process spider est appelé
> depuis une ligne de commande (management/commands/crawl.py)
```
process = CrawlerProcess({
                        'FEED_FORMAT': 'json',
                        'FEED_URI': '../../apps/crawl/mathcollector/collections_eudml.json'
                    })

                    process.crawl(EudmlSpider, export_collections=True, url=url)
                    process.start()

   ```

cf doc scrapy
[Common Practices &mdash; Scrapy 2.11.0 documentation](https://docs.scrapy.org/en/latest/topics/practices.html?highlight=crawlerprocess#run-scrapy-from-a-script)


## Configuration serveur
[Supervisor](https://supervisor.readthedocs.io/en/stable/)

deux process sont a créer au niveau de /etc/supervisor/conf.d :
- crawl.conf
- restart_crawl.conf

```
[program:crawl]
directory=/var/www/gdml/current/sites/gdml
command=python3.11 manage.py crawl -eudml -username mathdoc
stderr_logfile=/var/log/sv.err.log
stdout_logfile=/var/log/sv.out.log
environment=PYTHONPATH=/var/www/gdml/current/venv/bin:%(ENV_PATH)s,PATH=/var/www/gdml/current/sites/gdml:%(ENV_PATH)s
user=deployer
autostart=false
autorestart=false

```
> restart_crawl.conf

pour le process restart_crawl: ajouter ``python3.11 manage.py crawl -eudml -username mathdoc -restart``
dans la commande en reprennant la configuration au dessus

Ensuite recharger les configurations :

> sudo supervisorctl reread


**Import d'une DML (EuDML)**

Un serveur virtuel comportant un client web est instancié depuis un conteneur (Splash).
Celui-ci permet de jouer du code javascript sur une page html, lors d'un process de moissonnage.

lancer le conteneur Splash

```
 docker run -p 8050:8050 --rm gricad-registry.univ-grenoble-alpes.fr/lesaulnc/splash nslookup gricad-registry.univ-grenoble-alpes.fr/lesaulnc/splash --max-timeout 360000
```

depuis un service:

 ```
  [Service]
  Type=simple
  User=deployer
  Group=docker

  ExecStart=/bin/sh -c 'docker run -p 8050:8050 --rm scrapinghub/splash nslookup scrapinghub/splash --max-timeout=36000'

  Restart=on-failure
  StartLimitIntervalSec=0
  RestartSec=5
  [Install]
  WantedBy=multi-user.target

```
[Splash](https://splash.readthedocs.io/en/stable/)

> Démarrage du process crawl :


Ensuite démarrer la commande crawl avec :
```
sudo supervisorctl start crawl
```

> redémarrage du process crawl :

```
sudo supervisorctl start restart_crawl
```

si la tâche est interrompue, vider le log dans la base Redis:

> se connecter au client avec le shell:
>  > redis-cli
>
>       SET start_crawl 0
>
>       SET progress 0





