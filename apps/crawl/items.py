# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

from scrapy_djangoitem import DjangoItem

from crawl.crawl import CollectionCrawler


class MathcollectorItem(DjangoItem):
    # define the fields for your item here like:
    # name = scrapy.Field()
    django_model = CollectionCrawler
