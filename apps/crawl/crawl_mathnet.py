import re
from html import unescape
from urllib.parse import urljoin

import regex
import requests
from alive_progress import alive_bar
from bs4 import BeautifulSoup
from pylatexenc.latex2text import LatexNodes2Text

from django.conf import settings
from django.utils import timezone

from crawl.crawl import CollectionCrawler
from gdml.models import Periode
from gdml.models import Source
from gdml.signals import update_years_periode
from ptf import model_data
from ptf import model_data_converter
from ptf import model_helpers
from ptf.cmds import ptf_cmds
from ptf.cmds import xml_cmds
from ptf.cmds.xml.xml_utils import escape
from ptf.cmds.xml.xml_utils import get_contrib_xml
from ptf.exceptions import ResourceDoesNotExist
from ptf.model_data import JournalData
from ptf.model_data import create_issuedata
from ptf.model_data import create_publisherdata
from ptf.models import Container
from ptf.models import Provider

from .mathcollector.items import Issue


class MathnetCrawler(CollectionCrawler):
    def __init__(self, **kwargs):
        print(kwargs)
        super().__init__(**kwargs)
        print("init ..............")
        print(kwargs)
        self.pid = kwargs.get("pid")
        print(self.pid)
        self.collection = model_helpers.get_collection(self.pid, sites=False)
        if not self.collection:
            print("no collection")
            raise ResourceDoesNotExist(f"Resource {self.pid} does not exist")

        self.domain = kwargs.get("domain", None)
        self.website = kwargs["website"]
        self.issue_href = kwargs.get("issue_href", None)
        self.article_href = kwargs.get("article_href", None)
        self.pdf_href = kwargs.get("pdf_href", None)
        self.periode = kwargs.get("periode", None)
        self.doi_href = kwargs.get("doi_href", None)
        self.periode_href = kwargs.get("periode_href", None)
        self.pre_publish = kwargs.get("pre_publish", None)
        self.create_xissue = kwargs.get("create_xissue", None)
        self.collection_href = kwargs.get("collection_href", None)
        self.container_added = ""
        self.issue_links = kwargs.get("issue_links", None)
        self.id = kwargs.get("id", None)
        self.numbers = kwargs.get("numbers", None)
        self.periode_range = kwargs.get("periode_range", None)
        self.container = None
        self.issue = None
        publisher_name = kwargs.get("publisher", None)
        self.source = kwargs.get("source", None)

        if publisher_name is None and self.source is not None:
            publisher_name = self.source.name

        publisher = model_helpers.get_publisher(publisher_name)
        if publisher is None:
            publisher = model_data.PublisherData()
            publisher.name = publisher_name
            publisher = ptf_cmds.addPublisherPtfCmd({"xobj": publisher}).do()

        self.publisher = publisher_name

        self.latext_parser = LatexNodes2Text()
        self.is_json = kwargs.get("json_parser", None)

    # custom_signals_app/signals.py

    def setResourcesLink(self, resourcesLink):
        resources_to = []
        reg_issue = re.compile(self.collection["website"] + self.collection["issue_href"])
        resources_to = [r for r in resourcesLink if reg_issue.match(r)]
        """for entry in resourcesLink:
            is_issue_link = match_issue_href(self.collection.website+self.collection['issue_href'], entry)
            if is_issue_link:
                resources_to.append(entry)"""
        # resources = iter(resources_to)
        yield iter(resources_to)

    def get_links_by_periods(self, soup, year=None, pattern=None, start=None):
        print("get links by periode")
        links = []

        if self.periode_range != "" and len(self.periode_range) >= 2:
            periods = list(range(self.periode_range[0], self.periode_range[1] + 1))

        links_url = []
        reg_digit = re.compile(r"\d+")
        reg_volume = re.compile(r"\d{1,3}-")
        reg_number = re.compile(r"-\d{1,2}")
        reg_year = re.compile(r"\d{4}")
        reg_http = re.compile("^http")
        links_match = []
        reg_number = re.compile(r"issue\=\d{1,2}")
        if start:
            number = reg_number.search(start)
            number = reg_digit.search(number)
            number = number[0]

        for period in periods:
            if self.periode_href == "" or self.periode_href == r"/(?P<year>\d{4})":
                reg_issue = re.compile(str(period) + self.issue_href)
            else:
                reg_issue = regex.compile(regex.escape(str(pattern)) + str(period))

            links_match = [
                unescape(e.get("href"))
                for e in soup.find_all("a")
                if reg_issue.search(unescape(e.get("href")))
            ]

            for link in links_match:
                link = self.website + link
                links_url.append(link)

            if len(periods) == 1:
                links_url = links_match

        print(links_url)

        for link in links_url:
            reg_volume = re.compile(r"volume\=\d{1,4}")
            reg_number = re.compile(r"issue\=\d{1,2}")

            year = reg_year.search(link)
            volume = reg_volume.search(link)
            number = reg_number.search(link)
            if year:
                year = year[0]

            if volume:
                volume = volume[0]
                volume = reg_digit.search(volume)
                volume = volume[0]
            if number:
                number = number[0]
                number = reg_digit.search(number)
                number = number[0]

            if year is not None and not re.search(r"\d", year):
                html = requests.get(link, verify=False, timeout=5.0).text
                soup = BeautifulSoup(html, "html.parser")
                year_bloc = [
                    e.get_text() for e in soup.find_all("h2") if re.search(volume, e.get_text())
                ]
                if len(year_bloc) > 0:
                    year_ext = re.search(r"\d{4}", year_bloc[0])
                    year = year_ext[0]
                else:
                    year = reg_year.search(link)
                    if year:
                        year = year[0]
            if self.create_xissue is False or self.create_xissue == "":
                issue = Container.objects.filter(
                    pid__startswith=self.pid + "_" + year + "__" + volume + "_" + number
                )
                count = issue.count()
                if count == 0:
                    links.append(link)
            else:
                links.append(link)
        return links

    def get_links_in_html(self, html, link_to_find, url_to_start=None, url_from=None):
        links = []

        soup = BeautifulSoup(html, "lxml")
        start = url_to_start is None
        pattern = None
        if url_to_start is not None:
            start = True
            print(url_to_start)

            try:
                url_part = url_to_start.split("/")
                year = url_part[3]

                volume_part = url_part[4].split("-")
                volume = volume_part[0]
                number = volume_part[1]
            except:
                pass

            r"""if not isinstance(list, self.periode):
                reg_volume=re.compile("\d+")
                reg_issue = re.compile("\d+")
                is_volume = reg_volume.match(volume)
                is_year= reg_issue.match(year)

                if is_volume is None:
                    reg_volume = re.compile("volume\=\d+")
                    volume = reg_volume.search(url_to_start)
                    if volume:
                        volume_part = volume[0].split('=')
                        volume = volume_part[1]
                        is_volume = True
                if is_year is None:
                    reg_year = re.compile("year\=\d+")
                    year = reg_year.search(url_to_start)
                    if year:
                        year_part = year[0].split('=')
                        year = year_part[1]
                        is_year = True

                if is_volume and is_year:
                    reg_issue = re.compile("year\="+year+"\&volume\="+volume+"\&issue\=\d")
                    links_to = [d.get("href") for d in soup.find_all('a') if reg_issue.search(str(d.get("href")))]
                    for link in links_to:
                        links.append(self.domain+link)
            """

        """for link in soup.find_all('a'):
            path = link.get('href')
            if path and path.find(link_to_find) == 0:
                path = urljoin(self.domain, path)
                if url_to_start and path == url_to_start:
                    start = True

                if start:
                    links.append(path)
        """

        # get period if no periode (object) provided - from url_start input or not
        if isinstance(self.periode, type(None)):
            source = Source.objects.get(domain=self.domain)
            reg_year = re.compile(r"\d{4}")
            if url_to_start is None and url_from is None:
                self.periode = Periode.objects.filter(collection=self.collection).first()
            else:
                """
                if start from year
                """
                url_from = url_to_start if url_from is None else url_from
                year_start = reg_year.search(url_from)
                if year_start:
                    self.periode = (
                        Periode.objects.filter(collection=self.collection, source=source)
                        .filter(begin__lte=year_start[0])
                        .first()
                    )

        if not isinstance(self.periode, type(None)) and isinstance(self.issue, type(None)):
            print(self.periode_range)
            if (
                not isinstance(self.periode_range, type(None))
                and self.periode_range != ""
                and len(self.periode_range) > 0
            ):
                if self.issue_links is None and (
                    link_to_find == "" or link_to_find == self.issue_href
                ):
                    if self.issue_href is not None:
                        if self.periode_href != "" and self.periode_href is not None:
                            pattern = self.periode_href
                        else:
                            pattern = self.issue_href

                return self.get_links_by_periods(soup, pattern=pattern)
        print(">>> no periode")
        links_node = soup.find_all("h2")
        # Get volumes infos
        reg1 = re.compile(r"\/(?P<year>\d{4})\/(?P<vol>\d{1,3})\-(?P<number>\d{1})\/p")
        reg2 = re.compile(r"\d+")
        reg_issue = regex.compile(self.issue_href)

        # parse links issue or article
        if link_to_find is not None and link_to_find != "":
            try:
                reg_item_url = regex.compile(link_to_find)
            except:
                reg_item_url = regex.compile(re.escape(link_to_find))
                print(reg_item_url)

            for link in links_node:
                try:
                    if link.find("a").get("href").find(link_to_find) == 0:
                        path = urljoin(self.website, link.find("a").get("href"))
                        if start:
                            links.append(path)
                except:
                    pass

            if len(links) == 0:
                links_pre = [
                    unescape(e.get("href"))
                    for e in soup.find_all("a")
                    if not isinstance(e.get("href"), type(None))
                    and reg_item_url.search(unescape(e.get("href")))
                ]
                print(links_pre)

                for link in links_pre:
                    if link.startswith("/"):
                        url_article = self.website + link
                    if not link.startswith("/") and not link.startswith("http"):
                        if url_from is not None:
                            url_article = url_from + link
                        else:
                            url_article = self.website + self.collection_href + "/" + link
                        if regex.search(regex.compile(self.issue_href), link) or regex.search(
                            regex.compile(self.article_href), link
                        ):
                            url_article = self.website + link

                    if link.startswith("/") and not link.startswith("http"):
                        if url_from is not None and len(url_article) == 0:
                            url_article = url_from + link
                        else:
                            if len(url_article) == 0:
                                url_article = self.website + self.collection_href + link
                    if link.startswith("http") or link.startswith("https"):
                        url_article = link
                    if start:
                        links.append(url_article)

        if len(links) == 0:
            # Set log if no links found
            for link in links_node:
                links_to = link.findChildren("a", recursive=True)

            if (len(links_node) > 0) and links_to:
                if len(links_node) == 1:
                    for link in links_node:
                        link_article = link.find("a")
                        if link_article:
                            if reg2.search(str(link_article.get("href"))):
                                links.append(link_article)
                else:
                    for child in links_node:
                        links_to = child.findChildren("a", recursive=True)
                        link = [
                            child_node["href"]
                            for child_node in links_to
                            if reg1.search(child_node["href"])
                        ]
                        if len(link) > 0:
                            if start:
                                links.append(link[0])

                if len(links) == 0 and link_to_find and link_to_find != "":
                    try:
                        reg_article_url = re.compile(link_to_find)
                    except:
                        reg_article_url = re.compile(re.escape(link_to_find))
                    if start:
                        if len(links_node) > 1 and hasattr(links_node, "find_all"):
                            links = [
                                e.get("href")
                                for e in links_node.find_all("a")
                                if reg_article_url.search(str(e.get("href")))
                            ]
                        else:
                            for link in links_node:
                                if reg_article_url.search(link.find("a").get("href")):
                                    links.append(link)

                if len(links) == 0 and link_to_find == "" or not link_to_find:
                    if start:
                        links = [
                            e.get("href")
                            for e in soup.find_all("a")
                            if reg1.search(str(e.get("href")))
                        ]

                if len(links) == 0:
                    match_link = reg1.search(link.find("a").get("href"))
                    if match_link:
                        url = link.find("a").get("href")
                        if url.startswith("/"):
                            url_article = self.collection["website"] + link.find("a").get("href")
                        else:
                            url_article = url
                        if start:
                            links.append(url_article)

        if len(links) == 0 and link_to_find == "" or not link_to_find:
            if start:
                all_links = [e.get("href") for e in soup.find_all("a")]
                links = [
                    e.get("href") for e in soup.find_all("a") if reg1.search(str(e.get("href")))
                ]

        # if no links, try to parse table HTML
        if len(links) == 0:
            table = soup.find_all("table")
            links = []

            reg = re.compile(re.escape(link_to_find))

            for child in table:
                links_to = child.findChildren("a", recursive=True)
                link_nodes = [
                    child_node.get("href")
                    for child_node in links_to
                    if reg.search(str(child_node.get("href")))
                ]
                if len(link_nodes) > 0:
                    links = link_nodes
            for index, link in enumerate(links):
                if link.startswith("/"):
                    if start:
                        links[index] = self.website + link
        print(".......")
        print(links)
        return links

    def setUrlSource(self, url):
        self.website = url

    """
        get links of collection called from process task
    """

    def get_collection_issues(self, url_to_start=None, periode_range=None):
        start = url_to_start is None
        print("get issues")
        if self.collection_href is not None:
            url = self.website + self.collection_href
            location = url
        else:
            url = self.website
            location = url

        html = requests.get(location, verify=False, timeout=30.0).content

        if periode_range is not None:
            self.periode_range = periode_range

        if url_to_start:
            html = requests.get(url_to_start, verify=False, timeout=5.0).text
        urls = self.get_links_in_html(html, self.issue_href, url_to_start)

        self.issue_links = urls
        return self.issue_links

    """
        Import collection called from cmd
    """

    def import_collection(self, url_to_start=None, augment=False, periode_range=None):
        start = url_to_start is None

        html = requests.get(
            self.website + "/" + self.collection_href, verify=False, timeout=30.0
        ).content

        if periode_range is not None and periode_range != "":
            self.periode_range = periode_range

        if url_to_start:
            html = requests.get(url_to_start, verify=False, timeout=5.0).text
        urls = self.get_links_in_html(html, self.issue_href, url_to_start)

        self.issue_links = urls

        with alive_bar(
            len(urls),
            dual_line=True,
            title=f"Init {self.pid} - YYYY XX YY",
            stats="(eta {eta})",
            force_tty=True,
        ) as bar:
            for index, url in enumerate(urls):
                self.import_issue(url, augment, bar)
                bar()

    def import_issue(self, issue_url, augment=False, bar=None, url_articles=None):
        print("====================")
        print("import issue")

        # check for acta
        if self.collection.coltype == "journal" or self.collection.coltype == "acta":
            xissue = model_data.IssueData()
            xissue.ctype = "issue"
        else:
            xissue = model_data.BookData()
            xissue.ctype = "book-monograph"
            xissue.articles = []
        provider = Provider.objects.filter(name="mathdoc")
        self.collection.provider = provider[0]
        xissue.provider = provider[0]
        xissue.journal = self.collection
        xpub = create_publisherdata()
        xpub.name = self.publisher
        xissue.publisher = xpub
        xissue.last_modified_iso_8601_date_str = timezone.now().isoformat()
        items = ""
        reg_year = re.compile(r"\d+")
        if issue_url is not None:
            items = issue_url.split("&")

        if self.issue is not None:
            if isinstance(self.issue, Issue):
                xissue.volume = self.issue.volume
                xissue.year = self.issue.year
                self.create_xissue = self.issue.create_xissue
                if not self.create_xissue:
                    xissue.number = self.issue.number
                xissue.pid = self.issue.pid
        else:
            if len(items) > 1:
                xissue.year = [item.split("=")[1] for item in items if item.find("year=") == 0][0]
                xissue.volume = [
                    item.split("=")[1] for item in items if item.find("volume=") == 0
                ][0]
                xissue.number = [item.split("=")[1] for item in items if item.find("issue=") == 0][
                    0
                ]

                if self.collection.coltype == "journal":
                    number_alt_l = [
                        item.split("=")[1] for item in items if item.find("issue_alt=") == 0
                    ]
                    if number_alt_l:
                        number_alt = number_alt_l[0]
                        number_alt = number_alt.replace("--", "-")
                        if number_alt and xissue.number != number_alt:
                            xissue.number = number_alt

            if xissue.volume is not None:
                xissue.volume = xissue.volume.replace("/", "-")

            if self.create_xissue is None:
                xissue.number = xissue.number.replace("/", "-")

            if xissue.volume is None or xissue.volume == "":
                reg_issue = regex.compile(self.issue_href)
                if reg_issue.search(issue_url):
                    volume_part = reg_issue.search(issue_url)
                    volume_part = volume_part[0]
                    volume = re.search(r"\d{1,3}[-_]", volume_part)
                    if volume:
                        volume = volume[0]
                        volume = re.search(r"\d{1,3}", volume)
                        xissue.volume = volume[0]
                    number = re.search(r"[_-]\d{1,2}", volume_part)
                    if number:
                        number = number[0]
                        number = re.search(r"\d{1,2}", number)
                        xissue.number = number[0]
        if xissue.pid is None:
            if xissue.year == "":
                if reg_year.search(issue_url):
                    xissue.year = reg_year.search(issue_url)[0]
            xissue.pid = f"{self.pid}_{xissue.year}__{xissue.volume}_{xissue.number}"
        self.container = xissue.pid
        if self.pid == "BASR" and int(xissue.year) < 1937:
            return

        issue = model_helpers.get_container(xissue.pid)

        if not augment and issue is not None:
            cmd = ptf_cmds.addContainerPtfCmd()
            cmd.set_object_to_be_deleted(issue)
            cmd.undo()

        if bar:
            bar.title = f"Init {self.pid} - {xissue.year} {xissue.volume} {xissue.number}"
        else:
            print(issue_url, xissue.year, xissue.volume, xissue.number)

        attempt = 0
        done = False
        while not done and attempt < 3:
            try:
                if url_articles is not None:
                    self.parse_issue(xissue, url_articles=url_articles)
                else:
                    self.parse_issue(xissue, issue_url, bar=bar)
                done = True
            except (requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout):
                attempt += 1
                xissue.articles = []

            # création d'un numéro non existant
            if self.create_xissue:
                issue = model_helpers.get_container(xissue.pid)

        if augment:
            for xarticle in xissue.articles:
                self.augment_article(xarticle)
        else:
            self.container = xissue.pid

            if not self.create_xissue:
                if self.collection.coltype == "journal":
                    params = {
                        "xissue": xissue,
                        "use_body": False,
                    }
                    cmd = xml_cmds.addIssueXmlCmd(params)
                    provider = Provider.objects.filter(name="mathdoc")
                    xissue.provider = provider[0]
                else:
                    xbook = xissue
                    data_col = model_data_converter.db_to_collection_data(self.collection)
                    data_col.volume = xissue.volume
                    data_col.collection = data_col
                    xbook.incollection = [data_col]

                    xbook.parts = xissue.articles
                    xbook.body = None

                    params = {"xbook": xbook, "use_body": False}
                    cmd = xml_cmds.addBookXmlCmd(params)

                issue = cmd.do()

            if self.id is not None or not isinstance(self.periode, type(None)):
                if isinstance(self.periode, type(None)):
                    periode = Periode.objects.filter(id=self.id)
                    periode = periode[0]
                else:
                    periode = self.periode
                year = xissue.year
                update_years_periode.send(sender=self.__class__, periode=periode, year=year)

            return issue

    def parse_issue(self, xissue, issue_url=None, url_articles=None, bar=None):
        seq = 1

        articles_list = []
        if url_articles is not None:
            articles_list = url_articles

        if isinstance(url_articles, type(None)):
            issue_html = requests.get(issue_url, timeout=30.0, verify=False).content
            soup = BeautifulSoup(issue_html, "lxml")
            reg_issue = re.compile(r"(Issue|Number) (?P<issue>\d{1,3})")
            reg_year = re.compile(r"\d{4}")
            year_url = ""
            if self.periode_href is not None:
                year_url = reg_year.search(issue_url)

            reg_volume_year = re.compile(r"(?P<year>\d{4}),\s+Volume \w+")
            reg_volume = re.compile(r"Volume (?P<vol>\d{1,3})")

            volume_infos = [
                e.get_text() for e in soup.find_all("p") if reg_volume.search(e.get_text())
            ]

            number = None
            year = None
            volume = None

            print("parse issue >>>>>")
            if xissue.year == "":
                if year_url != "" and year_url is not None:
                    year = year_url[0]
                if year:
                    xissue.year = year
                if year_url == "":
                    year_url = reg_year.search(issue_url)
                    if year_url:
                        part_year = year_url[0]
                        xissue.year = part_year
                if year_url and xissue.year == "":
                    year = year_url[0].split("/")
                    if len(year) > 1:
                        xissue.year = year[1]
            if xissue.volume is None or xissue.volume == "":
                if len(volume_infos) == 0:
                    # find volume in div
                    volume_infos = [
                        e.get_text(strip=True).replace("\xa0", " ")
                        for e in soup.find_all("div")
                        if reg_volume.match(e.get_text(strip=True).replace("\xa0", " "))
                    ]
                if len(volume_infos) == 0:
                    # find volume in font tag
                    volume_infos = [
                        e.get_text(strip=True).replace("\xa0", " ")
                        for e in soup.find_all("font")
                        if reg_volume_year.search(e.get_text(strip=True).replace("\xa0", " "))
                    ]

                if len(volume_infos) == 0:
                    volume_infos = [
                        e.get_text(strip=True).replace("\xa0", " ")
                        for e in soup.find_all("p")
                        if reg_volume.search(e.get_text(strip=True).replace("\xa0", " "))
                    ]

                if len(volume_infos) == 0:
                    reg_volume = re.compile(
                        r"\/(?P<year>\d{4})\/(?P<vol>\d{1,3})\-(?P<number>\d{1})\/p"
                    )
                    volume_infos = [
                        e.get_text(strip=True).replace("\xa0", " ")
                        for e in soup.find_all("font")
                        if reg_volume.search(e.get_text(strip=True).replace("\xa0", " "))
                    ]

                if len(volume_infos) == 0:
                    volume_infos = [
                        e.get_text(strip=True).replace("\xa0", " ")
                        for e in soup.find_all("h2")
                        if reg_volume.search(e.get_text(strip=True).replace("\xa0", " "))
                    ]

                if len(volume_infos) > 0:
                    if reg_volume.search(volume_infos[0]):
                        volume = reg_volume.search(volume_infos[0])[0]
                        volume = re.findall(r"\d+", volume)[0]
                    if reg_issue.search(volume_infos[0]):
                        text_serie = reg_issue.search(volume_infos[0])[0]
                        if text_serie:
                            number = re.compile(r"\d").search(text_serie)
                            number = number[0]

                    if xissue.number is None or xissue.number == "":
                        reg_number = re.compile(r"issue=(?P<number>\d+)")
                        test_number = reg_number.search(issue_url)
                        if test_number:
                            xissue.number = test_number.group("number")
                    if xissue.volume is None or xissue.volume == "":
                        xissue.volume = volume
                    if xissue.year is None or xissue.year == "":
                        xissue.year = year

                if xissue.volume is None or xissue.volume == "":
                    try:
                        volume_bloc = soup.find_all("font")
                        volume = [
                            e.get_text() for e in volume_bloc if reg_volume.search(e.get_text())
                        ]
                    except:
                        print("no volume found in element")
            xissue.pid = f"{self.pid}_{xissue.year}__{xissue.volume}_{xissue.number}"
            print(xissue.pid)
            print("parse volume, year")
            if xissue.year == "" and xissue.volume != "":
                year_bloc = [
                    e.get_text()
                    for e in soup.find_all("h2")
                    if re.search(xissue.volume, e.get_text())
                ]
                if len(year_bloc) > 0:
                    year_ext = re.search(r"\d{4}", year_bloc[0])
                    year = year_ext[0]
                    xissue.year = year

                    xissue.pid = f"{self.pid}_{xissue.year}__{xissue.volume}_{xissue.number}"

            print(xissue.pid)
            if isinstance(self.issue, type(None)):
                self.issue = issue_url
            articles_list = self.get_links_in_html(
                issue_html, self.article_href, url_from=issue_url
            )
            print(articles_list)

        for article_url in articles_list:
            xarticle = self.parse_article(article_url, xissue=xissue)

            if xarticle:
                if bar:
                    bar.text = article_url
                try:
                    if xarticle is not None and not self.create_xissue:
                        seq += 1
                        xarticle.seq = seq
                        xissue.articles.append(xarticle)
                        print(xissue.articles)

                        if self.collection.coltype != "journal" and hasattr(
                            xarticle, "book_title_html"
                        ):
                            xissue.title_html = xissue.title_text = xarticle.book_title_html
                            xissue.title_xml = f"<book-title-group><book-title>{escape(xissue.title_tex)}</book-title></book-title-group>"
                except Exception as e:
                    print("error appending articles")
                    print(e)

        if (
            self.collection.coltype != "journal"
            and self.collection.coltype != "acta"
            and len(xissue.articles) == 1
        ):
            # Mathnet.ru sometimes creates books without title and with only 1 chapter
            # Skip the useless empty level and create a simple book instead
            xarticle = xissue.articles[0]
            xissue.title_html = xissue.title_text = xarticle.title_tex
            xissue.title_xml = f"<book-title-group><book-title>{escape(xissue.title_tex)}</book-title></book-title-group>"
            xissue.contributors = xarticle.contributors
            xissue.doi = xarticle.doi
            xissue.pid = xarticle.pid
            xissue.ext_links = xarticle.ext_links
            xissue.extids = xarticle.extids
            xissue.streams = xarticle.streams
            xissue.articles = []
        if (
            self.collection.coltype != "journal"
            and self.collection.coltype != "acta"
            and len(xissue.articles) > 1
        ):
            xissue.ctype = "book-edited-book"
        provider = Provider.objects.filter(name="mathdoc")
        xissue.provider = provider[0]

    def parse_article(self, article_url, xissue=None):
        response = requests.get(article_url, timeout=30.0, verify=False).content
        soup = BeautifulSoup(response, "html.parser")
        meta = soup.find_all("meta")
        volume = ""
        xarticle = model_data.create_articledata()
        xabstract = None
        find_abstract = soup.find("article", {"id": "unit-article-abstract"})
        xarticle.abstracts = []
        if find_abstract:
            section = find_abstract.find("section")
            if section:
                abstract = section.get_text()
                xabstract = {}
                xabstract["tag"] = "abstract"
                xabstract["value_html"] = abstract
                xabstract["value_xml"] = xabstract["value_html"]
                xabstract["value_tex"] = xabstract["value_xml"]
                xabstract["lang"] = "eng"
                xarticle.abstracts.append(xabstract)
        reg_issue = regex.compile(r"\d{1,3}")
        reg_article = regex.compile(r"\d+")
        reg_abstract_url = regex.compile("p00$")

        if reg_abstract_url.search(article_url):
            return None
        author_match = ""
        titles = ""
        if meta is None:
            raise requests.exceptions.ConnectionError
        authors = []
        if self.create_xissue is True:
            # retourne le volume présent
            volume_bloc = soup.find("dt", text="Volume")
            if volume_bloc is not None:
                volume = volume_bloc.parent.find("dd").get_text()

            if volume is None or volume == "":
                node_infos_em = soup.find_all("em")
                reg_volume = regex.compile(r"\d{1,3}([\w]?)")

                reg_year = re.compile(r"\d{4}")
                reg_author = re.compile("(([A-Z][.]?[a-z]?))+")
                try:
                    xissue.volume = reg_volume.search(article_url)
                    if xissue.volume:
                        xissue.volume = xissue.volume[0]
                    xissue.year = [
                        e.get_text() for e in node_infos_em if reg_year.match(e.get_text())
                    ][0]
                    author_bloc = [
                        e.get_text() for e in node_infos_em if reg_author.match(e.get_text())
                    ]
                    if self.article_href is not None and self.article_href != "":
                        # adapt group num for specific source
                        for m in regex.finditer(self.article_href, article_url):
                            try:
                                num = m.group("num")
                                if num != "":
                                    xarticle.pid = (
                                        f"{self.pid}_{xissue.year}__{xissue.volume}_{num}"
                                    )
                            except:
                                pass
                    if node_infos_em:
                        title = soup.find("strong", text="Title")
                        if title:
                            title = title.find_next_sibling().get_text()
                            xarticle.title_html = title

                    if author_bloc and len(author_bloc) > 1:
                        author_match = author_bloc[1]

                except Exception:
                    pass
            reg_volume_year = re.compile(r"(?P<year>\d{4}),\s+Volume \w+")
            reg_volume = re.compile(r"Volume (?P<vol>\d{1,3})")

            # vérification de numéro avec volume
            if xissue.volume != "" and xissue.volume is not None:
                volume = xissue.volume

            xissue.pid = f"{self.pid}_{xissue.year}__{volume}"

            self.container = xissue.pid

            if self.issue is not None and isinstance(self.issue, Issue):
                volume = self.issue.volume
                if re.compile(r"^0\d{1}").match(volume):
                    volume = re.sub(r"[^\w\s]|(.)(?=\1)", "", volume)

                xissue.pid = self.issue.pid
                self.container = xissue.pid

                if self.issue.pid != "":
                    try:
                        xissue.number = self.issue.number

                    except:
                        pass

            issue = model_helpers.get_container(xissue.pid)

            if issue is None:
                print("creation numero")

                # fix hdml creation issue
                """
                    Création xissue pour un volume
                """
                year = xissue.year

                xissue = create_issuedata()
                if self.issue is not None and isinstance(self.issue, Issue):
                    xissue.volume = self.issue.volume
                    if re.compile(r"^0\d{1}").match(xissue.volume):
                        xissue.volume = re.sub(r"(0)(?=\1)", "", xissue.volume)

                    xissue.year = self.issue.year
                    xissue.journal = self.issue.journal
                    xissue.pid = self.issue.pid

                    try:
                        xissue.number = self.issue.number

                        if xissue.number is None:
                            xissue.pid = f"{self.pid}_{xissue.year}__{xissue.volume}"
                    except:
                        xissue.pid = f"{self.pid}_{xissue.year}__{xissue.volume}"

                else:
                    xissue.pid = f"{self.pid}_{year}__{volume}"
                    xissue.year = year
                    xissue.volume = volume
                    xissue.journal = self.collection

                self.container = xissue.pid
                xissue.ctype = "issue"
                xissue.abstracts = [
                    {"tag": "trans-abstract", "lang": "fr", "value_xml": "This is an abstract"}
                ]
                xissue.last_modified_iso_8601_date_str = timezone.now().isoformat()
                provider = Provider.objects.filter(name="mathdoc")

                xissue.provider = provider[0]
                xpub = create_publisherdata()
                xpub.name = self.publisher
                xissue.publisher = xpub

                if xissue.number is None or xissue.number == "":
                    xissue.number = 1

                params = {"xissue": xissue, "use_body": False}

                cmd = xml_cmds.addOrUpdateIssueXmlCmd(params)

                # debug

                issue = cmd.do()

            """
                get periode by periode_range input
            """
            if (
                self.periode_range is None
                or len(self.periode_range) == 0
                or self.periode_range[0] == 0
            ):
                periode = Periode.objects.filter(collection=self.collection)
            else:
                periode = Periode.objects.filter(
                    collection=self.collection, periode__contains=[self.periode_range[0]]
                )

            periode = periode.first()

            year = xissue.year
            update_years_periode.send(sender=self.__class__, periode=periode, year=year)

        title = ""
        titles = soup.find("h1")

        if titles and title == "":
            titles = titles.get_text().strip()
            title = titles

            if (xarticle.title_html is None or xarticle.title_html == "") and title == "":
                if soup.find("h1").find("a") is not None:
                    title = soup.find("h1").find("a").get_text()
            titles = soup.find("h1", {"class": "item-title"})

            if not isinstance(titles, type(None)):
                if titles.find("span"):
                    title_bloc = soup.find_all("h1", class_="item-title")[0]
                    title = title_bloc.find_all("span")[0].get_text()
        else:
            if title == "" and xarticle.title_html == "":
                title = soup.find("font", {"size": "+1"})
                if title is not None:
                    title = title.get_text()
                if title is None:
                    sup = soup.find_all("h2")
                    if len(sup) > 1:
                        title = sup[1].get_text()
        try:
            if title is not None and title != "" and xarticle.title_html == "":
                xarticle.title_html = title
        except:
            title = soup.find("h1").get_text(strip=True).replace("\xa0", " ")

        if title != "":
            xarticle.title_html = xarticle.title_tex = title
        else:
            xarticle.title_tex = xarticle.title_html
        xarticle.title_xml = f"<title-group><article-title>{escape(xarticle.title_tex)}</article-title></title-group>"

        if self.collection.coltype != "journal":
            xarticle.parts = []

        # Add article_url as en ExtLink (to display the link in the article page)
        ext_link = model_data.create_extlink()
        ext_link["rel"] = "full-text"
        ext_link["location"] = article_url
        xarticle.ext_links.append(ext_link)

        author_span = soup.find("span")
        author_section_3 = soup.find("h3")

        metadata_infos = soup.find_all("dl", class_="metadata-definition")

        reg_author = re.compile(r"(?:by|and)[\s]")
        reg_pages = re.compile(r"Pages[\s]")
        reg_pages2 = re.compile(r"\d{1,4}-\d{1,4}")

        identifier = None
        pubdate = None

        amsbib_div = soup.find("div", {"class": "showamsbib"})
        bib_div = [p for p in soup.find_all("p") if "@article" in p.text]

        pages_infos = [
            e.get_text(strip=True).replace("\xa0", " ")
            for e in soup.find_all("div")
            if reg_pages2.match(e.get_text(strip=True).replace("\xa0", " "))
        ]

        if len(pages_infos) > 0:
            infos_pagerange = pages_infos[0].split("-")
            xarticle.fpage = infos_pagerange[0].replace(",", "")
            xarticle.lpage = infos_pagerange[1].replace(",", "")
        if len(pages_infos) == 0:
            pages_infos = [
                e.get_text() for e in soup.find_all("b") if re.search(xissue.volume, e.get_text())
            ]

        has_doi = False
        zblid = None
        doi_translation = None
        pdf_url = None
        mthscinet = None
        mthnet = None
        eudml_id = None

        if amsbib_div or bib_div:
            if len(bib_div) > 0:
                bib_tex = bib_div[0].get_text()
                text = bib_tex.split("\t")
                reg_author = re.compile("author ")
                reg_pages = re.compile("page|pages")
                reg_lang = re.compile("language=")
                reg_title = re.compile("title")

                pages = ""
                if len(text) > 1:
                    for text_part in text:
                        if reg_pages.search(text_part):
                            pages = (
                                text_part.replace("{", "").replace("}", "").replace("pages = ", "")
                            )

                        if reg_lang.search(text_part):
                            xarticle.lang = (
                                text_part.replace("{", "")
                                .replace("}", "")
                                .replace("language = ", "")
                                .replace(",", "")
                            )
                            if len(xarticle.lang) > 3:
                                xarticle.lang = xarticle.lang[:-1]

                            if len(xarticle.lang) > 0 and xabstract is not None:
                                xabstract["lang"] = xarticle.lang

                        if reg_title.search(text_part):
                            if (
                                xarticle.title_html is None
                                or xarticle.title_html == ""
                                or xarticle.title_html == "Contents"
                            ):
                                xarticle.title_html = (
                                    text_part.replace("{", "")
                                    .replace("}", "")
                                    .replace("title = ", "")
                                    .replace(",", "")
                                )
                                xarticle.title_tex = xarticle.title_html
                                xarticle.title_xml = f"<title-group><article-title>{xarticle.title_html}</article-title></title-group>"
                    pages = pages.split("-")
                    xarticle.fpage = pages[0].replace(",", "")

                    if len(pages) > 1:
                        xarticle.lpage = pages[1].replace(",", "")
            else:
                amsbib = amsbib_div.get_text()
                start = amsbib.find("\\by ")
                if start > 0:
                    end = amsbib.find("\n", start)
                    authors = amsbib[start + 4 : end].split(",")

                start = amsbib.find("\\paper ")
                if start > 0:
                    end = amsbib.find("\n", start)
                    xarticle.title_html = xarticle.title_tex = amsbib[start + 7 : end]
                    xarticle.title_xml = f"<title-group><article-title>{escape(xarticle.title_tex)}</article-title></title-group>"

                start = amsbib.find("\\book ")
                if start > 0:
                    end = amsbib.find("\n", start)
                    xarticle.book_title_html = self.latext_parser.latex_to_text(
                        amsbib[start + 6 : end]
                    )

                start = amsbib.find("\\inbook ")
                if start > 0:
                    end = amsbib.find("\n", start)
                    xarticle.book_title_html = self.latext_parser.latex_to_text(
                        amsbib[start + 8 : end]
                    )

                start = amsbib.find("\\pages ")
                if start > 0:
                    end = amsbib.find("\n", start)
                    page_range = amsbib[start + 7 : end]
                    pages = page_range.split("--")
                    if len(pages) == 2:
                        xarticle.fpage = pages[0].replace(",", "")
                        xarticle.lpage = pages[1].replace(",", "")
                    else:
                        xarticle.page_range = page_range

                start = amsbib.find("\\mathscinet{")
                if start > 0:
                    end = amsbib.find("}", start)
                    mthscinet = amsbib[start:end]

                start = amsbib.find("\\mathnet{")
                if start > 0:
                    end = amsbib.find("}", start)
                    mthscinet = amsbib[start:end]

            for author_text in authors:
                if author_text != "":
                    author_text = self.latext_parser.latex_to_text(author_text)
                    author = model_data.create_contributor()
                    author["role"] = "author"
                    author["string_name"] = author_text.replace("\xa0", "")
                    author["string_name"] = (
                        author["string_name"].replace(",", "").replace("by", "")
                    )
                    author["contrib_xml"] = get_contrib_xml(author)
                    xarticle.contributors.append(author)

        if xarticle.fpage == "":
            pages_infos = soup.find("div", {"class": "entry-meta"})
            if pages_infos:
                pages_range = reg_pages.search(pages_infos.get_text())
                if pages_range:
                    infos_text = pages_infos.get_text()
                    pages_range = infos_text[12:18].strip()
                    pages_range = pages_range.split("-")
                    xarticle.fpage = pages_range[0].replace(",", "")
                    xarticle.lpage = pages_range[1].replace(",", "")
        if not xarticle.title_xml:
            xarticle.title_xml = "<title-group><article-title>TBD</article-title></title-group>"

        if identifier is not None:
            part_id = identifier.split("-")
            part_id = part_id[1]
            reg_pdf = re.compile("/download/" + identifier + "/" + part_id + ".pdf")

        try:
            reg_doi = re.compile("doi.org")
            doi = None
            reg_doi = re.compile(self.doi_href)

            if self.doi_href is not None and self.doi_href != "":
                doi = [
                    d.get("href")
                    for d in soup.find_all("a", {"class": "SLink"})
                    if reg_doi.search(str(d.get("href")))
                ]

            if doi:
                if len(doi) > 0:
                    doi = doi[0]

                    has_doi = True
                    doi = doi.split("doi.org/")
                    # strip unwanted chars present

                    doi = re.sub("}", "", doi[1])
                    doi = re.sub("\t", "", doi)

                    # si pre-print a enregistrer

        except TypeError as e:
            print(e)

        reg_pdf = re.compile("(.pdf)|(.PDF)")

        for link in soup.find_all("a"):
            path = link.get("href")
            if path is not None:
                if self.pdf_href and path.find(self.website + self.pdf_href) == 0:
                    pdf_url = urljoin(self.website, path)

                if self.pdf_href != "" and path.find(self.pdf_href) == 0 and reg_pdf.search(path):
                    pdf_url = urljoin(self.website, path)

                if self.pdf_href.startswith(r"\w+"):
                    reg_pdf_url = re.compile(self.pdf_href)

                if self.pdf_href and path.find(self.pdf_href) == 0:
                    pdf_url = urljoin(self.website, path)

                if self.pdf_href == "":
                    if reg_pdf.search(path):
                        if re.search("^[http,https]", path):
                            pdf_url = path
                        if not re.search("^[http,https]", path) and not path.startswith("/"):
                            pdf_url = urljoin(self.website, path)
                        else:
                            pdf_url = urljoin(self.website, path)
                        break

                if path.find("http://www.zentralblatt-math.org/zmath/") == 0:
                    text = path.split("?q=")[1]
                    zblid = text
                if path.find("https://zbmath.org/?q=an:") == 0:
                    if amsbib_div and amsbib_div.sourceline > link.sourceline:
                        text = path.split("?q=an:")[1]
                        if text.find("|") < 0 and text.find("%7") < 0:
                            zblid = text
                if path.find("https://doi.org/10.1515/") == 0:
                    if amsbib_div and amsbib_div.sourceline > link.sourceline:
                        doi_translation = path[len("https://doi.org/") :]
        if has_doi:
            xarticle.doi = doi.replace(" ", "")
            xarticle.ids.append(("doi", doi))
            xarticle.pid = xarticle.doi
            xarticle.pid = xarticle.pid.replace("pid", "").replace(":", "_")
        else:
            if zblid is not None:
                xarticle.pid = xissue.pid + "_" + zblid
                xarticle.pid = xarticle.pid.replace(".", "_").replace(":", "_")

            if mthscinet is not None:
                xarticle.pid = xissue.pid + "_" + mthscinet.split("/")[3]

            if mthnet is not None:
                xarticle.pid = xissue.pid + "_" + mthnet.split("?mr=")[1]

            if identifier is not None:
                identifier_part = identifier.split("-")
                identifier_article = identifier_part[1]
                if self.pid in settings.ISSUE_TO_APPEAR_PIDS:
                    pid = settings.ISSUE_TO_APPEAR_PIDS[self.pid]
                    xarticle.pid = xissue.pid + "_" + identifier_article
                if self.domain == "archive.org":
                    xarticle.pid = identifier

            if xarticle.pid is None:
                xarticle_pid = reg_article.findall(article_url)
                if len(xarticle_pid) > 0:
                    id_article = xarticle_pid[0]
                    xarticle.pid = xissue.pid + "_" + id_article
                    is_eudml_article = self.issue.pid.split("EUDML")
                    if len(is_eudml_article) > 0:
                        xarticle.pid = id_article

        if xarticle.pid is None:
            url_article_part = article_url.split("/")
            len_url_part = len(url_article_part)
            id_article = url_article_part[len_url_part - 1]
            xarticle.pid = xissue.pid + "_" + id_article

        if zblid is not None:
            xarticle.extids.append(("zbl-item-id", zblid))
        if doi_translation is not None:
            xarticle.extids.append(("doi-translation", doi_translation))
        if eudml_id is not None:
            xarticle.extids.append(("eudml_id", eudml_id))

        if pdf_url:
            data = {
                "rel": "full-text",
                "mimetype": "application/pdf",
                "location": pdf_url,
                "base": "",
                "text": "Full Text",
            }
            xarticle.streams.append(data)

            # The pdf url is already added as a stream (just above) but might be replaced by a file later on.
            # Keep the pdf url as an Extlink if we want to propose both option:
            # - direct download of a local PDF
            # - URL to the remote PDF
            ext_link = model_data.create_extlink()
            ext_link["rel"] = "article-pdf"
            ext_link["location"] = pdf_url
            xarticle.ext_links.append(ext_link)

        issue = model_helpers.get_container(xissue.pid)
        provider = Provider.objects.filter(name="mathdoc")
        xarticle.provider = provider[0]

        if len(xarticle.lang) > 3:
            xarticle.lang = xarticle.lang[:2]
        if len(xarticle.trans_lang) >= 3:
            xarticle.trans_lang = xarticle.trans_lang[:2]

        if issue is not None:
            if self.create_xissue:
                cmd = xml_cmds.addArticleXmlCmd(
                    {"issue": issue, "xarticle": xarticle, "use_body": False}
                )
                cmd.set_collection(issue.my_collection)
                cmd.set_provider(provider[0])
                cmd.do()
        return xarticle

    def import_singlearticle(self, articles):
        xarticles = []
        journal = JournalData()
        journal.issn = "0003486X"
        journal.publisher = "JSTOR"
        journal.e_issn = ""

        for article in articles:
            xarticle = self.parse_article(article)
            xarticles.append(xarticle)

        yield {"articles": xarticles}

    """
        Permet la modification d'article existant
    """

    def augment_article(self, xarticle):
        if not xarticle.pid and not xarticle.doi:
            raise ValueError("No id")
        pid = xarticle.pid
        if not pid:
            pid = xarticle.doi.replace("/", "_").replace(".", "_").replace("-", "_")

        article = model_helpers.get_article(pid)
        if not article:
            raise ValueError("No {pid}")

        article_data = model_data_converter.db_to_article_data(article)
        links = [item for item in article_data.ext_links if item["rel"] == "article-pdf"]
        if not links:
            links = [item for item in xarticle.ext_links if item["rel"] == "article-pdf"]
            if links:
                ext_link = links[0]
                article_data.ext_links.append(ext_link)

                params = {
                    "xarticle": article_data,
                    "use_body": False,
                    "issue": article.my_container,
                    "standalone": True,
                }

                cmd = xml_cmds.addArticleXmlCmd(params)
                cmd.set_collection(article.get_collection())
                cmd.do()


if __name__ == "__main__":
    kwargs = {
        "pid": "DA",
        "publisher": "Steklov Mathematical Institute",
        "domain": "http://www.mathnet.ru",
        "website": "http://www.mathnet.ru/php/archive.phtml?jrnid=dm&wshow=contents&option_lang=eng",
        "issue_href": "/php/archive.phtml?jrnid=dm&wshow=issue",
        "article_href": "/eng/",
    }
    crawler = MathnetCrawler(**kwargs)
    crawler.import_collection()
