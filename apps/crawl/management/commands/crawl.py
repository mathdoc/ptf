import os

import pypdf
import requests
from alive_progress import alive_bar
from scrapy.crawler import CrawlerProcess

from django.conf import settings
from django.core.management.base import BaseCommand

from crawl.crawl import CollectionCrawler
from crawl.crawl_annals_of_maths import AMCrawler
from crawl.crawl_hdml import HdmlCrawler
from crawl.crawl_mathnet import MathnetCrawler
from crawl.list import collections_to_crawl
from crawl.mathcollector.spiders.eudml_spider import EudmlSpider
from crawl.mathcollector.spiders.jstor_spider import JstorSpider
from crawl.utils import convert_collections_items_to_csv
from crawl.utils import convert_csv_json
from crawl.utils import export_json_csv
from crawl.utils import extract_pid_numdam
from crawl.zbmath import zbmath_request_article
from matching import crossref
from ptf import model_data_converter
from ptf.cmds import ptf_cmds
from ptf.cmds import xml_cmds
from ptf.display.resolver import get_disk_location
from ptf.display.resolver import get_relative_folder
from ptf.exceptions import ResourceDoesNotExist
from ptf.model_data import create_publicationdata
from ptf.model_helpers import get_article
from ptf.model_helpers import get_collection
from ptf.model_helpers import get_provider
from ptf.models import Article
from ptf.models import Container


class Command(BaseCommand):
    help = "Export a container or an article or a collection"
    links = []

    def add_arguments(self, parser):
        parser.add_argument(
            "-pid",
            action="store",
            dest="pid",
            type=str,
            default=None,
            required=False,
            help="Collection pid",
        )
        parser.add_argument(
            "-library",
            dest="library",
            action="store_true",
            default=False,
            help="Crawl libary to get collections",
        )
        parser.add_argument(
            "-convert_csv",
            dest="export_csv",
            action="store_true",
            default=False,
            help="convert json to csv",
        )
        parser.add_argument(
            "-convert_json",
            dest="convert_json",
            action="store_true",
            default=False,
            help="convert csv to json",
        )
        parser.add_argument(
            "-file",
            dest="file",
            action="store",
            default=None,
            help="convert csv to json",
        )
        parser.add_argument(
            "-extract_pid_numdam",
            dest="extract_pid_numdam",
            action="store_true",
            default=False,
            help="extract pid numdam into csv field",
        )
        parser.add_argument(
            "-convert_eudml_collections_csv",
            dest="convert_eudml_collections_csv",
            action="store_true",
            default=False,
            help="convert extracted items journals EUDML to csv",
        )
        parser.add_argument(
            "-prepare_collections",
            dest="prepare_collections",
            action="store_true",
            default=False,
            help="List collections to crawl in json file",
        )
        parser.add_argument(
            "-list_cols",
            action="store_true",
            dest="list_cols",
            default=False,
            help="Used with -eudml to get list of eudml collections",
        )
        parser.add_argument(
            "-eudml",
            dest="eudml",
            action="store_true",
            default=False,
            help="Import or list collections of eudml corpus",
        )
        parser.add_argument(
            "-jstor",
            dest="jstor",
            action="store_true",
            default=False,
            help="Import periode Annals of Maths on Jstor",
        )
        parser.add_argument(
            "-init",
            action="store_true",
            dest="init",
            default=False,
            help="Crawl the collection website (choice among -init/-doi/-pdf)",
        )
        parser.add_argument(
            "-doi",
            action="store_true",
            dest="doi",
            default=False,
            help="Update the articles from Crossref/zbMATH (choice among -init/-doi/-pdf)",
        )

        parser.add_argument(
            "-pdf",
            action="store_true",
            dest="pdf",
            default=False,
            help="Download the PDFs from the collection website (choice among -init/-doi/-pdf)",
        )
        parser.add_argument(
            "-issue_url",
            dest="issue_url",
            type=str,
            default=None,
            help="Issue url to crawling (-init just for some issues, can be used with -restart)",
        )

        parser.add_argument(
            "-article_pid",
            dest="article_pid",
            type=str,
            default=None,
            help="Article pid to update (with -doi) or pdf download (-pdf), can be used with -restart",
        )
        parser.add_argument(
            "-task_id",
            dest="task_id",
            type=str,
            default=None,
            help="Task id from the command is executed",
        ),
        parser.add_argument(
            "-periode",
            dest="periode",
            type=str,
            default=None,
            help="period of the import execution",
        )
        parser.add_argument(
            "-numbers",
            dest="numbers",
            type=str,
            default=None,
            help="numbers of the import execution",
        )
        parser.add_argument(
            "-username",
            dest="username",
            type=str,
            default=None,
            help="username",
        )
        parser.add_argument(
            "-issues_list",
            dest="issues_list",
            action="store_true",
            default=None,
            help="get list of issues from collection",
        ),
        parser.add_argument(
            "-restart",
            action="store_true",
            dest="restart",
            default=False,
            help="Used with -issue_url or -article_pid. If False, handle only 1 issue/article. If True, restart the process",
        ),
        parser.add_argument(
            "-file_out", dest="file_out", type=str, default=None, help="file dest to export csv"
        ),
        parser.add_argument(
            "-file_in", dest="file_in", type=str, default=None, help="file input to convert"
        ),
        parser.add_argument(
            "-parse_numdam_collections",
            dest="parse_numdam_collections",
            action="store_true",
            default=None,
            help="parse collections of numdam",
        ),
        parser.add_argument(
            "-periode_id", dest="periode_id", type=str, default=None, help="periode id"
        ),

    def update_article(self, article, bar=None):
        collection = article.get_collection()

        if bar:
            bar.text = article.pid
        else:
            print(article.pid)
        article_data = None
        existing_article_data = model_data_converter.db_to_article_data(article)

        # An article in Mathnet.ru may have a DOI and a DOI for the translation
        # Metadata in Crossref for the DOI are in Russian: use the translated version.
        # TODO: get both
        qs = article.extid_set.filter(id_type="doi-translation")
        if qs:
            doi_translation = qs.first().id_value
            print(f"    DOI Translation {doi_translation}")
            article_data = crossref.fetch_article(doi_translation)
        else:
            # No translation, use zbMATH
            qs = article.extid_set.filter(id_type="zbl-item-id")
            if qs:
                zblid = qs.first().id_value
                if "|" not in zblid and "%7" not in zblid:
                    attempt = 0
                    done = False
                    while not done and attempt < 3:
                        try:
                            article_data = zbmath_request_article(zblid)
                            done = True
                        except (
                            requests.exceptions.ConnectionError,
                            requests.exceptions.ReadTimeout,
                        ):
                            attempt += 1
            elif article.doi is not None:
                pass
                # Mathnet.ru DOI metadata are in Russian. Ignore and keep what was read in the article web page
                # article_data = crossref.fetch_article(article.doi)

        if article_data is not None:
            model_data_converter.update_data_for_jats(article_data)

            if not article_data.title_xml:
                print(f"   Warning: {article.pid} has no title_xml")
            else:
                # Preserve existing values not set by the Crossref/zbMATH API
                article_data.pid = article.pid
                article_data.doi = article.doi
                article_data.seq = existing_article_data.seq
                article_data.ids = existing_article_data.ids
                article_data.extids = existing_article_data.extids
                article_data.ext_links = existing_article_data.ext_links
                if not article_data.fpage:
                    article_data.fpage = existing_article_data.fpage
                if not article_data.lpage:
                    article_data.lpage = existing_article_data.lpage
                article_data.streams = existing_article_data.streams

                # Make sure the zbMATH/Crossref has found more metadata
                if not article_data.abstracts:
                    article_data.abstracts = existing_article_data.abstracts
                if not article_data.contributors:
                    print(f"   Warning: {article.pid} has no contributors")
                    article_data.contributors = existing_article_data.contributors
                if not article_data.title_html:
                    print(f"   Warning: {article.pid} has no title_html")
                    article_data.title_html = existing_article_data.title_html
                if not article_data.title_tex:
                    print(f"   Warning: {article.pid} has no title_tex")
                    article_data.title_tex = existing_article_data.title_tex

                params = {
                    "xarticle": article_data,
                    "use_body": False,
                    "issue": article.my_container,
                    "standalone": True,
                }
                cmd = xml_cmds.addArticleXmlCmd(params)
                cmd.set_collection(collection)
                article = cmd.do()
                if not article_data.title_html:
                    print(f"   Warning: {article.pid} has no title_html after XmlCmd")

    def download_pdf(self, article, bar=None):
        collection = article.get_collection()

        qs = article.datastream_set.filter(mimetype="application/pdf")
        if qs:
            datastream = qs.first()
            href = datastream.location
            embargo = False

            if hasattr(article, "my_container"):
                container_id = article.my_container.pid
                article_id = article.pid
            else:
                # Download a PDF of a book
                container_id = article.pid
                article_id = None

            if href.find("http") == 0:
                disk_location = get_disk_location(
                    settings.RESOURCES_ROOT,
                    collection.pid,
                    "pdf",
                    container_id=container_id,
                    article_id=article_id,
                    do_create_folder=True,
                )

                if not os.path.isfile(disk_location):
                    # Download only if not already present

                    attempt = 0
                    done = False
                    while not done and attempt < 3:
                        try:
                            r = requests.get(href, stream=True, timeout=10.0)
                            if len(r.text) > 10 and r.text[0:21] == "<!DOCTYPE html PUBLIC":
                                print(f"{article.doi} has an embargo, no PDF")
                                done = True
                                embargo = True
                            else:
                                temp_location = os.path.join(settings.TEMP_FOLDER, "file.pdf")
                                with open(temp_location, "wb") as f_:
                                    f_.write(r.content)

                                done = True

                                # Remove front page
                                pdf_reader = pypdf.PdfReader(temp_location)
                                pdf_writer = pypdf.PdfWriter()
                                for page in range(len(pdf_reader.pages)):
                                    current_page = pdf_reader.pages[page]
                                    if page > 0:
                                        pdf_writer.add_page(current_page)

                                with open(disk_location, "wb") as f_:
                                    pdf_writer.write(f_)

                        except (
                            requests.exceptions.ConnectionError,
                            requests.exceptions.ReadTimeout,
                            pypdf.errors.PdfReadError,
                        ):
                            attempt += 1

                    # if not embargo:
                    #     time.sleep(1)
            else:
                disk_location = get_disk_location(
                    settings.RESOURCES_ROOT,
                    collection.pid,
                    "pdf",
                    container_id=container_id,
                    article_id=article_id,
                    do_create_folder=False,
                )

            if os.path.isfile(disk_location):
                new_location = get_relative_folder(
                    collection.pid, container_id=container_id, article_id=article_id
                )
                pdf_filename = os.path.join(new_location, article.pid + ".pdf")
                if bar:
                    bar.text = disk_location
                if datastream.location != pdf_filename:
                    datastream.location = pdf_filename
                    datastream.save()
            elif not embargo:
                print(f"Error: unable to download {href}")

        else:
            print(f"No PDF link for {article.pid}")

    def handle_article_qs(self, pid, restart, article_pid, ftor, title):
        if restart or not article_pid:
            qs = Article.objects.filter(my_container__my_collection__pid=pid).order_by(
                "my_container__year",
                "my_container__volume_int",
                "my_container__number_int",
                "fpage",
            )
            if not qs:
                # No articles, try the books
                qs = Container.objects.filter(my_collection__pid=pid).order_by(
                    "year", "volume_int"
                )

            i = 0
            if article_pid is not None:
                for article in qs:
                    if article.pid == article_pid:
                        break
                    else:
                        i += 1

            qs = qs[i:]
            with alive_bar(len(qs), dual_line=True, title=title, stats="(eta {eta})") as bar:
                for article in qs:
                    ftor(article, bar)
                    bar()
        else:
            article = get_article(article_pid)
            if article:
                ftor(article)
            else:
                print(f"f{article_pid} not found")

    def handle(self, *args, **options):
        pid = options["pid"]
        article_pid = options["article_pid"]
        export_csv = options["export_csv"]
        file_out = options["file_out"]
        file_in = options["file_in"]

        init = options["init"]
        doi = options["doi"]
        pdf = options["pdf"]
        get_issues = options["issues_list"]
        restart = options["restart"]
        periode = options["periode"]
        eudml = options["eudml"]
        prepare_collections = options["prepare_collections"]
        jstor = options["jstor"]
        username = options["username"]
        numdam_pid_extract = options["extract_pid_numdam"]
        eudml_collection_to_csv = options["convert_eudml_collections_csv"]
        convert_json = options["convert_json"]

        if convert_json:
            convert_csv_json()

        if eudml_collection_to_csv is not False:
            convert_collections_items_to_csv()

        if numdam_pid_extract is not False:
            extract_pid_numdam()

        if periode is not None:
            periode = list(periode.split(","))
            periode = [eval(i) for i in periode]

        if export_csv is not None and export_csv is not False:
            export_json_csv(file_in=file_in, file_out=file_out)

        if pid is not None:
            xcollection = collections_to_crawl[pid]

            collection = get_collection(xcollection["pid"], sites=False)

            if init:
                for issue in collection.content.all():
                    cmd = ptf_cmds.addContainerPtfCmd({"pid": issue.pid, "ctype": issue.ctype})
                    cmd.set_provider(provider=issue.provider)
                    cmd.add_collection(collection)
                    cmd.set_object_to_be_deleted(issue)
                    cmd.undo()

            if not collection:
                p = get_provider("mathdoc-id")
                xcol = create_publicationdata()
                xcol.coltype = "journal"
                xcol.pid = pid
                xcol.title_tex = xcollection["title"]

                xcol.title_html = xcollection["title"]
                xcol.title_tex = xcollection["title"]
                xcol.trans_title_tex = xcollection["title"]
                xcol.trans_title_html = xcollection["title"]

                xcol.title_xml = (
                    f"<title-group><title>{xcollection['title']}</title></title-group>"
                )
                xcol.lang = "en"
                xcol.trans_lang = "en"
                xcol.abbrev = ""

                cmd = ptf_cmds.addCollectionPtfCmd({"xobj": xcol})
                cmd.set_provider(p)
                collection = cmd.do()

            if not collection:
                raise ResourceDoesNotExist(f"Resource {pid} does not exist")

            if pid == "AM" or pid == "EMISAM":
                crawler = AMCrawler(**xcollection)
                crawler.import_collection()

            elif pid == "DM" or pid == "MBB":
                crawler = MathnetCrawler(**xcollection)
                crawler.import_collection()

            elif pid in ["EU", "EUA", "DEME"]:
                # if xcollection["numbers"] is not None:
                # set id for targeted periode
                crawler = HdmlCrawler(**xcollection)
                crawler.import_collection()

            else:
                crawler = CollectionCrawler(**xcollection)
                crawler.import_collection(periode_range=xcollection["periode_range"])

        if prepare_collections:
            url = "https://eudml.org/journals"
            if eudml:
                if init:
                    process = CrawlerProcess(
                        {
                            "FEED_FORMAT": "json",
                            "FEED_URI": settings.CRAWL_JSON_FEED,
                        }
                    )
                    process.crawl(EudmlSpider, export_collections=True, url=url)
                else:
                    process = CrawlerProcess()
                    crawler = process.create_crawler(EudmlSpider)
                    process.crawl(
                        crawler,
                        export_collections=False,
                        prepare_collections=True,
                        username="mathdoc",
                        url=url,
                    )
                process.start()
        if jstor:
            process = CrawlerProcess()
            crawler = process.create_crawler(JstorSpider)
            process.crawl(crawler, username="mathdoc")
            process.start()

        if eudml and not prepare_collections:
            process = CrawlerProcess(
                {
                    "FEEDS": {
                        settings.CRAWL_JSON_FEED: {
                            "format": "json",
                        }
                    }
                }
            )
            # crawler = process.create_crawler(EudmlSpider)
            if restart:
                process.crawl(EudmlSpider, url=None, username=username, restart=True)
            else:
                process.crawl(EudmlSpider, url=None, username=username)
            process.start()

        if get_issues:
            crawler = CollectionCrawler(**xcollection)
            crawler.get_collection_issues(periode=periode)
        if doi:
            self.handle_article_qs(
                pid=pid,
                restart=restart,
                article_pid=article_pid,
                ftor=self.update_article,
                title=f"Update {pid}",
            )
        if pdf:
            self.handle_article_qs(
                pid=pid,
                restart=restart,
                article_pid=article_pid,
                ftor=self.download_pdf,
                title=f"Download PDF for {pid}",
            )
