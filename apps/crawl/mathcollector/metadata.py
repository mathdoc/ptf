from crawl.mathcollector.settings import BOT_NAME


class SingletonMetaclass(type):
    _instance = None

    def __call__(cls, *args, **kwargs):
        assert not (args or kwargs), "Singleton should not accept arguments"

        if isinstance(cls._instance, cls):
            return cls._instance
        else:
            cls._instance = super().__call__()
            return cls._instance


class MetadataImport(metaclass=SingletonMetaclass):
    def __init__(self):
        self.configstring = BOT_NAME
        self.issues = []
        self.imported = True

    def getName(self):
        return self.configstring

    def getImportedStatus(self):
        return self.imported

    def getIssues(self):
        return self.issues

    def setIssues(self, issues):
        self.issues = issues

    def resetImport(self):
        self.imported = True

    def setImport(self):
        self.imported = False
