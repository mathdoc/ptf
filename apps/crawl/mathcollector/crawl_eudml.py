import logging
import logging.handlers
import re
from ssl import SSLError

import regex
import requests
from bs4 import BeautifulSoup
from middleware.redis_command import redisClient
from pylatexenc.latex2text import LatexNodes2Text

from django.contrib.auth.models import User
from django.utils import timezone

from crawl.mathcollector.items import Issue
from gdml.models import Periode
from gdml.signals import update_years_periode
from history.models import HistoryEvent
from history.models import insert_history_event
from history.views import manage_exceptions
from ptf import model_data
from ptf import model_data_converter
from ptf import model_helpers
from ptf.cmds import ptf_cmds
from ptf.cmds import xml_cmds
from ptf.cmds.xml.xml_utils import escape
from ptf.cmds.xml.xml_utils import get_contrib_xml
from ptf.exceptions import ResourceDoesNotExist
from ptf.model_data import JournalData
from ptf.model_data import create_issuedata
from ptf.model_data import create_publisherdata
from ptf.model_data import create_subj
from ptf.model_helpers import get_container
from ptf.models import Collection
from ptf.models import Container
from ptf.models import Provider


class EudmlCrawler:
    def __init__(self, *args, **kwargs):
        self.pid = kwargs["pid"]
        self.collection = model_helpers.get_collection(self.pid, sites=False)
        if not self.collection:
            raise ResourceDoesNotExist(f"Resource {self.pid} does not exist")

        self.domain = kwargs.get("domain", None)
        self.website = kwargs["website"]
        self.issue_href = kwargs.get("issue_href", None)
        self.article_href = kwargs.get("article_href", None)
        self.pdf_href = kwargs.get("pdf_href", None)
        self.periode = kwargs.get("periode", None)
        self.numbers = kwargs.get("numbers", None)
        self.username = kwargs.get("username", None)
        self.doi_href = kwargs.get("doi_href", None)
        self.periode_href = kwargs.get("periode_href", None)
        self.pre_publish = kwargs.get("pre_publish", None)
        self.create_xissue = kwargs.get("create_xissue", None)
        self.collection_href = kwargs.get("collection_href", None)
        self.container_added = ""
        self.issue_links = kwargs.get("issue_links", None)
        self.id = kwargs.get("id", None)
        self.container = None
        self.issue = None
        self.parent_event = kwargs.get("parent_event", None)
        publisher_name = kwargs.get("publisher", None)
        self.source = kwargs.get("source", None)
        if publisher_name is None and self.source is not None:
            publisher_name = self.source.name
        self.provider = Provider.objects.filter(name="mathdoc").first()
        publisher = model_helpers.get_publisher(publisher_name)
        if publisher is None:
            publisher = model_data.PublisherData()
            publisher.name = publisher_name
            publisher = ptf_cmds.addPublisherPtfCmd({"xobj": publisher}).do()

        self.publisher = publisher_name
        self.latext_parser = LatexNodes2Text()
        self.is_json = kwargs.get("json_parser", None)
        self.redis_client = redisClient()
        self.issue_errors = []

    @property
    def logger(self):
        return logging.getLogger("eudml_spider")

    """
        Import collection called from cmd
    """

    def import_issue(self, issue_url, augment=False, bar=None, url_articles=None):
        # self.has_error = [dict()]
        # check for acta
        if self.collection.coltype == "journal" or self.collection.coltype == "acta":
            xissue = model_data.IssueData()
            xissue.ctype = "issue"

        """
        else:
            xissue = model_data.BookData()
            xissue.ctype = "book-monograph"
            xissue.articles = []
        """
        provider = Provider.objects.filter(name="mathdoc")
        self.collection.provider = provider[0]
        xissue.provider = provider[0]
        xissue.journal = self.collection
        xpub = create_publisherdata()
        xpub.name = self.publisher
        xissue.publisher = xpub
        xissue.last_modified_iso_8601_date_str = timezone.now().isoformat()
        items = ""
        seq = None
        if issue_url is not None:
            items = issue_url.split("&")

        if self.issue is not None:
            if isinstance(self.issue, Issue):
                xissue.volume = self.issue.volume
                xissue.year = self.issue.year
                self.create_xissue = self.issue.create_xissue
                if not self.create_xissue:
                    xissue.number = self.issue.number
                xissue.pid = self.issue.pid
                issue = model_helpers.get_container(xissue.pid)
                """
                    if previous issue is a part of issue
                """
                if issue is not None and not augment:
                    seq = issue.article_set.all().count()

        else:
            if len(items) > 1:
                xissue.year = [item.split("=")[1] for item in items if item.find("year=") == 0][0]
                xissue.volume = [
                    item.split("=")[1] for item in items if item.find("volume=") == 0
                ][0]
                xissue.number = [item.split("=")[1] for item in items if item.find("issue=") == 0][
                    0
                ]

                if self.collection.coltype == "journal":
                    number_alt_l = [
                        item.split("=")[1] for item in items if item.find("issue_alt=") == 0
                    ]
                    if number_alt_l:
                        number_alt = number_alt_l[0]
                        number_alt = number_alt.replace("--", "-")
                        if number_alt and xissue.number != number_alt:
                            xissue.number = number_alt

            if xissue.volume is not None:
                xissue.volume = xissue.volume.replace("/", "-")

            if self.create_xissue is None:
                xissue.number = xissue.number.replace("/", "-")

            if xissue.volume is None or xissue.volume == "":
                reg_issue = regex.compile(self.issue_href)
                if reg_issue.search(issue_url):
                    volume_part = reg_issue.search(issue_url)
                    volume_part = volume_part[0]
                    volume = re.search(r"\d{1,3}[-_]", volume_part)
                    if volume:
                        volume = volume[0]
                        volume = re.search(r"\d{1,3}", volume)
                        xissue.volume = volume[0]
                    number = re.search(r"[_-]\d{1,2}", volume_part)
                    if number:
                        number = number[0]
                        number = re.search(r"\d{1,2}", number)
                        xissue.number = number[0]
        if xissue.pid is None:
            xissue.pid = f"{self.pid}_{xissue.year}__{xissue.volume}_{xissue.number}"
        self.container = xissue.pid

        collection = model_helpers.get_collection(pid=self.pid, sites=False)
        prev_issues = Container.objects.filter(volume=xissue.volume, my_collection=collection)

        regex_year = re.compile(r"\d+-\d+")
        if len(prev_issues) > 0:
            prev_issues = prev_issues.filter(year__gt="00")

            for issue in prev_issues:
                if not regex_year.search(issue.year):
                    if int(issue.year) > 0:
                        if str(xissue.year) not in str(issue.year):
                            issue.year = str(issue.year) + "-" + xissue.year
                            issue.save()

            if len(prev_issues) > 0:
                for issue in prev_issues:
                    if regex_year.search(issue.year):
                        xissue.year = issue.year

        if bar:
            bar.title = f"Init {self.pid} - {xissue.year} {xissue.volume} {xissue.number}"

        attempt = 0
        done = False
        user = User.objects.get(username=self.username)
        while not done and attempt < 3:
            try:
                if url_articles is not None:
                    self.parse_issue(xissue, url_articles=url_articles, seq=seq)

                done = True
            except SSLError:
                attempt += 1

            except (requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout):
                attempt += 1

            except Exception as ex:
                type_error = "erreur interne au processus"
                self.insert_exception(user, type_error, ex, xissue.pid)

        if attempt == 3:
            type_error = "Une erreur réseau est survenue"
            self.insert_exception(
                user, type_error, requests.exceptions.ConnectionError, xissue.pid
            )
            # add history Event for max retries

        xissue.articles = []

        issue = model_helpers.get_container(xissue.pid)

        """
        if augment:
            for xarticle in xissue.articles:
                self.augment_article(xarticle)
        else:
        """
        self.container = xissue.pid

        if self.id is not None:
            periode = Periode.objects.filter(id=self.id)
            periode = periode[0]

            year = xissue.year
            update_years_periode.send(sender=self.__class__, periode=periode, year=year)

        return issue

    def parse_issue(self, xissue, issue_url=None, url_articles=None, seq=None, bar=None):
        # for specific type of collection
        # if self.collection.coltype != "journal" and self.collection != "acta":

        if seq is not None:
            seq = seq + 1
        else:
            seq = 1

        user = User.objects.get(username=self.username)

        for article_url in url_articles:
            try:
                xarticle = self.parse_article(article_url, xissue=xissue, seq=seq)
                if xarticle:
                    if bar:
                        bar.text = article_url
                    seq += 1

            except Exception as ex:
                self.insert_exception(user, "erreur interne", ex, xissue.pid)
        # insert history event success
        insert_history_event(
            {
                "type": "import",
                "pid": xissue.pid,
                "col": self.pid,
                "parent_id": self.parent_event,
                "status": "OK",
                "title": self.collection.title_html,
                "type_error": "",
                "userid": user.id,
                "data": {
                    "ids_count": 0,
                    "message": "",
                    "target": xissue.pid,
                },
            }
        )

        """
        if (
            self.collection.coltype != "journal"
            and self.collection.coltype != "acta"
            and len(xissue.articles) == 1
        ):
            # Mathnet.ru sometimes creates books without title and with only 1 chapter
            # Skip the useless empty level and create a simple book instead
            xarticle = xissue.articles[0]
            xissue.title_html = xissue.title_text = xarticle.title_tex
            xissue.title_xml = f"<book-title-group><book-title>{escape(xissue.title_tex)}</book-title></book-title-group>"
            xissue.contributors = xarticle.contributors
            xissue.doi = xarticle.doi
            xissue.pid = xarticle.pid
            xissue.ext_links = xarticle.ext_links
            xissue.extids = xarticle.extids
            xissue.streams = xarticle.streams
            xissue.articles = []

        if (
            self.collection.coltype != "journal"
            and self.collection.coltype != "acta"
            and len(xissue.articles) > 1
        ):
            xissue.ctype = "book-edited-book"
         """
        provider = Provider.objects.filter(name="mathdoc")
        xissue.provider = provider[0]

    def parse_article(self, article_url, xissue=None, seq=None):
        response = requests.get(article_url, timeout=30.0, verify=False).content
        soup = BeautifulSoup(response, "html.parser")
        xarticle = model_data.create_articledata()
        xabstract = None
        find_abstract = soup.find("article", {"id": "unit-article-abstract"})
        xarticle.abstracts = []
        subjects = []
        subj_part = soup.find("article", {"id": "unit-subject-areas"})
        reg_msc = re.compile("/subject/MSC/[a-zA-Z0-9.]+")
        subjs = [a for a in subj_part.find_all("a") if reg_msc.search(a.get("href"))]
        reg_digit = re.compile(r"\d+")
        for subj in subjs:
            type_class = subj.get("href").split("/")
            subject = create_subj()
            subject["value"] = type_class[3]
            subject["type"] = "msc"
            subject["lang"] = "en"
            subjects.append(subject)

        xarticle.kwds = subjects

        if find_abstract:
            section = find_abstract.find("section")
            if section:
                abstract = section.get_text()
                xabstract = {}
                xabstract["tag"] = "abstract"
                xabstract["value_html"] = abstract
                xabstract["value_xml"] = xabstract["value_html"]
                xabstract["value_tex"] = xabstract["value_xml"]
                xabstract["lang"] = "eng"
                xarticle.abstracts.append(xabstract)
        reg_article = regex.compile(r"\d+")

        reg_abstract_url = regex.compile("p00$")
        if reg_abstract_url.search(article_url):
            # set_log
            return None

        authors = []

        xissue.pid = self.issue.pid
        authors_bloc = soup.find("p", {"class": "sub-title-1"})
        if authors_bloc:
            authors_node = authors_bloc.find_all("a")
            for author in authors_node:
                text_author = author.get_text()
                name_author = text_author.replace(",", "")
                authors.append(name_author)

        self.container = xissue.pid

        if re.compile(r"^0\d{1}").match(self.issue.volume):
            self.issue.volume = self.issue.volume[1:]

        self.container = xissue.pid

        if self.issue.pid != "":
            try:
                xissue.number = self.issue.number

            except Exception:
                pass

        collection = Collection.objects.filter(pid=self.pid)
        prev_issue = Container.objects.filter(
            volume=xissue.volume, my_collection=collection[0], number=xissue.number
        )

        if prev_issue.count() > 0:
            if str(xissue.year) != "00" and str(xissue.year) != "0":
                issue = prev_issue.first()

        else:
            if xissue.number is None:
                xissue.pid = f"{self.pid}_{xissue.year}__{xissue.volume}"
                xissue.number = ""
            else:
                # if twice year - extract year of previous issue
                reg_periode = re.compile(r"\d+-\d+")

                if prev_issue.count() > 0:
                    if reg_periode.search(xissue.year) and not reg_periode.search(
                        prev_issue.first().pid
                    ):
                        year = xissue.year.split("-")
                        year = year[1]
                else:
                    year = xissue.year

                xissue.pid = f"{self.pid}_{year}__{xissue.volume}_{xissue.number}"
            issue = get_container(xissue.pid, sites=False)

        if issue is None:
            """
            Création issue pour un volume
            depuis une instance Issue
            """

            year = xissue.year
            xissue = create_issuedata()
            xissue.volume = self.issue.volume

            xissue.journal = self.issue.journal
            xissue.year = year
            try:
                xissue.number = self.issue.number

                reg_periode = re.compile(r"\d+-\d+")
                if xissue.number is None:
                    xissue.pid = f"{self.pid}_{xissue.year}__{xissue.volume}"
                    xissue.number = ""
                else:
                    # if twice year - extract year of previous issue
                    if prev_issue.count() > 0:
                        if reg_periode.search(xissue.year) and not reg_periode.search(
                            prev_issue.first().pid
                        ):
                            year = xissue.year.split("-")
                            year = year[1]
                    else:
                        year = xissue.year

                    xissue.pid = f"{self.pid}_{year}__{xissue.volume}_{xissue.number}"

            except Exception:
                pass

            self.container = xissue.pid
            xissue.ctype = "issue"
            """
            xissue.abstracts = [
                {"tag": "trans-abstract", "lang": "fr", "value_xml": "This is an abstract"}
            ]
            """
            xissue.last_modified_iso_8601_date_str = timezone.now().isoformat()
            xissue.provider = self.provider
            """
            set publisher
            """
            section_oai = soup.find("h3", text="In Other Databases").parent
            section_oai = section_oai.find_all("dd")
            if section_oai is not None:
                pub = [
                    d.text
                    for d in section_oai
                    if d.text.strip() not in ["DOI", "ZBMath", "MathSciNet", "PUBLISHER"]
                ]
                if pub != "":
                    xpub = create_publisherdata()
                    xpub.name = pub[0].strip()
                    xissue.publisher = xpub
            xissue.journal = self.collection

            params = {"xissue": xissue, "use_body": False, "sites": False}

            cmd = xml_cmds.addOrUpdateIssueXmlCmd(params)
            # debug
            issue = cmd.do()

            periode = Periode.objects.filter(collection=self.collection)
            periode = periode[0]
            year = xissue.year
            update_years_periode.send(sender=self.__class__, periode=periode, year=year)

        try:
            title = soup.find("h1").get_text(strip=True).replace("\xa0", " ")
            xarticle.title_html = title.replace("\xa0", " ").replace("\n", "")
        except:
            pass

        if title != "":
            xarticle.title_html = xarticle.title_tex = title
            xarticle.title_tex = xarticle.title_html
        xarticle.title_xml = f"<title-group><article-title>{escape(xarticle.title_tex)}</article-title></title-group>"

        if self.collection.coltype != "journal":
            xarticle.parts = []

        # Add article_url as en ExtLink (to display the link in the article page)
        ext_link = model_data.create_extlink()
        ext_link["rel"] = "article"
        ext_link["location"] = article_url
        xarticle.ext_links.append(ext_link)

        bib_div = [p for p in soup.find_all("p") if "@article" in p.text]

        has_doi = False
        zblid = None
        doi_translation = None
        mthscinet = None

        if len(bib_div) > 0:
            bib_tex = bib_div[0].get_text()
            text = bib_tex.split("\t")
            reg_author = re.compile("author =")
            reg_lang = re.compile("language = ")
            reg_title = re.compile("title")
            reg_pages = re.compile("pages =")
            reg_id = re.compile(r"/doc/\d+")
            if reg_id.search(article_url):
                article_id = reg_id.search(article_url)[0]
                article_id = article_id.split("/doc/")
                article_id = article_id[1]
            if len(text) > 1:
                for text_part in text:
                    if reg_author.search(text_part):
                        authors_text = (
                            text_part.replace("{", "").replace("}", "").replace("author = ", "")
                        )
                        authors_bib = authors_text.split(",")
                        authors = []
                        for index, name in enumerate(authors_bib):
                            if index % 2 == 1:
                                author = authors_bib[index - 1] + " " + authors_bib[index]
                                authors.append(author)

                    if reg_lang.search(text_part):
                        xarticle.lang = (
                            text_part.replace("{", "")
                            .replace("}", "")
                            .replace("language = ", "")
                            .replace(",", "")
                        )
                        if len(xarticle.lang) >= 3:
                            xarticle.lang = xarticle.lang[:-1]

                        if len(xarticle.lang) > 0 and xabstract is not None:
                            xabstract["lang"] = xarticle.lang

                    if reg_pages.search(text_part):
                        pages = (
                            text_part.replace("{", "")
                            .replace("}", "")
                            .replace("(", "")
                            .replace(")", "")
                            .replace("[", "")
                            .replace("]", "")
                            .replace("pages = ", "")
                        )
                        if len(pages) > 0 and pages != "null":
                            pages = pages.split(",")
                            if re.compile(r"\d+-\d+").search(pages[0]):
                                pages = pages[0].split("-")
                                xarticle.fpage = pages[0]
                                if len(pages) > 1:
                                    if re.search(reg_digit, str(pages[1])):
                                        pages[1] = re.search(reg_digit, str(pages[1]))[0]
                                    xarticle.lpage = pages[1]
                                xarticle.page_range = pages[0] + "-" + pages[1]

                    if reg_title.search(text_part):
                        if (
                            xarticle.title_html is None
                            or xarticle.title_html == ""
                            or xarticle.title_html == "Contents"
                        ):
                            xarticle.title_html = (
                                text_part.replace("{", "")
                                .replace("}", "")
                                .replace("title = ", "")
                                .replace(",", "")
                            )
                            xarticle.title_tex = xarticle.title_html
                            xarticle.title_xml = f"<title-group><article-title>{xarticle.title_html}</article-title></title-group>"

            for author_text in authors:
                author_text = self.latext_parser.latex_to_text(author_text)
                author = model_data.create_contributor()
                author["role"] = "author"
                if author_text != "":
                    author["string_name"] = author_text.replace("\xa0", "")
                    author["contrib_xml"] = get_contrib_xml(author)

                    if author not in xarticle.contributors:
                        xarticle.contributors.append(author)

        try:
            reg_doi = re.compile("doi.org")
            doi = None

            doi_link = soup.find("article", {"id": "unit-other-ids"})
            doi = [
                d.get("href") for d in doi_link.find_all("a") if reg_doi.search(str(d.get("href")))
            ]
            if doi:
                if len(doi) > 0:
                    if len(doi) > 1:
                        start_dois = len(doi) - 1
                        doi = doi[start_dois:][0]
                    else:
                        doi = doi[0]

                    has_doi = True
                    doi = doi.split("doi.org/")
                    # strip unwanted chars present
                    if len(doi) > 1:
                        doi = doi[1].encode("ascii", "ignore")
                        doi = str(doi.decode())
                        doi = doi.split("\\u")
                        doi = str(doi[0])

                        doi = re.sub("}", "", doi)
                        doi = re.sub("\t", "", doi)
                    doi = doi.encode("ascii", "ignore")
                    doi = doi.decode()
        except TypeError as e:
            print(e)

        pdf_url = soup.find("a", text="Full (PDF)")
        if not isinstance(pdf_url, type(None)):
            pdf_url = pdf_url.get("href")
        zblid_link = soup.find(
            "a", {"href": re.compile(r"http:\/\/www.zentralblatt-math.org\/zmath\/")}
        )
        if zblid_link is not None:
            subtext = zblid_link.get("href").split("?q=")[1]
            zblid = subtext

        if has_doi:
            xarticle.doi = doi.replace(" ", "")
            xarticle.pid = xarticle.doi
            xarticle.pid = xarticle.pid.replace("pid", "").replace(":", "_")
            doi = bytes(r"{}".format(r"" + doi + ""), "utf-8")
            doi = doi.decode()
            doi = doi.split("\\u")
            doi = str(doi[0]).strip()
            xarticle.ids.append(("doi", doi))

        else:
            if not isinstance(zblid, type(None)) and zblid != "":
                xarticle.extids.append(("zbl-item-id", zblid))

            if not isinstance(mthscinet, type(None)):
                xarticle.pid = xissue.pid + "_" + mthscinet.split("/")[3]

            if not isinstance(xarticle.pid, type(None)):
                xarticle_pid = reg_article.findall(article_url)
                if len(xarticle_pid) > 0:
                    id_article = xarticle_pid[0]
                    xarticle.pid = xissue.pid + "_" + id_article

        if xarticle.pid is None:
            url_article_part = article_url.split("/")
            len_url_part = len(url_article_part)
            id_article = url_article_part[len_url_part - 1]
            xarticle.pid = xissue.pid + "_" + id_article

        if doi_translation is not None:
            xarticle.extids.append(("doi-translation", doi_translation))
        # if eudml_id is not None:
        #   xarticle.extids.append(("eudml_id", eudml_id))

        url_full_text = soup.find("a", text="Access to full text")
        if url_full_text is not None:
            url_pdf = url_full_text.get("href")
            ext_link = model_data.create_extlink()
            ext_link["rel"] = "full-text"
            ext_link["location"] = url_pdf
            xarticle.ext_links.append(ext_link)

        if pdf_url:
            data = {
                "rel": "full-text",
                "mimetype": "application/pdf",
                "location": pdf_url,
                "base": "",
                "text": "Full Text",
            }
            xarticle.streams.append(data)

            # The pdf url is already added as a stream (just above) but might be replaced by a file later on.
            # Keep the pdf url as an Extlink if we want to propose both option:
            # - direct download of a local PDF
            # - URL to the remote PDF
            ext_link = model_data.create_extlink()
            ext_link["rel"] = "article-pdf"
            ext_link["location"] = pdf_url
            xarticle.ext_links.append(ext_link)

        xarticle.provider = self.provider
        xarticle.seq = seq
        if len(xarticle.lang) >= 3:
            xarticle.lang = xarticle.lang[:2]
        if len(xarticle.trans_lang) >= 3:
            xarticle.trans_lang = xarticle.trans_lang[:2]

        if issue is not None:
            article = model_helpers.get_article(xarticle.pid)
            if article is None:
                cmd = xml_cmds.addArticleXmlCmd(
                    {"issue": issue, "xarticle": xarticle, "use_body": False}
                )
                cmd.set_collection(issue.my_collection)
                cmd.set_provider(self.provider)
                cmd.do()
        return xarticle

    def import_singlearticle(self, articles):
        xarticles = []
        journal = JournalData()
        journal.issn = "0003486X"
        journal.publisher = "JSTOR"
        journal.e_issn = ""

        for article in articles:
            xarticle = self.parse_article(article)
            xarticles.append(xarticle)

        yield {"articles": xarticles}

    """
        Enregistrement d'une exception dans l'historique
    """

    def insert_exception(self, user, type_error, exception, issue_pid):
        manage_exceptions(
            pid=issue_pid,
            colid=self.pid,
            status="ERROR",
            title=self.collection.title_html,
            exception=exception,
            target=issue_pid,
            userid=user.pk,
            type_error=type_error,
            operation="import",
            parent_id=self.parent_event,
        )
        parent_event = HistoryEvent.objects.filter(pk=self.parent_event)
        if len(parent_event) > 0:
            parent_event = parent_event.first()
            parent_event.status = "ERROR"
            parent_event.save()

        message = ""
        if hasattr(exception, "message"):
            message = exception.message
        else:
            message = exception
        raise Exception(message)

    """
        Ajout d'un stream pour log erreur/info
    """

    def add_stream(self, data, key):
        r = self.redis_client.get_connection()
        self.redis_client.exec_cmd(r.xadd, key, data)

    """
        Permet la modification d'article existant
    """

    def augment_article(self, xarticle):
        if not xarticle.pid and not xarticle.doi:
            raise ValueError("No id")
        pid = xarticle.pid
        if not pid:
            pid = xarticle.doi.replace("/", "_").replace(".", "_").replace("-", "_")

        article = model_helpers.get_article(pid)
        if not article:
            raise ValueError("No {pid}")

        article_data = model_data_converter.db_to_article_data(article)
        links = [item for item in article_data.ext_links if item["rel"] == "article-pdf"]
        if not links:
            links = [item for item in xarticle.ext_links if item["rel"] == "article-pdf"]
            if links:
                ext_link = links[0]
                article_data.ext_links.append(ext_link)

                params = {
                    "xarticle": article_data,
                    "use_body": False,
                    "issue": article.my_container,
                    "standalone": True,
                }

                cmd = xml_cmds.addArticleXmlCmd(params)
                cmd.set_collection(article.get_collection())
                cmd.do()
