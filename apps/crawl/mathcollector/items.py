# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

from dataclasses import dataclass

import scrapy


class ApiLink(scrapy.Item):
    link = scrapy.Field()


class Metadata(scrapy.Item):
    issue = scrapy.Field()
    link_article = scrapy.Field()
    volume = scrapy.Field()
    issn = scrapy.Field()
    year = scrapy.Field()


@dataclass
class Issue:
    pid: str
    year: str
    volume: str
    journal: object
    create_xissue: bool
    number: str


"""
@dataclass
class Collection:
    pid: str
    year: str
    volume: str
    journal: object
    create_xissue: bool
    number: str
"""


@dataclass
class Collection:
    title: str
    url: str
    pid: str
    exclude: str
    count: int
    set: str
