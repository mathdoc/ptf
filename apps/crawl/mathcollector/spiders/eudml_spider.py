import calendar
import json
import logging
import logging.handlers
import os
import re
import traceback
import uuid
from dataclasses import dataclass
from datetime import datetime

import regex
import scrapy
from bs4 import BeautifulSoup
from colorclass import Color
from etaprogress.progress import ProgressBar
from middleware.redis_command import redisClient
from psycopg2 import DatabaseError
from psycopg2 import IntegrityError
from pysolr import SolrError
from scrapy import Request
from scrapy import signals
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from scrapy_splash import SplashRequest

from django.conf import settings
from django.contrib.auth.models import User
from django.dispatch import Signal

from crawl.mathcollector.crawl_eudml import EudmlCrawler
from crawl.mathcollector.items import Issue
from gdml import settings as settings_base
from gdml.models import Periode
from gdml.models import Source
from gdml.signals import collection_created
from gdml.signals import send_result_import
from history.models import HistoryEvent
from history.models import insert_history_event
from history.views import manage_exceptions
from ptf.cmds import ptf_cmds
from ptf.exceptions import ResourceExists
from ptf.model_data import create_publicationdata
from ptf.model_helpers import get_collection
from ptf.model_helpers import get_provider
from ptf.models import Collection as CollectionPtf
from ptf.models import Container
from ptf.models import PtfSite
from ptf.models import ResourceId

end_crawl = Signal()


class VolumeItems:
    def __init__(self, links, year, volume, number):
        self.volume = volume
        self.year = year
        self.links = links
        self.number = number
        self.urls = []
        self.bar = None


@dataclass
class Collection:
    title: str
    url: str
    pid: str
    exclude: str
    count: int
    set: str


class EudmlSpider(scrapy.Spider):
    custom_settings = {
        "BOT_NAME": "mathcollector",
        "SPIDER_MODULES": ["mathcollector.spiders"],
        "NEWSPIDER_MODULE": "mathcollector.spiders",
        "ROBOTSTXT_OBEY": True,
        "SPIDER_MIDDLEWARES": {
            "scrapy_splash.SplashDeduplicateArgsMiddleware": 100,
        },
        "DOWNLOADER_MIDDLEWARES": {
            "scrapy_splash.SplashCookiesMiddleware": 723,
            "scrapy_splash.SplashMiddleware": 725,
            # This middleware provides the integration between Scrapy and Splash and is assigned the priority of 725.
            "scrapy.downloadermiddlewares.httpcompression.HttpCompressionMiddleware": 810,
            "scrapy.downloadermiddlewares.retry.RetryMiddleware": None,
            "crawl.mathcollector.spiders.middleware.retry_middleware.RetryMiddlewareSpider": 550,
        },
        "SPLASH_URL": "http://localhost:8050",
        # Define Splash DupeFilter
        "DOWNLOADER_CLIENT_TLS_METHOD": "TLSv1.2",
        "DUPEFILTER_CLASS": "scrapy_splash.SplashAwareDupeFilter",
        # Storage Mechanism
        "HTTPCACHE_STORAGE": "scrapy_splash.SplashAwareFSCacheStorage",
        "CONCURRENT_REQUESTS": 16,
        "DOWNLOAD_DELAY": 2,
        "RETRY_TIMES": 3,
        "DOWNLOAD_TIMEOUT": 300,
    }

    name = "eudml_spider"

    @property
    def logger(self):
        return logging.getLogger("eudml_spider")

    def __init__(
        self,
        url=None,
        urls=None,
        username=None,
        collection_to_start=None,
        prepare_collections=False,
        export_collections=None,
        scrap=False,
        parent_task=None,
        restart=None,
    ):
        self.logger_http_err = logging.getLogger("scrapy.spidermiddlewares.httperror")
        self.logger_retry = logging.getLogger("scrapy.downloadermiddlewares.retry")
        self.url = url
        self.collection = None
        self.collection_to_start = collection_to_start
        self.urls_collections = []
        self.pos = 0
        self.parent_id = parent_task
        self.export_collections = export_collections
        self.prepare_collections = prepare_collections
        self.restart = restart
        self.retry_count = 0
        self.numdam_collections = [
            "ACIRM",
            "ALCO",
            "AFST",
            "AIHPC",
            "AIHPA",
            "AIHPB",
            "AIF",
            "AIHP",
            "AUG",
            "AMPA",
            "AHL",
            "AMBP",
            "ASENS",
            "ASCFPA",
            "ASCFM",
            "ASNSP",
            "AST",
            "BSMF",
            "BSMA",
            "CTGDC",
            "BURO",
            "CSHM",
            "CG",
            "CM",
            "CRMATH",
            "CML",
            "CJPS",
            "CIF",
            "DIA",
            "COCV",
            "M2AN",
            "PS",
            "GAU",
            "GEA",
            "STS",
            "TAN",
            "JSFS",
            "JEP",
            "JMPA",
            "JTNB",
            "JEDP",
            "CAD",
            "CCIRM",
            "RCP25",
            "MSIA",
            "MRR",
            "MSH",
            "MSMF",
            "MSM",
            "NAM",
            "OJMO",
            "PHSC",
            "PSMIR",
            "PDML",
            "PMB",
            "PMIHES",
            "PMIR",
            "RO",
            "RCP",
            "ITA",
            "RSMUP",
            "RSA",
            "RHM",
            "SG",
            "SB",
            "SBCD",
            "SC",
            "SCC",
            "SAF",
            "SDPP",
            "SMJ",
            "SPHM",
            "SPS",
            "STNB",
            "STNG",
            "TSG",
            "SD",
            "SE",
            "SEDP",
            "SHC",
            "SJ",
            "SJL",
            "SLSEDP",
            "SLDB",
            "SL",
            "SPK",
            "SAC",
            "SMS",
            "SLS",
            "SSL",
            "SENL",
            "SSS",
            "SAD",
            "THESE",
            "SMAI-JCM",
            "WBLN",
        ]
        self.book_list = ["ZNSL", "AMUC"]
        self.conference_list = []
        if urls is None:
            self.urls = []
        self.items = []
        f_format = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
        f_handler = logging.FileHandler(settings.LOG_EUDML)
        f_handler.setFormatter(f_format)
        self.logger.addHandler(f_handler)
        self.collection = None
        self.redis_client = redisClient()
        r = self.redis_client.get_connection()
        self.redis_client.exec_cmd(r.set, "progress", 0)
        self.redis_client.exec_cmd(r.set, "start_crawl", 1)
        self.redis_client.exec_cmd(r.set, "failures_collection", 0)
        self.username = username
        if self.username is not None:
            if self.username != "":
                self.user = User.objects.get(username=self.username)

        self.uuid = str(uuid.uuid1())
        self.has_err = False
        self.logger_http_err.setLevel(logging.WARNING)
        self.logger.setLevel(logging.INFO)

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super().from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.item_scraped, signal=signals.item_scraped)
        crawler.signals.connect(spider.spider_closed, signal=signals.spider_closed)
        crawler.signals.connect(spider.spider_error, signal=signals.spider_error)
        crawler.signals.connect(spider.spider_closed, signal=signals.engine_stopped)
        crawler.signals.connect(spider.item_error, signal=signals.item_error)
        crawler.signals.connect(spider.spider_idle, signal=signals.spider_idle)

        return spider

    async def item_scraped(self, item):
        # Send the scraped item to the server

        self.bar.numerator += 1
        print(self.bar)
        print("Request finished!")

        if isinstance(item, Collection):
            pid = item.pid
            title = item.title

        else:
            if "pid" in item:
                pid = item["pid"]
            if "collection" in item:
                title = item["collection"]
            if "title" in item:
                title = item["title"]
        self.logger.info(
            "Import success : \n"
            + "col: "
            + str(pid)
            + "\n created on :"
            + str(datetime.now())
            + "\n message : "
            + str(title)
        )

    async def item_error(self, item, response, failure):
        if isinstance(item, Collection):
            pid = item.pid
            title = item.title

        else:
            if "pid" in item:
                pid = item["pid"]
            if "collection" in item:
                title = item["collection"]
            if "title" in item:
                title = item["title"]

        self.logger.info(
            "Import error : \n"
            + "col: "
            + str(pid)
            + "\n created on :"
            + str(datetime.now())
            + "\n message : "
            + str(title)
        )
        history_parent_event = HistoryEvent.objects.filter(unique_id=self.uuid)
        if len(history_parent_event) > 0:
            parent_event = history_parent_event[0]
            parent_event.status = "ERROR"
            parent_event.save()
            parent_event_id = parent_event.pk
            manage_exceptions(
                "import",
                pid="",
                colid=item.pid,
                status="ERROR",
                title="Erreur d'intègration " + item.title,
                exception=str(failure),
                target="",
                type_error="erreur d'import",
                parent_id=parent_event_id,
            )

    async def spider_idle(self):
        history_parent_event = HistoryEvent.objects.filter(unique_id=self.uuid)
        if len(history_parent_event) > 0:
            parent_event = history_parent_event[0]
            parent_event.status = "ERROR"
            parent_event.save()
            parent_event_id = parent_event.pk
            manage_exceptions(
                "import",
                pid="",
                colid="",
                status="ERROR",
                title="Processus d'import non actif ",
                exception="",
                target="",
                type_error="erreur système",
                parent_id=parent_event_id,
            )

    def spider_closed(self, spider, reason):
        print("end spider....")
        spider.logger.info("Spider closed: %s", spider.name)
        spider.logger.info("collection crawler instance: %s", self.collection)
        spider.logger.info("reason: %s", reason)

        r = self.redis_client.get_connection()
        self.redis_client.exec_cmd(r.set, "progress", 0)
        self.redis_client.exec_cmd(r.set, "start_crawl", 0)
        errors_count = self.redis_client.exec_cmd(r.get, "failures_collection")
        date_event = datetime.now()
        date_event = calendar.timegm(date_event.timetuple())
        if not isinstance(self.url, type(None)):
            title = "Préparation des collections EUDML"
        else:
            title = "collections EUDML"
        data_task = {
            "uid": self.uuid,
            "date": date_event,
            "collection": self.collection.pid,
            "title": title,
            "username": self.username,
            "total": self.bar.denominator,
            "success": self.bar.numerator,
            "failures": errors_count,
            "progress": 100,
        }
        print(data_task)
        self.redis_client.exec_cmd(r.xadd, "task:import", data_task)
        command_tasks = self.redis_client.exec_cmd(
            r.xrevrange, "task:import", max="+", min="-", count=1
        )

        print("get last inserted command")

        print(command_tasks)

        send_result_import.send(sender=self.__class__, url=self.url)

    def spider_error(self, failure, response, spider):
        r = self.redis_client.get_connection()
        item = response.meta["collection"].pid

        spider.logger.info("error : %s", failure)
        spider.logger.info("PID : %s", item)
        user = User.objects.filter(username=self.username)

        history_parent_event = HistoryEvent.objects.filter(unique_id=self.uuid)
        if len(history_parent_event) > 0:
            parent_event = history_parent_event[0]
            parent_event = parent_event.pk
            manage_exceptions(
                "import",
                pid=item,
                colid=item,
                status="ERROR",
                title=response.meta["collection"].title,
                exception=failure.value,
                target=response.meta["collection"].url,
                userid=user[0].id,
                type_error=str(failure.type.args),
                parent_id=parent_event,
            )
        errors_count = int(self.redis_client.exec_cmd(r.get, "failures_collection"))
        errors_count += 1
        history_parent_event = history_parent_event[0]
        history_parent_event.status = "ERROR"
        history_parent_event.data = {
            "message": " - ".join([str(failure.value), traceback.format_exc()]),
            "ids_count": 0,
            "target": item,
        }
        history_parent_event.save()
        self.redis_client.exec_cmd(r.set, "failures_collection", errors_count)

    def getArticle(self, response):
        """
        parse article from json
        """

        yield {"article": ""}

    def articlesLink(self, url, links, volume, number, year):
        """
        import issue with links into collection
        """

        self.item.import_issue(
            url, links=links, year=year, volume=volume, number=number, augment=False
        )

    def getSeries(self, volumes, issue_infos):
        for link_volume in volumes:
            # get articles
            articles = scrapy.Request(
                link_volume,
                callback=self.articlesLink,
                cb_kwargs=dict(
                    volume=issue_infos["volume"] + "-" + issue_infos["number"],
                    year=issue_infos["year"],
                    url=link_volume,
                ),
            )
            yield articles

    def get_metadata(self, response):
        print("article scrap")
        soup = BeautifulSoup(response.text, "html.parser")
        yield soup

    """
        Parse issue or volume
    """

    def parse_volume(self, url):
        xissue = self.item.import_issue(url)
        yield {"issue imported": xissue}

    def parse_result(self, response, *arg):
        # here you would extract links to follow and return Requests for
        # each of them, with another callback

        if self.url is not None:
            for journal in response.xpath("//h3/a").getall():
                journal = journal.replace("'", repr("'"))
                soup = BeautifulSoup(journal, "html.parser")
                title = soup.text.replace("\n", "")
                reg_url = re.compile(r"/journal/\d+")
                match_link = reg_url.search(journal)
                if match_link:
                    url = "https://eudml.org" + match_link[0]
                # fix pid, exclude
                item = Collection(
                    title=title.strip(), url=url, exclude="", count="", set="", pid=match_link[0]
                )
                yield item
            if self.pos == 0:
                next_url = response.xpath(
                    "//div[@class='unit unit-journal-pagination']/p[@class='pagination']/a/@href"
                ).extract_first()
            else:
                next_url = response.xpath(
                    "//div[@class='unit unit-journal-pagination']/p[@class='pagination']/a/@href"
                )
                if len(next_url) > 1:
                    next_url = next_url[1].root

            if next_url and isinstance(next_url, str):
                self.pos += 1
                print(self.bar)
                yield scrapy.Request("https://eudml.org/" + next_url, self.parse_result)
        else:
            title = response.xpath("//h1").getall()
            item = {"collection": title[0]}
            yield item

    def createCollectionObj(self):
        p = get_provider("mathdoc-id")
        xcol = create_publicationdata()
        xcol.pid = self.collection["pid"]
        """
        if xcol.pid in self.book_list:
            xcol.coltype = "book-series"
        """

        xcol.coltype = "journal"

        xcol.title_tex = self.collection["title"]

        xcol.title_html = self.collection["title"]
        xcol.title_tex = self.collection["title"]
        xcol.trans_title_tex = self.collection["title"]
        xcol.trans_title_html = self.collection["title"]
        xcol.issn = self.collection["issn"]
        xcol.title_xml = f"<title-group><title>{self.collection['title']}</title></title-group>"
        xcol.lang = "en"
        xcol.trans_lang = "en"
        xcol.abbrev = ""
        xcol.fyear = self.collection["fyear"]
        xcol.lyear = self.collection["lyear"]
        cmd = ptf_cmds.addCollectionPtfCmd({"xobj": xcol})
        cmd.set_provider(p)
        self.collection = cmd.do()
        resource = self.collection.resource_ptr
        id_issn = ResourceId.objects.filter(id_type="issn", id_value=xcol.issn)
        if len(id_issn) == 0:
            identifier = {"id_type": "issn", "id_value": xcol.issn}
            cmd_2 = ptf_cmds.updateResourceIdPtfCmd(identifier)
            cmd_2.set_resource(resource)
            cmd_2.do()

    def createCollection(self, url_journal):
        # source with domain eudml.org has to be created
        if re.compile(r"hdml\.di\.ionio\.gr").search(url_journal):
            source = Source.objects.get(domain="hdml.di.ionio.gr")
        else:
            source = Source.objects.get(domain="eudml.org")

        collection = CollectionPtf.objects.filter(pid=self.collection["pid"])
        existing_periode = False
        if len(collection) > 0:
            collection = collection[0]
            self.collection = collection
            periode_created = Periode.objects.filter(collection=collection, source=source)
            if len(periode_created) > 0:
                existing_periode = True
            # update collection
        else:
            self.createCollectionObj()
        # creation d'une periode
        if not existing_periode:
            collection_created.send(
                sender=self.__class__, source=source, collection=self.collection, url=url_journal
            )

        # update site id
        site_id = settings_base.SITE_ID
        sites = self.collection.sites.all()
        if site_id not in sites:
            ptfSite = PtfSite.objects.get(site_ptr_id=site_id)
            self.collection.sites.add(ptfSite)
            self.collection.save()
        return True

    def set_last_cmd_status(self, spider=None, reason=None):
        self.logger.info("Spider closed: %s", self.name)
        self.logger.info("collection crawler instance: %s", self.collection)
        r = self.redis_client.get_connection()
        self.redis_client.exec_cmd(r.set, "progress", 0)
        self.redis_client.exec_cmd(r.set, "start_crawl", 0)
        errors_count = int(self.redis_client.exec_cmd(r.get, "failures_collection"))
        errors_count += 1
        total = self.bar.denominator
        if not isinstance(self.url, type(None)):
            title = "Préparation des collections EUDML"
        else:
            title = "import des collections EUDML"
        date_event = datetime.now()
        date_event = calendar.timegm(date_event.timetuple())
        if self.bar.numerator == 0:
            progress = 0

        else:
            if self.collection is not None:
                progress = total - self.bar.numerator

        current_log_import = self.redis_client.exec_cmd(
            r.xrevrange, "task:import", max="+", min="-", count=1
        )
        current_log_import = current_log_import[0]
        id_stream = current_log_import[0]
        self.redis_client.exec_cmd(r.xdel, "task:import", id_stream)

        data_task = {
            "uid": self.uuid,
            "date": date_event,
            "collection": "",
            "title": title,
            "username": self.username,
            "total": total,
            "success": self.bar.numerator,
            "failures": errors_count,
            "progress": progress,
        }
        r = self.redis_client.get_connection()
        self.redis_client.exec_cmd(r.xadd, "task:import", data_task)

        position_ended = {
            "uid": self.uuid,
            "date": date_event,
            "collection": self.collection.pid,
            "failures": errors_count,
        }
        r = self.redis_client.get_connection()
        self.redis_client.exec_cmd(r.xadd, "import:EUDML", position_ended)

        current_step = self.redis_client.exec_cmd(
            r.xrevrange, "import:EUDML", max="+", min="-", count=1
        )
        current_step = current_step[0]
        id_step = current_step[0]
        if int(errors_count) > 0:
            history_parent_event = HistoryEvent.objects.get(unique_id=self.uuid)
            history_parent_event.status = "ERROR"
            history_parent_event.type_error = "Une erreur est survenue au démarrage"
            history_parent_event.data["message"] = reason
            history_parent_event.data["target"] = id_step
            history_parent_event.data["source"] = "EUDML"
            history_parent_event.save()

    def update_error_status(self, err, pid=None):
        if not err.__class__.__name__ == "CloseSpider":
            template = "Une exception de type {0} est survenue. Arguments:\n{1!r}"
        else:
            template = "Une exception est survenue. Arguments:\n{1!r}"
        if isinstance(err, str):
            message = err
        else:
            message = err.args

        self.set_last_cmd_status(reason=message)

        history_parent_event = HistoryEvent.objects.get(unique_id=self.uuid)
        manage_exceptions(
            operation="import",
            pid=pid,
            colid=self.collection.pid,
            status="ERROR",
            title=self.collection.title,
            exception=err,
            target=self.collection.pid,
            userid=self.user.pk,
            type_error=template.format(type(err).__name__, message),
            parent_id=history_parent_event.pk,
        )

    def process_issue(self, crawler, collection, pid, url_articles, parent_event):
        user = User.objects.get(username=self.username)
        template = "Une exception de type {0} est survenue. Arguments:\n{1!r}"

        try:
            crawler.import_issue(issue_url=None, url_articles=url_articles)
        except DatabaseError as db_err:
            print(db_err)

            self.logger.error(
                "Error : \n"
                + "pid: "
                + str(pid)
                + "\n col: "
                + collection.pid
                + "\n created on :"
                + str(datetime.now())
                + "\n message : "
                + db_err
            )

            manage_exceptions(
                pid=pid,
                colid=collection.pid,
                status="ERROR",
                title=collection.title_html,
                exception=db_err,
                target=parent_event,
                userid=user.pk,
                type_error=template.format(type(db_err).__name__, db_err.args),
                parent_id=parent_event,
            )
            self.update_error_status(db_err, pid)

        except ResourceExists as err:
            self.logger.error(
                "Error : \n"
                + "pid: "
                + str(pid)
                + "\n col: "
                + collection.pid
                + "\n created on :"
                + str(datetime.now())
                + "\n message : "
                + str(err.__cause__)
            )
            manage_exceptions(
                pid=pid,
                colid=collection.pid,
                status="ERROR",
                title=collection.title_html,
                exception=err,
                target=parent_event,
                userid=user.pk,
                type_error=template.format(type(err).__name__, err.args),
                parent_id=parent_event,
            )
            self.update_error_status(err, pid)

        except IntegrityError as integrity_err:
            self.logger.error(
                "Error : \n"
                + "pid: "
                + str(pid)
                + "\n col: "
                + self.collection.pid
                + "\n created on :"
                + str(datetime.now())
                + "\n message : "
                + str(integrity_err)
            )
            manage_exceptions(
                pid=pid,
                colid=collection.pid,
                status="ERROR",
                title=collection.title_html,
                exception=integrity_err,
                target=parent_event,
                userid=user.pk,
                type_error=template.format(type(integrity_err).__name__, integrity_err.args),
                parent_id=parent_event,
            )
            self.update_error_status(integrity_err, pid)

        except SolrError as solr_err:
            self.logger.info(
                "Import error : \n"
                + "col: "
                + str(pid)
                + "\n created on :"
                + str(datetime.now())
                + "\n message : "
                + str(collection.title_html)
            )

            self.logger.error(
                "Error : \n"
                + "pid: "
                + str(pid)
                + "\n col: "
                + collection.pid
                + "\n created on :"
                + str(datetime.now())
                + "\n message : "
                + str(solr_err.message)
            )
            manage_exceptions(
                pid=pid,
                colid=collection.pid,
                status="ERROR",
                title=collection.title_html,
                exception=solr_err,
                target=parent_event,
                userid=user.pk,
                type_error=template.format(type(solr_err).__name__, solr_err.args),
                parent_id=parent_event,
            )
            self.update_error_status(solr_err, pid)

        except Exception as ex:
            print("Exception inserting container")
            print(str(pid))

            self.logger.info(
                "Import error : \n"
                + "col: "
                + str(pid)
                + "\n created on :"
                + str(datetime.now())
                + "\n message : "
                + str(ex)
                + "\n collecton"
                + str(collection.pid)
            )
            manage_exceptions(
                pid=pid,
                colid=collection.pid,
                status="ERROR",
                title=collection.title_html,
                exception=ex,
                target=parent_event,
                userid=user.pk,
                operation="import",
                type_error=template.format(type(ex).__name__, ex.args),
                parent_id=parent_event,
            )
            self.update_error_status(ex, pid)

    def get_publisher(self, soup):
        """
        set publisher
        """
        section_oai = soup.find("h3", text="In Other Databases").parent
        section_oai = section_oai.find_all("dd")
        if section_oai is not None:
            pub = [
                d.text
                for d in section_oai
                if d.text.strip() not in ["DOI", "ZBMath", "MathSciNet", "PUBLISHER"]
            ]

            return pub[0].strip()

    def add_stream(self, data, key):
        r = self.redis_client.get_connection()
        self.redis_client.exec_cmd(r.xadd, key, data)

    """
        function to parse nodes and crawl collection
    """

    def get_page(self, response, *args):
        body = response.body
        body = body.decode("utf-8")
        # j = json.loads(response.body_as_unicode())

        collectionItem = response.meta["collection"]
        soup = BeautifulSoup(body, "html.parser")
        publisher = self.get_publisher(soup)

        collectionItem.set = publisher
        collectionItem.count = 0

        volume_nodes = soup.findAll("li", {"role": "treeitem"})
        reg_year = re.compile(r"\d{4}")
        for volume in volume_nodes:
            span_volume = volume.find("span")

            volume_text = span_volume.findChildren("strong", recursive=False)
            year = None

            if len(volume_text) > 1:
                if reg_year.search(volume_text[1].get_text()):
                    year = reg_year.search(volume_text[1].get_text())[0]
            else:
                if reg_year.search(volume_text[0].get_text()):
                    year = reg_year.search(volume_text[0].get_text())[0]
            reg_strip_chars = re.compile(r"\/")
            if isinstance(year, str):
                if reg_strip_chars.search(year):
                    year = year.split("/")
                    year = year[0]

            # regexp_volume_year = regex.compile(r"\d{4}\/\d{4}")

            issue_present = [span for span in volume.find_all("span") if "issue" in span.text]

            year_str = year if not isinstance(year, type(None)) else ""

            if len(issue_present) > 0 and year_str != "":
                # récuperation issues
                for index_issue, issue in enumerate(issue_present):
                    if issue.get("class")[0] != "artsp":
                        el = issue.get("id")
                        el = el.split("span-")

                        list_articles = soup.find("ul", {"id": el[1]})
                        if list_articles is not None:
                            url_articles = [a.get("href") for a in list_articles.find_all("a")]
                            collectionItem.count += len(url_articles)
            articles_node = volume.find_all("li", {"class": "toggleable art"})
            collectionItem.count += len(articles_node)

        item_exporter = Collection(
            title=collectionItem.title.strip(),
            url=collectionItem.url,
            exclude="",
            count=str(collectionItem.count),
            set=collectionItem.set,
            pid=collectionItem.pid,
        )
        yield item_exporter

        yield collectionItem

    def parse_nodes_collection(self, response, *arg):
        redis_client = self.redis_client
        r = redis_client.get_connection()

        parent_event = HistoryEvent.objects.filter(unique_id=self.uuid)

        if len(parent_event) > 0:
            parent_event = parent_event[0]
            parent_event = parent_event.pk

        collectionItem = response.meta["collection"]
        self.collection = collectionItem

        redis_client.exec_cmd(
            r.hset,
            "last_task_import",
            mapping={
                "id": self.uuid,
                "username": self.username,
                "collection": collectionItem.pid,
                "progress": self.bar.numerator,
                "total": self.bar.denominator,
            },
        )

        redis_client.exec_cmd(r.set, "progress", self.bar.numerator)
        client = self.redis_client
        r = client.get_connection()

        if response.status != 200:
            fails = client.exec_cmd(r.get, "failures_collection")
            fails += 1
            client.exec_cmd(r.set, "failures_collection", fails)

        # if self.collection.pid not in self.numdam_collections and self.collection.exclude != "yes":
        if self.collection.pid not in self.numdam_collections:
            collection = get_collection(pid=self.collection.pid, sites=False)
            redis_client.exec_cmd(
                r.hset,
                "last_task_import",
                mapping={
                    "id": self.uuid,
                    "username": self.username,
                    "collection": collectionItem.pid,
                    "progress": self.bar.numerator,
                },
            )

            if collection.coltype == "book-series":
                log = (
                    "Error - coltype: \n"
                    + " col: \n"
                    + str(collection.pid)
                    + "\n created on : "
                    + str(datetime.now())
                    + "\n message : "
                    + "Collection de type série-livre, numéros non importés"
                    + "type_error :"
                    + "insert error \n"
                )
                self.logger.error(log)
                yield collectionItem
            data = {}
            data["pid"] = collection.pid
            if (
                collection.pid in ["EU", "EUG", "EUA", "DEME", "MR"]
                or self.collection.exclude == "yes"
                or collection.coltype == "book-series"
            ):
                return collectionItem

            data["coltype"] = collection.coltype
            data["wall"] = (collection.wall,)
            data["title"] = collection.title_sort
            data["collection_href"] = "/journals/" + collection.pid

            source = Source.objects.get(domain="eudml.org")
            data["source"] = source
            data["domain"] = source.domain
            data["website"] = source.website
            data["pdf_href"] = ""
            data["article_href"] = ""
            data["periode_href"] = ""
            data["issue_href"] = ""
            data["parent_event"] = parent_event
            data["username"] = self.username
            collection_crawler = EudmlCrawler(**data)
            collection_crawler.create_xissue = True
            body = response.body
            body = body.decode("utf-8")
            # j = json.loads(response.body_as_unicode())

            # text = body.strip().replace('"', "").replace("\\", "")
            soup = BeautifulSoup(body, "html.parser")
            # récuperation des noeuds volume

            volume_nodes = soup.findAll("li", {"role": "treeitem"})
            reg_year = re.compile(r"\d{4}")
            for volume in volume_nodes:
                missing_issues = []
                span_volume = volume.find("span")

                volume_text = span_volume.findChildren("strong", recursive=False)
                volume_number = volume_text[0].get_text()
                year = None

                if len(volume_text) > 1:
                    if reg_year.search(volume_text[1].get_text()):
                        year = reg_year.search(volume_text[1].get_text())[0]
                else:
                    if reg_year.search(volume_text[0].get_text()):
                        year = reg_year.search(volume_text[0].get_text())[0]
                reg_strip_chars = re.compile(r"\/")
                if isinstance(year, str):
                    if reg_strip_chars.search(year):
                        year = year.split("/")
                        year = year[0]
                issue_id = span_volume.get("id")
                issue_pid_part = issue_id.split("journal-")
                issue_pid = issue_pid_part[1]
                issue_pid = issue_pid[:-1]
                # regexp_volume_year = regex.compile(r"\d{4}\/\d{4}")
                regex_volume_prefix = regex.compile(r"vol\. \d+")

                if regex_volume_prefix.search(volume_number):
                    volume_number = regex_volume_prefix.search(volume_number)[0]
                    volume_number = volume_number.split(" ")
                    volume_number = volume_number[1]

                # if regexp_volume_year.match(volume_number):
                #    volume_number = volume_number.split("/")
                #  volume_number = volume_number[0]

                if volume_number == "000":
                    volume_number = "0"
                if volume_number[:2] == "00":
                    volume_number = volume_number[2:]
                if volume_number[:1] == "0":
                    volume_number = volume_number[1:]
                if re.compile("Special Series").search(volume_number):
                    volume_number = "00"
                if re.compile(r"\d+-\d+").search(volume_number):
                    volume_number = volume_number.split("-")
                    volume_number = volume_number[0]
                if re.compile("Extra Vol").search(volume_number):
                    volume_number = "00"

                issue_present = [span for span in volume.find_all("span") if "issue" in span.text]
                url_articles = []

                year_str = year if not isinstance(year, type(None)) else ""

                if re.compile("year").search(volume_number):
                    regexp_year = regex.compile(r"\d{4}")
                    year = regexp_year.search(volume_number)[0]
                    year = "00"
                if year_str == "":
                    year = "0"

                if re.compile(r"\d+\/\d+").match(year):
                    year = year.split("/")
                    year = year[0]
                    year_str = str(year)
                if len(issue_present) > 0 and year_str != "":
                    # récuperation issues
                    for index_issue, issue in enumerate(issue_present):
                        issue_pid = collection.pid + "_" + year_str + "__" + volume_number

                        if issue.get("class")[0] != "artsp":
                            el = issue.get("id")
                            issue_number = issue.find_next("strong").get_text()
                            el = el.split("span-")

                            list_articles = soup.find("ul", {"id": el[1]})
                            if list_articles is not None:
                                url_articles = [a.get("href") for a in list_articles.find_all("a")]
                            # prefix url eudml each articles
                            for index, url in enumerate(url_articles):
                                url_articles[index] = "https://eudml.org" + url

                            collectionItem.count += len(url_articles)
                            if len(missing_issues) == 0:
                                if re.compile("[a-zA-Z]+").search(issue_number):
                                    issue_number = str(index_issue)

                            issue_pid += "_" + issue_number
                            if re.compile(",").search(issue_number):
                                issue_pid = issue_pid.replace(",", "-")
                                issue_number = issue_number.replace(",", "-")

                            if re.compile(r"\d+\/\d+").search(issue_number):
                                issue_part = issue_number.split("/")
                                issue_number = issue_part[0]

                            issue = Issue(
                                pid=issue_pid,
                                number=issue_number,
                                year=year,
                                volume=volume_number,
                                journal=self.collection,
                                create_xissue=True,
                            )

                            collection_crawler.issue = issue

                            if len(url_articles) > 0:
                                self.process_issue(
                                    collection_crawler,
                                    collection,
                                    issue_pid,
                                    url_articles,
                                    parent_event,
                                )

                # import issue on the fly

                collection_crawler.create_xissue = True
                if re.compile("Extra Vol").search(volume_number):
                    volume_number = "00"

                issue_pid = collection.pid + "_" + year_str + "__" + volume_number

                if re.compile(",").search(volume_number):
                    issue_pid = issue_pid.replace(",", "-")
                    volume_number = volume_number.replace(",", "-")

                issue = Issue(
                    pid=issue_pid,
                    number=None,
                    year=year,
                    volume=volume_number,
                    journal=self.collection,
                    create_xissue=True,
                )
                collection_crawler.issue = issue
                articles_node = volume.find_all("li", {"class": "toggleable art"})
                url_articles = []
                for article in articles_node:
                    article_link = article.find("a")
                    if not isinstance(article_link, type(None)):
                        article_link = article_link.get("href")
                        article_link = "https://eudml.org" + article_link
                        url_articles.append(article_link)

                collectionItem.count += len(url_articles)

                if len(articles_node) > 0:
                    self.process_issue(
                        collection_crawler,
                        collection,
                        issue_pid,
                        url_articles,
                        parent_event,
                    )

        item_exporter = Collection(
            title=collectionItem.title.strip(),
            url=collectionItem.url,
            exclude="",
            count=str(collectionItem.count),
            set=collectionItem.set,
            pid=collectionItem.pid,
        )
        yield item_exporter

    def parse_collection(self, url):
        if self.collection["pid"] in self.numdam_collections:
            print(self.collection["pid"])
            pass

        else:
            create_collection = self.createCollection(url_journal=url)

            if create_collection is None:
                print("collection outside")
                return

            if self.export_collections is False and self.prepare_collections is True:
                yield self.collection

                print(self.bar)
        yield self.collection

    def parse(self, response, *arg):
        # here you would extract links to follow and return Requests for
        # each of them, with another callback

        body = response.body
        body = body.decode("utf-8")
        # j = json.loads(response.body_as_unicode())

        text = body.strip().replace('"', "").replace("\\", "")
        soup = BeautifulSoup(text, "html.parser")
        # récuperation des noeuds volume

        title_collection = soup.find("h1")
        if title_collection:
            title_collection = title_collection.get_text()
        meta = soup.find_all("meta")
        fyear = ""
        lyear = ""
        lang = ""
        pid_journal = ""
        print(self.bar)
        try:
            for meta_header in meta:
                if meta_header.get("name") is not None:
                    meta_info = meta_header.get("name")
                    if "citation_language" in meta_info:
                        lang = meta_header.get("content")
                        lang = lang.split("/")
                        if len(lang) > 0:
                            lang = lang[0][:3]

                    if "citation_year" in meta_info:
                        periode = meta_header.get("content")
                        periode_range = periode.split(",")
                        if lyear == "":
                            lyear = periode_range[len(periode_range) - 1]
                        fyear = periode_range[0]
                        fyear_start = fyear.split("-")
                        if len(fyear_start) > 0:
                            fyear = fyear_start[0]
                        if len(fyear_start) > 1:
                            lyear = fyear_start[1]
                    if "citation_id" in meta_info:
                        extid = meta_header.get("content")
                        extid = extid.split("/")
                        if len(extid) > 0:
                            extid = extid[0]
                            id_part = extid.split(":")
                            pid_journal = id_part[len(id_part) - 1]

        except Exception as e:
            print(e)
        try:
            issn = soup.find("strong", text="ISSN:").parent.get_text()
            issn = issn.split("ISSN:")
            if len(issn) > 1:
                issn = issn[1].strip()
            else:
                issn = None
        except:
            issn = None

        pid = pid_journal
        if "collection" in response.meta:
            collection = response.meta["collection"]
            pid = collection.pid

        self.collection = {
            "title": collection.title,
            "fyear": fyear,
            "coltype": "journal",
            "lyear": lyear,
            "wall": 0,
            "lang": lang,
            "pid": pid,
            "issn": issn,
        }

        coll = None

        # if response.meta["exclude"] == "yes" or response.meta["collection"].pid in self.numdam_collections:
        if response.meta["collection"].pid in self.numdam_collections:
            pass
        else:
            coll = self.parse_collection(url=response.meta["url"])
            next(coll)
        # volumes = response.xpath("//li[contains(@class,'toggleable') and contains(@role, 'treeitem')]/span[contains(@class,'toggleable')]/strong").extract_first().getall()
        title = response.xpath("//h1").getall()
        item = {"collection": title[0], "pid": collection.pid}
        self.items.append(item)

        yield item

    def start_requests(self):
        if self.url is not None:
            script = """
                   function main(splash)
                       assert(splash:go(splash.args.url))
                       assert(splash:wait(2))
                       local element = splash:jsfunc(
                        [[ function(select) {
                        var el=document.getElementsByName('results-per-page')
                        el[0].getElementsByTagName('option')[3].selected='selected'
                        return el[0].getElementsByTagName('option')[3].innerHTML
                        }]])
                       element()
                       return {
                            html = splash:html()
                       }
                   end
                   """

            len_collections = 339
            self.bar = ProgressBar(len_collections, max_width=500)
            self.bar.bar.CHAR_FULL = Color("{autoyellow}#{/autoyellow}")
            self.bar.bar.CHAR_LEADING = Color("{autoyellow}#{/autoyellow}")
            self.bar.bar.CHAR_LEFT_BORDER = Color("{autoblue}[{/autoblue}")
            self.bar.bar.CHAR_RIGHT_BORDER = Color("{autoblue}]{/autoblue}")
            self.bar.numerator = 1

            if self.export_collections is not None and self.export_collections is True:
                # export collections eudml

                yield SplashRequest(
                    self.url,
                    self.parse_result,
                    endpoint="execute",
                    cache_args=["lua_source"],
                    args={"lua_source": script},
                )
            else:
                # import collections

                self.prepare_collections = True
                # 2 load_item - start request
                path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
                with open(path + "/data/collections_eudml.json") as f:
                    data = json.load(f)

                    for row in data:
                        url = row["url"]
                        title = row["title"]
                        pid = row["pid"]
                        exclude = row["exclude"]
                        collection = Collection(
                            title=title, url=url, pid=pid, exclude=exclude, count=0, set=""
                        )

                        yield Request(
                            row["url"],
                            callback=self.parse,
                            meta={"collection": collection, "url": row["url"], "exclude": exclude},
                        )
        """
            Parse nodes on each eudml collection
        """
        if self.url is None:
            script = """
                    function main(splash, args)
                         local open_issues = splash:jsfunc([[
                                                 function(){
                                                  var id_nodes=[]
                                                  var listelem = document.getElementsByClassName('toggleable vol')

                                                  for(var i=0; i<=listelem.length -1; i++) {
                                                     var id_node = listelem[i].children[0];
                                                      if(typeof id_node !== "undefined") {
                                                       id_node = id_node.getAttribute("id")
                                                       id_node = String(id_node.replace("span",""))
                                                       id_node=id_node.substring(1)
                                                       id_nodes.push(id_node)
                                                     }
                                                   }

                                               for(var i=0; i<= id_nodes.length; i++){
                                                 volume = $("ul #"+id_nodes[i])
                                                 issues = volume.children()

                                                 for(var j=0; j<=issues.length-1; j++) {
                                                         span = issues[j].firstChild

                                                   if(span != null) {
                                                       if(span.className == "toggleable"){
                                                             span.click()
                                                     }
                                                   }
                                                 }
                                               }
                                               return true
                                            }
                                         ]])

                                       local get_node_volume = splash:jsfunc([[
                                         function (){
                                         var id_nodes=[]
                                         var listelem = document.getElementsByClassName('toggleable vol')
                                         for(var i=0; i<=listelem.length -1; i++) {
                                         var id_node = listelem[i].children[0];

                                         if(typeof id_node !== "undefined") {
                                         id_node = id_node.getAttribute("id")
                                         id_node = String(id_node.replace("span",""))
                                         id_node=id_node.substring(1)
                                         id_nodes.push(id_node)

                                         }
                                       }

                                     for(var i=0; i<= id_nodes.length -1; i++) {
                                           var span = $("#span-"+id_nodes[i])
                                           span.click()

                                            //open articles

                                        }
                                       return true
                                    }
                                  ]])

                               assert(splash:go(args.url))
                               splash.js_enabled = true
                               assert(splash:wait(0.5))
                               assert(get_node_volume() == true)
                               assert(splash:wait(14))

                               assert(splash:wait(2))
                               local issues_opened = open_issues()
                               assert(issues_opened)
                               assert(splash:wait(22))



                               return {
                                  html = splash:html()
                               }

                       end
                    """

            redis_client = self.redis_client
            r = redis_client.get_connection()
            redis_client.exec_cmd(r.set, "failures", 0)
            with open(
                os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
                + "/data/collections_eudml.json"
            ) as f:
                rows = json.load(f)
                # reader = csv.DictReader(f, delimiter=",")
                # rows = list(reader)

                insert_history_event(
                    {
                        "type": "import",
                        "unique_id": self.uuid,
                        "pid": "",
                        "col": "",
                        "source": "EUDML",
                        "status": "OK",
                        "title": "import des collections EUDML",
                        "type_error": "",
                        "userid": self.user.id,
                        "data": {
                            "ids_count": 0,
                            "message": "",
                            "target": "",
                        },
                    }
                )

                if self.restart is not True:
                    len_collections = len(rows)
                else:
                    len_collections = 0
                    for row in rows:
                        pid = row["pid"]
                        exclude = row["exclude"]
                        if pid not in self.numdam_collections:
                            if exclude not in ["yes", "oui"]:
                                issues = Container.objects.filter(
                                    pid__startswith=pid + "_"
                                ).count()
                                if issues == 0:
                                    len_collections += 1

                self.bar = ProgressBar(len_collections, max_width=500)
                self.bar.bar.CHAR_FULL = Color("{autoyellow}#{/autoyellow}")
                self.bar.bar.CHAR_LEADING = Color("{autoyellow}#{/autoyellow}")
                self.bar.bar.CHAR_LEFT_BORDER = Color("{autoblue}[{/autoblue}")
                self.bar.bar.CHAR_RIGHT_BORDER = Color("{autoblue}]{/autoblue}")
                self.bar.numerator = 0
                redis_client.exec_cmd(r.set, "progress", self.bar.numerator)
                redis_client.exec_cmd(r.set, "total", len(rows))

                for row in rows:
                    url = row["url"]
                    title = row["title"]
                    pid = row["pid"]
                    exclude = row["exclude"]

                    if isinstance(exclude, type(None)):
                        exclude = ""
                    self.collection = Collection(
                        title=title, url=url, pid=pid, exclude=exclude, count=0, set=""
                    )

                    if self.restart is True:
                        issues = Container.objects.filter(pid__startswith=pid + "_").count()

                        if issues == 0 or issues is None:
                            len_collections += 1
                            if pid not in self.numdam_collections:
                                if exclude not in ["yes", "oui"]:
                                    yield SplashRequest(
                                        url,
                                        self.parse_nodes_collection,
                                        endpoint="execute",
                                        args={"lua_source": script, "timeout": 36000},
                                        cache_args=["lua_source"],
                                        meta={"collection": self.collection},
                                    )
                    else:
                        if pid not in self.numdam_collections:
                            if exclude not in ["yes", "oui"]:
                                yield SplashRequest(
                                    url,
                                    self.parse_nodes_collection,
                                    endpoint="execute",
                                    args={"lua_source": script, "timeout": 36000},
                                    cache_args=["lua_source"],
                                    meta={"collection": self.collection},
                                )


if __name__ == "__main__":
    process = CrawlerProcess(settings=get_project_settings())
    process.crawl(EudmlSpider)
    process.start()
