import csv
import json
import logging
import logging.handlers
import os
import re
import signal
import traceback
import types
import uuid
from dataclasses import dataclass
from datetime import datetime
from datetime import time
from multiprocessing import Process
from multiprocessing import Queue
from random import random

import regex
import scrapy
from bs4 import BeautifulSoup
from colorclass import Color
from etaprogress.progress import ProgressBar
from psycopg2 import DatabaseError
from psycopg2 import IntegrityError
from scrapy import Request
from scrapy import signals
from scrapy.utils.project import get_project_settings
from scrapy_splash import SplashRequest

from django.conf import settings
from django.dispatch import Signal

from crawl.mathcollector.crawl_eudml import EudmlCrawler
from gdml.models import Periode
from gdml.models import Source
from gdml.signals import collection_created
from gdml.signals import collection_updated
from gdml.signals import send_result_import
from history.views import manage_exceptions
from ptf.cmds import ptf_cmds
from ptf.exceptions import ResourceExists
from ptf.model_data import create_publicationdata
from ptf.model_helpers import *
from ptf.models import Collection as CollectionPtf
from ptf.models import ResourceId

end_crawl = Signal()
import scrapy.crawler as crawler
from middleware.redis_command import redisClient
from pysolr import SolrError
from scrapy.crawler import Crawler
from scrapy.crawler import CrawlerProcess
from scrapy.settings import Settings
from twisted.internet import reactor

from django.contrib.auth.models import User

from crawl.mathcollector.items import Issue
from history.models import HistoryEvent
from history.models import insert_history_event
from ptf.model_helpers import get_collection


class VolumeItems:
    def __init__(self, links, year, volume, number):
        self.volume = volume
        self.year = year
        self.links = links
        self.number = number
        self.urls = []
        self.bar = None


@dataclass
class Collection:
    title: str
    url: str
    pid: str
    exclude: str


class JstorSpider(scrapy.Spider):
    custom_settings = {
        "BOT_NAME": "mathcollector",
        "SPIDER_MODULES": ["mathcollector.spiders"],
        "NEWSPIDER_MODULE": "mathcollector.spiders",
        "ROBOTSTXT_OBEY": True,
        "SPIDER_MIDDLEWARES": {
            "scrapy_splash.SplashDeduplicateArgsMiddleware": 100,
        },
        "DOWNLOADER_MIDDLEWARES": {
            "scrapy_splash.SplashCookiesMiddleware": 723,
            "scrapy_splash.SplashMiddleware": 725,
            # This middleware provides the integration between Scrapy and Splash and is assigned the priority of 725.
            "scrapy.downloadermiddlewares.httpcompression.HttpCompressionMiddleware": 810,
        },
        "SPLASH_URL": "http://localhost:8050",
        # Define Splash DupeFilter
        "DOWNLOADER_CLIENT_TLS_METHOD": "TLSv1.2",
        "DUPEFILTER_CLASS": "scrapy_splash.SplashAwareDupeFilter",
        # Storage Mechanism
        "HTTPCACHE_STORAGE": "scrapy_splash.SplashAwareFSCacheStorage",
        "CONCURRENT_REQUESTS": 16,
        "RANDOMIZE_DOWNLOAD_DELAY": True,
        "DOWNLOAD_DELAY": 2,
        "SPLASH_COOKIES_DEBUG": True,
    }

    name = "eudml_spider"

    @property
    def logger(self):
        return logging.getLogger("eudml_spider")

    def __init__(
        self,
        username=None,
        export_collections=None,
        scrap=False,
        parent_task=None,
    ):
        self.collection = None
        self.urls_collections = []
        self.pos = 0
        self.parent_id = parent_task
        self.export_collections = export_collections
        self.scrap = False
        self.total = None
        self.periode_range = []
        self.begin = 1884
        self.end = 1927
        self.collection_href = "/journal/annamath"
        self.user_agents_list = [
            "Mozilla / 5.0(X11; Linux x86_64) AppleWebKit / 537.36(KHTML, like Gecko) Chrome / 57.0.2987.110 Safari / 537.36 Chrome / 61.0.3163.79 Safari / 537.36",
            "Mozilla / 5.0(X11; Ubuntu; Linux x86_64; rv: 55.0) Gecko / 2010010 Firefox / 55.0",
            "Mozilla / 5.0(X11; Linux x86_64) AppleWebKit / 537.36(KHTML, like Gecko) Chrome / 61.0.3163.91 Safari / 537.36"
            "Mozilla/4.0 (compatible; MSIE 9.0; Windows NT 6.1)",
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 Edg/87.0.664.75",
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 Edge/18.18363",
        ]
        self.items = []
        f_format = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
        f_handler = logging.FileHandler(settings.LOG_EUDML)
        f_handler.setFormatter(f_format)
        self.logger.addHandler(f_handler)
        self.collection = None
        self.redis_client = redisClient()
        r = self.redis_client.get_connection()
        self.redis_client.exec_cmd(r.set, "progress", 0)
        self.redis_client.exec_cmd(r.set, "start_crawl", 1)
        self.redis_client.exec_cmd(r.set, "failures_collection", 0)
        self.username = username
        self.uuid = str(uuid.uuid1())
        self.has_err = False

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super().from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.item_scraped, signal=signals.item_scraped)
        crawler.signals.connect(spider.spider_closed, signal=signals.spider_closed)
        crawler.signals.connect(spider.spider_error, signal=signals.spider_error)
        crawler.signals.connect(spider.spider_closed, signal=signals.engine_stopped)

        return spider

    async def item_scraped(self, item):
        # Send the scraped item to the server

        self.bar.numerator += 1
        print(self.bar)
        print("Request finished!")

    def spider_closed(self, spider, reason):
        print("end spider....")
        spider.logger.info("Spider closed: %s", spider.name)
        spider.logger.info("collection crawler instance: %s", self.collection)
        r = self.redis_client.get_connection()
        self.redis_client.exec_cmd(r.set, "progress", 0)
        self.redis_client.exec_cmd(r.set, "start_crawl", 0)
        errors_count = self.redis_client.exec_cmd(r.get, "failures_collection")
        date_event = datetime.now()
        date_event = str(date_event)
        if not isinstance(self.url, type(None)):
            title = "Préparation des collections EUDML"
        else:
            title = "collections EUDML"
        data_task = {
            "uid": self.uuid,
            "date": date_event,
            "collection": "AM",
            "title": title,
            "username": self.username,
            "total": self.total,
            "success": self.bar.numerator,
            "failures": errors_count,
            "progress": 100,
        }
        print(data_task)
        self.redis_client.exec_cmd(r.xadd, "task:import", data_task)
        len_stream = self.redis_client.exec_cmd(r.xlen, "task:import")
        command_tasks = self.redis_client.exec_cmd(
            r.xrevrange, "task:import", max="+", min="-", count=1
        )
        current_command = command_tasks[0]

        print("get last inserted command")

        """
            attach log collections /var/log/gdml/eudml.log
        """
        # send_result_import.send(sender=self.__class__, url=self.url)

    def spider_error(self, failure, response, spider):
        r = self.redis_client.get_connection()
        item = response.meta["collection"].pid

        spider.logger.info("error : %s", failure)
        spider.logger.info("PID : %s", item)
        user = User.objects.filter(username=self.username)

        history_parent_event = HistoryEvent.objects.filter(unique_id=self.uuid)
        if len(history_parent_event) > 0:
            parent_event = history_parent_event[0]
            parent_event = parent_event.pk
            manage_exceptions(
                "import",
                pid=item,
                colid=item,
                status="ERROR",
                title=response.meta["collection"].title,
                exception=failure.value,
                target=response.meta["collection"].url,
                userid=user[0].id,
                type_error=str(failure.type.args),
                parent_id=parent_event,
            )
        errors_count = int(self.redis_client.exec_cmd(r.get, "failures_collection"))
        errors_count += 1
        history_parent_event = history_parent_event[0]
        history_parent_event.status = "ERROR"
        history_parent_event.data = {
            "message": " - ".join([str(failure.value.response), traceback.format_exc()]),
            "ids_count": 0,
            "target": item,
        }
        history_parent_event.save()
        self.redis_client.exec_cmd(r.set, "failures_collection", errors_count)

    def setResourcesLink(self, resourcesLink):
        resources_to = []
        website = self.collection["website"]

        for entry in resourcesLink:
            is_issue_link = match_issue_href(self.collection["issue_href"], entry)
            if is_issue_link:
                resources_to.append(entry)
        # resources = iter(resources_to)
        yield resources_to

    def getArticle(self, response):
        """
        parse article from json
        """

        yield {"article": ""}

    def articlesLink(self, url, links, volume, number, year):
        """
        import issue with links into collection
        """

        self.item.import_issue(
            url, links=links, year=year, volume=volume, number=number, augment=False
        )

    def getSeries(self, volumes, issue_infos):
        for link_volume in volumes:
            # get articles
            articles = scrapy.Request(
                link_volume,
                callback=self.articlesLink,
                cb_kwargs=dict(
                    volume=issue_infos["volume"] + "-" + issue_infos["number"],
                    year=issue_infos["year"],
                    url=link_volume,
                ),
            )
            yield articles

    def get_metadata(self, response):
        print("article scrap")
        soup = BeautifulSoup(response.text, "html.parser")
        yield soup

    """
        Parse issue or volume
    """

    def parse_volume(self, url):
        xissue = self.item.import_issue(url)
        yield {"issue imported": xissue}

    def getIssueLinks(self, text):
        soup = BeautifulSoup(text, "html.parser")
        esc_domain = re.escape(self.collection["website"])
        reg = re.compile(esc_domain + self.collection["issue_href"])
        issues = []
        if self.collection["periode"] is not None:
            periods = list(range(self.collection["periode"][0], self.collection["periode"][1] + 1))
            links_issue = []
            for period in periods:
                reg_issue = re.compile(
                    esc_domain + r"\/" + str(period) + self.collection["issue_href"]
                )
                links = [
                    e.get("href")
                    for e in soup.find_all("a")
                    if reg_issue.match(str(e.get("href")))
                ]
                for link in links:
                    xissue = self.item.import_issue(link)
                    issues.append(xissue)
        else:
            links = [e.get("href") for e in soup.find_all("a") if reg.match(str(e.get("href")))]
            for link in links:
                xissue = self.item.import_issue(link)

                issues.append(xissue)

    def get_volume(self, volume_links):
        is_gen = isinstance(volume_links, types.GeneratorType)
        reg_url = re.compile("^https|http")
        part_links = {}
        issues = []
        reg = re.compile(r"\/(?P<year>\d{4})\/(?P<vol>\d{2,3})\-(?P<number>\d{1})\/p")
        if not is_gen:
            if isinstance(volume_links[0], list) and not is_gen:
                print("list volume")
                print(volume_links)

                print("cpt")
                print(self.cpt)
                print(".....")
                for x, list_volume in enumerate(volume_links):
                    for y, link_item in enumerate(volume_links[x]):
                        if reg_url.search(link_item):
                            link_issue = link_item
                        else:
                            link_issue = (
                                self.collection["domain"] + link_item
                            )  # elements_volume = ([e for e in soup.find_all('div') if

                        ret = scrapy.Request(str(link_issue), callback=self.parse_volume)
                        self.cpt += 1

                        yield ret

        else:
            if is_gen:
                for index, list_item in enumerate(volume_links):
                    cpt = 0
                    for item in list_item:
                        if reg_url.search(item):
                            link_issue = item
                        else:
                            link_issue = self.collection["domain"] + item
                        # récperation volume - issue

                        # return scrapy.Request(str(link_issue), callback=self.parse_volume, cb_kwargs=dict(url=link_issue))
                        xissue = self.item.import_issue(link_issue)
                        issues.append(xissue)

                    cpt += 1
        yield {"import volume": "ok"}

    def parse_result(self, response, *arg):
        # here you would extract links to follow and return Requests for
        # each of them, with another callback
        body = response.body
        # body = body.decode('utf-8')
        # j = json.loads(response.body_as_unicode())

        if self.url is not None:
            for journal in response.xpath("//h3/a").getall():
                journal = journal.replace("'", repr("'"))
                soup = BeautifulSoup(journal, "html.parser")
                title = soup.text.replace("\n", "")
                reg_url = re.compile(r"/journal/\d+")
                reg_title = re.compile(r"[\w\s]+")
                match_link = reg_url.search(journal)
                if match_link:
                    url = "https://eudml.org" + match_link[0]

                item = Collection(title=title.strip(), url=url)
                yield item
            if self.pos == 0:
                next_url = response.xpath(
                    "//div[@class='unit unit-journal-pagination']/p[@class='pagination']/a/@href"
                ).extract_first()
            else:
                next_url = response.xpath(
                    "//div[@class='unit unit-journal-pagination']/p[@class='pagination']/a/@href"
                )
                if len(next_url) > 1:
                    next_url = next_url[1].root

            if next_url and isinstance(next_url, str):
                self.pos += 1
                print(self.bar)
                yield scrapy.Request("https://eudml.org/" + next_url, self.parse_result)
        else:
            title = response.xpath("//h1").getall()
            item = {"collection": title[0]}
            yield item

    def process_issue(self, crawler, collection, pid, url_articles, parent_event):
        try:
            crawler.import_issue(issue_url=None, url_articles=url_articles)
        except DatabaseError as db_err:
            print(db_err)

            self.logger.error(
                "Error : \n"
                + "pid: "
                + str(pid)
                + "\n col: "
                + collection.pid
                + "\n created on :"
                + str(datetime.now())
                + "\n message : "
                + db_err
            )

            self.has_err = True

        except ResourceExists as err:
            self.logger.error(
                "Error : \n"
                + "pid: "
                + str(pid)
                + "\n col: "
                + collection.pid
                + "\n created on :"
                + str(datetime.now())
                + "\n message : "
                + err.__cause__
            )
            self.has_err = True

        except IntegrityError as integrity_err:
            self.logger.error(
                "Error : \n"
                + "pid: "
                + str(pid)
                + "\n col: "
                + self.collection.pid
                + "\n created on :"
                + str(datetime.now())
                + "\n message : "
                + integrity_err
            )
            self.has_err = True

        except SolrError as solr_err:
            self.logger.error(
                "Error : \n"
                + "pid: "
                + str(pid)
                + "\n col: "
                + collection.pid
                + "\n created on :"
                + str(datetime.now())
                + "\n message : "
                + solr_err.message
            )
            self.has_err = True

        except Exception as ex:
            print("Exception inserting container")
            print(str(pid))
            data_event = {
                "type": "import",
                "pid": str(pid),
                "col": collection.pid,
                "parent": parent_event,
                "status": "ERROR",
                "created_on": str(datetime.now()),
                "data": {"message": ex},
            }
            """
                @todo update task parent
            """
            # event = HistoryEvent.objects.create(**data_event)
            # insert_history_event(data_event)
            self.logger.error(
                "Error : \n"
                + "pid: "
                + data_event["pid"]
                + "\n col: "
                + data_event["col"]
                + "\n created on :"
                + data_event["created_on"]
                + "\n message : "
                + str(data_event["data"]["message"])
            )
            self.has_err = True

    """
        function to parse nodes and crawl collection
    """

    def parse_nodes_collection(self, response, *arg):
        user = User.objects.get(username=self.username)

        redis_client = self.redis_client
        r = redis_client.get_connection()

        parent_event = HistoryEvent.objects.filter(unique_id=self.uuid)

        if len(parent_event) > 0:
            parent_event = parent_event[0]
            parent_event = parent_event.pk

        collectionItem = response.meta["collection"]
        self.collection = collectionItem

        if response.status != 200:
            client = self.redis_client
            r = client.get_connection()
            fails = client.exec_cmd(r.get, "failures_collection")
            fails += 1
            client.exec_cmd(r.set, "failures_collection", fails)
        print(self.collection)
        print("collection ....")
        if self.collection.pid not in self.numdam_collections:
            collection = get_collection(pid=self.collection.pid, sites=False)
            redis_client.exec_cmd(
                r.hset,
                "last_task_import",
                mapping={
                    "id": self.uuid,
                    "username": self.username,
                    "collection": collection.pid,
                    "progress": self.bar.numerator,
                },
            )

            data = {}

            data["pid"] = collection.pid
            if (
                collection.pid in ["PMEC", "EU", "EUG", "EUA", "DEME", "MR"]
                or self.collection.exclude == "yes"
            ):
                return collectionItem

            data["coltype"] = collection.coltype
            data["wall"] = (collection.wall,)
            data["title"] = collection.title_sort
            data["collection_href"] = "/journals/" + collection.pid

            source = Source.objects.get(domain="eudml.org")
            data["source"] = source
            data["domain"] = source.domain
            data["website"] = source.website
            data["pdf_href"] = ""
            data["article_href"] = ""
            data["periode_href"] = ""
            data["issue_href"] = ""
            collection_crawler = EudmlCrawler(**data)
            collection_crawler.create_xissue = True
            body = response.body
            body = body.decode("utf-8")
            # j = json.loads(response.body_as_unicode())

            text = body.strip().replace('"', "").replace("\\", "")
            soup = BeautifulSoup(text, "html.parser")
            # récuperation des noeuds volume

            volumes_bloc = soup.find_all("li", {"class": "toggleable"})
            volume_nodes = soup.findAll("li", {"role": "treeitem"})

            len_volumes = len(volume_nodes)
            settings = Settings()
            crawler = self.crawler

            reg_year = re.compile(r"\d{4}")

            for volume in volume_nodes:
                span_volume = volume.find("span")

                volume_text = span_volume.findChildren("strong", recursive=False)
                volume_number = volume_text[0].get_text()
                year = None

                if len(volume_text) > 1:
                    if reg_year.search(volume_text[1].get_text()):
                        year = reg_year.search(volume_text[1].get_text())[0]
                else:
                    if reg_year.search(volume_text[0].get_text()):
                        year = reg_year.search(volume_text[0].get_text())[0]
                reg_strip_chars = re.compile(r"\/")
                if isinstance(year, str):
                    if reg_strip_chars.search(year):
                        year = year.split("/")
                        year = year[0]
                issue_id = span_volume.get("id")
                issue_pid_part = issue_id.split("journal-")
                issue_pid = issue_pid_part[1]

                issue_pid = issue_pid[:-1]
                regexp_volume = regex.compile(r"\d{4}\/\d{4}")
                regex_volume_prefix = regex.compile(r"vol\. \d+")
                if regex_volume_prefix.search(volume_number):
                    volume_number = regex_volume_prefix.search(volume_number)[0]
                    volume_number = volume_number.split(" ")

                    volume_number = volume_number[1]

                if regexp_volume.match(volume_number):
                    volume_number = volume_number.split("/")
                    volume_number = volume_number[0]
                if volume_number == "000":
                    volume_number = "0"
                if volume_number[:2] == "00":
                    volume_number = volume_number[2:]

                id_container = "_" + str(volume_number)
                issue_present = [span for span in volume.find_all("span") if "issue" in span.text]
                url_articles = []

                year_str = year if not isinstance(year, type(None)) else ""
                has_err = False
                if len(issue_present) > 0 and year_str != "":
                    # récuperation issues
                    for index_issue, issue in enumerate(issue_present):
                        issue_pid = collection.pid + "_" + year_str + "__" + volume_number

                        class_el = issue.get("class")[0]
                        if issue.get("class")[0] != "artsp":
                            el = issue.get("id")
                            id_issue_part = el.split("_")
                            pos_number = len(id_issue_part) - 1
                            issue_number = issue.find_next("strong").get_text()
                            if regex.match(r"(\d+)-(\d+)", issue_number):
                                issue_part = issue_number.split("-")
                                issue_number = issue_part[0]
                            el = el.split("span-")

                            list_articles = soup.find("ul", {"id": el[1]})
                            if list_articles is not None:
                                url_articles = [a.get("href") for a in list_articles.find_all("a")]
                            # prefix url eudml each articles
                            for index, url in enumerate(url_articles):
                                url_articles[index] = "https://eudml.org" + url
                            print(list_articles)
                            if re.compile("[azAZ]+").search(issue_number):
                                issue_number = "1"
                            if re.compile("Suppl").search(issue_number):
                                issue_number = str(index_issue)
                            issue_pid += "_" + issue_number
                            issue = Issue(
                                pid=issue_pid,
                                number=issue_number,
                                year=year,
                                volume=volume_text[0].get_text(),
                                journal=self.collection,
                                create_xissue=True,
                            )
                            collection_crawler.issue = issue

                            self.process_issue(
                                collection_crawler,
                                collection,
                                issue_pid,
                                url_articles,
                                parent_event,
                            )
                # import issue on the fly
                else:
                    if year_str != "":
                        collection_crawler.create_xissue = True
                        issue_pid = collection.pid + "_" + year_str + "__" + volume_number
                        issue = Issue(
                            pid=issue_pid,
                            number=None,
                            year=year,
                            volume=volume_number,
                            journal=self.collection,
                            create_xissue=True,
                        )
                        collection_crawler.issue = issue
                        articles_node = volume.find_all("li", {"class": "toggleable"})

                        for article in articles_node:
                            article_link = article.find("a").get("href")
                            article_link = "https://eudml.org" + article_link
                            if article_link in url_articles:
                                pos_article_issue = url_articles.index(article_link)
                                url_articles.pop(pos_article_issue)
                            else:
                                url_articles.append(article_link)

                        self.process_issue(
                            collection_crawler, collection, issue_pid, url_articles, parent_event
                        )

            print(self.bar)
            redis_client.exec_cmd(r.set, "progress", self.bar.numerator)
        # set error mesage
        if self.has_err == True:
            history_parent_event = HistoryEvent.objects.get(unique_id=self.uuid)
            history_parent_event.status = "ERROR"
            history_parent_event.save()
        print("collections imported")
        return collectionItem

    def parse(self, response, *arg):
        # here you would extract links to follow and return Requests for
        # each of them, with another callback

        body = response.body
        body = body.decode("utf-8")
        # j = json.loads(response.body_as_unicode())

        text = body.strip().replace('"', "").replace("\\", "")
        soup = BeautifulSoup(text, "html.parser")
        # récuperation des noeuds volume

        volumes_bloc = soup.find_all("li", {"class": "toggleable"})
        volume_nodes = soup.findAll("li", {"role": "treeitem"})
        title_collection = soup.find("h1")
        if title_collection:
            title_collection = title_collection.get_text()
        meta = soup.find_all("meta")
        fyear = ""
        lyear = ""
        extid = ""
        lang = ""
        pid_journal = ""
        print(self.bar)
        try:
            for meta_header in meta:
                if meta_header.get("name") is not None:
                    meta_info = meta_header.get("name")
                    if "citation_language" in meta_info:
                        lang = meta_header.get("content")
                        lang = lang.split("/")
                        if len(lang) > 0:
                            lang = lang[0][:3]

                    if "citation_year" in meta_info:
                        periode = meta_header.get("content")
                        periode_range = periode.split(",")
                        if lyear == "":
                            lyear = periode_range[len(periode_range) - 1]
                        fyear = periode_range[0]
                        fyear_start = fyear.split("-")
                        if len(fyear_start) > 0:
                            fyear = fyear_start[0]
                        if len(fyear_start) > 1:
                            lyear = fyear_start[1]
                    if "citation_id" in meta_info:
                        extid = meta_header.get("content")
                        extid = extid.split("/")
                        if len(extid) > 0:
                            extid = extid[0]
                            id_part = extid.split(":")
                            pid_journal = id_part[len(id_part) - 1]

        except Exception as e:
            print(e)
        try:
            issn = soup.find("strong", text="ISSN:").parent.get_text()
            issn = issn.split("ISSN:")
            if len(issn) > 1:
                issn = issn[1].strip()
            else:
                issn = None
        except:
            issn = None
        pid = pid_journal
        if "collection" in response.meta:
            collection = response.meta["collection"]
            pid = collection.pid

        self.collection = {
            "title": collection.title,
            "fyear": fyear,
            "coltype": "journal",
            "lyear": lyear,
            "wall": 0,
            "lang": lang,
            "pid": pid,
            "issn": issn,
        }

        coll = None

        if self.list_cols:
            with open(os.path.join(settings.LOG_DIR, "eudml_collections.txt"), "a") as file_:
                file_.write(title_collection + "\n")
        else:
            if response.meta["exclude"] == "yes":
                pass
            else:
                coll = self.parse_collection(url=response.meta["url"])
                next(coll)
        # volumes = response.xpath("//li[contains(@class,'toggleable') and contains(@role, 'treeitem')]/span[contains(@class,'toggleable')]/strong").extract_first().getall()
        reg_volume = re.compile(r"<strong>\d{1,3}</strong>")
        title = response.xpath("//h1").getall()
        item = {"collection": title[0]}
        self.items.append(item)

        yield item

    def create_publisher(self):
        print("create pub")

    def start_requests(self):
        self.collection = CollectionPtf.objects.get(pid="AM")
        self.create_publisher()

        script = """
                function main(splash, args)

                    local all_headers = {}

                     splash:on_response_headers(function(response)
                        local acceptranges = response.headers["accept-ranges"]
                        local cookie = response.headers["set-cookie"]
                        all_headers["accept-ranges"] = accept-ranges
                        all_headers["set-cookie"] = cookie

                    end)

                           splash.js_enabled = true

                           assert(splash:wait(2))
                           local cookies = splash:get_cookies()
                           splash:init_cookies(cookies)


                           local headers = {
                                            ["Accept"]= "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
                                            ["Accept-Encoding"] = "gzip, deflate, br",
                                            ["Accept-Language"] = "fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3",
                                            ["Connection"] = "keep-alive",
                                            ["Host"] = "www.jstor.org",
                                            ["Referer"] = "https://www.jstor.org/journal/annamath",
                                            ["Sec-Fetch-Dest"] = "document",
                                            ["Sec-Fetch-Mode"] = "navigate",
                                            ["Sec-Fetch-Site"] = "none",
                                            ["Sec-Fetch-User"] = "?1",
                                            ["Sec-GPC"] = "1",
                                            ["TE"] = "trailers",
                                            ["Upgrade-Insecure-Requests"] = "1",
                                            ["User-Agent"] = "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/119.0"
                                         }

                            local open_issues = splash:jsfunc([[
                                             function(response){
                                              let links=[]
                                              var body = document.body;
                                              var id_nodes=[1880,1890,1900,1910,1920]
                                              var elems = body.getElementsByTagName('details')

                                              for(let elem of elems) {
                                                elem.children[0].click()

                                                 }
                                              }
                                              return true
                                           }
                                     ]])

                           splash:set_custom_headers(headers)
                           assert(splash:wait(2))
                           assert(splash:go("https://www.jstor.org/journal/annamath"))
                           assert(splash:wait(2))


                            local title = splash:evaljs("document.title")
                            splash:on_response(function(response)
                                 assert(splash:wait(4))
                                 assert(open_issues() == true)
                           end)
                           assert(splash:wait(2))
                           assert(splash:go("https://www.jstor.org/journal/annamath/decade/AXllYXI6WzE5MjAgVE8gMTkzMH0"))
                           assert(splash:wait(1))

                             return { html= splash:html() }



                   end
                """

        user = User.objects.get(username=self.username)

        insert_history_event(
            {
                "type": "import",
                "unique_id": self.uuid,
                "pid": "",
                "col": "AM",
                "status": "OK",
                "title": "import des numéros Annals of Mathematics - période JSTOR",
                "type_error": "",
                "userid": user.id,
                "data": {
                    "ids_count": 0,
                    "message": "",
                    "target": "",
                },
            }
        )
        redis_client = self.redis_client
        r = redis_client.get_connection()

        self.scrap = True
        # self.collection = Collection(title=title, url=url, pid=pid, exclude=exclude)
        redis_client.exec_cmd(
            r.hset,
            "last_task_import",
            mapping={
                "id": self.uuid,
                "username": self.username,
                "collection": self.collection.pid,
                "progress": "0",
            },
        )
        url = "https://www.jstor.org"
        HEADERS = {
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
            "Accept-Encoding": "gzip, deflate, br",
            "Accept-Language": "fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3",
            "Connection": "keep-alive",
            "Host": "www.jstor.org",
            "Sec-Fetch-Dest": "document",
            "Sec-Fetch-Mode": "navigate",
            "Sec-Fetch-Site": "same-origin",
            "Sec-Fetch-User": "?1",
            "Sec-GPC": "1",
            "Upgrade-Insecure-Requests": "1",
            "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/119.0",
        }
        ua = self.user_agents_list[random.randint(0, len(self.user_agents_list) - 1)]
        yield SplashRequest(
            url,
            self.parse_nodes_collection,
            endpoint="execute",
            args={"wait": 2, "lua_source": script},
            headers=HEADERS,
            cache_args=["lua_source"],
            meta={"collection": self.collection},
        )
