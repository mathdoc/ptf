import re
import time
import calendar
from urllib.request import Request

import scrapy
from middleware.redis_command import redisClient
from scrapy.downloadermiddlewares.retry import RetryMiddleware
from scrapy.downloadermiddlewares.retry import get_retry_request
from scrapy.utils.response import response_status_message
from django.utils import timezone
from history.models import HistoryEvent
from history.views import manage_exceptions
from urllib3.exceptions import HTTPError as BaseHTTPError
from twisted.internet.error import TCPTimedOutError, TimeoutError

import warnings
from logging import Logger, getLogger
from typing import Optional, Type, Union

from scrapy.exceptions import NotConfigured, ScrapyDeprecationWarning
from scrapy.http.request import Request
from scrapy.settings import Settings
from scrapy.spiders import Spider
from scrapy.utils.misc import load_object
from scrapy.utils.python import global_object_name
from scrapy.utils.response import response_status_message

retry_logger = getLogger(__name__)

"""
    class from RetryMiddleware
    see: https://doc.scrapy.org/en/latest/topics/downloader-middleware.html#module-scrapy.downloadermiddlewares.retry
    request response interceptor from spider
"""


class RetryMiddlewareSpider(RetryMiddleware):

    def get_retry_request(
        request: Request,
        *,
        spider: Spider,
        reason: Union[str, Exception, Type[Exception]] = "unspecified",
        max_retry_times: Optional[int] = None,
        priority_adjust: Optional[int] = None,
        logger: Logger = retry_logger,
        stats_base_key: str = "retry",
    ):
        """
        Returns a new :class:`~scrapy.Request` object to retry the specified
        request, or ``None`` if retries of the specified request have been
        exhausted.

        For example, in a :class:`~scrapy.Spider` callback, you could use it as
        follows::

            def parse(self, response):
                if not response.text:
                    new_request_or_none = get_retry_request(
                        response.request,
                        spider=self,
                        reason='empty',
                    )
                    return new_request_or_none

        *spider* is the :class:`~scrapy.Spider` instance which is asking for the
        retry request. It is used to access the :ref:`settings <topics-settings>`
        and :ref:`stats <topics-stats>`, and to provide extra logging context (see
        :func:`logging.debug`).

        *reason* is a string or an :class:`Exception` object that indicates the
        reason why the request needs to be retried. It is used to name retry stats.

        *max_retry_times* is a number that determines the maximum number of times
        that *request* can be retried. If not specified or ``None``, the number is
        read from the :reqmeta:`max_retry_times` meta key of the request. If the
        :reqmeta:`max_retry_times` meta key is not defined or ``None``, the number
        is read from the :setting:`RETRY_TIMES` setting.

        *priority_adjust* is a number that determines how the priority of the new
        request changes in relation to *request*. If not specified, the number is
        read from the :setting:`RETRY_PRIORITY_ADJUST` setting.

        *logger* is the logging.Logger object to be used when logging messages

        *stats_base_key* is a string to be used as the base key for the
        retry-related job stats
        """
        settings = spider.crawler.settings
        assert spider.crawler.stats
        stats = spider.crawler.stats
        retry_times = request.meta.get("retry_times", 0) + 1
        if max_retry_times is None:
            max_retry_times = request.meta.get("max_retry_times")
            if max_retry_times is None:
                max_retry_times = settings.getint("RETRY_TIMES")
        if retry_times <= max_retry_times:
            logger.debug(
                "Retrying %(request)s (failed %(retry_times)d times): %(reason)s",
                {"request": request, "retry_times": retry_times, "reason": reason},
                extra={"spider": spider},
            )
            new_request: Request = request.copy()
            new_request.meta["retry_times"] = retry_times
            new_request.dont_filter = True
            if priority_adjust is None:
                priority_adjust = settings.getint("RETRY_PRIORITY_ADJUST")
            new_request.priority = request.priority + priority_adjust

            if callable(reason):
                reason = reason()
            if isinstance(reason, Exception):
                reason = global_object_name(reason.__class__)

            stats.inc_value(f"{stats_base_key}/count")
            stats.inc_value(f"{stats_base_key}/reason_count/{reason}")
            return new_request
        stats.inc_value(f"{stats_base_key}/max_reached")
        logger.error(
            "Gave up retrying %(request)s (failed %(retry_times)d times): " "%(reason)s",
            {"request": request, "retry_times": retry_times, "reason": reason},
            extra={"spider": spider},
        )
        return None
    def _retry(self, request, reason, spider):
        max_retry_times = request.meta.get("max_retry_times", self.max_retry_times)
        priority_adjust = request.meta.get("priority_adjust", self.priority_adjust)
        retry_times = request.meta.get("retry_times")
        return get_retry_request(
            request,
            reason=reason,
            spider=spider,
            max_retry_times=max_retry_times,
            priority_adjust=priority_adjust,
        )
    def process_exception(self, request, exception, spider):

        new_request: Request = request.copy()

        if isinstance(exception, TimeoutError) or isinstance(exception, TCPTimedOutError) or isinstance(exception, BaseHTTPError) and not request.meta.get(
            "dont_retry", False
        ):
            if isinstance(request.meta.get("retry_times"), type(None)):
                retry_times = 1

            else:
                retry_times = request.meta.get("retry_times") + 1

            new_request.meta["retry_times"] = retry_times
            new_request.dont_filter = True
        return self._retry(new_request, exception, spider)





    def process_response(self, request, response, spider):
        if request.meta.get("dont_retry", False):
            return response
        # add type error
        self.retry_http_codes.add(400)

        if response.status in self.retry_http_codes :
            reason = response_status_message(response.status)

            if spider.retry_count >= 3 or request.meta.get("retry_times") == 3:
                history_parent_event = HistoryEvent.objects.filter(unique_id=spider.uuid)
                redis_client = redisClient()
                r = redis_client.get_connection()
                errors_count = int(redis_client.exec_cmd(r.get, "failures_collection"))
                errors_count += 1
                total = spider.bar.denominator
                redis_client.exec_cmd(r.set, "failures_collection", errors_count)
                collection = request.meta.get("collection")

                if len(history_parent_event) > 0:
                    parent_event = history_parent_event[0]
                    parent_event.status = "ERROR"
                    parent_event.data["message"] = "Gateway 504"
                    parent_event.data["target"] = collection.pid
                    parent_event.save()
                    parent_event_id = parent_event.pk
                    type_error = "Connection réseau perdue ou hôte injoignable"
                    # get current collection to scrap : record.spider.collection
                    spider.collection = collection

                    manage_exceptions(
                        "import",
                        pid=collection.pid,
                        colid=collection.pid,
                        status="ERROR",
                        title=collection.title,
                        exception="Le site distant est injoignable - Gateway 504",
                        target=request.url,
                        userid=spider.user.pk,
                        type_error="Gateway 504",
                        parent_id=parent_event_id,
                    )
                    return scrapy.exceptions.CloseSpider("Gateway 504")

            spider.retry_count += 1
            return self._retry(request, reason, spider)

        if response.status not in self.retry_http_codes:
            spider.retry_count = 0

        return response
