import re
from html import unescape
from urllib.parse import urljoin

import regex
import requests
from alive_progress import alive_bar
from bs4 import BeautifulSoup
from pylatexenc.latex2text import LatexNodes2Text

from django.utils import timezone

from gdml.models import Periode
from gdml.signals import update_years_periode
from history.models import insert_history_event
from ptf import model_data
from ptf import model_data_converter
from ptf import model_helpers
from ptf.cmds import ptf_cmds
from ptf.cmds import xml_cmds
from ptf.cmds.ptf_cmds import addContainerPtfCmd
from ptf.cmds.xml.xml_utils import escape
from ptf.cmds.xml.xml_utils import get_contrib_xml
from ptf.exceptions import ResourceDoesNotExist
from ptf.model_data import JournalData
from ptf.model_data import create_publisherdata
from ptf.models import Provider

from .mathcollector.items import Issue


class HdmlCrawler:
    def __init__(self, *args, **kwargs):
        self.pid = kwargs["pid"]
        self.collection = model_helpers.get_collection(self.pid, sites=False)
        if not self.collection:
            raise ResourceDoesNotExist(f"Resource {self.pid} does not exist")

        self.domain = kwargs.get("domain", None)
        self.website = kwargs["website"]
        self.issue_href = kwargs.get("issue_href", None)
        self.article_href = kwargs.get("article_href", None)
        self.pdf_href = kwargs.get("pdf_href", None)
        self.periode = kwargs.get("periode", None)
        self.numbers = kwargs.get("numbers", None)
        self.periode_range = None
        self.parent_cmd_id = kwargs.get("parent_cmd_id", None)
        self.doi_href = kwargs.get("doi_href", None)
        self.periode_href = kwargs.get("periode_href", None)
        self.pre_publish = kwargs.get("pre_publish", None)
        self.create_xissue = kwargs.get("create_xissue", None)
        self.collection_href = kwargs.get("collection_href", None)
        self.container_added = ""
        self.issue_links = kwargs.get("issue_links", None)
        self.id = kwargs.get("id", None)
        self.container = None
        self.issue = None
        self.title = kwargs.get("title", None)
        publisher_name = kwargs.get("publisher", None)
        self.source = kwargs.get("source", None)
        self.userid = kwargs.get("userid", None)

        if publisher_name is None and self.source is not None:
            publisher_name = self.source.name

        publisher = model_helpers.get_publisher(publisher_name)
        if publisher is None:
            publisher = model_data.PublisherData()
            publisher.name = publisher_name
            publisher = ptf_cmds.addPublisherPtfCmd({"xobj": publisher}).do()

        self.publisher = publisher_name

        self.latext_parser = LatexNodes2Text()
        self.is_json = kwargs.get("json_parser", None)

    def get_links_by_numbers(self, soup, pattern=None):
        print("start exctract url numbers")

        print(self.numbers[0])
        print(self.numbers[1])
        range_numbers = list(range(int(self.numbers[0]), int(self.numbers[1]) + 1))
        links = []
        print(range_numbers)
        links_match = []
        pattern = self.collection_href + "/" + pattern
        for number in range_numbers:
            if len(str(number)) == 1:
                number_pattern = re.compile(
                    self.collection_href
                    + "/"
                    + "(?P<number>0?(?<!["
                    + str(number + 1)
                    + r"\-9])("
                    + str(number)
                    + r"(?!\d+)))"
                )
                # '\/?(?<number>((0)?3{1}([AZ]+)?))\/?$'
                links_match += [
                    unescape(e.get("href"))
                    for e in soup.find_all("a")
                    if number_pattern.search(e.get("href").replace("\xa0", ""))
                ]

            if len(str(number)) == 2:
                # \/?(?<number>((0)?3{1}$([AZ]+)?))-?([09-AZ]+)\/?$
                number_pattern = regex.compile(
                    self.collection_href + "/" + r"\/?(?<number>((" + str(number) + r"[1-9]?)))"
                )
                links = [
                    unescape(e.get("href"))
                    for e in soup.find_all("a")
                    if number_pattern.search(e.get("href").replace("\xa0", ""))
                ]
                print(links)
                links_match += links
            print(pattern)

            print("links matchs")
            print(links_match)
        links = []
        for link in links_match:
            link = self.website + "/" + link
            links.append(link)
        return links

    def get_links_in_html(self, html, link_to_find, url_to_start=None, url_from=None):
        links = []
        """if (self.issue is None or self.issue == ""):
            link_to_find = self.collection_href+link_to_find
        print(link_to_find)
        print('link to find ______')
        """

        soup = BeautifulSoup(html, "lxml")

        # issue links are not fetched
        if self.issue_links is None or len(self.issue_links) == 0:
            if self.numbers is not None and isinstance(self.issue, type(None)):
                if len(self.numbers) > 0:
                    """
                    get url from numbers
                    """
                    pattern = self.issue_href
                    return self.get_links_by_numbers(soup, pattern=pattern)

            if self.issue_links is None or len(self.issue_links) == 0:
                reg_issue = regex.compile(self.collection_href + "/" + self.issue_href)
                print("***")
                print(reg_issue)
                links = [
                    (self.website + "/" + a.get("href"))
                    for a in soup.find_all("a")
                    if reg_issue.search(str(a.get("href")))
                ]
                print(links)
        else:
            reg_article = regex.compile(link_to_find)
            links = [
                (a.get("href"))
                for a in soup.find_all("a")
                if reg_article.search(str(a.get("href")))
            ]
        return links

    def setUrlSource(self, url):
        self.website = url

    """
        get links of collection called from process task
    """

    def get_collection_issues(self, url_to_start=None, periode_range=None):
        html = requests.get(
            self.website + "/" + self.collection_href, verify=False, timeout=30.0
        ).content

        self.periode = Periode.objects.get(id=self.id)

        urls = self.get_links_in_html(html, self.issue_href, url_to_start)

        self.issue_links = urls

        return self.issue_links

    """
        Import collection called from cmd
    """

    def import_collection(self, url_to_start=None, augment=False):
        html = requests.get(
            self.website + "/" + self.collection_href, verify=False, timeout=30.0
        ).content

        self.periode = Periode.objects.get(id=self.id)

        urls = self.get_links_in_html(html, self.issue_href, url_to_start)

        self.issue_links = urls

        with alive_bar(
            len(urls),
            dual_line=True,
            title=f"Init {self.pid} - YYYY XX YY",
            stats="(eta {eta})",
            force_tty=True,
        ) as bar:
            for index, url in enumerate(urls):
                self.import_issue(url, augment, bar)
                bar()

    def import_issue(self, issue_url, augment=False, bar=None, url_articles=None):
        print("====================")
        print("import issue")

        # check for acta
        if self.collection.coltype == "journal" or self.collection.coltype == "acta":
            xissue = model_data.IssueData()
            xissue.ctype = "issue"
        else:
            xissue = model_data.BookData()
            xissue.ctype = "book-monograph"
            xissue.articles = []
        provider = Provider.objects.filter(name="mathdoc")
        self.collection.provider = provider[0]
        xissue.provider = provider[0]
        xissue.journal = self.collection
        xpub = create_publisherdata()
        xpub.name = self.publisher
        xissue.publisher = xpub
        xissue.last_modified_iso_8601_date_str = timezone.now().isoformat()

        if self.issue is not None:
            if isinstance(self.issue, Issue):
                xissue.volume = self.issue.volume
                xissue.year = self.issue.year
                self.create_xissue = self.issue.create_xissue
                if not self.create_xissue:
                    xissue.number = self.issue.number
                xissue.pid = self.issue.pid
                issue = model_helpers.get_container(xissue.pid)

                if issue is not None and not augment:
                    cmd = addContainerPtfCmd()
                    cmd.set_object_to_be_deleted(issue)
                    cmd.undo()

        if xissue.pid is None:
            xissue.pid = f"{self.pid}_{xissue.year}__{xissue.volume}_{xissue.number}"
            issue = model_helpers.get_container(xissue.pid)

        self.container = xissue.pid

        if not augment and issue is not None:
            cmd = ptf_cmds.addContainerPtfCmd({"pid": issue.pid, "ctype": issue.ctype})
            cmd.set_provider(provider=issue.provider)
            cmd.add_collection(self.collection)
            cmd.set_object_to_be_deleted(issue)
            cmd.undo()

        if bar:
            bar.title = f"Init {self.pid} - {xissue.year} {xissue.volume} {xissue.number}"
        else:
            print(issue_url, xissue.year, xissue.volume, xissue.number)
        xissue.number = regex.search(r"((0)?(\d+))", issue_url)
        xissue.number = xissue.number[0]
        attempt = 0
        done = False
        while not done and attempt < 3:
            try:
                if url_articles is not None:
                    self.parse_issue(xissue, url_articles=url_articles)
                else:
                    self.parse_issue(xissue, issue_url, bar=bar)
                done = True
            except (requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout):
                attempt += 1
                xissue.articles = []
        if attempt == 3:
            print("insert exception")
            # création d'un numéro non existant
            if self.create_xissue:
                issue = model_helpers.get_container(xissue.pid)

        if augment:
            for xarticle in xissue.articles:
                self.augment_article(xarticle)
        else:
            self.container = xissue.pid

            if not self.create_xissue:
                if self.collection.coltype == "journal":
                    params = {
                        "xissue": xissue,
                        "use_body": False,
                    }
                    cmd = xml_cmds.addIssueXmlCmd(params)
                    provider = Provider.objects.filter(name="mathdoc")
                    xissue.provider = provider[0]
                else:
                    xbook = xissue
                    data_col = model_data_converter.db_to_collection_data(self.collection)
                    data_col.volume = xissue.volume
                    data_col.collection = data_col
                    xbook.incollection = [data_col]

                    xbook.parts = xissue.articles
                    xbook.body = None

                    params = {"xbook": xbook, "use_body": False}
                    cmd = xml_cmds.addBookXmlCmd(params)

                issue = cmd.do()

                """
                if year != "":
                    update_years_periode.send(sender=self.__class__, periode=periode, year=year)
                """
            insert_history_event(
                {
                    "type": "import",
                    "pid": xissue.pid,
                    "col": self.pid,
                    "parent_id": self.parent_cmd_id,
                    "status": "OK",
                    "title": self.title,
                    "userid": self.userid,
                    "data": {
                        "ids_count": 0,
                        "message": "",
                        "target": "",
                    },
                }
            )

            return issue

    def parse_issue(self, xissue, issue_url=None, url_articles=None, bar=None):
        seq = 1

        articles_list = []
        # if url_articles is not None:
        #   articles_list = url_articles

        if isinstance(url_articles, type(None)):
            try:
                issue_html = requests.get(issue_url, timeout=30.0, verify=False).content
            except TimeoutError:
                raise BlockingIOError(issue_url + "can't be reached")
            except Exception as e:
                raise Exception(e.__cause__)
            soup = BeautifulSoup(issue_html, "lxml")
            reg_issue = re.compile(r"(Issue|Number) (?P<issue>\d{1,3})")
            reg_year = re.compile(r"\d{4}")
            year_url = ""

            reg_volume_year = re.compile(r"(?P<year>\d{4}),\s+Volume \w+")
            reg_volume = re.compile(r"Volume (?P<vol>\d{1,3})")

            volume_infos = [
                e.get_text() for e in soup.find_all("p") if reg_volume.search(e.get_text())
            ]

            number = None
            year = None
            volume = None

            print("parse issue >>>>>")
            if xissue.year == "":
                if year_url != "" and year_url is not None:
                    year = year_url[0]
                if year:
                    xissue.year = year
                if year_url == "":
                    year_url = reg_year.search(issue_url)
                    if year_url:
                        part_year = year_url[0]
                        xissue.year = part_year
                if year_url and xissue.year == "":
                    year = year_url[0].split("/")
                    if len(year) > 1:
                        xissue.year = year[1]
            if xissue.volume is None or xissue.volume == "":
                if len(volume_infos) == 0:
                    # find volume in div
                    volume_infos = [
                        e.get_text(strip=True).replace("\xa0", " ")
                        for e in soup.find_all("div")
                        if reg_volume.match(e.get_text(strip=True).replace("\xa0", " "))
                    ]
                if len(volume_infos) == 0:
                    # find volume in font tag
                    volume_infos = [
                        e.get_text(strip=True).replace("\xa0", " ")
                        for e in soup.find_all("font")
                        if reg_volume_year.search(e.get_text(strip=True).replace("\xa0", " "))
                    ]

                if len(volume_infos) == 0:
                    volume_infos = [
                        e.get_text(strip=True).replace("\xa0", " ")
                        for e in soup.find_all("p")
                        if reg_volume.search(e.get_text(strip=True).replace("\xa0", " "))
                    ]

                if len(volume_infos) == 0:
                    reg_volume = re.compile(
                        r"\/(?P<year>\d{4})\/(?P<vol>\d{1,3})\-(?P<number>\d{1})\/p"
                    )
                    volume_infos = [
                        e.get_text(strip=True).replace("\xa0", " ")
                        for e in soup.find_all("font")
                        if reg_volume.search(e.get_text(strip=True).replace("\xa0", " "))
                    ]

                if len(volume_infos) == 0:
                    volume_infos = [
                        e.get_text(strip=True).replace("\xa0", " ")
                        for e in soup.find_all("h2")
                        if reg_volume.search(e.get_text(strip=True).replace("\xa0", " "))
                    ]

                if len(volume_infos) > 0:
                    if reg_volume.search(volume_infos[0]):
                        volume = reg_volume.search(volume_infos[0])[0]
                        volume = re.findall(r"\d+", volume)[0]
                    if reg_issue.search(volume_infos[0]):
                        text_serie = reg_issue.search(volume_infos[0])[0]
                        if text_serie:
                            number = re.compile(r"\d").search(text_serie)
                            number = number[0]

                    if xissue.number is None or xissue.number == "":
                        xissue.number = number
                    if xissue.volume is None or xissue.volume == "":
                        xissue.volume = volume
                    if xissue.year is None or xissue.year == "":
                        xissue.year = year

                if xissue.volume is None or xissue.volume == "":
                    try:
                        volume_bloc = soup.find_all("font")
                        volume = [
                            e.get_text() for e in volume_bloc if reg_volume.search(e.get_text())
                        ]
                    except:
                        print("no volume found in element")
            xissue.pid = f"{self.pid}_{xissue.year}__{xissue.volume}_{xissue.number}"
            print(xissue.pid)
            print("parse volume, year")
            if xissue.year == "" and xissue.volume != "":
                year_bloc = [
                    e.get_text()
                    for e in soup.find_all("h2")
                    if re.search(xissue.volume, e.get_text())
                ]
                if len(year_bloc) > 0:
                    year_ext = re.search(r"\d{4}", year_bloc[0])
                    year = year_ext[0]
                    xissue.year = year
                    xissue.pid = f"{self.pid}_{xissue.year}__{xissue.volume}_{xissue.number}"

            print(xissue.pid)
            if isinstance(self.issue, type(None)):
                self.issue = issue_url
            articles_list = self.get_links_in_html(
                issue_html, self.article_href, url_from=issue_url
            )
            print(articles_list)

        for article_url in articles_list:
            xarticle = self.parse_article(self.website + "/" + article_url, xissue=xissue)

            if xarticle:
                if bar:
                    bar.text = article_url
                try:
                    if xarticle is not None and not self.create_xissue:
                        seq += 1
                        xarticle.seq = seq
                        xissue.articles.append(xarticle)
                        print(xissue.articles)

                        if self.collection.coltype != "journal" and hasattr(
                            xarticle, "book_title_html"
                        ):
                            xissue.title_html = xissue.title_text = xarticle.book_title_html
                            xissue.title_xml = f"<book-title-group><book-title>{escape(xissue.title_tex)}</book-title></book-title-group>"
                except Exception as e:
                    print("error appending articles")
                    print(e)

        if (
            self.collection.coltype != "journal"
            and self.collection.coltype != "acta"
            and len(xissue.articles) == 1
        ):
            # Mathnet.ru sometimes creates books without title and with only 1 chapter
            # Skip the useless empty level and create a simple book instead
            xarticle = xissue.articles[0]
            xissue.title_html = xissue.title_text = xarticle.title_tex
            xissue.title_xml = f"<book-title-group><book-title>{escape(xissue.title_tex)}</book-title></book-title-group>"
            xissue.contributors = xarticle.contributors
            xissue.doi = xarticle.doi
            xissue.pid = xarticle.pid
            xissue.ext_links = xarticle.ext_links
            xissue.extids = xarticle.extids
            xissue.streams = xarticle.streams
            xissue.articles = []
        if (
            self.collection.coltype != "journal"
            and self.collection.coltype != "acta"
            and len(xissue.articles) > 1
        ):
            xissue.ctype = "book-edited-book"
        provider = Provider.objects.filter(name="mathdoc")
        xissue.provider = provider[0]

    def parse_article(self, article_url, xissue=None):
        try:
            response = requests.get(article_url, timeout=30.0, verify=False).content
        except TimeoutError:
            raise BlockingIOError(article_url + "can't be reached")
        except Exception as e:
            raise Exception(e.__cause__)
        soup = BeautifulSoup(response, "html.parser")
        meta = soup.find_all("meta")
        volume = ""
        xarticle = model_data.create_articledata()
        xabstract = None
        find_abstract = soup.find("article", {"id": "unit-article-abstract"})
        xarticle.abstracts = []
        if find_abstract:
            section = find_abstract.find("section")
            if section:
                abstract = section.get_text()
                xabstract = {}
                xabstract["tag"] = "abstract"
                xabstract["value_html"] = abstract
                xabstract["value_xml"] = xabstract["value_html"]
                xabstract["value_tex"] = xabstract["value_xml"]
                xabstract["lang"] = "eng"
                xarticle.abstracts.append(xabstract)

        reg_abstract_url = regex.compile("p00$")

        if reg_abstract_url.search(article_url):
            return None

        if meta is None:
            raise requests.exceptions.ConnectionError
        if self.create_xissue is True:
            # retourne le volume présent
            volume_bloc = soup.find("dt", text="Volume")
            if volume_bloc is not None:
                volume = volume_bloc.parent.find("dd").get_text()

            if volume is None or volume == "":
                node_infos_em = soup.find_all("em")

                try:
                    if node_infos_em:
                        title = node_infos_em[0].get_text()
                        xarticle.title_html = title
                        year = node_infos_em[5].get_text()
                        xarticle.year = year
                        pages = node_infos_em[4].get_text()
                        xarticle.page_range = pages
                        xissue.year = year
                        pages_infos = pages.split("-")
                        xarticle.fpage = pages_infos[0]
                        if len(pages_infos) > 1:
                            xarticle.lpage = pages_infos[1]

                except Exception:
                    pass

            # vérification de numéro avec volume
            xissue.volume = xissue.number

            xissue.pid = f"{self.pid}_{xissue.year}__{xissue.number}"

            self.container = xissue.pid

            issue = model_helpers.get_container(xissue.pid)

            if issue is None:
                print("creation numero")

                # fix hdml creation issue
                """
                    Création xissue pour un volume
                """
                year = xissue.year

                ##xissue = create_issuedata()

                xissue.pid = f"{self.pid}_{xissue.year}__{xissue.number}"
                self.container = xissue.pid
                xissue.ctype = "issue"
                xissue.abstracts = [
                    {"tag": "trans-abstract", "lang": "fr", "value_xml": "This is an abstract"}
                ]
                xissue.last_modified_iso_8601_date_str = timezone.now().isoformat()
                provider = Provider.objects.filter(name="mathdoc")

                xissue.provider = provider[0]
                xpub = create_publisherdata()
                xpub.name = self.publisher
                xissue.publisher = xpub

                if xissue.number is None or xissue.number == "":
                    xissue.number = 1

                params = {"xissue": xissue, "use_body": False}

                cmd = xml_cmds.addOrUpdateIssueXmlCmd(params)
                issue = cmd.do()

                periode = Periode.objects.filter(collection=self.collection)
                periode = periode[0]
                year = xissue.year
                update_years_periode.send(sender=self.__class__, periode=periode, year=year)
                print(issue)
                print("--------------------------------------------")

        if title != "":
            xarticle.title_html = xarticle.title_tex = title
        else:
            xarticle.title_tex = xarticle.title_html
        xarticle.title_xml = f"<title-group><article-title>{escape(xarticle.title_tex)}</article-title></title-group>"
        xarticle.lang = "gr"
        if self.collection.coltype != "journal":
            xarticle.parts = []

        # Add article_url as en ExtLink (to display the link in the article page)
        ext_link = model_data.create_extlink()
        ext_link["rel"] = "article"
        ext_link["location"] = article_url
        xarticle.ext_links.append(ext_link)

        """
        collections HDML
        """

        if len(xarticle.contributors) == 0:
            authors = soup.find("strong", text="Authors")
            if not isinstance(authors, type(None)):
                contribs = authors.find_next("em").get_text()
                contribs = contribs.split(",")

            else:
                author = soup.find("strong", text="Author")
                if not isinstance(author, type(None)):
                    contribs = author.find_next("em").get_text()
                    contribs = contribs.split(",")
            for contrib in contribs:
                author = model_data.create_contributor()
                author["role"] = "author"
                author["string_name"] = contrib.replace("\xa0", "")
                author["string_name"] = author["string_name"].replace(",", "").replace("by", "")
                author["contrib_xml"] = get_contrib_xml(author)
                xarticle.contributors.append(author)

        pdf_url = None

        reg_pdf = re.compile("(.pdf)|(.PDF)")
        url_notice = article_url
        ext_link = model_data.create_extlink()
        ext_link["rel"] = "full-text"
        ext_link["location"] = url_notice
        xarticle.ext_links.append(ext_link)
        for link in soup.find_all("a"):
            path = link.get("href")
            if path is not None:
                if self.pdf_href and path.find(self.website + self.pdf_href) == 0:
                    if reg_pdf.search(path):
                        pdf_url = urljoin(self.website, path)
                    else:
                        pdf_url = urljoin(self.website, path)

                if self.pdf_href != "" and path.find(self.pdf_href) == 0 and reg_pdf.search(path):
                    pdf_url = urljoin(self.website, path)

                if self.pdf_href and path.find(self.pdf_href) == 0:
                    pdf_url = urljoin(self.website, path)

                if self.pdf_href == "":
                    if reg_pdf.search(path):
                        if re.search("^[http,https]", path):
                            pdf_url = path
                        if not re.search("^[http,https]", path) and not path.startswith("/"):
                            pdf_url = urljoin(self.website, path)
                        else:
                            pdf_url = urljoin(self.website, path)
                        break

        if isinstance(xarticle.pid, type(None)):
            url_article_part = article_url.split("/")
            len_url_part = len(url_article_part)
            id_article = url_article_part[len_url_part - 2]
            xarticle.pid = xissue.pid + "_" + id_article

        if pdf_url:
            data = {
                "rel": "full-text",
                "mimetype": "application/pdf",
                "location": pdf_url,
                "base": "",
                "text": "Full Text",
            }
            xarticle.streams.append(data)

            # The pdf url is already added as a stream (just above) but might be replaced by a file later on.
            # Keep the pdf url as an Extlink if we want to propose both option:
            # - direct download of a local PDF
            # - URL to the remote PDF
            ext_link = model_data.create_extlink()
            ext_link["rel"] = "article-pdf"
            ext_link["location"] = pdf_url
            xarticle.ext_links.append(ext_link)

        issue = model_helpers.get_container(xissue.pid)
        provider = Provider.objects.filter(name="mathdoc")
        xarticle.provider = provider[0]

        if len(xarticle.lang) >= 3:
            xarticle.lang = xarticle.lang[:2]
        if len(xarticle.trans_lang) >= 3:
            xarticle.trans_lang = xarticle.trans_lang[:2]

        if issue is not None:
            if self.create_xissue:
                cmd = xml_cmds.addArticleXmlCmd(
                    {"issue": issue, "xarticle": xarticle, "use_body": False}
                )
                cmd.set_collection(issue.my_collection)
                cmd.set_provider(provider[0])
                cmd.do()
        return xarticle

    def import_singlearticle(self, articles):
        xarticles = []
        journal = JournalData()
        journal.issn = "0003486X"
        journal.publisher = "JSTOR"
        journal.e_issn = ""

        for article in articles:
            xarticle = self.parse_article(article)
            xarticles.append(xarticle)

        yield {"articles": xarticles}

    """
        Permet la modification d'article existant
    """

    def augment_article(self, xarticle):
        if not xarticle.pid and not xarticle.doi:
            raise ValueError("No id")
        pid = xarticle.pid
        if not pid:
            pid = xarticle.doi.replace("/", "_").replace(".", "_").replace("-", "_")

        article = model_helpers.get_article(pid)
        if not article:
            raise ValueError("No {pid}")

        article_data = model_data_converter.db_to_article_data(article)
        links = [item for item in article_data.ext_links if item["rel"] == "article-pdf"]
        if not links:
            links = [item for item in xarticle.ext_links if item["rel"] == "article-pdf"]
            if links:
                ext_link = links[0]
                article_data.ext_links.append(ext_link)

                params = {
                    "xarticle": article_data,
                    "use_body": False,
                    "issue": article.my_container,
                    "standalone": True,
                }

                cmd = xml_cmds.addArticleXmlCmd(params)
                cmd.set_collection(article.get_collection())
                cmd.do()
