from django.templatetags.static import static
from django.urls import reverse
from django.utils.translation import gettext_lazy as _


class AdminActionBar:
    row_name = ""
    actions = None
    allow_tags = True

    def __init__(self, obj):
        self.__class__.__name__ = self.get_row_name() or self.__class__.__name__
        self.obj = obj
        self.actions = self.actions or list()

    def __str__(self):
        return self.to_html()

    def get_row_name(self):
        return str(self.row_name)

    def to_html(self):
        html = ""
        for action in self.actions:
            html += action(self.obj).to_html()
        return html


class Action:
    url = ""
    icon_uri = ""
    icon_title = ""
    success_message = ""
    name = ""

    def __init__(self, obj):
        self.obj = obj

    def __str__(self):
        return self.name.title()

    def is_disable(self):
        return False

    def execute(self):
        return ""

    def get_icon_title(self):
        return self.icon_title

    def get_icon_uri(self):
        return static(self.icon_uri)

    def get_url(self):
        return self.url

    def get_icon(self):
        if self.icon_uri:
            return '<img src="{}" title="{}" height="16px">'.format(
                self.get_icon_uri(), self.get_icon_title()
            )
        return self.__str__()

    def get_success_message(self):
        return self.success_message

    def to_html(self):
        # return '<a type="button" href="{}">{}</a>'.format(
        #     self.get_url(), self.get_icon()
        # )
        return """<form action="{}" style="display: inline;">
                    <button type="submit">{}</button>
                </form>""".format(
            self.get_url(), self.get_icon()
        )


class ChangeAction(Action):
    name = _("Change")

    def get_url(self):
        url_name = f"admin:{self.obj._meta.app_label}_{self.obj._meta.model_name}_change"
        return reverse(url_name, args=[self.obj.pk])


class DeleteAction(Action):
    name = _("Delete")

    def get_url(self):
        url_name = f"admin:{self.obj._meta.app_label}_{self.obj._meta.model_name}_delete"
        return reverse(url_name, args=[self.obj.pk])
