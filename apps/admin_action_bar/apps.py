from django.apps import AppConfig


class AdminActionBarConfig(AppConfig):
    name = "admin_action_bar"
