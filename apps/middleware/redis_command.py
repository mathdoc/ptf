
import redis
import time


class redisClient:

    def __init__(self):
        self.host="localhost"
        self.port=6379
        self.decode_responses=True

    def get_connection(self):
        return redis.Redis(host=self.host, port=self.port, decode_responses=self.decode_responses)
    def exec_cmd(self, command, *args, **kwargs):
        max_retries = 3
        timeout = 5
        count = 0
        while True:
            try:
                return command(*args, **kwargs)
            except (redis.exceptions.ConnectionError, redis.exceptions.TimeoutError):
                count +=1
                if count > max_retries:
                    raise
                print('Retrying in {] seconds'.format(timeout))
                time.sleep(timeout)
            except Exception as ex:
                print("Exception occured {}".format(ex))
