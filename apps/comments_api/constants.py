from django.utils.translation import gettext_lazy as _

# -------- Model constants
STATUS_DRAFT = "draft"
STATUS_SUBMITTED = "submitted"
STATUS_VALIDATED = "validated"
STATUS_SOFT_DELETED = "deleted"
STATUS_REJECTED = "rejected"
COMMENT_STATUS_CHOICES = (
    (STATUS_DRAFT, _("Draft")),
    (STATUS_SUBMITTED, _("Submitted")),
    (STATUS_VALIDATED, _("Validated")),
    (STATUS_REJECTED, _("Rejected")),
    (STATUS_SOFT_DELETED, _("Deleted")),
)

MODERATION_STATUS_CHOICES = (
    (STATUS_VALIDATED, _("Validated")),
    (STATUS_REJECTED, _("Rejected")),
)

STATUS_LIST = [t[0] for t in COMMENT_STATUS_CHOICES]
# Base status for which the comment's deletion is allowed
STATUS_CAN_DELETE = [STATUS_DRAFT, STATUS_SUBMITTED]
# Base status for which the comment's edition is allowed
STATUS_CAN_EDIT = [STATUS_DRAFT, STATUS_SUBMITTED]
# Moderation status
STATUS_MODERATED = [STATUS_VALIDATED, STATUS_REJECTED]

STATUS_PARENT_VALIDATED = [STATUS_VALIDATED, STATUS_SOFT_DELETED]

# -------- Request constants
API_MESSAGE_KEY = "error_message"

PARAM_DASHBOARD = "dashboard"
PARAM_WEBSITE = "website"
PARAM_DOI = "doi"
PARAM_STATUS = "status"
PARAM_COLLECTION = "collection_id"
PARAM_USER = "user_id"
PARAM_MODERATOR = "moderator_id"
PARAM_ADMIN = "admin"
PARAM_PREVIEW = "preview_id"
PARAM_COMMENT = "comment_id"
PARAM_ACTION = "action"

PARAM_ACTION_CREATE = "create"
PARAM_ACTION_DELETE = "delete"
PARAM_BOOLEAN_VALUE = "true"

# --------- Content constants
API_DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%SZ"
