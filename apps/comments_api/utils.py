from datetime import datetime

from .constants import API_DATETIME_FORMAT


def parse_date(date_str: str, format: str = API_DATETIME_FORMAT) -> datetime:
    """Wrapper around `datetime.strptime` with default API date format."""
    return datetime.strptime(date_str, format)


def date_to_str(date: datetime, format: str = API_DATETIME_FORMAT) -> str:
    """Wrapper around `datetime.strftime` with default API date format."""
    return date.strftime(format)
