import csv

from django.conf import settings
from django.http import HttpResponse
from django.utils import timezone

from ptf.models import Collection

from .utils import calendar_year
from .utils import display_last_volume
from .utils import find_preceding_title
from .utils import scope


def export(request):
    platform = "Numdam" if settings.SITE_NAME == "numdam" else "Mersenne"
    response = HttpResponse(content_type="text/tab-separated-value")
    response["Content-Disposition"] = 'attachment; filename="Mathdoc_Global_{}_{}.txt"'.format(
        platform,
        timezone.now().strftime("%Y-%m-%d"),
    )

    CSV_HEADER = [
        "publication_title",
        "print_identifier",
        "online_identifier",
        "date_first_issue_online",
        "num_first_vol_online",
        "num_first_issue_online",
        "date_last_issue_online",
        "num_last_vol_online",
        "num_last_issue_online",
        "title_url",
        "first_author",
        "title_id",
        "embargo_info",
        "coverage_depth",
        "notes",
        "publisher_name",
        "publication_type",
        "date_monograph_published_print",
        "date_monograph_published_online",
        "monograph_volume",
        "monograph_edition",
        "first_editor",
        "parent_publication_title_id",
        "preceding_publication_title_id",
        "access_type",
    ]

    writer = csv.writer(response, delimiter="\t")
    writer.writerow(CSV_HEADER)
    # We exclude MBK because it may contain several books in the future
    # We exclude STNB second series as a standalone collection
    kbart_collections = Collection.objects.exclude(pid__in=["MBK", "CR"]).exclude(
        pid__startswith="JTNB", resourceid__isnull=True
    )
    for collection in kbart_collections:
        if platform == "Mersenne":
            location = collection.get_top_collection().extlink_set.get(rel="website").location
        else:
            location = "http://www.numdam.org"
        url = f"{location}{collection.get_absolute_url()}"
        first_year, first_volume, first_issue, last_year, last_volume, last_issue = scope(
            collection
        )
        first_year = calendar_year(first_year)
        last_year = calendar_year(last_year)
        publication_type = (
            "monograph"
            if collection.coltype in ["lecture-notes", "thesis"] or collection.pid == "MALSM"
            else "serial"
        )
        show_last_volume = platform != "Mersenne" or display_last_volume(collection)
        preceding = find_preceding_title(collection)
        writer.writerow(
            [
                collection.title_tex,
                collection.issn,
                collection.e_issn,
                first_year,
                first_volume,
                first_issue,
                last_year if show_last_volume else "",
                last_volume if show_last_volume else "",
                last_issue if show_last_volume else "",
                url,
                "",  # first_author
                collection.pid,
                f"P{collection.wall}Y" if collection.wall else "",
                "fulltext",  # are we sure for all ?
                "",  # 'notes',
                collection.provider.name,
                publication_type,
                "",  # 'date_monograph_published_print',
                "",  # 'date_monograph_published_online',
                "",  # 'monograph_volume',
                "",  # 'monograph_edition',
                "",  # 'first_editor',
                "",  # 'parent_publication_title_id',
                preceding,
                "F",  # 'access_type'
            ]
        )

    return response
