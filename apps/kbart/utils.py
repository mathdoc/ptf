from ptf.models import CollectionMembership
from ptf.models import Container
from ptf.models import cmp_container_base


def calendar_year(year):
    # Pick the latest year in 1853-1854 (1854)
    if len(year) == 9:
        year = year[5:]
    return year


def display_last_volume(collection):
    """
    No longer user, change of specs
    As a content provider, we always want to show the latest volume present in Numdam/Mersenne
    => The alive field could be removed from the model
    """
    if not collection.alive:
        return True
    return False


def find_preceding_title(collection):
    if collection.pid == "JTNB":
        return ""
    preceding = collection.preceding_journal()
    if preceding:
        return preceding.pid
    return ""


def malsm_scope(first_year, last_year):
    memberships = CollectionMembership.objects.filter(collection__pid="MALSM").order_by(
        "vseries_int",
        "volume_int",
        "number_int",
    )
    first = memberships.first()
    last = memberships.last()
    return (
        first_year,
        first.volume_int,
        first.number_int,
        last_year,
        last.volume_int,
        last.number_int,
    )


def scope(collection):
    containers = collection.content.exclude(year="0").order_by(
        "year", "vseries_int", "volume_int", "number_int"
    )

    # We have here a special case :
    # STNB second series is supposed to be part of JTNB
    # So we select all containers being part of both JTNB and STNB second series
    if collection.pid == "JTNB":
        containers = (
            Container.objects.exclude(year=0)
            .filter(my_collection__pid__in=["JTNB", "JTNB-None"])
            .order_by("year", "vseries_int", "volume_int", "number_int")
        )

    other_containers = CollectionMembership.objects.filter(collection=collection).order_by(
        "vseries_int", "volume_int", "number_int"
    )

    if not containers.exists() and not other_containers.exists():
        return "", "", "", "", "", ""

    if containers.exists() and other_containers.exists():
        first1 = containers.first()
        first2 = other_containers.first()
        first2.year = first2.container.year  # Year should also be in CollectionMembership
        first = first1 if cmp_container_base(first1, first2) else first2

        last1 = containers.last()
        last2 = other_containers.last()
        last2.year = last2.container.year
        last = last2 if cmp_container_base(last1, last2) else last1
    elif containers.exists():
        first = containers.first()
        last = containers.last()
    else:
        first = other_containers.first()
        first.year = first.container.year
        last = other_containers.last()
        last.year = last.container.year

    return (
        first.year,
        first.volume_int,
        first.number_int,
        last.year,
        last.volume_int,
        last.number_int,
    )
