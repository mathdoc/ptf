from django.conf import settings

from ptf.model_helpers import get_collection

from . import oai_helpers
from .oai_helpers import OAIInfoVisitor
from .oai_helpers import OAIMetadataPrefixVisitor
from .oai_helpers import get_collections
from .oai_helpers import get_oai_item

MAX_RESULT_SIZE = settings.MAX_RESULT_SIZE


class Result:
    """
    iterator sur le qs passe en param et determine le context et le template
    en fonction de l'item et param setspec
    """

    def __init__(
        self,
        setspec,
        results,
        cursor,
        restart,
        total,
        metaformat,
        fd,
        ud,
        repositoryIdentifier,
        header_only=False,
    ):
        self.setspec = setspec
        self.results = results
        self.cursor = cursor
        self.restart = restart
        self.total = total
        self.px = metaformat
        self.fd = fd
        self.ud = ud
        self.repositoryIdentifier = repositoryIdentifier
        self.header_only = header_only

    def __iter__(self):
        for item in self.results:
            context = item.accept(OAIInfoVisitor(self.px))
            context["template"] = self.get_template(item)
            context["repositoryIdentifier"] = self.repositoryIdentifier
            yield context

    def get_template(self, object_):
        classname = object_.classname.lower()
        return "oai/" + classname + "_" + self.px + ".xml"

    def resumptionToken(self):
        # le protocole est bizarre au niveau des resumption tokens
        if not self.restart:
            if self.cursor == 0:
                return ""
            return '<resumptionToken completeListSize="{}" cursor="{}"/>'.format(
                self.total, self.cursor
            )
        if self.setspec:
            return (
                '<resumptionToken completeListSize="{}"'
                ' cursor="{}">{}:{}:{}:{}:{}:{}</resumptionToken>'
            ).format(
                self.total,
                self.cursor,
                self.px,
                self.restart,
                self.setspec,
                self.total,
                self.fd,
                self.ud,
            )
        return (
            '<resumptionToken completeListSize="{}"'
            ' cursor="{}">{}:{}::{}:{}:{}</resumptionToken>'
        ).format(self.total, self.cursor, self.px, self.restart, self.total, self.fd, self.ud)


class OAIRepository:
    def __init__(self, base_url):
        self.base_url = base_url
        self.sets_descr = ""
        self.setspec = ""
        self.cursor = 0
        self.fromdate = ""
        self.untildate = ""
        self.px = None

    def Identify(self):
        pass

    def setmetaDataFormat(self, fmt):
        self.px = fmt

    def setSet(self, setspec):
        self.setspec = setspec

    def setfromDate(self, fd):
        self.fromdate = fd

    def setuntilDate(self, ud):
        self.untildate = ud

    def setresumptionToken(self, token):
        (px, c, s, t, fromdate, untildate) = token.split(":")
        cursor = int(c)
        total = int(t)
        self.setspec = s
        if cursor >= total:
            return 0
        self.px = px
        self.cursor = cursor
        self.fromdate = fromdate
        self.untildate = untildate
        return 1

    def get(self, id_):
        """
        en fonction de l'id determine le queryset et retourne un Result
        @param id:
        @return: Result, template: string,
        """
        my_id = self.make_internal_id(id_)
        items = [get_oai_item(my_id)]
        setspec = None
        result = Result(
            setspec,
            items,
            "",
            "",
            "",
            self.px,
            "",
            "",
            repositoryIdentifier=self.Identify()["repositoryIdentifier"],
            header_only=False,
        )
        return result

    @staticmethod
    def has_set(setspec):
        if setspec in ["NUMDAM", "NUMDAM_book", "gallica"]:
            return True
        col = get_collection(setspec)
        if col:
            return True
        return False

    @staticmethod
    def listsets():
        collections = get_collections()
        sets = (
            {"pid": "NUMDAM", "title": "NUMDAM in eudml-article2"},
            {"pid": "NUMDAM_book", "title": "NUMDAM_book in eudml - book2"},
            {"pid": "gallica", "title": "All Collections in oai_dc for Gallica BNF"},
        )
        return (collections, sets)

    @staticmethod
    def make_internal_id(id_):
        if len(id_.split(":")) == 3:
            return id_.split(":")[2]  # oai:numdam.org:JTNB_XXX
        return None

    def has_identifier(self, id_):
        my_id = self.make_internal_id(id_)
        item = get_oai_item(my_id)
        if item:
            return True
        return False

    def get_items(self, px, setspec):
        """
        * renvoie une liste d'item en fonction du setspec et prefix :
        cf Mathdoc / Services-Projets / Numdam / Documents d'étude et
        développement / Etudes techniques / OAI_sur_la_PTF_et_etat_des_lieux.md

        @param setspec:
        @return: list of items with pagination AND total count of items without pagination
        """
        params = {
            "cursor": self.cursor,
            "page_size": MAX_RESULT_SIZE,
            "fromdate": self.fromdate,
            "untildate": self.untildate,
        }

        items = []
        total = 0
        col = None
        if setspec in ["NUMDAM", "NUMDAM_book"]:
            transform = {"NUMDAM": "eudml-article2", "NUMDAM_book": "eudml-book2"}
            px = transform[setspec]
        elif setspec == "gallica":
            items, total = get_collections(params)
            return items, total
        elif setspec:
            col = get_collection(setspec)

        restrict_by_date_published = getattr(settings, "OAI_BY_DATE_PUBLISHED", False)
        if restrict_by_date_published:
            items, total = oai_helpers.get_articles_by_date_published(params, col)
        else:
            klass = "get_items_{}".format(px.replace("-", "_"))
            items, total = getattr(oai_helpers, klass)(params, col)

        return items, total

    def get_next_page(self, items, total):
        count = len(items)
        if self.cursor + count >= total:
            return 0
        return self.cursor + MAX_RESULT_SIZE

    def listids(self):
        items, total = self.get_items(self.px, self.setspec)
        restart = self.get_next_page(items, total)
        result = Result(
            self.setspec,
            items,
            self.cursor,
            restart,
            total,
            self.px,
            self.fromdate,
            self.untildate,
            repositoryIdentifier=self.Identify()["repositoryIdentifier"],
            header_only=True,
        )
        return result

    def listrecs(self):
        items, total = self.get_items(self.px, self.setspec)
        restart = self.get_next_page(items, total)
        result = Result(
            self.setspec,
            items,
            self.cursor,
            restart,
            total,
            self.px,
            self.fromdate,
            self.untildate,
            repositoryIdentifier=self.Identify()["repositoryIdentifier"],
            header_only=False,
        )
        return result

    @staticmethod
    def has_format(px, object_=None, setspec=None):
        """
        renvoie si le format est supporte par rapport au set ou objet
        @param px:
        @param object:
        @param setspec:
        @return: True or False
        """

        matrice = {
            "eudml-book2": ["NUMDAM_book"],
            "eudml-article2": ["NUMDAM"],
            "oai_dc": ["NUMDAM_book", "NUMDAM", "gallica"],
        }
        value = True
        if px not in matrice:
            return False

        if object_:  # on regarde le type d'objet
            klass = "get_items_{}".format(px.replace("-", "_"))
            items = getattr(oai_helpers, klass)(pid=object_.pid)
            if len(items) != 1:
                value = False
            # cas particulier item est une collection
            if object_.classname == "Collection" and px == "oai_dc":
                value = True

        if setspec in matrice[px]:
            setspec = None  # on est dans le cas des set dédiés ne représentant pas une collection
        if setspec:
            value = get_collection(setspec) is not None

        return value

    def listmetadataformats(self, identifier=None):
        oai_dc = {
            "prefix": "oai_dc",
            "schema": "http://www.openarchives.org/OAI/2.0/oai_dc.xsd",
            "namespace": "http://www.openarchives.org/OAI/2.0/oai_dc/",
        }
        eudml_article2 = {
            "prefix": "eudml-article2",
            "schema": "http://eudml.org/schema/2.0/eudml-article-2.0.xsd",
            "namespace": "http://jats.nlm.nih.gov",
        }
        eudml_book2 = {
            "prefix": "eudml-book2",
            "schema": "http://eudml.org/schema/2.0/eudml-book-2.0.xsd",
            "namespace": "http://eudml.org/schema/2.0/eudml-book",
        }

        if identifier is None:
            repository = self.Identify()
            if repository["repositoryIdentifier"] == "centre-mersenne.org":
                if repository["base_url"] != "proceedings.centre-mersenne.org/oai":
                    return [oai_dc, eudml_article2]
            return [oai_dc, eudml_article2, eudml_book2]

        value = None
        pid = self.make_internal_id(identifier)
        item = get_oai_item(pid)

        if item:
            formats = item.accept(OAIMetadataPrefixVisitor())
            if formats:
                value = []
                for px in formats:
                    value.append(locals()[px])

            #
            #
            # if item.classname == 'Article':
            #     article = item.cast()
            #     if article.my_container.ctype == 'issue':
            #         value = [oai_dc, eudml_article2]
            #     else:
            #         value = [oai_dc]
            #
            # elif item.classname == 'Collection':
            #     value = [oai_dc]
            # elif item.ctype.startswith('book'):  # container
            #     value = [eudml_book2]

        return value
