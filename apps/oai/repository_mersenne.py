from django.conf import settings

from oai import oai_helpers
from oai.repository import OAIRepository
from ptf import model_helpers
from ptf.models import Article
from ptf.models import SiteMembership


class MersenneRepository(OAIRepository):
    def Identify(self):
        return {
            "name": settings.SITE_NAME,
            "base_url": f"{settings.SITE_DOMAIN}/oai",
            "protocol_version": "2.0",
            "admin_email": "mersenne@listes.mathdoc.fr",
            "earliest_datestamp": SiteMembership.objects.filter(site__id=settings.SITE_ID)
            .order_by("deployed")
            .first()
            .deployed.strftime("%Y-%m-%d"),
            "deleted_record": "no",
            "granularity": "YYYY-MM-DDThh:mm:ssZ",
            "repositoryIdentifier": "centre-mersenne.org",
            "delimiter": ":",
            "sample_identifier": f"oai:centre-mersenne.org:{Article.objects.first().pid}",
        }

    def has_set(self, setspec):
        col = model_helpers.get_collection(setspec)
        return bool(col)

    def listsets(self):
        collections = oai_helpers.get_collections()
        return [collections]


def get_repository(base_url):
    # thismodule = sys.modules[__name__]
    # repo = settings.REPOSITORIES[base_url]
    # repoClass = getattr(thismodule, repo)
    return MersenneRepository(base_url)
