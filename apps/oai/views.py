import importlib
from datetime import datetime

from django.conf import settings
from django.http import HttpResponseServerError
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View

from .protocol import argdescrs

try:
    REPOSITORY_MODULE = importlib.import_module(settings.OAI_REPOSITORY)
except Exception:
    REPOSITORY_MODULE = None


@method_decorator(csrf_exempt, name="dispatch")
class OAIView(View):
    def post(self, request, *args, **kwargs):
        request.REQUEST = request.POST
        return self.oai(request)

    def get(self, request, *args, **kwargs):
        request.REQUEST = request.GET
        return self.oai(request)

    def oai(self, request):
        if REPOSITORY_MODULE is None:
            return HttpResponseServerError("No OAI repository configured")
        code = "200 OK"
        status = 200
        message = ""
        if "verb" in request.REQUEST:
            verb = request.REQUEST["verb"]
            try:
                argdescr, klass = argdescrs[verb]

            except KeyError:
                code = "badVerb"
                status = 405
                message = "Illegal or missing OAI verb"
                pass
            else:
                base_url = settings.SITE_DOMAIN
                repo = REPOSITORY_MODULE.get_repository(base_url)
                view = klass(verb, argdescr, repo)
                return view(request)
        if status == 405:
            return error(request, code=code, message=message, status=status)
        else:
            return success(request, code=code, message=message, status=status)

    # TODO : j ai l impression que le meme code existe dans protocole.py


def error(request, code, message, status):
    context = {
        "request": request,
        "code": code,
        "message": message,
    }

    return render(
        request=request,
        template_name="oai/error.html",
        context=context,
        content_type="text/xml; charset=UTF-8",
        status=status,
    )


def success(request, code, message, status):
    base_url = settings.SITE_DOMAIN

    repo = REPOSITORY_MODULE.get_repository(base_url)
    identify = repo.Identify()
    granularity = identify["granularity"]
    granularity_format = "Y-m-d"
    format_date = "%Y-%m-%d"
    if len(granularity) > 10:
        granularity_format = granularity_format + r"\TH:i:sZ"
        format_date = "%Y-%m-%dT%H:%M:%S.%fZ"

    context = {
        "request": request,
        "code": code,
        "message": message,
        "granularity": granularity_format,
        "now": datetime.now().strftime(format_date),
    }

    return render(
        request=request,
        template_name="oai/response.html",
        context=context,
        content_type="text/xml; charset=UTF-8",
        status=status,
    )


def is_authorized(request):
    x_forwarded_for = request.headers.get("x-forwarded-for")
    if x_forwarded_for:
        ip_address = x_forwarded_for.split(",")[0]
    else:
        ip_address = request.META.get("REMOTE_ADDR")
    if ip_address in settings.EUDML_AUTHORIZED_IP:
        return True
    return False
