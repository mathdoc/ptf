import time
from datetime import datetime

from django.conf import settings
from django.shortcuts import render
from django.template import RequestContext
from django.utils.html import escape

CONTENT_TYPE = "text/xml; charset=UTF-8"


class BadArgument(Exception):
    pass


class BadResumptionToken(Exception):
    pass


class CanNotDisseminateFormat(Exception):
    pass


class NoRecordsMatch(Exception):
    pass


class OAIRequest:
    def __init__(self, rq, baseurl, argdescr):
        self.rq = rq
        self.baseurl = baseurl
        self.argdescr = argdescr

    def __str__(self):
        attrs = "".join(
            [
                f' {key}="{escape(value)}"'
                for key, value in self.rq.items()
                if key == "verb" or key in self.argdescr
            ]
        )
        return f"<request{attrs}>{self.baseurl}</request>"


class Request:
    def __init__(self, verb, argdescr, repository):
        self._verb = verb
        self._argdescr = argdescr
        self._repository = repository
        self._exclusive = None
        for aname, atype in argdescr.items():
            if atype == "exclusive":
                self._exclusive = aname

    def _processArgs(self, form):
        argdescr = self._argdescr
        for key, value in form.items():
            if key == "verb":
                continue
            if key not in argdescr:
                raise BadArgument("Request contains illegal arguments")
            if isinstance(value, list):
                raise BadArgument("Request contains repeated arguments")
        if self._exclusive and self._exclusive in form:
            if len(form.keys()) > 2:
                raise BadArgument("Request contains extra arguments")
        else:
            for aname, atype in argdescr.items():
                if atype == "required":
                    if aname not in form:
                        raise BadArgument("Required argument missing")
        self._validate(form)

    def _validate(self, request):
        pass

    def error_response(self, code, mess):
        self.context["code"] = code
        if code in ["BadArgument", "BadResumptionToken", "badVerb", "CanNotDisseminateFormat"]:
            status = 405
        if code in ["idDoesNotExist", "NoRecordsMatch", "noMetadataFormats", "noSetHierarchy"]:
            status = 200
        self.context["message"] = mess
        return render(
            self.request,
            template_name="oai/error.html",
            context=self.context,
            status=status,
            content_type=CONTENT_TYPE,
        )

    def response(self, template):
        return render(
            request=self.request,
            template_name=template,
            context=self.context,
            content_type=CONTENT_TYPE,
        )

    def __call__(self, request):
        context = {}
        baseurl = self._repository.base_url
        granularity = self._repository.Identify()["granularity"]
        context["baseurl"] = baseurl
        granularity_format = "Y-m-d"
        format_date = "%Y-%m-%d"
        if len(granularity) > 10:
            granularity_format = granularity_format + r"\TH:i:sZ"
            format_date = "%Y-%m-%dT%H:%M:%S.%fZ"
        context["granularity"] = granularity_format
        context["now"] = datetime.now().strftime(format_date)
        context["oairequest"] = OAIRequest(request.REQUEST, baseurl, self._argdescr)

        granularity = self._repository.Identify()["granularity"]
        granularity_format = "Y-m-d"
        format_date = "%Y-%m-%d"
        if len(granularity) > 10:
            granularity_format = granularity_format + r"\TH:i:sZ"
            format_date = "%Y-%m-%dT%H:%M:%S.%fZ"
        context["granularity"] = granularity_format
        context["now"] = datetime.now().strftime(format_date)

        rq = RequestContext(request)
        self.context = context
        self.request = request
        self.rq = rq
        try:
            self._processArgs(request.REQUEST)
        except BadArgument as mess:
            return self.error_response("BadArgument", mess)
        except BadResumptionToken as mess:
            return self.error_response("BadResumptionToken", mess)
        except CanNotDisseminateFormat as mess:
            return self.error_response("CanNotDisseminateFormat", mess)
        except NoRecordsMatch as mess:
            return self.error_response("NoRecordsMatch", mess)
        return self.real_run()

    def real_run(self):
        pass


class IdentifyRequest(Request):
    def real_run(self):
        identify = self._repository.Identify()
        self.context["identify"] = identify
        self.context["granularity"] = identify["granularity"]
        return self.response("oai/identify.html")


class ListMetadataFormatsRequest(Request):
    def real_run(self):
        ident = None
        if "identifier" in self.request.REQUEST:
            if not self._repository.has_identifier(self.request.REQUEST["identifier"]):
                return self.error_response("idDoesNotExist", "identifier maps to no item")
            ident = self.request.REQUEST["identifier"]
        formats = self._repository.listmetadataformats(ident)
        self.context["formats"] = formats
        return self.response("oai/listmetadataformats.html")


# from ptf import models


class GetRecordRequest(Request):
    def real_run(self):
        px = self.request.REQUEST["metadataPrefix"]
        id = self.request.REQUEST["identifier"]
        if not self._repository.has_identifier(id):
            return self.error_response("idDoesNotExist", "identifier maps to no item")
        else:
            self._repository.setmetaDataFormat(px)
            rec = self._repository.get(id)
            for res in rec:
                result = res
            item = result["item"]
            if not self._repository.has_format(px, item):
                return self.error_response(
                    "CanNotDisseminateFormat", "This server does not support the requested format"
                )

            self.context["record"] = result
            self.context["base_url"] = settings.SITE_DOMAIN
            # self.context['include_template'] = template
            self.context["is_authorized"] = is_authorized(self.request)
            return self.response("oai/getrecord.xml")


class ListSetsRequest(Request):
    def _validate(self, _dict):
        if "resumptionToken" in _dict:
            raise BadResumptionToken("Invalid resumption token")

    def real_run(self):
        sets = self._repository.listsets()
        if not sets:
            return self.error_response("noSetHierarchy", "This repository does not support sets")
        self.context["sets"] = sets
        return self.response("oai/listsets.html")


class ListRequest(Request):
    def _validate(self, request):
        Request._validate(self, request)
        if "set" in request:
            set = request["set"]
            if not self._repository.has_set(set):
                raise BadArgument("Unknown set specification")
            px = request["metadataPrefix"]
            if not self._repository.has_format(px, None, set):
                raise CanNotDisseminateFormat(
                    f"This server does not support the requested format for set {set}"
                )
            self._repository.setSet(set)
        else:
            set = None
        fromdate = untildate = ""
        if "from" in request:
            datestr = request["from"]
            fromdate = datestr
            _list = datestr.split("T")
            if len(_list) > 1:
                raise BadArgument("This repository only supports YYYY-MM-DD granularity")
            try:
                time.strptime(datestr, "%Y-%m-%d")
            except ValueError:
                raise BadArgument("Invalid from date")
            self._repository.setfromDate(fromdate)
        if "until" in request:
            datestr = request["until"]
            untildate = datestr
            _list = datestr.split("T")
            if len(_list) > 1:
                raise BadArgument("This repository only supports YYYY-MM-DD granularity")
            try:
                time.strptime(datestr, "%Y-%m-%d")
            except ValueError:
                raise BadArgument("Invalid until date")

            self._repository.setuntilDate(untildate)
        if fromdate and untildate:
            if fromdate > untildate:
                raise BadArgument("Invalid from date and until date combination")
        if untildate:
            edt = self._repository.Identify()["earliest_datestamp"]
            if untildate < edt:
                raise NoRecordsMatch("Nothing to list")
        if "resumptionToken" in request:
            token = request["resumptionToken"]
            try:
                success = self._repository.setresumptionToken(token)
            except Exception:
                raise BadResumptionToken("Invalid resumption token")
            else:
                if not success:
                    raise BadResumptionToken("Invalid resumption token")
        else:
            px = request["metadataPrefix"]
            if not self._repository.has_format(px):  # TODO : duplique ?
                raise CanNotDisseminateFormat("This server does not support the requested format")
            self._repository.setmetaDataFormat(px)
        self.context["by_date_published"] = getattr(settings, "OAI_BY_DATE_PUBLISHED", False)


class ListIdsRequest(ListRequest):
    def real_run(self):
        result = self._repository.listids()
        if result.total == 0:
            return self.error_response("NoRecordsMatch", "Nothing to list")
        self.context["records"] = result

        return self.response("oai/listidentifiers.xml")


class ListRecordsRequest(ListRequest):
    def real_run(self):
        result = self._repository.listrecs()
        if result.total == 0:
            return self.error_response("NoRecordsMatch", "Nothing to list")
        self.context["records"] = result
        # self.context['include_template'] = template
        self.context["base_url"] = settings.SITE_DOMAIN
        self.context["is_authorized"] = is_authorized(self.request)
        return self.response("oai/listrecords.xml")


argdescrs = {
    "Identify": (
        {},
        IdentifyRequest,
    ),
    "ListMetadataFormats": (
        {"identifier": "optional"},
        ListMetadataFormatsRequest,
    ),
    "GetRecord": (
        {"identifier": "required", "metadataPrefix": "required"},
        GetRecordRequest,
    ),
    "ListSets": (
        {"resumptionToken": "exclusive"},
        ListSetsRequest,
    ),
    "ListIdentifiers": (
        {
            "from": "optional",
            "until": "optional",
            "metadataPrefix": "required",
            "set": "optional",
            "resumptionToken": "exclusive",
        },
        ListIdsRequest,
    ),
    "ListRecords": (
        {
            "from": "optional",
            "until": "optional",
            "set": "optional",
            "resumptionToken": "exclusive",
            "metadataPrefix": "required",
        },
        ListRecordsRequest,
    ),
}


def is_authorized(request):
    x_forwarded_for = request.headers.get("x-forwarded-for")
    if x_forwarded_for:
        ip = x_forwarded_for.split(",")[0]
    else:
        ip = request.META.get("REMOTE_ADDR")
    if hasattr(settings, "EUDML_AUTHORIZED_IP") and ip in settings.EUDML_AUTHORIZED_IP:
        return True
    return False
