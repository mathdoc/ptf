from django.conf import settings
from django.db.models import Q
from django.urls import reverse

from ptf.model_helpers import get_first_last_years
from ptf.model_helpers import get_resource
from ptf.model_helpers import get_volumes_in_collection
from ptf.model_helpers import parse_date_str
from ptf.models import Article
from ptf.models import Collection
from ptf.models import Resource


def get_oai_item(pid):
    """
    verifie si l'id est un object que l'on peut retourner :
    - Article
    - Container de type Book-monograph ou edited-book n'ayant pas d'issue
    - Collection
    retourne 1 item en fonction de l'id
    @param id:
    @return: item OR None
    """

    item = get_resource(pid)
    if item:
        if item.classname == "Container":
            item = item.cast()
            count_articles = item.article_set.count()
            if (
                item.ctype == "book-lecture-notes"
                or item.ctype == "book-series"
                or item.ctype == "book-edited-book"
            ) and count_articles == 0:
                item = None
    return item


def get_articles_by_date_published(params, col):
    """
    retourne uniquement les articles dans le cas de l'option OAI_BY_DATE_PUBLISHED = True dans settings
    et donc aucun online_first
    @param params: filtres optionnels pour la pagination et la restriction par date
    @return: list of Articles
    """
    if col:
        q1 = Q(
            classname="Article",
            my_container__my_collection=col,
            date_published__isnull=False,
        )
    else:
        q1 = Q(classname="Article", date_published__isnull=False)
    qs = (
        Article.objects.filter(q1)
        .filter(sites__id=settings.SITE_ID)
        .exclude(classname="TranslatedArticle")
        .order_by("id")
    )

    if "fromdate" in params or "untildate" in params:
        fromdate = params["fromdate"]
        untildate = params["untildate"]
        if fromdate:
            fromdate = parse_date_str(fromdate)
            qs = qs.filter(date_published__gte=fromdate)
        if untildate:
            untildate = parse_date_str(untildate)
            qs = qs.filter(date_published__lte=untildate)

    total = qs.count()  # necessaire pour resumptionToken
    if "cursor" in params and "page_size" in params:
        qs = restrict_for_pagination(qs, params["cursor"], params["page_size"])
    return qs, total


def restrictResource_by_date(qs, fromdate=None, untildate=None):
    if fromdate:
        fromdate = parse_date_str(fromdate)
        qs = qs.filter(sitemembership__deployed__gte=fromdate)
    if untildate:
        untildate = parse_date_str(untildate)
        untildate = untildate.replace(hour=23, minute=59, second=59)

        qs = qs.filter(sitemembership__deployed__lte=untildate)
    return qs


def restrict_for_pagination(qs, cursor, page_size):
    total = qs.count()
    if total > cursor + page_size:
        slice_end = cursor + page_size
        # restart = slice_end
    else:
        slice_end = total
        # restart = 0
    qs = qs[cursor:slice_end]
    return qs


def restrict_qs(qs, params):
    if "fromdate" in params or "untildate" in params:
        qs = restrictResource_by_date(qs, params["fromdate"], params["untildate"])
    total = qs.count()  # necessaire pour resumptionToken
    if "cursor" in params and "page_size" in params:
        qs = restrict_for_pagination(qs, params["cursor"], params["page_size"])
    return qs, total


def get_collections(params=None):
    """
    retourne toutes les collections
    en params : filtres optionnels pour la pagination et la restriction par date
    @param params: 'cursor': self.cursor,
                'page_size': MAX_RESULT_SIZE,
                'fromdate': self.fromdate,
                'untildate': self.untildate}
    @return: list of items
    """
    qs = Collection.objects.filter(sites__id=settings.SITE_ID)
    qs = qs.order_by("id")
    if params:
        qs, total = restrict_qs(qs, params)
        return qs.all(), total
    return qs.all()


def get_items_eudml_article2(params=None, col=None, pid=None):
    """
    retourne les articles non contenu dans des livres
    en params : filtres optionnels pour la pagination et la restriction par date
    @param params:
    @return:
    """
    qs = Article.objects.filter(my_container__ctype="issue", sites__id=settings.SITE_ID).order_by(
        "id"
    )
    if col:
        qs = qs.filter(my_container__my_collection=col)
    if pid:
        qs = qs.filter(pid=pid)
    if params:
        qs, total = restrict_qs(qs, params)
        return qs.all(), total
    return qs.all()


def get_items_eudml_book2(params=None, col=None, pid=None):
    """
    pour le format eudml-book2, on doit retourner les monograph au niveau du container - un record par container (avec les book-parts)
    et les edited-books au niveau book-parts - un record par book-part si ils ont des book part, sinon retourner le niveau container
    @param params: filtres optionnels pagination et restriction par date
    @return:
    """

    if col:
        q1 = Q(
            classname="Article",
            article__my_container__ctype="book-edited-book",
            article__my_container__my_collection=col,
        )
        q2 = Q(
            classname="Container",
            container__ctype__in=["book-monograph", "book-lecture-notes"],
            container__my_collection=col,
        )
    else:
        q1 = Q(classname="Article", article__my_container__ctype="book-edited-book")
        q2 = Q(
            classname="Container",
            container__ctype__in=[
                "book-monograph",
                "book-lecture-notes",
                "book-series",
                "book-edited-book",
            ],
        )

    qs = Resource.objects.filter(q1 | q2).filter(sites__id=settings.SITE_ID).order_by("id")

    if pid:
        qs = qs.filter(pid=pid)

    # requete quand on ne retournait que des containers pour eudml-book2
    # qs = Container.objects.filter(sites__id=settings.SITE_ID, ctype__startswith='book').order_by('id')
    if params:
        qs, total = restrict_qs(qs, params)
        return qs.all(), total
    return qs.all()


# def get_objects_from_collection(col, params=None):
#     """
#         retourne les objects contenu dans une collection :
#         pour une book-series ou des lecture-notes : retourne des containers
#         pour le reste retourne des articles
#         @param params: filtres optionnels pour la pagination et la restriction par date
#         @return:
#     """
#
#     # TODO : these : classe derivee ?
#     if col.coltype in ['book-series', 'these', 'lecture-notes'] :
#         qs = Container.objects.filter(sites__id=settings.SITE_ID, my_collection=col).order_by('id')
#     else:
#         qs = Article.objects.filter( sites__id=settings.SITE_ID, my_container__my_collection=col).order_by('id')
#     if params:
#         qs, total = restrict_qs(qs, params)
#         return qs.all(), total
#     return qs.all()


def get_items_oai_dc(params=None, col=None, pid=None):
    """
    retourne les Articles / les Book-parts d'edited-book et book-monograph(niveau container)
    @param params: filtres optionnels pour la pagination et la restriction par date
    @return: list of Resources
    """
    if col:
        q1 = Q(classname="Article", article__my_container__my_collection=col)
        q2 = Q(
            classname="Container",
            container__ctype__in=[
                "book-monograph",
                "book-lecture-notes",
                "book-edited-book",
                "book-series",
            ],
            container__my_collection=col,
        )
    else:
        q1 = Q(classname="Article")
        q2 = Q(
            classname="Container",
            container__ctype__in=[
                "book-monograph",
                "book-lecture-notes",
                "book-edited-book",
                "book-series",
            ],
        )
    q3 = ~Q(
        classname="Article",
        article__my_container__ctype__in=["book-monograph", "book-lecture-notes"],
    )

    qs = Resource.objects.filter((q1 & q3) | q2).filter(sites__id=settings.SITE_ID).order_by("id")

    # qs = Article.objects.filter(sites__id=settings.SITE_ID).order_by('id')
    if pid:
        qs = qs.filter(pid=pid)

    if params:
        qs, total = restrict_qs(qs, params)
        return qs.all(), total
    return qs.all()


def get_publishers_for_GALLICA(collection):
    """
    retourne la liste des publishers des volumes d'un journal OU d'une collection avec les dates
    @param object : Collection
    @return: [] : list of publishers with dates
    """
    publishers = []
    list = collection.content.all().order_by("id")

    for container in list:
        container = container.cast()
        # une date year peut-être sous la forme YYYY-YYYY : issue comprenant
        # des articles de 2 années
        if len(container.year.split("-")) > 1:
            datedeb = container.year.split("-")[0]
            datefin = container.year.split("-")[1]
        else:
            datedeb = datefin = container.year
        if container.my_publisher:
            if publishers:
                item = publishers[0]
                if container.my_publisher.pub_name != item["name"]:
                    item = {
                        "name": container.my_publisher.pub_name,
                        "datedeb": datedeb,
                        "datefin": datefin,
                    }
                    publishers.insert(0, item)
                else:
                    if datedeb < item["datedeb"]:
                        item["datedeb"] = datedeb
                    if datefin > item["datefin"]:
                        item["datefin"] = datefin
            else:
                publishers.insert(
                    0,
                    {
                        "name": container.my_publisher.pub_name,
                        "datedeb": datedeb,
                        "datefin": datefin,
                    },
                )
    return publishers


def get_containers_for_GALLICA(collection):
    """
    retourne une liste ordonee volume/annee des volumes d'un journal ou des books d'une collection
    volume[volume_int]{'title': TODO,
                    'vseries' : vserie_int,
                    'index' : volume_int,
                    'fyear': int 'first year',
                    'lyear': int 'last year',
                    }
    @param object: Collection
    @return: list
    """
    # TODO : ceci n'est pas une liste ordonnée : faire avec une liste
    volumes = {}
    containers = collection.content.all().order_by("id")

    for container in containers:
        # on verifie si ce volume existe deja
        year = container.year
        fyear, lyear = get_first_last_years(year)
        fyear = int(fyear)
        lyear = int(lyear)

        if container.volume_int in volumes:
            volume = volumes[container.volume_int]

            if volume["fyear"] > fyear:
                volume["fyear"] = fyear
            if volume["lyear"] < lyear:
                volume["lyear"] = lyear

            volume["title"] = container.title_tex
            volume["vseries"] = container.vseries_int
            volume["index"] = container.volume_int
            volumes[container.volume_int] = volume

        else:
            volume = {
                "title": container.title_tex,
                "index": container.volume_int,
                "vseries": container.vseries_int,
                "fyear": fyear,
                "lyear": lyear,
            }
            volumes[container.volume_int] = volume

    # for container in containers:
    #
    #     my_hash = {'vseries': container.vseries, 'volume': container.volume, 'year': container.year}
    #     # if container.vseries != '':
    #     #     text = container.vseries + 'e série, '
    #     # text += "Vol. " + container.volume + ", " + container.year
    #     if my_hash not in list:
    #         list.append(my_hash)

    return volumes


class OAIInfoVisitor:
    def __init__(self, px):
        self.px = px

    def visit(self, resource):
        meth = getattr(self, "visit" + resource.classname)
        return meth(resource)

    def visitArticle(self, article):
        setspec = []
        # EuDML harvests Numdam metadata and expects a "NUMDAM" set
        if self.px == "eudml-article2" and settings.SITE_NAME == "numdam":
            setspec.append("NUMDAM")
        if self.px == "eudml-book2" and settings.SITE_NAME == "numdam":
            setspec.append("NUMDAM_book")

        setspec.append(article.my_container.my_collection.pid)

        return {"item": article, "setspec": setspec}

    def visitCollection(self, collection):
        result = {}
        result["item"] = collection

        context = get_volumes_in_collection(collection)
        result["publishers"] = context["publishers"]
        result["issues_in_vseries"] = context["sorted_issues"]
        result["vols_number"] = context["volume_count"]
        # result['publishers'] = get_publishers_for_GALLICA(collection)
        # result['issues'] = get_containers_for_GALLICA(collection)
        result["url"] = "{}{}".format(
            settings.SITE_DOMAIN, reverse("journal-issues", args=[collection.pid])
        )
        result["icon"] = collection.icon()
        result["setspec"] = ["gallica"]
        return result

    def visitContainer(self, container):
        references = False
        if self.px == "eudml-book2":
            # on regarde si type monograph avec book-parts et si les book part
            # ont une biblio
            if (
                container.ctype in ["book-monograph", "book-lecture-notes"]
                and container.article_set.count() > 0
            ):
                for article in container.article_set.all():
                    if article.bibitem_set.count() > 0:
                        references = True

        return {
            "item": container,
            "setspec": ["NUMDAM_book", container.my_collection.pid],
            "references": references,
        }


class OAIMetadataPrefixVisitor:
    def visit(self, resource):
        meth = getattr(self, "visit" + resource.classname)
        return meth(resource)

    def visitArticle(self, article):
        if article.my_container.ctype == "book-edited-book":
            return ["oai_dc", "eudml_book2"]
        elif article.my_container.ctype == "issue":
            return ["oai_dc", "eudml_article2"]
        return None

    def visitCollection(self, collection):
        return ["oai_dc"]

    def visitContainer(self, container):
        if container.ctype == "book-monograph" or container.ctype == "book-lecture-notes":
            return ["oai_dc", "eudml_book2"]
        return None
