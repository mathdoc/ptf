from django.conf import settings

from oai.repository import OAIRepository
from ptf.models import SiteMembership


class TestRepository(OAIRepository):
    def Identify(self):
        return {
            "name": "test-repo",
            "base_url": "https://test.numdam.org/oai",
            "protocol_version": "2.0",
            "admin_email": "mersenne@listes.mathdoc.fr",
            "earliest_datestamp": SiteMembership.objects.filter(site__id=settings.SITE_ID)
            .order_by("deployed")
            .first()
            .deployed.strftime("%Y-%m-%d"),
            "deleted_record": "no",
            "granularity": "YYYY-MM-DD",
            "repositoryIdentifier": "numdam.org",
            "delimiter": ":",
            "sample_identifier": "oai:numdam.org:ALCO_2018__1_1_1_0",
        }

    # def has_set(self, setspec):
    #     col = model_helpers.get_collection(setspec)
    #     return bool(col)

    # def listsets(self):
    #     collections = oai_helpers.get_collections()
    #     return [collections]


def get_repository(base_url):
    # thismodule = sys.modules[__name__]
    # repo = settings.REPOSITORIES[base_url]
    # repoClass = getattr(thismodule, repo)
    return TestRepository(base_url)
