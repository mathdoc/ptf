import os

from lxml import etree

from django.conf import settings
from django.test import TestCase

from ptf.models import Article
from ptf.models import Collection
from ptf.models import Container
from ptf.shortcuts import get_article


class RequestTestCase(TestCase):
    def setUp(self):
        # settings.SITE_NAME = "numdam"
        # settings.SITE_DOMAIN = "www.numdam.org"

        with open("tests/data/xml/collection.xml") as xml_file:
            self.client.post(
                "/upload/collections/", data=xml_file.read(), content_type="application/xml"
            )

        with open("tests/data/xml/collection-msmf.xml") as xml_file:
            self.client.post(
                "/upload/collections/", data=xml_file.read(), content_type="application/xml"
            )

        with open("tests/data/xml/collection-these.xml") as xml_file:
            self.client.post(
                "/upload/collections/", data=xml_file.read(), content_type="application/xml"
            )

        with open("tests/data/xml/journal-sms.xml") as xml_file:
            self.client.post(
                "/upload/collections/", data=xml_file.read(), content_type="application/xml"
            )

        with open("tests/data/xml/journal.xml") as xml_file:
            self.client.post(
                "/upload/collections/", data=xml_file.read(), content_type="application/xml"
            )

        with open("tests/data/xml/issue.xml") as xml_file:
            self.client.post(
                "/upload/issues/", data=xml_file.read(), content_type="application/xml"
            )

        with open("tests/data/xml/issue2.xml") as xml_file:
            self.client.post(
                "/upload/issues/", data=xml_file.read(), content_type="application/xml"
            )

        with open("tests/data/xml/issue4.xml") as xml_file:
            self.client.post(
                "/upload/issues/", data=xml_file.read(), content_type="application/xml"
            )

    def test_oai_identify(self):
        print("** Test: test_oai_identify **")
        response = self.client.get("/oai/?verb=Identify", headers={"host": "127.0.0.1"})
        self.assertEqual(response.status_code, 200)
        # self.assertEqual(response.content, ('<?xml version="1.0" encoding="UTF-8"?>'
        #                                     '<OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/ http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd">'
        #                                     '<responseDate>2016-05-02T16:13:25Z</responseDate>'
        #                                     '<request verb="Identify">http://127.0.0.1/oai/</request>'
        #                                     '  <Identify>'
        #                                     '    <repositoryName>numdam</repositoryName>'
        #                                     '    <baseURL>http://www.numdam.org/oai</baseURL>'
        #                                     '    <protocolVersion>2.0</protocolVersion>'
        #                                     '    <adminEmail>webmaster@numdam.org</adminEmail>'
        #                                     '    <earliestDatestamp>2003-12-15</earliestDatestamp>'
        #                                     '    <deletedRecord>no</deletedRecord>'
        #                                     '    <granularity>YYYY-MM-DD</granularity>'
        #                                     '    <description>'
        #                                     '      <oai-identifier xmlns="http://www.openarchives.org/OAI/2.0/oai-identifier" xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/oai-identifier http://www.openarchives.org/OAI/2.0/oai-identifier.xsd">'
        #                                     ' <scheme>oai</scheme>'
        #                                     '        <repositoryIdentifier>numdam.org</repositoryIdentifier>'
        #                                     '        <delimiter>:</delimiter>'
        #                                     '        <sampleIdentifier>oai:numdam.org:AIF_1971__21_3_1_0</sampleIdentifier>'
        #                                     '      </oai-identifier>'
        #                                     '    </description>'
        #                                     '  </Identify>'
        #                                     ''
        #                                     '</OAI-PMH>'
        #                                     ))
        # TODO : test du post

    def test_oai_one_post(self):
        print("** Test: test_oai_via method post **")
        response = self.client.post("/oai/", {"verb": "Identify"}, headers={"host": "127.0.0.1"})
        self.assertEqual(response.status_code, 200)
        # self.assertEqual(response.content, "xml...")

    def test_oai_badVerb(self):
        print("** Test: test_oai_badVerb **")
        response = self.client.get("/oai/?verb=badRequest")
        self.assertEqual(response.status_code, 405)

    def test_oai_GetRecord(self):
        print("** Test: test_oai_GetRecord eudml-article2 et oai_dc **")
        response = self.client.get(
            "/oai/?verb=GetRecord&identifier=oai:numdam.org:SMS_1969-1970__1__A1_0&metadataPrefix=oai_dc",
            headers={"host": "127.0.0.1"},
        )
        self.assertContains(
            response,
            "<dc:identifier>oai:numdam.org:SMS_1969-1970__1__A1_0</dc:identifier>",
            status_code=200,
        )

        response = self.client.get(
            "/oai/?verb=GetRecord&identifier=oai:numdam.org:SMS_1969-1970__1__A1_0&metadataPrefix=eudml-article2",
            headers={"host": "127.0.0.1"},
        )
        self.assertEqual(response.status_code, 200)
        dataXml = response.content
        tree = etree.XML(dataXml)

        module_dir = os.path.dirname(__file__)  # get current directory
        file_path = os.path.join(module_dir, "eudml-article-2.0.xsd")
        xmlschema_doc = etree.parse(file_path)
        xmlschema = etree.XMLSchema(xmlschema_doc)
        article_xml = tree.xpath('//*[local-name()="article"]')
        self.assertEqual(len(article_xml), 1)
        xmlschema.assertValid(article_xml[0])
        is_valid = xmlschema.validate(article_xml[0])
        self.assertTrue(is_valid)
        xmlschema.assertValid(article_xml[0])

        title = tree.xpath('//*[local-name()="article-title"]')[0]
        self.assertEqual(title.text, "Un langage quasi-rationnel lié aux graphes planaires test &")
        sets = tree.xpath('//*[local-name()="setSpec"]')
        # With settings.SITE_NAME = "numdam", we should have 2 sets
        self.assertEqual(len(sets), 2)

        # test de la date du record
        date_str = tree.xpath('//*[local-name()="datestamp"]')[0].text
        my_article = Article.objects.get(pid="SMS_1969-1970__1__A1_0")
        deploy_date = my_article.deployed_date().strftime("%Y-%m-%d")
        self.assertEqual(date_str, deploy_date)

        # test de l'absence de prod-deployed-date : uniquement utilisé pour l'export
        prod_deployed_date = tree.xpath('//*[@date-type="prod-deployed-date"]')
        self.assertTrue(len(prod_deployed_date) == 0)

    # ( / bookstore / book[ @ location = 'US'])[1]
    def test_oai_GetRecord_badMetadataPrefix(self):
        print("** Test: test_GetRecord_badPX **")
        response = self.client.get(
            "/oai/?verb=GetRecord&identifier=oai:numdam.org:SMS_1969-1970__1__A1_0&metadataPrefix=eudml-book",
            headers={"host": "127.0.0.1"},
        )
        self.assertEqual(response.status_code, 405)
        dataXml = response.content
        tree = etree.XML(dataXml)
        # print(etree.tostring(tree, pretty_print=True))
        nsmap = {"oai": "http://www.openarchives.org/OAI/2.0/"}
        error = tree.xpath("//oai:error", namespaces=nsmap)
        error = error[0].attrib["code"]
        self.assertEqual(error, "CanNotDisseminateFormat")

    def test_oai_GetRecord_badID(self):
        print("** Test: test_oai_GetRecord_badID **")
        response = self.client.get(
            "/oai/?verb=GetRecord&identifier=oaitest.org:_IDtest_&metadataPrefix=oai_dc",
            headers={"host": "127.0.0.1"},
        )
        self.assertEqual(response.status_code, 200)

    def test_oai_GetRecord_badObject_eudml_article2(self):
        f = open("tests/data/xml/book.xml")
        body = f.read()
        f.close()
        # Ajout d'un book
        response = self.client.post("/upload/books/", data=body, content_type="application/xml")
        self.assertEqual(response.status_code, 201)

        print("** Test: test_oai_GetRecord bad Object eudml-article2 **")
        response = self.client.get(
            "/oai/?verb=GetRecord&identifier=oai:numdam.org:MSMF_2014_2_138-139__1_0&metadataPrefix=eudml-article2",
            headers={"host": "127.0.0.1"},
        )
        self.assertEqual(response.status_code, 405)

    def test_oai_GetRecord_BOOK(self):
        print("** test_oai_GetRecord_BOOK oai_dc et eudml_book2")

        f = open("tests/data/xml/book.xml")
        body = f.read()
        f.close()
        # Ajout d'un book
        response = self.client.post("/upload/books/", data=body, content_type="application/xml")
        self.assertEqual(response.status_code, 201)

        response = self.client.get(
            "/oai/?verb=GetRecord&identifier=oai:numdam.org:MSMF_2014_2_138-139__1_0&metadataPrefix=oai_dc",
            headers={"host": "127.0.0.1"},
        )
        self.assertContains(
            response,
            "<dc:identifier>oai:numdam.org:MSMF_2014_2_138-139__1_0</dc:identifier>",
            status_code=200,
        )

        response = self.client.get(
            "/oai/?verb=GetRecord&identifier=oai:numdam.org:MSMF_2014_2_138-139__1_0&metadataPrefix=eudml-book2",
            headers={"host": "127.0.0.1"},
        )
        self.assertEqual(response.status_code, 200)
        dataXml = response.content
        tree = etree.XML(dataXml)

        module_dir = os.path.dirname(__file__)  # get current directory
        file_path = os.path.join(module_dir, "eudml-book-2.0.xsd")
        xmlschema_doc = etree.parse(file_path)
        xmlschema = etree.XMLSchema(xmlschema_doc)
        book_xml = tree.xpath('//*[local-name()="book"]')
        self.assertEqual(len(book_xml), 1)
        self.assertIsNone(xmlschema.assertValid(book_xml[0]))

    def test_oai_GetRecord_edited_book_eudml_book2(self):
        print("** test_oai_GetRecord_edited_book_eudml_book2")

        f = open("tests/data/xml/book-with-part.xml")
        body = f.read()
        f.close()
        # Ajout d'un book
        response = self.client.post("/upload/books/", data=body, content_type="application/xml")
        self.assertEqual(response.status_code, 201)

        # niveau container : on vérifie la présence des articles
        response = self.client.get(
            "/oai/?verb=GetRecord&identifier=oai:numdam.org:MSMF_1993_2_52_&metadataPrefix=eudml-book2",
            headers={"host": "127.0.0.1"},
        )
        dataXml = response.content
        tree = etree.XML(dataXml)
        book_parts = tree.xpath('//*[local-name()="book-part"]')
        ids_article = []
        for part in book_parts:
            child = part[0]
            for neighbor in child.iter():
                if "pub-id-type" in neighbor.attrib:
                    if neighbor.attrib["pub-id-type"] == "mathdoc-id":
                        id_node = neighbor.text
                        ids_article.append(id_node)

        self.assertEqual(len(ids_article), 3)

        # niveau article : doit retourner un seul book part
        response = self.client.get(
            "/oai/?verb=GetRecord&identifier=oai:numdam.org:MSMF_1993_2_52__1_0&metadataPrefix=eudml-book2",
            headers={"host": "127.0.0.1"},
        )
        self.assertEqual(response.status_code, 200)
        dataXml = response.content
        tree = etree.XML(dataXml)

        module_dir = os.path.dirname(__file__)  # get current directory
        file_path = os.path.join(module_dir, "eudml-book-2.0.xsd")
        xmlschema_doc = etree.parse(file_path)
        xmlschema = etree.XMLSchema(xmlschema_doc)
        book_xml = tree.xpath('//*[local-name()="book"]')
        self.assertEqual(len(book_xml), 1)
        self.assertIsNone(xmlschema.assertValid(book_xml[0]))

        book_part = tree.xpath('//*[local-name()="book-part"]')
        self.assertEqual(len(book_part), 1)

    def test_oai_GetRecord_eudml_book2_monograph_with_book_parts(self):
        print("** test_oai_GetRecord eudml_book2_monograph_with_book_parts ")

        f = open("tests/data/xml/book-monograph-with-parts.xml")
        body = f.read()
        f.close()
        # Ajout d'un book
        response = self.client.post("/upload/books/", data=body, content_type="application/xml")
        self.assertEqual(response.status_code, 201)

        # on doit avoir une réponse niveau Container pas niveau Article avec
        # concaténation des bibliographies
        response = self.client.get(
            "/oai/?verb=GetRecord&identifier=oai:numdam.org:CIF_1982-1983__18_&metadataPrefix=eudml-book2",
            headers={"host": "127.0.0.1"},
        )
        self.assertEqual(response.status_code, 200)
        dataXml = response.content
        tree = etree.XML(dataXml)

        book_parts = tree.xpath('//*[local-name()="book-part"]')
        self.assertEqual(len(book_parts), 6)

        nsmap = {"b": "http://eudml.org/schema/2.0/eudml-book", "j": "http://jats.nlm.nih.gov"}
        book_refs = tree.xpath("//b:book/j:back/j:ref-list/j:ref", namespaces=nsmap)
        self.assertEqual(len(book_refs), 38)

        response = self.client.get(
            "/oai/?verb=GetRecord&identifier=oai:numdam.org:CIF_1982-1983__18__A2_0&metadataPrefix=eudml-book2",
            headers={"host": "127.0.0.1"},
        )
        self.assertEqual(response.status_code, 405)

    def test_oai_GetRecord_Collection(self):
        print("** test_oai_GetRecord_Collection")
        response = self.client.get(
            "/oai/?verb=GetRecord&identifier=oai:numdam.org:SMS&metadataPrefix=oai_dc",
            headers={"host": "127.0.0.1"},
        )
        self.assertEqual(response.status_code, 200)

        dataXml = response.content
        tree = etree.XML(dataXml)

        module_dir = os.path.dirname(__file__)  # get current directory
        file_path = os.path.join(module_dir, "oai_dc.xsd")
        xmlschema_doc = etree.parse(file_path)
        xmlschema = etree.XMLSchema(xmlschema_doc)
        oai_xml = tree.xpath('//*[local-name()="dc"]')
        self.assertEqual(len(oai_xml), 1)
        self.assertTrue(xmlschema.validate(oai_xml[0]))
        # pour avoir les erreurs : xmlschema.assertValid(oai_xml[0]))

        sets = tree.xpath('//*[local-name()="setSpec"]')
        self.assertEqual(len(sets), 1)

    def test_oai_ListRecords_oai_dc(self):
        print("** test_oai_ListRecords_oai_dc")

        # ajout d'un book monograph sans book-part
        f = open("tests/data/xml/these.xml")
        body = f.read()
        f.close()
        # Ajout d'un book
        response = self.client.post("/upload/books/", data=body, content_type="application/xml")
        self.assertEqual(response.status_code, 201)

        # ajout d'un book edited book
        f = open("tests/data/xml/book-msmf.xml")
        body = f.read()
        f.close()
        # Ajout d'un book
        response = self.client.post("/upload/books/", data=body, content_type="application/xml")
        self.assertEqual(response.status_code, 201)

        compteur = 0
        for cont in Container.objects.all():
            if cont.ctype in ["book-monograph", "book-edited-book"]:
                compteur = compteur + 1
            if cont.article_set.count() > 0:
                for art in cont.article_set.all():
                    if art.my_container.ctype != "book-monograph":
                        compteur = compteur + 1

        response = self.client.get(
            "/oai/?verb=ListRecords&metadataPrefix=oai_dc", headers={"host": "127.0.0.1"}
        )
        self.assertEqual(response.status_code, 200)

        dataXml = response.content
        tree = etree.XML(dataXml)

        # record = tree.xpath('//*[local-name()="record"]')

        resumptionToken = tree.xpath('//*[local-name()="resumptionToken"]')[0]
        size = resumptionToken.get("completeListSize")

        self.assertEqual(int(size), compteur)

    def test_oai_ListRecords_with_Journalset(self):
        print("** test_oai_ListRecords_with_Journalset")
        response = self.client.get(
            "/oai/?verb=ListRecords&metadataPrefix=oai_dc&set=SMS", headers={"host": "127.0.0.1"}
        )
        self.assertEqual(response.status_code, 200)

    def test_oai_ListRecords_with_THESESet(self):
        print("** test_oai_ListRecords_with_THESESet")
        f = open("tests/data/xml/these.xml")
        body = f.read()
        f.close()
        # Ajout d'un book
        response = self.client.post("/upload/books/", data=body, content_type="application/xml")
        self.assertEqual(response.status_code, 201)

        compteur = Container.objects.filter(my_collection__pid="THESE").count()
        response = self.client.get(
            "/oai/?verb=ListRecords&metadataPrefix=oai_dc&set=THESE", headers={"host": "127.0.0.1"}
        )
        self.assertEqual(response.status_code, 200)

        dataXml = response.content
        tree = etree.XML(dataXml)

        record = tree.xpath('//*[local-name()="record"]')
        self.assertEqual(len(record), compteur)

    def test_oai_ListsRecords_GALLICA(self):
        print("** test_oai_ListsRecords_GALLICA")
        response = self.client.get(
            "/oai/?verb=ListRecords&metadataPrefix=oai_dc&set=gallica",
            headers={"host": "127.0.0.1"},
        )
        self.assertEqual(response.status_code, 200)
        dataXml = response.content
        tree = etree.XML(dataXml)
        compteur = Collection.objects.all().count()
        print(Collection.objects.all().values("pid"))
        record = tree.xpath('//*[local-name()="record"]')
        self.assertEqual(len(record), compteur)

    def test_oai_ListsRecords_gallicaPrefix_mustFailed(self):
        print("** test_oai_ListsRecords_gallicaPrefix")
        response = self.client.get(
            "/oai/?verb=ListRecords&metadataPrefix=gallica", headers={"host": "127.0.0.1"}
        )
        self.assertEqual(response.status_code, 405)

    def test_oai_ListsRecords_Eudml_book(self):
        print("** test_oai_ListsRecords_Eudml_book")

        f = open("tests/data/xml/book.xml")
        body = f.read()
        f.close()
        # Ajout d'un book
        response = self.client.post("/upload/books/", data=body, content_type="application/xml")
        self.assertEqual(response.status_code, 201)

        response = self.client.get(
            "/oai/?verb=ListRecords&metadataPrefix=eudml-book2&set=NUMDAM_book",
            headers={"host": "127.0.0.1"},
        )
        self.assertEqual(response.status_code, 200)
        book_monograph_count = Container.objects.filter(ctype="book-monograph").count()
        edited_book_count = Article.objects.filter(my_container__ctype="book-edited-book").count()
        dataXml = response.content
        tree = etree.XML(dataXml)
        record = tree.xpath('//*[local-name()="record"]')
        self.assertEqual(len(record), book_monograph_count + edited_book_count)

        # TODO ; vérif nbre de réponse et resumptionToken
        response = self.client.get(
            "/oai/?verb=ListRecords&metadataPrefix=eudml-book2&set=MSMF",
            headers={"host": "127.0.0.1"},
        )
        self.assertEqual(response.status_code, 200)

        MSMF_count = Container.objects.filter(my_collection__pid="MSMF").count()
        dataXml = response.content
        tree = etree.XML(dataXml)
        record = tree.xpath('//*[local-name()="record"]')
        self.assertEqual(len(record), MSMF_count)

    def test_oai_ListsRecords_badSet_Eudml_book(self):
        print("** test_oai_ListsRecords_badSet_Eudml_book")
        f = open("tests/data/xml/book.xml")
        body = f.read()
        f.close()
        # Ajout d'un book
        response = self.client.post("/upload/books/", data=body, content_type="application/xml")
        self.assertEqual(response.status_code, 201)

        response = self.client.get(
            "/oai/?verb=ListRecords&metadataPrefix=eudml-book2&set=GALLICA",
            headers={"host": "127.0.0.1"},
        )
        self.assertEqual(response.status_code, 405)
        # TODO ; vérif nbre de réponse et resumptionToken

    def test_oai_ListsRecords_eudml_article2(self):
        print("** test_oai_ListsRecords_eudml_article2")
        response = self.client.get(
            "/oai/?verb=ListRecords&metadataPrefix=eudml-article2&set=NUMDAM",
            headers={"host": "127.0.0.1"},
        )
        self.assertEqual(response.status_code, 200)

        # a priori 14 reponses
        # je vais mettre la max size à 7 pour voir si il y a un token foireux ?
        # TODO ; vérif nbre de réponse et resumptionToken

    def test_oai_ListsRecords_eudml_article2_with_Journalset(self):
        print("** test_oai_ListsRecords_eudml_article2_with_Journalset")
        response = self.client.get(
            "/oai/?verb=ListRecords&metadataPrefix=eudml-article2&set=SMS",
            headers={"host": "127.0.0.1"},
        )
        self.assertEqual(response.status_code, 200)
        # TODO ; vérif nbre de réponse et resumptionToken

    def test_oai_ListsRecords_eudml_article2_badSet(self):
        print("** test_oai_ListsRecords_eudml_article2_badSet")
        response = self.client.get(
            "/oai/?verb=ListRecords&metadataPrefix=eudml-article2&set=GALLICA",
            headers={"host": "127.0.0.1"},
        )
        self.assertEqual(response.status_code, 405)

    def test_oai_ListsRecords_oai_dc(self):
        print("** test_oai_ListsRecords_eudml_article2_badSet")
        response = self.client.get(
            "/oai/?verb=ListRecords&metadataPrefix=oai_dc&set=NUMDAM",
            headers={"host": "127.0.0.1"},
        )
        self.assertEqual(response.status_code, 200)

    def test_oai_ListMetadataFormats(self):
        print("** test_oai_ListMetadataFormats")
        response = self.client.get("/oai/?verb=ListMetadataFormats", headers={"host": "127.0.0.1"})
        self.assertEqual(response.status_code, 200)
        dataXml = response.content
        tree = etree.XML(dataXml)
        nsmap = {"oai": "http://www.openarchives.org/OAI/2.0/"}
        metadataPrefixes = tree.xpath("//oai:metadataPrefix", namespaces=nsmap)
        self.assertEqual(len(metadataPrefixes), 3)

    def test_oai_ListMetadataFormats_with_ArticlePid(self):
        print("** test_oai_ListMetadataFormats with article pid")
        response = self.client.get(
            "/oai/?verb=ListMetadataFormats&identifier=oai:numdam.org:SMS_1969-1970__1__A1_0",
            headers={"host": "127.0.0.1"},
        )
        self.assertEqual(response.status_code, 200)
        dataXml = response.content
        tree = etree.XML(dataXml)
        # print(etree.tostring(tree, pretty_print=True))
        nsmap = {"oai": "http://www.openarchives.org/OAI/2.0/"}
        metadataPrefixes = tree.xpath("//oai:metadataPrefix", namespaces=nsmap)
        self.assertEqual(len(metadataPrefixes), 2)

    def test_oai_ListMetadataFormats_with_Article_from_Book(self):
        print("** test_oai_ListMetadataFormats with Article from Book")

        f = open("tests/data/xml/book.xml")
        body = f.read()
        f.close()
        # Ajout d'un book
        response = self.client.post("/upload/books/", data=body, content_type="application/xml")
        self.assertEqual(response.status_code, 201)

        response = self.client.get(
            "/oai/?verb=ListMetadataFormats&identifier=oai:numdam.org:MSMF_2014_2_138-139__1_0",
            headers={"host": "127.0.0.1"},
        )
        self.assertEqual(response.status_code, 200)
        dataXml = response.content
        tree = etree.XML(dataXml)
        # print(etree.tostring(tree, pretty_print=True))
        nsmap = {"oai": "http://www.openarchives.org/OAI/2.0/"}
        metadataPrefixes = tree.xpath("//oai:metadataPrefix", namespaces=nsmap)
        self.assertEqual(len(metadataPrefixes), 2)

    def test_oai_ListMetadataFormats_with_Serial(self):
        print("** test_oai_ListMetadataFormats with Serial")
        response = self.client.get(
            "/oai/?verb=ListMetadataFormats&identifier=oai:numdam.org:SMS",
            headers={"host": "127.0.0.1"},
        )
        self.assertEqual(response.status_code, 200)
        dataXml = response.content
        tree = etree.XML(dataXml)
        # print(etree.tostring(tree, pretty_print=True))
        nsmap = {"oai": "http://www.openarchives.org/OAI/2.0/"}
        metadataPrefixes = tree.xpath("//oai:metadataPrefix", namespaces=nsmap)
        self.assertEqual(len(metadataPrefixes), 1)

    def test_oai_ListSets(self):
        print("** test_oai_ListSets")
        response = self.client.get("/oai/?verb=ListSets", headers={"host": "127.0.0.1"})
        self.assertEqual(response.status_code, 200)
        # TODO ; vérif nbre de réponse

    def test_oai_from_until(self):
        print("** test_oai_from_until **")
        response = self.client.get(
            "/oai/?verb=ListRecords&metadataPrefix=eudml-article2&set=NUMDAM&from=2011-01-21&until=2056-02-15",
            headers={"host": "127.0.0.1"},
        )
        self.assertEqual(response.status_code, 200)

        response = self.client.get(
            "/oai/?verb=ListRecords&metadataPrefix=eudml-article2&set=NUMDAM&from=2055-01-21&until=2056-02-15",
            headers={"host": "127.0.0.1"},
        )
        self.assertContains(
            response, '<error code="NoRecordsMatch">Nothing to list</error>', status_code=200
        )

    def test_oai_from_until_with_date_published(self):
        print("** test_oai_from_until_with_date_published **")
        # dans le cas où l'on récupère que des Articles avec date_published
        # hook la date deployed :
        art = Article.objects.get(pid="SMS_1969-1970__1__A1_0")
        sm = art.sitemembership_set.filter(site__id=settings.SITE_ID).get()
        from oai.oai_helpers import parse_date_str

        sm.deployed = parse_date_str("2018-10-03")
        sm.save()
        settings.OAI_BY_DATE_PUBLISHED = True
        response = self.client.get(
            "/oai/?verb=ListRecords&metadataPrefix=oai_dc&set=NUMDAM&from=2018-10-03&until=2018-10-06",
            headers={"host": "127.0.0.1"},
        )
        self.assertEqual(response.status_code, 200)
        dataXml = response.content
        tree = etree.XML(dataXml)
        # print(etree.tostring(tree, pretty_print=True))
        # nsmap = {"oai": "http://www.openarchives.org/OAI/2.0/"}
        articles = tree.xpath('//*[local-name()="record"]')
        self.assertEqual(len(articles), 1)

        response = self.client.get(
            "/oai/?verb=ListRecords&metadataPrefix=eudml-article2&set=NUMDAM&from=2055-01-21&until=2056-02-15",
            headers={"host": "127.0.0.1"},
        )
        self.assertContains(
            response, '<error code="NoRecordsMatch">Nothing to list</error>', status_code=200
        )
        settings.OAI_BY_DATE_PUBLISHED = False

    def test_oai_datestamp(self):
        print("** test_oai_datestamp")
        aid = "SMS_1969-1970__1__A1_0"
        article = get_article(aid)
        datestamp = article.deployed_date
        f = open("tests/data/xml/issue.xml")
        body = f.read()
        f.close()
        # PUT de la meme issue
        response = self.client.put("/upload/issues/", data=body, content_type="application/xml")
        self.assertEqual(response.status_code, 204)
        article = get_article(aid)
        self.assertNotEqual(datestamp, article.deployed_date)

    def test_oai_gallica_container_without_publisher(self):
        print("** test_oai_gallica_container_without_publisher **")
        first_container = Container.objects.first()
        first_container.my_publisher = None
        first_container.save()
        response = self.client.get(
            "/oai/?verb=ListRecords&metadataPrefix=oai_dc&set=gallica",
            headers={"host": "127.0.0.1"},
        )
        self.assertEqual(response.status_code, 200)

    def test_oai_resumptionToken(self):
        print("** test_oai_resumptionToken **")

        with open("tests/data/xml/collection-nam.xml") as xml_file:
            self.client.post(
                "/upload/collections/", data=xml_file.read(), content_type="application/xml"
            )

        with open("tests/data/xml/issue-nam.xml") as xml_file:
            self.client.post(
                "/upload/issues/", data=xml_file.read(), content_type="application/xml"
            )

        response = self.client.get(
            "/oai/?verb=ListRecords&metadataPrefix=oai_dc", headers={"host": "127.0.0.1"}
        )
        self.assertEqual(response.status_code, 200)

        dataXml = response.content
        tree = etree.XML(dataXml)

        __ = tree.xpath('//*[local-name()="record"]')

        resumptionToken = tree.xpath('//*[local-name()="resumptionToken"]')[0]
        __ = resumptionToken.get("completeListSize")
        resumptionToken = resumptionToken.text

        while resumptionToken is not None:
            response = self.client.get(
                "/oai/?verb=ListRecords&resumptionToken=" + resumptionToken,
                headers={"host": "127.0.0.1"},
            )
            self.assertEqual(response.status_code, 200)

            dataXml = response.content
            tree = etree.XML(dataXml)

            __ = tree.xpath('//*[local-name()="record"]')

            resumptionToken = tree.xpath('//*[local-name()="resumptionToken"]')[0]
            __ = resumptionToken.get("completeListSize")
            resumptionToken = resumptionToken.text
