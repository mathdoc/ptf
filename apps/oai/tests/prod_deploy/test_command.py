import io
import json

from django.core.management import call_command
from django.test import TestCase

from ptf import model_helpers
from ptf.cmds import xml_cmds
from ptf.management.commands.deploy import DeployFailed


class DeployTest(TestCase):
    def test_command_deploy(self):
        # on peuple avant la base

        print("** test command deploy")

        f = open("tests/data/xml/collection.xml")
        body = f.read()
        f.close()

        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": body})
        collections = cmd0.do()

        self.assertEqual(len(collections), 1)

        f = open("tests/data/xml/book-with-last-modified-date.xml")
        body = f.read()
        f.close()

        cmd = xml_cmds.addBookXmlCmd({"body": body})
        book = cmd.do()

        # sms = SiteMembership.objects.filter(resource__classname='Container').all()
        # for sm in sms:
        #     resource = sm.resource
        #     date = sm.deployed
        out = io.StringIO()

        # on recupere sous format JSON les donnees de publication des
        # containers
        call_command("deploy", export=True, stdout=out)
        val = out.getvalue()
        dict = json.loads(val)
        self.assertEqual(1, len(dict))

        # test OAI : on fait un OAI GetRecord pour voir si on a bien la date de
        # deploy

        response = self.client.get(
            "/oai/?verb=GetRecord&identifier=oai:numdam.org:CIF_1980__15__1_0&metadataPrefix=eudml-book2",
            headers={"host": "127.0.0.1"},
        )
        self.assertEqual(response.status_code, 200)
        dataXml = response.content
        from lxml import etree

        tree = etree.XML(dataXml)

        title = tree.xpath('//*[local-name()="book-title"]')[0]
        self.assertEqual(title.text, "Résidus en codimension ")

        # test de la date du record
        date_str = tree.xpath('//*[local-name()="datestamp"]')[0].text
        # my_book = Container.objects.get(pid='CIF_1980__15__1_0')
        deploy_date = book.deployed_date().strftime("%Y-%m-%d")
        self.assertEqual(date_str, deploy_date)

        # on modifie la date deployed pour voir si elle est bien prise en compte
        # et si la date 'last_modified' provenant du raffinement n'a pas change

        new_deployed_date = model_helpers.parse_date_str("2014-01-01")
        # dict[book.pid]['deployed'] = new_deployed_date.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
        dict[book.pid]["deployed"] = new_deployed_date.isoformat()

        val = json.dumps(dict)
        filemem = io.StringIO()
        filemem.write(val)
        call_command("deploy", importation=filemem, stdout=out)
        # sms = SiteMembership.objects.filter(resource__classname='Container').first()

        # self.assertEqual(book.last_modified.strftime("%Y-%m-%dT%H:%M:%SZ"), dict[book.pid]['last_modified'])
        self.assertEqual(
            book.last_modified, model_helpers.parse_date_str(dict[book.pid]["last_modified"])
        )

        self.assertEqual(book.deployed_date(), new_deployed_date)

        # test OAI : on refait un OAI GetRecord pour voir si on a bien
        # l'ancienne date de deploy

        response = self.client.get(
            "/oai/?verb=GetRecord&identifier=oai:numdam.org:CIF_1980__15__1_0&metadataPrefix=eudml-book2",
            headers={"host": "127.0.0.1"},
        )
        self.assertEqual(response.status_code, 200)
        dataXml = response.content

        tree = etree.XML(dataXml)
        title = tree.xpath('//*[local-name()="book-title"]')[0]
        self.assertEqual(title.text, "Résidus en codimension ")

        # test de la date du record
        date_str = tree.xpath('//*[local-name()="datestamp"]')[0].text
        self.assertEqual(date_str, new_deployed_date.strftime("%Y-%m-%d"))

        # on teste avec from avec une date > new_deployed_date pour vérifier
        # que rien n'est retourné

        response = self.client.get(
            "/oai/?verb=ListRecords&metadataPrefix=eudml-book2&from=2014-02-01",
            headers={"host": "127.0.0.1"},
        )
        self.assertContains(
            response, '<error code="NoRecordsMatch">Nothing to list</error>', status_code=200
        )

        # fin test OAI

        # on met une date de last modified plus recente sur la prod dans le json exporte
        # pour tester l'erreur PROD plus a jour que PRE
        dict[book.pid]["last_modified"] = model_helpers.parse_date_str("2050-01-01").isoformat()
        val = json.dumps(dict)
        filemem = io.StringIO()
        filemem.write(val)
        self.assertRaises(DeployFailed, call_command, "deploy", importation=filemem, stdout=out)
