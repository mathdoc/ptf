from django.urls import include
from django.urls import path

from ptf import urls as ptf_urls
from ptf.urls import ptf_i18n_patterns
from upload import urls as upload_urls

from ..urls import urlpatterns

urlpatterns += [
    path("", include(ptf_urls)),
    path("upload/", include(upload_urls)),
]

urlpatterns += ptf_i18n_patterns
