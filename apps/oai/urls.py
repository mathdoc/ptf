from django.urls import path

from oai.views import OAIView

urlpatterns = [
    path("oai/", OAIView.as_view(), name="oai-server"),
]
