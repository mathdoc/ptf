# editor - ptf vuejs widget

Article and Collection component

 > Collection
    app for edit collection of trammel
 > Article
    app for article editor and ptf - translation

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

To compile vart with Article.vue
```
npm run build
```

To compile vart with Collection.vue
```
GEODESIC=1 npm run build
```
> collection compiled must be in dist/js/collection

### Lints and fixes files

```
npm run lint
```



### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

