const { defineConfig } = require("@vue/cli-service");
module.exports = defineConfig({
    transpileDependencies: true,
    filenameHashing: false,
    lintOnSave: false,
    chainWebpack: (config) => {
        config.performance.maxEntrypointSize(400000).maxAssetSize(400000);
    },
    pages: {
        app: {
          // entry for the page
          entry: process.env.GEODESIC ? 'src/main_collection.js' : 'src/main.js',
          // output as dist/index.html
          filename: 'index.html',
        },
    },
    outputDir: process.env.GEODESIC ? 'geodesic' : 'dist' // dist/collection for building collection
});
