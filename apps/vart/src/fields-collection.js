export const fieldsCollectionPtf = {
  pid: {'type':'text','required':false, 'label': 'Pid'},
  provider: {'type':'select', 'required':true, 'label': 'Provider'},
  coltype: {'type':'text', 'required':true, 'label': 'Type de collection'},
  title_tex: {'type':'text', 'required':true, 'label': 'Titre tex'},
  abbrev: {'type':'text', 'required':false, 'label': 'Abbrev'},
  doi: {'type':'text', 'required':false, 'label': 'Doi'},
  wall: {'type':'text', 'required':false, 'label': 'Wall', default:0},
  alive: {'type':'checkbox', 'required':true, 'label': 'Alive'},
  sites: {'type':'multiple', 'required':true, 'label': 'Sites'},
  description_en: {'type':'editor', 'required':false, 'label': 'Description (EN)'},
  description_fr: {'type':'editor', 'required':false, 'label': 'Description (FR)'},
  external_links: {'type':'component', 'required':false, 'label': 'Liens externes'}
}
