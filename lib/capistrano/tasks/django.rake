namespace :python do
  def virtualenv_path
    File.join(
      fetch(:shared_virtualenv) ? shared_path : release_path, "venv"
    )
  end

  desc "Create a python virtualenv"
  task :create_virtualenv do
    unless fetch(:skip_python)
      on roles(:all) do |h|
        execute "python3 -m venv #{virtualenv_path}"
        execute "#{virtualenv_path}/bin/pip install --upgrade pip"
        execute "#{virtualenv_path}/bin/pip install --upgrade -r #{release_path}/#{fetch(:pip_requirements)}"
        if fetch(:shared_virtualenv)
          execute :ln, "-s", virtualenv_path, File.join(release_path, 'venv')
        end
      end

      unless fetch(:ignore_npm)
        invoke 'nodejs:npm'
      end
      invoke 'django:setup'
      invoke 'python:precompile'
    end
  end

  after 'deploy:updating', 'python:create_virtualenv'

  desc "Create __pycache__"
  task :precompile do
    on roles(:all) do
      execute "#{virtualenv_path}/bin/python -m compileall -q #{release_path}/"
    end
  end
end

namespace :django do
  def django(args, flags="", run_on=:all)
    on roles(run_on) do |h|
      manage_path = File.join(release_path, fetch(:django_project_dir) || '', 'manage.py')
      execute "#{release_path}/venv/bin/python #{manage_path} #{args} --settings=#{fetch(:django_settings)} #{flags}"
    end
  end

  desc "Setup Django environment"
  task :setup do
    # invoke 'django:symlink_settings'
    invoke 'django:compilemessages'
    invoke 'django:collectstatic'
    # invoke 'django:symlink_wsgi'

    invoke 'django:migrate'
  end

  desc "Compile Messages"
  task :compilemessages do
    if fetch :compilemessages
      django("compilemessages")
    end
  end

  desc "Run django's collectstatic"
  task :collectstatic do
    django("collectstatic", "-l -i *.coffee -i *.less --noinput")
  end

  desc "Symlink django settings to settings_deployed.py"
  task :symlink_settings do
    settings_path = File.join(release_path, fetch(:django_settings_dir))
    on roles(:all) do
      execute "ln -s #{settings_path}/#{fetch(:django_settings)}.py #{settings_path}/settings_deployed.py"
    end
  end

  desc "Symlink wsgi script to live.wsgi"
  task :symlink_wsgi do
    on roles(:web) do
      wsgi_path = File.join(release_path, fetch(:wsgi_path, 'wsgi'))
      wsgi_file_name = fetch(:wsgi_file_name, 'main.wsgi')
      execute "ln -sf #{wsgi_path}/#{wsgi_file_name} #{wsgi_path}/live.wsgi"
    end
  end

  desc "Run django migrations"
  task :migrate do
    django("migrate", "--noinput", run_on=:web)
    if ENV['convert_contrib']
      invoke 'django:convert_contrib'
    end
  end

  desc "Run django database migration"
  task :convert_contrib do
    colid = fetch(:application).upcase
    flag = "-pid #{colid}"
    django("convert_contribs", flag, run_on=:web)
  end

  desc "Restart Solr and Apache2 service"
  task :restart_daemons do
    on roles(:all) do
      unless fetch(:ignore_solr)
        execute "curl -s 'http://localhost:8983/solr/admin/cores?action=RELOAD&core=core0'"
      end
      unless fetch(:ignore_apache)
        execute "sudo service apache2 restart"
      end
      invoke 'django:restart_celery'
    end
  end

  desc "Restart Celery"
  task :restart_celery do
    if fetch(:celery_name)
      invoke 'django:restart_celeryd'
    end
  end

  desc "Restart Celeryd"
  task :restart_celeryd do
    on roles(:all) do
      execute "sudo systemctl restart #{fetch(:celery_name)}"
    end
  end

  desc "Lock"
  task :lock do
    on roles(:all) do
      execute "touch #{fetch(:lock_file)}"
    end
  end

  desc "Unlock"
  task :unlock do
    on roles(:all) do
      execute "rm #{fetch(:lock_file)}"
    end
  end
end

namespace :nodejs do
  desc 'Install node modules'
  task :npm_install do
    on roles(:web) do
      if fetch(:npm_path)
        path = fetch(:npm_path) ? File.join(release_path, fetch(:npm_path)) : release_path
        within path do
          execute 'npm', 'install', fetch(:npm_install_production, '--production')
        end
      end
    end
  end

  desc 'Run npm tasks'
  task :npm do
    invoke 'nodejs:npm_install'
    if fetch(:npm_tasks)
      on roles(:web) do
        path = fetch(:npm_path) ? File.join(release_path, fetch(:npm_path)) : release_path
        within path do
          fetch(:npm_tasks).each do |task, args|
            execute "./node_modules/.bin/#{task}", args
          end
        end
      end
    end
  end
end
