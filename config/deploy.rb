require "colorize"
# config valid only for current version of Capistrano
#lock '3.7'

puts "Inside deploy.rb"
set :repo_url, 'git@gricad-gitlab.univ-grenoble-alpes.fr:mathdoc/ptf.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp
# set :branch, 'master'
set :branch, ENV['BRANCH'] if ENV['BRANCH']

# Default deploy_to directory is /var/www/my_app_name
#set :deploy_to, "/var/www/#{fetch(:application)}"

set :shared_virtualenv, true
#set :django_project_dir, "sites/#{fetch(:application)}"
#set :django_settings_dir, "sites/#{fetch(:application)}/#{fetch(:application)}"
#set :django_settings, "#{fetch(:application)}.settings"
#set :pip_requirements, "sites/#{fetch(:application)}/requirements.txt"
#set :npm_path, "sites/#{fetch(:application)}/#{fetch(:application)}/static"
set :python2_needed, false

# Default value for :scm is :git
#set :scm, :git

set :user, "deployer"
set :use_sudo, false

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
#set :linked_files, fetch(:linked_files, []).push("#{fetch(:django_settings_dir)}/settings_local.py")

# Default value for linked_dirs is []
# set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 3
set :git_shallow_clone, 2

namespace :deploy do

  # Print the target server for deploy. Ask user to continue.
  before :starting, :ensure_consent do
    unless ENV['FORCE_DEPLOY']
      on roles(:app) do |server|
        puts "[Deployment config]".colorize(:yellow)
        puts "You are about to deploy with the following parameters:"
        puts "\tServer:\t\t" + "#{server.hostname}".colorize(:light_cyan)
        puts "\tApplication:\t" + "#{fetch(:application)}".colorize(:light_cyan)
        puts "\tBranch:\t\t" + "#{fetch(:branch)}".colorize(:light_cyan)
      end
      ask(:proceed_with_deploy, nil, prompt: "Proceed with deploy? [Y/n]".colorize(:yellow))
      if fetch(:proceed_with_deploy) != "y" and fetch(:proceed_with_deploy) != "Y"
        puts "You aborted the deployment by inputing \"#{fetch(:proceed_with_deploy)}\"".colorize(:red)
        exit 1
      end
    end
    puts "Proceeding with deployment".colorize(:green)
    invoke "django:lock"
  end

  after :finishing, "django:unlock"

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

  after "deploy", "django:restart_daemons" unless ENV['NORESTART']
end

desc "Restart daemons"
task :restart do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      invoke "django:restart_daemons"
    end
end
