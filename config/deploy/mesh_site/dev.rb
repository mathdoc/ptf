server 'mathdoc-mesh-test.u-ga.fr', user: 'deployer', roles: %w{app db web}
set :application, 'mesh_site'
load './config/common.rb'
set :ignore_solr, true
set :ignore_apache, true
set :npm_path, false

set :lock_file, "/var/www/mesh_site/shared/lock.txt"
