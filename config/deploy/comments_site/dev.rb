server 'mathdoc-ptf-comment.u-ga.fr', user: 'deployer', roles: %w{app db web}
set :application, 'comments_site'
load './config/common.rb'
set :ignore_solr, true
set :ignore_apache, true
set :npm_path, false

set :lock_file, "/var/www/comments_site/shared/lock.txt"
