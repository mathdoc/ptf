puts 'In common.rb'

set :deploy_to, "/var/www/#{fetch(:application)}"

set :django_project_dir, "sites/#{fetch(:application)}"
set :django_settings_dir, "sites/#{fetch(:application)}/#{fetch(:application)}"
set :django_settings, "#{fetch(:application)}.settings"
set :pip_requirements, "sites/#{fetch(:application)}/requirements.txt"
set :npm_path, "sites/#{fetch(:application)}/#{fetch(:application)}/static"
set :ignore_npm, "true"
set :linked_files, fetch(:linked_files, []).push("#{fetch(:django_settings_dir)}/settings_local.py")
set :lock_file, "/var/www/mersenne_shared/lock.txt"

