#!/bin/bash
# Base script to execute the provided Django management command.
# It requires $VENV variable to be set.
# $1: Directory containing the target manage.py file
# $2: the Django management command
set -eo pipefail
echo -e "`date +"%F %T(%:z)"`: Executing Django command '$2'"
source $VENV/bin/activate
cd $1
python manage.py $2
