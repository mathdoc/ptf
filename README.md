# PTF quickstart guide

#### Cloner le dépôt ptf

Dans un premier temps, il faut générer une paire de clé ssh pour son poste de développement

```bash
ssh-keygen -t rsa -b 2048 -C "comment"
```

Ensuite ajouter sa clé publique dans les SSH Keys des User settings de son compte Gitlab de Gricad
On peut ensuite cloner le dépôt de la manière suivante :

```bash
git clone git@gricad-gitlab.univ-grenoble-alpes.fr:mathdoc/ptf.git
# et on se place dans le dossier ptf créé
cd ptf
```

#### Installation des dépendances système :

```bash
sudo apt install python3-dev python3-venv libpq-dev postgresql npm poppler-utils qpdf gettext openjdk-11-jre-headless
```

#### Créer les dossiers nécessaires au fonctionnement de ptf

```bash
sudo mkdir -p /numdam_data
sudo mkdir -p /mersenne_test_data
sudo mkdir -p /mersenne_prod_data
sudo mkdir -p /mathdoc_archive
sudo chown -R <user>:<user> /numdam_data
sudo chown -R <user>:<user> /mersenne_test_data
sudo chown -R <user>:<user> /mersenne_prod_data
sudo chown -R <user>:<user> /mathdoc_archive
```

#### Création des bases de données mersenne

```bash
sudo su - postgres
# On crée une base de données pour les différents sites de revues
createuser -P revues_user
createdb revues -O revues_user

# Si on souhaite travailler sur le site numdam, il est nécessaire de lui créer une base de données aussi
# Il suffit de créer cette base de données de la même manière que les deux autres en utilisant le nom "numdam" et l'utilisateur "numdam_user"

# On quitte le shell postgres
<ctrl-D>
```

#### Installation du moteur de recherche Solr

Voir : https://solr.apache.org/guide/solr/latest/deployment-guide/taking-solr-to-production.html

```bash
wget https://downloads.apache.org/solr/solr/9.6.1/solr-9.6.1-slim.tgz
mv solr-9.6.1-slim.tgz /tmp/
tar xzf /tmp/solr-9.6.1-slim.tgz solr-9.6.1-slim/bin/install_solr_service.sh --strip-components=2
sudo bash ./install_solr_service.sh /tmp/solr-9.6.1-slim.tgz

# Lors d'une mise à jour, pour forcer l'installation utiliser l'option "-f"
sudo bash ./install_solr_service.sh /tmp/solr-9.6.1-slim.tgz -f

# Vérifier que Solr est bien activé
sudo systemctl status solr.service

# Vérifier la version de Solr
sudo /opt/solr/bin/solr version

# Et supprimer le script d'installation solr du dossier courant
rm install_solr_service.sh
```

#### Accéder à l'interface de Solr

Si besoin, l'url d'accès à l'inteface de Solr de l'hôte local est : http://localhost:8983/solr/
Pour accéder à l'interface de Solr d'un serveur ouvrir le fichier /opt/solr/server/etc/jetty-http.xml et commenter la ligne suivante ainsi :
```xml
<!-- <Set name="host"><Property name="solr.jetty.host" default="127.0.0.1"/></Set> -->
```
Cela permet d'accéder à Solr depuis http://hostname:8983/solr/

Vous pouvez aussi faire une redirection locale de port avec SSH :

```bash
ssh -L 8999:localhost:8983 hostname
```

Puis avec votre navigateur accéder à l'interface de Solr depuis http://localhost:8999/solr/

#### Création des collections Solr (une pour les sites des revues)

```bash
sudo su - solr

# On crée une collection solr pour les sites des revues
/opt/solr/bin/solr create -c revues
mv data/revues/conf/solrconfig.xml data/revues/conf/solrconfig.xml.backup
# dans les commandes suivantes <ptf-path> correspond au chemin d'accès vers le répertoire du projet ptf
ln -s /<ptf-path>/apps/ptf/solr/solrconfig.xml data/revues/conf/
ln -s /<ptf-path>/apps/ptf/solr/schema.xml data/revues/conf/

# Si votre disque dur est chiffré, le lien symbolique ne fonctionnera pas. Il faudra donc copier la configuration.
sudo usermod -aG sudo solr
sudo cp /home/<ptf-path>/ptf/apps/ptf/solr/solrconfig.xml data/revues/conf/
sudo cp /home/<ptf-path>/ptf/apps/ptf/solr/schema.xml data/revues/conf/
# ATTENTION : Cette manipulation ne mettera pas à jour automatiquement la configuration, vérifiez régulièrement que celle-ci n'a pas changé.

# Si on souhaite travailler sur le site numdam, il est nécessaire de lui créer une collection solr aussi
# Il suffit de le créer de la même manière que les deux autres en utilisant le nom "numdam"

# On quitte le shell solr
<ctrl-D>

# Et on relance le serveur pour qu'il prenne en compte les nouveaux coeurs
sudo systemctl restart solr.service
```

#### Création des dossiers pour le fonctionnement de Django
```bash
sudo mkdir /var/log/mersenne
sudo mkdir /var/log/mersenne/django_cache
```

## Mise en place du serveur de dev d'une revue

Le projet du journal de l’École polytechnique (JEP) se trouve dans le dossier sites/jep

```bash
cd sites/jep
```

#### Copie de la conf

```bash
cp jep/settings_local.sample.py jep/settings_local.py
```

Vérifier ensuite dans le fichier résultant que les constantes correspondent à l'environnement de développement

#### Installation des dépendances Python

```bash
python3 -m venv venv
source ./venv/bin/activate
pip install -r requirements.txt --upgrade
```

#### Mise en place de pre-commit
Rentrer la commande suivante pour initialiser l'outil 'pre-commit'
```bash
pre-commit install
```

#### Migration de la base de données

```bash
python manage.py migrate
```

#### Création d'un superuser

```bash
python manage.py createsuperuser
```

et suivre les questions posées.

#### Importer une collection

Récuparation des fichiers sources depuis /mathdoc_archive

```bash
sudo mkdir -p /mathdoc_archive
sudo chown -R <user>:<user> /mathdoc_archive
rsync -avzhe ssh ptf-tools:/mathdoc_archive/JEP /mathdoc_archive/
```

Import de la collection
```bash
python manage.py import -pid JEP -folder /mathdoc_archive
```

#### Lancement du serveur de dev

```bash
python -Wa manage.py runserver 8008
```


## Mise en place du serveur de dev de numdam

Le projet numdam se situe dans le dossier sites/ du dépôt ptf

```bash
cd sites/numdam
```

#### Copie de la conf

```bash
cp numdam/settings_local.sample.py numdam/settings_local.py
```

Vérifier ensuite dans le fichier résultant que les constantes correspondent à l'environnement de développement

#### Installation des dépendances Python

```bash
python3 -m venv venv
source ./venv/bin/activate
pip install -r requirements.txt --upgrade
```

#### Création du dossier de log pour numdam

```bash
sudo mkdir -p /var/log/numdam
sudo chown -R <user>:<user> /var/log/numdam
```

#### Migration de la base de données

```bash
python manage.py migrate
```

#### Importer les données de mathdoc_archive

```bash
rsync -avzhe ssh ptf-tools:/mathdoc_archive/AFST /mathdoc_archive/
python manage.py import -pid=AFST -folder=/mathdoc_archive

# Ici, on a choisi d'importer la collection AFST. Mais la procédure demeure identique pour les autres collections (AIF, JEP, ect...)
```

#### Lancement du serveur de dev

```bash
python -Wa manage.py runserver 8003
```

## Utilisation de Capistrano

### Installation de Ruby

Capistrano n'étant plus compatible passé la version **3.2** de Ruby, on doit installer la version **3.1.X**

On va passer par RVM pour gérer l'installation de Ruby

#### Installation de RVM

##### Installation des clés pour RVM

Avant l'installation de RVM, il faudra installer leurs clés, les serveurs de clés peuvent ne pas marcher, dans ce cas, on passera par les serveurs de RVM directement

```bash
gpg --keyserver keyserver.ubuntu.com --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
```

Si cela a marché, vous avez beaucoup de chance !
Sinon, vous pouvez exécuter ces commandes :

```bash
curl -sSL https://rvm.io/mpapis.asc | gpg --import -
curl -sSL https://rvm.io/pkuczynski.asc | gpg --import -
```

Se référer à ces deux pages [[1]](http://rvm.io/rvm/install) [[2]](http://rvm.io/rvm/security)

##### Installation de RVM

```bash
\curl -sSL https://get.rvm.io | bash
```

RVM n'est utilisable qu'au lancement d'un nouveau terminal, donc rouvrez un terminal.

#### Installation de la bonne version de Ruby

```bash
rvm install 3.1
```

Pour utiliser les commandes liées à Ruby, vous pouvez exécuter les commandes suivantes (vous en aurez besoin pour l'étape suivante) :

```bash
/bin/bash --login
rvm use 3.1
```

Si vous sautez la première commande, vous aurez probablement une erreur vous demandant de l'exécuter ou d'aller voir une documentation. Après exécution de celle-ci, tout fonctionnera.

### Installation des dépendances

Utilisez les commandes données plus haut pour activer Ruby. Puis positionnez-vous à la racine du projet ptf et exécutez cette commande qui va installer les dépendances données dans le fichier `Gemfile`

```bash
bundle install
```

Désormais, vous avez accès à la commande `cap` qui permettra l'utilisation de Capistrano

### Déploiement

N'oubliez pas d'initialiser ruby :

```bash
/bin/bash --login
rvm use 3.1
```

Vous pourrez ensuite déployer en utilisant Capistrano :

```bash
cap PID:SERVEUR [BRANCH=NAME] deploy
```

- PID est le PID du site à déployer (`pcj` par exemple)
- SERVEUR donne le serveur sur lequel il faut déployer (`test` ou `prod`)
- NAME permet de choisir la branche dont le code sera utilisé pour le déploiement (facultatif, `master` par défaut mais peut-être gérer au cas par cas dans les configs `/ptf/config/deploy/`)

Si vous avez un problème de droit lors de la commande, c'est probable que ce soit dû à vos clés SSH qui ne sont pas ajoutés automatiquement lors de la création d'un nouveau terminal (créé lors de la commande `/bin/bash --login`).
Dans ce cas, cette commande peut être utile :

```bash
ssh-add -k
```

## Lancement des tests

Afin de pouvoir lancer les tests, il faut donner les droits de création (et suppression) de bases de données.

```bash
sudo su - postgres
psql
```

```sql
ALTER USER revues_user CREATEDB;
ALTER USER numdam_user CREATEDB;
```

```bash
# On quitte le shell psql
<ctrl-D>
# On quitte le shell postgres
<ctrl-D>
```

Pour l'instant, les tests principaux sont dans le dossier numdam.
```bash
cd sites/numdam
source venv/bin/activate

# Pour lancer tous les tests :
python manage.py test tests

# Pour lancer un dossier de test en particulier :
python manage.py test tests.urls

# Pour lancer un fichier de test en particulier :
python manage.py test tests.urls.test_pdf

# Pour lancer un test en particulier :
python manage.py test tests.urls.test_search_url.RequestTestCase.test_01_emptyform
```



# Contributors

- Claude Goutorbe
- Olivier Labbe
- Simon Chevance
- Basile Legal
- Simon Panay
- Alexandre Bouquet
- Jérôme Touvier
- Youness Lehdili
- Matthieu Fanget
- Cynthia Rakotoarisoa
- Clément Lesaulnier
- Guillaume Alzieu
- Patrick Bernaud
- Céline Smith
- Matthieu Guimard
- Xavier Beaufils
- Yohan Baret
- Samuel Conjard
- Nathan Tien You
