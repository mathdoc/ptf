from django.contrib import admin
from django.urls import include
from django.urls import path
from django.urls import re_path

from comments_database import urls as comments_urls

urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    path('', include(comments_urls)),
]
