from rest_framework.permissions import BasePermission

from django.http import HttpRequest

from .models import User


class MathdocSitePermission(BasePermission):
    def has_permission(self, request: HttpRequest, view):
        return (
            isinstance(request.user, User)
            and request.user.is_authenticated
            and request.user.mathdoc_site is True
        )
