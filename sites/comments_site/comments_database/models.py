from typing import Optional

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _

from comments_api.constants import COMMENT_STATUS_CHOICES
from comments_api.constants import MODERATION_STATUS_CHOICES
from comments_api.constants import STATUS_SUBMITTED
from ptf.site_register import SITE_REGISTER


class User(AbstractUser):
    """
    Custom user class.

    A regular (API) user represents a mathdoc website.
    The username must match an entry in site_register.py for mathdoc websites.
    """

    first_name = None
    last_name = None
    url = models.URLField(_("URL"), max_length=258, unique=True, null=True, blank=True)
    mathdoc_site = models.BooleanField(_("Mathdoc site"), default=False)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["mathdoc_site", "username"], name="unique_mathdoc_site"
            )
        ]

    def save(self, *args, **kwargs) -> None:
        if self.mathdoc_site:
            assert self.username in SITE_REGISTER, (
                "ERROR: the user to save is flagged as a mathdoc site but its"
                "username does not match an entry in site_register.py"
            )
        return super().save(*args, **kwargs)


class Moderator(models.Model):
    """
    Represents a comment moderator.
    """

    id = models.IntegerField(_("Trammel user ID"), primary_key=True)

    def __str__(self) -> str:
        return f"Trammel user #{self.id}"


class Comment(models.Model):
    """
    Represents a Resource comment.
    """

    site = models.ForeignKey(
        User, on_delete=models.PROTECT, null=False, blank=False, related_name="comments"
    )
    doi = models.CharField(max_length=64, null=False, blank=False)
    parent: "models.ForeignKey[Comment]" = models.ForeignKey(
        "self", on_delete=models.PROTECT, null=True, blank=True, related_name="children"
    )  # type:ignore
    date_submitted = models.DateTimeField(blank=True)
    date_last_modified = models.DateTimeField(blank=True)

    # Author related data
    # Null values are allowed for author fields, corresponding to deleted comments
    # We do not create an author model because this data can vary for each comment
    # of the same author (mainly provider & provider UID but also e-mail)
    author_id = models.BigIntegerField(null=True, blank=False)
    author_email = models.EmailField(null=True, blank=False)
    author_first_name = models.CharField(max_length=128, null=True, blank=False)
    author_last_name = models.CharField(max_length=128, null=True, blank=False)
    author_provider = models.CharField(max_length=64, null=True, blank=False)
    author_provider_uid = models.CharField(max_length=128, null=True, blank=False)

    # We store the both the raw, unsanitized HTML value of the comment
    # and the sanitized one.
    # The API should always return the sanitized version of this field.
    raw_html = models.TextField()
    sanitized_html = models.TextField()
    status = models.CharField(
        _("Status"),
        default=STATUS_SUBMITTED,
        choices=COMMENT_STATUS_CHOICES,
        blank=False,
        max_length=127,
    )

    # Moderator related
    moderators = models.ManyToManyField(
        Moderator, through="ModerationRights", related_name="comments"
    )
    article_author_comment = models.BooleanField(default=False)
    editorial_team_comment = models.BooleanField(default=False)
    hide_author_name = models.BooleanField(default=False)

    # Boolean used to flag "new" comments. The value is used and updated by a cron
    # script responsible for sending e-mail when there are comments with is_new = True
    is_new = models.BooleanField(default=True)
    # Boolean used to track if a comment has already been "Validated". As comments
    # can be re-moderated, we use it to not send an e-mail when a comment gets
    # validated again.
    validation_email_sent = models.BooleanField(default=False)

    def get_base_url(self) -> str:
        """
        Return the base_url of the website where the comment was posted.
        The URL is by default the one in site_register.py but it can be overriden
        with the "URL" field in the user form (useful for local and test env).
        """
        url = self.site.url if self.site.url else SITE_REGISTER[self.site.username]["site_domain"]
        if url.endswith("/"):
            url = url[:-1]
        if not url.startswith("http"):
            url = f"https://{url}"
        return url

    def __str__(self) -> str:
        return f"Comment {self.pk} - {self.doi}"

    def author_full_name(self) -> str | None:
        """
        Returns the author's actual full name.
        """
        if self.author_first_name and self.author_last_name:
            return f"{self.author_first_name} {self.author_last_name}"
        return None

    def get_moderators(self):
        return self.moderators.values_list("pk", flat=True)

    def get_moderation_data(self) -> Optional["ModerationRights"]:
        """
        Returns the moderation data.
        This corresponds to the published date of the comment for a validated comment.
        """
        return (
            self.moderationrights_set.filter(date_moderated__isnull=False, status=self.status)
            .order_by("date_moderated")
            .first()
        )


class ModerationRights(models.Model):
    """
    ManyToMany model between Moderator & Comment
    """

    moderator = models.ForeignKey(Moderator, on_delete=models.CASCADE)
    comment = models.ForeignKey(Comment, on_delete=models.CASCADE)
    date_created = models.DateTimeField(null=False, blank=False)
    date_moderated = models.DateTimeField(null=True, blank=True)
    status = models.CharField(
        _("Status"), choices=MODERATION_STATUS_CHOICES, blank=False, max_length=127
    )

    class Meta:
        unique_together = ["moderator", "comment"]
