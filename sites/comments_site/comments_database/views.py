from typing import Any

from rest_framework.exceptions import APIException
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK as HTTP_200
from rest_framework.status import HTTP_201_CREATED as HTTP_201
from rest_framework.status import HTTP_401_UNAUTHORIZED as HTTP_401
from rest_framework.views import APIView

from django.db import transaction
from django.db.models import Count
from django.db.models import F
from django.db.models import Q
from django.http import QueryDict
from django.utils import timezone

from comments_api.constants import PARAM_ACTION
from comments_api.constants import PARAM_ACTION_CREATE
from comments_api.constants import PARAM_ACTION_DELETE
from comments_api.constants import PARAM_ADMIN
from comments_api.constants import PARAM_BOOLEAN_VALUE
from comments_api.constants import PARAM_COLLECTION
from comments_api.constants import PARAM_COMMENT
from comments_api.constants import PARAM_DASHBOARD
from comments_api.constants import PARAM_DOI
from comments_api.constants import PARAM_MODERATOR
from comments_api.constants import PARAM_PREVIEW
from comments_api.constants import PARAM_STATUS
from comments_api.constants import PARAM_USER
from comments_api.constants import PARAM_WEBSITE
from comments_api.constants import STATUS_CAN_DELETE
from comments_api.constants import STATUS_CAN_EDIT
from comments_api.constants import STATUS_LIST
from comments_api.constants import STATUS_PARENT_VALIDATED
from comments_api.constants import STATUS_REJECTED
from comments_api.constants import STATUS_SOFT_DELETED
from comments_api.constants import STATUS_SUBMITTED
from comments_api.constants import STATUS_VALIDATED
from comments_api.utils import date_to_str

from .model_helpers import get_all_comments
from .model_helpers import get_comments_for_site
from .models import Comment
from .models import ModerationRights
from .models import Moderator
from .permissions import MathdocSitePermission
from .serializers import CommentSerializer
from .serializers import CommentSerializerCreate
from .serializers import ModerationRightsReadSerializer
from .utils import response_with_error_message


class CommentListView(APIView):
    """
    API view used for:
        -   GET: retrieving the data of the comments matching the given filters
            (query parameters).
        -   POST: creating a new comment.

    All methods expect a bunch of end-user rights in the form of query parameters.
    """

    permission_classes = [MathdocSitePermission]

    def get(self, request: Request) -> Response:
        """
        Returns all the comments related to the website performing the query,
        ordered by `date_submitted`.
        """
        query_filter = Q()

        # Query params result in queryset filtering.
        # Common intersecting filters: DOI and status
        doi = request.query_params.get(PARAM_DOI)
        if doi:
            query_filter = query_filter & Q(doi=doi)

        status_code = request.query_params.get(PARAM_STATUS)
        if status_code:
            status_codes = status_code.split(",")
            if not all([s in STATUS_LIST for s in status_codes]):
                return response_with_error_message("Error: Query parameter 'status' malformed.")
            query_filter = query_filter & Q(status__in=status_codes)

        # There are 2 main modes:
        #   - website (for display on the article page)
        #   - dashboard (for display in comments dashboard)
        try:
            dashboard = request.query_params.get(PARAM_DASHBOARD)
            if dashboard and dashboard != PARAM_BOOLEAN_VALUE:
                raise ValueError
            website = request.query_params.get(PARAM_WEBSITE)
            if website and website != PARAM_BOOLEAN_VALUE:
                raise ValueError
            user = int(request.query_params.get(PARAM_USER, 0))
            moderator = int(request.query_params.get(PARAM_MODERATOR, 0))
            admin = request.query_params.get(PARAM_ADMIN)
            if admin and admin != PARAM_BOOLEAN_VALUE:
                raise ValueError
        except ValueError:
            return response_with_error_message("Error: Query parameter(s) malformed.")

        if dashboard:
            # We filter the comments per site if the user is a comment author.
            # Otherwise it should be a moderator.
            if user:
                comments = get_comments_for_site(request.user)
            else:
                comments = get_all_comments()

            filters_valid, query_filter = update_dashboard_query_filters(
                query_filter, request.query_params, admin, user, moderator
            )

            if not filters_valid:
                return response_with_error_message(
                    f"Error: Either '{PARAM_ADMIN}', '{PARAM_MODERATOR}' or "
                    f"'{PARAM_USER}' is required."
                )

        elif website:
            comments = get_comments_for_site(request.user)
            preview_id = request.query_params.get(PARAM_PREVIEW)
            if preview_id and user:
                query_filter = query_filter | Q(id=preview_id, author_id=user)

        else:
            return response_with_error_message(
                f"Error: Either '{PARAM_DASHBOARD}' or '{PARAM_WEBSITE}' query parameter"
                " is required."
            )

        serializer = CommentSerializer(
            # We need to select DISTINCT row because the moderator condition filtering
            # left joins on the comment moderators (MtoM).
            comments.filter(query_filter).order_by("date_submitted", "id").distinct(),
            many=True,
        )
        return Response(serializer.data, status=HTTP_200)

    def post(self, request: Request) -> Response:
        """
        Creates a new comment.
        """
        # If the comment has a parent, it MUST be validated
        data: dict[str, Any] = dict(request.data)
        parent = data.get("parent")
        if (
            parent
            and not Comment.objects.filter(pk=parent, status__in=STATUS_PARENT_VALIDATED).exists()
        ):
            return response_with_error_message(
                "Error: The comment's parent does not exist or is not validated."
            )

        if not data.get("date_last_modified"):
            data["date_last_modified"] = date_to_str(timezone.now())
        # Correctly populate the foreign keys
        data["site"] = request.user.pk

        comment_serializer = CommentSerializerCreate(data=data)
        if comment_serializer.is_valid():
            comment_serializer.save()
            return Response(comment_serializer.data, status=HTTP_201)

        return response_with_error_message(
            "Error when trying to serialize the comment.", base_data=comment_serializer.errors
        )


def update_dashboard_query_filters(
    query_filter: Q,
    query_params: QueryDict,
    admin: str | None,
    user: int | None,
    moderator: int | None,
) -> tuple[bool, Q]:
    """
    Updates the filtering logic for the comment dataset query when the API request
    is made for the comments dashboard.
    Returns False if the given parameters are not valid (aka. malformed query params).
    """
    # Admin view (Mathdoc staff) - Get all comments
    if admin and admin == PARAM_BOOLEAN_VALUE:
        return True, query_filter
    # Base user dashboard. The user can only access his/her comments.
    elif user:
        query_filter = query_filter & Q(author_id=user)
        return True, query_filter
    # Moderator (Trammel) - Union of:
    #   - all comments of the moderator's allowed collections.
    #   - all comments having a direct relation with this moderator
    #     (see ModeratorRights)
    elif moderator:
        collections = query_params.get(PARAM_COLLECTION)
        if collections:
            collections = [col.lower() for col in collections.split(",")]
            query_filter = query_filter & (
                Q(site__username__in=collections) | Q(moderators__id=moderator)
            )
        else:
            query_filter = query_filter & Q(moderators__id=moderator)
        return True, query_filter
    return False, Q()


class CommentDetailView(APIView):
    """
    API view for retrieving and updating the data of a single comment.

    All methods expect a bunch of end-user rights in the form of query parameters.
    """

    permission_classes = [MathdocSitePermission]

    # Fields available for modification when a comment already exists.
    editable_fields = [
        "status",
        "sanitized_html",
        "raw_html",
        "date_last_modified",
        "article_author_comment",
        "editorial_team_comment",
        "hide_author_name",
        "validation_email_sent",
    ]
    # Mod fields only available for moderators
    moderator_only_fields = [
        "article_author_comment",
        "editorial_team_comment",
        "hide_author_name",
        "validation_email_sent",
    ]
    # Mod fields only available for the comment's author
    author_only_fields = ["sanitized_html", "raw_html"]

    def get_comment(self, request: Request, pk: int) -> Comment:
        """
        Retrieves the existing requested comment according to the query parameters.
        The query parameters indicate the "rights" of the end-user.
        """
        query_filter = Q()

        try:
            user = int(request.query_params.get(PARAM_USER, 0))
            admin = request.query_params.get(PARAM_ADMIN)
            moderator = int(request.query_params.get(PARAM_MODERATOR, 0))
        except ValueError:
            raise APIException("Error: Request malformed")

        # We filter the comments per site if the user is a comment author.
        # Otherwise it should be a moderator.
        if user:
            comment = get_comments_for_site(request.user)
        else:
            comment = get_all_comments()

        filters_valid, query_filter = update_dashboard_query_filters(
            query_filter, request.query_params, admin, user, moderator
        )
        if not filters_valid:
            raise APIException(
                f"Error: {PARAM_ADMIN}, {PARAM_USER} or"
                + f" {PARAM_MODERATOR} filter is required."
            )

        try:
            comment = comment.filter(query_filter).distinct().get(pk=pk)
        except Comment.DoesNotExist:
            raise APIException(
                f"The requested comment (# {pk}) does not exist "
                + "or you don't have the required right to see/edit it."
            )

        return comment

    def get(self, request: Request, pk: int) -> Response:
        try:
            comment = self.get_comment(request, pk)
        except APIException as e:
            return response_with_error_message(e.detail)

        serializer = CommentSerializer(comment)
        return Response(serializer.data)

    @transaction.atomic
    def put(self, request: Request, pk: int) -> Response:
        """
        Updates or deletes an existing comment.

        A comment is actually deleted (removed from DB) if its state allows it.
        Otherwise it gets "soft deleted", ie. we remove its content, its author and
        its status is changed to `deleted`.
        """
        try:
            comment = self.get_comment(request, pk)
        except APIException as e:
            return response_with_error_message(e.detail)

        data = {k: v for k, v in dict(request.data).items() if k in self.editable_fields}
        now = date_to_str(timezone.now())

        moderator = int(request.query_params.get(PARAM_MODERATOR, 0))
        status_code = data.get("status")

        # Check if the update is the moderation of the comment
        if moderator:
            if status_code not in [STATUS_VALIDATED, STATUS_REJECTED]:
                return response_with_error_message(
                    "Error: Incorrect status update for a moderation."
                )
            # For a moderation (change of status), we need to assert the parent comment
            # is in a valid state. This is slightly overkill as a comment should not be
            # created if its parent is not in a valid state.
            if comment.parent is not None and comment.parent.status not in STATUS_PARENT_VALIDATED:
                return response_with_error_message(
                    "Error: The parent comment is in an inconsistent state.", HTTP_401
                )

            data: dict[str, Any] = {
                k: v for k, v in data.items() if k not in self.author_only_fields
            }

            # Forbidden to reject previously validated comment with children
            if (
                status_code != comment.status
                and status_code == STATUS_REJECTED
                and comment.children.exists()
            ):
                return response_with_error_message(
                    "Error: The comment can't be rejected because it has replies.", HTTP_401
                )
            if status_code == STATUS_VALIDATED:
                data["validation_email_sent"] = True
            # Flag the comment as not new anymore when it gets moderated.
            # Otherwise moderators could receive an e-mail about this "pending" comment.
            data["is_new"] = False

        else:
            # Assert the user is the comment's author
            if not int(request.query_params.get(PARAM_USER, 0)) == comment.author_id:
                return response_with_error_message(
                    "Error: You don't have sufficient rights to delete this comment.", HTTP_401
                )
            # Discard fields reserved for moderators
            data = {k: v for k, v in data.items() if k not in self.moderator_only_fields}
            # Check if the update is the deletion of the comment
            if status_code == STATUS_SOFT_DELETED:
                # DELETE_AFTER_MODERATION UPDATE: Deletion is only allowed if the
                # comment has not been moderated yet
                if comment.status not in STATUS_CAN_DELETE:
                    return response_with_error_message(
                        "Error: You can't delete a moderated which has " "already been processed.",
                        HTTP_401,
                    )
                comment.delete()
                return Response({}, status=HTTP_200)

                # DELETE_AFTER_MODERATION UPDATE - Disabled for now
                # # Actually deletes the comment from the databse if it's not
                # # published yet or if it has not children
                # if (
                #     comment.status in STATUS_CAN_DELETE
                #     or not comment.children.exists()
                # ):
                #     comment.delete()
                #     return Response({}, status=HTTP_200)

                # # The comment has already been published - we will just empty its content
                # data = {
                #     "status": STATUS_SOFT_DELETED,
                #     "date_last_modified": now,
                #     "sanitized_html": "<p>This comment has been deleted.</p>",
                #     "raw_html": "<p>This comment has been deleted.</p>",
                #     "author_id": None,
                #     "author_email": None,
                #     "author_first_name": None,
                #     "author_last_name": None,
                #     "author_provider": None,
                #     "author_provider_uid": None,
                # }
            # EDIT case
            else:
                if comment.status not in STATUS_CAN_EDIT:
                    return response_with_error_message(
                        "Error: You can't edit a comment which has " "already been processed.",
                        HTTP_401,
                    )
                elif comment.moderators.exists():
                    return response_with_error_message(
                        "Error: You can't edit a comment with assigned moderators.", HTTP_401
                    )
                data = {k: v for k, v in data.items() if k in self.author_only_fields}
                if "date_last_modified" not in data:
                    data["date_last_modified"] = now

        serializer = CommentSerializerCreate(comment, data=data, partial=True)
        if serializer.is_valid():
            serializer.save()

            # For a moderation update/create the comment moderation right
            if moderator:
                try:
                    moderator_obj = Moderator.objects.get(pk=moderator)
                except Moderator.DoesNotExist:
                    moderator_obj = Moderator(id=moderator)
                    moderator_obj.save()

                try:
                    moderation_right = moderator_obj.moderationrights_set.get(comment__id=pk)
                except ModerationRights.DoesNotExist:
                    moderation_right = ModerationRights(
                        moderator=moderator_obj, comment=comment, date_created=now
                    )

                # Update the moderated date only for status change
                if (
                    moderation_right.status != status_code
                    or moderation_right.date_moderated is None
                ):
                    moderation_right.date_moderated = now
                moderation_right.status = status_code
                moderation_right.save()

            return Response(serializer.data, status=HTTP_200)

        return response_with_error_message(
            f"Error when trying to update the comment # {pk}.", base_data=serializer.errors
        )


class ModeratorRightList(APIView):
    permission_classes = [MathdocSitePermission]

    def get(self, request: Request) -> Response:
        """
        Filters the moderation rights related to comments in the queried collections.
        Returns all the moderation rights of the pending (submitted) comments
        and the used moderation rights of the other (processed) comments.
        Returns:
            [
                {
                    "moderator_id": int,
                    "comment_id": int,
                    "comment__status": str
                }
            ]
        """
        admin = request.query_params.get(PARAM_ADMIN, "")
        collections = request.query_params.get(PARAM_COLLECTION, "")
        if not admin == PARAM_BOOLEAN_VALUE and not collections:
            return response_with_error_message("Error: missing or malformed query parameter.")

        pending_moderation = ModerationRights.objects.filter(comment__status=STATUS_SUBMITTED)
        performed_moderation = ModerationRights.objects.filter(
            ~Q(comment__status=STATUS_SUBMITTED),
            date_moderated__isnull=False,
        )

        if collections:
            collections = [col.lower() for col in collections.split(",")]

            pending_moderation = pending_moderation.filter(comment__site__username__in=collections)
            performed_moderation = ModerationRights.objects.filter(
                comment__site__username__in=collections,
            )
        moderation_rights = (
            pending_moderation.union(performed_moderation)
            .values("moderator_id", "comment_id", "comment__status")
            .order_by("moderator_id", "comment_id")
        )

        serializer = ModerationRightsReadSerializer(moderation_rights, many=True)
        return Response(serializer.data, status=HTTP_200)

    @transaction.atomic
    def post(self, request: Request) -> Response:
        """
        Handles 2 cases:
            - Create a new moderator right. Also create a moderator if it does not exist.
            - Delete a bulk of moderator rights.
        """
        # Check the provided moderator data
        try:
            # PARAM_MODERATOR is either a single int or comma-separated ints
            moderator_id = request.data[PARAM_MODERATOR]
            if not isinstance(moderator_id, int) and "," in moderator_id:
                moderator_id = [int(val) for val in moderator_id.split(",")]
            else:
                moderator_id = int(moderator_id)
            comment_id = int(request.data[PARAM_COMMENT])
            collections = request.query_params[PARAM_COLLECTION]
            action = request.query_params[PARAM_ACTION]
            if action not in [PARAM_ACTION_CREATE, PARAM_ACTION_DELETE]:
                raise ValueError
        except (KeyError, ValueError):
            return response_with_error_message("Error: missing or malformed parameter(s).")

        collections = [col.lower() for col in collections.split(",")]
        try:
            comment = Comment.objects.filter(site__username__in=collections).get(pk=comment_id)
        except Comment.DoesNotExist:
            return response_with_error_message(
                "Error: The provided comment does not exist or you don't have "
                "the rights to manage its moderators."
            )

        # Creation
        if action == PARAM_ACTION_CREATE:
            if not isinstance(moderator_id, int):
                return response_with_error_message("Error: malformed parameter 'moderator_id'.")
            try:
                moderator = Moderator.objects.get(pk=moderator_id)
            except Moderator.DoesNotExist:
                moderator = Moderator(id=moderator_id)
                moderator.save()

            now = timezone.now()
            mod_right = ModerationRights(moderator=moderator, comment=comment, date_created=now)
            mod_right.save()
            return Response(ModerationRightsReadSerializer(mod_right).data, status=HTTP_201)
        # Deletion
        else:
            if not isinstance(moderator_id, list):
                moderator_id = [moderator_id]
            ModerationRights.objects.filter(
                comment=comment, moderator_id__in=moderator_id
            ).delete()
            return Response({}, status=HTTP_200)


class NewCommentsList(APIView):
    permission_classes = [MathdocSitePermission]

    def get_comments(self, request: Request):
        """
        Gets the comments dataset related to the given request from its
        query parameters.
        """
        comments = Comment.objects.filter(is_new=True)
        collections = request.query_params.get(PARAM_COLLECTION)
        if collections:
            collections = [col.lower() for col in collections.split(",")]
            comments = comments.filter(site__username__in=collections)

        return comments

    def get(self, request: Request) -> Response:
        """
        Returns the list of new comments' ID, grouped by site.
        Returns:
            [
                {
                    "pid": str,
                    "comments": list(int)
                }
            ]
        """
        comments = self.get_comments(request)

        # Cool way to do it, but shitty database engines don't use the same function name
        # It's GROUP_CONCAT for MySQL & SQLLite
        # It's ARRAY_AGG/STRING_AGG for PostgreSQL
        # data = comments.annotate(pid=F("site__username")) \
        #                .values("pid") \
        #                .annotate(comments=ArrayAgg("pk"))

        # Need additional post-processing to aggregate
        data = {}
        for comment in comments.annotate(pid=F("site__username")).values("pid", "pk"):
            pid = comment["pid"]
            if pid not in data:
                data[pid] = {"pid": pid, "comments": []}
            data[pid]["comments"].append(comment["pk"])

        return Response(data.values(), status=HTTP_200)

    def post(self, request: Request) -> Response:
        """
        Marks the given comment IDs as not new anymore.
        """
        comments_id = request.data.get(PARAM_COMMENT)
        if not isinstance(comments_id, list):
            return response_with_error_message(f"Error: malformed parameter ({PARAM_COMMENT}).")

        comments = self.get_comments(request)

        comments.filter(pk__in=comments_id).update(is_new=False)

        return Response({}, status=HTTP_200)


class CommentsSummaryView(APIView):
    permission_classes = [MathdocSitePermission]

    def get(self, request: Request) -> Response:
        """
        Returns the summary of number of comments per status, per site (collection).
        Returns:
        {
            "Site 1: {
                "Status 1": nb,
                "Status 2": nb
            },
            "Site 2: {...},
            ...
        }
        """
        comments = Comment.objects.all()
        collections = request.query_params.get(PARAM_COLLECTION)
        if collections:
            collections = [col.lower() for col in collections.split(",")]
            comments = comments.filter(site__username__in=collections)

        comments = (
            comments.annotate(pid=F("site__username"))
            .values("pid", "status")
            .annotate(count=Count("pk"))
        )

        data = {}
        for comment in comments:
            pid = comment["pid"]
            status = comment["status"]
            if pid not in data:
                data[pid] = {}
            data[pid][status] = comment["count"]

        return Response(data, HTTP_200)
