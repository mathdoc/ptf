from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import Group
from django.utils.translation import gettext_lazy as _

from .forms import UserChangeForm
from .forms import UserCreationForm
from .models import Comment
from .models import ModerationRights
from .models import Moderator
from .models import User

# We don't need this, so remove it from the admin dashboard
admin.site.unregister(Group)


@admin.register(User)
class UserAdmin(BaseUserAdmin):
    add_form = UserCreationForm
    form = UserChangeForm
    model = User
    list_display = ("username", "email", "mathdoc_site", "url", "is_staff", "is_superuser")
    list_filter = ("is_staff", "is_superuser", "is_active", "groups", "mathdoc_site")
    fieldsets = (
        (_("Credentials"), {"fields": ("username", "password")}),
        (_("Mathdoc site"), {"fields": ("mathdoc_site", "url")}),
        (
            _("Permissions"),
            {
                "fields": ("is_active", "is_staff", "is_superuser", "groups", "user_permissions"),
            },
        ),
        (_("Important dates"), {"fields": ("last_login", "date_joined")}),
    )
    add_fieldsets = (
        (
            _("Credentials"),
            {
                "classes": ("wide",),
                "fields": ("username", "password1", "password2"),
            },
        ),
        (_("Mathdoc site"), {"fields": ("mathdoc_site", "url")}),
    )
    search_fields = ("username", "url", "email")
    ordering = ("mathdoc_site", "username")


class ModerationRightsAdmin(admin.TabularInline):
    model = ModerationRights
    extra = 0
    verbose_name_plural = "Comments"


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    inlines = (ModerationRightsAdmin,)
    empty_value_display = ""
    list_display = (
        "id",
        "doi",
        "site",
        "author_id",
        "author_email",
        "author_full_name",
        "date_submitted",
        "status",
    )
    list_filter = ("site", "status", "doi", "author_email")
    ordering = ("id",)

    def has_change_permission(self, request, obj=None) -> bool:
        return False


@admin.register(Moderator)
class ModeratorAdmin(admin.ModelAdmin):
    inlines = (ModerationRightsAdmin,)
    list_display = ["__str__"]
