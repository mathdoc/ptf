from django import forms
from django.contrib.auth.forms import UserChangeForm as BaseUserChangeForm
from django.contrib.auth.forms import UserCreationForm as BaseUserCreationForm

from ptf.site_register import SITE_REGISTER

from .models import User


def check_website_data(form: forms.BaseForm, data: dict):
    url = data.get("url")
    mathdoc_site = data.get("mathdoc_site")
    if url and not mathdoc_site:
        form.add_error("mathdoc_site", "Mathdoc site must be checked when filling the URL")
    # If the user to create is flagged as a mathdoc website, its username must be
    # an entry in the site_register
    if mathdoc_site:
        username = data.get("username")
        if username not in SITE_REGISTER:
            form.add_error(
                "username",
                "The username MUST match an entry in site_register.py when mathdoc site is checked",
            )


class UserCreationForm(BaseUserCreationForm):
    class Meta:
        model = User
        fields = ("email", "mathdoc_site", "url")

    def clean(self) -> dict:
        cleaned_data = super().clean()
        check_website_data(self, cleaned_data)
        return cleaned_data


class UserChangeForm(BaseUserChangeForm):
    class Meta:
        model = User
        fields = ("email", "mathdoc_site", "url")

    def clean(self) -> dict:
        cleaned_data = super().clean()
        check_website_data(self, cleaned_data)
        return cleaned_data
