import datetime
from urllib.parse import urlencode

from django.db.utils import IntegrityError
from django.utils import timezone

from comments_api.utils import date_to_str

from ..models import Comment
from ..models import ModerationRights
from ..models import Moderator
from .comment_test_case import CommentTestCase


class ViewTestCase(CommentTestCase):
    @classmethod
    def setUpClass(cls) -> None:
        print("\n**App: 'comments_database' - Testing `views.py`**")
        super().setUpClass()

    def setUp(self) -> None:
        super().setUp()
        self.doi_1 = "10.1111/site.1"
        self.doi_2 = "10.1111/site.2"
        self.doi_3 = "10.1112/site_bis.1"
        self.insert_comment_test_data()

    def tearDown(self) -> None:
        super().tearDown()

    def get_api_url(self, base_url, query_params):
        return f"{base_url}?{urlencode(query_params)}"

    def insert_comment_test_data(self):
        """
        Insert useful test data:
            -   2 moderators
            -   6 comments for DOI 10.1111/site.1
                -   2 submitted comments
                -   3 validated comments
                -   1 rejected comment
                -   1 deleted comment
            -   2 comments for DOI 10.1111/site.2
                -   1 submitted comment
                -   1 validated comment
            -   2 comments for DOI 10.1112/site_bis.1
                -   1 submitted comment
                -   1 validated comment
        """
        ## Moderators
        self.moderator_1 = Moderator.objects.create(id=1)
        self.moderator_2 = Moderator.objects.create(id=2)

        ## DOI 1
        comment_with_answer_1 = self.create_comment(
            {
                "doi": self.doi_1,
                "status": "validated",
                "site": self.site_1,
                "date_submitted": datetime.datetime(2023, 1, 1, tzinfo=datetime.UTC),
                "date_last_modified": datetime.datetime(2023, 1, 1, tzinfo=datetime.UTC),
                "validation_email_sent": True,
                **self.author_1,
            }
        )
        ModerationRights.objects.create(
            comment=comment_with_answer_1,
            moderator=self.moderator_1,
            date_created=datetime.datetime(2023, 1, 1, tzinfo=datetime.UTC),
            date_moderated=datetime.datetime(2023, 1, 2, tzinfo=datetime.UTC),
            status="validated",
        )
        ModerationRights.objects.create(
            comment=comment_with_answer_1,
            moderator=self.moderator_2,
            date_created=datetime.datetime(2023, 1, 1, tzinfo=datetime.UTC),
        )
        comment = self.create_comment(
            {
                "doi": self.doi_1,
                "status": "validated",
                "site": self.site_1,
                "date_submitted": datetime.datetime(2023, 1, 2, tzinfo=datetime.UTC),
                "validation_email_sent": True,
                **self.author_2,
            }
        )
        # Answer to comment 1
        comment_with_answer_2 = self.create_comment(
            {
                "doi": self.doi_1,
                "status": "validated",
                "site": self.site_1,
                "date_submitted": datetime.datetime(2023, 1, 3, tzinfo=datetime.UTC),
                "date_last_modified": datetime.datetime(2023, 1, 3, tzinfo=datetime.UTC),
                "validation_email_sent": True,
                "parent": comment_with_answer_1,
                **self.author_2,
            }
        )
        ModerationRights.objects.create(
            comment=comment_with_answer_2,
            moderator=self.moderator_1,
            date_created=datetime.datetime(2023, 1, 3, tzinfo=datetime.UTC),
            date_moderated=datetime.datetime(2023, 1, 4, tzinfo=datetime.UTC),
            status="validated",
        )

        comment = self.create_comment(
            {
                "doi": self.doi_1,
                "status": "submitted",
                "site": self.site_1,
                "date_submitted": datetime.datetime(2023, 1, 5, tzinfo=datetime.UTC),
                **self.author_1,
            }
        )
        ModerationRights.objects.create(
            comment=comment,
            moderator=self.moderator_1,
            date_created=datetime.datetime(2023, 1, 5, tzinfo=datetime.UTC),
        )

        # Answer to comment 2
        comment = self.create_comment(
            {
                "doi": self.doi_1,
                "status": "submitted",
                "site": self.site_1,
                "parent": comment_with_answer_2,
                "date_submitted": datetime.datetime(2023, 1, 4, tzinfo=datetime.UTC),
                **self.author_2,
            }
        )
        ModerationRights.objects.create(
            comment=comment,
            moderator=self.moderator_1,
            date_created=datetime.datetime(2023, 1, 5, tzinfo=datetime.UTC),
        )

        self.create_comment(
            {
                "doi": self.doi_1,
                "status": "rejected",
                "site": self.site_1,
                "date_submitted": datetime.datetime(2023, 1, 3, 12, tzinfo=datetime.UTC),
                **self.author_1,
            }
        )
        self.create_comment(
            {
                "doi": self.doi_1,
                "status": "deleted",
                "site": self.site_1,
                "date_submitted": datetime.datetime(2023, 1, 2, 12, tzinfo=datetime.UTC),
                "raw_html": "<p>This comment has been deleted.</p>",
                "sanitized_html": "<p>This comment has been deleted.</p>",
            }
        )

        ## DOI 2
        comment_with_answer = self.create_comment(
            {
                "doi": self.doi_2,
                "status": "validated",
                "site": self.site_1,
                "validation_email_sent": True,
                **self.author_1,
            }
        )
        comment = self.create_comment(
            {
                "doi": self.doi_2,
                "status": "submitted",
                "site": self.site_1,
                "parent": comment_with_answer,
                **self.author_2,
            }
        )
        ModerationRights.objects.create(
            comment=comment,
            moderator=self.moderator_1,
            date_created=datetime.datetime(2023, 1, 1, tzinfo=datetime.UTC),
        )
        ModerationRights.objects.create(
            comment=comment,
            moderator=self.moderator_2,
            date_created=datetime.datetime(2023, 1, 1, tzinfo=datetime.UTC),
        )

        ## DOI 3
        self.create_comment(
            {
                "doi": self.doi_3,
                "status": "validated",
                "site": self.site_2,
                "date_submitted": datetime.datetime(2023, 1, 2, tzinfo=datetime.UTC),
                "validation_email_sent": True,
                **self.author_1,
            }
        )
        self.create_comment(
            {
                "doi": self.doi_3,
                "status": "validated",
                "site": self.site_2,
                "date_submitted": datetime.datetime(2021, 1, 1, tzinfo=datetime.UTC),
                "validation_email_sent": True,
                **self.author_2,
            }
        )

    def test_comment_list_endpoint_protected(self):
        test_url = "/api/comments/"
        query_params = {
            "dashboard": "true",
            "moderator_id": 1,
            "collection_id": self.site_1.username,
        }
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 403)

        post_data = {"blabla": "true"}
        response = self.client.post(
            self.get_api_url(test_url, query_params), post_data, format="json"
        )
        self.assertEqual(response.status_code, 403)

    def test_get_article_comments_doi_1(self):
        """Retrieve all displayed comments for article DOI 1."""
        test_url = "/api/comments/"
        query_params = {"website": "true", "doi": self.doi_1, "status": "validated,deleted"}

        self.client.force_authenticate(self.site_1)
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 200)

        data = response.json()
        self.assertEqual(len(data), 4)

        comment = data[0]
        self.assertEqual(comment["status"], "validated")
        self.assertEqual(comment["author_name"], "User 1")
        self.assertEqual(comment["doi"], self.doi_1)
        self.assertEqual(comment["site"], self.site_1.pk)

        comment = data[1]
        self.assertEqual(comment["status"], "validated")
        self.assertEqual(comment["author_name"], "User 2")
        self.assertEqual(comment["doi"], self.doi_1)
        self.assertEqual(comment["site"], self.site_1.pk)

        comment = data[2]
        self.assertEqual(comment["status"], "deleted")
        self.assertIsNone(comment["author_name"])
        self.assertEqual(comment["doi"], self.doi_1)
        self.assertEqual(comment["site"], self.site_1.pk)

        # For this comment, check that all the expected data is present
        comment = data[3]
        expected_data = {
            "id": 3,
            "doi": self.doi_1,
            "sanitized_html": "<p>My wonderful comment</p>",
            "status": "validated",
            "date_submitted": "2023-01-03T00:00:00Z",
            "date_last_modified": "2023-01-03T00:00:00Z",
            "article_author_comment": False,
            "editorial_team_comment": False,
            "hide_author_name": False,
            "author_id": 2,
            "author_email": "user_2@test.test",
            "author_name": "User 2",
            "author_provider": "orcid",
            "author_provider_uid": "0000-0000-0000-0002",
            "site": self.site_1.pk,
            "site_name": self.site_1.username,
            "parent": {
                "id": 1,
                "article_author_comment": False,
                "editorial_team_comment": False,
                "hide_author_name": False,
                "author_id": 1,
                "author_email": "user_1@test.test",
                "author_name": "User 1",
                "author_provider": "orcid",
                "author_provider_uid": "0000-0000-0000-0001",
            },
            "base_url": self.site_1.url,
            "moderation_data": {
                "id": 3,
                "date_created": "2023-01-03T00:00:00Z",
                "date_moderated": "2023-01-04T00:00:00Z",
                "status": "validated",
                "moderator": 1,
                "comment": 3,
            },
            "moderators": [1],
            "is_new": False,
            "validation_email_sent": True,
        }
        self.assertDictEqual(expected_data, comment)

    def test_get_article_comments_doi_2(self):
        """Retrieve all displayed comments for article DOI 2."""
        test_url = "/api/comments/"
        query_params = {"website": "true", "doi": self.doi_2, "status": "validated,deleted"}

        self.client.force_authenticate(self.site_1)
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 200)

        data = response.json()
        self.assertEqual(len(data), 1)

        comment = data[0]
        self.assertEqual(comment["status"], "validated")
        self.assertEqual(comment["author_name"], "User 1")
        self.assertEqual(comment["doi"], self.doi_2)
        self.assertEqual(comment["site"], self.site_1.pk)

    def test_get_article_comments_doi_3(self):
        """Retrieve all displayed comments for article DOI 3."""
        test_url = "/api/comments/"
        query_params = {"website": "true", "doi": self.doi_3, "status": "validated,deleted"}

        self.client.force_authenticate(self.site_2)
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 200)

        data = response.json()
        self.assertEqual(len(data), 2)

        comment = data[0]
        self.assertEqual(comment["status"], "validated")
        self.assertEqual(comment["author_name"], "User 2")
        self.assertEqual(comment["doi"], self.doi_3)
        self.assertEqual(comment["site"], self.site_2.pk)

        comment = data[1]
        self.assertEqual(comment["status"], "validated")
        self.assertEqual(comment["author_name"], "User 1")
        self.assertEqual(comment["doi"], self.doi_3)
        self.assertEqual(comment["site"], self.site_2.pk)

    def test_get_article_comments_doi_not_in_site(self):
        """Retrieve all displayed comments for article DOI 3, using the wrong site."""
        test_url = "/api/comments/"
        query_params = {"website": "true", "doi": self.doi_3, "status": "validated,deleted"}

        self.client.force_authenticate(self.site_1)
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 200)

        data = response.json()
        self.assertEqual(len(data), 0)

    def test_get_author_comments(self):
        """
        Retrieve all posted comments for author 1 (comments dashboard).
        The results should contain only comments for the site performing the API call
        for an author.
        """
        test_url = "/api/comments/"
        query_params = {"dashboard": "true", "user_id": self.author_1["author_id"]}

        self.client.force_authenticate(self.site_1)
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 200)

        data = response.json()
        self.assertEqual(len(data), 4)
        for comment in data:
            self.assertEqual(comment["author_name"], "User 1")

        self.client.force_authenticate(self.site_2)
        response_2 = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response_2.status_code, 200)

        data_2 = response_2.json()
        self.assertEqual(len(data_2), 1)

    def test_get_base_moderator_comments(self):
        """
        Retrieve all comments available to moderator 1 (comments dashboard).
        It should return the same data no matter which site is used for the API call.
        """
        test_url = "/api/comments/"
        query_params = {"dashboard": "true", "moderator_id": self.moderator_1.pk}

        self.client.force_authenticate(self.site_1)
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 200)

        data = response.json()
        self.assertEqual(len(data), 5)
        for comment in data:
            self.assertIn(self.moderator_1.pk, comment["moderators"])

        self.client.force_authenticate(self.site_2)
        response_2 = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response_2.status_code, 200)

        data_2 = response_2.json()
        self.assertEqual(data, data_2)

    def test_get_staff_moderator_comments(self):
        """
        Retrieve all comments available for collection of site 2 (comments dashboard).
        It should return the same data no matter which site is used for the API call.
        """
        test_url = "/api/comments/"
        query_params = {
            "dashboard": "true",
            "moderator_id": -1,
            "collection_id": self.site_2.username,
        }

        self.client.force_authenticate(self.site_1)
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 200)

        data = response.json()
        self.assertEqual(len(data), 2)
        for comment in data:
            self.assertEqual(comment["doi"], self.doi_3)
            self.assertEqual(comment["site"], self.site_2.pk)

        self.client.force_authenticate(self.site_2)
        response_2 = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response_2.status_code, 200)

        data_2 = response_2.json()
        self.assertEqual(data, data_2)

    def test_mix_base_staff_moderator_comments(self):
        """
        Emulate a moderator (moderator_2) with access to the collection of site 2
        and some directly assigned comments.
        It should return the same data no matter which site is used for the API call.
        """
        test_url = "/api/comments/"
        query_params = {
            "dashboard": "true",
            "moderator_id": self.moderator_2.pk,
            "collection_id": self.site_2.username,
        }

        self.client.force_authenticate(self.site_1)
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 200)

        data = response.json()
        self.assertEqual(len(data), 4)
        for comment in data:
            self.assertTrue(
                self.moderator_2.pk in comment["moderators"]
                or comment["site_name"] == self.site_2.username
            )

        self.client.force_authenticate(self.site_2)
        response_2 = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response_2.status_code, 200)

        data_2 = response_2.json()
        self.assertEqual(data, data_2)

    def test_get_admin_moderator_comments(self):
        """
        Retrieve all comments for admin (comments dashboard)X.
        It should return the same data no matter which site is used for the API call.
        """
        test_url = "/api/comments/"
        query_params = {
            "dashboard": "true",
            "moderator_id": -1,
            "admin": "true",
            "collection_id": self.site_2.username,
        }

        self.client.force_authenticate(self.site_1)
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 200)

        data = response.json()
        self.assertEqual(len(data), 11)

        self.client.force_authenticate(self.site_2)
        response_2 = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response_2.status_code, 200)

        data_2 = response_2.json()
        self.assertEqual(data, data_2)

    def test_comment_list_query_parameters(self):
        """
        Test the comment list endpoint returns HTTP 400 errors when the value or the
        combination of the query parameters value is wrong.
        """
        test_url = "/api/comments/"
        self.client.force_authenticate(self.site_1)

        # Error
        query_params = {"something": "true"}
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 400)
        data = response.json()
        self.assertEqual(
            data["error_message"],
            "Error: Either 'dashboard' or 'website' query parameter is required.",
        )

        # Error
        query_params = {"website": "tru"}
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 400)
        data = response.json()
        self.assertEqual(data["error_message"], "Error: Query parameter(s) malformed.")

        # Error
        query_params = {"dashboard": "tru"}
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 400)
        data = response.json()
        self.assertEqual(data["error_message"], "Error: Query parameter(s) malformed.")

        # OK
        query_params = {
            "website": "true",
        }
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 200)

        # Error
        query_params = {"website": "true", "status": "non-existing"}
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 400)
        data = response.json()
        self.assertEqual(data["error_message"], "Error: Query parameter 'status' malformed.")

        # Error
        query_params = {"website": "true", "status": "submitted,non-existing"}
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 400)
        data = response.json()
        self.assertEqual(data["error_message"], "Error: Query parameter 'status' malformed.")

        # OK
        query_params = {"website": "true", "status": "submitted,validated"}
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 200)

        # Error
        query_params = {"dashboard": "true"}
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 400)
        data = response.json()
        self.assertEqual(
            data["error_message"],
            "Error: Either 'admin', 'moderator_id' or 'user_id' is required.",
        )

        # Error
        query_params = {"dashboard": "true", "user_id": "my_string"}
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 400)
        data = response.json()
        self.assertEqual(data["error_message"], "Error: Query parameter(s) malformed.")

        # Error
        query_params = {"dashboard": "true", "moderator_id": "my_string"}
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 400)
        data = response.json()
        self.assertEqual(data["error_message"], "Error: Query parameter(s) malformed.")

    def test_post_new_comment(self):
        """
        Test posting a new comment.
        It does not require any query parameter here.
        """
        test_url = "/api/comments/"
        self.client.force_authenticate(self.site_1)
        comment_data = {
            "doi": self.doi_1,
            "raw_html": "<p>My newly posted comment</p>",
            "sanitized_html": "<p>My newly posted comment</p>",
            "status": "submitted",
            "date_submitted": date_to_str(timezone.now()),
            **self.author_1,
        }

        response = self.client.post(test_url, comment_data, format="json")
        self.assertEqual(response.status_code, 201)
        data = response.json()
        self.assertEqual(data["sanitized_html"], "<p>My newly posted comment</p>")
        self.assertEqual(data["author_name"], "User 1")
        self.assertEqual(data["is_new"], True)

        # Test posting a comment with a parent
        comment_data["parent"] = 1
        response = self.client.post(test_url, comment_data, format="json")
        self.assertEqual(response.status_code, 201)
        data = response.json()
        self.assertEqual(data["sanitized_html"], "<p>My newly posted comment</p>")
        self.assertEqual(data["author_name"], "User 1")
        self.assertEqual(data["parent"], 1)
        self.assertEqual(data["is_new"], True)

        # Test posting a comment with a non-moderated parent
        self.assertIsNotNone(Comment.objects.filter(pk=4, status="submitted").first)
        comment_data["parent"] = 4
        response = self.client.post(test_url, comment_data, format="json")
        self.assertEqual(response.status_code, 400)
        data = response.json()
        self.assertEqual(
            data["error_message"],
            "Error: The comment's parent does not exist or is not validated.",
        )

        # Test posting a comment with a unvalid parent ID
        comment_data["parent"] = -1
        response = self.client.post(test_url, comment_data, format="json")
        self.assertEqual(response.status_code, 400)
        data = response.json()
        self.assertEqual(
            data["error_message"],
            "Error: The comment's parent does not exist or is not validated.",
        )

    def test_comment_detail_protected(self):
        """
        Test that the comment_detail endpoint is authentication protected.
        """
        test_url = "/api/comments/1/"
        query_params = {"user_id": 1}

        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 403)

        response = self.client.put(self.get_api_url(test_url, query_params), {})
        self.assertEqual(response.status_code, 403)

    def test_author_get_comment(self):
        """
        Test a comment author trying to get data of a specific comment.
        Should be restricted to the comments the author wrote.
        The server should return a HTTP 400 response if the comment was not posted
        from the site performing the API call.
        """
        self.client.force_authenticate(self.site_1)
        query_params = {"user_id": 1}
        # Comment written by author ID 1
        test_url = "/api/comments/1/"
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(data["author_id"], 1)

        # Test 400 with other site
        self.client.force_authenticate(self.site_2)
        response_2 = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response_2.status_code, 400)

        # Comment written by author ID 2
        test_url = "/api/comments/2/"
        # Assert the comment exists
        self.assertTrue(Comment.objects.filter(pk=2).exists())
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 400)
        data = response.json()
        self.assertEqual(
            data["error_message"],
            "The requested comment (# 2) does not exist or you don't have "
            "the required right to see/edit it.",
        )

    def test_base_moderator_get_comment(self):
        """
        Test a base moderator trying to get data of a specific comment.
        Should be restricted to the comments the moderator is assigned to.
        It should return the same data no matter which site is used for the API call.
        """
        self.client.force_authenticate(self.site_1)
        query_params = {"moderator_id": 1}

        test_url = "/api/comments/1/"
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertIn(1, data["moderators"])

        # Test OK with other site
        self.client.force_authenticate(self.site_2)
        response_2 = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response_2.status_code, 200)
        data_2 = response_2.json()
        self.assertEqual(data, data_2)

        test_url = "/api/comments/2/"
        self.assertTrue(Comment.objects.filter(pk=2).exists())
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 400)
        data = response.json()
        self.assertEqual(
            data["error_message"],
            "The requested comment (# 2) does not exist or you don't have "
            "the required right to see/edit it.",
        )

    def test_staff_moderator_get_comment(self):
        """
        Test a staff moderator trying to get data of a specific comment.
        Should be restricted to the comments in one of the moderator's collections.
        It should return the same data no matter which site is used for the API call.
        """
        self.client.force_authenticate(self.site_1)
        query_params = {"moderator_id": -1, "collection_id": self.site_1.username}

        test_url = "/api/comments/1/"
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(data["site_name"], self.site_1.username)

        # Test OK with other site
        self.client.force_authenticate(self.site_2)
        response_2 = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response_2.status_code, 200)
        data_2 = response_2.json()
        self.assertEqual(data, data_2)

        comment_id = Comment.objects.filter(site_id=self.site_2.pk).values_list("pk", flat=True)[0]
        test_url = f"/api/comments/{comment_id}/"
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 400)
        data = response.json()
        self.assertEqual(
            data["error_message"],
            f"The requested comment (# {comment_id}) does not exist or you"
            " don't have the required right to see/edit it.",
        )

    def test_mix_base_staff_moderator_get_comment(self):
        """
        Test mix of staff and base moderator trying to get data of a specific comment.
        It should return the same data no matter which site is used for the API call.
        """
        self.client.force_authenticate(self.site_1)
        query_params = {"moderator_id": 1, "collection_id": self.site_2.username}
        # This comment has a moderation right with mod. #1
        test_url = "/api/comments/1/"
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertIn(1, data["moderators"])

        # Test OK with other site
        self.client.force_authenticate(self.site_2)
        response_2 = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response_2.status_code, 200)
        data_2 = response_2.json()
        self.assertEqual(data, data_2)

        # This comment is in the query params collections
        comment_id = Comment.objects.filter(site_id=self.site_2.pk).values_list("pk", flat=True)[0]
        test_url = f"/api/comments/{comment_id}/"
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(data["site_name"], self.site_2.username)

    def test_admin_get_comment(self):
        """
        Test admin trying to acces a specific comment.
        It should return the same data no matter which site is used for the API call.
        """
        self.client.force_authenticate(self.site_1)
        query_params = {
            "admin": "true",
        }
        test_url = "/api/comments/1/"
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 200)
        data = response.json()

        # Test OK with other site
        self.client.force_authenticate(self.site_2)
        response_2 = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response_2.status_code, 200)
        data_2 = response_2.json()
        self.assertEqual(data, data_2)

        comment_id = Comment.objects.filter(site_id=self.site_2.pk).values_list("pk", flat=True)[0]
        test_url = f"/api/comments/{comment_id}/"
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 200)

    def test_author_edit_comment(self):
        """
        Test the author of a comment editing the comment.
        The server should return a HTTP 400 response if the comment was not posted
        from the site performing the API call.
        """
        self.client.force_authenticate(self.site_1)
        query_params = {"user_id": 1}

        comment = Comment.objects.filter(
            status="submitted", author_id=1, moderators__id__isnull=False, site_id=self.site_1.pk
        ).first()
        test_url = f"/api/comments/{comment.pk}/"
        comment_data = {
            "id": comment.pk,
            "raw_html": "<p>My EDITED comment</p>",
            "sanitized_html": "<p>My EDITED comment</p>",
            "status": "submitted",
            **self.author_1,
        }
        # Try editing a comment with assigned moderators - Forbidden
        response = self.client.put(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 401)
        data = response.json()
        self.assertEqual(
            data["error_message"], "Error: You can't edit a comment with assigned moderators."
        )

        # Try a regular submitted comment without assigned moderators
        comment.moderators.all().delete()
        test_url = f"/api/comments/{comment.pk}/"
        comment_data["pk"] = comment.pk
        response = self.client.put(
            self.get_api_url(test_url, query_params), comment_data, format="json"
        )
        self.assertEqual(response.status_code, 200)
        comment = Comment.objects.get(pk=comment.pk)
        self.assertEqual(comment.sanitized_html, "<p>My EDITED comment</p>")

        # Delete the submitted comment
        comment_data_delete = {"status": "deleted"}
        response = self.client.put(
            self.get_api_url(test_url, query_params), comment_data_delete, format="json"
        )
        self.assertEqual(response.status_code, 200)
        self.assertRaises(Comment.DoesNotExist, Comment.objects.get, pk=comment.pk)

        # Try from another site
        self.client.force_authenticate(self.site_2)
        response = self.client.put(
            self.get_api_url(test_url, query_params), comment_data, format="json"
        )
        self.assertEqual(response.status_code, 400)

        # Try updating comment already processed
        self.client.force_authenticate(self.site_1)
        comment = Comment.objects.filter(status="validated", author_id=1).first()
        test_url = f"/api/comments/{comment.pk}/"
        comment_data["id"] = comment.pk
        comment_data["status"] = "validated"
        response = self.client.put(
            self.get_api_url(test_url, query_params), comment_data, format="json"
        )
        self.assertEqual(response.status_code, 401)
        data = response.json()
        self.assertEqual(
            data["error_message"],
            "Error: You can't edit a comment which has already been processed.",
        )

        # Try deleting an already processed comment
        response = self.client.put(
            self.get_api_url(test_url, query_params), comment_data_delete, format="json"
        )
        self.assertEqual(response.status_code, 401)
        data = response.json()
        self.assertEqual(
            data["error_message"],
            "Error: You can't delete a moderated which has already been processed.",
        )

        # DELETE_AFTER_MODERATION UPDATE: Not possible anymore
        # # Delete the already processed comment with children
        # response = self.client.put(self.get_api_url(test_url, query_params),
        #                            comment_data_delete, format="json")
        # self.assertEqual(response.status_code, 200)
        # comment = Comment.objects.get(pk=comment.pk)
        # self.assertEqual(comment.status, "deleted")
        # self.assertEqual(comment.author_id, None)
        # self.assertEqual(comment.author_email, None)
        # self.assertEqual(comment.author_first_name, None)
        # self.assertEqual(comment.author_last_name, None)
        # self.assertEqual(comment.author_provider, None)
        # self.assertEqual(comment.author_provider_uid, None)
        # self.assertEqual(comment.raw_html, "<p>This comment has been deleted.</p>")
        # self.assertEqual(comment.sanitized_html,
        #                  "<p>This comment has been deleted.</p>")

        # Try to edit/delete the comment of another author
        # Edit
        comment = Comment.objects.filter(
            status="submitted", author_id=2, site_id=self.site_1.pk
        ).first()
        comment.moderators.all().delete()
        test_url = f"/api/comments/{comment.pk}/"
        response = self.client.put(
            self.get_api_url(test_url, query_params), comment_data, format="json"
        )
        self.assertEqual(response.status_code, 400)
        data = response.json()
        self.assertEqual(
            data["error_message"],
            f"The requested comment (# {comment.pk}) does not exist "
            + "or you don't have the required right to see/edit it.",
        )

        # Delete
        response = self.client.put(
            self.get_api_url(test_url, query_params), comment_data_delete, format="json"
        )
        self.assertEqual(response.status_code, 400)
        data = response.json()
        self.assertEqual(
            data["error_message"],
            f"The requested comment (# {comment.pk}) does not exist "
            + "or you don't have the required right to see/edit it.",
        )

    def test_moderator_moderate_comment(self):
        self.client.force_authenticate(self.site_2)
        query_params = {"moderator_id": 1}

        comment = Comment.objects.filter(status="submitted", moderators__id=1).first()
        self.assertTrue(comment.is_new)
        comment_data = {"status": "submitted"}
        test_url = f"/api/comments/{comment.pk}/"

        # Incorrect status
        response = self.client.put(
            self.get_api_url(test_url, query_params), comment_data, format="json"
        )
        self.assertEqual(response.status_code, 400)
        data = response.json()
        self.assertEqual(data["error_message"], "Error: Incorrect status update for a moderation.")

        # Correct status
        self.assertFalse(comment.validation_email_sent)
        comment_data["status"] = "validated"
        response = self.client.put(
            self.get_api_url(test_url, query_params), comment_data, format="json"
        )
        self.assertEqual(response.status_code, 200)
        comment = Comment.objects.get(pk=comment.pk)
        self.assertEqual(comment.status, "validated")
        self.assertFalse(comment.is_new)

        date_moderated = comment.get_moderation_data().date_moderated
        self.assertTrue(date_moderated > timezone.now() - datetime.timedelta(seconds=5))
        self.assertTrue(date_moderated < timezone.now())
        self.assertTrue(comment.validation_email_sent)

    def test_moderator_re_moderate_comment(self):
        """
        A moderator can moderate an already moderated comment.
        """
        self.client.force_authenticate(self.site_1)
        query_params = {"moderator_id": 1}

        comment = Comment.objects.filter(status="submitted", moderators__id=1).first()
        comment_data = {"status": "validated"}
        test_url = f"/api/comments/{comment.pk}/"
        response = self.client.put(
            self.get_api_url(test_url, query_params), comment_data, format="json"
        )
        self.assertEqual(response.status_code, 200)
        comment = Comment.objects.get(pk=comment.pk)
        self.assertEqual(comment.status, "validated")
        self.assertTrue(comment.validation_email_sent)
        self.assertFalse(comment.article_author_comment)
        self.assertFalse(comment.is_new)

        # Re-moderate to update the article_author_name boolean
        comment_data = {"status": "validated", "article_author_comment": True}
        response = self.client.put(
            self.get_api_url(test_url, query_params), comment_data, format="json"
        )
        self.assertEqual(response.status_code, 200)
        comment = Comment.objects.get(pk=comment.pk)
        self.assertEqual(comment.status, "validated")
        self.assertTrue(comment.article_author_comment)
        self.assertFalse(comment.is_new)

        # Re-moderate to reject the comment
        comment_data = {"status": "rejected"}
        response = self.client.put(
            self.get_api_url(test_url, query_params), comment_data, format="json"
        )
        self.assertEqual(response.status_code, 200)
        comment = Comment.objects.get(pk=comment.pk)
        self.assertEqual(comment.status, "rejected")

    def test_staff_moderator_moderate_comment(self):
        """
        A staff moderator moderating a comment he/she has not been explicitely assigned
        to. It should create the Moderator and the related ModerationRights.
        """
        self.client.force_authenticate(self.site_2)
        query_params = {"moderator_id": 10, "collection_id": self.site_1.username}

        self.assertRaises(Moderator.DoesNotExist, Moderator.objects.get, pk=10)
        comment = Comment.objects.filter(status="submitted", site_id=self.site_1.pk).first()
        self.assertTrue(comment.is_new)
        test_url = f"/api/comments/{comment.pk}/"
        comment_data = {"status": "rejected"}
        response = self.client.put(
            self.get_api_url(test_url, query_params), comment_data, format="json"
        )
        self.assertEqual(response.status_code, 200)
        moderator = Moderator.objects.get(pk=10)
        self.assertTrue(
            ModerationRights.objects.filter(
                comment=comment, moderator=moderator, date_moderated__isnull=False
            ).exists()
        )
        comment = Comment.objects.get(pk=comment.pk)
        self.assertFalse(comment.is_new)

    def test_moderator_edit_comment(self):
        """
        A moderator trying to edit the content of a comment. Forbidden.
        """
        self.client.force_authenticate(self.site_1)
        query_params = {"moderator_id": 1}
        comment = Comment.objects.filter(status="submitted", moderators__id=1).first()

        # Try to edit the comment data - Forbidden because wrong status update for
        # a moderation
        comment_data = {
            "id": comment.pk,
            "raw_html": "<p>My EDITED comment</p>",
            "sanitized_html": "<p>My EDITED comment</p>",
            "status": "submitted",
            **self.author_1,
        }
        test_url = f"/api/comments/{comment.pk}/"
        response = self.client.put(
            self.get_api_url(test_url, query_params), comment_data, format="json"
        )
        self.assertEqual(response.status_code, 400)
        data = response.json()
        self.assertEqual(data["error_message"], "Error: Incorrect status update for a moderation.")

        # Try and validate it along - Should only validate the comment and not modify
        # the content
        comment_data["status"] = "validated"
        comment_init_content = comment.sanitized_html
        self.assertEqual(comment_init_content, "<p>My wonderful comment</p>")

        response = self.client.put(
            self.get_api_url(test_url, query_params), comment_data, format="json"
        )
        self.assertEqual(response.status_code, 200)

        comment = Comment.objects.get(pk=comment.pk)
        self.assertEqual(comment_init_content, comment.sanitized_html)
        self.assertEqual(comment.status, "validated")

        # Try to delete the comment - Forbidden
        comment_data_delete = {"status": "deleted"}
        response = self.client.put(
            self.get_api_url(test_url, query_params), comment_data_delete, format="json"
        )
        self.assertEqual(response.status_code, 400)
        data = response.json()
        self.assertEqual(data["error_message"], "Error: Incorrect status update for a moderation.")

    def test_moderator_rights_list_protected(self):
        """
        Test that the moderator rights endpoint is authentication protected.
        """
        test_url = "/api/moderators/"
        query_params = {"collection_id": self.site_1.username}

        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 403)

        post_data = {"moderator_id": 1}
        response = self.client.post(
            self.get_api_url(test_url, query_params), post_data, format="json"
        )
        self.assertEqual(response.status_code, 403)

    def test_get_moderator_list(self):
        """
        Test a staff moderator accessing the list of its assigned moderators.
        It should return the same data no matter which site is used for the API call.
        """
        self.client.force_authenticate(self.site_1)
        test_url = "/api/moderators/"

        # Missing parameters
        response = self.client.get(test_url)
        self.assertEqual(response.status_code, 400)
        data = response.json()
        self.assertEqual(data["error_message"], "Error: missing or malformed query parameter.")

        # Valid query - No moderation rights for site_2
        query_params = {"collection_id": self.site_2.username}
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(data, [])

        # Moderation rights of interest for site_1 & site_2
        all_mod_rights = ModerationRights.objects.all()
        self.assertEqual(len(all_mod_rights), 7)
        query_params = {"collection_id": f"{self.site_1.username},{self.site_2.username}"}
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 200)

        comment_ids = Comment.objects.filter(
            site_id__in=[self.site_1.pk, self.site_2.pk]
        ).values_list("pk", flat=True)
        data = response.json()
        self.assertEqual(len(data), 7)
        for comment in data:
            self.assertIn(comment["comment_id"], comment_ids)

        expected = {"moderator_id": 1, "comment_id": 1, "comment__status": "validated"}
        self.assertEqual(expected, data[0])

        # Test OK with other site
        self.client.force_authenticate(self.site_2)
        response_2 = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response_2.status_code, 200)
        data_2 = response_2.json()
        self.assertEqual(data, data_2)

    def test_post_moderator_right(self):
        """
        Test a staff moderator updating moderation rights (creation & deletion).
        It should return the same data no matter which site is used for the API call.
        """
        test_url = "/api/moderators/"
        self.client.force_authenticate(self.site_1)

        # Delete existing moderation right
        self.assertTrue(ModerationRights.objects.filter(moderator_id=1, comment_id=1).exists())
        query_params = {"collection_id": self.site_1.username, "action": "delete"}

        post_data = {"moderator_id": 1, "comment_id": 1}
        response = self.client.post(
            self.get_api_url(test_url, query_params), post_data, format="json"
        )
        self.assertEqual(response.status_code, 200)
        self.assertFalse(ModerationRights.objects.filter(moderator_id=1, comment_id=1).exists())

        # Delete again
        post_data["moderator_id"] = "1,2"
        response = self.client.post(
            self.get_api_url(test_url, query_params), post_data, format="json"
        )
        self.assertEqual(response.status_code, 200)
        self.assertFalse(
            ModerationRights.objects.filter(moderator_id__in=[1, 2], comment_id=1).exists()
        )

        # Create 1 mod right - Test with other site
        self.client.force_authenticate(self.site_2)
        query_params["action"] = "create"
        post_data["moderator_id"] = 1
        response = self.client.post(
            self.get_api_url(test_url, query_params), post_data, format="json"
        )
        self.assertEqual(response.status_code, 201)
        self.assertTrue(ModerationRights.objects.filter(moderator_id=1, comment_id=1).exists())
        self.assertFalse(ModerationRights.objects.filter(moderator_id=2, comment_id=1).exists())

        # Create 1 mod right with new moderator
        self.assertFalse(Moderator.objects.filter(id=10).exists())
        post_data["moderator_id"] = 10
        response = self.client.post(
            self.get_api_url(test_url, query_params), post_data, format="json"
        )
        self.assertEqual(response.status_code, 201)
        self.assertTrue(Moderator.objects.filter(id=10).exists())
        self.assertTrue(ModerationRights.objects.filter(moderator_id=10, comment_id=1).exists())

    def test_post_moderator_right_wrong_param(self):
        """
        Test the moderator right endpoint with wrong combination of query params and
        POST data.
        """
        test_url = "/api/moderators/"
        self.client.force_authenticate(self.site_1)

        query_params = {"collection_id": self.site_2.username, "action": "create"}
        post_data = {"moderator_id": 10, "comment_id": 1}
        response = self.client.post(
            self.get_api_url(test_url, query_params), post_data, format="json"
        )
        self.assertEqual(response.status_code, 400)
        data = response.json()
        self.assertEqual(
            data["error_message"],
            "Error: The provided comment does not exist or you don't have "
            "the rights to manage its moderators.",
        )

        query_params["collection_id"] = f"{self.site_1.username},{self.site_2.username}"
        query_params["action"] = "wrong"
        response = self.client.post(
            self.get_api_url(test_url, query_params), post_data, format="json"
        )
        self.assertEqual(response.status_code, 400)
        data = response.json()
        self.assertEqual(data["error_message"], "Error: missing or malformed parameter(s).")

        post_data["comment_id"] = "not_parsable"
        query_params["action"] = "create"
        response = self.client.post(
            self.get_api_url(test_url, query_params), post_data, format="json"
        )
        self.assertEqual(response.status_code, 400)
        data = response.json()
        self.assertEqual(data["error_message"], "Error: missing or malformed parameter(s).")

        post_data["moderator_id"] = "not_parsable"
        post_data["comment_id"] = 1
        query_params["action"] = "create"
        response = self.client.post(
            self.get_api_url(test_url, query_params), post_data, format="json"
        )
        self.assertEqual(response.status_code, 400)
        data = response.json()
        self.assertEqual(data["error_message"], "Error: missing or malformed parameter(s).")

        # moderator_id must be an int for 'create' action
        post_data["moderator_id"] = "1,2"
        response = self.client.post(
            self.get_api_url(test_url, query_params), post_data, format="json"
        )
        self.assertEqual(response.status_code, 400)
        data = response.json()
        self.assertEqual(data["error_message"], "Error: malformed parameter 'moderator_id'.")

        # Test creating exist mod right
        post_data["moderator_id"] = 1
        self.assertRaises(
            IntegrityError,
            self.client.post,
            self.get_api_url(test_url, query_params),
            post_data,
            format="json",
        )

    def test_new_comment_list_protected(self):
        """
        Test that the new comments endpoint is authentication protected.
        """
        test_url = "/api/comments-new/"

        response = self.client.get(test_url)
        self.assertEqual(response.status_code, 403)

        response = self.client.post(test_url, {}, format="json")
        self.assertEqual(response.status_code, 403)

    def test_new_comment_list(self):
        """
        Test getting the newly posted comments.
        It should return the same data no matter which site is used for the API call.
        """
        self.client.force_authenticate(self.site_1)
        test_url = "/api/comments-new/"

        Comment.objects.all().update(is_new=False)
        comment_1 = self.create_comment(
            {
                "doi": self.doi_1,
                "status": "submitted",
                "site": self.site_1,
                "date_submitted": datetime.datetime(2023, 2, 1, tzinfo=datetime.UTC),
                **self.author_1,
            }
        )
        comment_2 = self.create_comment(
            {
                "doi": self.doi_1,
                "status": "submitted",
                "site": self.site_1,
                "date_submitted": datetime.datetime(2023, 2, 1, tzinfo=datetime.UTC),
                **self.author_2,
            }
        )
        comment_3 = self.create_comment(
            {
                "doi": self.doi_3,
                "status": "submitted",
                "site": self.site_2,
                "date_submitted": datetime.datetime(2023, 2, 1, tzinfo=datetime.UTC),
                **self.author_2,
            }
        )

        # No query params = new comments for all collections
        response = self.client.get(test_url)
        self.assertEqual(response.status_code, 200)
        expected = [
            {"pid": self.site_1.username, "comments": [comment_1.pk, comment_2.pk]},
            {"pid": self.site_2.username, "comments": [comment_3.pk]},
        ]
        data = response.json()
        self.assertEqual(expected, data)

        # Filter on site_1
        query_params = {"collection_id": self.site_1.username}
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 200)
        expected = [{"pid": self.site_1.username, "comments": [comment_1.pk, comment_2.pk]}]
        data = response.json()
        self.assertEqual(expected, data)

        # Test OK with other site
        self.client.force_authenticate(self.site_2)
        response_2 = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response_2.status_code, 200)
        data_2 = response_2.json()
        self.assertEqual(data, data_2)

    def test_post_not_new_comment(self):
        """
        Test marking new comments as not new.
        It should return the same data no matter which site is used for the API call.
        """
        self.client.force_authenticate(self.site_2)
        test_url = "/api/comments-new/"

        Comment.objects.all().update(is_new=False)
        comment_1 = self.create_comment(
            {
                "doi": self.doi_1,
                "status": "submitted",
                "site": self.site_1,
                "date_submitted": datetime.datetime(2023, 2, 1, tzinfo=datetime.UTC),
                **self.author_1,
            }
        )
        comment_2 = self.create_comment(
            {
                "doi": self.doi_1,
                "status": "submitted",
                "site": self.site_1,
                "date_submitted": datetime.datetime(2023, 2, 1, tzinfo=datetime.UTC),
                **self.author_2,
            }
        )
        comment_3 = self.create_comment(
            {
                "doi": self.doi_3,
                "status": "submitted",
                "site": self.site_2,
                "date_submitted": datetime.datetime(2023, 2, 1, tzinfo=datetime.UTC),
                **self.author_2,
            }
        )
        self.assertTrue(comment_1.is_new)
        self.assertTrue(comment_2.is_new)
        self.assertTrue(comment_3.is_new)

        query_params = {"collection_id": self.site_1.username}
        post_data = {"comment_id": [comment_1.pk]}

        response = self.client.post(
            self.get_api_url(test_url, query_params), post_data, format="json"
        )
        self.assertEqual(response.status_code, 200)

        comment_1 = Comment.objects.get(pk=comment_1.pk)
        comment_2 = Comment.objects.get(pk=comment_2.pk)
        comment_3 = Comment.objects.get(pk=comment_3.pk)
        self.assertFalse(comment_1.is_new)
        self.assertTrue(comment_2.is_new)
        self.assertTrue(comment_3.is_new)

        # Test from other site
        self.client.force_authenticate(self.site_1)
        query_params = {"collection_id": f"{self.site_1.username},{self.site_2.username}"}
        post_data = {"comment_id": [comment_1.pk, comment_2.pk, comment_3.pk]}

        response = self.client.post(
            self.get_api_url(test_url, query_params), post_data, format="json"
        )
        self.assertEqual(response.status_code, 200)

        comment_1 = Comment.objects.get(pk=comment_1.pk)
        comment_2 = Comment.objects.get(pk=comment_2.pk)
        comment_3 = Comment.objects.get(pk=comment_3.pk)
        self.assertFalse(comment_1.is_new)
        self.assertFalse(comment_2.is_new)
        self.assertFalse(comment_3.is_new)

    def test_comments_summary_protected(self):
        """
        Test that the comments summary endpoint is authentication protected.
        """
        test_url = "/api/comments-summary/"

        response = self.client.get(test_url)
        self.assertEqual(response.status_code, 403)

    def test_comments_summary(self):
        """
        Test getting the summary of all comments.
        It should return the same data no matter which site is used for the API call.
        """
        self.client.force_authenticate(self.site_1)
        test_url = "/api/comments-summary/"
        query_params = {"moderator_id": 1}

        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 200)

        data = response.json()
        expected_data = {
            self.site_1.username: {"submitted": 3, "validated": 4, "rejected": 1, "deleted": 1},
            self.site_2.username: {"validated": 2},
        }
        self.assertDictEqual(data, expected_data)

        query_params["collection_id"] = self.site_2.username
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 200)

        data = response.json()
        expected_data = {self.site_2.username: {"validated": 2}}
        self.assertDictEqual(data, expected_data)

        # Test from other site
        self.client.force_authenticate(self.site_2)
        response = self.client.get(self.get_api_url(test_url, query_params))
        self.assertEqual(response.status_code, 200)

        data = response.json()
        self.assertDictEqual(data, expected_data)
