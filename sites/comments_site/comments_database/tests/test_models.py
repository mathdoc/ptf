from django.db import IntegrityError
from django.db import transaction
from django.utils import timezone

from ..models import ModerationRights
from ..models import Moderator
from ..models import User
from .comment_test_case import CommentTestCase


class ModelTestCase(CommentTestCase):
    @classmethod
    def setUpClass(cls) -> None:
        print("\n**App: 'comments_database' - Testing `models.py`**")
        super().setUpClass()

    def setUp(self) -> None:
        super().setUp()

    def tearDown(self) -> None:
        super().tearDown()

    def test_save_user(self):
        User.objects.all().delete()
        user = User.objects.create_user("not_registered", "not_registered@test.test", "***")
        user.mathdoc_site = True
        self.assertRaises(AssertionError, user.save)

        user = User.objects.create_user("crbiol", "", "***")
        user.mathdoc_site = True
        user.save()

        self.assertTrue(user.mathdoc_site)

    def test_comment_base_url(self):
        comment = self.create_comment({"site": self.site_1, **self.author_1})
        self.assertEqual("http://crbiol.test", comment.get_base_url())

        # Site with no URL
        self.site_1.url = None
        self.site_1.save()

        self.assertEqual(
            "https://comptes-rendus.academie-sciences.fr/biologies", comment.get_base_url()
        )

        self.assertEqual("User 1", comment.author_full_name())

    def test_moderation_rights(self):
        comment = self.create_comment({"status": "validated", **self.author_1})

        moderator_1 = Moderator.objects.create(id=1)
        moderator_2 = Moderator.objects.create(id=2)
        mod_right_1 = ModerationRights.objects.create(
            moderator=moderator_1, comment=comment, date_created=timezone.now()
        )
        mod_right_2 = ModerationRights.objects.create(
            moderator=moderator_2,
            comment=comment,
            date_created=timezone.now(),
            date_moderated=timezone.now(),
            status="validated",
        )

        moderators = list(comment.get_moderators())
        self.assertEqual(moderators, [1, 2])

        moderation_data = comment.get_moderation_data()
        self.assertEqual(moderation_data.pk, mod_right_2.pk)

        # New moderation, rejected
        mod_right_1.status = "rejected"
        mod_right_1.date_moderated = timezone.now()
        mod_right_1.save()

        comment.status = "rejected"
        comment.save()
        moderation_data = comment.get_moderation_data()
        self.assertEqual(moderation_data.pk, mod_right_1.pk)

        # Emulate the comment having been rejected before by mod_right_2
        # This is the moderation data that must be returned.
        mod_right_2.status = "rejected"
        mod_right_2.save()

        moderation_data = comment.get_moderation_data()
        self.assertEqual(moderation_data.pk, mod_right_2.pk)

        # Test unique constraint
        with transaction.atomic():
            self.assertRaises(
                IntegrityError,
                ModerationRights.objects.create,
                moderator=moderator_1,
                comment=comment,
                date_created=timezone.now(),
            )
