import base64

from ..models import User
from .comment_test_case import CommentTestCase


class HttpAuthenticationTestCase(CommentTestCase):
    def test_request_with_http_auth(self):
        """
        Test hitting a protected endpoint requiring MathdocSitePermission
        """
        test_url = "/api/comments/?website=true&doi=doi"

        # Test non-authenticated client
        response = self.client.get(test_url)
        self.assertEqual(response.status_code, 403)
        data = response.json()
        self.assertEqual(data, {"detail": "Authentication credentials were not provided."})

        # Test client with correct credentials
        credentials = base64.b64encode(b"crbiol:***").decode("utf-8")
        auth_header = {"HTTP_AUTHORIZATION": f"Basic {credentials}"}
        response = self.client.get(test_url, **auth_header)
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(data, [])

        # Test client with bad credentials
        credentials = base64.b64encode(b"crbiol:wrong").decode("utf-8")
        auth_header = {"HTTP_AUTHORIZATION": f"Basic {credentials}"}
        response = self.client.get(test_url, **auth_header)
        self.assertEqual(response.status_code, 403)
        data = response.json()
        self.assertEqual(data, {"detail": "Invalid username/password."})

    def test_user_permission(self):
        """
        Test hitting a protected endpoint with a non-authorized user.
        """
        test_url = "/api/comments/?website=true&doi=doi"
        User.objects.create_user("no_site", "no_site@test.test", "***")
        credentials = base64.b64encode(b"no_site:***").decode("utf-8")
        auth_header = {"HTTP_AUTHORIZATION": f"Basic {credentials}"}
        response = self.client.get(test_url, **auth_header)
        self.assertEqual(response.status_code, 403)
        data = response.json()
        self.assertEqual(data, {"detail": "You do not have permission to perform this action."})
