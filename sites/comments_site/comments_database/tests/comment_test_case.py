from rest_framework.test import APITestCase

from django.utils import timezone

from ..models import Comment
from ..models import Moderator
from ..models import User


class CommentTestCase(APITestCase):
    def setUp(self) -> None:
        site_1 = User.objects.create_user("crbiol", "crbiol@test.test", "***")
        site_1.mathdoc_site = True
        site_1.url = "http://crbiol.test"
        site_1.save()
        self.site_1 = site_1

        site_2 = User.objects.create_user("crmath", "crmath@test.test", "***")
        site_2.mathdoc_site = True
        site_2.url = "http://crmath.test"
        site_2.save()
        self.site_2 = site_2

        self.author_1 = {
            "author_id": 1,
            "author_email": "user_1@test.test",
            "author_first_name": "User",
            "author_last_name": "1",
            "author_provider": "orcid",
            "author_provider_uid": "0000-0000-0000-0001",
        }
        self.author_2 = {
            "author_id": 2,
            "author_email": "user_2@test.test",
            "author_first_name": "User",
            "author_last_name": "2",
            "author_provider": "orcid",
            "author_provider_uid": "0000-0000-0000-0002",
        }

        super().setUp()

    def tearDown(self) -> None:
        self.site = None
        self.delete_all_comments()
        Moderator.objects.all().delete()
        User.objects.all().delete()

        super().tearDown()

    def delete_comment(self, comment: Comment):
        """Recursive deletion of a comment and all its children"""
        children = comment.children.all()
        for child in children:
            self.delete_comment(child)
        comment.delete()

    def delete_all_comments(self):
        top_level_comments = Comment.objects.filter(parent_id__isnull=True)
        for comment in top_level_comments:
            self.delete_comment(comment)

    def create_comment(self, update_data={}):
        """
        Creates a Comment object and return it.
        All default values can be overriden via `update_data`.
        """
        comment_data = {
            "site": self.site_1,
            "doi": "10.1111/site.1",
            "date_submitted": timezone.now(),
            "date_last_modified": timezone.now(),
            "raw_html": "<p>My wonderful comment</p>",
            "sanitized_html": "<p>My wonderful comment</p>",
            "status": "submitted",
        }

        if update_data:
            comment_data.update(update_data)

        if "is_new" not in comment_data:
            comment_data["is_new"] = comment_data["status"] == "submitted"

        return Comment.objects.create(**comment_data)
