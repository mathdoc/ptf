from django.apps import AppConfig


class CommentsBackConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "comments_database"
