from rest_framework import serializers

from comments_api.constants import COMMENT_STATUS_CHOICES

from .models import Comment
from .models import ModerationRights


class ModerationRightsSerializer(serializers.ModelSerializer):
    class Meta:
        model = ModerationRights
        fields = "__all__"


class ModerationRightsReadSerializer(serializers.ModelSerializer):
    comment__status = serializers.ChoiceField(COMMENT_STATUS_CHOICES, read_only=True)

    class Meta:
        model = ModerationRights
        fields = ["moderator_id", "comment_id", "comment__status"]


class ParentCommentSerializer(serializers.ModelSerializer):
    """
    Serializer for a comment's parent data.

    We only extract part of the data directly attached to the comment model, no
    relations.
    This is used to format the comment's author name
    """

    author_name = serializers.CharField(source="author_full_name", read_only=True)

    class Meta:
        model = Comment
        fields = [
            "id",
            "article_author_comment",
            "editorial_team_comment",
            "hide_author_name",
            "author_id",
            "author_email",
            "author_name",
            "author_provider",
            "author_provider_uid",
        ]


class CommentSerializerCreate(serializers.ModelSerializer):
    """
    Same as the base CommentSerializer without `raw_html` field and a non-modified
    `parent` field
    """

    author_name = serializers.CharField(source="author_full_name", read_only=True)
    site_name = serializers.CharField(source="site.username", read_only=True)
    base_url = serializers.URLField(source="get_base_url", read_only=True)
    moderation_data = ModerationRightsSerializer(source="get_moderation_data", read_only=True)
    moderators = serializers.ListField(source="get_moderators", read_only=True)

    class Meta:
        model = Comment
        fields = [
            "id",
            "doi",
            "sanitized_html",
            "raw_html",
            "status",
            "date_submitted",
            "date_last_modified",
            "article_author_comment",
            "editorial_team_comment",
            "hide_author_name",
            "author_id",
            "author_email",
            "author_name",
            "author_first_name",
            "author_last_name",
            "author_provider",
            "author_provider_uid",
            "is_new",
            "validation_email_sent",
            "site",
            "site_name",
            "parent",
            "base_url",
            "moderation_data",
            "moderators",
        ]


class CommentSerializer(CommentSerializerCreate):
    parent = ParentCommentSerializer(read_only=True)

    class Meta:
        model = Comment
        fields = [
            "id",
            "doi",
            "sanitized_html",
            "status",
            "date_submitted",
            "date_last_modified",
            "article_author_comment",
            "editorial_team_comment",
            "hide_author_name",
            "author_id",
            "author_email",
            "author_name",
            "author_provider",
            "author_provider_uid",
            "is_new",
            "validation_email_sent",
            "site",
            "site_name",
            "parent",
            "base_url",
            "moderation_data",
            "moderators",
        ]
