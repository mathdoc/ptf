from django.urls import path

from .views import CommentDetailView
from .views import CommentListView
from .views import CommentsSummaryView
from .views import ModeratorRightList
from .views import NewCommentsList

urlpatterns = [
    path("api/comments/", CommentListView.as_view(), name="comment_list"),
    path(
        "api/comments/<int:pk>/",
        CommentDetailView.as_view(),
        name="comment_detail"
    ),
    path(
        "api/moderators/",
        ModeratorRightList.as_view(),
        name="moderator_list"
    ),
    path(
        "api/comments-new/",
        NewCommentsList.as_view(),
        name="comments_new_list"
    ),
    path(
        "api/comments-summary/",
        CommentsSummaryView.as_view(),
        name="comments_summary"
    )
]
