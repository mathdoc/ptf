from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST

from comments_api.constants import API_MESSAGE_KEY


def response_with_error_message(
    error_message: str, response_http_status: int = HTTP_400_BAD_REQUEST, base_data: dict = {}
) -> Response:
    """Returns a DRF (error) response with an added message."""
    if error_message:
        base_data[API_MESSAGE_KEY] = error_message
    return Response(base_data, status=response_http_status)
