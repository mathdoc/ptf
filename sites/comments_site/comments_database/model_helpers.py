from .models import Comment
from .models import User


def comments_default_queryset():
    comments = Comment.objects.prefetch_related(
        "site", "parent", "moderationrights_set", "moderators"
    )
    return comments
    #### Here is a try to annotate directly in the comment queryset the data of the
    #### ModerationRights related to the comment having date_moderated NOT NULL.
    #### This is super easy in raw SQL:
    #### ```
    #### SELECT comment.*, r.*
    #### FROM comment
    #### LEFT JOIN moderation_rights r ON r.comment_id = comment.id AND r.date_moderated IS NOT NULL
    #### ```
    #### However this is quite troublesome with Django.
    #### This first try is successfully annotating the moderation data as a JSON field using a subquery
    #### The drawback is that it's way less effecive than the desired raw SQL writed above.
    # return comments.annotate(moderation=Subquery(
    #     ModerationRights.objects.filter(comment=OuterRef("pk"), date_moderated__isnull=False).values(data=JSONObject(moderator_id="moderator_id", date_added="date_added", date_moderation="date_moderation"))[:1]
    # ))
    #### This one correctly selects everyfield, however they are only accessible if we use values() queryset method (stupid ?).
    # return comments.annotate(
    #         moderation=FilteredRelation('moderationrights', condition=Q(moderationrights__date_moderated__isnull=False))
    #     ).select_related('moderation')


def get_all_comments():
    return comments_default_queryset().all()


def get_comments_for_site(site: User):
    return comments_default_queryset().filter(site=site)
