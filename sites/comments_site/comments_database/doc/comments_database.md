# Comments database application

This application implements the database models for the comments and the API system serving it using Django Rest Framework.

## I. Requirements

The application requires the `comments_api` application to run, ontaining constants and utilities to be shared among the `comments_database` application and the other applications using the API (namely [`comments_views`](/apps/comments_views/doc/comments_views.md) and [`comments_moderation`](/sites/ptf_tools/comments_moderation/doc/comments_moderation.md)). \
It also needs to stay in the __ptf__ repository becauses it uses the `site_register.py` file.

## I. Performing API request

### I.1 Register a mathdoc site user

Comments API is restricted to authenticated mathdoc site users. \
You can register a new mathdoc site user using the Django admin panel. When registering a mathdoc site to use the API, you must:
  - Use an entry of the [`SITE_REGISTER`](/apps/ptf/site_register.py) as an username.
  - Enter a strong password for authentication and save it.
  - Check the Mathdoc site boolean
  - [Optional]: Enter an URL corresponding to the site. If nothing is provided, the code will use the site domain corresponding to the username in `SITE_REGISTER`. This should only be used for local development or for testing purposes.

See below an example of local mathodc site users:

![Comments database local users](./comments_database_users.png)


### I.2 Regular HTTP requests with authentication

A mathdoc site can then simply interacts with the API using HTTP requests with a basic authentication header including the above credentials (`username:password`). \
Example using request:

```python
import requests

response = requests.get("https://comments_database_url/api/comments/", auth=("username", "password"))
data = response.json()
```

## II. [Structure](../models.py)

There are very few models used for storing the comments data. See the following UML diagram. \
The `Comment` object contains all the data and metadata of a comment, except for moderation related data.
Note that the actual comment is stored "twice":
  - `raw_html`: the raw value of the posted comment. The API should never return this field. \
    We keep this data to be able to rollback or update the comment in case the sanitization process goes wrong or has security updates.
  -  `sanitized_html`: the sanitized version of the above field.

The `ModerationRights` store the rights of moderators (Trammel users) over specific comments. \
The moderation data (`moderation_data` in the API endpoint) corresponds to the first moderation made with the current
status of the comment.
A comment can be moderated multiple times, either to update the metadata (eg. `article_author_comment`) or to change the comment status  (Validated <-> Rejected).
Due to this behavior, we use as the processed & published date the first moderation made with the current status of the comment (validated or rejected).


![Comments database UML diagram](./comments_database_uml.png)


