import pytest

from django.conf import settings

from ptf.cmds.ptf_cmds import addArticlePtfCmd
from ptf.cmds.ptf_cmds import addContainerPtfCmd
from ptf.cmds.xml_cmds import addArticleXmlCmd
from ptf.model_helpers import get_article
from ptf.model_helpers import get_container
from ptf.models import Collection


@pytest.fixture(scope="session")
def django_db_setup():
    settings.DATABASES["default"] = {"ENGINE": "django.db.backends.sqlite3"}
    settings.RESOURCES_ROOT = "tests/data"
    settings.SOLR_URL = "http://127.0.0.1:8983/solr/core0-test"
    settings.MAX_RESULT_SIZE = 7
    settings.REPOSITORIES = {"http://127.0.0.1/oai/": "NumdamRepository"}


@pytest.mark.django_db
def test_import_single():
    f = open("test/jcrma100001.xml")
    body = f.read()
    f.close()

    journal = Collection.objects.get(pid="CRMATH")
    issue = get_container(pid="CRMATH_2020__357_1")

    if issue:
        cmd = addContainerPtfCmd({"pid": issue.pid, "ctype": issue.ctype})
        cmd.set_provider(provider=issue.provider)
        cmd.add_collection(journal)
        cmd.undo()

    article = get_article(pid="10.5802/crmath.2020-00071")
    if article:
        cmd = addArticlePtfCmd({"pid": article.pid, "doi": article.doi})
        cmd.add_collection(journal)
        cmd.set_provider(provider=article.provider)
        issue = get_container(pid="CRMATH_2019")
        if issue:
            cmd.set_container(issue)
        cmd.undo()

    cmd = addArticleXmlCmd({"body": body})
    cmd.set_collection(journal)
    cmd.set_provider(journal.provider)
    article = cmd.do()
