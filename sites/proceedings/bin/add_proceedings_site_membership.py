"""
Change the site linked to the seminars and books of the Centre Mersenne.
"""
import os
import sys

import django

# Setup django with proceedings settings
path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
if path not in sys.path:
    sys.path.append(path)
os.environ["DJANGO_SETTINGS_MODULE"] = "proceedings.settings"

#### ONLY FOR NOTEBOOK TO RUN ####
# os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"
##################################

django.setup()

from datetime import datetime

# Actual script
from django.conf import settings
from django.db.utils import IntegrityError
from django.utils import timezone

from ptf.models import Article
from ptf.models import Collection
from ptf.models import Container
from ptf.models import PtfSite
from ptf.models import Resource
from ptf.models import SiteMembership

collection_pids = ["JEDP", "MALSM", "SLSEDP", "TSG", "WBLN"]

print(
    f"This will add a Site membership to 'proceedings.centre-mersenne.org' for the resources linked to the collections:\n\t{collection_pids}\n"
)
input("Press any key to continue\n")

proceedings_site = PtfSite.objects.get(id=settings.SITE_ID)


def add_site_membership_to_resource(
    site: PtfSite, resource: Resource, date: datetime, verbose=False
):
    try:
        site_membership = SiteMembership(resource=resource, deployed=date, online=date, site=site)
        site_membership.save()
    except IntegrityError as e:
        if verbose:
            print(f"\nIntegrity error for resource: {resource}")
            print(e)
            input("Press any key to continue\n")


# Move the collections' site to proceedings
print("Adding proceedings site to the corresponding collections...")
collections = Collection.objects.filter(pid__in=collection_pids)
date_now = timezone.now()


for collection in collections:
    add_site_membership_to_resource(proceedings_site, collection, date_now)


# Move the containers' site to proceedings
print("Adding proceedings site to the corresponding containers...")
containers = Container.objects.filter(my_collection__in=collections)

for container in containers:
    add_site_membership_to_resource(proceedings_site, container, date_now)


# Move the articles' site to proceedings
print("Adding proceedings site to the corresponding articles...")
articles = Article.objects.filter(my_container__in=containers)

for article in articles:
    add_site_membership_to_resource(proceedings_site, article, date_now)


print("DONE !")
