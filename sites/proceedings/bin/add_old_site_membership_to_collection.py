"""
Change the site linked to the seminars and books of the Centre Mersenne.
"""
import os
import sys

import django

# Setup django with proceedings settings
path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
if path not in sys.path:
    sys.path.append(path)
os.environ["DJANGO_SETTINGS_MODULE"] = "proceedings.settings"

django.setup()

from datetime import datetime

# Actual script
from django.db.utils import IntegrityError
from django.utils import timezone

from ptf.models import Article
from ptf.models import Collection
from ptf.models import Container
from ptf.models import PtfSite
from ptf.models import Resource
from ptf.models import SiteMembership
from ptf.site_register import SITE_REGISTER

collection_pids = ["JEDP", "MALSM", "SLSEDP", "TSG", "WBLN"]

old_sites_id = [SITE_REGISTER[name.lower()]["site_id"] for name in collection_pids + ["mbk"]]

print(
    f"This add a site membership to the original individual site for the collections (seminars):\n\t{collection_pids}\n"
)
input("Press any key to continue\n")


def add_site_membership_to_resource(
    site: PtfSite, resource: Resource, date: datetime, verbose=False
):
    try:
        site_membership = SiteMembership(resource=resource, deployed=date, online=date, site=site)
        site_membership.save()
    except IntegrityError as e:
        if verbose:
            print(f"\nIntegrity error for resource: {resource}")
            print(e)
            input("Press any key to continue\n")


collections = Collection.objects.filter(pid__in=collection_pids)
date_now = timezone.now()

for collection in collections:
    print(f"Adding old site membership to collection {collection.pid}")

    site_name = "MBK" if collection.pid == "MALSM" else collection.pid
    site_id = SITE_REGISTER[site_name.lower()]["site_id"]
    site = PtfSite.objects.get(id=site_id)
    add_site_membership_to_resource(site, collection, date_now)

    containers = Container.objects.filter(my_collection=collection)
    for container in containers:
        add_site_membership_to_resource(site, container, date_now)

        articles = Article.objects.filter(my_container=container)
        for article in articles:
            add_site_membership_to_resource(site, article, date_now)
