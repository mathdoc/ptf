from django.shortcuts import render
from django.views.decorators.http import require_http_methods

from ptf import model_helpers


@require_http_methods(["GET"])
def actas_and_lectures(request, api=False):
    actas = model_helpers.get_actas().filter(parent=None)
    lectures = model_helpers.get_lectures().filter(parent=None)
    journals = actas | lectures
    context = {"journals": journals, "coltype": "acta"}

    return render(request, "blocks/journal-list.html", context)
