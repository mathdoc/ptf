random = Math.floor(Math.random() * 22);
signatures = [
    "Ampere",
    "Appell",
    "Bertrand",
    "Bonnet",
    "Cauchy",
    "Chasles",
    "Duhamel",
    "Fourier",
    "Goursat",
    "Hermite",
    "Jordan",
    "Lagrange",
    "Laplace",
    "Levy",
    "Liouville",
    "Mandelbrojt",
    "Monge",
    "Painleve",
    "Picard",
    "Poincare",
    "Poisson",
    "Serret",
];
$(document).ready(function () {
    to_display = signatures[random];
    $(".signature").append(
        "<img src='/static/jep/img/signatures/" +
            to_display +
            ".png' class='img-responsive center-block' alt='signature " +
            to_display +
            "'>"
    );
});
