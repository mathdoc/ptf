$(document).ready(function () {
    lightbox(".lightbox", {
        close: true,
        closeText: "x",
        keyboard: true,
        zoom: true,
        zoomText: "<span class='glyphicon glyphicon-zoom-in'><span>",
        docClose: true,
    });
});
