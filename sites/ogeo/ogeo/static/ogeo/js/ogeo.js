$(document).ready(function () {
    $(".block-with-ellipsis").dotdotdot({
        ellipsis: "...",
        height: 80,
    });
});
