from django.views.generic import TemplateView

from ptf import model_helpers


class ArticlesView(TemplateView):
    template_name = "blocks/all-articles.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        articles = model_helpers.get_articles_by_published_date()
        for article in articles:
            article.title_html = article.title_html.replace("\u00A0", " ")
        context.update({"articles": articles})
        return context
