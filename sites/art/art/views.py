from mersenne_cms.views import HomeView


class ARTHomeView(HomeView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["display_new_articles"] = True

        return context
