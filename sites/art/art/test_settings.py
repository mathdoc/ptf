from .settings import *

DATABASES = {}
DATABASES["default"] = {"ENGINE": "django.db.backends.sqlite3"}
RESOURCES_ROOT = "tests/data"
SOLR_URL = "http://127.0.0.1:8983/solr/core0-test"
MAX_RESULT_SIZE = 7
REPOSITORIES = {"http://127.0.0.1/oai/": "AlcoRepository"}
SECRET_KEY = "dsfqsdlkfjlkqjsdhokqdfhgkdqfhgildfhgidfhgiqdfhghdfghsfiughel"
TEMP_FOLDER = "/tmp"
MERSENNE_TMP_FOLDER = "/tmp"
MERSENNE_LOG_FILE = MERSENNE_TMP_FOLDER + "/transformations.log"
LOG_DIR = "/tmp"
