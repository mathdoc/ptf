from cr_app.views import CRHomeView as AbstractCRHomeView

from ptf.site_register import SITE_REGISTER


class CRMathHomeView(AbstractCRHomeView):
    _sites = [SITE_REGISTER["crmath"]["site_id"]]

    def _get_class(self):
        return CRMathHomeView
