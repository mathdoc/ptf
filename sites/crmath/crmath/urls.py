from django.conf import settings
from django.conf.urls.static import static
from django.urls import include
from django.urls import path
from django.urls import re_path

from comments_views.journal import urls as comments_urls
from crmath.views import CRMathHomeView
from mersenne_cms import urls as cms_urls
from mersenne_cms.admin import mersenne_admin
from oai import urls as oai_urls
from ptf import urls as ptf_urls
from ptf.urls import ptf_i18n_patterns
from upload import urls as upload_urls

urlpatterns = [
    path('', CRMathHomeView.as_view(), name='home'),
    path('', include(ptf_urls)),
    path('', include(cms_urls)),
    path('', include(oai_urls)),
    path('', include(comments_urls)),
]

urlpatterns += ptf_i18n_patterns

if settings.ALLOW_ADMIN:
    urlpatterns += [
        path('upload/', include(upload_urls)),
        re_path(r'^admin/', mersenne_admin.urls),
        path("accounts/", include("django.contrib.auth.urls")),
    ]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

