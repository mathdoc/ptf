from cr_app.views import CRHomeView as AbstractCRHomeView

from django.views.generic.base import TemplateView

from ptf.site_register import SITE_REGISTER


class HistoryGraphView(TemplateView):
    template_name = "graph.html"


class CRHomeView(AbstractCRHomeView):
    _sites = [
        SITE_REGISTER["crmath"]["site_id"],
        SITE_REGISTER["crchim"]["site_id"],
        SITE_REGISTER["crphys"]["site_id"],
        SITE_REGISTER["crbiol"]["site_id"],
        SITE_REGISTER["crmeca"]["site_id"],
        SITE_REGISTER["crgeos"]["site_id"],
    ]

    def _get_class(self):
        return CRHomeView
