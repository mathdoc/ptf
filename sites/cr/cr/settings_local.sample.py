# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.10/howto/deployment/checklist/
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

ALLOWED_HOSTS = ["*"]

# SECURITY WARNING: keep the serevueset key used in production serevueset!
SECRET_KEY = "change_me"

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": "revues",  # Or path to database file if using sqlite3.
        "USER": "revues_user",  # Not used with sqlite3.
        "PASSWORD": "revues_password",  # Not used with sqlite3.
        "HOST": "localhost",  # Set to empty string for localhost. Not used with sqlite3.
        "PORT": "",  # Set to empty string for default. Not used with sqlite3.
    }
}

SOLR_URL = "http://127.0.0.1:8983/solr/revues/"

RESOURCES_ROOT = "/mersenne_test_data"

LOG_DIR = "/tmp/"

ALLOW_ADMIN = True

SENDFILE_BACKEND = "django_sendfile.backends.development"
SENDFILE_ROOT = RESOURCES_ROOT
