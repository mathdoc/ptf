
function updateGraphData(data, g) {
    var nodes = data.nodes;
    var links = data.links;
    var canvas = document.createElement("canvas");
    var ctx = canvas.getContext('2d');

    // Add nodes
    nodes.forEach(function(node) {
        var label = '<div class="graph-text"><div class="row"><div class="col-xs-12 graph-content';
        if (node.active && nodes.length > 1 ) {
            label += ' active';
        }
        label += '" data-bs-toggle="tooltip" data-placement="bottom" title="' + node.name + '">' + node.name + '</div></div>';

        label += '<div class="graph-description row">';
        label += '<div class="col-12">' + node.description + '</div>';

        var dict = {
          labelType: "html",
          label: label,
          class: "comp",
          rx: 5,
          ry: 5,
        };

        if (node.active && nodes.length > 1) {
            dict['class'] = 'active';
        }

        g.setNode(node.id, dict);
    });

    // Add links
    links.forEach(function(link) {
      var linkclass = "normal";
      if (link.source == 'comp_d') {
          linkclass = "warning";
      }
      g.setEdge(link.source, link.target,
        {
          arrowhead: "vee",
          class: linkclass,
          lineCurve: d3.curveBasis
        });
    });
}

function updateGraphDimensions(g) {
    var render = new dagreD3.render();
    var svg = d3.select("svg");
    var container = svg.select("g");
    render(container, g);

    // Adjust SVG height and width to content
    var svgParent = $('#history_graph > svg');
    var main = svgParent.find('g > g');
    var h = main.get(0).getBoundingClientRect().height;
    var w = main.get(0).getBoundingClientRect().width;
    var newHeight = h + 40;
    var newWidth = w + 100;
    newHeight = newHeight < 80 ? 80 : newHeight;
    newWidth = newWidth < 200 ? 200 : newWidth;
    svgParent.height(newHeight);
    svgParent.width(newWidth);
}

function createGraph(data) {
    // Preparation of DagreD3 data structures
    var g = new dagreD3.graphlib.Graph().setGraph({
        nodesep: 30,
        ranksep: 50,
        rankdir: "LR",
        marginx: 10,
        marginy: 10
      });
    updateGraphData(data, g);
    updateGraphDimensions(g);
}

$(document).ready(function(){

    let data = {
        "nodes": [
            {
                "id": 1,
                "name": "Comptes rendus hebdomadaires des séances de l'Académie des sciences",
                "description": "1835-1965<br><br>ISSN: 0001-4036<br>(eISSN: 2419-6304)",
                "active": 1,
            },
            {
                "id": 2,
                "name": "Procès-verbaux des séances de l'Académie",
                "description": "1910-1922<br><br>ISSN: 2416-9943<br>(eISSN: 2453-2681)",
                "active": 0,
            },
            {
                "id": 3,
                "name": "Comptes rendus hebdomadaires des séances de l'Académie des sciences. Vie académique",
                "description": "1966-1979<br><br>ISSN: 0151-0525<br>(eISSN: 2419-6908)",
                "active": 0,
            },
            {
                "id": 4,
                "name": "Comptes rendus des séances de l'Académie des sciences. Vie académique",
                "description": "1979-1983<br><br>ISSN: 0249-6321<br>(eISSN: 2419-7610)",
                "active": 0,
            },
            {
                "id": 5,
                "name": "La Vie des sciences",
                "description": "1984-1996<br><br>ISSN: 0762-0969<br>(eISSN: 2419-8730)",
                "active": 0,
            },
            {
                "id": 6,
                "name": "Les Nouvelles de l'Académie",
                "description": "1984-1993<br><br>ISSN: 0246-1226",
                "active": 0,
            },
            {
                "id": 7,
                "name": "La Lettre de l'Académie des sciences et du CADAS",
                "description": "1994-2000<br><br>ISSN: 1250-5331",
                "active": 0,
            },
            {
                "id": 8,
                "name": "La Lettre de l'Académie des sciences",
                "description": "2001-<br><br>ISSN: 1631-0462<br>eISSN: 2102-5398",
                "active": 0,
            },
            {
                "id": 9,
                "name": "Comptes rendus hebdomadaires des séances de l'Académie des sciences. Séries A et B, Sciences mathématiques et sciences physiques",
                "description": "1966-1973<br><br>ISSN: 0997-4482<br>(eISSN: 2420-028X)",
                "active": 0,
            },
            {
                "id": 10,
                "name": "Comptes rendus hebdomadaires des séances de l'Académie des sciences. Série A, Sciences mathématiques",
                "description": "1974<br><br>ISSN: 0302-8429<br>(eISSN: 2419-7750)",
                "active": 0,
            },
            {
                "id": 11,
                "name": "Comptes rendus hebdomadaires des séances de l'Académie des sciences. Séries A et B. Série A, Sciences mathématiques",
                "description": "1975-1980<br><br>ISSN: 0151-0509",
                "active": 0,
            },
            {
                "id": 12,
                "name": "Comptes rendus des séances de l'Académie des sciences. Série 1, Mathématique",
                "description": "1981-1996<br><br>ISSN: 0249-6291<br>(eISSN: 2419-7580)",
                "active": 0,
            },
            {
                "id": 13,
                "name": "Comptes rendus des séances de l'Académie des sciences. Série 1, Mathématique",
                "description": "1997-2001<br><br>ISSN: 0764-4442<br>eISSN: 1778-3577",
                "active": 0,
            },
            {
                "id": 14,
                "name": "Comptes rendus. Mathématique",
                "description": "2002-<br><br>ISSN: 1631-073X<br>eISSN: 1778-3569",
                "active": 1,
            },
            {
                "id": 15,
                "name": "Comptes rendus hebdomadaires des séances de l'Académie des sciences. Série B, Sciences physiques",
                "description": "1974<br><br>ISSN: 0302-8437<br>(eISSN: 2419-7769)",
                "active": 0,
            },
            {
                "id": 16,
                "name": "Comptes rendus hebdomadaires des séances de l'Académie des sciences. Séries A et B. Série B, Sciences physiques",
                "description": "1975-1980<br><br>ISSN: 0335-5993",
                "active": 0,
            },
            {
                "id": 17,
                "name": "Comptes rendus des séances de l'Académie des sciences. Série 2, Mécanique, Physique, Chimie, Sciences de l'Univers, Sciences de la Terre",
                "description": "1981-1989<br><br>ISSN: 0249-6305<br>(eISSN: 2419-7599)",
                "active": 0,
            },
            {
                "id": 18,
                "name": "Comptes rendus des séances de l'Académie des sciences. Série 2, Mécanique, physique, chimie, sciences de l'univers, sciences de la terre",
                "description": "1990-1994<br><br>ISSN: 0764-4450<br>(eISSN: 2419-8773)",
                "active": 0,
            },
            {
                "id": 19,
                "name": "Comptes rendus des séances de l'Académie des sciences. Série IIa, Sciences de la terre et des planètes",
                "description": "1995-2001<br><br>ISSN: 1251-8050<br>eISSN: 1778-4107",
                "active": 0,
            },
            {
                "id": 20,
                "name": "Comptes rendus. Géoscience",
                "description": "2002-<br><br>ISSN: 1631-0713<br>eISSN: 1778-7025",
                "active": 1,
            },
            {
                "id": 21,
                "name": "Comptes rendus. Palévol",
                "description": "2002-<br><br>ISSN: 1631-0683<br>eISSN: 1777-571X",
                "active": 1,
            },
            {
                "id": 22,
                "name": "Comptes rendus des séances de l'Académie des sciences. Série IIb, Mécanique, physique, chimie, astronomie",
                "description": "1995-1998<br><br>ISSN: 1251-8069<br>eISSN: 1873-7250",
                "active": 0,
            },
            {
                "id": 23,
                "name": "Comptes rendus des séances de l'Académie des sciences. Série IIb, Mécanique, physique, astronomie",
                "description": "1998-2000<br><br>ISSN: 1287-4620<br>eISSN: 1873-7404",
                "active": 0,
            },
            {
                "id": 24,
                "name": "Comptes rendus des séances de l'Académie des sciences. Série IIb, Mécanique",
                "description": "2000-2001<br><br>ISSN: 1620-7742<br>eISSN: 1873-751X",
                "active": 0,
            },
            {
                "id": 25,
                "name": "Comptes rendus. Mécanique",
                "description": "2002-<br><br>ISSN: 1631-0721<br>eISSN: 1873-7234",
                "active": 1,
            },
            {
                "id": 26,
                "name": "Comptes rendus des séances de l'Académie des sciences. Série IV, Physique, astrophysique",
                "description": "2000-2001<br><br>ISSN: 1296-2147<br>eISSN: 1878-5557",
                "active": 0,
            },
            {
                "id": 27,
                "name": "Comptes rendus. Physique",
                "description": "2002-<br><br>ISSN: 1631-0705<br>eISSN: 1878-1535",
                "active": 1,
            },
            {
                "id": 28,
                "name": "Comptes rendus des séances de l'Académie des sciences. Série IIc, Chimie",
                "description": "1998-2001<br><br>ISSN: 1387-1609<br>eISSN: 1878-5859",
                "active": 0,
            },
            {
                "id": 29,
                "name": "Comptes rendus. Chimie",
                "description": "2002-<br><br>ISSN: 1631-0748<br>eISSN: 1878-1543",
                "active": 1,
            },
            {
                "id": 30,
                "name": "Comptes rendus hebdomadaires des séances de l'Académie des sciences. Série C, Sciences chimiques",
                "description": "1966-1980<br><br>ISSN: 0567-6541<br>(eISSN: 2419-8382)",
                "active": 0,
            },
            {
                "id": 31,
                "name": "Comptes rendus hebdomadaires des séances de l'Académie des sciences. Série D, Sciences naturelles",
                "description": "1966-1980<br><br>ISSN: 0567-655X<br>(eISSN: 2419-8390)",
                "active": 0,
            },
            {
                "id": 32,
                "name": "Comptes rendus des séances de l'Académie des sciences. Série 3, Sciences de la vie",
                "description": "1981-1989<br><br>ISSN: 0249-6313<br>(eISSN: 2419-7602)",
                "active": 0,
            },
            {
                "id": 33,
                "name": "Comptes rendus des séances de l'Académie des sciences. Série 3, Sciences de la vie",
                "description": "1990-2001<br><br>ISSN: 0764-4469<br>eISSN: 1878-5565",
                "active": 0,
            },
            {
                "id": 34,
                "name": "Comptes rendus. Biologies",
                "description": "2002-<br><br>ISSN: 1631-0691<br>eISSN: 1768-3238",
                "active": 1,
            },

        ],
         "links": [
            {
                "source": 1,
                "target": 2,
            },
            {
                "source": 1,
                "target": 3,
            },
            {
                "source": 1,
                "target": 9,
            },
            {
                "source": 1,
                "target": 30,
            },
            {
                "source": 1,
                "target": 31,
            },
            {
                "source": 2,
                "target": 3,
            },
            {
                "source": 3,
                "target": 4,
            },
            {
                "source": 4,
                "target": 5,
            },
            {
                "source": 4,
                "target": 6,
            },
//            {
//                "source": 5,
//                "target": 6,
//            },
            {
                "source": 6,
                "target": 7,
            },
            {
                "source": 7,
                "target": 8,
            },
            {
                "source": 9,
                "target": 10,
            },
            {
                "source": 9,
                "target": 15,
            },
            {
                "source": 10,
                "target": 11,
            },
            {
                "source": 11,
                "target": 12,
            },
            {
                "source": 12,
                "target": 13,
            },
            {
                "source": 13,
                "target": 14,
            },
            {
                "source": 15,
                "target": 16,
            },
            {
                "source": 16,
                "target": 17,
            },
            {
                "source": 17,
                "target": 18,
            },
            {
                "source": 18,
                "target": 19,
            },
            {
                "source": 18,
                "target": 22,
            },
            {
                "source": 19,
                "target": 20,
            },
            {
                "source": 19,
                "target": 21,
            },
            {
                "source": 22,
                "target": 23,
            },
            {
                "source": 22,
                "target": 28,
            },
            {
                "source": 23,
                "target": 24,
            },
            {
                "source": 23,
                "target": 26,
            },
            {
                "source": 24,
                "target": 25,
            },
            {
                "source": 26,
                "target": 27,
            },
            {
                "source": 28,
                "target": 29,
            },
            {
                "source": 32,
                "target": 33,
            },
            {
                "source": 33,
                "target": 34,
            },
            {
                "source": 30,
                "target": 17,
            },
            {
                "source": 31,
                "target": 32,
            },
        ],
    };

    createGraph(data);

});
