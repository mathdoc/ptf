## application Geodesic

## Démarrage
#### Installation
> pip3 install -r requirements.txt
>
> python3 manage.py migrate
>
> python3 manage.py runserver_plus
> > http://localhost:8002

### Groupes et permissions

> Django-sync-groups

[Django-groups-sync](https://pypi.org/project/django-groups-sync/)

    Affecter aux utilisateurs les groupes donnant permissions pour la gestion des ressources et
    des opérations pour gérer les données de la bibliothèque numérique.

    Le fichier pour affecter les groupes et permissions est : groups_permission.json

   ``` bash
   python3 manage.py remove_stale_contenttypes (to remove old content types)
   python3 manage.py sync_groups_permissions
   ```

- export d'un nouveau fichier de groupes
   ``` bash
   python3 manage.py export_groups_permissions
   ```


Pour attacher les groupes aux utilisateurs, depuis l'interface d'administration

    groups:
        can_manage_resources (level to grant staff resources)
        can_manage_import (level to manage collections)



### Gestion des piles d'événements
> Rabbitmq


    sudo apt-get install rabbitmq-server

Configuration de rabbitmq (https://docs.celeryproject.org/en/stable/getting-started/brokers/rabbitmq.html)

    sudo rabbitmqctl add_user guest guest_pwd
    sudo rabbitmqctl add_vhost ptf_host
    sudo rabbitmqctl set_permissions -p ptf_host guest ".*" ".*" ".*"


- accès à la gestion des messages rabbit avec le GUI:
- http://localhost:15672

#### vérification de la liste d'évènements
 > sudo rabbitmqctl list_queues

###  Celery

[Installation](../ptf_tools/README.rst)

#### Suivi du broker et des messages
> celery -A gdml flower --adress='localhost'

Démarrer un worker pour gérer la file d'attente et les tâches:

##### celery -A gdml worker -B -l INFO --concurrency=1

### Redis

[implémentation de redis](Redis.md)



utilisé tant que système de Log et de cache


## Modules et applications

     gdml: front-office et accès publique

     composant collection vuejs: /apps/vart/src/components/collection.vue

     backend: application back-office

     api : gestion des ressources

     frontend: thème frontend


### Logs
 configurable dans settings.py
 ````
 'applogfile': {
            'level': 'DEBUG',
            'class':'logging.handlers.FileHandler',
            'filename': '/tmp/geodesic_logger.log'
        }
 ````

### Processus d'import

#### Tests

lancement des tests de processus d'import

> / python manage.py test

## Mise en place de l'environnement

- données initiales:
Utilisation de la librairie ** django-import-export **


- ## django-import-export

### Import des données initiales

Importer les **sources**, **collections** et les **périodes** associées

Depuis l'interface d'administration, choisir le modèle puis importer les fichiers du modèle dans :

    gdml
        /data

possibilité de paramètrer les champs des clés étrangères des modèles ressource dans l'admin

    backend
    / admin.py
```bash
PeriodeResource
```


[django import export - usage avancé](https://django-import-export.readthedocs.io/en/latest/advanced_usage.html)

### Synchronisation des données

Depuis l'interface d'administration, sélectionner un ou plusieurs objet(s): Collection, Source et Période
et réaliser l'export: (JSON/CSV)

importer ensuite les données sur l'administration du serveur cible

## Process d'import de données


[Processus d'import](gdml/Import_processus.md)

## Configuration serveur

[Celery]

installation de la version 5.3.6


création du service dans systemctl

```commandline
[Unit]
Description=Celery Service
After=network.target

[Service]
Type=forking
User=deployer
Group=deployers
EnvironmentFile=-/etc/conf/celery.conf
WorkingDirectory=/var/www/gdml/current/sites/gdml
ExecStart=/bin/sh -c '${CELERY_BIN} -A $CELERY_APP multi start $CELERYD_NODES \
    --pidfile=${CELERYD_PID_FILE} --logfile=${CELERYD_LOG_FILE} \
    --loglevel=${CELERYD_LOG_LEVEL} $CELERYD_OPTS'
ExecStop=/bin/sh -c '${CELERY_BIN} multi stopwait $CELERYD_NODES \
    --pidfile=${CELERYD_PID_FILE} --logfile=${CELERYD_LOG_FILE} \
    -loglevel="${CELERYD_LOG_LEVEL}"'
ExecReload=/bin/sh -c '${CELERY_BIN} -A $CELERY_APP multi restart $CELERYD_NODES \
    --pidfile=${CELERYD_PID_FILE} --logfile=${CELERYD_LOG_FILE} \
    --loglevel="${CELERYD_LOG_LEVEL}" $CELERYD_OPT'
Restart=on-failure
RestartSec=5
[Install]
WantedBy=multi-user.target

```
configuration celery:

créer un fichier /etc/conf/celery.conf

```
# Name of nodes to start
# here we have a single node
CELERYD_NODES="w1"
# or we could have three nodes:
#CELERYD_NODES="w1 w2 w3"

# Absolute or relative path to the 'celery' command:
CELERY_BIN="/var/www/gdml/current/venv/bin/celery"
#CELERY_BIN="/virtualenvs/def/bin/celery"

# App instance to use
# comment out this line if you don't use an app
CELERY_APP="gdml"
# or fully qualified:
#CELERY_APP="proj.tasks:app"

# How to call manage.py
CELERYD_MULTI="multi"

# Extra command-line arguments to the worker
CELERYD_OPTS="-B -l INFO --time-limit=3600 --concurrency=1"

# - %n will be replaced with the first part of the nodename.
# - %I will be replaced with the current child process index
#   and is important when using the prefork pool to avoid race conditions.
CELERYD_PID_FILE="/var/run/celery/%n.pid"
CELERYD_LOG_FILE="/var/log/celery/%n%I.log"
CELERYD_LOG_LEVEL="INFO"

```


```
> sudo systemctl daemon-reload
```

> Logs

    sortie paramètrable dans settings_local.py :

    LOG_EUDML = "/var/log/gdml/eudml_import.log"

    configuration celery : gdml/tasks.py

[Redis]

> [Installation de redis](https://redis.io/docs/install/install-redis/#install-redis-properly)

Chemin de données

créer un lien symbolique vers un espace de stockage: /gdml_test_data/

> Supervisor

  Installation
  [Supervisor-doc](https://supervisor.readthedocs.io/en/latest/installing.html#installing-to-a-system-with-internet-access)
