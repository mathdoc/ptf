import datetime

import regex

import django.dispatch
from django.dispatch import receiver

from ptf.models import Collection

collection_created = django.dispatch.Signal()
collection_updated = django.dispatch.Signal()

update_years_periode = django.dispatch.Signal()
send_result_import = django.dispatch.Signal()
send_result_import = django.dispatch.Signal()
from django.core.mail import EmailMultiAlternatives

from .models import Periode
from .models import Source

"""
    Signals called during process crawl or at first step
"""


@receiver(send_result_import)
def send_log_to(sender, **kwargs):
    subject, from_email, to = (
        "Résultat d'import: EUDML",
        "clement.lesaulnier@univ-grenoble-alpes.fr",
        "taban.danaei@univ-grenoble-alpes.fr",
    )
    text_content = "Résultat dernier import EUDML:"
    html_content = "<p><strong>/var/log/gdml/eudml_import.log</strong></p>"
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.attach_file("/var/log/gdml/eudml_import.log")
    msg.send()


@receiver(update_years_periode)
def update_periode_years(sender, **kwargs):
    year = ""
    if isinstance(kwargs["periode"], Periode):
        reg_periode = regex.compile(r"(\d+)-(\d+)")
        if reg_periode.match(kwargs["year"]):
            years = kwargs["year"].split("-")
            year = int(years[0])
        if year == "":
            year = int(kwargs["year"])
        periode = kwargs["periode"]

        start = 0
        if not isinstance(periode.begin, type(None)) or not isinstance(periode.begin, type(None)):
            if periode.begin > 0:
                start = periode.begin
            end = 0
            if periode.end > 0:
                end = periode.end
            print(start)
            print(end)
            if periode.begin == 0:
                periode.begin = int(year)
            if periode.end == 0:
                periode.end = int(year)
            if year != "":
                if periode.begin > 0:
                    if int(year) < periode.begin or start == 0:
                        periode.begin = int(year)

                    if int(year) > periode.end or end == 0:
                        periode.end = int(year)
                else:
                    if start == 0:
                        periode.start = int(year)

                    if end == 0:
                        periode.end = int(year)
        else:
            if year is None:
                year = 0
            periode.begin = int(year)
            periode.end = int(year)
        periode.save()


@receiver(collection_updated)
def update_periode(sender, **kwargs):
    print(kwargs)
    data = {}

    data["collection"] = kwargs["collection"]
    collection = kwargs["collection"]
    if isinstance(kwargs["source"], Source):
        data["source"] = kwargs["source"]
        periode = Periode.objects.filter(collection=collection, source=data["source"]).first()
        url = kwargs["url"]
        url = url.split(data["source"].domain)
        if len(url) > 1:
            periode.collection_href = url[1]
        else:
            periode.collection_href = url[0]

        periode.save()
    print("Periode updated")


@receiver(collection_created)
def create_periode(sender, **kwargs):
    print(kwargs)
    data = {}
    if isinstance(kwargs["source"], Source):
        data["source"] = kwargs["source"]
    data["collection"] = kwargs["collection"]
    url = kwargs["url"]
    pid_journal = data["collection"].pid

    url = url.split(data["source"].domain)
    data["collection_href"] = url[1]
    # data["pdf_href"] = ""
    # data["article_href"] = ""
    # data["periode_href"] = ""
    data["issue_href"] = ""
    data["begin"] = 0
    data["end"] = 0
    collection = Collection.objects.get(pid=data["collection"].pid)
    """if data["collection"].fyear != 0 or data["collection"].fyear != '' :
        data["periode"].append(data["collection"].fyear)
    if data["collection"].lyear != 0 or data["collection"].lyear != '' :
        data["periode"].append(data["collection"].lyear)"""

    # data["create_xissue"]
    data["title"] = data["collection"].title_html
    p = Periode.objects.create(**data)
    print("Periode created")
