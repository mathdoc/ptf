from django.apps import AppConfig

from crawl.mathcollector import metadata


class GdmlConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "gdml"
    metadata_import = metadata.MetadataImport()
