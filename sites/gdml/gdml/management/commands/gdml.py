import json

from django.core.management.base import BaseCommand

from backend.cron import reset_progression_eudml
from backend.cron import start_crawl_eudml
from gdml.models import Periode
from gdml.models import Source
from ptf.models import Collection
from ptf.models import Container

from ptf.cmds import ptf_cmds


class Command(BaseCommand):
    help = "Import collections to manage from list"
    links = []

    def add_arguments(self, parser):
        parser.add_argument(
            "-issn",
            dest="issn",
            action="store_true",
            default=None,
            required=False,
            help="Get issn list",
        ),
        parser.add_argument(
            "-numdam",
            dest="numdam",
            action="store_true",
            default=None,
            required=False,
            help="Numdam collection",
        ),

        parser.add_argument(
            "-add_permissions",
            dest="add_permissions",
            action="store_true",
            default=None,
            required=False,
            help="Set permission to groups management of Geodesic",
        ),

        parser.add_argument(
            "-delete_objects",
            dest="delete_objects",
            action="store_true",
            default=None,
            required=False,
            help="Reset database objects for collection, periode",
        ),

        parser.add_argument(
            "-reset_crawl_eudml",
            dest="reset_crawl_eudml",
            action="store_true",
            default=None,
            required=False,
            help="Reset crawl",
        ),

        parser.add_argument(
            "-start_crawl_eudml",
            dest="start_crawl_eudml",
            action="store_true",
            default=None,
            required=False,
            help="Start crawl",
        ),

        parser.add_argument(
            "-username", dest="username", type=str, default=None, help="username executing thread"
        ),
        parser.add_argument(
            "-pid", dest="pid", type=str, default=None, help="username executing thread"
        ),

    def extract_issn(self):
        numdam_collections = "'ACIRM' 'AFST' 'AIF' 'AIHP' 'AIHPA' 'AIHPB' 'AIHPC' 'ALCO' 'AMBP' 'AMPA' 'ASCFM' 'ASCFPA' 'ASENS' 'ASNSP' 'AST' 'AUG' 'BSMA' 'BSMF' 'BURO' 'CAD' 'CCIRM' 'CG' ' CIF' 'CJPS' 'CM' 'CML' 'COCV' 'CRASCHIM' 'CRASMATH' 'CRBIOL' 'CRCHIM' 'CRGEOS' 'CRMATH' 'CRMECA' 'CRPHYS' 'CSHM' 'CTGDC' 'DIA' 'GAU' 'GEA' 'ITA' 'JEDP' 'JEP' 'JTNB' 'JSFS' 'M2AN' 'MALSM' 'MBK' 'MRR' 'MSH' 'MSIA' 'MSM' 'MSMF' 'NAM' 'OGEO' 'OJMO' 'PCJ' 'PDML' 'PHSC' 'PMB' 'PMIHES' 'PS' 'PSMIR' 'RCP25' 'RHM' 'RO' 'ROIA' 'RSA' 'RSMUP' 'SAC' 'SAD' 'SAF' 'SB' 'SBCD' 'SC' 'SCC' 'SD' 'SDPP' 'SE' 'SEDP' 'SEML' 'SG' 'SHC' 'SJ' 'SJL' 'SL' 'SLDB' 'SLS' 'SLSEDP' 'SMAI-JCM' 'SMJ' 'SMS' 'SPHM' 'SPK' 'SPS' 'SSL' 'SSS' 'STNB' 'STNG' 'STS' 'TAN' 'THESE' 'TSG' 'WBLN'"

        cols = numdam_collections.split(" ")

        data = []
        for pid in cols:
            key = pid.strip().replace("'", "")
            c = Collection.objects.filter(pid=key)
            if len(c) > 0:
                c = c[0]
                item = {"pid": pid, "issn": c.issn}
                data.append(item)

        with open("issn.json", "w") as f:
            json.dump(data, f, ensure_ascii=False)

    def handle(self, *args, **options):
        issn_action = options["issn"]
        numdam = options["numdam"]
        delete_objects = options["delete_objects"]
        username = options["username"]
        pid = options["pid"]
        start_crawl_eudml = options["start_crawl_eudml"]
        reset_crawl_eudml = options["reset_crawl_eudml"]

        if start_crawl_eudml:
            start_crawl_eudml(username, pid)
        if reset_crawl_eudml:
            reset_progression_eudml()

        source_1 = Source.objects.get(name="European Digital Mathematics Library")
        source_2 = Source.objects.get(name="Mathnet.ru")
        if delete_objects:
            periodes = Periode.objects.filter(source__in=[source_1, source_2])
            for periode in periodes:
                collection = periode.collection
                containers = Container.objects.filter(my_collection=collection)
                for issue in containers:
                    cmd = ptf_cmds.addContainerPtfCmd({"pid": issue.pid, "ctype": issue.ctype})
                    cmd.set_provider(provider=issue.provider)
                    cmd.add_collection(collection)
                    cmd.set_object_to_be_deleted(issue)
                    cmd.undo()

        if issn_action:
                if numdam:
                    self.extract_issn()
