import os

from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import redirect
from django.utils.translation import gettext as _

from gdml import settings
from gdml.models import Gdml_log

# from gdml.tasks import remove_articles
# from gdml.tasks import remove_lonely_container as remove_lonely_container_task
from ptf.cmds.xml_cmds import importEntireCollectionXmlCmd


@staff_member_required
def import_from_local(request):
    dirs = os.listdir(settings.DATA_FOLDER)
    for dir in dirs:
        cmd = importEntireCollectionXmlCmd({"pid": dir, "import_folder": settings.DATA_FOLDER})
        cmd.do()

    messages.success(request, _("Import OK"))
    return redirect("admin:index")


# @staff_member_required
# def remove_all_articles(request, id_collection):
#     remove_articles.delay(id_collection)
#     messages.success(request, _("Delete Launched"))
#     return redirect("admin:index")


# @staff_member_required
# def remove_lonely_container(request):
#     remove_lonely_container_task.delay()
#     messages.success(request, _("Remove lonely container Launched"))
#     return redirect("admin:index")


@staff_member_required
def remove_all_logs(request):
    Gdml_log.objects.all().delete()
    messages.success(request, _("Delete OK"))
    return redirect("admin:index")
