from enumfields import Enum
from enumfields import EnumField

from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _

from ptf.models import Article
from ptf.models import Collection
from ptf.models import CollectionQuerySet
from ptf.models import Container
from ptf.models import Provider
from ptf.models import Publisher


class Formats(Enum):
    OAI_DC = "oai_dc"
    JATS = "jats"
    NLM = "nlm"
    BOOK = "book"
    JATS_JATS = "jats jats"


class Labels:
    OAI_DC = "oai_dc"
    JATS = "eudml-article2"
    JATS_JATS = "jats"
    BOOK = "eudml-book2"
    NLM = "nlm"


class SourceItem(models.TextChoices):
    EUDML = "EUDML"
    AUSTRALIAN_MATHEMATICAL_SOCIETY = "Australian Mathematical Society"
    EUCLID = "Projet Euclid"
    ELIBM = "The Electronic Library of Mathematics"
    E_PERIODICA = "ETH Library"
    MATHNET = "Mathnet"
    AM = "Annals Of Mathematics"
    HDML = "Hellenic Digital Mathematics Library"
    PM = "Portugaliae Mathematica"


"""
    Set source name label associated with domain name
"""


class SourceName(Enum):
    AMC_JOURNAL_EU = "ARS MATHEMATICA CONTEMPORANEA"
    EUDML_ORG = "EUDML"
    JOURNAL_AUSTMS_ORG_AU = "Australian Mathematical Society"
    PROJECTEUCLID_ORG = "Projet Euclid"
    ELIBM_ORG = "The Electronic Library of Mathematics"
    LIBRARY_ETHZ_CH = "E-Periodica"
    MATHNET_RU = "Mathnet.Ru"
    JSTOR_ORG = "Jstor"
    HDML_DI_IONIO_GR = "Hellenic Digital Mathematics Library"
    BNDIGITAL_BNPORTUGAL_GOV_PT = "Portugaliae Mathematica"
    BDIM_EU = "Biblioteca Digitale Italiana di Matematica"
    DMLE_ICMAT_ES = (
        "Biblioteca Digital de Matemáticas (ICMAT – Institute of Mathematical Sciences) "
    )
    EMIS_DE = "European Mathematical Information Service"
    DML_CZ = "Czech Digital Mathematics Library"
    DIGITAL_CSIS_ES = "DIGITAL.CSIC"
    GDZ_SUB_UNI_GOETTINGEN_DE = "Göttinger Digitalisierungszentrum"
    PLDML_ICM_EDU_PL = "Interdisciplinary Centre for Mathematical and Computational Modelling"
    BIBLIOTEKNAUKI_PL = "Library of Science"
    NUMDAM_ORG = "Numdam, la bibliothèque numérique française de mathématiques"
    SCI_GEMS_MATH_BAS_BG = "Bulgarian Digital Mathematics Library"
    DML_ICMAT_ES = "Instituto de Ciencias Matemáticas (DML-E)"
    INDICES_APP_CSIC_ES = "Información y Documentación de la Ciencia en España"
    ANNALS_MATH_PRINCETON_EDU = "Princeton University & Institute for Advanced Study"
    CAI_SK = "Computing and Informatics"
    CUBO_UFRO_CL = "Cubo A Mathematical Journal"
    CORE_AC_UK = "core.ac.uk"
    JFR_UNIBO_IT = "Journal of Formalized Reasoning"
    MATHOS_UNIOS_HR = "Mathematical Communications "
    MAT_SAVBA_SK = "Matematický ústav"
    PERIODICOS_UEM_BR = "Universidade Estadual de Maringá - PORTAL DE PERIODICOS"
    COMBINATORICS_ORG = "The Electronic Journal of Combinatorics"
    ARXIV_ORG = "arXiv"
    HAL_SCIENCE = "HAL science ouverte"
    LIB_WASHINGTON_EDU = "Mathematics Research Library"
    THESES_HAL_SCIENCE = "Theses HAL science ouverte"
    CDS_CERN_CH = "CERN"


class SourceManager(models.Manager):
    def get_by_natural_key(self, name):
        return self.get(name=name)


class Source(models.Model):
    object_id = models.PositiveIntegerField(null=True)
    domain = models.CharField(max_length=150, null=True)
    website = models.CharField(max_length=250, null=True)
    name = models.CharField(max_length=300)
    create_xissue = models.BooleanField(blank=True, null=True)
    periode_href = models.CharField(max_length=250, blank=True, null=True)
    article_href = models.CharField(max_length=150, null=True, blank=True)
    pdf_href = models.CharField(max_length=250, blank=True)
    objects = SourceManager()

    def natural_key(self):
        return (self.name,)

    def __str__(self):
        return f"{self.name}"

    class Meta:
        ordering = ["name"]


class CollectionManager(models.Manager):
    def get_by_natural_key(self, pid):
        return self.get(pid=pid)


class PeriodeManager(models.Manager):
    def get_by_natural_key(self, title):
        return self.get(title=title)


# old definition of model between collection and sources
class CollectionSources(models.Model):
    parent = models.ForeignKey(
        "self", on_delete=models.CASCADE, null=True, blank=True, related_name="ancestors"
    )
    objects = CollectionQuerySet.as_manager()
    source = models.ManyToManyField(Source, related_name="sources_collection")
    title_sort = models.CharField(max_length=255, db_index=True)  # sort key, not displayed
    collection_ptr = models.ForeignKey(
        Collection, on_delete=models.CASCADE, null=True, related_name="source_collection_ptr"
    )

    def __str__(self):
        return f"{self.title_sort}"

    class Meta:
        ordering = ["title_sort"]


class ColType(Enum):
    JOURNAL = "journal"
    BOOK = "book"
    BOOK_SERIES = "book-series"


class Periode(models.Model):
    collection = models.ForeignKey(
        Collection, null=True, related_name="periods", on_delete=models.CASCADE
    )
    source = models.ForeignKey(
        Source, blank=True, null=True, related_name="source", on_delete=models.CASCADE
    )
    title = models.CharField(max_length=250, default="title")
    issue_href = models.CharField(max_length=150, null=True, blank=True)
    collection_href = models.CharField(max_length=300, null=True, blank=True)
    doi_href = models.CharField(max_length=90, blank=True)
    published = models.BooleanField(blank=True, null=True)
    begin = models.IntegerField(_("Début"), blank=True, null=True)
    end = models.IntegerField(_("Fin"), blank=True, null=True)
    first_issue = models.IntegerField(_("premier numéro"), blank=True, null=True)
    last_issue = models.IntegerField(_("dernier numéro"), blank=True, null=True)

    publisher = models.ForeignKey(
        Publisher, blank=True, null=True, related_name="publisher_col", on_delete=models.PROTECT
    )
    wall = models.IntegerField(default=5)
    objects = PeriodeManager()

    def natural_key(self):
        return (self.title, self.collection)

    def __str__(self):
        return f"{self.collection}"


class OAI_harvest(models.Model):
    source = models.CharField(max_length=80, blank=True, null=True)
    url_home = models.CharField(max_length=80, blank=True, null=True)
    url = models.CharField(max_length=60)
    xml_format_spec = models.CharField(max_length=32)
    xml_format = EnumField(Formats, verbose_name=_("Type"), blank=True, null=True)
    provider = models.ForeignKey(Provider, on_delete=models.CASCADE)
    provider_col = models.ForeignKey(
        Provider, blank=True, null=True, related_name="provider_col", on_delete=models.CASCADE
    )
    lang = models.CharField(max_length=20, default="en")

    is_prepub = models.BooleanField(default=False)
    is_thesis = models.BooleanField(default=False)

    one_shot = models.BooleanField(default=False)

    # a set for selective harvesting, optional
    oai_set = models.CharField(max_length=50, blank=True, null=True)

    last_harvest = models.DateField(default=now)

    # need to add spec like sub stuff for oai bla bla (set maybe?)

    class Meta:
        ordering = ["source"]

    def __str__(self):
        return self.source if self.source else self.url

    def get_count_articles(self):
        if self.xml_format == Formats.BOOK:
            return self.oai_book_set.count()
        else:
            return self.oai_article_set.count()

    def get_title(self):
        if self.xml_format == Formats.BOOK:
            return "books"
        else:
            return "articles"


class OAI_article(models.Model):
    oai_identifier = models.CharField(max_length=100)
    article = models.OneToOneField(Article, on_delete=models.CASCADE)
    harvest_source = models.ForeignKey(OAI_harvest, on_delete=models.CASCADE)

    def delete(self, *args, **kwargs):
        self.article.delete()
        return super(self.__class__, self).delete(*args, **kwargs)

    def get_oai_link(self):
        return "{url}?verb=GetRecord&identifier={id}&metadataPrefix={pf}".format(
            url=self.harvest_source.url,
            id=self.oai_identifier,
            pf=self.harvest_source.xml_format.label,
        )


class OAI_Book(models.Model):
    oai_identifier = models.CharField(max_length=100, unique=True)
    book = models.OneToOneField(Container, on_delete=models.CASCADE)
    harvest_source = models.ForeignKey(OAI_harvest, on_delete=models.CASCADE)

    def delete(self, *args, **kwargs):
        self.article.delete()
        return super(self.__class__, self).delete(*args, **kwargs)


class Gdml_log(models.Model):
    source = models.ForeignKey(OAI_harvest, blank=True, null=True, on_delete=models.CASCADE)
    oai_identifier = models.CharField(max_length=100, blank=True, null=True)
    msg = models.TextField()
    trace = models.TextField(blank=True, null=True)
    raw = models.TextField(blank=True, null=True)
    create_datetime = models.DateTimeField(auto_now_add=True, verbose_name="Created at")

    def __str__(self):
        return f"{self.source} - {self.msg}"
