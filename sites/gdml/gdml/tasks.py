# import calendar
# import json
# import os
# import re
#
# from celery import Celery
# from celery import shared_task
#
# from django.db import transaction
# from django.db.models import Q
#
# from backend.cron import restart_crawl_eudml
# from backend.cron import start_crawl_eudml
# from backend.serializers import PeriodeSerializer
# from crawl.list import collections_to_crawl
# from gdml.import_oai.import_site import ImportSite
# from gdml.models import Gdml_log
# from gdml.models import OAI_article
# from gdml.models import OAI_harvest
# from gdml.models import Periode
# from gdml.models import Source
# from ptf.cmds import ptf_cmds
# from ptf.cmds.ptf_cmds import addArticlePtfCmd
# from ptf.cmds.ptf_cmds import addCollectionPtfCmd
# from ptf.model_helpers import get_collection
# from ptf.models import Collection
#
# # Set the default Django settings module for the 'celery' program.
# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "proj.settings")
#
# app = Celery("gdml")
#
# # Using a string here means the worker doesn't have to serialize
# # the configuration object to child processes.
# # - namespace='CELERY' means all celery-related configuration keys
# #   should have a `CELERY_` prefix.
# app.config_from_object("django.conf:settings", namespace="CELERY")
#
# # Load task modules from all registered Django apps.
# app.autodiscover_tasks()
# import logging
# import os
#
# import redis
# from celery import states
# from celery.signals import before_task_publish
#
# # from celery.signals import task_failure
# # from celery.signals import task_success
# from django_celery_results.models import TaskResult
#
# from history.views import manage_exceptions
#
# logger = logging.getLogger("celery")
# from celery import signals
#
# from django.core.mail import EmailMultiAlternatives
# from django.utils import timezone
#
#
# @app.task(name="gdml.tasks.start_crawling_eudml", bind=True)
# def start_crawling_eudml(self, pid="", username=None):
#     start_crawl_eudml()
#     return True
#
#
# @app.task(name="gdml.tasks.test_mail", bind=True)
# def test_mail(self, username):
#     print(username)
#     try:
#         subject, from_email, to = (
#             "Résultat d'import: EUDML",
#             "mailhost.u-ga.fr",
#             "clement.lesaulnier@univ-grenoble-alpes.fr",
#         )
#         text_content = "Résultat dernier import EUDML:"
#         html_content = "<p><strong>/var/log/gdml/eudml_import.log</strong></p>"
#         msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
#         msg.attach_alternative(html_content, "text/html")
#         msg.attach_file("/var/log/gdml/eudml_import.log")
#         msg.send()
#     except Exception as ex:
#         return "Attachment error" + ex
#
#
# @signals.setup_logging.connect
# def on_celery_setup_logging(**kwargs):
#     config = {
#         "version": 1,
#         "disable_existing_loggers": False,
#         "formatters": {
#             "default": {
#                 "format": "%(asctime)s%(process)d/%(thread)d%(name)s%(funcName)s %(lineno)s%(levelname)s%(message)s",
#                 "datefmt": "%Y/%m/%d %H:%M:%S",
#             }
#         },
#         "handlers": {
#             "celery": {
#                 "level": "INFO",
#                 "class": "logging.FileHandler",
#                 "filename": "/var/log/celery.log",
#                 "formatter": "default",
#             },
#             "default": {
#                 "level": "DEBUG",
#                 "class": "logging.StreamHandler",
#                 "formatter": "default",
#             },
#         },
#         "loggers": {
#             "celery": {"handlers": ["celery"], "level": "INFO", "propagate": False},
#         },
#         "root": {"handlers": ["default"], "level": "DEBUG"},
#     }
#
#     logging.config.dictConfig(config)
#
#
# # @signals.task_success.connect
# # def task_success_handler(sender, result, **kwargs):
# #     print("success")
# #     print(kwargs)
# #     print(result)
# #     print(sender)
#
#
# @before_task_publish.connect
# def create_task_result_on_publish(sender=None, headers=None, body=None, **kwargs):
#     if "task" not in headers:
#         return
#
#     TaskResult.objects.store_result(
#         "application/json",
#         "utf-8",
#         headers["id"],
#         None,
#         states.PENDING,
#         task_name=headers["task"],
#         task_args=headers["argsrepr"],
#         task_kwargs=headers["kwargsrepr"],
#     )
#
#
# @app.task(name="gdml.tasks.restart_crawling_eudml", bind=True)
# def restart_crawling_eudml(self, username):
#     restart_crawl_eudml(username)
#     return True
#
#
# def update_error_task(id_task):
#     redis_conn = redis.StrictRedis(host="127.0.0.1", port=6379)
#     errors_count = int(redis_conn.get("failures_collection"))
#     errors_count += 1
#     redis_conn.set("failures_collection", errors_count)
#
#     date_event = timezone.now()
#     date_event = calendar.timegm(date_event.timetuple())
#     task = TaskResult.objects.get(task_id=id_task)
#
#     task_kwargs = task.task_kwargs.replace("\n", "")
#     print("parametres tache")
#     print(task_kwargs)
#     print("------------------------------------------------")
#     # task_kwargs = json.loads(task_kwargs)
#     task_kwargs = task_kwargs.replace('"', "'").replace("'", '"').replace("None", "")
#     print(task_kwargs)
#     task_kwargs = json.loads(task_kwargs)
#     args = eval(task.task_args)
#
#     if args != "()":
#         print("args")
#         print(args)
#         count = args[3]
#
#     t = TaskResult.objects.filter(Q(task_name="gdml.tasks.prepare_crawling")).last()
#     meta_task_parent = json.loads(t.meta)
#     total = len(meta_task_parent["children"])
#
#     # get last import log and replace to a new
#     data_task = {
#         "uid": id_task,
#         "date": date_event,
#         "collection": task_kwargs["pid"],
#         "title": task_kwargs["title"],
#         "username": task_kwargs["username"],
#         "periode": ",".join(map(str, task_kwargs["periode_range"])),
#         "numbers": ",".join(map(str, task_kwargs["number_range"])),
#         "total": total,
#         "success": total - errors_count,
#         "failures": errors_count,
#         "progress": count,
#     }
#     command_tasks = redis_conn.xrevrange("task:import", max="+", min="-", count=1)
#     if len(command_tasks) > 0:
#         last_command = command_tasks[0]
#         print(">>>>>")
#         last_id_command = last_command[0].decode()
#         redis_conn.xdel("task:import", last_id_command)
#
#     redis_conn.xadd("task:import", data_task)
#
#
# @app.task(name="gdml.tasks.import_issue", soft_time_limit=180, bind=True)
# @transaction.atomic
# def import_issue(
#     self,
#     periode_id,
#     parent,
#     url,
#     count,
#     urls=None,
#     pid=None,
#     userid=None,
#     periode_range=None,
#     number_range=None,
#     title=None,
#     username=None,
# ):
#     # id_task = self.request.id
#
#     if isinstance(periode_id, str):
#         # reg_pid = re.match("^[A-Z]*$", colid)
#         reg_id = re.match("^[0-9]*$", periode_id)
#
#         if reg_id:
#             periode = Periode.objects.get(pk=periode_id)
#
#     else:
#         periode = Periode.objects.get(pk=periode_id)
#
#     serializer = PeriodeSerializer(periode)
#
#     data = serializer.data
#     collection = periode.collection
#
#     data["pid"] = collection.pid
#     data["coltype"] = collection.coltype
#     data["wall"] = (collection.wall,)
#     data["title"] = collection.title_sort
#     data["source"] = periode.source
#     source = Source.objects.get(name=periode.source)
#     data["domain"] = source.domain
#     data["create_xissue"] = source.create_xissue
#     data["website"] = source.website
#     data["collection_href"] = periode.collection_href
#     data["pdf_href"] = source.pdf_href
#     data["article_href"] = source.article_href
#     data["periode_href"] = source.periode_href
#     data["issue_href"] = periode.issue_href
#     data["issue_links"] = urls
#     data.pop("collection")
#     data.pop("published")
#     data["collection"] = collection.pid
#     data["periode"] = periode
#     data["wall"] = "0"
#     data["parent_history"] = parent
#     data["userid"] = userid
#     """
#     crawler = CollectionCrawler(**data)
#     if source.domain == "hdml.di.ionio.gr":
#         crawler = HdmlCrawler(**data)
#     if source.domain in ["annals.math.princeton.edu", "archive.org"]:
#         crawler = AMCrawler(**data)
#     if source.domain == "www.mathnet.ru" or source.domain == "mathnet.ru":
#         crawler = MathnetCrawler(**data)
#         print(crawler)
#     print(crawler)
#     print("START")
#         """
#     """
#     try:
#         if len(periode_range) > 0:
#             periode_list = ""
#             for index, date in enumerate(periode_range):
#                 periode_list += str(date)
#                 if index == 0:
#                     periode_list += ","
#             periode_range = periode_list
#
#         if len(number_range) > 0:
#             number_list = []
#             for number in periode_range:
#                 number_list.append(number)
#             number_range = number_list
#         print(periode_range)
#         print(number_range)
#
#         with transaction.atomic():
#             time.sleep(1)
#             try:
#                 crawler.import_issue(url)
#                 # operation succeeded
#
#
#             except Exception as ex:
#                 print("Exception inserting container")
#                 print(str(crawler.container))
#                 print("EXCEPTION")
#                 print(ex)
#                 print(parent)
#                 template = "Une exception de type {0} est survenue. Arguments:\n{1!r}"
#                 type_error = template.format(type(ex).__name__, ex.args)
#                 # insert history event for container and update history command parent
#
#                 manage_exceptions(
#                     "import",
#                     pid=str(crawler.container),
#                     colid=collection.pid,
#                     status="ERROR",
#                     title=collection.title_html,
#                     exception=ex,
#                     target=str(crawler.container),
#                     userid=userid,
#                     type_error=type_error,
#                     parent_id=parent,
#                 )
#
#                 time.sleep(1)
#                 h = HistoryEvent.objects.get(pk=parent)
#                 print("unique id ..............")
#                 print(h.unique_id)
#                 manage_exceptions(
#                     "import",
#                     pid=collection.pid + "-" + str(periode_id),
#                     colid=collection.pid,
#                     status="ERROR",
#                     title=collection.title_html,
#                     exception=ex,
#                     target=str(crawler.container),
#                     userid=userid,
#                     type_error=type_error,
#                     unique_id=h.unique_id,
#                 )
#                 print(collection.pid + "-" + str(periode_id))
#                 update_error_task(id_task)
#
#     except BlockingIOError as exception:
#         exception = str(exception)
#         exception.split("raise ")
#
#         manage_exceptions(
#             "import",
#             pid=str(crawler.container),
#             colid=collection.pid,
#             status="WARNING",
#             title=collection.title_html,
#             exception=exception,
#             target=url,
#             userid=userid,
#             type_error="Erreur 404",
#             parent_id=parent,
#         )
#         raise
#
#     except Exception as exception:
#         exception = str(exception)
#         exception.split("raise ")
#
#         manage_exceptions(
#             "import",
#             pid=str(crawler.container),
#             colid=collection.pid,
#             status="ERROR",
#             title=collection.title_html,
#             exception=exception,
#             target=url,
#             userid=userid,
#             type_error="Problème d'entrée/sortie",
#             parent_id=parent,
#         )
#         update_error_task(id_task)
#         raise
#     """
#
#
# @shared_task()
# def reset_import(pid):
#     xcollection = collections_to_crawl[pid]
#     collection = get_collection(xcollection["pid"])
#
#     for issue in collection.content.all():
#         cmd = ptf_cmds.addContainerPtfCmd({"pid": issue.pid, "ctype": issue.ctype})
#         cmd.set_provider(provider=issue.provider)
#         cmd.add_collection(collection)
#         cmd.set_object_to_be_deleted(issue)
#         cmd.undo()
#
#
# @app.task(name="gdml.tasks.prepare_crawling", bind=True)
# def prepare_crawling(
#     self,
#     periodeid,
#     periode=None,
#     numbers=None,
#     restart=None,
#     userid=None,
#     username=None,
#     title=None,
#     pid=None,
# ):
#     print(periodeid)
#     print("start crawl")
#     print(type(periodeid))
#
#     periode_obj = Periode.objects.get(pk=int(periodeid))
#     print("periode >")
#     print(periode_obj)
#     periode_range = ""
#
#     # instance of redis client in order to update result
#
#     # re-init results of tasks
#     TaskResult.objects.all().delete()
#     #
#     if isinstance(numbers, type(None)):
#         numbers = ""
#
#     if not isinstance(periode, type(None)):
#         try:
#             if len(periode) > 0:
#                 periode_range = periode
#                 if len(periode_range) > 0:
#                     if periode_range[0] == periode_range[1]:
#                         periode_range = [periode_range[0]]
#
#         except Exception as e:
#             print("exception")
#             print(e)
#             pass
#
#     number_range = ",".join(map(str, numbers))
#     date_event = timezone.now()
#     date_event = calendar.timegm(date_event.timetuple())
#     data_task = {
#         "periodeid": periodeid,
#         "date": date_event,
#         "userid": userid,
#         "periode": ",".join(map(str, periode_range)),
#         "numbers": number_range,
#     }
#     print("data TASK ::")
#     print(data_task)
#     redis_conn = redis.StrictRedis(host="127.0.0.1", port=6379)
#     redis_conn.set("failures_collection", 0)
#
#     try:
#         serializer_periode = PeriodeSerializer(periode_obj)
#         data = serializer_periode.data
#         collection = periode_obj.collection
#
#         # if periode range in input
#
#         try:
#             if len(numbers) > 0:
#                 data["numbers"] = numbers
#         except Exception:
#             pass
#         data["pid"] = collection.pid
#         data["coltype"] = collection.coltype
#         data["wall"] = (collection.wall,)
#         data["title"] = collection.title_html
#         data["source"] = periode_obj.source
#         data["collection_href"] = periode_obj.collection_href
#
#         source = Source.objects.get(name=periode_obj.source)
#         data["periode_range"] = periode
#         print("periode range .....")
#         print(data["periode_range"])
#         data["domain"] = source.domain
#         data["website"] = source.website
#         data["pdf_href"] = source.pdf_href
#         data["article_href"] = source.article_href
#         data["periode_href"] = source.periode_href
#         data["issue_href"] = periode_obj.issue_href
#         data["numbers"] = numbers
#         data["periode"] = periode_obj
#         print("step")
#         print("data .....")
#         print(data)
#         data.pop("collection")
#         data.pop("published")
#         data["wall"] = "0"
#
#     #     crawler = CollectionCrawler(**data)
#     #     if source.domain == "hdml.di.ionio.gr":
#     #         crawler = HdmlCrawler(**data)
#     #
#     #     if source.domain == "annals.math.princeton.edu":
#     #         crawler = AMCrawler(**data)
#     #     if source.domain == "www.mathnet.ru" or source.domain == "mathnet.ru":
#     #         crawler = MathnetCrawler(**data)
#     #         print(crawler)
#     #
#     #     if restart:
#     #         reset_import.delay(data.pk)
#     #
#     #     print("periode_range")
#     #     print("---")
#     #     print(periode_range)
#     #     issues_url = crawler.get_collection_issues(
#     #         periode_range=periode_range,
#     #     )
#     #     print("issues url >>>>>>")
#     #     print(issues_url)
#     #
#     #     # insert history with id command
#     #     data_task["total"] = len(issues_url)
#     #     redis_conn.xadd("import:" + collection.pid, data_task)
#     #     key = "import:" + collection.pid
#     #     command_tasks = redis_conn.xrevrange("import:" + collection.pid, max="+", min="-", count=1)
#     #     current_command = command_tasks[0]
#     #     print(">>>>>")
#     #     print(current_command)
#     #     last_id_command = current_command[0].decode()
#     #     # data_task["total"] = len(issues_url)
#     #
#     #     print("get issues")
#     #
#     #     if len(issues_url) == 0:
#     #         manage_exceptions(
#     #             "import",
#     #             pid=str(crawler.container),
#     #             colid=collection.pid,
#     #             status="ERROR",
#     #             unique_id=last_id_command,
#     #             exception="numéros non récupérés depuis la source pour la période :"
#     #             + periode_range,
#     #             target=None,
#     #             userid=userid,
#     #             type_error="Problème lors de la préparation au moissonage",
#     #             title=collection.title_html,
#     #         )
#     #
#     #     # progress_recorder = ProgressRecorder(self)
#     #
#     #     # set as redis import command
#     #     date_event = datetime.now()
#     #     date_event = calendar.timegm(date_event.timetuple())
#     #
#     #     data_current_task = {
#     #         "periodeid": periodeid,
#     #         "date": date_event,
#     #         "userid": userid,
#     #         "periode": ",".join(map(str, periode_range)),
#     #         "numbers": number_range,
#     #         "total": len(issues_url),
#     #     }
#     #     redis_conn = redis.StrictRedis(host="127.0.0.1", port=6379)
#     #     redis_conn.xadd("import:" + collection.pid, data_current_task)
#     #     key = "import:" + collection.pid
#     #     command_tasks = redis_conn.xrevrange(key, max="+", min="-", count=1)
#     #     current_command = command_tasks[0]
#     #     last_id_command = current_command[0].decode()
#     #
#     #     insert_history_event(
#     #         {
#     #             "type": "import",
#     #             "unique_id": last_id_command,
#     #             "pid": collection.pid + "-" + str(periodeid),
#     #             "col": collection.pid,
#     #             "status": "OK",
#     #             "title": collection.title_html,
#     #             "type_error": "",
#     #             "userid": userid,
#     #             "data": {
#     #                 "ids_count": 0,
#     #                 "message": "",
#     #                 "target": "",
#     #             },
#     #         }
#     #     )
#     #     parent_evt = HistoryEvent.objects.filter(unique_id=last_id_command)
#     #     parent_evt = parent_evt[0]
#     #
#     #     for index, url in enumerate(issues_url):
#     #         print("import issue ....")
#     #
#     #         self.update_state(state="PROGRESS", meta={"current": index, "total": len(issues_url)})
#     #
#     #         if source.domain != "hdml.di.ionio":
#     #             title = data["title"]
#     #         else:
#     #             title = ""
#     #         import_issue.apply_async(
#     #             args=[periodeid, parent_evt.id, url, index + 1, issues_url],
#     #             kwargs={
#     #                 "pid": collection.pid,
#     #                 "userid": userid,
#     #                 "periode_range": periode_range,
#     #                 "number_range": numbers,
#     #                 "title": title,
#     #                 "username": username,
#     #             },
#     #         )
#     #
#     except Exception as exception:
#         redis_conn.xadd("import:" + collection.pid, data_task)
#         command_tasks = redis_conn.xrevrange("import:" + collection.pid, max="+", min="-", count=1)
#         current_command = command_tasks[0]
#         last_id_command = current_command[0].decode()
#         print("exception %s", exception)
#         manage_exceptions(
#             "import",
#             colid=collection.pid,
#             pid=collection.pid,
#             status="ERROR",
#             unique_id=last_id_command,
#             exception=exception,
#             userid=userid,
#             type_error="Problème lors de la préparation au moissonage",
#             title=collection.title_html,
#         )
#         raise
#
#
# # @task_success.connect()
# # def prepare_crawling_success_handler(sender, result, **kwargs):
# #     t = TaskResult.objects.filter(Q(task_name="gdml.tasks.prepare_crawling")).last()
# #     id_task = t.task_id
# #     progress = TaskResult.objects.get(task_id=sender.request.id)
# #
# #     redis_conn = redis.StrictRedis(host="127.0.0.1", port=6379)
# #     success_count = int(redis_conn.get("success_collection"))
# #     args = progress.task_args.split(",")
# #     print(args)
# #     print(">>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
# #     if len(args) >= 4:
# #         count = args[3]
# #         print("success from task: %", id_task)
# #         redis_conn = redis.StrictRedis(host="127.0.0.1", port=6379)
# #
# #         print("task_id")
# #         print(id_task)
# #         date_event = timezone.now()
# #         date_event = calendar.timegm(date_event.timetuple())
# #         children = json.loads(t.meta)
# #         total = len(children["children"])
# #
# #         params = t.task_kwargs
# #         total_failures = 0
# #         task_kwargs = json.loads(params)
# #         task_kwargs = (
# #             task_kwargs.replace('"', "'").replace("'", '"').replace("None", '""').replace("\n", "")
# #         )
# #         task_kwargs = json.loads(task_kwargs)
# #
# #         for index, child in enumerate(children["children"]):
# #             res = child[0][0]
# #
# #             task = TaskResult.objects.get(task_id=res)
# #             if task.status == "SUCCESS":
# #                 success_count += 1
# #
# #                 # remove old task in history
# #             if task.status == "FAILURE":
# #                 total_failures += 1
# #
# #         logger.info(task_kwargs)
# #         periode = ""
# #
# #         if "periode" in task_kwargs:
# #             if task_kwargs["periode"] != "" and not isinstance(task_kwargs["periode"], type(None)):
# #                 periode = task_kwargs["periode"]
# #         else:
# #             periode = ""
# #         if "numbers" in task_kwargs:
# #             numbers = task_kwargs["numbers"]
# #         else:
# #             numbers = ""
# #
# #         if int(count) == total:
# #             data_task = {
# #                 "uid": id_task,
# #                 "date": date_event,
# #                 "collection": task_kwargs["pid"],
# #                 "title": task_kwargs["title"],
# #                 "username": task_kwargs["username"],
# #                 "periode": " , ".join(map(str, periode)),
# #                 "numbers": " , ".join(map(str, numbers)),
# #                 "total": total,
# #                 "success": success_count,
# #                 "failures": total_failures,
# #                 "progress": 100,
# #             }
# #             print(data_task)
# #             redis_conn.xadd("task:import", data_task)
# #             command_tasks = redis_conn.xrevrange("task:import", max="+", min="-", count=1)
# #             current_command = command_tasks[0]
# #             print("get last inserted command")
# #             print(current_command)
# #
# #
# # @app.task
# # @task_failure.connect
# # def task_failure_notifier(sender=None, **kwargs):
# #     print("From task_failure_notifier ==> Task failed successfully! 😅")
# #     print("command inputs")
# #     redis_conn = redis.StrictRedis(host="127.0.0.1", port=6379)
# #
# #     t = TaskResult.objects.filter(Q(task_name="gdml.tasks.prepare_crawling")).last()
# #     id_task = t.task_id
# #     print(sender)
# #     logger.info(rf"error on sender \{sender}")
# #
# #     progress = TaskResult.objects.get(task_id=sender.request.id)
# #
# #     print("_______")
# #     print(progress)
# #     args = eval(progress.task_args)
# #     print(len(args))
# #     if len(progress.task_args) > 0 and (args != "()"):
# #         print("args")
# #         print(eval(progress.task_args))
# #         args = progress.task_args.split(",")
# #
# #         count = args[3]
# #         print("success from task: %", id_task)
# #         date_event = timezone.now()
# #         date_event = str(date_event)
# #         children = json.loads(t.meta)
# #         total = len(children["children"])
# #         params = t.task_kwargs
# #         total_succeeded = count
# #         total_failures = total - int(count)
# #
# #         task_kwargs = json.loads(params)
# #         task_kwargs = task_kwargs.replace('"', "'").replace("'", '"').replace("None", '""')
# #         print(task_kwargs)
# #         task_kwargs = json.loads(task_kwargs)
# #
# #         if "periode" in task_kwargs:
# #             if task_kwargs["periode"] and not isinstance(task_kwargs["periode"], type(None)):
# #                 periode = task_kwargs["periode"]
# #         else:
# #             periode = ""
# #         if "numbers" in task_kwargs:
# #             numbers = task_kwargs["numbers"]
# #         else:
# #             numbers = ""
# #
# #         data_task = {
# #             "uid": id_task,
# #             "date": date_event,
# #             "collection": task_kwargs["pid"],
# #             "title": task_kwargs["title"],
# #             "username": task_kwargs["username"],
# #             "periode": ",".join(map(str, periode)),
# #             "numbers": ",".join(map(str, numbers)),
# #             "total": total,
# #             "success": total_succeeded,
# #             "failures": total_failures,
# #             "progress": int(total_failures / total),
# #         }
# #         redis_conn.xadd("task:import", data_task)
# #         command_tasks = redis_conn.xrevrange("task:import", max="+", min="-", count=1)
# #         current_command = command_tasks[0]
# #         parent_id = str.strip(args[1])
# #
# #     insert_history_event(
# #         {
# #             "type": "import",
# #             "parent_id": parent_id,
# #             "pid": task_kwargs["pid"],
# #             "col": task_kwargs["col"],
# #             "status": "ERROR",
# #             "title": task_kwargs["title"],
# #             "type_error": "erreur d'execution de la tâche d'import ",
# #             "userid": task_kwargs["userid"],
# #             "data": {
# #                 "ids_count": 0,
# #                 "message": progress.result,
# #                 "target": args,
# #             },
# #             "source": "",
# #         }
# #     )
# #
# #     print("get last inserted command")
# #     print(current_command)
#
#
# @app.task(name="import_oai")
# def import_oai(id_harvest=None, resumption_token=None):
#     if id_harvest:
#         harvests = [OAI_harvest.objects.get(pk=id_harvest)]
#     else:
#         harvests = OAI_harvest.objects.filter(one_shot=False)
#     for harvest in harvests:
#         print(f"Start harvest for {harvest}")
#         import_site = ImportSite(harvest, resumption_token=resumption_token)
#         import_site.parse()
#         obj = Gdml_log(msg="Finished harvesting", source=harvest)
#         obj.save()
#
#
# @app.task(name="import_oai_collection_log")
# def import_oai_collection_log(id_harvest=None):
#     harvest = OAI_harvest.objects.get(pk=id_harvest)
#     logs = Gdml_log.objects.filter(Q(source=harvest) & Q(oai_identifier_isnull=False))
#     for log in logs:
#         import_site = ImportSite(oai_harvest=harvest, article_oai_id=log.oai_identifier)
#         ret = import_site.parse()
#         if ret:
#             log.delete()
#
#     obj = Gdml_log(msg="Finished log re import", source=harvest)
#     obj.save()
#
#
# @app.task(name="remove_articles")
# def remove_articles(id):
#     if id:
#         qs = OAI_article.objects.filter(harvest_source__id=id)
#     else:
#         qs = OAI_article.objects.all()
#
#     for a in qs:
#         cmd = addArticlePtfCmd({"pid": a.pid})
#         cmd.set_container(a.my_container)
#         cmd.set_provider(a.provider)
#         cmd.undo()
#         a.delete()
#
#
# @app.task(name="remove_lonely_container")
# def remove_lonely_container():
#     exclude = ["GDML_Books", "GDML_Articles"]
#
#     for j in Collection.objects.all():
#         if j.content.count() == 0:
#             if j.pid not in exclude:
#                 print(j)
#                 cmd = addCollectionPtfCmd({"pid": j.pid})
#                 cmd.set_provider(j.provider)
#                 cmd.undo()
