function filterBySource(){
    $(".journal-grid").hide();
    let source_id = $( "#source-select" ).val();
    $(".journal-grid")
        .filter(function () {
            var data = $(this).attr("data-source");
            let sources = data.split(",");
            var result = source_id == "ALL" || sources.includes(source_id);
            return result;
        })
        .show();
}

$(document).ready(function () {
    $(window).on("scroll", function(e) {
        if(window.scrollY > 0){
            $('.navbar-brand').addClass('tiny');
        }
        else{
            $('.navbar-brand').removeClass('tiny');
        }
    });

    $( "#source-select" ).on( "change", function() {
        filterBySource();
    });
});
