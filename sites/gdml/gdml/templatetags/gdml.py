import re

from django import template

from gdml.models import Formats
from gdml.models import OAI_article
from gdml.models import SourceName

register = template.Library()


@register.filter
def source_name(link):
    reg_source = re.compile(r"(?P<domain>([a-zA-Z0-9.-]+){1,5})(?P<authority>(\.[a-zA-Z0-9]+){1})")
    source = reg_source.search(link)
    if source:
        domain = source.group("domain") + source.group("authority")
        domain = re.sub("www.", "", domain)
        source_name = domain.upper().replace(".", "_").replace("-", "_")
        try:
            source_name = SourceName[source_name].value
            return source_name
        except Exception as ex:
            print(ex)
            return ""


@register.filter
def get_links(article):
    ret = []
    if article and article.datastream_set.count() > 0:
        ret += list(article.datastream_set.all())

    oai_article = None
    try:
        if article:
            oai_article = article.oai_article
    except OAI_article.DoesNotExist:
        pass

    if oai_article and oai_article.harvest_source.xml_format == Formats.OAI_DC:
        ret.append(
            {
                "location": article.extlink_set.first().location,
                "text": f"Text on {article.oai_article.harvest_source}",
            }
        )
        if article.provider.name == "euclid":
            end_url = article.extlink_set.first().location.split("/")
            ret.append(
                {
                    "location": f"https://projecteuclid.org/download/pdf_1/{end_url[-2]}/{end_url[-1]}",
                    "text": "PDF",
                }
            )
            ret.append(
                {
                    "location": f"https://projecteuclid.org/download/pdfview_1/{end_url[-2]}/{end_url[-1]}",
                    "text": "Alt PDF",
                }
            )

    return ret


@register.filter
def get_data_source(collection):
    if collection is None:
        return ""

    data_source = ""

    for period in collection.periods.all():
        if data_source:
            data_source += ","
        data_source += period.source.domain

    return data_source


@register.filter
def get_col_type(obj):
    collection_type = ""

    if obj:
        document_type = obj.get_document_type()

        if document_type in ["Article de revue", "Revue"]:
            collection_type = "journal"
        elif document_type in ["Acte de séminaire", "Séminaire"]:
            collection_type = "acta"
        elif document_type == "Thèse":
            collection_type = "thesis"
        elif document_type == "Notes de cours":
            collection_type = "lecture-notes"
        elif document_type in ["Acte de rencontre", "Actes de rencontres"]:
            collection_type = "proceedings"
        elif document_type in ["Chapitre de livre", "Livre"]:
            collection_type = "book"

    return collection_type
