# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-04-15 13:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gdml', '0002_auto_20190415_0944'),
    ]

    operations = [
        migrations.AddField(
            model_name='oai_harvest',
            name='url_home',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='oai_harvest',
            name='url',
            field=models.CharField(max_length=50),
        ),
    ]
