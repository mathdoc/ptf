# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-04-11 14:33
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import enumfields.fields
import gdml.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('ptf', '0027_auto_20190409_1238'),
    ]

    operations = [
        migrations.CreateModel(
            name='Gdml_log',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('msg', models.TextField()),
                ('trace', models.TextField(blank=True, null=True)),
                ('create_datetime', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('collection', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ptf.Collection')),
            ],
        ),
        migrations.CreateModel(
            name='OAI_article',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('oai_identifier', models.CharField(max_length=50, unique=True)),
                ('article', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ptf.Article')),
            ],
        ),
        migrations.CreateModel(
            name='OAI_harvest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url', models.CharField(max_length=50, unique=True)),
                ('xml_format_spec', models.CharField(max_length=32)),
                ('xml_format', enumfields.fields.EnumField(blank=True, enum=gdml.models.Formats, max_length=10, null=True, verbose_name='Type')),
                ('lang', models.CharField(default='en', max_length=20)),
                ('last_harvest', models.DateField(default=django.utils.timezone.now)),
                ('collection', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ptf.Collection')),
                ('provider', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ptf.Provider')),
            ],
        ),
        migrations.AddField(
            model_name='oai_article',
            name='harvest_source',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gdml.OAI_harvest'),
        ),
    ]
