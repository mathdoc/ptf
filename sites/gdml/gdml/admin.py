from import_export.admin import ImportExportModelAdmin

from django import forms
from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.models import Permission
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.urls import reverse
from django.utils.html import format_html

from gdml.models import Gdml_log
from gdml.models import OAI_article
from gdml.models import OAI_Book
from gdml.models import OAI_harvest
from ptf.models import Article
from ptf.models import Container
from ptf.models import Contrib
from ptf.models import ContribGroup
from ptf.models import ExtLink
from ptf.models import Provider
from ptf.models import PtfSite
from ptf.models import Publisher
from ptf.models import ResourceId


class GeodesicAdminSite(admin.AdminSite):
    site_header = "Administration de Géodesic"
    site_title = "Administration de Géodesic"

    def get_app_list(self, request):
        """
        Here, we just re-order the models for geodesic in admin interface
        """

        app_dict = self._build_app_dict(request)

        # Sort the apps alphabetically.
        app_list = sorted(app_dict.values(), key=lambda x: x["name"].lower())

        # Sort the models alphabetically within each app.
        for app in app_list:
            app["models"].sort(key=lambda x: x["name"])

        return app_list

    def add_view_permissions(sender, **kwargs):
        """
        This syncdb hooks takes care of adding a view permission too all our
        content types.
        """
        # for each of our content types
        for content_type in ContentType.objects.all():
            # build our permission slug
            codename = "view_%s" % content_type.model

            # if it doesn't exist..
            if not Permission.objects.filter(content_type=content_type, codename=codename):
                # add it
                Permission.objects.create(
                    content_type=content_type,
                    codename=codename,
                    name="Can view %s" % content_type.name,
                )
                print
                "Added view permission for %s" % content_type.name

    # check for all our view permissions after a syncdb

    def index(self, request, extra_context=None):
        # Update extra_context with new variables
        return super().index(request, extra_context)


geodesic_admin = GeodesicAdminSite(name="admin")


class IdentifierInline(admin.TabularInline):
    model = ResourceId


class ExtLinkInline(admin.StackedInline):
    model = ExtLink


class SerialAdmin(admin.ModelAdmin):
    inlines = [ExtLinkInline]
    ordering = ["title_sort"]


class GroupAdminForm(forms.ModelForm):
    """
    ModelForm that adds an additional multiple select field for managing
    the users in the group.
    """

    users = forms.ModelMultipleChoiceField(
        User.objects.all(),
        widget=admin.widgets.FilteredSelectMultiple("Users", False),
        required=False,
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance.pk:
            initial_users = self.instance.user_set.values_list("pk", flat=True)
            self.initial["users"] = initial_users

    def save(self, *args, **kwargs):
        kwargs["commit"] = True
        return super().save(*args, **kwargs)

    def save_m2m(self):
        self.instance.user_set.clear()
        self.instance.user_set.add(*self.cleaned_data["users"])


@admin.register(Group, site=geodesic_admin)
class GroupAdmin(admin.ModelAdmin):
    model = Group
    form = GroupAdminForm
    filter_horizontal = ("permissions",)


@admin.register(User, site=geodesic_admin)
class UserAdmin(admin.ModelAdmin):
    model = User
    filter_horizontal = (
        "groups",
        "user_permissions",
    )


"""
@admin.register(Collection, site=geodesic_admin)
class CollectionAdmin(ImportExportModelAdmin):
    model = Collection
    search_fields = ("pid", "title_tex", "title_html")


admin.site.register(Collection)
"""


@admin.register(Container, site=geodesic_admin)
class ContainerAdmin(admin.ModelAdmin):
    model = Container
    search_fields = [
        "pid",
    ]


admin.site.register(Container)


@admin.register(Article, site=geodesic_admin)
class ArticleAdmin(admin.ModelAdmin):
    search_fields = ("pid",)
    list_display = (
        "provider",
        "pid",
        "doi",
        "title_html",
        "date_published",
        "date_pre_published",
    )
    list_display_links = (
        "title_html",
        "pid",
    )
    search_fields = ("title_html", "pid", "id")

    change_list_template = "back-end/django-admin/remove_all_articles.html"


@admin.register(Provider, site=geodesic_admin)
class ProviderAdmin(admin.ModelAdmin):
    model = Provider


@admin.register(Publisher, site=geodesic_admin)
class PublisherAdmin(admin.ModelAdmin):
    model = Publisher


@admin.register(ExtLink, site=geodesic_admin)
class ExtLinkAdmin(admin.ModelAdmin):
    model = ExtLink


@admin.register(OAI_harvest, site=geodesic_admin)
class OAI_harvestAdmin(admin.ModelAdmin):
    model = OAI_harvest


@admin.register(OAI_article, site=geodesic_admin)
class OAI_articleAdmin(admin.ModelAdmin):
    model = OAI_article


@admin.register(OAI_Book, site=geodesic_admin)
class OAI_BookAdmin(admin.ModelAdmin):
    model = OAI_Book


@admin.register(ContribGroup, site=geodesic_admin)
class ContribGroupAdmin(admin.ModelAdmin):
    model = ContribGroup


@admin.register(Contrib, site=geodesic_admin)
class ContribAdmin(admin.ModelAdmin):
    model = Contrib


@admin.register(PtfSite, site=geodesic_admin)
class PtfSiteAdmin(ImportExportModelAdmin):
    model = PtfSite


@admin.register(Gdml_log)
class GdmlLogAdmin(admin.ModelAdmin):
    list_display = ("source", "msg", "create_datetime", "gdmllog_actions")
    list_filter = ("source",)
    change_list_template = "back-end/django-admin/remove_all_logs.html"

    @admin.display(description="Redo import")
    def gdmllog_actions(self, obj):
        return format_html(
            '<a class="btn btn-primary" href="{}">Redo OAI import (this article)</a>&nbsp;',
            '<a class="btn btn-primary" href="{}">Redo OAI import Batch Collection</a>&nbsp;',
            reverse(
                "import_oai_one_article",
                kwargs={"harvest_id": obj.source.id, "oai_id": obj.oai_identifier},
            ),
            reverse("import_oai_collection_log", kwargs={"harvest_id": obj.source.id}),
        )
