## Processus d'import

Fonctionnement :

    • Processus en deux étapes:
     - création des collections et périodes associées
     - tâche/process d'aspiration

    • Intégration de redis et enregistrement des erreur dans l’historique

Lancement des processus :

moissonnage EUDML :
cf (crawl) : [crawl](../../../apps/crawl/README.md)

Historique et état des processus de moissonnage :

Pour une commande, un hash redis peut être créé, afin d'avoir son status :

> redis_client.exec_cmd(

    r.hset, "last_task_import",

    mapping={
    'id': self.uuid,
    'username': self.username,
    'collection': collection.pid,
    'progress': self.bar.numerator
    })

Pour suivre l'avancement d'un process de moissonnage, des streams (redis) sont enregistrés.

> data_task = {
            "date": date_event,
            "collection": "AA",
            "username": self.username,
            "total": total,
            "failures": 0,
            "progress": 20,
        }

> self.redis_client.exec_cmd(r.xadd, "import:EUDML", data_task)

Chaque commande peut être historisée avec ce fonctionnement.


[redis streams](https://redis.io/docs/data-types/streams/)

> Enregistrement de la commande dans l’historique


Une commande effectuée (tâche parente) est enregistrée dans l'historique et comporte des évènements enfants.


![alt text](static/gdml/img/historique_commande.png "Dernière commande")


