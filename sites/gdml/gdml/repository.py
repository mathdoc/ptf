from django.conf import settings

from gdml.settings import OAI_HOST
from oai import oai_helpers
from oai.repository import OAIRepository
from ptf import model_helpers
from ptf.models import SiteMembership


class GdmlRepository(OAIRepository):
    def Identify(self):
        return {
            "name": "Geodesic",
            "base_url": OAI_HOST,
            "protocol_version": "2.0",
            "admin_email": "mersenne@listes.mathdoc.fr",
            "earliest_datestamp": SiteMembership.objects.filter(site__id=settings.SITE_ID)
            .order_by("deployed")
            .first()
            .deployed.strftime("%Y-%m-%d"),
            "deleted_record": "no",
            "granularity": "YYYY-MM-DD",
            "repositoryIdentifier": "gdml",
            "delimiter": ":",
            "sample_identifier": "oai:gdml:JEP_2018__1_1_1_0",
        }

    def has_set(self, setspec):
        col = model_helpers.get_collection(setspec)
        return bool(col)

    def listsets(self):
        collections = oai_helpers.get_collections()
        return [collections]


def get_repository(base_url):
    # thismodule = sys.modules[__name__]
    # repo = settings.REPOSITORIES[base_url]
    # repoClass = getattr(thismodule, repo)
    return GdmlRepository(base_url)
