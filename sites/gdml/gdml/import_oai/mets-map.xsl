<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet 
  version="1.0" 
  xmlns:METS="http://www.loc.gov/METS/" 
  xmlns:mets="http://www.loc.gov/METS/" 
 xmlns:MODS="http://www.loc.gov/mods/v3"
 xmlns:mods="http://www.loc.gov/mods/v3"
 xmlns:xlink="http://www.w3.org/1999/xlink" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
exclude-results-prefixes="xsi mods xlink mets METS MODS"
>
<xsl:output method="xml" indent="yes"/>

<xsl:template match="mets">
<thumbnail-group>
<xsl:apply-templates select="mets:structMap[@TYPE='PHYSICAL']"/>
</thumbnail-group>
</xsl:template>

<xsl:template match="mets:structMap[@TYPE='PHYSICAL']">
<xsl:apply-templates select="mets:div/mets:div"/>
</xsl:template>

<xsl:template match="mets:div/mets:div">
<thumbnail seq="{@ORDER}">
<xsl:apply-templates select="mets:fptr[4]"/>
</thumbnail> 
</xsl:template>

<xsl:template match="mets:fptr">
<xsl:variable name="fileid" select="@FILEID"/>
<!-- <file id="{@FILEID}"/> -->
<xsl:apply-templates select="/mets/mets:fileSec/mets:fileGrp[@USE='THUMBS']/mets:file[@ID=$fileid]"/>
</xsl:template>

<xsl:template match="mets:file">
<link type="{@MIMETYPE}" rel="thumbnail" href="{mets:FLocat/@xlink:href}"/>
</xsl:template>
</xsl:stylesheet>
