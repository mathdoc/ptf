<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet
  version="1.0"
  xmlns:METS="http://www.loc.gov/METS/"
  xmlns:mets="http://www.loc.gov/METS/"
 xmlns:MODS="http://www.loc.gov/mods/v3"
 xmlns:mods="http://www.loc.gov/mods/v3"
 xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns="http://jats.nlm.nih.gov"
exclude-result-prefixes="METS MODS xlink xsi"
>

<xsl:output method="xml" omit-xml-declaration="no" indent="yes"/>
<xsl:variable name="dmdvolume" select='//mets:structMap[@TYPE="LOGICAL"]/mets:div/mets:div[@TYPE="PeriodicalVolume"]/@DMDID'/>
<xsl:variable name="ppnjournal" select='//METS:dmdSec[@ID=$dmdvolume]//MODS:relatedItem[@type="host"]/MODS:identifier[@type="PPN"]/text()'/>
<xsl:variable name="ppnissue" select='/METS:mets/mets:dmdSec[@ID=$dmdvolume]//mods:mods/mods:recordInfo/mods:recordIdentifier/text()'/>
<xsl:variable name="path" select="toto.xml"/>
<xsl:variable name="jdata" select="document($path)"/>
<xsl:variable name="journal">
<journal-meta>
<journal-id journal-id-type="gdz-id"><xsl:value-of select='$ppnjournal'/>
</journal-id>
<journal-title-group>
<journal-title>
<xsl:value-of select="$jdata//MODS:title/text()"/>
</journal-title>
</journal-title-group>

</journal-meta>
</xsl:variable>
<xsl:variable name="year" select="//METS:mdWrap[1]//MODS:dateIssued/text()"/>
<xsl:template match="/">
<nlmmeta>
<xsl:apply-templates select='//METS:mets/METS:structMap[@TYPE="LOGICAL"]//METS:div[@TYPE="Article"]|//METS:mets/METS:structMap[@TYPE="LOGICAL"]//METS:div[@TYPE="Errata"]'/>
</nlmmeta>
</xsl:template>

  <xsl:template match='METS:mets/METS:structMap[@TYPE="LOGICAL"]//METS:div[@TYPE="Article"]|//METS:mets/METS:structMap[@TYPE="LOGICAL"]//METS:div[@TYPE="Errata"]'>

<xsl:variable name="dmdid"><xsl:value-of select="@DMDID"/></xsl:variable>
<xsl:variable name="flogid" select="@ID"/>
<xsl:variable name="logid"><xsl:value-of select="concat('LOG_', format-number(translate(@ID, translate(@ID, '0123456789', ''), ''), '0000'))"/></xsl:variable>
<xsl:variable name="href"><xsl:value-of select='//METS:mets/METS:dmdSec[@ID=$dmdid]//MODS:identifier/text()'/>
</xsl:variable>
<!--- <xsl:document href="{$href}.xml"> -->

<article
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	 xsi:schemaLocation="http://jats.nlm.nih.gov http://eudml.org/schema/2.0/eudml-article-2.0.xsd">
<!--
<xsl:if test="/METS:mets/METS:dmdSec[@ID=$dmdid]//MODS:languageTerm/text()">
<xsl:attribute name="xml:lang"><xsl:value-of select='/METS:mets/METS:dmdSec[@ID=$dmdid]//MODS:languageTerm/text()'/>
</xsl:attribute>
</xsl:if>
-->
<front>
<xsl:copy-of select="$journal"/>

<article-meta>

  <xsl:apply-templates select="//METS:mets/METS:dmdSec[@ID=$dmdid]"/>
<xsl:apply-templates select='ancestor::METS:div[@TYPE="PeriodicalVolume"]'/>
<!--
<xsl:apply-templates select='parent::METS:div[@TYPE="PeriodicalIssue"]'/>
-->
<fpage><xsl:apply-templates select='//METS:mets/METS:structLink/METS:smLink[@xlink:from=$flogid][1]'/></fpage>
<lpage>
<xsl:apply-templates select='//METS:mets/METS:structLink/METS:smLink[@xlink:from=$flogid][last()]'/>
</lpage>
<xsl:apply-templates select='//METS:mets/METS:dmdSec[@ID=$dmdid]/mets:mdWrap//mods:mods/MODS:identifier[@type="Zbl"]'/>
<ext-link>
<xsl:attribute name="ext-link-type">eudml-fulltext:application/pdf</xsl:attribute>
<xsl:attribute name="specific-use">index,enhance</xsl:attribute>
<xsl:attribute name="xlink:href">http://gdz-srv1.sub.uni-goettingen.de/gcs/gcs?action=pdf&amp;pagesize=A4&amp;metsFile=<xsl:value-of select="$ppnissue"/>&amp;divID=<xsl:value-of select="$logid"/>&amp;targetFileName=<xsl:value-of select="concat($ppnissue,'_', $logid,'.pdf')"/></xsl:attribute>MetaFull (PDF)</ext-link>
<xsl:choose>
<xsl:when test='//METS:mets/METS:dmdSec[@ID=$dmdid]/mets:mdWrap//mods:mods/MODS:identifier[@type="PPN"]'>
<self-uri><xsl:attribute name="xlink:href">http://gdz.sub.uni-goettingen.de/dms/resolveppn/?PPN=<xsl:value-of select='//METS:mets/METS:dmdSec[@ID=$dmdid]/mets:mdWrap//mods:mods/MODS:identifier[@type="PPN"]'/></xsl:attribute>Access to full text</self-uri>
</xsl:when>
<xsl:otherwise>
<self-uri><xsl:attribute name="xlink:href">http://gdz.sub.uni-goettingen.de/dms/load/img/?PPN=<xsl:value-of select="$ppnissue"/>&amp;DMDID=<xsl:value-of select="$dmdid"/></xsl:attribute>Access to full text</self-uri>
</xsl:otherwise>
</xsl:choose>
<custom-meta-group>
<custom-meta>
<meta-name>provider</meta-name>
<meta-value>gdz</meta-value>
</custom-meta>
</custom-meta-group>
</article-meta>
</front>
</article>

<!-- </xsl:document> -->

  </xsl:template>

  <xsl:template match='//METS:mets/METS:structLink/METS:smLink'>
<xsl:variable name="phys"><xsl:value-of select="@xlink:to"/></xsl:variable>
<xsl:apply-templates select='//METS:structMap[@TYPE="PHYSICAL"]/METS:div/METS:div[@ID=$phys]'/>
</xsl:template>

<xsl:template match="METS:dmdSec">
<xsl:apply-templates select="METS:mdWrap//MODS:mods"/>
</xsl:template>
<xsl:template match="MODS:mods">
<xsl:choose>
<xsl:when test='MODS:identifier[@type="PPN"]'>
<xsl:apply-templates select='MODS:identifier[@type="PPN"]'/>
</xsl:when>
<xsl:otherwise>
<article-id pub-id-type="gdz-id"><xsl:value-of select="$ppnissue"/>+<xsl:value-of select="ancestor::mets:dmdSec/@ID"/></article-id>
</xsl:otherwise>
</xsl:choose>
<xsl:if test="mods:titleInfo">
<title-group>
<article-title>
<xsl:apply-templates select="MODS:titleInfo[not(@type) and not(@transliteration)]"/>
</article-title>
<xsl:apply-templates select='mods:titleInfo[@transliteration="true"]'/>
<xsl:apply-templates select='mods:titleInfo[@type="translated"]'/>
</title-group>
</xsl:if>
<xsl:if test="MODS:name">
<contrib-group content-type="authors">
<xsl:apply-templates select="MODS:name"/>
</contrib-group>
</xsl:if>
<pub-date>
<year><xsl:value-of select="$year"/></year>
</pub-date>

</xsl:template>
<xsl:template match='MODS:identifier[@type="PPN"]'>
<article-id pub-id-type="gdz-id"><xsl:value-of select="text()"/></article-id>

</xsl:template>
<xsl:template match='MODS:identifier[@type="Zbl"]'>
<ext-link ext-link-type="zbl-item-id"><xsl:attribute name="xlink:href">http://www.zentralblatt-math.org/zmath/en/search/?q=an:<xsl:value-of select="text()"/></xsl:attribute><xsl:value-of select="text()"/></ext-link>

</xsl:template>
<xsl:template match="MODS:mods/MODS:titleInfo[not(@type) and not(@transliteration)]">
<xsl:value-of select="mods:title/text()"/>
</xsl:template>
<xsl:template match="MODS:mods/MODS:name">
<contrib>
<xsl:if test='MODS:role/MODS:roleTerm/text()="aut"'>
<xsl:attribute name="contrib-type">author</xsl:attribute>
</xsl:if>
<name>
<xsl:apply-templates select='MODS:namePart[@type="family"]'/>
<xsl:apply-templates select='MODS:namePart[@type="given"]'/>
</name>
</contrib>
</xsl:template>
<xsl:template match='MODS:namePart[@type="family"]'>
<surname><xsl:value-of select="text()"/></surname>
</xsl:template>
<xsl:template match='MODS:namePart[@type="given"]'>
<given-names><xsl:value-of select="text()"/></given-names>
</xsl:template>
<xsl:template match='METS:structMap[@TYPE="PHYSICAL"]/METS:div/METS:div'>
<xsl:value-of select="@ORDERLABEL"/>
</xsl:template>
<xsl:template match='mods:titleInfo[@type="translated"]'>
<trans-title-group>
<trans-title>
<xsl:value-of select="mods:title/text()"/>
</trans-title>
</trans-title-group>
</xsl:template>
<xsl:template match='mods:titleInfo[@transliteration="true"]'>
<alt-title alt-title-type="transliteration">
<xsl:value-of select="mods:title/text()"/>
</alt-title>
</xsl:template>
<xsl:template match='METS:div[@TYPE="PeriodicalIssue"]'>
<xsl:variable name="dmdid" select="@DMDID"/>
<xsl:call-template name="issue">
<xsl:with-param name="dmdsec" select='//METS:dmdSec[@ID=$dmdid]'/>
</xsl:call-template>
</xsl:template>
<xsl:template name="issue">
<xsl:param name="dmdsec"/>
<xsl:if test="$dmdsec">
<issue-id pub-id-type="gdz-id"><xsl:value-of select="$dmdsec/METS:mdWrap//MODS:identifier/text()"/></issue-id>
</xsl:if>
</xsl:template>
<xsl:template match='METS:div[@TYPE="PeriodicalVolume"]'>
<xsl:variable name="dmdid" select="@DMDID"/>
<xsl:call-template name="volume">
<xsl:with-param name="dmdsec" select='//METS:dmdSec[@ID=$dmdid]'/>
</xsl:call-template>
</xsl:template>
<xsl:template name="volume">
<xsl:param name="dmdsec"/>
<volume><xsl:value-of select="$dmdsec//MODS:part/MODS:detail/MODS:number/text()"/></volume>
<volume-id pub-id-type="gdz-id"><xsl:value-of select='$dmdsec//MODS:mods/MODS:identifier[@type="PPN"]/text()'/></volume-id>
<volume-id pub-id-type="uri"><xsl:value-of select='$dmdsec//MODS:mods/MODS:identifier[@type="purl"]/text()'/>
</volume-id>
</xsl:template>
<xsl:template match='MODS:originInfo/MODS:publisher'>
<publisher-name><xsl:value-of select="text()"/></publisher-name>
</xsl:template>
</xsl:stylesheet>
