import getopt
import os
import sys

from lxml import etree


def usage():
    print("transform [-f file]")
    print("or transform <mathematica|rusdml>")
    sys.exit(1)


try:
    optval, args = getopt.getopt(sys.argv[1:], "f:")
except getopt.GetoptError:
    usage()
if optval and args:
    usage()
if optval:
    name = optval[0][1]
    subdir = name.split("/")[0]
    names = [name]
elif len(args) > 1 or not args:
    usage()
else:
    what = args[0]
    dirmap = {
        "mathematica": "Mathematica",
        "math": "Mathematica",
        "rusdml": "Rusdml",
        "rus": "Rusdml",
    }
    try:
        subdir = dirmap[what]
    except:
        usage()
    names = []
    l = os.listdir(subdir)
    for name in l:
        dpath = f"{subdir}/{name}"
        ll = os.listdir(dpath)
        for n in ll:
            names.append(f"{dpath}/{n}")
xsls = {"Mathematica": "mets.xsl", "Rusdml": "rus1.xsl"}

errors = open("transform_%s.err" % subdir, "w")
xsl = xsls["Mathematica"]

xml_namespace = "http://www.w3.org/XML/1998/namespace"
XML = "{%s}" % xml_namespace
nsmap = {"xml": xml_namespace}
total = 0
f = open(xsl)
xslt_doc = etree.parse(f)
transform = etree.XSLT(xslt_doc)
count = len(names)
errs = 0
jats_namespace = "http://jats.nlm.nih.gov"
for name in names:
    print("File: %s" % name)
    path = name
    g = open(path)
    doc = etree.parse(g)
    try:
        tree = transform(doc)
    except Exception as e:
        print(e)
        errors.write(str(e) + "\n")
        errs += 1
        continue
    xp = "e:article[1]/e:front/e:journal-meta/e:journal-id"
    # try:
    ppn_j = tree.xpath(xp, namespaces={"e": jats_namespace})
    # except:
    #   errors.write('File:%s, peux pas trouver PPN journal\n' % name)
    #    errs += 1
    #    continue
    if not ppn_j:
        errors.write("File:%s, peux pas trouver PPN journal\n" % name)
        continue
    ppn_j = ppn_j[0].text
    print("Processing %s" % ppn_j)
    outdir = f"Transformed/{subdir}/{ppn_j}"
    if os.path.isdir(outdir):
        pass
    else:
        os.mkdir(outdir)
    try:
        ppn_v = tree.xpath(
            (
                "e:article[1]/e:front"
                "/e:article-meta/e:volume-id"
                '[@pub-id-type="gdz-id"]/text()'
            ),
            namespaces={"e": jats_namespace},
        )
    except:
        errors.write("File:%s, pas articles")
        errs += 1
        continue
    if not ppn_v:
        errors.write("File:%s, peux pas trouver PPN volume\n" % name)
        errs += 1
        continue

    ppn_v = ppn_v[0]

    print("    %s" % ppn_v)

    contribs = tree.xpath(
        ("e:article[1]/e:front" "/e:article-meta/e:contrib-group/e:contrib"),
        namespaces={"e": jats_namespace},
    )
    if len(contribs) > 2:
        errors.write(f"MULTIPLE:{ppn_j} {ppn_v}\n")

    if subdir == "Rusdml":
        nodes = tree.xpath("e:article", namespaces={"e": jats_namespace})
        for n in nodes:
            lang = n.get("%slang" % XML)
            if lang.startswith("en."):
                n.set("%slang" % XML, "en")
            elif not lang or len(lang) > 2:
                n.set("%slang" % XML, "ru")
            meta = n.xpath("e:front/e:article-meta", namespaces={"e": jats_namespace})[0]
            zbl = meta.xpath(
                ('e:ext-link[@ext-link-type="zbl-item-id"]'), namespaces={"e": jats_namespace}
            )
            if zbl:
                zblid = zbl[0].text or ""
                if zblid:
                    py = zbl_year.get(zblid, "")
                    if py:
                        pub_date = etree.Element("{%s}pub-date" % jats_namespace)
                        year = etree.Element("{%s}year" % jats_namespace)
                        year.text = py
                        pub_date.append(year)
                        contribs = meta.xpath(
                            ("e:contrib-group"), namespaces={"e": jats_namespace}
                        )
                        if contribs:
                            c = contribs[0]
                            i = meta.index(c)
                            meta.insert(i + 1, pub_date)
        kwdgs = tree.xpath(
            ("e:article/e:front" "/e:article-meta/e:kwd-group[not(@kwd-group-type)]"),
            namespaces={"e": jats_namespace},
        )
        for kwdg in kwdgs:
            cyrkwd = []
            for e in kwdg.iterchildren():
                t = e.text.strip()
                ch1 = t[-1]
                ch0 = t[0]
                if (ord(ch0) >> 8 == 4) or (ord(ch1) >> 8) == 4:
                    cyrkwd.append(e)

            if len(cyrkwd) == len(kwdg):
                kwdg.set("%slang" % XML, "ru")
                continue
            if not cyrkwd:
                kwdg.set("%slang" % XML, "en")
                continue
            cyrkwdg = etree.Element("kwd-group")
            cyrkwdg.set("%slang" % XML, "ru")

            for e in cyrkwd:
                kwdg.remove(e)
            for e in cyrkwd:
                cyrkwdg.append(e)
            kwdg.set("%slang" % XML, "en")
            p = kwdg.getparent()
            i = p.index(kwdg)
            p.insert(i, cyrkwdg)

    s = etree.tostring(tree.getroot(), encoding="utf-8", pretty_print=True)
    print("----------")
    f = open(f"{outdir}/{ppn_v}.xml", "w")
    f.write(s)
    print("e")
    total += 1
    if total % 10 == 0:
        print("%d/%d" % (total, count))
print(total, errs)
