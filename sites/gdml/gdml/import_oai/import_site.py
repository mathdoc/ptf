import logging
import os
import traceback

from sickle import Sickle

from django.db import IntegrityError
from django.utils.timezone import now

from gdml.models import Formats
from gdml.models import Gdml_log
from gdml.models import OAI_article
from gdml.models import OAI_Book
from ptf.cmds.dc_cmds import addArticleDc
from ptf.cmds.xml_cmds import addArticleXmlCmd
from ptf.cmds.xml_cmds import addBookXmlCmd
from ptf.exceptions import ResourceExists

db_logger = logging.getLogger("db")


class ImportSite:
    one_article_mode = False
    local_mode = False  # do we already have local stuff

    def __init__(self, oai_harvest, article_oai_id=None, resumption_token=None):
        self.sickle = Sickle(oai_harvest.url, max_retries=15)
        self.oai_harvest = oai_harvest
        self.article_oai_id = article_oai_id
        self.resumption_token = resumption_token

    def atomic_import(self, r, oai_id):
        raw = None
        try:
            if self.oai_harvest.xml_format == Formats.OAI_DC:
                raw = r.metadata if hasattr(r, "metadata") else r
                print(raw)
                print("parse oai dc")
                """xcollection = {"publisher": raw['publisher'], "title": raw["title"],"wall":0, "pid":"pid" }
                coll = Collection.objects.create(**xcollection)"""
                """
                    add collection to raw
                """
                cmd = addArticleDc(raw)
                cmd.set_eprint(self.oai_harvest.is_prepub)
                cmd.set_thesis(self.oai_harvest.is_thesis)
                cmd.set_lang(self.oai_harvest.lang)
            elif self.oai_harvest.xml_format in (Formats.JATS, Formats.NLM, Formats.JATS_JATS):
                print("parse jats")

                raw = r.raw if hasattr(r, "raw") else r
                cmd = addArticleXmlCmd(
                    {"body": raw, "xml_format": self.oai_harvest.xml_format_spec}
                )
            elif self.oai_harvest.xml_format == Formats.BOOK:
                raw = r.raw if hasattr(r, "raw") else r
                cmd = addBookXmlCmd({"body": raw, "xml_format": self.oai_harvest.xml_format_spec})
                cmd.set_import_oai_mode()

            cmd.set_provider(self.oai_harvest.provider)
            if self.oai_harvest.provider_col and type(cmd) != addBookXmlCmd:
                print("provider")
                p = self.oai_harvest.provider_col
                print(p)
                print("___")
                " ---> set_provider_col"
                """cmd.set_provider_col(self.oai_harvest.provider_col)"""
            item = None
            try:
                item = cmd.do()
            except (ValueError, IntegrityError, ResourceExists) as e:
                obj = Gdml_log(
                    msg=e.message,
                    trace=traceback.format_exc(),
                    source=self.oai_harvest,
                    oai_identifier=oai_id,
                    raw=raw,
                )
                obj.save()
            print("SUCESS")
            if item:
                kw = {"oai_identifier": oai_id, "harvest_source": self.oai_harvest}

                if self.oai_harvest.xml_format == Formats.BOOK:
                    kw["book"] = item
                    cls = OAI_Book
                else:
                    kw["article"] = item
                    cls = OAI_article

                oa = cls(**kw)
                oa.save()
                print("one mo item")
        except Exception as e:
            obj = Gdml_log(
                msg=e,
                trace=traceback.format_exc(),
                source=self.oai_harvest,
                oai_identifier=oai_id,
                raw=raw,
            )
            obj.save()

    def getListRecords(self, set):
        sickle = Sickle(self.oai_harvest.url)
        r = sickle.ListRecords(identifier=self.article_oai_id, metadataPrefix="oai_dc")
        return r

    def parse(self):
        # TODO add from
        """
        records = sickle.ListRecords(             **{'metadataPrefix': 'oai_dc',
             'from': '2012-12-12'
                         })
        """

        params = {"metadataPrefix": self.oai_harvest.xml_format.label}

        if self.article_oai_id:
            params["identifier"] = self.article_oai_id
            self.one_article_mode = True

        if self.oai_harvest.oai_set != "" and not self.one_article_mode:
            params["set"] = self.oai_harvest.oai_set

        if not self.one_article_mode:
            params["ignore_deleted"] = True

        # params['from'] = self.oai_harvest.last_harvest

        if self.resumption_token:
            params = {"resumptionToken": self.resumption_token}

        try:
            if self.one_article_mode:
                sickle = Sickle(self.oai_harvest.url)
                r = sickle.GetRecord(identifier=self.article_oai_id, metadataPrefix="oai_dc")
                print(r)
            kls = self.sickle.GetRecord if self.one_article_mode else self.sickle.ListRecords
            records = kls(**params)
        except Exception as e:
            obj = Gdml_log(msg=e, trace=traceback.format_exc(), source=self.oai_harvest)
            obj.save()
            return

        r = records.next() if not self.one_article_mode else records

        self.oai_harvest.last_harvest = now()
        self.oai_harvest.save()

        folder_harvest_exist = False
        path_folder_harvest = "/payload_oai/" + str(self.oai_harvest.id)
        folder_harvest_exist = os.path.exists(path_folder_harvest)
        if not folder_harvest_exist:
            os.mkdir(path_folder_harvest)
            folder_harvest_exist = True

        if not self.local_mode or not folder_harvest_exist:
            while r:
                """identifier_safe = r.header.identifier.replace("/", '\\')
                f = open('{p}/{r}.xml'.format(p=path_folder_harvest, r=identifier_safe), "w+")
                f.write(r.raw)
                f.close()
                """
                self.atomic_import(r, r.header.identifier)

                try:
                    r = records.next() if not self.one_article_mode else None
                    # break
                except Exception:
                    break
        """else:
            for file in os.listdir(path_folder_harvest):
                with open(path_folder_harvest + '/' + file, 'r') as f:
                    text = f.read()
                    parser = etree.XMLParser(
                        huge_tree=True, recover=True, remove_blank_text=True, remove_comments=True)
                    tree = etree.fromstring(text, parser=parser)
                    record = Record(tree)
                    self.atomic_import(record, file)
                    #break
        """
        return True
