from lxml import etree

xsl = "mets-map.xsl"
f = open(xsl)
xslt_doc = etree.parse(f)
transform = etree.XSLT(xslt_doc)
g = open("/home/bouqueta/Documents/nlo/mets.xml")
doc = etree.parse(g)
tree = transform(doc)
print("---")
val = etree.tostring(tree.getroot(), encoding="utf-8", pretty_print=True)


text_file = open("output.xml", "wb")
text_file.write(val)
text_file.close()
