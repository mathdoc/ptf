"""jep URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  re_path(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  re_path(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import include
    2. Add a URL to urlpatterns:  re_path(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.urls import include
from django.urls import path
from django.urls import re_path

# from gdml.backend_views import import_from_local
# from gdml.backend_views import remove_all_articles
# from gdml.backend_views import remove_all_logs
# from gdml.backend_views import remove_lonely_container
from gdml.views import AboutView
from gdml.views import GetRecordsOAISetView
from gdml.views import HomePageView
from gdml.views import ImportOAIView
from gdml.views import ImportOAIViewOneArticle
from gdml.views import ShowHarvestSource

from .admin import geodesic_admin

# from .views import GDMLLecturesView
from .views import GDMLActasView
from .views import GDMLBooksView
from .views import GDMLCollectionView
from .views import GDMLJournalsView
from .views import GDMLProceedingsView

# from .views import GDMLThesisView

urlpatterns = [
    path("", HomePageView.as_view(), name="home"),
    path("journals/", GDMLJournalsView.as_view(), name="journals"),
    path("actas/", GDMLActasView.as_view(), name="actas"),
    path("books/", GDMLBooksView.as_view(), name="books"),
    path("proceedings/", GDMLProceedingsView.as_view(), name="proceedings"),
    # re_path(r"^lectures/*$", GDMLLecturesView.as_view(), name="lectures"),
    # re_path(r"^thesis/*$", GDMLThesisView.as_view(), name="thesis"),
    path("collection/<path:jid>/", GDMLCollectionView.as_view(), name="collection-issues"),
    path("", include("ptf.urls")),
    path("", include("mersenne_cms.urls")),
    path("", include("oai.urls")),
    path("about/", AboutView.as_view(), name="about"),
    re_path(
        r"^import_oai/(?P<harvest_id>\w+|)/(?P<oai_set>[:\/\.\w]+)",
        GetRecordsOAISetView.as_view(),
        name="import_oai_set",
    ),
    re_path(r"^import_oai/(?P<harvest_id>\w+|)", ImportOAIView.as_view(), name="import_oai"),
    re_path(
        r"^import_oai_one_article/(?P<harvest_id>\w+|)/(?P<oai_id>[:\/\.\w]+)",
        ImportOAIViewOneArticle.as_view(),
        name="import_oai_one_article",
    ),
    re_path(
        r"^import_oai_collection_log/(?P<harvest_id>\w+|)",
        ImportOAIViewOneArticle.as_view(),
        name="import_oai_collection_log",
    ),
    re_path(r"^sources", ShowHarvestSource.as_view(), name="show_harvest_source"),
    re_path(r"^admin", geodesic_admin.urls),
    # re_path(
    #     r"^admin_gdml/import_collection_xml/", import_from_local, name="import_collection_xml"
    # ),
    # re_path(
    #     r"^admin_gdml/remove_all_articles/(?P<id_collection>\w+|)",
    #     remove_all_articles,
    #     name="remove_all_articles",
    # ),
    # re_path(r"^admin_gdml/remove_all_logs/", remove_all_logs, name="remove_all_logs"),
    # re_path(
    #     r"^admin_gdml/remove_lonely_container/",
    #     remove_lonely_container,
    #     name="remove_lonely_container",
    # ),
    path("backend/", include("backend.urls")),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
