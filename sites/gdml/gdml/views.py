import logging

from unidecode import unidecode

from django.http.response import HttpResponse
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.views.generic.base import TemplateView
from django.views.generic.base import View

from backend.serializers import SourceSerializer
from gdml.import_oai.import_site import ImportSite
from gdml.models import Gdml_log
from gdml.models import OAI_harvest

# from gdml.tasks import import_oai_collection_log
from ptf import model_helpers
from ptf.models import Article
from ptf.models import Collection
from ptf.models import Container
from ptf.views import ArticleView
from ptf.views import ContainerView
from ptf.views import IssuesView
from ptf.views import ItemViewClassFactory

from .models import Periode
from .models import Source

# import re


# Get an instance of a logger
logger = logging.getLogger("file")

from django.contrib.auth.models import Group


class HomePageView(TemplateView):
    template_name = "home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        sources = Source.objects.all()
        sources_list = []
        articles_count = Article.objects.all().count()
        sources_joined = "numdam"
        for source in sources:
            serializer = SourceSerializer(source)
            sources_list.append(serializer.data)
        number_internal_sources = sources_joined.split(",")
        context["size_sources"] = len(sources_list) + len(number_internal_sources)
        context["articles_count"] = articles_count
        context["sources"] = sources_list
        context["homepage"] = True
        return context


class ImportOAIView(View):
    def get(self, request, harvest_id, resumption_token=None, *args, **kwargs):
        h = OAI_harvest.objects.get(pk=harvest_id)
        if harvest_id:
            harvests = [h]
        else:
            harvests = OAI_harvest.objects.filter(one_shot=False)
        for harvest in harvests:
            print(f"Start harvest for {harvest}")
            import_site = ImportSite(harvest, resumption_token=resumption_token)
            import_site.parse()
            obj = Gdml_log(msg="Finished harvesting", source=harvest)
            obj.save()

        return HttpResponse(status=200)


class GetRecordsOAISetView(View):
    def get(self, request, harvest_id, *args, **kwargs):
        h = OAI_harvest.objects.get(id=harvest_id)
        set = self.kwargs["oai_set"]
        import_site = ImportSite(oai_harvest=h)
        records = import_site.getListRecords(set)
        r = records.next()
        results = []
        while r:
            try:
                r = records.next()
                results.append(r.raw)
                continue
                print(r)
            except Exception:
                break
        return HttpResponse(records.oai_responbse.xml, content_type="text/xml", status=200)


class ImportOAIViewOneArticle(View):
    def get(self, request, harvest_id, oai_id, *args, **kwargs):
        h = OAI_harvest.objects.get(id=harvest_id)
        import_site = ImportSite(oai_harvest=h, article_oai_id=oai_id)
        import_site.parse()

        return HttpResponse(status=200)


class ImportOAIViewOneArticleCollectionLog(View):
    def get(self, request, harvest_id, oai_id, *args, **kwargs):
        # import_oai_collection_log.delay(harvest_id)

        return HttpResponse(status=200)


class ShowHarvestSource(TemplateView):
    template_name = "blocks/source-list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        sources_list = []

        sources = Source.objects.all()

        for source in sources:
            data_source = {"name": source.name, "url": source.website, "documents": {}}

            for periode in source.source.all():
                periode_range = None
                if periode.begin and periode.end:
                    begin = periode.begin if periode.begin is not None else 0
                    end = periode.end if periode.end is not None else 0
                    periode_range = list(range(abs(begin), abs(end)))

                col = periode.collection

                # TODO: Add lecture-notes and thesis
                # for coltype in ["journal", "acta", "proceeding", "book"]:

                if col.coltype in ["journal", "acta", "proceedings"]:
                    qs = Article.objects.filter(my_container__my_collection=col)
                    if periode_range:
                        qs = qs.filter(my_container__year__in=periode_range)
                else:
                    qs = Container.objects.filter(my_collection=col)
                    if periode_range:
                        qs = qs.filter(year__in=periode_range)

                if col.coltype in data_source["documents"]:
                    data_source_by_coltype = data_source["documents"][col.coltype]
                else:
                    data_source_by_coltype = {
                        "type": col.coltype,
                        "items_count": 0,
                        "collections_count": 0,
                        "url": "",
                    }

                data_source_by_coltype["items_count"] += qs.count()
                data_source_by_coltype["collections_count"] += 1
                if data_source_by_coltype["collections_count"] > 1:
                    data_source_by_coltype["url"] = (
                        reverse("journals") + f"?source_id={source.domain}"
                    )
                else:
                    data_source_by_coltype["url"] = reverse(
                        "journal-issues", kwargs={"jid": col.pid}
                    )

                data_source["documents"][col.coltype] = data_source_by_coltype

            if len(data_source["documents"]) > 0:
                sources_list.append(data_source)

        context["sources"] = sources_list
        # context["harvests_books"] = OAI_harvest.objects.filter(xml_format=Formats.BOOK)
        return context


class AboutView(TemplateView):
    template_name = "about.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["about"] = True
        return context


class GDMLJournalsView(TemplateView):
    template_name = "blocks/journal-list.html"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.document_type = "journal"

    def get_collections_by_type(self):
        if self.document_type == "journal":
            qs = model_helpers.get_journals()
        elif self.document_type == "book":
            qs = model_helpers.get_collection_of_books()
        elif self.document_type == "acta":
            qs = model_helpers.get_actas()
        elif self.document_type == "lecture-notes":
            qs = model_helpers.get_lectures()
        elif self.document_type == "proceeding":
            qs = model_helpers.get_proceedings()
        elif self.document_type == "thesis":
            qs = model_helpers.get_collection_of_thesis()
        else:
            return [], []

        qs = qs.filter(parent=None).order_by("title_tex")
        list_collections = []

        groups = self.request.user.groups.values_list("name", flat=True)
        has_perm = False
        for g in groups:
            if g == "can_manage_resources":
                group_management = Group.objects.get(name="can_manage_resources")
                for permission in group_management.permissions.all():
                    if permission.codename == "change_collection":
                        has_perm = True

        for index, collection in enumerate(qs):
            containers = Container.objects.filter(my_collection=collection)
            if containers is not None and containers.count() > 0:
                # Olivier 2024/04/29: Show all collections till the European conference
                if True or has_perm is True:
                    list_collections.append(collection)
                else:
                    collection = Collection.objects.get(pid=collection.pid)
                    periode = Periode.objects.filter(collection=collection, published=True)

                    if len(periode) > 0:
                        list_collections.append(collection)

                    # if (not has_perm or len(groups) ==0):
                #     if containers > 0:
                #         # retrieve periode
                #         collection = Collection.objects.get(pid=collection.pid)
                #         periodes = Periode.objects.filter(collection=collection)
                #         for periode in periodes:
                #             if periode.published:
                #                 list_collections.append(collection)
                #
                # else:
                #     list_collections.append(collection)

        return qs, list_collections

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        source_id = self.request.GET.get("source_id", None)

        collections, list_collections = self.get_collections_by_type()

        context["document_type"] = self.document_type
        context["collections"] = list_collections
        context["sources"] = Source.objects.all()
        context["source_id"] = source_id

        letters = []
        for i in range(0, 26):
            index = chr(ord("A") + i)
            found = False
            # TODO: bug: collections is the original Queryset. it has been filtered in list_collections
            for collection in collections:
                letter = collection.title_tex[0]
                new_letter = unidecode(letter).upper()
                if new_letter == index:
                    found = True
                    break
            letters.append({"name": index, "found": found})
        context["letters"] = letters

        return context


class GDMLBooksView(GDMLJournalsView):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.document_type = "book"


class GDMLActasView(GDMLJournalsView):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.document_type = "acta"


class GDMLLecturesView(GDMLJournalsView):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.document_type = "lecture-notes"


class GDMLProceedingsView(GDMLJournalsView):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.document_type = "proceeding"


class GDMLThesisView(GDMLJournalsView):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.document_type = "thesis"


class GDMLCollectionView(IssuesView):
    def get_context_data(self, **kwargs):
        if "jid" not in kwargs and "pid" in kwargs:
            self.kwargs["jid"] = kwargs["jid"] = kwargs["pid"]
        context = super().get_context_data(**kwargs)
        # journal = context["obj"]

        # user = self.request.user
        #
        #
        # volumes_ctx = context["sorted_issues"][0]["volumes"]
        #
        # def sort_volume(volume):
        #     term_out = ("DS", "", "Extra_volume")
        #     for item in term_out:
        #         reg = re.compile(item)
        #         if reg.search(volume):
        #             volume.replace(item, "")
        #         if item == "":
        #             volume.replace("'", "")
        #     reg_volume = re.compile(r"\d+")
        #     volume_number = reg_volume.search(volume)
        #     if volume_number:
        #         volume = int(volume_number[0])
        #     if volume == "":
        #         volume = 0
        #     return volume
        #
        # if user.has_perm("ptf.add_collection"):
        #     pass
        # if len(periodes) > 0 and not user.has_perm("ptf.add_collection"):
        #     volumes = []
        #     for periode in periodes:
        #         periode_range = list()
        #
        #         if not isinstance(periode.begin, type(None)) and periode.begin > 0:
        #             if periode.published:
        #                 periode_range = periode_range + list(range(periode.begin, periode.end + 1))
        #                 volumes_periode = list(
        #                     filter(
        #                         lambda d: d["fyear"] in periode_range,
        #                         context["sorted_issues"][0]["volumes"],
        #                     )
        #                 )
        #                 volumes += volumes_periode
        #
        #     context["sorted_issues"][0]["volumes"] = sorted(
        #         volumes, key=lambda d: int(sort_volume(d["volume"])), reverse=True
        #     )
        # else:
        #     context["sorted_issues"][0]["volumes"] = sorted(
        #         volumes_ctx, key=lambda d: int(sort_volume(d["volume"])), reverse=True
        #     )

        return context


class GDMLItemView:
    def add_source_to_context(self, context):
        item = self.obj

        source_id = self.request.GET.get("source", None)

        if source_id is None:
            qs = item.extlink_set.filter(rel="source")
            if qs.exists():
                link = qs.first()
                source_id = link.metadata

        source = None
        qs = Source.objects.filter(domain=source_id)
        if qs.exists():
            source = qs.first()

        document_type = item.get_document_type()
        if document_type == "Article de revue":
            link_to_source_msg = "Voir la notice de l'article dans"
        elif document_type == "Acte de séminaire":
            link_to_source_msg = "Voir la notice de l'acte dans"
        elif document_type == "Thèse":
            link_to_source_msg = "Voir la notice de la thèse dans"
        elif document_type == "Notes de cours":
            link_to_source_msg = "Voir la notice des notes de cours dans"
        elif document_type == "Acte de rencontre":
            link_to_source_msg = "Voir la notice de l'acte dans"
        elif document_type == "Livre":
            link_to_source_msg = "Voir la notice du livre dans"
        else:
            link_to_source_msg = "Voir la notice du chapitre de livre dans"

        link_to_source_msg = str(_(link_to_source_msg))

        context.update({"source": source, "link_to_source_msg": link_to_source_msg})


class GDMLArticleView(ArticleView, GDMLItemView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        self.add_source_to_context(context)
        return context


class GDMLContainerView(ContainerView, GDMLItemView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        self.add_source_to_context(context)
        return context


ItemViewClassFactory.views["article"] = GDMLArticleView
ItemViewClassFactory.views["container"] = GDMLContainerView
ItemViewClassFactory.views["collection"] = GDMLCollectionView
