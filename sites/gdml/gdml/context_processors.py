from gdml.models import OAI_article
from gdml.models import OAI_harvest


def gdml_processor(request):
    return {
        "count_article": OAI_article.objects.all().count(),
        "count_collect": OAI_harvest.objects.all().count(),
    }
