#!/bin/bash
SETTINGS_LOCAL=$PWD+'/settings_local.py'
DIR_OUTPUT="/gdml_test_data/"

for col in 'ACIRM' 'AFST' 'AIF' 'AIHP' 'AIHPA' 'AIHPB' 'AIHPC' 'ALCO' 'AMBP' 'AMPA' 'ASCFM' 'ASCFPA' 'ASENS' 'ASNSP' 'AST' 'AUG' 'BSMA' 'BSMF' 'BURO' 'CAD' 'CCIRM' 'CG' ' CIF' 'CJPS' 'CM' 'CML' 'COCV' 'CRASCHIM' 'CRASMATH' 'CRBIOL' 'CRCHIM' 'CRGEOS' 'CRMATH' 'CRMECA' 'CRPHYS' 'CSHM' 'CTGDC' 'DIA' 'GAU' 'GEA' 'ITA' 'JEDP' 'JEP' 'JTNB' 'JSFS' 'M2AN' 'MALSM' 'MBK' 'MRR' 'MSH' 'MSIA' 'MSM' 'MSMF' 'NAM' 'OGEO' 'OJMO' 'PCJ' 'PDML' 'PHSC' 'PMB' 'PMIHES' 'PS' 'PSMIR' 'RCP25' 'RHM' 'RO' 'ROIA' 'RSA' 'RSMUP' 'SAC' 'SAD' 'SAF' 'SB' 'SBCD' 'SC' 'SCC' 'SD' 'SDPP' 'SE' 'SEDP' 'SEML' 'SG' 'SHC' 'SJ' 'SJL' 'SL' 'SLDB' 'SLS' 'SLSEDP' 'SMAI-JCM' 'SMJ' 'SMS' 'SPHM' 'SPK' 'SPS' 'SSL' 'SSS' 'STNB' 'STNG' 'STS' 'TAN' 'THESE' 'TSG' 'WBLR'
do
    echo "Importing $col"
    python manage.py import -pid ${col} -folder /mathdoc_archive -to_folder $DIR_OUTPUT
    echo "progressing"
done
