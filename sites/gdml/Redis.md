# Redis

## Installation

[redis installation](https://redis.io/docs/getting-started/installation/install-redis-on-linux/)

[redis python](https://redis.io/docs/clients/python/)



> redis-server

##### vérifier le démarrage du serveur

####

```
redis-server
```

Désactiver le service :

```
sudo update-rc.d redis-server disable
```

## Fonctionnement

Permet l'historisation des opérations et le suivi de progression d'une commande



[redis crawl](gdml/static/crawl_redis.jpg)

## Implémentation

client à initialiser pour les échanges avec redis:

    / middleware

        / redis_command

```
self.redis_client = redisClient()
r = self.redis_client.get_connection()
self.redis_client.exec_cmd(r.set,"progress",0)
self.redis_client.exec_cmd(r.set,"start_crawl",1)
```

#### Configuration


### Utilisation de streams

Cas d'utilisation :
- enregistrement des logs pour les commandes effectuées,
- récupération/enregistrement des paramètres d'entrée pour une tâche

[redis streams](https://redis.io/docs/data-types/streams/)

Cas d'historisation de log pour une opération en echec:

![alt image](gdml/static/gdml/img/redis-stream-erreur1.png "redis stream erreur")

![alt image](gdml/static/gdml/img/redis-stream-erreur2.png "redis stream erreur macro")

pour récupérer une queue de stream descendante avec une clé:

     last_streams = redis_conn.xrevrange("import_issue", min="-", max="+", count=10)

### Purger une liste de streams

Depuis le shell :
> redis-cli

suppression des dernières commandes

``` XDEL task:import ```

Pour supprimer l'état d'avancement de l'import d'une collection

``` XDEL import:PID```
