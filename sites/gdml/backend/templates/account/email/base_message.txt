{% load i18n %}{% autoescape off %}{% blocktrans with site_name=current_site.name %}Hello from Peer Community Journal!{% endblocktrans %}

{% block content %}{% endblock %}

{% blocktrans with site_name="Peer Community Journal" site_domain="https://peercommunityjournal.org/" %}Thank you for using {{ site_name }}!
{{ site_domain }}{% endblocktrans %}
{% endautoescape %}
