from celery import chain
from celery import shared_task
from crawler.factory import crawler_factory
from crawler.utils import get_all_cols
from crawler.utils import insert_crawl_event_in_history

# from history.views import manage_exceptions


def get_crawl_collection_task_names():
    return ["backend.tasks.crawl_collection_task", "backend.tasks.crawl_issue_task"]


@shared_task
def on_success(colid, source_name, username):
    print("SUCCESS !", colid, source_name)
    insert_crawl_event_in_history(colid, source_name, username, "OK", 0, "")


@shared_task
def log_crawl_error(context, exc, traceback):
    colid = context.args[0]
    source_name = context.args[1]
    username = context.args[-1]

    insert_crawl_event_in_history(colid, source_name, username, "ERROR", 0, traceback)


@shared_task
def crawl_collection_task(colid, source_name, username):
    print("Inside crawl_collection_task")
    all_cols = get_all_cols()
    col_data = all_cols[colid]
    url = col_data["sources"][source_name]

    crawler = crawler_factory(source_name, colid, url, username)
    merged_xissues = crawler.crawl_collection(col_only=True)

    insert_crawl_event_in_history(colid, source_name, username, "PENDING", len(merged_xissues), "")

    return merged_xissues
    # for xissue in xissues:
    #     crawl_issue_task.delay(colid, source_name, xissue, username)


@shared_task
def crawl_issues_task(merged_xissues, colid, source_name, username):
    print("Inside crawl_issues_task", len(merged_xissues))

    chain(
        [
            crawl_issue_task.si(colid, source_name, pid, merged_xissues, username)
            for pid in merged_xissues
        ]
    ).apply_async(link=on_success.si(colid, source_name, username), link_error=log_crawl_error.s())

    # eudml_collections = get_eudml_collections()
    # eudml_col = eudml_collections[colid]
    #
    # class_name = source_name + "Crawler"
    # klass = globals()[class_name]
    # crawler = klass(collection_id=colid, collection_url=eudml_col["url"], username=username)
    #
    # xissues = crawler.crawl_collection(col_only=True)
    #
    # for xissue in xissues:
    #     crawl_issue_task.delay(colid, source_name, xissue, username)


@shared_task
def crawl_issue_task(colid, source_name, pid, merged_xissues, username):
    all_cols = get_all_cols()
    col_data = all_cols[colid]
    url = col_data["sources"][source_name]

    print("Inside crawl_issue_task", pid)

    crawler = crawler_factory(source_name, colid, url, username)
    crawler.crawl_issue(pid, merged_xissues)


@shared_task
def crawl_collection_top_task(colid, source_name, username):
    print("Inside crawl_collection_top_task ")
    insert_crawl_event_in_history(colid, source_name, username, "PENDING", 10, "")

    crawl_collection_task.apply_async(
        (colid, source_name, username),
        link=crawl_issues_task.s(colid, source_name, username),
        link_error=log_crawl_error.s(),
    )
