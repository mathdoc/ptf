from django.conf import settings
from django.core.cache import cache
from django.core.management.base import BaseCommand

from apps.ptf.models import Container
from crawl.list import collections_to_crawl
from gdml.models import Periode
from gdml.models import Source
from gdml.models import SourceItem
from ptf.models import Collection
from ptf.models import Provider


class Command(BaseCommand):
    help = "Import collections to manage from list"
    links = []

    def add_arguments(self, parser):
        parser.add_argument(
            "-create",
            dest="create",
            action="store_true",
            default=None,
            required=False,
            help="Create objects",
        ),
        parser.add_argument(
            "-clear-cache",
            dest="clear-cache",
            action="store_true",
            default=None,
            required=False,
            help="Clear cache",
        ),
        parser.add_argument(
            "-sources",
            dest="sources",
            action="store_true",
            default=None,
            required=False,
            help="Create corpus objects with enum list",
        ),
        parser.add_argument(
            "-set_collection_source",
            dest="set_collection_source",
            action="store_true",
            default=None,
            required=False,
            help="Set collection to source for imported periods",
        ),
        parser.add_argument(
            "-import",
            dest="import",
            action="store_true",
            default=None,
            required=False,
            help="Import collections to manage from list",
        ),
        parser.add_argument(
            "-set-title-collections",
            dest="set_title",
            action="store_true",
            default=None,
            required=False,
            help="Set title in all formats",
        ),
        parser.add_argument(
            "-colid",
            dest="colid",
            type=str,
            default=None,
            required=False,
            help="Get existing collections by pid",
        ),
        parser.add_argument(
            "-collections_list",
            dest="collections_list",
            type=str,
            default=None,
            required=False,
            help="list collections file",
        ),
        parser.add_argument(
            "-delete",
            dest="delete",
            action="store_true",
            default=None,
            required=False,
            help="Delete all collections references",
        ),
        parser.add_argument(
            "-duplicate-collections",
            dest="duplicate-collections",
            action="store_true",
            default=None,
            required=False,
            help="Delete all duplicates collections",
        ),

    def get_collections(self, arg):
        cols = Periode.objects.filter(pid=arg)
        collections = []
        for row, col in enumerate(cols):
            collections.append(col.collection_ptr)
        return str(collections)

    def delete_duplicate_colls(self):
        for title in Collection.objects.values_list("title_sort", flat=True).distinct():
            Collection.objects.filter(
                pk__in=Collection.objects.filter(title_sort=title).values_list("id", flat=True)[1:]
            ).delete()

    def clean_colls(self, fields=None):
        for col in Collection.objects.all():
            c = col.get_container()
            colls = []
            if c is None:
                colls.append(col)

    def set_title_col(self):
        colls = Collection.objects.all()
        for index, col in enumerate(colls):
            try:
                title = col.title_sort
                col.title_tex = title = col.title_sort
                col.title_html = title
                col.title_xml = f"<title-group><title>{title}</title></title-group>"
                col.save()
            except Exception as e:
                print(e)
                pass

    def clear_cache(self):
        cache.clear()
        self.stdout.write("Cleared cache\n")

    def create_collections(self, collections_list):
        provider = Provider.objects.filter(name="mathdoc")

        for index, row in enumerate(collections_list):
            print(row)
            periode = ""

            xcollection = {
                "pid": collections_list[row]["pid"],
                "coltype": collections_list[row]["coltype"],
                "title_sort": collections_list[row]["title"],
                "wall": collections_list[row]["wall"],
            }
            try:
                periode = collections_list[row]["periode"]
                fyear = periode[0]
                xcollection["fyear"] = fyear
                xcollection["lyear"] = 9999
            except:
                pass
            coll = Collection.objects.filter(pid=xcollection["pid"])
            if coll.count() > 0:
                collection = coll[0]
                collections_list[row]["collection"] = coll[0]

            else:
                xcollection["title_html"] = xcollection["title_sort"]
                xcollection["title_tex"] = xcollection["title_sort"]
                xcollection[
                    "title_xml"
                ] = f"<title-group><title>{xcollection['title_html']}</title></title-group>"
                provider = Provider.objects.filter(name="mathdoc")
                xcollection["provider"] = provider[0]
                collection = Collection.objects.create(**xcollection)
                collection.sites.set([settings.SITE_ID])
                collection.provider = provider[0]
                collection.save()
                collections_list[row]["collection"] = collection

            if collections_list[row]["website"] == "http://www.mathnet.ru":
                try:
                    source = Source.objects.get(name=SourceItem.MATHNET.value)

                except:
                    source = Source.objects.create(name="Mathnet")

            else:
                try:
                    source = Source.objects.get(name=collections_list[row]["publisher"])
                except:
                    domain = ""
                    try:
                        if collections_list[row]["domain"] != "":
                            domain = collections_list[row]["domain"]
                    except:
                        pass
                    source_data = {
                        "name": collections_list[row]["publisher"],
                        "website": collections_list[row]["website"],
                        "domain": domain,
                    }
                    source = Source.objects.create(**source_data)

            # create default periode
            periodes = Periode.objects.filter(source=source, collection=collection)

            periode_href = (
                ""
                if "periode_href" not in collections_list[row]
                else collections_list[row]["periode_href"]
            )
            doi_href = (
                ""
                if "doi_href" not in collections_list[row]
                else collections_list[row]["doi_href"]
            )
            pdf_href = (
                ""
                if "pdf_href" not in collections_list[row]
                else collections_list[row]["pdf_href"]
            )
            periode = (
                [] if "periode" not in collections_list[row] else collections_list[row]["periode"]
            )
            collection_href = (
                ""
                if "collection_href" not in collections_list[row]
                else collections_list[row]["collection_href"]
            )
            article_href = (
                ""
                if "article_href" not in collections_list[row]
                else collections_list[row]["article_href"]
            )
            issue_href = (
                ""
                if "issue_href" not in collections_list[row]
                else collections_list[row]["issue_href"]
            )

            if periodes.count() == 0:
                periode_data = {
                    "article_href": article_href,
                    "collection_href": collection_href,
                    "issue_href": issue_href,
                    "source": source,
                    "periode_href": periode_href,
                    "periode": periode,
                    "pdf_href": pdf_href,
                    "doi_href": doi_href,
                    "collection": collection,
                    "title": collection.title_html,
                }
                periode = Periode.objects.create(**periode_data)

                print("collection reference added :" + str(periode.pk))

    def handle(self, *args, **options):
        import_action = options["import"]
        getcollection_action = options["colid"]
        delete_action = options["delete"]
        delete_duplicate_action = options["duplicate-collections"]
        set_title_action = options["set_title"]
        clear_cache = options["clear-cache"]
        set_collection_source = options["set_collection_source"]
        collections_list = options["collections_list"]
        if clear_cache:
            return self.clear_cache()

        if set_collection_source:
            return self.set_collection_to_source()

        if set_title_action:
            return self.set_title_col()
        if getcollection_action:
            return self.get_collections(getcollection_action)
        if delete_action:
            if delete_duplicate_action:
                return self.delete_duplicate_colls()
            else:
                return self.delete_colls()

        if import_action:
            # create collections from list
            return self.create_collections(collections_list)
