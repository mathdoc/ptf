import datetime
import json
import traceback
from collections import OrderedDict

import redis
from django_celery_results.models import TaskResult
from middleware.redis_command import redisClient
from pyrabbit.api import Client
from rest_framework import generics
from rest_framework import mixins
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.permissions import DjangoModelPermissions
from rest_framework.response import Response
from rest_framework.views import APIView
from task.views import get_messages_in_task_queue

from django import forms
from django.contrib import messages
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib.auth.views import LoginView
from django.forms import ModelForm
from django.http import Http404
from django.http.response import HttpResponse
from django.http.response import HttpResponseRedirect
from django.http.response import JsonResponse
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.shortcuts import render
from django.urls import reverse
from django.urls import reverse_lazy
from django.utils.html import strip_tags
from django.utils.safestring import SafeString
from django.views.generic.base import RedirectView
from django.views.generic.base import TemplateView
from django.views.generic.base import View
from django.views.generic.edit import DeleteView
from django.views.generic.edit import FormView
from django.views.generic.list import ListView

from gdml.models import Periode
from gdml.models import Source

# from gdml.tasks import prepare_crawling
# from gdml.tasks import start_crawling_eudml
from history.models import HistoryEvent
from history.models import insert_history_event
from history.views import HistoryView
from ptf.cmds.ptf_cmds import get_or_create_publisher
from ptf.models import Collection
from ptf.models import Provider
from ptf.views import CollectionEditAPIView

from .serializers import AbstractSerializer
from .serializers import CollectionNestedSerializer
from .serializers import CollectionSerializer
from .serializers import HistoryEventSerializer
from .serializers import PeriodeSerializer
from .serializers import ProviderSerializer
from .serializers import SourceSerializer
from .tasks import crawl_collection_top_task


class LogoutView(TemplateView):
    def get_success_url(self):
        return reverse("dashboard")


class MyLoginView(LoginView):
    def get_context_data(self, **kwargs):
        print(self)
        print("login")

    def get_success_url(self):
        return reverse(
            "dashboard", kwargs={"pk": self.request.user.pk, "name": self.request.user.username}
        )


class PeriodeForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["published"].widget = forms.CheckboxInput(
            attrs={
                "name": "published_form",
                "id": "id_published",
                "class": "",
                "checked": "false",
                "onclick": "this.form.submit();",
            }
        )

    class Meta:
        model = Periode
        # fields = forms.BooleanField(label="Published", required=True, widget=forms.CheckboxInput(attrs={'checked': '', 'onclick': 'this.form.submit();'}))
        fields = ["published"]


class ImportCollectionForm(forms.Form):
    number_start = forms.DecimalField(label="Start", required=False)
    number_end = forms.DecimalField(label="End", required=False)
    date_start = forms.DecimalField(label="Start", required=False)
    date_end = forms.DecimalField(label="End", required=False)
    # issue_url = forms.CharField(label="URL", required=False)
    periode_id = forms.CharField(required=False)

    def __init__(self, *args, **kwargs):
        pk = kwargs.pop("pk", None)
        date_start = kwargs.pop("date_start", None)
        date_end = kwargs.pop("date_end", None)
        number_start = kwargs.pop("number_start", None)
        number_end = kwargs.pop("number_end", None)

        super().__init__(*args, **kwargs)

        if pk:
            periode = Periode.objects.get(pk=pk)
            self.fields["periode_id"].initial = periode.id
            self.fields["periode_id"].widget = self.fields["periode_id"].hidden_widget()
            self.fields["date_start"].initial = date_start
            self.fields["date_end"].initial = date_end
            self.fields["number_start"].initial = number_start
            self.fields["number_end"].initial = number_end


class AuthentificatedView(UserPassesTestMixin, View):
    def test_func(self):
        try:
            groups = self.request.user.groups.values_list("name", flat=True)

            for g in groups:
                if g == "can_manage_resources" or g == "can_manage_import" or g == "is_staff":
                    return True
        except:
            return False

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["homepage"] = False
        context["login"] = False
        menu = OrderedDict(
            [
                ("front-office", "home"),
                ("collections", "collections"),
                ("sources", "sources"),
                ("historique", "history"),
            ]
        )

        items = menu.items()
        context["menu"] = items
        if self.request.user.is_authenticated:
            context["login"] = True
        return context

    def has_no_permission(self, request, view):
        return HttpResponse({"status": 403}, status=403)


class CollectionAPIViewSet(
    generics.ListAPIView,
    generics.RetrieveAPIView,
):
    """
    Endpoint for collections
    """

    queryset = Collection.objects.all()
    serializer_class = CollectionSerializer
    lookup_field = "pid"

    def get_serializer_class(self):
        return CollectionSerializer

    def test_func(self):
        try:
            groups = self.request.user.groups.values_list("name", flat=True)
            for g in groups:
                if g == "can_manage_resources" or g == "can_manage_import":
                    return True
        except:
            return False

    def list(self, request):
        # Note the use of `get_queryset()` instead of `self.queryset`
        queryset = self.get_queryset()
        serializer = CollectionSerializer(queryset, many=True)
        return Response(serializer.data)

    def handle_no_permission(self):
        # add custom message
        messages.error(self.request, "Permission requise")
        self.login_url = "account_login"
        return redirect(self.login_url)


class CollectionApiView(UserPassesTestMixin, generics.GenericAPIView):
    serializer_class = CollectionSerializer
    queryset = Collection.objects.all()
    lookup_field = "pid"

    def test_func(self):
        try:
            groups = self.request.user.groups.values_list("name", flat=True)
            for g in groups:
                if g == "can_manage_resources" or g == "can_manage_import":
                    return True
        except:
            return False

    def get(self, request, *args, **kwargs):
        pid = kwargs.get("pid")
        collection = get_object_or_404(Collection, pid=pid)
        print(collection)
        serializer = CollectionSerializer(collection)
        serializer_data = serializer.data

        abstract = collection.abstract_set.filter(tag="description")
        serializer_abstract = AbstractSerializer(abstract, many=True)
        providers = Provider.objects.all()
        serializer_provider = ProviderSerializer(providers, many=True)
        return JsonResponse(
            {
                "collection": serializer_data,
                "descriptions": serializer_abstract.data,
                "providers": serializer_provider.data,
            }
        )


class CollectionView(AuthentificatedView, FormView):
    """
    Collection access view
    """

    template_name = "collection.html"
    form_class = PeriodeForm
    model = Periode

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        pid = self.kwargs.get("pid", None)
        if not isinstance(pid, type(None)):
            collection = get_object_or_404(Collection, pid=pid)

            periodes = collection.periods.all()
            context["collection"] = collection
            context["periodes"] = periodes

        return context

    def post(self, request, *args, **kwargs):
        print(request)
        if request.POST["pk"]:
            pk = request.POST["pk"]
            published = False
            csrfmiddlewaretoken = request.POST["csrfmiddlewaretoken"]
            try:
                published_value = request.POST["published"]
                if published_value == "on":
                    published = True
            except:
                pass

            p = Periode.objects.get(id=pk)
            data_form = {
                "encoding": "utf-8",
                "csrfmiddlewaretoken": csrfmiddlewaretoken,
                "published": published,
                "pk": pk,
            }
            form = PeriodeForm(instance=p, data=data_form)
            if form.is_valid():
                instance = form.save(commit=False)
                instance.published = published
                instance.save()
        return HttpResponseRedirect(
            reverse("collection_view", kwargs={"pid": self.kwargs.get("pid")})
        )

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        # form.send_email()
        return HttpResponseRedirect(
            reverse("collection_view", kwargs={"pid": self.kwargs.get("pid")})
        )


class CollectionsView(AuthentificatedView, TemplateView):
    template_name = "collections.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        collections = Collection.objects.all()
        context["collections"] = collections
        return context


class TheCollectionEditView(AuthentificatedView, TemplateView):
    """
    Edit Collection Metadata page
    The HTML page uses the vuejs widget inside apps/vart
    """

    template_name = "ptf/backend/collection_edit.html"


class TheCollectionEditAPIView(CollectionEditAPIView):
    def convert_data_for_editor(self, data):
        super().convert_data_for_editor(data)
        data.periods = [
            {
                "source": period.source.name if period.source else "",
                "collection_href": period.collection_href,
                "issue_href": period.issue_href,
                "begin": period.begin,
                "end": period.end,
                "first_issue": period.first_issue,
                "last_issue": period.last_issue,
                "id": period.pk,
                "doi_href": period.doi_href,
            }
            for period in self.collection.periods.all()
        ]

    def update_collection(self, data):
        collection = super().update_collection(data)

        collection.periods.all().delete()

        for data_period in data.periods:
            publisher_obj = data_period.get("publisher", None)
            publisher = get_or_create_publisher(publisher_obj.name) if publisher_obj else None

            source = None
            source_name = data_period.get("source", None)
            if source_name:
                qs = Source.objects.filter(name=source_name)
                if len(qs) > 0:
                    source = qs.first()

            period = Periode(
                collection=collection,
                source=source,
                publisher=publisher,
                title=data_period.get("title", ""),
                issue_href=data_period.get("issue_href", None),
                collection_href=data_period.get("collection_href", None),
                doi_href=data_period.get("doi_href", ""),
                published=data_period.get("published", False),
                begin=data_period.get("begin"),
                end=data_period.get("end"),
                first_issue=data_period.get("first_issue"),
                last_issue=data_period.get("last_issue"),
                wall=collection.wall,  # TODO remove this field from Period
            )
            period.save()
            # set hook to update history entry ref period

        return collection


class TasksPendingView(UserPassesTestMixin, View):
    permission_required = "can_manage_import"

    def test_func(self):
        try:
            groups = self.request.user.groups.values_list("name", flat=True)
            for g in groups:
                if g == "can_manage_resources" or g == "can_manage_import":
                    return True
        except:
            return False

    def get(self, *args, **kwargs):
        remaining = (
            TaskResult.objects.filter(task_name="gdml.tasks.import_issue")
            .exclude(status__in=["SUCCESS", "FAILURE"])
            .values()
        )
        remaining_count = TaskResult.objects.filter(
            task_name="gdml.tasks.import_issue", status="PENDING"
        ).count()
        remainings = []

        for next in remaining:
            remainings.append(next)
        data = {"remaining": remainings, "total": remaining_count}
        return JsonResponse(data)


class TasksImportFailedView(UserPassesTestMixin, View):
    permission_required = "can_manage_import"

    def test_func(self):
        try:
            groups = self.request.user.groups.values_list("name", flat=True)
            for g in groups:
                if g == "can_manage_resources" or g == "can_manage_import":
                    return True
        except:
            return False

    def get(self, *args, **kwargs):
        errors = TaskResult.objects.filter(
            task_name="gdml.tasks.import_issue", status__in=["FAILURE", "ERROR"]
        ).values()
        errors_count = TaskResult.objects.filter(
            task_name="gdml.tasks.import_issue", status__in=["FAILURE", "ERROR"]
        ).count()
        errs = []

        for error in errors:
            errs.append(error)
        data = {"errors": errs, "total": errors_count}
        return JsonResponse(data)

    def handle_no_permission(self):
        # add custom message
        messages.error(self.request, "Permission requise")
        self.login_url = "login"
        return super().handle_no_permission()


class TasksImportProgressView(AuthentificatedView, View):
    def get(self, *args, **kwargs):
        redis_conn = redis.StrictRedis(port=6379, host="127.0.0.1")
        is_crawling_eudml = redis_conn.get("start_crawl")

        task_import = TaskResult.objects.first()
        collection = ""
        titre = ""
        username = ""
        periode = ""
        numbers = ""
        if not isinstance(task_import, type(None)):
            if len(task_import.task_kwargs) > 0:
                evt = (
                    task_import.task_kwargs.replace('"', "")
                    .replace('"', "'")
                    .replace("'", '"')
                    .replace("None", '"0,0"')
                    .replace("\n", "")
                )

                data = json.loads(evt)

                if "pid" in data:
                    collection = data["pid"]

                if "title" in data:
                    titre = data["title"]

                if "collection" in data:
                    collection = data["collection"]

                if "username" in data:
                    username = data["username"]

                if "periode_range" in data:
                    periode = data["periode_range"]
                    if periode == "0,0":
                        periode = ""

                if "number_range" in data:
                    numbers = data["number_range"]
                    if numbers == "0,0":
                        numbers = ""

                if "periodeid" in data:
                    if collection == "" and data["periodeid"] != "":
                        periodeid = data["periodeid"]
                        periode = Periode.objects.get(pk=periodeid)
                        serializer = PeriodeSerializer(periode)
                        periode = serializer.data
                        collection = periode["collection"]["pid"]

        successes = TaskResult.objects.filter(
            task_name="gdml.tasks.import_issue", status="SUCCESS"
        ).count()
        fails = TaskResult.objects.filter(
            task_name="gdml.tasks.import_issue", status__in=["ERROR", "FAILURE"]
        ).count()
        last_task = (
            TaskResult.objects.filter(task_name="gdml.tasks.import_issue")
            .order_by("-date_done")
            .first()
        )
        obj = TaskResult.objects.all().filter(task_name="gdml.tasks.import_issue")
        pendings = []
        for result in obj:
            print(result.meta)
            pendings.append(result.meta)

        if last_task:
            last_task = " : ".join([last_task.date_done.strftime("%Y-%m-%d"), last_task.task_args])
        remaining = get_messages_in_queue()

        all = int(successes) + remaining

        """
        if done == 1:
            all = 1
            last_task = {}
            last_task["date_done"] = result.date_done
            last_task["task_args"] = result.task_kwargs
            last_task = " : ".join(
                [last_task["date_done"].strftime("%Y-%m-%d"), last_task["task_args"]]
            )
        """
        progress = int(successes * 100 / all) if all else 0
        error_rate = int(fails * 100 / all) if all else 0
        remaining = all - int(successes)

        if not isinstance(is_crawling_eudml, type(None)):
            print(type(is_crawling_eudml))
            if int(is_crawling_eudml) == 1:
                all = int(redis_conn.hget("last_task_import", "total"))
                successes = int(redis_conn.hget("last_task_import", "progress"))
                username = str(redis_conn.hget("last_task_import", "username").decode())
                fails = int(redis_conn.get("failures_collection"))
                periode = ""
                collection = redis_conn.hget("last_task_import", "collection").decode()
                last_task = ""
                remaining = all - successes
                progress = int(successes * 100 / all) if all else 0
                numbers = ""
                titre = "EUDML"

        status = (
            "consuming_queue"
            if (successes or fails) and (not progress == 100 and not remaining == 0 and all > 1)
            else "polling"
        )

        data = {
            "status": status,
            "progress": progress,
            "total": all,
            "remaining": remaining,
            "successes": successes,
            "fails": fails,
            "error_rate": error_rate,
            "last_task": last_task,
            "collection": collection,
            "titre": titre,
            "username": username,
            "periode": periode,
            "numbers": numbers,
        }
        return JsonResponse(data)

    def handle_no_permission(self):
        # add custom message
        messages.error(self.request, "Permission requise")
        self.login_url = "login"
        return super().handle_no_permission()


class SourcesView(AuthentificatedView, TemplateView):
    template_name = "sources.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["sources"] = Source.objects.all()

        return context


class SourceView(AuthentificatedView, TemplateView):
    template_name = "source.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        pk = self.kwargs.get("pk", None)
        source = get_object_or_404(Source, pk=pk)

        context["source"] = source
        context["collections"] = Collection.objects.filter(periods__source=source).distinct()

        return context


class SourceForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs["class"] = "form-control"
        self.fields["name"].label = "nom"
        self.fields["domain"].label = "nom de domaine"
        self.fields["website"].label = "site web"

    class Meta:
        model = Source
        fields = ["name", "domain", "website"]


class HistoryClearView(DeleteView):
    model = HistoryEvent
    success_url = reverse_lazy("history")

    def get_object(self):
        # for deleting events excluding latest
        return self.get_queryset().get_stale_events()
        # return self.get_queryset()


def get_messages_in_queue():
    cl = Client("localhost:15672", "guest", "guest")
    vhost = cl.get_vhost_names()[0]
    messages = cl.get_queue_depth(vhost, "celery")

    return messages


def manage_exceptions(operation, pid, colid, status, exception, target=""):
    """
    Insert exception in history

    @param operation:
    @param pid:
    @param colid:
    @param status:
    @param exception:
    @param target:
    @return:
    """
    if type(exception).__name__ == "ServerUnderMaintenance":
        message = " - ".join([str(exception), "Please try again later"])
    else:
        exceptiondata = traceback.format_exc().splitlines()
        message = " - ".join([str(exception), exceptiondata[-1]])

    insert_history_event(
        {
            "type": operation,
            "pid": pid,
            "col": colid,
            "status": status,
            "data": {
                "ids_count": 0,
                "message": message,
                "target": target,
            },
        }
    )


"""
   Endpoint to list last events
"""


class HistoryEvents(UserPassesTestMixin, APIView):
    def test_func(self):
        try:
            groups = self.request.user.groups.values_list("name", flat=True)
            for g in groups:
                if g == "can_manage_resources" or g == "can_manage_import":
                    return True
        except:
            return False

    def get(self, request, *args, **kwargs):
        # deployed_date = issue.deployed_date()
        results = []
        collections = Collection.objects.all()
        if "pid" in kwargs:
            pid = kwargs["pid"]
            try:
                latest = HistoryEvent.objects.filter(col=pid)
                event = {}
                for result in latest:
                    h = HistoryEventSerializer(result)
                    h = h.data
                    if h is not None:
                        event["type"] = h["type"]
                        event["pid"] = h["pid"]
                        event["created"] = result.created_on.strftime("%Y-%m-%d %H:%M")
                        event["status"] = h["status"]
                        results.append(h)
            except HistoryEvent.DoesNotExist as _:
                pass
        else:
            for collection in collections:
                try:
                    latest = HistoryEvent.objects.filter(col=collection.pid, status="ERROR").last()
                    result = {"pid": collection.pid}
                    if latest is not None:
                        if latest.type is not None:
                            result["type"] = latest.type
                            result["created"] = latest.created_on.strftime("%Y-%m-%d %H:%M")
                            result["status"] = latest.status

                except HistoryEvent.DoesNotExist:
                    pass
                results.append(result)

        return JsonResponse(results, safe=False)


class HistoryGDMLView(AuthentificatedView, HistoryView):
    template_name = "history/history.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        all_filters = ["type", "pid", "col", "status", "month"]
        filters = {}
        for filter in all_filters:
            value = self.request.GET.get(filter, None)
            if value:
                filters[filter] = value

            params = [
                f"{key}={value}"
                for key, value in self.request.GET.items()
                if key not in [filter, "page", "search"]
            ]
            # Insert empty string to make sure '&' is added before the first param
            params.insert(0, "")
            params_str = "&".join(params)
            context[filter + "_link"] = "?search=" + params_str

        # MongoDB can create groups, SQL does not
        # events_list = HistoryEvent.objects.exclude(unique_id__isnull=True).distinct()
        #
        # events = []
        #
        # events_list = serialize(
        #     "json",
        #     events_list,
        #     fields=(
        #         "type",
        #         "created_on",
        #         "title",
        #         "pid",
        #         "user",
        #         "type_error",
        #         "unique_id",
        #         "status",
        #         "data",
        #         "parent",
        #     ),
        # )
        #
        # events_list = json.loads(events_list)
        # for event in events_list:
        #     grouped_event = event["fields"]
        #     pk = event["pk"]
        #     id = grouped_event["user"]
        #     if not isinstance(id, type(None)):
        #         grouped_event["user"] = User.objects.get(pk=id).username
        #     if grouped_event["parent"] is None:
        #         childs = HistoryEvent.objects.filter(parent=pk).exclude(unique_id__isnull=False)
        #         childs = serialize(
        #             "json",
        #             childs,
        #             fields=(
        #                 "type",
        #                 "created_on",
        #                 "title",
        #                 "pid",
        #                 "user",
        #                 "type_error",
        #                 "status",
        #                 "data",
        #             ),
        #         )
        #         childs = json.loads(childs)
        #         child_events = []
        #         for child in childs:
        #             child["fields"]["type"] = ""
        #             id = child["fields"]["user"]
        #             if not isinstance(id, type(None)):
        #                 child["fields"]["user"] = User.objects.get(pk=id).username
        #             child_events.append(child["fields"])
        #
        #         grouped_event["events"] = child_events
        #     events.append(grouped_event)
        #
        # context["grouped_events"] = events
        # context["now"] = timezone.now()
        # collections = []
        #
        # col = Collection.objects.all()
        # for collection in enumerate(col):
        #     if collection is not None:
        #         serializer = CollectionNestedSerializer(collection)
        #         data = serializer.data
        #         collections.append(data)
        # context["collections"] = collections
        return context


def get_last_command_params(id_cmd):
    """
    Retrieve params of a previous command
    @param id_cmd:
    @return: params
    """
    redis_client = redisClient()
    redis_conn = redis_client.get_connection()
    history_event = HistoryEvent.objects.filter(unique_id=id_cmd)
    if len(history_event) > 0:
        history_event = history_event.first()
        source = history_event.source
        collection = history_event.col

        if source != "":
            process_name = source
        else:
            process_name = collection
    stream_event = redis_client.exec_cmd(
        redis_conn.xrevrange, "import:" + process_name, max="+", min="-", count=1
    )
    date_start = 0
    date_end = ""
    number_start = None
    number_end = None
    if len(stream_event) > 0:
        data_stream = stream_event[0][1]

        if "periode" in data_stream:
            periode_range = data_stream["periode"]
            periode_range = periode_range.split(",")
            if len(periode_range) == 2:
                date_start = periode_range[0]
                date_end = periode_range[1]
            if len(periode_range) == 1:
                date_start = periode_range[0]
            if "numbers" in data_stream:
                numbers = data_stream["numbers"]
                numbers = numbers.split(",")
                if len(numbers) == 2:
                    number_start = numbers[0]
                    number_end = numbers[1]

    params = {}
    params["date_start"] = date_start
    params["date_end"] = date_end
    params["number_start"] = number_start
    params["number_end"] = number_end

    yield params


"""
 View to import periode with parameters passed
"""


class PeriodeImportFormView(AuthentificatedView, FormView):
    template_name = "periode_import.html"
    form_class = ImportCollectionForm

    @staticmethod
    def reset_task_results():
        TaskResult.objects.all().delete()

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        try:
            periodes = Periode.objects.filter(pk=self.kwargs.get("pk"))

            progress_id = self.kwargs.get("progress_id", None)
            date_start = ""
            date_end = ""
            number_start = ""
            number_end = ""
            if progress_id is not None:
                params = get_last_command_params(progress_id)
                print(params)
                for attr in params:
                    print(attr["date_end"])
                    date_start = attr["date_start"]
                    date_end = attr["date_end"]
                    number_start = attr["number_start"]
                    number_end = attr["number_end"]
        except Exception:
            periodes = None
        all_fields = {"periods": [], "numbers": []}
        if len(periodes) > 0:
            periode = periodes[0]
            title_tex = periode.collection.title_tex
            name_source = periode.source.name
            id_source = periode.source.id
            collection = periode.collection
            try:
                import_collection_form = ImportCollectionForm(
                    pk=periode.id,
                    date_start=date_start,
                    date_end=date_end,
                    number_start=number_start,
                    number_end=number_end,
                )
                all_fields["periods"].append(import_collection_form["date_start"])
                all_fields["periods"].append(import_collection_form["date_end"])
                all_fields["numbers"].append(import_collection_form["number_start"])
                all_fields["numbers"].append(import_collection_form["number_end"])
                # all_fields["issues"].append(import_collection_form["continue_import"])
                all_fields["issues"].append(import_collection_form["issue_url"])
            except Exception as e:
                print(e)
            context["title"] = title_tex
            context["collection"] = collection
            context["name_source"] = name_source
            context["id_source"] = id_source
            context["periode"] = periode
            context["all_fields"] = all_fields

        else:
            messages.error(self.request, "La periode a été supprimée")
            context["error"] = "La periode a été supprimée"
        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        if "error" in context:
            if context["error"] != "":
                return redirect(reverse("history"))
        return render(self.request, self.template_name, context)

    def get_success_url(self):
        return reverse("dashboard")

    def form_valid(self, form):
        # user = self.request.user
        selected_import = self.request.POST.get("typeimport")
        context = super().get_context_data()

        if get_messages_in_queue() > 0:
            context.update({"crawling": True})
            return render(self.request, "dashboard.html", context)

        # periode = Periode.objects.get(pk=self.kwargs.get("pk"))
        # col = periode.collection
        # title = col.title_tex
        # pid = col.pid

        if selected_import == "all_period":
            pass
            # TODO use apps/tasks to import

            # prepare_crawling.delay(
            #     periodeid=self.kwargs.get("pk"),
            #     periode="",
            #     userid=user.id,
            #     username=user.username,
            #     title=title,
            #     pid=pid,
            # )
        else:
            period_begin = form.cleaned_data["date_start"]
            period_end = form.cleaned_data["date_end"]
            number_begin = form.cleaned_data["number_start"]
            number_end = form.cleaned_data["number_end"]
            periodes = []
            numbers = []

            if not isinstance(period_begin, type(None)):
                if period_begin != "":
                    periodes.append(int(period_begin))
            if not isinstance(period_end, type(None)):
                if period_end != "":
                    periodes.append(int(period_end))

            if not isinstance(number_begin, type(None)):
                if number_begin != "":
                    numbers.append(int(number_begin))

            if not isinstance(number_end, type(None)):
                if number_end != "":
                    numbers.append(int(number_end))

            if len(numbers) > 0:
                pass
                # TODO use apps/tasks to import

                # prepare_crawling.delay(
                #     periodeid=self.kwargs.get("pk"),
                #     numbers=numbers,
                #     userid=user.id,
                #     username=user.username,
                #     title=title,
                #     pid=pid,
                # )

            if len(periodes) > 0:
                pass
                # TODO use apps/tasks to import

                # prepare_crawling.delay(
                #     periodeid=self.kwargs.get("pk"),
                #     periode=periodes,
                #     userid=user.id,
                #     username=user.username,
                #     title=title,
                #     pid=pid,
                # )

        return super().form_valid(form)


class Home2DashboardView(AuthentificatedView, TemplateView):
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        if context.get("login") is False:
            return redirect(reverse("login"))

        return render(request, "dashboard2.html", context)

    def handle_no_permission(self):
        messages.error(self.request, "Permission requise")
        self.login_url = "login"
        return redirect(self.login_url)


class HomeDashboardView(AuthentificatedView, TemplateView):
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)

        if self.request.user.is_authenticated:
            context["login"] = True

        redis_conn = redis.StrictRedis(host="127.0.0.1", port=6379)
        is_crawling_eudml = redis_conn.get("start_crawl")

        if is_crawling_eudml == "1":
            context["crawl_progress"] = 1

        last_commands = redis_conn.xrevrange("task:import", max="+", min="-", count=5)

        commands = []
        for cmd in last_commands:
            command = {}
            id = None
            for index, object in enumerate(cmd):
                if index == 0:
                    id = object.decode()
                    command[id] = []
                else:
                    result = {}
                    for dict_object in object:
                        if dict_object.decode() == "date":
                            decoded_time = object[dict_object].decode()
                            date_from = datetime.datetime.fromtimestamp(int(decoded_time))
                            if len(str(date_from.day)) == 1:
                                day = "0" + str(date_from.day)
                            else:
                                day = str(date_from.day)
                            if len(str(date_from.month)) == 1:
                                month = "0" + str(date_from.month)
                            else:
                                month = str(date_from.month)

                            date_from = day + "-" + month + "-" + str(date_from.year)
                            result[dict_object.decode()] = date_from
                        if dict_object.decode() == "number_range":
                            numbers = object[dict_object].decode()
                            numbers = numbers.split(",")
                            if not isinstance(numbers, type(None)):
                                if len(numbers[0]) > 0:
                                    numbers = str(numbers[0]) + " et " + str(numbers[1])
                                    result[dict_object.decode()] = numbers
                        else:
                            if (
                                dict_object.decode() != "date"
                                and dict_object.decode() != "number_range"
                            ):
                                result[dict_object.decode()] = object[dict_object].decode()
                    if result not in command[id]:
                        command[id].append(result)
            commands.append(command)

        context["commands"] = commands

        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        if context.get("login") is False:
            return redirect(reverse("login"))

        return render(request, "dashboard.html", context)

    def handle_no_permission(self):
        messages.error(self.request, "Permission requise")
        self.login_url = "login"
        return redirect(self.login_url)


class SourcesManageView(AuthentificatedView, ListView, FormView):
    model = Source
    template_name = "sources.html"
    add_home = True
    form_class = SourceForm
    success_url = reverse_lazy("sources")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["homepage"] = False
        context["login"] = False
        pk = self.kwargs.get("pk")
        if pk:
            source = Source.objects.get(pk=pk)
            serializer = SourceSerializer(source)
            context["source"] = serializer.data

        user = self.request.user
        permissions = user.has_perm("source.can_change")
        context["can_change"] = permissions
        menu = OrderedDict(
            [("front-office", "home"), ("sources", "sources"), ("historique", "history")]
        )

        items = menu.items()
        context["menu"] = items
        source_form = SourceForm()
        sources_dict = []

        sources = Source.objects.all()
        for source in sources:
            sources_dict.append(source)
        if self.request.user.is_authenticated:
            context["login"] = True
        sources = Source.objects.all()
        result = []
        for source in sources:
            periodes = Periode.objects.filter(source=source)
            collections = []
            source_collections = {
                "name": source.name,
                "domain": source.domain,
                "website": source.website,
                "id": source.pk,
            }

            for periode in periodes:
                if periode.collection not in collections:
                    col = CollectionNestedSerializer(periode.collection)
                    col.data["title_html"] = strip_tags(SafeString(col.data["title_html"]))[:250]
                    col.data["title_html"].replace("[[", "(")
                    col.data["title_html"].replace("]]", ")")

                    collections.append(col.data)
            source_collections["collections"] = collections
            result.append(source_collections)
        context["sources"] = result

        context["sources_data"] = result

        context["form_source"] = source_form

        return context

    def handle_no_permission(self):
        # add custom message
        messages.error(self.request, "Permission requise")
        self.login_url = "login"
        return redirect(self.login_url)

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        serializer = SourceSerializer(data=form.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return super().form_valid(form)

    def form_invalid(self, form):
        print("invalid")


class TaskStatusView(UserPassesTestMixin, View):
    def test_func(self):
        try:
            groups = self.request.user.groups.values_list("name", flat=True)
            for g in groups:
                if g == "can_manage_resources" or g == "can_manage_import":
                    return True
        except:
            return False

    def get(request, task_id, *args, **kwargs):
        taskid = kwargs.get("taskid")
        task = TaskResult.objects.filter(task_id=taskid).first()

        try:
            response = {
                "task_id": taskid,
                "state": task.status,
                "progression": "None",
                "info": str(task.meta),
                "task_name": task.task_name,
            }
            return JsonResponse(response, status=200)
        except:
            return JsonResponse({"task not found": taskid}, status=200)


class RestartImportItemView(AuthentificatedView, View):
    def get(self, request, *args, **kwargs):
        # id_task = kwargs.get("id_task")

        # redis_client = redisClient()
        # redis_conn = redis_client.get_connection()
        # history_event = HistoryEvent.objects.filter(unique_id=id_task)
        # source = ""
        # if len(history_event) > 0:
        #     history_event = history_event.first()
        #     source = history_event.source
        #     collection = history_event.col
        #
        #     if source != "":
        #         process_name = source
        #     else:
        #         process_name = collection
        # stream_event = redis_client.exec_cmd(
        #     redis_conn.xrevrange, "import:" + process_name, max="+", min="-", count=1
        # )
        #
        # if len(stream_event) > 0:
        #     data_stream = stream_event[0][1]
        #     if "periodeid" in data_stream:
        #         periode_id = data_stream["periodeid"]
        #         return redirect(
        #             reverse(
        #                 "reimport_periode",
        #                 kwargs={"pk": periode_id, "progress_id": id_task},
        #             )
        #         )
        #
        #     else:
        #         if source == "EUDML":
        #             start_crawling_eudml.delay(pid="", username=self.request.user.username)
        #             # run_spider(EudmlSpider)

        return redirect(reverse("dashboard"))

    def handle_no_permission(self):
        # add custom message
        messages.error(self.request, "Permission requise")
        self.login_url = "login"
        return redirect(self.login_url)


class AccountsView:
    template_name = "profile.html"

    def get_context_data(self, request, **kwargs):
        context = super().get_context_data(**kwargs)

        if request.user.is_authenticated:
            context["user"] = request.user

        return context

    def user_info(request):
        def get_context_data(self, request, **kwargs):
            context = super(AccountsView, self).get_context_data(**kwargs)
            context["user"] = request.user

            return context

        return render(request, "profile.html")


"""
    Api view for collections linked to source
"""


class SourceAPIViewSet(
    AuthentificatedView,
    generics.ListCreateAPIView,
    LimitOffsetPagination,
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
):
    """
    Endpoint for collections ref
    """

    queryset = Source.objects.all()
    serializer_class = SourceSerializer
    permission_classes = [DjangoModelPermissions]

    def get_serializer_class(self):
        return SourceSerializer

    def get(self, request, pk=None, format=None):
        try:
            if "pk" in self.kwargs:
                pk = self.kwargs["pk"]
                source = Source.objects.get(pk=pk)
                serializer = SourceSerializer(source)
                return Response(serializer.data)
        except:
            pass

        sources = Source.objects.all()
        results = self.paginate_queryset(sources, request)
        serializer = SourceSerializer(results, many=True)
        return self.get_paginated_response(serializer.data)

    def patch(self, request, *args, **kwargs):
        instance = get_object_or_404(Source, *args, **kwargs)
        source_serializer = self.get_serializer(instance, data=request.data, partial=True)
        if source_serializer.is_valid():
            source_serializer.save()
            return Response(source_serializer.data)
        else:
            return Response(source_serializer.errors)


class TaskCrawlCollectionView(RedirectView):
    @staticmethod
    def reset_task_results():
        TaskResult.objects.all().delete()

    def get_redirect_url(self, *args, colid, source):

        # Make sure tasks are not already running
        if not get_messages_in_task_queue():
            self.reset_task_results()
            # if "progress/" in self.colid:
            #     self.colid = self.colid.replace("progress/", "")
            # if "/progress" in self.colid:
            #     self.colid = self.colid.replace("/progress", "")

            # TODO check colid
            if False:
                return Http404

            user = self.request.user
            crawl_collection_top_task.delay(colid, source, user.username)

        return reverse("dashboard")
