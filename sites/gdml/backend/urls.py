"""dashboard URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib.auth.views import PasswordChangeView
from django.contrib.staticfiles import views
from django.urls import include
from django.urls import path
from django.urls import re_path

from history.views import HistoryEventDeleteView

# from .views import HomeDashboardView
from .views import AccountsView
from .views import CollectionApiView
from .views import CollectionAPIViewSet
from .views import CollectionsView
from .views import CollectionView
from .views import HistoryClearView
from .views import HistoryEvents
from .views import HistoryGDMLView
from .views import Home2DashboardView
from .views import LogoutView
from .views import MyLoginView
from .views import PeriodeImportFormView
from .views import RestartImportItemView
from .views import SourceAPIViewSet
from .views import SourcesView
from .views import SourceView
from .views import TaskCrawlCollectionView
from .views import TasksImportFailedView
from .views import TasksImportProgressView
from .views import TasksPendingView
from .views import TaskStatusView
from .views import TheCollectionEditAPIView
from .views import TheCollectionEditView

urlpatterns = [
    path("", Home2DashboardView.as_view(), name="dashboard"),
    re_path(r"col/(?P<pid>\w+)/$", CollectionView.as_view(), name="collection_view"),
    re_path(
        r"^collection-edit/(?P<colid>[A-Z0-9-_]+)/$",
        TheCollectionEditView.as_view(),
        name="edit-collection",
    ),
    path("accounts/profile", AccountsView.user_info, name="user_info"),
    path("api-sources/", SourceAPIViewSet.as_view(), name="api_sources"),
    path("api-sources/<path:pk>/", SourceAPIViewSet.as_view(), name="api_sources"),
    path("api-events/", HistoryEvents.as_view(), name="api_events"),
    path("api-collections/", CollectionAPIViewSet.as_view(), name="api_collections"),
    re_path(r"api-collections/(?P<pid>\w+)/$", CollectionApiView.as_view(), name="api_collection"),
    path("periode/import/<int:pk>", PeriodeImportFormView.as_view(), name="import_periode"),
    re_path(
        r"periode/import/(?P<pk>\d+)/(?P<progress_id>([\w-]+))/$",
        PeriodeImportFormView.as_view(),
        name="reimport_periode",
    ),
    path("sources/<int:pk>/", SourceView.as_view(), name="source"),
    path("sources/", SourcesView.as_view(), name="sources"),
    path("collections/", CollectionsView.as_view(), name="collections"),
    re_path(
        r"^api-collection-edit/(?P<colid>[A-Z0-9-_]+)/$",
        TheCollectionEditAPIView.as_view(),
        name="api-collection-edit",
    ),
    re_path(
        r"restart_import_item/(?P<id_task>([\w-]+))/$",
        RestartImportItemView.as_view(),
        name="restart_import_item",
    ),
    re_path(r"task_status/(?P<taskid>([\w-]+))/$", TaskStatusView.as_view(), name="task_status"),
    path("import-issues/progress/", TasksImportProgressView.as_view(), name="tasks_progress"),
    path("import-issues/errors/", TasksImportFailedView.as_view(), name="tasks_errors"),
    path("import-issues/pending/", TasksPendingView.as_view(), name="tasks_pending"),
    path(
        "crawl_collection/<path:colid>/<path:source>",
        TaskCrawlCollectionView.as_view(),
        name="crawl_collection",
    ),
]

urlpatterns += [
    path("accounts/", include("django.contrib.auth.urls")),
    re_path(r"^accounts/login/", MyLoginView.as_view(), name="login"),
    re_path(r"^accounts/logout/", LogoutView.as_view(), name="account_logout"),
    re_path(
        r"^accounts/changepassword/", PasswordChangeView.as_view(), name="account_change_password"
    ),
    re_path(r"^static/(?P<path>.*)$", views.serve),
    path("", include("task.urls")),
    path(
        "celery-progress/", include("celery_progress.urls")
    ),  # add this line (the endpoint is configurable)
]
urlpatterns += [
    path("history/", HistoryGDMLView.as_view(), name="history"),
    path("history/clear/", HistoryClearView.as_view(), name="history-clear"),
    path("history/<int:pk>/delete/", HistoryEventDeleteView.as_view(), name="history-delete"),
]
