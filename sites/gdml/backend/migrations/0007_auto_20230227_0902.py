# Generated by Django 3.2.16 on 2023-02-27 09:02

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0006_alter_collectionref_collection_href'),
    ]

    operations = [
        migrations.AlterField(
            model_name='collectionref',
            name='issue_href',
            field=models.CharField(blank=True, max_length=150, null=True),
        ),
        migrations.AlterField(
            model_name='collectionref',
            name='periode',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.IntegerField(blank=True), blank=True, null=True, size=4),
        ),
    ]
