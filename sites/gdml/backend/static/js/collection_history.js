let issues_not_imported=[]
let issues=[]

function update_issue_row(line) {
    let pid = line[0].cells[0].firstChild.textContent
    console.log(pid)
    console.log(error)
}


function getErrors(e){
  get_history(e.target.getAttribute('data-id'))
}

function getLastEvent(pid) {
   let last_event = last_events.filter(e => e.pid == pid)
   let res=""
   if(last_event.length > 0){

      if(last_event[0].status == "ERROR") {
        res = "ERROR import<i class='bi bi-fire'></i>"
      }
   }

   return res
}

function gotoEdit(pid) {
  // window.location.href='/dashboard/collections-manage/'+pid
  window.location.href='/dashboard/col/'+pid
}
function deleteCollection(pid) {
       let uri = "/api/collections/"+pid+"/"
       let csrf_token = document.querySelector('[name=csrfmiddlewaretoken]').value;
       let collection = collections.filter( c => c.pid == pid)
       if (collection > 0) {
        collection = collection[0]
       }
       $.ajax({
          xhrFields: {
            withCredentials: true
          },
          type: "DELETE",
          headers: {"X-CSRFTOKEN": csrf_token},
        contentType: "application/json",
        url: uri,
        data:collection}).done(function(response) {

        $('.col-deleted').html('<p class="col-deleted">'+collection[0].title_html+'</p>');
        var myModal = new bootstrap.Modal(document.getElementById('confirmModal'));

        myModal.show()


      })

}

function getListCollections(collections) {
    let uri = "/api/history";
    let data = "";
    $.get(uri)
        .done(function (results) {
            last_events = results;
            console.log(last_events);

            var columnNames = [];
            var tblData = { "data": [] };
            console.log(collections);

            collections.forEach(item => {
                var pissn = null;
                var e_issn = null;
                var action = null;
                if (item.resourceid_set.length > 0) {
                    for (var i = 0; i < item.resourceid_set.length; i++) {
                        if (item.resourceid_set[i].id_type == "issn") {
                            pissn = item.resourceid_set[i].id_value;
                        } else if (item.resourceid_set[i].id_type == "e-issn") {
                            e_issn = item.resourceid_set[i].id_value;
                        }
                    }
                }
                var issn = "<p>";
                if (pissn !== null) {
                    issn += "p-issn : " + pissn;
                } else {
                    issn += "p-issn : ";
                }
                if (e_issn !== null) {
                    issn += "<br>e-issn : " + e_issn + "</p>";
                } else {
                    issn += "<br>e-issn : </p>";
                }
                tblData["data"].push({ 'title': item.title_html, 'pid': item.pid, 'pissn': pissn, 'e_issn': e_issn, 'action': action, 'issn': issn });
            });

            columnNames.push({
                title: '', data: 'title', "render": function (data, type, row, meta) {
                    return '<h6>' + data + '</h6>'
                }
            }, { title: '', data: 'issn', defaultContent: '' }, { title: '', data: 'action' });

            var table = $('#collections_list').DataTable({
                data: tblData.data,
                ordering: true,
                select: true,
                pageLength: 50,
                columnDefs: [{ width: '40%', targets: 0 }, { width: '20%', targets: 1 }, { width: '5%', targets: 2 }],
                bAutoWidth: false,
                fixedColumns: true,
                columns: columnNames,
                createdRow: function (row, data, dataIndex) {
                    // Set the data-status attribute, and add a class
                    $(row).addClass('issue-row');
                    $(row).attr('data-pid', data.pid);
                },
                bDestroy: true,
                drawCallback: function () {
                    addOnClickDataTable();
                }
            });
//            table.on('page.dt', function () {
//                addOnClickDataTable();
//            });
            table.rows().iterator('row', function (context, index) {
                data = $(this.row(index).data());
                console.log("data");
                console.log(data);
                last_evt = getLastEvent(data[0].pid);
                console.log("last_evt");
                console.log(last_evt);
                get_errors = "";
                var action = '<div class="btn btn-group">' +
                    '<button type="button" class="btn btn-danger" onclick="deleteCollection(\'' + data[0].pid + '\')">Supprimer</button>';
                if (last_evt !== "") {
                    return_url_history = url_history + "?search=&col=" + data[0].pid + "&status=error";
                    action += '<button class="btn btn-warning" data-id="' + data + '" onclick="window.location.href=\'' + return_url_history + '\';">Error Import</button></div>';
                }

                var collection = data[0].title;
                var title = '<div>' + data[0].title + '</div>';
                data_r = { "title": title, "issn": data[0].issn, "action": action };

                $(this.row(index).data(data_r));

                $(function () {
                    $('[data-bs-toggle="popover"]').popover();
                });
            });

            $('#collections_list').on("draw.dt", function () {
                $(function () {
                    $('[data-bs-toggle="popover"]').popover();
                });
                console.log("draw");
            }).DataTable();
        });
}


function draw_issues(){
    issues_not_imported = []
    console.log("draw table")
    var columnNames = [];
    var tblData = {"data":[]};
    columnNames.push( { title:'pid', data: 'pid'});

    issues_list.forEach(item => {
      // build an object containing the column names:

      if(typeof(item.pid) == "undefined") {
        item.pid="null"
      }

      tblData["data"].push( {"pid":item.pid} );
    });
    var table= $('#collection_issues').DataTable({
            data: tblData.data,
            ordering:  true,
            pageLength: 50,
            columns: columnNames,
             createdRow: function( row, data, dataIndex ) {
            // Set the data-status attribute, and add a class
            $( row ).addClass('issue-row');
            },
            bDestroy: true
     });

    table.rows().iterator( 'row', function ( context, index ) {
         data=$(this.row(index).data())



    } );
    $('#collection_issues').on("draw.dt", function() {
           $(function () {
                    $('[data-bs-toggle="popover"]').popover();
             });
            console.log("draw")
    }).DataTable()




}

function addOnClickDataTable() {
    var table = $('#collections_list').DataTable();
    table.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var rowNode = this.node();
        var pid_collection = $(rowNode).attr('data-pid');
        $(rowNode).find('td:first-child, td:nth-child(2)').css('cursor', 'pointer').attr('onclick', 'gotoEdit(\'' + pid_collection + '\')');
    });
}

