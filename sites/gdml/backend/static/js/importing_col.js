var end_progress = ""
var last_task = ""
var fails = ""


function displayProgressBar() {
    $('#importing-progress').removeClass('hidden')
}

function updateProgressBar(progress, error_rate) {
    $('.progress-bar-success').css('width', progress+'%').text(progress+'%');
    $('.progress-bar-warning').css('width', error_rate+'%').text(error_rate+'%');
}

function updateReport(result) {
    end_progress=result.remaining
    console.log("result")
    last_task = result.last_task
    fails = result.fails
    collection = result.collection
    remaining = "<div class='col-md-3'><p>Remaining tasks : " + result.remaining
    fails = "<br>Errors : " + result.fails
    successes = "<br>Successes : " + result.successes + '</p></div>'
    if(typeof last_task !== "undefined" && last_task != null && last_task != "" ){
        if(last_task.length > 0 && last_task !=="" ){
        last_task = last_task.split(" : ")
        date_task = last_task[0]
        }

       /** if(last_task.length > 1) {
            var message = JSON.parse(last_task[1]).split(",")
        }**/
    }

    else {
        d=new Date()
        date_task=d.toISOString()
        date_task=date_task.split("T")
        date_task=date_task[0]
    }

    var message = {}


    var target = ""
     if(message.length == 1) {
        url_message = message[1].split('\':')
        url  = url_message[1].replace('\'','').replace('}','')
        url = url.replace('\'','')
        target = url
    }
    else {
        if (message.length > 0) {
            var target = message
            target.shift()
            for (var i =0; i<= target.length -1; i++){
                console.log(i)
                target[i] = target[i].replace(')','')
            }
        }

    }


    //   last_task =  date_task +" :  <b> Import  [" + target +"]</hb>"
    last_cmd=""
    console.log(">>>")
    console.log(result.periode.length)
    result_periode=false
    result_numbers=false
    if(result.periode.length > 0) {
       if(result.periode[0] != null && result.periode[0] != ""  ){
        let periode_input =  result.periode.length > 1 ? result.periode[0] + " - " + result.periode[1] :  result.periode[0]
        last_cmd =  "<div class='col-md-8'><p> Import de <b>" + result.titre + "</b> sur la période <b>" + periode_input + "</b> par <b>" + result.username +"</b> le <b>" + date_task + "</b></p></div>"
        result_periode=true
        }
    }
    if(result.numbers.length > 0) {
        let numbers = result.numbers.split(",")
        if(result.numbers[0] != null && result_periode == false ){
           last_cmd =  "<div class='col-md-8'><p> Import de <b>" + result.titre + "</b> sur la période <b>" + numbers[0] + " - " + numbers[1] + "</b> par <b>" + result.username +"</b> le <b>" + date_task + "</b></p></div>"
           result_numbers=true
        }
    }
    else{
            if( result_periode == false && result_numbers == false){
              last_cmd =  "<div class='col-md-8'><p> Import de la collection <b>" + collection +"</b> par <b>" + result.username +"</b> le <b>" + date_task + "</b></p></div>"
            }
    }

    return remaining + fails + successes + last_cmd
}


function GetProgress() {
console.log("progress");
    $.ajax({
        url: '/backend/import-issues/progress/',
        success: function (result) {
            progress = result.progress
            error_rate = result.error_rate
            total = result.total
            remaining = result.remaining

            $('#results').html(updateReport(result))
            /* Archiving in progress */
            if (result.status == 'consuming_queue') {
                $('#polling').addClass('hidden')
                $('#results').removeClass('hidden')
                if (progress != 100) {
                    displayProgressBar()
                    $('.progress').removeClass('hidden')
                    if(result.fails > 0) {
                      $('.progress-error').removeClass('hidden')
                    }
                    updateProgressBar(progress, error_rate)
                }
                if(total == 0) {
                    $('#importing-progress').addClass('hidden')
                    $('#results').addClass('hidden')
                }
            }

            /* Archiving complete */
            if (progress == 100) {
                updateProgressBar(progress, error_rate)
                clearInterval(progressbar);
                //$('#results').removeClass('hidden')
                $('.progress').addClass('hidden')

                /* celery archiving button is usable again */
                if (result.fails != 0) {
                    collection = result.collection

                    $('#import-errors-btn').attr('href','/dashboard/history?search&col='+collection+'&status=error')
                    $('#import-errors-btn').removeClass('hidden')
                }
                $('#celery-importing').removeClass('disabled')
                $('#last_cmd_btn').removeClass("hidden")


            /* Polling all issues to archive */
            } else if (result.status == 'polling') {

                console.log('poll')

                if (total == 0) {
                    $('#celery-importing').removeClass('disabled')
                } else {
                    displayProgressBar()
                    $('#polling').removeClass('hidden')
                }
                console.log(remaining)

                if(result.remaining == 0) {
                    $('.progress').addClass('hidden')
                    $('#importing-progress').addClass('hidden')

                    $('#celery-importing').addClass('disabled')
                    console.log("last task")



                }
            }
             if(end_progress == 0 && total > 1 ) {
             console.log("fin tache")
                $("#results").html("")
               if( result.fails > 0) {
                 //$("#results").append(fails)
                 $("#archive-errors-btn").addClass('btn btn-xs btn-warning')
               }
                if(last_task == null){
                     $('#results').addClass('hidden')

                    //$("#results").append("<div class='card'><div class='card-header'>Dernière commande: </div> <p>"+last_task+"</p></div>")

                }

            }
        },
        error: function (err) {
        console.log(err);
        }
    });
}

progressbar=setInterval(GetProgress, 1000);
