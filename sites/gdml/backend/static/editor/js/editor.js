$(document).ready(function() {

    $('.create-form-long-action').click(function() {
        if ($('#checkbox_author').is(':checked') && $('#checkbox_review').is(':checked')) {
            $('.spinner-border').show();
        }
     });

     $('.long-action').click(function() {
        $('.spinner-border').show();
     });

//    $('.checkbox-of-submission').click(function() {
//        if ($('#checkbox_author').is(':checked') && $('#checkbox_review').is(':checked')) {
//            $('#submit-submission').removeClass('disabled');
//        } else {
//            $('#submit-submission').addClass('disabled');
//        }
//    });

    $('.submit-form-with-popup').click(function(){
        var data = $(this).attr("data-form");
        $('#is-submit').val("submit");
        console.log("submit", data)
        $('#' + data).submit();
    });


  let navLinks = document.querySelectorAll("sidenav a");
  window.addEventListener("scroll", event => {
    let fromTop = window.scrollY;

    navLinks.forEach(link => {
      let section = document.querySelector(link.hash);
      if (section) {
        if (
          section.offsetTop <= fromTop &&
          section.offsetTop + section.offsetHeight > fromTop
        ) {
          link.classList.add("current");
        } else {
          link.classList.remove("current");
        }
      }
    });
  });

  $(".save-reminder").click(
    function(e) {
      var elt = document.getElementById("id-save");
      var saved = elt.classList.contains("btn-needs-save");
      if (saved) {
        if (confirm('You haven’t saved your data. Are you sure ?')) {
            return true;
        }
        e.preventDefault();
        return false;
      }
    }
  )

});
