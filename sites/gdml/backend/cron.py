import re
from subprocess import PIPE
from subprocess import Popen

from middleware.redis_command import redisClient

###   Methods to restart EuDML process crawl ###


def reset_progression_eudml():
    """

    @return:
    """
    client = redisClient()
    r = client.get_connection()
    # reset count eudml scrap process
    client.exec_cmd(r.set, "start_crawl", 0)
    client.exec_cmd(r.set, "failures_collection", 0)
    client.exec_cmd(r.set, "progress", 0)
    print("redis cmd executed")
    print(client.exec_cmd(r.get, "progress"))

    with Popen('ps -ef | grep "python3"', shell=True, stdout=PIPE, stderr=PIPE) as process:
        stdout, stderr = process.communicate()
        print("process check")
        print(stdout)
        reg_crawl_process = re.compile("crawl -eudml")
        get_process_eudml = False

        for line in stdout.splitlines():
            print(line)
            if reg_crawl_process.search(str(line)):
                print("doing process")
                args = line.decode()
                args = args.split(" ")
                pid = args[3]
                print(pid)
                Popen("kill -9 " + pid, shell=True, stdout=PIPE, stderr=PIPE)
    with Popen('supervisorctl stop crawl', shell=True, stdout=PIPE, stderr=PIPE) as process:
        stdout, stderr = process.communicate()
        print("stop crawl EUDML process")
        print(stdout)
        print(stderr)

    with Popen('supervisorctl stop restart_crawl', shell=True, stdout=PIPE, stderr=PIPE) as process:
        stdout, stderr = process.communicate()
        print("stop (re)start crawl EUDML process")
        print(stdout)
        print(stderr)

    return True

def start_crawl_eudml():
    """
    @param username:
    @param pid:
    @return:
    """
    client = redisClient()
    r = client.get_connection()
    reset_crawl =reset_progression_eudml()

        # reset count eudml scrap process
    if reset_crawl == True:

        """
            Start Eudml crawl process
        """
        with Popen('supervisorctl start crawl', shell=True, stdout=PIPE, stderr=PIPE) as process:
            stdout, stderr = process.communicate()
            print("restart EUDML crawl process")
            print(stdout)
            print(stderr)

        return "ok"

def restart_crawl_eudml(username):
    """

    @param username:
    @param pid:
    @return:
    """

    client = redisClient()
    r = client.get_connection()
    reset_crawl =reset_progression_eudml()

        # reset count eudml scrap process
    if reset_crawl == True:

        """
            Start Eudml crawl process
        """
        with Popen('supervisorctl start restart_crawl', shell=True, stdout=PIPE, stderr=PIPE) as process:
            stdout, stderr = process.communicate()
            print("restart EUDML crawl process")
            print(stdout)
            print(stderr)

        return "ok"


if __name__ == "__main__":
    start_crawl_eudml("mathdoc-2", "AFRNUC")
