from import_export import fields
from import_export import resources
from import_export.admin import ImportExportActionModelAdmin
from import_export.admin import ImportExportModelAdmin
from import_export.widgets import ForeignKeyWidget

from django.contrib import admin

from gdml.admin import geodesic_admin
from gdml.models import Periode
from gdml.models import Publisher
from gdml.models import Source
from history.models import HistoryEvent
from ptf.models import Collection


class PublisherInline(ImportExportModelAdmin):
    model = Publisher
    list_display = (
        "pub_key",
        "pub_name",
    )


class CollectionResource(resources.ModelResource):
    class Meta:
        model = Collection
        import_id_fields = ("pid",)
        exclude = ("resource_ptr", "id")
        fields = (
            "pid",
            "title_html",
            "title_tex",
            "title_xml",
            "lang",
            "coltype",
            "title_sort",
            "wall",
            "fyear",
            "lyear",
            "alive",
            "provider",
        )

    def get_export_order(self):
        return (
            "pid",
            "title_html",
            "title_tex",
            "title_xml",
            "lang",
            "coltype",
            "title_sort",
            "wall",
            "fyear",
            "lyear",
            "alive",
            "provider",
        )


@admin.register(Collection, site=geodesic_admin)
class CollectionAdmin(ImportExportActionModelAdmin):
    resource_classes = [CollectionResource]
    exclude = ("resource_ptr", "id")

    list_display = (
        "pid",
        "title_html",
    )
    list_display_links = ("pid",)
    search_fields = ("pid", "title_tex", "title_html")
    ordering = ["pid"]


class SourceInline(ImportExportActionModelAdmin):
    model = Source
    list_display = (
        "name",
        "method",
        "id",
        "article_href",
        "pdf_href",
        "website",
        "domain",
        "periode_href",
    )


class PeriodeResource(resources.ModelResource):
    source = fields.Field(
        column_name="source",
        attribute="source",
        widget=ForeignKeyWidget(Source, field="name", use_natural_foreign_keys=True),
    )
    collection = fields.Field(
        column_name="collection",
        attribute="collection",
        widget=ForeignKeyWidget(Collection, field="pid", use_natural_foreign_keys=True),
    )

    publisher = fields.Field(
        column_name="publisher",
        attribute="publisher",
        widget=ForeignKeyWidget(Publisher, field="pub_name"),
    )

    class Meta:
        model = Periode
        import_id_fields = ("collection_href",)
        fields = (
            "collection",
            "source",
            "title",
            "issue_href",
            "collection_href",
            "doi_href",
            "published",
            "begin",
            "end",
            "first_issue",
            "last_issue",
            "publisher",
            "wall",
        )


@admin.register(Source, site=geodesic_admin)
class SourceAdmin(ImportExportModelAdmin):
    list_display = ("name",)
    search_fields = ("name",)


@admin.register(Periode, site=geodesic_admin)
class PeriodeAdmin(ImportExportActionModelAdmin):
    resource_classes = [PeriodeResource]
    list_display = (
        "id",
        "collection",
        "source",
        "doi_href",
        "issue_href",
        "begin",
        "end",
        "first_issue",
        "last_issue",
    )
    list_display_links = ("id", "collection")
    ordering = ["collection"]
    search_fields = ["id", "title", "collection__pid", "collection__title_html", "begin", "end"]


geodesic_admin.register(HistoryEvent)
