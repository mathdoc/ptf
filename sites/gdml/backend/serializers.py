from rest_framework import serializers

from gdml.models import Periode
from gdml.models import Source
from history.models import HistoryEvent
from ptf.models import Abstract
from ptf.models import Collection
from ptf.models import Provider
from ptf.models import ResourceId


class ResourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = ResourceId
        fields = "__all__"


class CollectionLinkSerializer(serializers.ModelSerializer):
    """
    Serializer for nested Collection objects.
    """

    class Meta:
        model = Collection
        fields = ["pid"]


class ProviderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Provider
        fields = ["id", "name"]


class AbstractSerializer(serializers.ModelSerializer):
    class Meta:
        model = Abstract
        fields = ["id", "lang", "seq", "value_html"]


class CollectionNestedSerializer(serializers.ModelSerializer):
    """
    Serializer for Collection objects.
    """

    class Meta:
        model = Collection
        fields = ["pid", "title_html"]
        depth = 1


class CollectionSerializer(serializers.ModelSerializer):
    """
    Serializer for Collection objects.
    """

    resourceid_set = ResourceSerializer(many=True, read_only=False)

    class Meta:
        model = Collection
        fields = [
            "id",
            "pid",
            "doi",
            "title_html",
            "title_tex",
            "alive",
            "lang",
            "coltype",
            "abbrev",
            "doi",
            "wall",
            "fyear",
            "lyear",
            "resourceid_set",
        ]
        depth = 1


class SourceSerializer(serializers.ModelSerializer):
    collections = CollectionNestedSerializer(many=True, read_only=True)

    class Meta:
        model = Source
        fields = [
            "id",
            "name",
            "domain",
            "website",
            "article_href",
            "periode_href",
            "pdf_href",
            "collections",
        ]


class PeriodeSerializer(serializers.ModelSerializer):
    source = SourceSerializer(many=False, read_only=True)
    collection = CollectionLinkSerializer(many=False, read_only=True)

    class Meta:
        model = Periode
        fields = [
            "id",
            "title",
            "collection",
            "source",
            "collection_href",
            "issue_href",
            "begin",
            "end",
            "doi_href",
            "wall",
            "published",
            "first_issue",
            "last_issue",
        ]


class HistoryEventSerializer(serializers.ModelSerializer):
    class Meta:
        model = HistoryEvent
        fields = ["pk", "pid", "created_on", "status", "type", "col", "data"]

        depth = 1
