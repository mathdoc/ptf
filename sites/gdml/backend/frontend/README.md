# collection and periodes view and edit- geodesic back-office

Article and Collection component

takes route of geodesic dashboard

* path: "/dashboard",
    > CollectionSelect

* path: "/dashboard/col/:col",
    > CollectionsSelect

* path: "/dashboard/collections-manage",
     > CollectionView,

* path: "/dashboard/collections-manage/:pid",
     > CollectionEdit,

## Project setup

#### in dashboard app set corsheaders

``` CORS_ALLOW_CREDENTIALS = True
CORS_ORIGIN_WHITELIST = (
    'http://localhost:8080',
    ...
) `
CORS_ALLOW_CREDENTIALS = True


CORS_ALLOWED_ORIGINS = [
"http://localhost",
 ...

]

CORS_ALLOW_HEADERS = (
    'accept',
    'accept-encoding',
    'authorization',
    'content-type',
    'dnt',
    'origin',
    'user-agent',
    'x-csrftoken',
    'x-requested-with',
)

CORS_ALLOW_METHODS = [
'DELETE',
'GET',
'OPTIONS',
'PATCH',
'POST',
'PUT',
]
```

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```
> collection compiled is in ``` static/vuejs ```

### Lints and fixes files

```
npm run lint
```



### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).


# Theme for dashboard

### compile scss

     / scss
        / bootstrap.min.css
          bootstrap-icons.scss
          custom.scss

>   npm run build-css

     set bootstrap-theme in dashboard/static/css
