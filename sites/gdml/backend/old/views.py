import json
import re

from django_celery_results.models import TaskResult
from pyrabbit.api import Client

from django.contrib import messages
from django.contrib.auth.mixins import UserPassesTestMixin
from django.http.response import JsonResponse
from django.shortcuts import redirect
from django.views.generic.base import View

from gdml.models import Periode
from gdml.tasks import import_issue
from ptf.models import Collection


def get_messages_in_queue():
    cl = Client("localhost:15672", "guest", "guest")
    vhost = cl.get_vhost_names()[0]
    messages = cl.get_queue_depth(vhost, "celery")

    return messages


class ImportIssueView(UserPassesTestMixin, View):
    def test_func(self):
        try:
            groups = self.request.user.groups.values_list("name", flat=True)
            for g in groups:
                if g == "can_manage_resources" or g == "can_manage_import":
                    return True
        except:
            return False

    @staticmethod
    def reset_task_results():
        TaskResult.objects.all().delete()

    def post(self, request, *args, **kwargs):
        body_unicode = request.body.decode("utf-8")
        body_data = json.loads(body_unicode)
        colid = body_data.get("colid")
        issue_url = body_data.get("issue_url")
        collection = Collection.objects.get(pid=colid)
        pid = body_data.get("pid")
        reg_year = re.compile(r"\d{4}")
        match_year = reg_year.search(pid)
        if match_year:
            year = match_year[0]
            periode_range = list([year, year])

            periode = Periode.objects.filter(collection=collection, periode__overlap=periode_range)
            if periode:
                periode = periode.first()
            else:
                periode = Periode.objects.filter(collection=collection).first()

            print("messages in queue")
            if not get_messages_in_queue():
                self.reset_task_results()
            import_task = import_issue.delay(colid=periode.pk, url=issue_url)

            return JsonResponse({"taskid": import_task.task_id}, status=202)
        else:
            return JsonResponse({"task": "not imported"}, status=200)

    def handle_no_permission(self):
        # add custom message
        messages.error(self.request, "Permission requise")
        self.login_url = "login"
        return redirect(self.login_url)
