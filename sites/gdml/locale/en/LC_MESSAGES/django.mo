��            )   �      �     �  <   �     �     �       	     �     3   �     �     �     �     
          0     G     V     l  
   x  '   �  &   �     �     �     �     �     �               	  '       6  2   D     w     �  
   �     �  {   �  /        N     _     e     q     ~     �     �     �  	   �     �     �     �     �               #     )     5     9     <     
                                                                                                                 	                    Afficher les articles Articles acceptés à paraître dans une prochaine livraison Articles à paraître Auteur Bibliographie Fascicule Geodesic, la bibliothèque numérique de mathématiques de Mathdoc, contient actuellement %(articles_count)s articles de %(size_sources)s  Inclure les e-prints dans la recherche (arXiv, HAL) Liste complète des exposés Livres Pages finales Pages préliminaires Pages spéciales Pages supplémentaires Pages volantes Parcourir les volumes Plein texte Rechercher Rechercher des articles, des auteurs... Rechercher des articles, des livres... Revues Sommaire Séminaires Titre Tome complet Tout ou éd. Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-02-14 10:03+0100
Last-Translator: Simon Panay <simon.panay@univ-grenoble-alpes.fr>
Language-Team: 
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.1
 View articles Accepted articles to appear in a forthcoming issue Articles to appear Author References Issue Geodesic, the Mathdoc digital mathematics library, is currently indexing %(articles_count)s articles from %(size_sources)s  Include e-prints in search results (arXiv, HAL) List of lectures Books Back Matter Front Matter Special pages Additional pages Loose sheets Browse issues Full text Search Search articles, authors... Search articles, books... Journals Table of contents Seminars Title Full volume All or ed. 