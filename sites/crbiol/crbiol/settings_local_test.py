import os

DATABASES = {"default": {"ENGINE": "django.db.backends.sqlite3"}}

RESOURCES_ROOT = "tests/data"
SOLR_URL = "http://localhost:8983/solr/core0-test"
MAX_RESULT_SIZE = 7
REPOSITORIES = {"http://127.0.0.1/oai/": "NumdamRepository"}
SECRET_KEY = "dsfqsdlkfjlkq"
TEMP_FOLDER = "/tmp"
MERSENNE_TMP_FOLDER = "/tmp"
MERSENNE_LOG_FILE = MERSENNE_TMP_FOLDER + "/transformations.log"
LOG_DIR = "/tmp"


#### [BEGIN] Temporary settings for comments_views applications ####
# We use crbiol site to test comments_views applications
# This requires a large set of additional settings that are put here while the
# feature is not activated on the site
INSTALLED_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.sites",
    "django_extensions",
    "modeltranslation.apps.ModeltranslationConfig",
    "crbiol",
    "mersenne_cms",
    "cr_app",
    "ptf",
    "oai",
    "ckeditor",
    "ckeditor_uploader",
    "upload",
    "pubmed",
    "django.contrib.admin",
    "comments_api",
    "mozilla_django_oidc",
    "comments_views.core",
    "comments_views.journal",
]

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PTF_DIR = os.path.dirname(os.path.dirname(BASE_DIR))
APPS_DIR = "%s/apps" % PTF_DIR

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [
            "%s/crbiol/templates" % BASE_DIR,
            "%s/cr_app/templates" % APPS_DIR,
            "%s/ptf/templates/ptf/bs5" % APPS_DIR,
            "%s/oai/templates" % APPS_DIR,
            "%s/pubmed/templates" % APPS_DIR,
            # Warning! The below comments order does matter
            f"{APPS_DIR}/comments_views/journal/templates/bs5",
            f"{APPS_DIR}/comments_views/core/templates",
            # '%s/mersenne_cms/templates' % BASE_DIR,
            BASE_DIR,
            "%s/" % APPS_DIR,
        ],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.template.context_processors.i18n",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "ptf.context_processors.ptf",
            ],
        },
    },
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "comments_views.journal.middleware.OIDCMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "django.contrib.sites.middleware.CurrentSiteMiddleware",
]

from mersenne_cms.settings import CKEDITOR_CONFIGS
from comments_views.core.ckeditor_config import COMMENT_CKEDITOR_CONFIGS

CKEDITOR_CONFIGS.update(COMMENT_CKEDITOR_CONFIGS)

COMMENTS_VIEWS_ARTICLE_COMMENTS = True
# Display the comments thread with max. 1 level of nesting if True
COMMENTS_VIEWS_COMMENTS_NESTING = True

AUTHENTICATION_BACKENDS = [
    # Authentication against our custom OpenID Connect provider (OP)
    "comments_views.journal.auth.customOIDCAuthenticationBackend",
    # Needed to login by username in Django admin (only in test/preprod no ?)
    "django.contrib.auth.backends.ModelBackend",
]

COMMENTS_VIEWS_DELETE_BEFORE_MODERATION = False

# The following settings are not uses but required for the setup of mozilla_django_oidc
# application.
OIDC_RP_CLIENT_ID = "id"
OIDC_RP_CLIENT_SECRET = "password"
OIDC_RP_SIGN_ALGO = "RS256"
OIDC_ALLOW_UNSECURED_JWT = False
OIDC_VERIFY_JWT = True
OIDC_VERIFY_KID = True
OIDC_RP_IDP_SIGN_KEY = {
    "alg": "RS256",
    "use": "sig",
    "kid": "aa",
    "e": "AQAB",
    "kty": "RSA",
    "n": "something",
}
OIDC_OP_AUTHORIZATION_ENDPOINT = "auth_endpoint"
OIDC_OP_TOKEN_ENDPOINT = "token_endpoint"
OIDC_OP_USER_ENDPOINT = "user_info_endpoint"
OIDC_OP_BASE_URL = "base_url"

# OIDC scopes requested.
OIDC_RP_SCOPES = "openid profile email authentication"
# Redirect in case the OIDC authentication fails
LOGIN_REDIRECT_URL_FAILURE = "/"

#### [END] Temporary settings for comments_views applications ####
