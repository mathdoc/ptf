# -*- coding: utf-8 -*-
# Created manually on 05/19/2020
from __future__ import unicode_literals

from django.db import migrations
from django.core.management import call_command
from django.conf import settings
from ptf.models import *
from mersenne_cms.models import *

def apply_migration(apps, schema_editor):

    qs = Page.objects.filter(site_id=settings.SITE_ID, mersenne_id="next_articles")
    if qs.count() == 0:
        homepage = Page.objects.get(site_id=settings.SITE_ID, mersenne_id="homepage")

        p = Page(
            mersenne_id="next_articles",
            parent_page=homepage,
            site_id=settings.SITE_ID,
            menu_title_fr="Articles à paraître",
            menu_title_en="Articles to appear",
            state="published",
            slug_fr="article-a-paraitre",
            slug_en="coming-articles",
            editable=False,
            position="top"
        )
        p.save()

def revert_migration(apps, schema_editor):

    Page.sudo_objects.filter(site_id=settings.SITE_ID, mersenne_id="next_articles").delete()


class Migration(migrations.Migration):

    dependencies = [
        (settings.SITE_NAME, '0002_load_initial_data')
    ]

    operations = [
        migrations.RunPython(apply_migration, revert_migration),
    ]
