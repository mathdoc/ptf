from django.conf import settings
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.urls import include
from django.urls import path
from django.urls import re_path

from comments_views.journal import urls as comments_urls
from mersenne_cms import urls as cms_urls
from mersenne_cms.admin import mersenne_admin
from oai import urls as oai_urls
from ptf import urls as ptf_urls
from ptf.views import LatestArticlesFeed
from pubmed import urls as pubmed_urls
from upload import urls as upload_urls

urlpatterns = [
    path("", include(ptf_urls)),
    path("", include(cms_urls)),
    path("", include(oai_urls)),
    path("", include(pubmed_urls)),
    path("", include(comments_urls)),
]

urlpatterns += i18n_patterns(
    re_path(r"^latest/feed/(?P<name>[\w]+)/$", LatestArticlesFeed(), name="latest-articles-feed"),
)

if settings.ALLOW_ADMIN:
    urlpatterns += [
        path("upload/", include(upload_urls)),
        re_path(r"^admin/", mersenne_admin.urls),
        path("accounts/", include("django.contrib.auth.urls")),
    ]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
