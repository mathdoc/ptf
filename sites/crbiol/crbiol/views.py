from cr_app.views import CRHomeView as AbstractCRHomeView

from ptf.site_register import SITE_REGISTER


class CRBiolHomeView(AbstractCRHomeView):
    _sites = [SITE_REGISTER["crbiol"]["site_id"]]

    def _get_class(self):
        return CRBiolHomeView
