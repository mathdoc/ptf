"""editor URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  re_path(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  re_path(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import include
    2. Add a URL to urlpatterns:  re_path(r'^blog/', include('blog.urls'))
"""

from ckeditor_uploader.views import upload

from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.urls import include
from django.urls import path
from django.urls import re_path
from django.views.decorators.cache import never_cache

# from editor.views import ArticleEditBodyViewfrom editor.views import ArticleConvertAPIView
from editor.views import ArticleConvertAPIView
from editor.views import ArticleCrossrefAPIView
from editor.views import ArticleDeleteBodyHtmlAPIView
from editor.views import ArticleEditView
from editor.views import ArticleInSubmissionEditAPIView
from editor.views import ArticlePreView
from editor.views import ArticleRefsCrossrefAPIView
from editor.views import ArticleSaveConvertAPIView
from editor.views import EditorHomeView
from editor.views import GetBibTexRefView
from editor.views import GetDoiRefView
from editor.views import MathJaxCallbackView
from editor.views import MathJaxView
from editor.views import PostPublicationModificationView
from editor.views import PostPublicationUpdateView
from editor.views import SendSubmissionRequest
from editor.views import StaffView
from editor.views import SubmissionAcceptView
from editor.views import SubmissionCreateView
from editor.views import SubmissionDeleteView
from editor.views import SubmissionDownloadSourceView
from editor.views import SubmissionFinalizeView
from editor.views import SubmissionListView
from editor.views import SubmissionPostedView
from editor.views import SubmissionPublishAPIView
from editor.views import SubmissionRevertView
from editor.views import SubmissionUpdateView
from editor.views import file_browse_in_article
from editor.views import file_upload_in_article
from matching import urls as matching_urls
from mersenne_cms import urls as cms_urls
from oai import urls as oai_urls
from ptf import urls as ptf_urls
from ptf.urls import ptf_i18n_patterns
from upload import urls as upload_urls

urlpatterns = [
    path('', EditorHomeView.as_view(), name='home'),
    re_path(r'^api-article-edit/(?P<colid>[A-Z-]+)/(?P<doi>.+)/$', ArticleInSubmissionEditAPIView.as_view(), name='api-edit-article'),
    path('api-article-publish/<path:doi>/', SubmissionPublishAPIView.as_view(), name='api-publish-article'),
    re_path(r'^article-edit/(?P<colid>[A-Z-]+)/(?P<doi>.+)/$', ArticleEditView.as_view(), name='edit-article'),
    re_path(r'^article-preview/(?P<colid>[A-Z-]+)/(?P<doi>.+)/$', ArticlePreView.as_view(), name='preview-article'),
    re_path(r'api-fetch-crossref/(?P<colid>[A-Z-]+)/(?P<doi>.+)/$', ArticleCrossrefAPIView.as_view(), name='api-fetch-crossref'),
    re_path(r'api-fetch-refs-crossref/(?P<colid>[A-Z-]+)/(?P<doi>.+)/$', ArticleRefsCrossrefAPIView.as_view(), name='api-fetch-refs-crossref'),
    re_path(r'^article-edit-body/(?P<colid>[A-Z-]+)/(?P<doi>.+)/$', ArticleConvertAPIView.as_view(), name='convert-file' ),
    re_path(r'^article-delete-body/(?P<colid>[A-Z-]+)/(?P<doi>.+)/$', ArticleDeleteBodyHtmlAPIView.as_view(), name='delete-convert' ),
    re_path(r'^confirm-article-conversion/(?P<colid>[A-Z-]+)/(?P<doi>.+)/$', ArticleSaveConvertAPIView.as_view(), name='save-convert-page' ),
    re_path(r'^post_publication_modification/(?P<colid>[A-Z-]+)/(?P<doi>.+)/$', PostPublicationModificationView.as_view(), name='post-publication-modification' ),
    re_path(r'^post_publication_update/(?P<colid>[A-Z-]+)/(?P<doi>.+)/$', PostPublicationUpdateView.as_view(), name='post-publication-update' ),
    path('send-submission-request/<int:pk>/', SendSubmissionRequest.as_view(), name='send-submission-request' ),

    path('all-submissions/', StaffView.as_view(), name='staff-home'),
    path('submission/', SubmissionListView.as_view(), name='submission-list'),

    path('submission/<int:pk>/update/', SubmissionUpdateView.as_view(), name='submission-update'),
    path('submission/<int:pk>/posted/', SubmissionPostedView.as_view(), name='submission-posted'),
    path('submission/<int:pk>/accept/<colid>', SubmissionAcceptView.as_view(), name='submission-accept'),
    path('submission/<int:pk>/revert/', SubmissionRevertView.as_view(), name='submission-revert'),
    path('submission/<int:pk>/delete/', SubmissionDeleteView.as_view(), name='submission-delete'),
    path('submission/<int:pk>/finalize/', SubmissionFinalizeView.as_view(), name='submission-finalize'),

    path('submission/<int:pk>/download/<type>', SubmissionDownloadSourceView.as_view(), name='submission-download'),
    path('submission/create', SubmissionCreateView.as_view(), name='submission-create'),
    path('api-mathjax/', MathJaxView.as_view(), name='api-mathjax'),
    path('api-mathjax-cb/', MathJaxCallbackView.as_view(), name='api-mathjax-cb'),
    path('bibtex-ref/', GetBibTexRefView.as_view(), name='api-bibtex-ref'),
    path('doi-ref/', GetDoiRefView.as_view(), name='api-doi-ref'),

    # path('ckeditor/', include('ckeditor_uploader.urls')),
]

urlpatterns += [
    re_path(
        r"^ckeditor/upload/(?P<colid>[A-Z].+)/(?P<issue_pid>.+)/(?P<pid>.+)/$",
        login_required(file_upload_in_article),
        name="ckeditor_upload_in_article",
    ),
    re_path(
        r"^ckeditor/browse/(?P<colid>[A-Z].+)/(?P<issue_pid>.+)/(?P<pid>.+)/$",
        never_cache(login_required(file_browse_in_article)),
        name="ckeditor_browse_in_article",
    ),
    re_path(r"^ckeditor/upload/", login_required(upload), name="ckeditor_upload"),
    path("ckeditor/", include("ckeditor_uploader.urls")),
]

urlpatterns += cms_urls.urlpatterns_editors

urlpatterns += [
    path('', include(ptf_urls)),
    path('', include(oai_urls)),
    path('', include(matching_urls)),
]

urlpatterns += ptf_i18n_patterns

if settings.ALLOW_ADMIN:
    urlpatterns += [
        path('upload/', include(upload_urls)),
        path('admin/', admin.site.urls),
    ]

urlpatterns += ptf_urls.admin_urlpatterns
urlpatterns += [
    path('accounts/', include('allauth.urls')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
