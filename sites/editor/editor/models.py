import os

from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.files.storage import FileSystemStorage
from django.db import models
from django.utils.translation import gettext_lazy as _

from ptf.display.resolver import PCJ_MANDATORY_TOPICS
from ptf.models import Article

STATE_EDIT_INVALID = "edit_invalid"
STATE_EDIT = "edit"
STATE_SUBMITTED_TO_PCJ = "submitted_to_pcj"
STATE_ACCEPTED = "accepted"
STATE_PUBLISHED = "published"
SUBMISSION_STATES = (
    (STATE_EDIT_INVALID, _("Edit not yet valid")),
    (STATE_EDIT, _("Edit")),
    (STATE_SUBMITTED_TO_PCJ, _("Submitted to PCJ")),
    (STATE_ACCEPTED, _("Accepted")),
    (STATE_PUBLISHED, _("Published")),
)


def is_state_lower(state1, state2):
    states = [item[0] for item in SUBMISSION_STATES]
    return states.index(state1) < states.index(state2)


def submission_directory_path(submission, filename):
    return os.path.join(str(submission.pk), filename)


class OverwriteStorage(FileSystemStorage):
    def get_available_name(self, name, max_length=None):
        if self.exists(name):
            os.remove(os.path.join(settings.MEDIA_ROOT, name))
        return name


def validate_file_extension(value):
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = [".pdf"]
    if ext.lower() not in valid_extensions:
        raise ValidationError("Unsupported file extension.")


def validate_image(image):
    min_width = 200
    min_height = 200

    width = image.width
    height = image.height

    if width < min_width or height < min_height:
        raise ValidationError(f"The image has to be at least {min_width}x{min_height} px")


def validate_source_file_extension(value):
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = [".odt", ".tex", ".doc", ".docx"]
    if ext.lower() not in valid_extensions:
        raise ValidationError("Supported file extension: odt, tex, doc, docx")


def validate_bib_file_extension(value):
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = [".bib", ".txt"]
    if ext.lower() not in valid_extensions:
        raise ValidationError("Supported file extension: bib (BibTeX), txt (list of DOIs)")


def is_article_valid(article_data):
    is_valid = True

    if is_valid and len(article_data.title_tex) == 0:
        is_valid = False

    # Disabled on 10/07/2021: for the opening, PCJ wants the references to be optional
    # if is_valid and len([ref for ref in article_data.bibitems if ref.type == 'unknown']) > 0:
    #    is_valid = False

    return is_valid


class Submission(models.Model):
    users = models.ManyToManyField(User)
    article = models.ForeignKey(Article, models.SET_NULL, blank=True, null=True)
    preprint_id = models.CharField(max_length=64, db_index=True, unique=True)
    recommendation_doi = models.CharField(max_length=64, db_index=True)
    pdf = models.FileField(
        upload_to=submission_directory_path,
        blank=True,
        null=True,
        storage=OverwriteStorage(),
        validators=[validate_file_extension],
        max_length=255,
    )
    image = models.ImageField(
        upload_to=submission_directory_path,
        blank=True,
        null=True,
        storage=OverwriteStorage(),
        validators=[validate_image],
        max_length=255,
    )
    source = models.FileField(
        upload_to=submission_directory_path,
        blank=True,
        null=True,
        storage=OverwriteStorage(),
        validators=[validate_source_file_extension],
        max_length=255,
    )
    biblio = models.FileField(
        upload_to=submission_directory_path,
        blank=True,
        null=True,
        storage=OverwriteStorage(),
        validators=[validate_bib_file_extension],
        max_length=255,
    )
    state = models.CharField(
        _("State"), default=STATE_EDIT, choices=SUBMISSION_STATES, blank=False, max_length=255
    )

    def __str__(self):
        users_str = " ".join([user.username for user in self.users.all()])
        return f"Users: {users_str}, preprint: {self.preprint_id}, recommendation:{self.recommendation_doi}"

    def check_validity(self, data_article):
        if is_state_lower(self.state, STATE_SUBMITTED_TO_PCJ):
            is_valid = is_article_valid(data_article)
            state = STATE_EDIT if is_valid else STATE_EDIT_INVALID
            self.state = state
            self.save()

    def is_invalid(self):
        return self.state == STATE_EDIT_INVALID

    def get_corresponding_emails(self):
        emails = []

        author_contributions = self.article.get_author_contributions()
        for contribution in author_contributions:
            if contribution.corresponding and contribution.email:
                emails.append(contribution.email)

        first_user = self.users.first()
        if first_user.email and first_user.email not in emails:
            emails.append(first_user.email)
        # -> pas sûr de la suite
        # else:
        #     emails += str(first_user)

        return emails

    def corresponding_emails_display(self):
        emails = self.get_corresponding_emails()
        display = "\n".join(emails)

        return display

    def is_submission_form_valid(self):
        if (
            self.pdf
            and self.pdf.name
            and self.image
            and self.image.name
            and self.source
            and self.source.name
            and self.biblio
            and self.biblio.name
        ):
            return True
        else:
            return False


def get_article_formulas(article):
    formulas = []
    if article.abstract_set.count() > 0:
        abstract = article.abstract_set.first()
        value_tex = abstract.value_tex
        l_ = value_tex.split('<span class="mathjax-formula">')
        formulas.extend(
            [f"${formula[0:formula.find('</span>')]}$" for formula in l_ if formula[0] == "$"]
        )

    return formulas


def set_article_formulas(article, formulas):
    if article.abstract_set.count() > 0:
        abstract = article.abstract_set.first()
        value_xml = abstract.value_xml
        l_ = value_xml.split("</alternatives>")

        new_value_xml = (
            "".join([f"{x[0]}{x[1]}</alternatives>" for x in zip(l_, formulas)]) + l_[-1]
        )

        # TODO: change value_xml ?
        return new_value_xml


def set_mandatory_topics(article_data):
    pci_section = "".join([subj["value"] for subj in article_data.subjs if subj["type"] == "pci"])
    mandatory_topic = (
        PCJ_MANDATORY_TOPICS[pci_section] if pci_section in PCJ_MANDATORY_TOPICS else None
    )

    if mandatory_topic is not None:
        topics = [
            subj["value"]
            for subj in article_data.subjs
            if subj["type"] == "topic" and subj["value"] == mandatory_topic
        ]

        if len(topics) == 0:
            article_data.subjs.append({"type": "topic", "lang": "en", "value": mandatory_topic})
