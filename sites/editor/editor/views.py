import io
import json
import os
import shutil

import pypandoc
import requests
from braces.views import CsrfExemptMixin
from braces.views import LoginRequiredMixin
from braces.views import StaffuserRequiredMixin
from ckeditor_uploader.views import ImageUploadView
from ckeditor_uploader.views import browse

from django.conf import settings
from django.contrib import messages
from django.core.files.storage import default_storage
from django.core.mail import EmailMultiAlternatives
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.utils.html import strip_tags
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import CreateView
from django.views.generic import ListView
from django.views.generic import TemplateView
from django.views.generic import UpdateView
from django.views.generic import View

from editor.utils import change_html_from_ckeditor
from editor.utils import check_biblio
from editor.utils import check_form_for_breadcrumb
from editor.utils import create_article_biblio
from editor.utils import get_issue_pid
from editor.utils import get_unused_ref
from matching import crossref
from mersenne_cms.views import HomeView
from ptf import model_data_converter
from ptf import model_helpers
from ptf.bibtex import parse_bibtex
from ptf.citedby import bibtex_to_refs
from ptf.cmds import ptf_cmds
from ptf.cmds import xml_cmds
from ptf.cmds.xml.xml_utils import clean_doi
from ptf.model_data import Foo
from ptf.model_data import create_issuedata
from ptf.model_data import create_publisherdata
from ptf.models import Article
from ptf.utils import convert_image_for_web
from ptf.views import ArticleEditAPIView

from .forms import ConversionDocxHtmlForm
from .forms import SubmissionForm
from .models import STATE_ACCEPTED
from .models import STATE_EDIT
from .models import STATE_EDIT_INVALID
from .models import STATE_PUBLISHED
from .models import STATE_SUBMITTED_TO_PCJ
from .models import Submission
from .models import is_state_lower
from .models import set_article_formulas
from .models import set_mandatory_topics

# from urllib.parse import urlencode


def get_media_base_root(colid, article_pid="", issue_pid=""):
    """
    Base folder where media files are stored in Editor
    """
    if colid in ["CRMECA", "CRBIOL", "CRGEOS", "CRCHIM", "CRMATH", "CRPHYS"]:
        colid = "CR"
        return os.path.join(settings.RESOURCES_ROOT, "media", colid)
    elif colid in ["PCJ"]:
        # path = os.path.join(settings.RESOURCES_ROOT, "media", colid, article_pid)
        path = os.path.join(
            settings.RESOURCES_ROOT, "EDITOR", "media", "img", issue_pid, article_pid
        )
        return path


def get_media_base_root_in_test(colid, article_pid="", issue_pid=""):
    """
    Base folder where media files are stored in the test website
    Use the same folder as the Trammel media folder so that no copy is necessary when deploy in test
    """
    return get_media_base_root(colid, article_pid, issue_pid)


def get_media_base_root_in_prod(colid, article_pid="", issue_pid=""):
    """
    Base folder where media files are stored in the prod website
    """
    if colid in ["CRMECA", "CRBIOL", "CRGEOS", "CRCHIM", "CRMATH", "CRPHYS"]:
        colid = "CR"
        return os.path.join(settings.RESOURCES_ROOT, "media", colid)
    elif colid in ["PCJ"]:
        return os.path.join(settings.RESOURCES_ROOT, colid, issue_pid, article_pid)

    # {issue_pid}


def get_media_base_url(colid, article_pid="", issue_pid=""):
    path = os.path.join(settings.MEDIA_URL, colid, article_pid)

    if colid in ["CRMECA", "CRBIOL", "CRGEOS", "CRCHIM", "CRMATH", "CRPHYS"]:
        prefixes = {
            "CRMECA": "mecanique",
            "CRBIOL": "biologies",
            "CRGEOS": "geoscience",
            "CRCHIM": "chimie",
            "CRMATH": "mathematique",
            "CRPHYS": "physique",
        }
        path = f"/{prefixes[colid]}{settings.MEDIA_URL}/CR"
    if colid in ["PCJ"]:
        path = os.path.join(settings.MEDIA_URL, "img", issue_pid, article_pid)
    #     # path = os.path.join(settings.RESOURCES_ROOT, colid, issue_pid, article_pid)
    #     path = os.path.join("media", "img", issue_pid, article_pid)
    return path


def change_ckeditor_storage(colid, article_pid="", issue_pid=""):
    """
    By default, CKEditor stores all the files under 1 folder (MEDIA_ROOT)
    We want to store the files under a subfolder of @colid
    To do that we have to
    - change the URL calling this view to pass the site_id (info used by the Pages to filter the objects)
    - modify the storage location
    """

    from ckeditor_uploader import utils
    from ckeditor_uploader import views

    from django.core.files.storage import FileSystemStorage

    storage = FileSystemStorage(
        location=get_media_base_root(colid, article_pid, issue_pid),
        base_url=get_media_base_url(colid, article_pid, issue_pid),
    )

    utils.storage = storage
    views.storage = storage


class ArticleImageUploadView(ImageUploadView):
    """
    By default, CKEditor stores all the files under 1 folder (MEDIA_ROOT)
    We want to store the files under a subfolder of @colid
    To do that we have to
    - change the URL calling this view to pass the site_id (info used by the Pages to filter the objects)
    - modify the storage location
    """

    def dispatch(self, request, *args, **kwargs):
        colid = kwargs["colid"]
        article_pid = kwargs["pid"]
        issue_pid = kwargs.get("issue_pid", None)

        change_ckeditor_storage(colid, article_pid, issue_pid)

        return super().dispatch(request, **kwargs)


class ArticleBrowseView(View):
    def dispatch(self, request, **kwargs):
        colid = kwargs.get("colid", None)
        article_pid = kwargs.get("pid", None)
        issue_pid = kwargs.get("issue_pid", None)
        change_ckeditor_storage(colid, article_pid, issue_pid)

        return browse(request)


file_upload_in_article = csrf_exempt(ArticleImageUploadView.as_view())
file_browse_in_article = csrf_exempt(ArticleBrowseView.as_view())


class ArticleConvertAPIView(LoginRequiredMixin, TemplateView):
    model = Article
    template_name = "article_edit_body_preview.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["colid"] = settings.COLLECTION_PID
        context["issue_pid"] = get_issue_pid(context["colid"])
        doi = kwargs.get("doi", None)
        article = model_helpers.get_article_by_doi(doi)
        show_breadcrumb = True
        disable_edit = True

        if article:
            submission = Submission.objects.get(article=article)
            if submission:
                context["submission_id"] = submission.pk
                form = ConversionDocxHtmlForm(
                    colid=context["colid"],
                    pid=article.pid,
                    issue_pid=context["issue_pid"],
                    initial={"content": submission.article.body_html},
                )
                context["form"] = form
                context["source"] = submission.source
                show_breadcrumb = is_state_lower(submission.state, STATE_PUBLISHED)
                disable_edit = not (is_state_lower(submission.state, STATE_ACCEPTED))

        context["article"] = article
        context["disable_edit"] = disable_edit
        context["current_page"] = "convert"
        context["show_breadcrumb"] = show_breadcrumb
        context["body_html"] = article.body_html
        collection = model_helpers.get_collection(settings.COLLECTION_PID, sites=False)
        context["collection"] = collection
        context["bs_version"] = (
            settings.BOOTSTRAP_VERSION if hasattr(settings, "BOOTSTRAP_VERSION") else 3
        )
        context["is_submission_valid"] = submission.is_submission_form_valid()
        context["is_article_valid"] = article.title_html != ""
        context["state"] = submission.state
        context["issue_pid"] = get_issue_pid(context["colid"])
        # change_ckeditor_storage(context["colid"], article_pid=context["article"].pid, issue_pid=issue_pid)
        return context

    def post(self, request, **kwargs):
        colid = kwargs.get("colid", None)
        doi = kwargs.get("doi", None)
        article = model_helpers.get_article_by_doi(doi)
        submission = Submission.objects.get(article=article)

        # getting issue_pid
        issue_pid = get_issue_pid(colid)

        # conversion from docx to html
        source = submission.source
        docx_file_path = os.path.join(source.path)
        html_file_path = str(docx_file_path[:-4]) + "html"

        article_media_path = f"{settings.RESOURCES_ROOT}/{colid}/{issue_pid}/{article.pid}/src"

        pandoc_args = [f"--extract-media={article_media_path}", "--mathjax"]

        output = pypandoc.convert_file(
            docx_file_path,
            to="html5",
            format="docx+styles",
            extra_args=pandoc_args,
            outputfile=html_file_path,
        )
        assert output == ""

        # Copy the html file as an article attribute
        imgs_path = f"{article_media_path}/media"
        try:
            imgs = os.listdir(imgs_path)
            for img in imgs:
                # convert_image_for_web check the extension and converts the image if needed
                # it also changes the mode from RGBA to RGB (only ofr tif) and resize it if needed (width > 1600px)
                convert_image_for_web(f"{imgs_path}/{img}")

        except FileNotFoundError:
            pass

        # Copy the html file as an article attribute
        with open(html_file_path, encoding="utf-8") as html_content:
            html_text = html_content.read()

        submission.article.body_html = html_text.replace(r"\[", "").replace(r"\]", "")

        # Creation of article bibliography to create inner link for citations
        biblio = create_article_biblio(submission)

        submission = change_html_from_ckeditor(
            submission, colid, check_citation=True, biblio=biblio
        )
        with open(html_file_path, "w", encoding="utf-8") as html_content:
            html_content.write(submission.article.body_html)
        submission.article.save()
        prefix = request.get_host()
        base_url = (
            f"http://{prefix}{reverse('save-convert-page', kwargs={'colid':colid, 'doi':doi})}"
        )

        # query_string = urlencode({"checked_biblio": checked_biblio})
        # url = f"{base_url}?{query_string}"
        # return ArticleSaveConvertAPIView.as_view(checked_biblio=checked_biblio)(request, colid=colid, doi=doi, checked_biblio=checked_biblio)
        # try:
        #     response = requests.post(base_url, files=files, data={"coucou": "coucou"})
        # except Exception as e:
        #     print(e)
        return redirect(base_url)
        # return requests.get(url, data={"checked_bilio": checked_biblio})
        # return JsonResponse({"response": response.status_code})
        return JsonResponse({"message": "ok"})


class ArticleDeleteBodyHtmlAPIView(LoginRequiredMixin, TemplateView):
    model = Article
    template_name = "article_edit_body_preview.html"
    show_breadcrumb = False

    def post(self, request, **kwargs):
        colid = kwargs.get("colid", None)
        doi = kwargs.get("doi", None)
        article = model_helpers.get_article_by_doi(doi)
        submission = Submission.objects.get(article=article)

        # getting issue_pid
        issue_pid = get_issue_pid(colid)

        # reset article body html to none
        article.body_html = ""
        article.save()

        # delete html file from conversion
        source = submission.source
        docx_file_path = os.path.join(source.path)
        html_file_path = str(docx_file_path[:-4]) + "html"
        default_storage.delete(html_file_path)

        # deleting images from conversion
        article_media_path = (
            f"{settings.RESOURCES_ROOT}/{colid}/{issue_pid}/{article.pid}/src/media"
        )
        if os.path.isdir(article_media_path):
            shutil.rmtree(article_media_path)

        return redirect("convert-file", colid, doi)


class ArticleEditView(LoginRequiredMixin, TemplateView):
    """
    Edit Article Metadata page
    The HTML page uses a vuejs widget developed outside this git
    """

    template_name = "article_edit.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["colid"] = settings.COLLECTION_PID

        doi = kwargs.get("doi", None)
        article = model_helpers.get_article_by_doi(doi)
        show_breadcrumb = False
        disable_edit = True
        if article:
            submission = Submission.objects.get(article=article)
            if submission:
                context["submission"] = submission
                context["submission_id"] = submission.pk
                show_breadcrumb = is_state_lower(submission.state, STATE_PUBLISHED)
                disable_edit = not (is_state_lower(submission.state, STATE_ACCEPTED))
                context["is_submission_valid"] = submission.is_submission_form_valid()

        context["article"] = article
        context["current_page"] = "edit"
        context["show_breadcrumb"] = show_breadcrumb
        context["disable_edit"] = disable_edit

        context["is_article_valid"] = article.title_html != ""
        context["state"] = submission.state
        if self.request.user.is_staff:
            context["is_staff"] = "true"
        else:
            context["is_staff"] = "false"
        return context


# class ArticleInSubmissionEditAPIView(CsrfExemptMixin, LoginRequiredMixin, ArticleEditAPIView):
class ArticleInSubmissionEditAPIView(CsrfExemptMixin, ArticleEditAPIView):
    """
    API to get/post article metadata
    The class is derived from ArticleEditAPIView (see ptf.views) to keep the submission up to date
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.submission = None
        self.edit_all_fields = True
        # self.users = []

    def convert_data_for_editor(self, data_article):
        super().convert_data_for_editor(data_article)
        data_article.is_staff = self.request.user.is_staff
        data_article.bibitems_with_ordered_label = True

    def save_data(self, data_article):
        article = model_helpers.get_article_by_doi(self.doi)
        self.submission = Submission.objects.get(article=article)
        self.submission.check_validity(data_article)

        # self.submission = Submission(pk=submission.pk)
        # self.submission.pk = None
        # self.submission.article = None
        # for user in submission.users.all():
        #     self.users.append(user)

    def restore_data(self, article):
        self.submission.article = article
        self.submission.save()
        # for user in self.users:
        #     self.submission.users.add(user)


class ArticleCrossrefAPIView(ArticleInSubmissionEditAPIView):
    """
    API to let the user fetch metadata from Crossref (from the Edit Article Metadata page)
    This will replace the references
    """

    def post(self, request, *args, **kwargs):
        self.colid = kwargs.get("colid", None)
        self.doi = kwargs.get("doi", None)

        submission = None
        article = model_helpers.get_article_by_doi(self.doi)
        if article:
            submission = Submission.objects.get(article=article)

        if submission:
            try:
                crossref_data = crossref.fetch_article(submission.preprint_id)

                article_data = model_data_converter.db_to_article_data(article)
                article_data.bibitems = crossref_data.bibitems

                self.update_article(article_data, None)

                result = HttpResponseRedirect(
                    reverse("edit-article", kwargs={"colid": self.colid, "doi": self.doi})
                )

            except requests.exceptions.HTTPError as e:
                if e.response.status_code == 504:
                    messages.error(
                        request,
                        "Crossref is currently unavailable and prevents us from fetching your article metadata. Please try again later",
                    )
                    result = HttpResponseRedirect(reverse("staff-home"))
                else:
                    raise

        return result


class ArticlePreView(ArticleEditView):
    """
    Preview page using the ptf common templates
    TODO: use the PCJ template
    """

    template_name = "article_preview.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["current_page"] = "preview"
        context["display_errors"] = self.request.user.is_staff
        context["bs_version"] = (
            settings.BOOTSTRAP_VERSION if hasattr(settings, "BOOTSTRAP_VERSION") else 3
        )

        article = context["article"]

        context["show_breadcrumb"] = True
        submission = Submission.objects.get(article=article)
        context["submission_ended"] = (
            not self.request.user.is_staff and is_state_lower(STATE_EDIT, submission.state)
        ) or (
            self.request.user.is_staff and is_state_lower(STATE_SUBMITTED_TO_PCJ, submission.state)
        )

        context["body_html"] = article.body_html
        context["show_send_submission"] = check_form_for_breadcrumb(submission)
        context["is_submission_valid"] = submission.is_submission_form_valid()
        context["is_article_valid"] = article.title_html != ""
        return context


class ArticleRefsCrossrefAPIView(ArticleInSubmissionEditAPIView):
    """
    API to let the user fetch metadata of all the references from Crossref from a list of DOIs
    This will replace all the article references
    """

    def post(self, request, *args, **kwargs):
        self.colid = kwargs.get("colid", None)
        self.doi = kwargs.get("doi", None)

        article = get_object_or_404(Article, doi=self.doi)

        if "filename" in request.FILES:
            filename = request.FILES["filename"]
            dois = filename.read().decode("utf-8").splitlines()

            bibitems = []
            for doi in dois:
                doi = clean_doi(doi)
                ref_data = crossref.crossref_request_reference(doi)
                model_data_converter.update_ref_data_for_jats(ref_data, 0)
                bibitems.append(ref_data)

            article_data = model_data_converter.db_to_article_data(article)
            article_data.bibitems = bibitems

            self.update_article(article_data, None)

        return HttpResponseRedirect(
            reverse("edit-article", kwargs={"colid": self.colid, "doi": self.doi})
        )


@method_decorator(csrf_exempt, name="dispatch")
class ArticleSaveConvertAPIView(LoginRequiredMixin, TemplateView):
    template_name = "article_edit_body_preview.html"
    # checked_biblio = ""

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["colid"] = kwargs.get("colid", None)
        doi = kwargs.get("doi", None)
        article = model_helpers.get_article_by_doi(doi)
        show_breadcrumb = True
        disable_edit = True
        if article:
            submission = Submission.objects.get(article=article)
            if submission:
                context["submission_id"] = submission.pk
                context["source"] = submission.source
                form = ConversionDocxHtmlForm(initial={"content": submission.article.body_html})
                context["form"] = form
                show_breadcrumb = is_state_lower(submission.state, STATE_PUBLISHED)
                disable_edit = not (is_state_lower(submission.state, STATE_ACCEPTED))
                biblio = create_article_biblio(submission)
                checked_biblio = check_biblio(article.body_html, biblio)
                unused_ref = get_unused_ref(checked_biblio)
                if unused_ref:
                    context["unused_ref"] = [value for value in unused_ref.values()]

        context["article"] = article
        context["disable_edit"] = disable_edit
        context["current_page"] = "convert"
        context["show_breadcrumb"] = show_breadcrumb
        context["body_html"] = article.body_html
        collection = model_helpers.get_collection(settings.COLLECTION_PID, sites=False)
        context["collection"] = collection
        context["bs_version"] = (
            settings.BOOTSTRAP_VERSION if hasattr(settings, "BOOTSTRAP_VERSION") else 3
        )
        context["is_submission_valid"] = submission.is_submission_form_valid()
        context["is_article_valid"] = article.title_html != ""
        context["state"] = submission.state

        return context
        # checked_biblio = self.request.POST["checked_biblio"]

    def post(self, request, **kwargs):
        colid = kwargs.get("colid", None)
        doi = kwargs.get("doi", None)
        article = model_helpers.get_article_by_doi(doi)
        submission = Submission.objects.get(article=article)

        # we update the html file with the new content
        source = submission.source
        folder = os.path.split(os.path.join(source.path))[0]
        html_file_path = (
            folder
            + "/"
            + os.path.splitext(os.path.split(os.path.join(source.path))[1])[0]
            + ".html"
        )
        form = ConversionDocxHtmlForm(request.POST)
        if form.is_valid():
            new_body_html = form.cleaned_data["content"]
            article.body_html = new_body_html
            article.save()
            with open(html_file_path, "w", encoding="utf-8") as html_file:
                html_file.write(new_body_html)

        return redirect("save-convert-page", colid=colid, doi=doi)


def get_icon(obj):
    """
    Replace the Article.get_icon function (see ptf.models)
    as the icon comes from the Submission object
    :param obj:
    :return:
    """
    icon = None
    submission = Submission.objects.get(article=obj)
    if submission and submission.image:
        icon = submission.image.url
    return icon


def get_binary_files(obj):
    """
    Replace the Article.get_binary_files function (see ptf.models)
    as the PDF comes from the Submission object
    :param obj:
    :return:
    """
    binary_files = {}
    submission = Submission.objects.get(article=obj)
    if submission and submission.pdf:
        binary_files = {"self": {"pdf": submission.pdf.url}}

    return binary_files


# Override Article.icon() to use submission.image instead of an article Extlink
Article.icon = get_icon
Article.get_binary_files_href = get_binary_files


class EditorHomeView(HomeView):
    """
    Home Page: CMS page if user is not logged in
               Redirect to another url based on the user role
    """

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            if request.user.is_staff:
                return HttpResponseRedirect(reverse("staff-home"))

            submissions = request.user.submission_set.all().order_by("-pk")
            if submissions.count() > 0:
                submission = submissions.first()

                if is_state_lower(submission.state, STATE_SUBMITTED_TO_PCJ):
                    return HttpResponseRedirect(
                        reverse("submission-update", kwargs={"pk": submission.pk})
                    )
                else:
                    return HttpResponseRedirect(
                        reverse("submission-posted", kwargs={"pk": submission.pk})
                    )

            else:
                return HttpResponseRedirect(reverse("submission-create"))
        else:
            return HttpResponseRedirect(reverse("account_login"))


def get_bibitems_from_bibtex(txt):
    bibitems = parse_bibtex(txt)
    bibitems = bibtex_to_refs(bibitems)

    refs = []
    for pos, ref in enumerate(bibitems):
        model_data_converter.update_ref_data_for_jats(ref, pos)
        contribs_text = [
            f"{contrib['last_name']}, {contrib['first_name']}" for contrib in ref.contributors
        ]
        ref.contribs_text = "\n".join(contribs_text)
        refs.append(ref)

    def obj_to_dict(obj):
        return obj.__dict__

    json_dump = json.dumps(refs, default=obj_to_dict)
    return json_dump


def get_bibitems_from_dois(txt):
    def obj_to_dict(obj):
        return obj.__dict__

    dois = []
    for doi in txt.splitlines():
        dois.append(doi.strip().replace("https://doi.org/", ""))

    refs = crossref.crossref_request_references(dois)
    return json.dumps(refs, default=obj_to_dict)


class GetDoiRefView(View):
    def post(self, request, *args, **kwargs):
        txt = json.loads(request.body).get("data", "")
        json_dump = get_bibitems_from_dois(txt)
        return HttpResponse(json_dump, content_type="application/json")


class GetBibTexRefView(View):
    def post(self, request, *args, **kwargs):
        bibtex = json.loads(request.body).get("data", "")
        json_dump = get_bibitems_from_bibtex(bibtex)
        return HttpResponse(json_dump, content_type="application/json")


class MathJaxView(TemplateView):
    """
    API View to generate MathML from TeX
    """

    template_name = "mathjax_to_mathml.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["doi"] = self.request.GET.get("doi", "")

        tex_formulas = []
        tex_formulas.append(r"$$x = {-b \pm \sqrt{b^2-4ac} \over 2a}.$$")
        tex_formulas.append(r"$$y = {-b \pm \sqrt{b^2-4ac} \over 2b}.$$")
        context["tex_formulas"] = tex_formulas

        return context

    def get(self, request, *args, **kwargs):
        response = super().get(request, *args, **kwargs)
        return response
        # return HttpResponseRedirect('/')


class MathJaxCallbackView(CsrfExemptMixin, View):
    def post(self, request, *args, **kwargs):
        doi = request.POST.get("doi", None)
        article = model_helpers.get_article_by_doi(doi)

        if article is not None:
            formulas = request.POST.get("formulas", None)
            if formulas is not None:
                formulas = json.loads(formulas)
                set_article_formulas(article, formulas)

        return JsonResponse({})


class PostPublicationUpdateView(CsrfExemptMixin, View):
    """
    View called to apply the modification on the article on test site.
    The new html is stored in files attributes of the requests methode and transfered
    to ptf_tools."""

    def post(self, request, **kwargs):
        doi = kwargs.get("doi", None)
        colid = kwargs.get("colid", None)
        article = model_helpers.get_article_by_doi(doi)
        if article.body_html:
            body_html = article.body_html.replace("/submit/media", "/media")
            files = {"html": io.StringIO(body_html)}
            prefix = settings.PTF_TOOLS_URL
            url = f"{prefix}/upload/PCJ/post-publication-modification/{colid}/{doi}/"
            requests.post(url, files=files, verify=False, data={"pid": article.pid})

        return redirect("post-publication-modification", colid, doi)


class PostPublicationModificationView(LoginRequiredMixin, TemplateView):
    """
    View that enable editorial staff to modify html version of an article already published.
    The modification is only effective on editor. The push to pcj is done by PostPublicationUpdateView
    """

    template_name = "post_published_modification.html"

    def get_context_data(self, **kwargs) -> dict:
        context = super().get_context_data(**kwargs)
        context["colid"] = settings.COLLECTION_PID
        doi = kwargs.get("doi", None)
        article = model_helpers.get_article_by_doi(doi)

        if article:
            submission = Submission.objects.get(article=article)
            if submission:
                form = ConversionDocxHtmlForm(initial={"content": submission.article.body_html})
                context["form"] = form

        context["article"] = article
        context["body_html"] = article.body_html
        collection = model_helpers.get_collection(settings.COLLECTION_PID, sites=False)
        context["collection"] = collection
        return context

    def post(self, request, **kwargs):
        """
        We change the body_html of the article, the html file, and transfer te body_html in requests files
        """
        doi = kwargs.get("doi", None)
        colid = kwargs.get("colid", None)
        article = model_helpers.get_article_by_doi(doi)
        submission = Submission.objects.get(article=article)
        form = ConversionDocxHtmlForm(request.POST)
        if form.is_valid():
            # we first update article.body_html
            new_body_html = form.cleaned_data["content"]
            article.body_html = new_body_html
            article.save()
            # Then, we update the html file
            source = submission.source
            folder = os.path.split(os.path.join(source.path))[0]
            html_file_path = (
                folder
                + "/"
                + os.path.splitext(os.path.split(os.path.join(source.path))[1])[0]
                + ".html"
            )
            with open(html_file_path, "w", encoding="utf-8") as html_file:
                html_file.write(new_body_html)

        return redirect("post-publication-modification", colid, doi)


class SendSubmissionRequest(LoginRequiredMixin, UpdateView):
    model = Submission
    template_name = "send_submission.html"
    fields = ("preprint_id", "recommendation_doi", "pdf", "image", "source", "biblio")
    success_url = "/"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["colid"] = settings.COLLECTION_PID
        context["article"] = self.object.article
        context["doi"] = self.object.article.doi
        context["submission_id"] = self.object.pk

        context["current_page"] = "send-submission"
        context["show_breadcrumb"] = True
        context["message"] = "Page en construction"
        context["pdf_file_name"] = os.path.split(self.object.pdf.name)[1]
        context["article_image"] = os.path.split(self.object.image.name)[1]
        context["source_file_name"] = os.path.split(self.object.source.name)[1]
        context["biblio_file_name"] = os.path.split(self.object.biblio.name)[1]
        context["artitcle_title"] = self.object.article.title_html
        context["recommendation_doi"] = self.object.recommendation_doi
        context["is_submission_valid"] = self.object.is_submission_form_valid()
        context["is_article_valid"] = self.object.article.title_html != ""

        return context

    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)

        submission = Submission.objects.get(pk=kwargs["pk"])

        path_pdf = self.object.pdf.name
        path_image = self.object.image.name
        with open(
            os.path.join(settings.LOG_DIR, "submissions_posted.log"), "a", encoding="utf-8"
        ) as file_:
            file_.write(f"{submission.article.doi} - {path_pdf} {path_image}\n")

        submission.state = STATE_SUBMITTED_TO_PCJ
        submission.save()

        if request.user.is_staff:
            response = HttpResponseRedirect(reverse("home"))
        else:
            # mail to staff and users (get_corresponding_emails)
            self.send_mail_new_submission(submission)
            response = HttpResponseRedirect(
                reverse("submission-posted", kwargs={"pk": kwargs["pk"]})
            )

        return response

    def send_mail_new_submission(self, submission):
        subject = "Confirmation of a new submission"
        template = "mail/submission_ack.html"
        to = submission.get_corresponding_emails()
        editorial_email = settings.CONTACT_PCJ
        cc = [editorial_email]

        html_content = render_to_string(
            template, {"submission": submission, "editorial_email": editorial_email}
        )  # render with dynamic value
        text_content = strip_tags(
            html_content
        )  # Strip the html tag. So people can see the pure text at least.
        # create the email, and attach the HTML version as well.
        msg = EmailMultiAlternatives(
            subject,
            text_content,
            editorial_email,
            to,
            cc=cc,
            headers={"Return-path": settings.RETURN_PATH},
        )
        msg.attach_alternative(html_content, "text/html")
        return msg.send(fail_silently=False)


class StaffView(LoginRequiredMixin, StaffuserRequiredMixin, TemplateView):
    """
    Staff dashboard
    """

    template_name = "editor/staff.html"
    raise_exception = True
    redirect_unauthenticated_users = True

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["submitted_submissions"] = Submission.objects.filter(state=STATE_SUBMITTED_TO_PCJ)
        context["edit_submissions"] = Submission.objects.filter(
            state__in=[STATE_EDIT, STATE_EDIT_INVALID]
        )
        context["accepted_submissions"] = Submission.objects.filter(state=STATE_ACCEPTED)
        context["published_submissions"] = Submission.objects.filter(
            state=STATE_PUBLISHED
        ).order_by("-article__date_published")
        context["colid"] = settings.COLLECTION_PID

        return context


class SubmissionAcceptView(LoginRequiredMixin, StaffuserRequiredMixin, UpdateView):
    """
    Handle the post request of a staff member who accepts an article
    send an email to Mersenne
    """

    template_name = "editor/staff.html"
    raise_exception = True
    redirect_unauthenticated_users = True

    def post(self, request, *args, **kwargs):
        submission = Submission.objects.get(pk=kwargs["pk"])
        article = submission.article
        article.article_number = "XX"
        article.save()
        colid = kwargs.get("colid", None)

        xml = ptf_cmds.exportPtfCmd(
            {
                "pid": article.pid,
                "with_body": False,
                "with_djvu": False,
                "article_standalone": True,
                "collection_pid": settings.COLLECTION_PID,
                "export_folder": "/var/log/mersenne",
            }
        ).do()

        # getting issue_pid
        issue_pid = get_issue_pid(colid)

        files = {"xml": io.StringIO(xml)}

        # html modification for correct img url in PCJ
        base_article_media_path = f"{settings.RESOURCES_ROOT}/{colid}/{issue_pid}/{article.pid}"

        source_article_media_path = f"{base_article_media_path}/src/media"

        upload_article_media_path = f"{base_article_media_path}/uploads"
        if article.body_html:
            body_html = article.body_html.replace("/submit/media/", "/media/")

            files["html"] = io.StringIO(body_html)

            if os.path.isdir(source_article_media_path):
                for image in os.listdir(source_article_media_path):
                    img = open(f"{source_article_media_path}/{image}", "rb")
                    name, extension = os.path.splitext(image)
                    if extension in [".tif", ".tiff"]:
                        img.close()
                        continue
                    files[f"{image}"] = img

            if os.path.isdir(upload_article_media_path):
                years = os.listdir(upload_article_media_path)
                for year in years:
                    months = os.listdir(os.path.join(upload_article_media_path, year))
                    for month in months:
                        days = os.listdir(os.path.join(upload_article_media_path, year, month))
                        for day in days:
                            for image in os.listdir(
                                os.path.join(upload_article_media_path, year, month, day)
                            ):
                                path = os.path.join("uploads", year, month, day, image)
                                img = open(
                                    f"{os.path.join(upload_article_media_path, year, month, day)}/{image}",
                                    "rb",
                                )
                                name, extension = os.path.splitext(image)
                                if extension in [".tif", ".tiff"]:
                                    img.close()
                                    continue
                                files[f"{path}"] = img

        try:
            with open(submission.pdf.path, "rb") as files["pdf"]:
                with open(submission.image.path, "rb") as files["image"]:
                    prefix = settings.PTF_TOOLS_URL
                    url = f"{prefix}/upload/articles/{settings.COLLECTION_PID}/"
                    requests.post(url, files=files, verify=False, data={"pid": article.pid})

            if os.path.isdir(source_article_media_path):
                for image in os.listdir(source_article_media_path):
                    if ".tif" in image or ".tiff" in image:
                        continue
                    img = files[image]
                    img.close()

        except OSError:
            messages.error(request, "A problem occurred when uploading files.")
            return HttpResponseRedirect(reverse("staff-home"))

        submission.state = STATE_ACCEPTED
        submission.save()
        # send mail to Mersenne (cc users avec rôle Staff)
        self.send_mail_submission_accepted(submission)

        return HttpResponseRedirect(reverse("staff-home"))

    def send_mail_submission_accepted(self, submission):
        subject = "[PCJ] A submission is accepted by editorial team"
        template = "mail/submission_accepted.html"
        to = [settings.CONTACT_MERSENNE]
        editorial_email = settings.CONTACT_PCJ
        cc = [editorial_email]

        html_content = render_to_string(
            template, {"submission": submission, "journal_site_test": settings.JOURNAL_TEST_URL}
        )  # render with dynamic value
        text_content = strip_tags(
            html_content
        )  # Strip the html tag. So people can see the pure text at least.
        # create the email, and attach the HTML version as well.
        msg = EmailMultiAlternatives(
            subject,
            text_content,
            editorial_email,
            to,
            cc=cc,
            headers={"Return-path": settings.RETURN_PATH},
        )
        msg.attach_alternative(html_content, "text/html")
        return msg.send(fail_silently=True)


class SubmissionCreateView(LoginRequiredMixin, CreateView):
    """
    Create a submission.
    The user enters 2 DOIs (original article and recommendation)
    Crossref is called in form_valid to get the article metadata
    """

    model = Submission
    # template_name = "editor/submission_create.html"
    form_class = SubmissionForm
    is_valid = True

    def get_or_create_issue(self, collection):
        now = timezone.now()
        curyear = now.year
        # issue_pid = f"{settings.COLLECTION_PID}_{curyear}_1_1_1"
        issue_pid = f"{settings.COLLECTION_PID}_{curyear}__{curyear - 2020}_"
        issue = model_helpers.get_container(issue_pid)

        if issue is None:
            xissue = create_issuedata()
            xissue.pid = issue_pid
            xissue.ctype = "issue"
            xissue.year = str(curyear)
            xissue.last_modified_iso_8601_date_str = timezone.now().isoformat()

            xpublisher = create_publisherdata()
            xpublisher.name = "Peer Community In"
            xissue.publisher = xpublisher

            cmd = ptf_cmds.addContainerPtfCmd({"pid": issue_pid, "xobj": xissue})

            cmd.add_collection(collection)
            cmd.set_provider(model_helpers.get_provider("mathdoc-id"))
            issue = cmd.do()

        return issue

    def set_pci(self, article_data, recommendation_doi, preprint_id):
        if recommendation_doi.find("10.24072/pci.") == 0:
            pci = recommendation_doi[13:].split(".")[0]
            if pci == "cneuro":
                pci = "neuro"

            article_data.subjs = [{"type": "pci", "lang": "en", "value": pci}]
            article_data.extids.append(("rdoi", recommendation_doi))
            article_data.extids.append(("preprint", preprint_id))
            article_data.article_number = "XX"

    def form_valid(self, form):
        preprint_id = form.cleaned_data["preprint_id"]
        recommendation_doi = form.cleaned_data["recommendation_doi"]

        collection = model_helpers.get_collection(settings.COLLECTION_PID, sites=False)
        doi_int = collection.last_doi + 1

        collection.last_doi = int(doi_int)
        collection.save()

        doi = f"10.24072/pcjournal.{doi_int}"

        image = None

        if preprint_id.find("10.24072/pcjournal.") == 0:
            # Corrigendum of an existing article
            article = model_helpers.get_article_by_doi(preprint_id)

            # Clone of the existing article
            article_data = model_data_converter.db_to_article_data(article)

            # Set new DOI/PID
            article_data.doi = doi
            article_data.pid = doi.replace("/", "_").replace(".", "_").replace("-", "_")

            # Reset some data
            article_data.prod_deployed_date_iso_8601_date_str = None
            article_data.date_published_iso_8601_date_str = None
            article_data.ids = []
            article_data.article_number = "XX"
            article_data.atype = "research-article"

            # Add a relationship "corrects"
            obj = Foo()
            obj.rel_type = "corrects"
            obj.id_value = preprint_id
            article_data.relations.append(obj)

            # Use the same image
            submission = article.submission_set.first()
            image = submission.image
        else:
            try:
                article_data = crossref.fetch_article(
                    preprint_id, with_bib=False, create_author_if_empty=True
                )
            except requests.exceptions.HTTPError as e:
                if e.response.status_code == 504:
                    form.add_error(
                        "Crossref is currently unavailable",
                        "Crossref is currently unavailable and prevents us from fetching your article metadata. Please try again later",
                    )
                    self.is_valid = False
                    result = self.form_invalid(form)
                else:
                    raise

            if self.is_valid:
                article_data.doi = doi
                article_data.atype = "research-article"
                article_data.lang = "en"
                article_data.pid = doi.replace("/", "_").replace(".", "_").replace("-", "_")
                # Request from PCJ to not fill title/abstract: preprints have different versions
                # The title/abstract sent by crossref is not always the good one
                article_data.title_html = article_data.title_tex = ""
                article_data.title_xml = (
                    "<title-group><article-title></article-title></title-group>"
                )
                article_data.abstracts = []
                self.set_pci(article_data, recommendation_doi, preprint_id)
                set_mandatory_topics(article_data)

        if self.is_valid:
            issue = self.get_or_create_issue(collection)

            cmd = xml_cmds.addArticleXmlCmd(
                {"xarticle": article_data, "use_body": False, "issue": issue, "standalone": True}
            )
            cmd.set_collection(collection)
            article = cmd.do()

            obj, created = Submission.objects.get_or_create(
                preprint_id=preprint_id, recommendation_doi=recommendation_doi, article=article
            )
            obj.users.add(self.request.user)
            if image:
                obj.image = image
            obj.save()
            obj.check_validity(article_data)

            result = HttpResponseRedirect(reverse("submission-update", kwargs={"pk": obj.pk}))
        else:
            doi_int = collection.last_doi - 1
            collection.last_doi = int(doi_int)
            collection.save()

        return result


class SubmissionDeleteView(LoginRequiredMixin, View):
    raise_exception = True
    redirect_unauthenticated_users = True

    def post(self, request, *args, **kwargs):
        submission = Submission.objects.get(pk=kwargs["pk"])
        article = submission.article
        article.delete()
        submission.delete()

        return HttpResponseRedirect(reverse("home"))


class SubmissionDownloadSourceView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        submission = Submission.objects.get(pk=self.kwargs["pk"])
        type_ = self.kwargs["type"]

        if type_ == "src":
            obj = submission.source
        else:
            obj = submission.biblio

        if not bool(obj):
            if type_ == "src":
                messages.error(request, "The submission has no source file")
            else:
                messages.error(request, "The submission has no file with references")
            return HttpResponseRedirect(reverse("staff-home"))

        if type_ != "src":
            content_type = "text/plain"
        else:
            ext = os.path.splitext(obj.name)[1]
            if ext == ".tex":
                content_type = "application/x-tex"
            elif ext == ".odt":
                content_type = "application/vnd.oasis.opendocument.text"
            elif ext == ".doc":
                content_type = "application/msword"
            else:
                content_type = (
                    "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                )

        response = HttpResponse(obj.file, content_type=content_type)
        response["Content-Type"] = "application/x-binary"
        response["Content-Disposition"] = f'attachment; filename="{obj.name}"'
        return response


class SubmissionFinalizeView(LoginRequiredMixin, TemplateView):
    model = Submission
    template_name = "editor/submission_finalize.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["colid"] = settings.COLLECTION_PID
        # context["article"] = self.object.article
        # context["doi"] = self.object.article.doi
        context["submission_id"] = kwargs.get("pk", None)
        submission = Submission.objects.get(pk=context["submission_id"])
        article = submission.article
        context["doi"] = article.doi

        context["current_page"] = "finalize"
        context["show_breadcrumb"] = True
        context["message"] = "Page en construction"
        context["pdf_file_name"] = os.path.split(submission.pdf.name)[1]
        context["article_image"] = os.path.split(submission.image.name)[1]
        context["source_file_name"] = os.path.split(submission.source.name)[1]
        context["biblio_file_name"] = os.path.split(submission.biblio.name)[1]
        context["artitcle_title"] = submission.article.title_html
        context["recommendation_doi"] = submission.recommendation_doi
        context["state"] = submission.state
        context["is_submission_valid"] = submission.is_submission_form_valid()
        context["is_article_valid"] = submission.article.title_html != ""
        return context


class SubmissionListView(LoginRequiredMixin, ListView):
    """
    Unused as of 09/01/2021.
    Could be used of we allow multiple submissions for 1 author
    """

    model = Submission
    template = "editor/submission_list.html"
    context_object_name = "submissions"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user

        submissions = user.submission_set.all()
        context["submissions"] = submissions
        context["colid"] = settings.COLLECTION_PID

        return context


class SubmissionPostedView(LoginRequiredMixin, TemplateView):
    """
    Page displayed to the user when the submission has been posted
    """

    template_name = "editor/submission_posted.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["colid"] = settings.COLLECTION_PID

        pk = kwargs.get("pk", None)
        submission = Submission.objects.get(pk=pk)
        context["submission"] = submission
        context["submission_id"] = submission.pk

        # tex_formulas = get_article_formulas(submission.article)
        # # tex_formulas.append('$$x = {-b \pm \sqrt{b^2-4ac} \over 2a}.$$')
        # # tex_formulas.append('$$y = {-b \pm \sqrt{b^2-4ac} \over 2b}.$$')
        # context['tex_formulas'] = tex_formulas
        context["doi"] = submission.article.doi

        context["current_page"] = "posted"
        context["disable_edit"] = True
        context["submission_ended"] = True
        context["show_breadcrumb"] = True
        context["state"] = submission.state

        return context


class SubmissionPublishAPIView(CsrfExemptMixin, View):
    def post(self, request, *args, **kwargs):
        doi = kwargs.get("doi", None)
        article = model_helpers.get_article_by_doi(doi)

        if article is None:
            return JsonResponse({"status_code": 404, "message": "article not found"})

        submission = Submission.objects.get(article=article)

        body_unicode = request.body.decode("utf-8")
        data = json.loads(body_unicode)

        if "date_published" in data:
            date = model_helpers.parse_date_str(data["date_published"])
            article.date_published = date
            article.save()
        if "article_number" in data:
            article.article_number = data["article_number"]
            article.save()

        submission.state = STATE_PUBLISHED
        submission.save()

        result = self.send_mail_article_published(submission)

        with open(
            os.path.join(settings.LOG_DIR, "publish_mail.log"), "a", encoding="utf-8"
        ) as file_:
            file_.write(f"{article.doi} - {str(result)}\n")

        return JsonResponse({"message": "OK"})

    def send_mail_article_published(self, submission):
        subject = "Your article has been published in Peer Community Journal"
        template = "mail/submission_published.html"
        to = submission.get_corresponding_emails()
        editorial_email = settings.CONTACT_PCJ
        cc = [editorial_email]

        html_content = render_to_string(
            template, {"submission": submission, "journal_site": settings.JOURNAL_URL}
        )  # render with dynamic value
        text_content = strip_tags(
            html_content
        )  # Strip the html tag. So people can see the pure text at least.
        # create the email, and attach the HTML version as well.
        msg = EmailMultiAlternatives(
            subject,
            text_content,
            editorial_email,
            to,
            cc=cc,
            headers={"Return-path": settings.RETURN_PATH},
        )
        msg.attach_alternative(html_content, "text/html")
        return msg.send(fail_silently=True)


class SubmissionRevertView(LoginRequiredMixin, StaffuserRequiredMixin, UpdateView):
    """
    Handle the post request of a staff member who reverts an article (Accept -> Submitted)
    """

    template_name = "editor/staff.html"
    raise_exception = True
    redirect_unauthenticated_users = True

    def post(self, request, *args, **kwargs):
        submission = Submission.objects.get(pk=kwargs["pk"])
        submission.state = STATE_SUBMITTED_TO_PCJ
        submission.save()

        return HttpResponseRedirect(reverse("staff-home"))


@method_decorator(csrf_exempt, name="dispatch")
class SubmissionUpdateView(LoginRequiredMixin, UpdateView):
    """
    Last page of the submission "wizard"
    Enable/Disable the submit button based on the submission state
    The user can update the submission (PDF/Image)
    send confirmation email on submit to author/cc: contact_pcj
    TODO: The form has 2 submit buttons (Save and Submit)
          It would be better to move the PDF/Image fields in the Edit Data view
          and have 1 submit button the update form.
          It would avoid the form_valid function to add errors

    """

    model = Submission
    template_name = "editor/submission_update.html"
    fields = ("pdf", "image", "source", "biblio")
    success_url = "/"

    def form_invalid(self, form):
        submission = Submission.objects.get(pk=self.kwargs["pk"])

        # If the form is invalid, self.object has still the invalid values
        # which is wrong for the FileField/ImageField
        self.object = submission
        response = super().form_invalid(form)

        return response

    def post(self, request, *args, **kwargs):
        super().post(request, *args, **kwargs)
        return redirect("submission-update", self.object.pk)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["colid"] = settings.COLLECTION_PID
        context["article"] = self.object.article
        context["doi"] = self.object.article.doi
        context["submission_id"] = self.object.pk
        context["current_page"] = "files"
        context["show_breadcrumb"] = True

        context["show_send_submission"] = check_form_for_breadcrumb(self.object)
        context["is_submission_valid"] = self.object.is_submission_form_valid()
        context["is_article_valid"] = self.object.article.title_html != ""
        context["state"] = self.object.state

        return context

    def form_valid(self, form):
        new_submission = self.object
        submission = Submission.objects.get(pk=new_submission.pk)

        # preprint_id = form.cleaned_data["preprint_id"]
        # recommendation_doi = form.cleaned_data["recommendation_doi"]
        pdf = form.cleaned_data["pdf"]
        image = form.cleaned_data["image"]
        source = form.cleaned_data["source"]
        biblio = form.cleaned_data["biblio"]

        if "save_and_submit" in self.request.POST and (
            pdf is None or image is None or source is None or biblio is None
        ):
            if pdf is None:
                form.add_error("pdf", "a PDF file is required")
            if image is None:
                form.add_error("image", "an image is required")
            if source is None:
                form.add_error("source", "a source file is required")
            if biblio is None:
                form.add_error("biblio", "a file with references is required")
            result = self.form_invalid(form)
        else:
            result = super().form_valid(form)
            path_pdf = os.path.join(settings.MEDIA_ROOT, self.object.pdf.name)
            path_image = os.path.join(settings.MEDIA_ROOT, self.object.image.name)
            path_source = os.path.join(settings.MEDIA_ROOT, self.object.source.name)
            path_biblio = os.path.join(settings.MEDIA_ROOT, self.object.biblio.name)
            if (
                not os.path.exists(path_pdf)
                or not os.path.exists(path_image)
                or not os.path.exists(path_source)
                or not os.path.exists(path_biblio)
            ):
                if not os.path.exists(path_pdf):
                    form.add_error(
                        "pdf", "Your PDF file was not correctly uploaded. Please try again."
                    )
                if not os.path.exists(path_image):
                    form.add_error(
                        "image", "Your image was not correctly uploaded. Please try again."
                    )
                if not os.path.exists(path_source):
                    form.add_error(
                        "source", "Your source file was not correctly uploaded. Please try again."
                    )
                if not os.path.exists(path_biblio):
                    form.add_error(
                        "biblio",
                        "Your file with references was not correctly uploaded. Please try again.",
                    )
                result = self.form_invalid(form)

        path_pdf = self.object.pdf.name if self.object.pdf else ""
        path_image = self.object.image.name if self.object.image else ""
        path_source = self.object.source.name if self.object.source else ""
        path_biblio = self.object.biblio.name if self.object.biblio else ""
        with open(
            os.path.join(settings.LOG_DIR, "submissions_posted.log"), "a", encoding="utf-8"
        ) as file_:
            file_.write(
                f"{submission.article.doi} - {path_pdf} {path_image} {path_source} {path_biblio}\n"
            )

        return result

    def send_mail_new_submission(self, submission):
        subject = "Confirmation of a new submission"
        template = "mail/submission_ack.html"
        to = submission.get_corresponding_emails()
        editorial_email = settings.CONTACT_PCJ
        cc = [editorial_email]

        html_content = render_to_string(
            template, {"submission": submission, "editorial_email": editorial_email}
        )  # render with dynamic value
        text_content = strip_tags(
            html_content
        )  # Strip the html tag. So people can see the pure text at least.
        # create the email, and attach the HTML version as well.
        msg = EmailMultiAlternatives(
            subject,
            text_content,
            editorial_email,
            to,
            cc=cc,
            headers={"Return-path": settings.RETURN_PATH},
        )
        msg.attach_alternative(html_content, "text/html")
        return msg.send(fail_silently=False)
