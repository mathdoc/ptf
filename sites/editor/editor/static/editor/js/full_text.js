function highlightReference(elementId, duration, interval) {
    const element = document.getElementById(elementId);
    element.classList.add("highlight")
        setTimeout(() => {
            element.classList.remove("highlight");
        }, duration)
}

document.addEventListener('DOMContentLoaded', () => {
    const tooltips = document.querySelectorAll('.tooltipPCJ')
    const articleDisplay = document.querySelector('#right-pannel')
    if (articleDisplay==null) {
        return
    }
    articleDisplayRect = articleDisplay.getBoundingClientRect()

    tooltips.forEach(tooltip => {
        tooltip.removeAttribute('style')
        tooltip.addEventListener('mouseover', function(event)  {
            console.log("event")
            const tooltipText = tooltip.querySelector('.tooltip')
            tooltipText.removeAttribute('style')

            let tooltipRect = tooltipText.getBoundingClientRect()
            if (tooltipRect.right + tooltipText.offsetWidth > articleDisplayRect.right) {
                tooltipText.style.left = 'unset'
                tooltipText.style.right = '10%'
            }

            /* 300 is the distance between top of the window and the bottom of breadcrumb */
            if (tooltipRect.top < 500) {
                tooltipText.style.bottom = 'unset'
                tooltipText.style.top = '100%'
            }

            tooltipText.classList.remove('tooltiptexthidden')
            tooltipText.classList.add('tooltiptextshow')

        })

        tooltip.addEventListener('mouseout', () => {
            const tooltipText = tooltip.querySelector('.tooltip')
            tooltipText.classList.remove("tooltiptextshow")
            tooltipText.classList.add("tooltiptexthidden")
        })

    })
})
try {
    document.getElementById('submitBodyHtmlButton').addEventListener("click", function(event) {
        const form = document.getElementById('bodyHtmlForm')
        form.submit()
})
}
catch (err) {
    console.log("Error", err)
}


// let ckeditorContent = document.getElementsByClassName('ckeditor-content')[0]
let header = document.getElementsByClassName("header")[0]
let headerHeight = header.getBoundingClientRect().height
let buttonCkeditor = document.getElementById("buttons-plus-ckeditor")

document.addEventListener("DOMContentLoaded", function(){
    try {
        buttonCkeditor.style.top = headerHeight + "px"
    } catch(err) {
        console.log("error", err)
    }
})