
let saveButton = document.getElementById('files-save-button')
let pdf = document.getElementById('id_pdf')
let image = document.getElementById('id_image')
let source = document.getElementById('id_source')
let biblio = document.getElementById('id_biblio')
let nextButton1 = document.getElementById("filesNext1")
let nextButton2 = document.getElementById("filesNext2")


function checkNextButton(pdf, image, source, biblio, nextButton1, nextButton2) {
    inputs = new Array(pdf.value, image.value, source.value, biblio.value)
    let activateNext = true
    inputs.forEach((field) => {
        if (field.length == 0) {
            activateNext = false      
        }
    })
    if (activateNext) {
        nextButton1.classList = ["btn btn-lg btn-success next-button"]
        nextButton2.classList = ["btn btn-lg btn-success next-button"]
    }
}


pdf.addEventListener("input", () => {
    checkNextButton(pdf, image, source, biblio, nextButton1, nextButton2)
})
image.addEventListener("input", () => {
    checkNextButton(pdf, image, source, biblio, nextButton1, nextButton2)
})
source.addEventListener("input", () => {
    checkNextButton(pdf, image, source, biblio, nextButton1, nextButton2)
})
biblio.addEventListener("input", () => {
    checkNextButton(pdf, image, source, biblio, nextButton1, nextButton2)
})




document.addEventListener('DOMContentLoaded', (event) => {
    const form = document.getElementById("submission-form")
    let submitAction = ""
    form.addEventListener('submit', function(event) {
        event.preventDefault()

        const formData = new FormData(form)

        fetch(form.action, {
            method: form.method,
            body: formData
        })
        .then(response => {
            if (submitAction == 'next') {
                window.location.href = nextButton1.getAttribute('data-href')
            } else {
                window.location.href = form.action
            }
        })
        .catch(err => {
            console.error("Erreur", err)
        })

    })

    form.querySelectorAll('button[type="submit"]').forEach(button => {
        button.addEventListener('click', function(event) {
            submitAction = event.target.getAttribute('data-action');
        });
    });
})
