window.MathJax.Hub.Config({
    tex2jax: {
        inlineMath: [
            ["$", "$"],
            ["\\\\(", "\\\\)"],
        ],
    },
});

function toMathML(jax, callback) {
    var mml;
    try {
        mml = jax.root.toMathML("");
    } catch (err) {
        console.log("error");
        if (!err.restart) {
            throw err;
        } // an actual error
        return MathJax.Callback.After([toMathML, jax, callback], err.restart);
    }
    console.log(mml);
    MathJax.Callback(callback)(mml);
}

window.MathJax.Hub.Queue(function () {
    var jax = MathJax.Hub.getAllJax();

    for (var i = 0; i < jax.length; i++) {
        toMathML(jax[i], function (mml) {
            alert(jax[i].originalText + "\n\n=>\n\n" + mml);
        });
    }
});
