from django.contrib import admin

from mersenne_cms.admin import PageAdmin
from mersenne_cms.models import Page
from ptf.models import Article
from ptf.models import Collection
from ptf.models import Container

from .models import Submission

admin.site.register(Page, PageAdmin)
admin.site.register(Article)
admin.site.register(Container)
admin.site.register(Collection)
admin.site.register(Submission)
