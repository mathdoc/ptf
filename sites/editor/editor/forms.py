import re

import requests
from ckeditor_uploader.widgets import CKEditorUploadingWidget

from django import forms

from matching import crossref
from ptf import model_helpers
from ptf.cmds.xml.xml_utils import clean_doi
from ptf.display.resolver import find_id_type

from .models import Submission


class SubmissionForm(forms.ModelForm):
    class Meta:
        model = Submission
        fields = (
            "preprint_id",
            "recommendation_doi",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field in self.fields.values():
            field.error_messages = {"required": f"The field {field.label} is required"}

    def clean_preprint_id(self):
        preprint_id = clean_doi(self.cleaned_data["preprint_id"])
        id_type = find_id_type(preprint_id)

        if id_type is None:
            raise forms.ValidationError(
                "The preprint id has to be a DOI, an HAL or an arXiv identifier"
            )

        return preprint_id

    def clean_recommendation_doi(self):
        if "preprint_id" not in self.cleaned_data:
            # clean_preprint_id is raising an error, do not proceed
            # return None or "" will raise an additional error
            return "None"

        recommendation_doi = clean_doi(self.cleaned_data["recommendation_doi"])
        prefix_pos = recommendation_doi.find("10.24072/pci.")
        if prefix_pos != 0:
            raise forms.ValidationError("The recommendation doi must start with '10.24072/pci.'")

        preprint_id = clean_doi(self.cleaned_data["preprint_id"])

        if preprint_id.find("10.24072/pcjournal.") == 0:
            # Corrigendum
            article = model_helpers.get_article_by_doi(preprint_id)
            recommendations = article.extid_set.filter(id_type="rdoi")
            if recommendations:
                rdoi = recommendations.first().id_value
            else:
                rdoi = ""

            if recommendation_doi.lower() != rdoi.lower():
                raise forms.ValidationError(
                    "The recommendation doi is not the one associated with the article"
                )
        else:
            try:
                article_data = crossref.crossref_request_article(
                    recommendation_doi, with_bib=False
                )
            except requests.exceptions.HTTPError as e:
                if e.response.status_code == 504:
                    raise forms.ValidationError(
                        "Crossref is currently unavailable and prevents us from fetching your article metadata. Please try again later."
                    )
                else:
                    raise
            if not hasattr(article_data, "title_tex") or article_data.title_tex == "":
                raise forms.ValidationError("The recommendation doi is not found by Crossref")

            if hasattr(article_data, "preprint_id"):
                crossref_preprint_id = crossref.fix_id(article_data.preprint_id)

                id_type = find_id_type(preprint_id)
                if id_type == "hal" or id_type == "arxiv":
                    # The id specified in Crossref may have a version (ex: "1909.11490v4"). Remove it
                    if re.match(r".*v\d+$", crossref_preprint_id) is not None:
                        pos = crossref_preprint_id.rfind("v")
                        crossref_preprint_id = crossref_preprint_id[:pos]

                # The id specified in crossref may not have the "hal-" or "arXiv-" prefix. Add it.
                if id_type == "hal" and crossref_preprint_id.lower().find("hal-") != 0:
                    crossref_preprint_id = "hal-" + crossref_preprint_id
                if id_type == "arxiv" and crossref_preprint_id.lower().find("arxiv:") != 0:
                    crossref_preprint_id = "arXiv:" + crossref_preprint_id

                if crossref_preprint_id.lower() != preprint_id.lower():
                    raise forms.ValidationError(
                        "The preprint id is not the one associated with the recommendation"
                    )

        return recommendation_doi


class ConversionDocxHtmlForm(forms.Form):
    content = forms.CharField(widget=CKEditorUploadingWidget(config_name="editor_pages"))

    def __init__(self, *args, **kwargs):
        colid = kwargs.pop("colid", None)
        pid = kwargs.pop("pid", None)
        issue_pid = kwargs.pop("issue_pid", None)

        super().__init__(*args, **kwargs)
        if pid:
            # if working in local you must delete /submit/ from your urls
            self.fields["content"].widget.config["filebrowserUploadUrl"] = (
                "/submit/ckeditor/upload/"
                + f"{colid}"
                + f"/{issue_pid}/"
                + pid
                + "/"
                # "/ckeditor/upload/" + f"{colid}" + f"/{issue_pid}/" + pid + "/"
            )
            self.fields["content"].widget.config["filebrowserBrowseUrl"] = (
                "/submit/ckeditor/browse/"
                + f"{colid}"
                + f"/{issue_pid}/"
                + pid
                + "/"
                # "/ckeditor/browse/" + f"{colid}" + f"/{issue_pid}/" + pid + "/"
            )
            pass
