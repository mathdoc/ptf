import panflute as pf


def no_action(elem, doc):
    pass


pcj_equation_counter = 0


def get_pcj_style(elem, doc):
    if isinstance(elem, pf.Div):
        if ("custom-style", "PCJ Section") in elem.attributes.items():
            text = ""
            for item in elem.content:
                for txt in item.content:
                    if isinstance(txt, pf.Str):
                        text += txt.text
                    elif isinstance(txt, pf.Space):
                        text += " "
            return pf.Header(pf.Str(text), level=1, classes=["PCJ-Section"])
        elif ("custom-style", "PCJ Subsection") in elem.attributes.items():
            text = ""
            for item in elem.content:
                for txt in item.content:
                    if isinstance(txt, pf.Str):
                        text += txt.text
                    elif isinstance(txt, pf.Space):
                        text += " "
            return pf.Header(pf.Str(text), level=2, classes=["PCJ-Subsection"])
        elif ("custom-style", "PCJ text") in elem.attributes.items():
            text = ""
            inline_math = False
            eq = ""
            # to_inject = pf.Str("")
            to_inject = pf.Para()
            for item in elem.content:
                for txt in item.content:
                    if isinstance(txt, pf.Str):
                        text += txt.text
                    elif isinstance(txt, pf.Space):
                        text += " "
                    elif isinstance(txt, pf.Math):
                        inline_math = True
                        eq = txt.text
                        to_inject.content.append(pf.Str(text))
                        to_inject.content.append(pf.Math(eq, format="InlineMath"))
                        text = ""
                        eq = ""

            if inline_math:
                to_inject.content.append(pf.Str(text))
                elem_returned = pf.Div()
                elem_returned.classes = ["PCJ-text"]
                elem_returned.content.append(to_inject)
                return elem_returned
            return pf.Div(pf.Para(pf.Str(text)), classes=["PCJ-text"])
        elif ("custom-style", "PCJ Sub-subsection") in elem.attributes.items():
            text = ""
            for item in elem.content:
                for txt in item.content:
                    if isinstance(txt, pf.Str):
                        text += txt.text
                    elif isinstance(txt, pf.Space):
                        text += " "
            return pf.Header(pf.Str(text), level=2, classes=["PCJ-Sub-subsection"])
        elif ("custom-style", "PCJ caption figure") in elem.attributes.items():
            text = ""
            for item in elem.content:
                for txt in item.content:
                    if isinstance(txt, pf.Str):
                        text += txt.text
                    elif isinstance(txt, pf.Space):
                        text += " "
            elem.classes = ["PCJ-caption-figure"]
            return elem
        elif ("custom-style", "PCJ Figure") in elem.attributes.items():
            elem.classes = ["PCJ-Figure"]
            return pf.Figure(elem)
        elif ("custom-style", "PCJ note table") in elem.attributes.items():
            text = ""
            for item in elem.content:
                for txt in item.content:
                    if isinstance(txt, pf.Str):
                        text += txt.text
                    elif isinstance(txt, pf.Space):
                        text += " "
            return pf.Div(pf.Para(pf.Str(text)), classes=["PCJ-note-table"])
        elif ("custom-style", "PCJ table legend") in elem.attributes.items():
            elem.classes = ["PCJ-table-legend"]
            return elem
        elif ("custom-style", "PCJ note table") in elem.attributes.items():
            elem.classes = ["PCJ-note-table"]
            return elem
        elif ("custom-style", "PCJ Reference") in elem.attributes.items():
            elem.classes = ["PCJ-reference"]
            return elem
        elif ("custom-style", "PCJ Equation") in elem.attributes.items():
            text = pf.Para()
            for item in elem.content:
                for txt in item.content:
                    if isinstance(txt, pf.Str) or isinstance(txt, pf.Space):
                        text.content.append(txt)
                    elif isinstance(txt, pf.Math):
                        text.content.append(pf.Str("$" + txt.text + "$"))
            return pf.Div(text, classes=["PCJ-Equation"])

    elif isinstance(elem, pf.Table):
        elem.classes = ["PCJ-table"]

        return elem
    elif isinstance(elem, pf.OrderedList):
        return pf.Div(elem, classes=["PCJ-OrderedList"])

    return elem


def main(doc=None):
    return pf.run_filter(get_pcj_style, doc=doc)


if __name__ == "__main__":
    main()
