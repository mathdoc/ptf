from django.http import Http404

from ptf import model_helpers
from ptf.cmds.xml.ckeditor.ckeditor_parser import CkeditorParser
from ptf.utils import create_innerlink_for_citation


def get_unused_ref(checked_biblio):
    # checked_biblio = json.loads(checked_biblio)
    unused_ref = {}
    count = 0
    for key, value in checked_biblio.items():
        if not value["cited_in_article"]:
            count += 1
            unused_ref[count] = key
    return unused_ref


def change_html_from_ckeditor(
    submission, colid, check_citation=False, biblio=False, checked_required=False, **kwargs
):
    issue_pid = get_issue_pid(colid)
    xbody = CkeditorParser(
        html_value=submission.article.body_html,
        mml_formulas=[],
        pid=submission.article.pid,
        volume=submission.article.my_container.pid,
        issue_pid=issue_pid,
        biblio=biblio,
        check_citation=check_citation,
        for_pcj_display=True,
    )

    submission.article.body_html = xbody.value_html
    submission.article.body_html = create_innerlink_for_citation(
        submission.article.body_html, biblio
    )
    submission.article.body_tex = xbody.value_tex
    submission.article.body_xml = xbody.value_xml
    return submission


def check_biblio(html_text, biblio):
    for key, value in biblio.items():
        if "Bischof" in key:
            pass
        if key in html_text:
            value["cited_in_article"] = True
            continue

        key2 = key.replace("&", "&amp;")
        if key2 in html_text:
            value["cited_in_article"] = True
            continue

        key3 = key.replace("&", "and")
        if key3 in html_text:
            value["cited_in_article"] = True
            continue

        key4 = key3.replace(",", "")
        if key4 in html_text:
            value["cited_in_article"] = True
            continue

        key5 = key.replace(",", "")
        if key5 in html_text:
            value["cited_in_article"] = True
            continue

        key6 = key2.replace(",", "")
        if key6 in html_text:
            value["cited_in_article"] = True
            continue

    return biblio


def get_issue_pid(colid):
    """Get the collection and return dynamicaly the current issue_pid"""

    collection = model_helpers.get_collection(colid, sites=False)
    if collection is None:
        raise Http404(f"{colid} does not exist")

    qs = collection.content.order_by("-year", "-vseries", "-volume", "-volume_int", "-number_int")
    issue = qs.first()
    issue_pid = issue.pid
    return issue_pid


def create_article_biblio(submission: object) -> dict:
    """Get the bibitem_set from the article of the submission and create a dictionary where the keys depend on number of authors
    and values are the label of the reference
    1 authors => {{LAST_NAME}, {YEAR}
    2 authors => {LAST_NAME1} & {LAST_NAME2}, {YEAR}
    >= 3 authors => {LAST_NAME1} et al., {YEAR}
    """
    biblio = submission.article.bibitem_set.all()
    biblio_dict = {}
    for citation in biblio:
        try:
            reference = citation.citation_html.replace(f"{citation.label}", "")
            reference += f' | <a href=https://doi.org/{citation.get_doi()} target="_blank">DOI</a>'
            if len(citation.contributions.all()) == 1:
                # if citation.contributions.all()[0].first_name:
                #     author = (
                #         citation.contributions.all()[0].last_name
                #         + " "
                #         + citation.contributions.all()[0].first_name[0]
                #         + "."
                #     )
                # else:
                author = citation.contributions.all()[0].last_name
                biblio_dict[f"{author}, {citation.year}"] = {
                    "label": citation.label,
                    "reference": reference,
                    "cited_in_article": False,
                }
            elif len(citation.contributions.all()) == 2:
                first_author = citation.contributions.all()[0].last_name
                second_author = citation.contributions.all()[1].last_name
                biblio_dict[f"{first_author} & {second_author}, {citation.year}"] = {
                    "label": citation.label,
                    "reference": reference,
                    "cited_in_article": False,
                }
            else:
                first_author = citation.contributions.all()[0].last_name
                if first_author == "Bischof":
                    pass
                biblio_dict[f"{first_author} et al., {citation.year}"] = {
                    "label": citation.label,
                    "reference": reference,
                    "cited_in_article": False,
                }

            if "<a href" not in citation.citation_html:
                citation.citation_html = citation.citation_html.replace(
                    f"{citation.label}", f"<a href=#{citation.label[1:-1]}>{citation.label}</a>"
                )
                citation.save()

        except IndexError:
            # this error happens whe there are no authors in the doi
            # for ex only editor for a book
            pass

    return biblio_dict


def check_form_for_breadcrumb(submission):
    if (
        submission
        and submission.pdf
        and submission.pdf.name
        and submission.image
        and submission.image.name
        and submission.source
        and submission.source.name
        and submission.biblio
        and submission.biblio.name
    ):
        return True
    else:
        return False
