sites/editor: Back-office of the PCJ submission application
It's a Django application (login - CRUD for submission - Crossref APIs - POST to ptf-tools)

The Edit Article page is done with VueJS.
The VueJS project is currently outside this git.
It is available at ptf-tools.u-ga:/home/labbeo/editor
sites/editor/editor/static/vuejs css and js are links to the dist folder of the vuejs project
Note that the dist file names do not change thanks to
    // vue.config.js
    module.exports = {
        filenameHashing: false
    }

# TODO
On crossref request
    generate label

Editor
    hide label
    add/delete ref
    move ref up/down
    crossref request on ref doi ?

Template for register local account

Submit to ptf-tools

Resize image ? Check image ratio ?

Allow multiple users on 1 submission (at least for admin that takes over)



