ALLOWED_HOSTS = ["*"]

SECRET_KEY = "change me"

DEBUG = True

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": "mesh_base",
        "USER": "mesh_base_user",
        "PASSWORD": "",
        "HOST": "localhost",
        "PORT": "",
    }
}

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

RESOURCES_ROOT = "/mesh_test_data"

SENDFILE_BACKEND = "django_sendfile.backends.development"

MESH_FILES_DIRECTORY = "/mesh_files/MESH"

DEFAULT_FROM_EMAIL = "no_reply@listes.mathdoc.fr"
MESH_JOURNAL_EMAIL_CONTACT = "contact@journal.fr"

import sys

if "test" in sys.argv or "pytest" in sys.modules:
    DATABASES["default"] = {"ENGINE": "django.db.backends.sqlite3"}
    MESH_FILES_DIRECTORY = "/tmp/MESH"
