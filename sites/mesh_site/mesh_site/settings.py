import os
from pathlib import Path
import sys

try:
    from .settings_local import (
        DATABASES,
        DEBUG,
        SECRET_KEY,
        RESOURCES_ROOT,
        SENDFILE_BACKEND,
        ALLOWED_HOSTS,
        EMAIL_BACKEND,
        MESH_FILES_DIRECTORY,
        DEFAULT_FROM_EMAIL,
        MESH_JOURNAL_EMAIL_CONTACT,
    )
except ImportError:
    raise ImportError(
        "You must define a 'settings_local.py' file with some required settings. Check 'settings_local.sample.py'"
    )

# Facultative EMAIL variables from settings_local
try:
    from .settings_local import EMAIL_FILE_PATH  # type:ignore
except (NameError, ImportError):
    pass

try:
    from .settings_local import (
        EMAIL_HOST,  # type:ignore
        EMAIL_PORT,  # type:ignore
        EMAIL_USE_TLS,  # type:ignore
    )
except (NameError, ImportError):
    pass


BASE_DIR = Path(__file__).resolve().parent.parent
PTF_DIR = BASE_DIR.parent.parent
APPS_DIR = f"{PTF_DIR}/apps"

sys.path.append(APPS_DIR)

from mesh.ckeditor_config import MESH_CKEDITOR_CONFIGS

SITE_NAME = "MESH Site"
SITE_ID = 1

# Don't define allowed hosts other than the actual website base URL
# ALLOWED_HOSTS = []

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.sites",
    "django_extensions",
    "ptf",
    "mesh",
    "allauth",
    "allauth.account",
    "allauth.socialaccount",
    "ckeditor",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "mesh.middleware.ImpersonateMiddleware",  # ImpersonateMiddleware must be placed after all authentication middlewares.
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "allauth.account.middleware.AccountMiddleware",
]

ROOT_URLCONF = "mesh_site.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [
            f"{BASE_DIR}/mesh_site/templates",
            f"{APPS_DIR}/mesh/templates",
            f"{APPS_DIR}/ptf/templates",
        ],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = "mesh_site.wsgi.application"

# Password validation
# https://docs.djangoproject.com/en/4.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
        "OPTIONS": {"min_length": 9},
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

AUTH_USER_MODEL = "mesh.User"

# Internationalization
# https://docs.djangoproject.com/en/4.1/topics/i18n/

LANGUAGE_CODE = "en-us"

USE_TZ = True

TIME_ZONE = "UTC"

USE_I18N = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/4.1/howto/static-files/
STATIC_URL = "/static/"
STATIC_ROOT = f"{BASE_DIR}/static/"

STATICFILES_DIRS = [f"{BASE_DIR}/mesh_site{STATIC_URL}"]

MEDIA_URL = "/media/"
MEDIA_ROOT = os.path.join(RESOURCES_ROOT, "MESH", "media")
# Need to add /media in urls.py for local dev ??

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

#### [BEGIN] ALLAUTH Config ####
LOGIN_URL = "account_login"

LOGIN_REDIRECT_URL = "home"

# We don't need username, only email adress.
ACCOUNT_USER_MODEL_USERNAME_FIELD = None

ACCOUNT_USERNAME_REQUIRED = False

ACCOUNT_AUTHENTICATION_METHOD = "email"

ACCOUNT_EMAIL_REQUIRED = True

ACCOUNT_EMAIL_VERIFICATION = "mandatory"

ACCOUNT_CONFIRM_EMAIL_ON_GET = True

ACCOUNT_PREVENT_ENUMERATION = False

ACCOUNT_FORMS = {"signup": "mesh.forms.user_forms.SignupForm"}
#### [END] ALLAUTH Config ####


#### [BEGIN] PTF config ####
COLLECTION_PID = "MY_PID"
#### [END] PTF config ####

LOGIN_REDIRECT_URL = "mesh:home"

#### [BEGIN] Mesh config ####
AUTHENTICATION_BACKENDS = ["django.contrib.auth.backends.ModelBackend", "mesh.auth.TokenBackend"]

MESH_ENABLED_ROLES = ["author", "reviewer", "editor", "journal_manager"]

MESH_BLIND_MODE = "double_blind"

SENDFILE_ROOT = MESH_FILES_DIRECTORY

CKEDITOR_CONFIGS = MESH_CKEDITOR_CONFIGS

MESH_EMAIL_PREFIX = "[MESH SITE]"
ACCOUNT_EMAIL_SUBJECT_PREFIX = MESH_EMAIL_PREFIX
#### [END] Mesh config ####
