from allauth import urls as allauth_urls
from mesh import urls as mesh_urls

from django.conf import settings
from django.conf.urls.static import static
from django.contrib.admin import site
from django.urls import include
from django.urls import path
from django.urls import re_path

urlpatterns = [
    re_path(r"^admin/", site.urls),
    path("", include((mesh_urls, "mesh"), namespace="mesh")),
    path("accounts/", include(allauth_urls)),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
