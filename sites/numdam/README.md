# Numdam settings

#### option pour OAI : utiliser les date_published au lieu des dates de mise en ligne

il faut mettre le settings pour le site :

```bash
OAI_BY_DATE_PUBLISHED = True
```

et pour la définition du Repository OAI, mettre :

```bash
 'earliest_datestamp': Article.objects.filter(
                    sitemembership__site_id=settings.SITE_ID
                ).order_by('date_published').first().date_published.strftime('%Y-%m-%d'),
```

Ne prend en compte que les Articles actuellement


# Numdam workflow

#### Reset numdam-pre

1. remove the convent of /var/log/numdam/

2. Run bin/RAZ.sh of this git from numdam-pre to remove the content of the database and SolR

3. If SolR fails, you need to access the admin web page (numdam-pre.u-ga.fr:8983/solr), select core0, then "Documents".
   Select "XML" as the Document Type, <delete><query>_:_</query></delete> in the "Document(s)" field and click Submit
4. `python manage.py migrate` to apply the Django migations
5. Deploy the content from Numdam-plus
6. Deploy the Numdam content from Trammel
7. ` python manage.py import -pid CJPS -col_only -folder /tmp_mathdoc_archive -to_folder <a temp folder>` to fix images coming from Numdam-plus
