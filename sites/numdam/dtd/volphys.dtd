<?xml version="1.0" encoding="ISO-8859-1"?>

<!--
# DTD name : volphys.dtd
# Date de cr�ation: 29/09/2003
# Date de modification : 15/06/2014
# Version  : 1.3
# Description : Cette DTD decrit la structure des volumes des journaux des revues vivantes et ceux de num�ris� dans le cadre du programme NUMDAM
# Beaucoup d'�lements sont facultatifs. 
# Auteurs : E. Cherhal, H. Begnis, C. Zoppis-Barbes, S. Min-Picault, - Cellule MathDoc (UMS5638 - CNRS, UJF)
-->

<!--
# Les attributs suivants sont utilis�s uniquement dans le cadre de la mise en ligne. Donc ils ne sont pas � remplir dans le cadre d'op�ration de num�risation.
# nomref de autfields
# ordre_annee, label_annee, ordre_fascicule, label_fascicule de notice
# doi, date de article
# b, em, sub, sup, span, a, xref de bibitem (ces attributs sont rajout�s dans le cadre de revues nativement num�rique. Ce sont des balises styles ou ancre
-->

<!ELEMENT volphys (notice, (article | special)+)>
<!-- d�finition de l'auteur -->
<!ENTITY % autfields "nom, prenom, nomref*, initiale*, mel*, adresse*">

<!ENTITY % pageatts 'systnum (arabe | romain) "arabe"
        pagination (normal | doublon | volant) "normal"
	prefixe CDATA #IMPLIED
 	typepag (normal | supplement | special | vie | preliminaire) "normal" '>

<!-- notice -->
<!ELEMENT notice (idvol, revue, serie*, tome, fascicule?, isbn?, resp*, editeur, pages?, ordre_annee?, label_annee?, ordre_fascicule?, label_fascicule?, numerisation?, note*)>
<!-- idvol = un identifieur qui identifie le volume avec l'identifiant du journal, normalement il est contruit � partir des donn�es ci dessous (acronumdam, tome etc..)-->
<!ELEMENT idvol (#PCDATA)>
<!ELEMENT revue (issn, acronumdam, titre_revue*)>
<!-- issn est l'ISSN papier du volume -->
<!ELEMENT issn (#PCDATA)>
<!-- acronumdam = l'acronyme du journal-->
<!ELEMENT acronumdam (#PCDATA)>
<!-- titre_revue concerne d'abord le nom du s�minaire � afficher pour le feuilletage -->
<!ELEMENT titre_revue (#PCDATA)>
<!ELEMENT serie (#PCDATA)>
<!ELEMENT tome (numero, annee, titre_vol*, responsable*)>
<!ELEMENT numero (#PCDATA)>
<!ELEMENT titre_vol (#PCDATA)>
<!-- responsable = liste des differents directeurs de s�minaires -->
<!ELEMENT responsable (#PCDATA)>
<!-- numero = un entier de n � n-->
<!ELEMENT annee (#PCDATA)>
<!ELEMENT fascicule (#PCDATA)>
<!ELEMENT isbn (#PCDATA)>
<!-- resp = le responsable de l'�dition du volume, par exemples actes d'un congr�s)-->
<!ELEMENT resp (#PCDATA)>
<!-- editeur = le d�tenteur du copyright-->
<!ELEMENT editeur (#PCDATA)>
<!-- pages = un entier repr�sentant le nombre total de pages physiques du volume-->
<!ELEMENT pages (#PCDATA)>
<!-- ordre_annee = reference pour l'affichage des volumes par regroupement d'ann�es -->
<!ELEMENT ordre_annee (#PCDATA)>
<!-- ann�e(s) � afficher -->
<!ELEMENT label_annee (#PCDATA)>
<!-- ordre pour l'affichage des fascicules d'un groupement d'ann�es -->
<!ELEMENT ordre_fascicule (#PCDATA)>
<!-- num�ro de fascicule � afficher (entier sinon 'non numerot�')-->
<!ELEMENT label_fascicule (#PCDATA)>
<!-- num�risation ainsi que ses sous-�lements sont remplis par l'op�rateur de num�risation. Cet �lement donne des caract�ristiques techniques.-->
<!ELEMENT numerisation (idphys, datescan, infos)>
<!-- idphys = correspondance avec le r�pertoire correspondant dans le systeme de fichiers physique-->
<!ELEMENT idphys (#PCDATA)>
<!-- format date aaaa-mm-jj-->
<!ELEMENT datescan (#PCDATA)>
<!--info concernant les caract�ristiques du volume ou la num�risation, r�solution, niveau de gris, couleur, rempli de mani�re automatique �  partir des infos fournies par le scanneur-->
<!ELEMENT infos (#PCDATA)>
<!--note = permet de signaler les anomalies ou particularit�s concernant le volume-->
<!ELEMENT note (#PCDATA)>
<!-- /notice -->

<!-- special -->
<!--special (un ou plusieurs �l�ments) ainsi que ses sous �lements sont remplis par l'op�rateur, nous attendons surtout les �lements de type photo, planche, carte et horstexte, sommaire nous avons laisse les autres types au cas o� ils serviraient un jour-->
<!ELEMENT special ((pagedeb | pagefin)+, langue*, ordre?, titre*, note*, cphys)>
<!ATTLIST special
	type (pagetitre | sommaire | index | pagepub | pageblanche | ours | horstexte | special | planche | carte | photo | errata ) #REQUIRED >
<!-- pagedeb et pagefin risquent souvent d'avoir la meme valeur, et l'attribut pagination risque souvent d'avoir la valeur volant-->
<!ELEMENT pagedeb (#PCDATA)>
<!ATTLIST pagedeb
	%pageatts; >
<!ELEMENT pagefin (#PCDATA)>
<!ATTLIST pagefin
	%pageatts; >
<!ELEMENT langue (#PCDATA)>
<!--Ordre dans la page, en general vaut 0-->
<!ELEMENT ordre (#PCDATA)>
<!--correspondance physique relation vers la liste des identifiants physiques ou noms des fichiers monopages dans le syst�me de fichiers constituant le present �lement)-->
<!ELEMENT cphys (#PCDATA)>
<!-- /special -->

<!-- article -->
<!ELEMENT article (idart, doi?, date?, ordreart?, num_expose?, (pagedeb | pagefin)+, nbpages?, ordre, auteur+, contributeur*, titre+, titrecomplement*, relations*, langue+, note*, cphys?, resume*, motcle*, msc?, filename?, biblio?)>
<!ATTLIST article
	type (normal | preface | postface | erratum | corrigendum | addendum | pv) "normal">
<!--idart correspond a idvol plus numero d'article-->
<!ELEMENT idart (#PCDATA)>
<!-- DOI = 'Digital Object Identifier' (digital identifier for any object of intellectual property) -->
<!ELEMENT doi (#PCDATA)>
<!-- date = date de cr�ation/modification du XML au format aaaa-mm-jj -->
<!ELEMENT date (#PCDATA)>
<!--ordre de l'article dans la revue - utilis�e quand chaque article � une pagination propre/sp�ciale de 1 � n, donc une num�rotation non continue dans le volume-->
<!ELEMENT ordreart (#PCDATA)>
<!-- num�ro d'expos� de l'article dans le s�minaire -->
<!ELEMENT num_expose (#PCDATA)>
<!-- nbpages = nombre de pages physique de l'article -->
<!ELEMENT nbpages (#PCDATA)>
<!-- d�finition de l'auteur -->
<!ELEMENT auteur (%autfields;)>
<!ELEMENT nom (#PCDATA)>
<!ELEMENT prenom (#PCDATA)>
<!ELEMENT nomref (#PCDATA)>
<!ELEMENT initiale (#PCDATA)>
<!ELEMENT mel (#PCDATA)>
<!ELEMENT adresse (#PCDATA)>
<!ELEMENT contributeur (%autfields;)>
<!ATTLIST contributeur
	role (traducteur | redacteur | prefacier | collaborateur) #IMPLIED >
<!ELEMENT titre ANY>
<!--xml:lang est un attribut pr�d�fini: codes en 2 caract�res, orig est le titre dans la langue originel-->
<!ATTLIST titre
	xml:lang CDATA #IMPLIED
	orig CDATA #IMPLIED >
<!ELEMENT titrecomplement (#PCDATA)>
<!--xml:lang est un attribut pr�d�fini : codes en 2 caract�res, type pr�cise le type de complement-->
<!ATTLIST titrecomplement
	xml:lang CDATA #IMPLIED
        type (normal | indexation) #IMPLIED >
<!-- relation vers l'idnumdam de l'article-->
<!ELEMENT relations (#PCDATA)>
<!-- obligatoire si l'article a un type erratum corrigendum ou addendum, l'attribut doit �tre mis en cons�quence-->
<!ATTLIST relations
	type (corrige | estcorrige | complete | estcomplete | suitede | estsuivide | pagesprec | pagessuiv | apoursolution | solutionde  | commente | estcommente ) #REQUIRED >
<!ELEMENT resume ANY>
<!ATTLIST resume
	xml:lang CDATA #IMPLIED >
<!ELEMENT motcle (#PCDATA)>
<!ATTLIST motcle
	xml:lang CDATA #IMPLIED >
<!ELEMENT msc (#PCDATA)>
<!ELEMENT filename (#PCDATA)>

<!-- bibliographie  -->
<!ELEMENT biblio (bibitem*)>
<!ELEMENT bibitem (#PCDATA | bauteur | btitre | bsoustitre | brevue | bconference | bserie | btome | bfasc | bnumed | bediteur | blieued | bannee | bpagedeb | bpagefin | bcoll | buri | b | em | sub | sup | span | a | xref )*>
<!-- bauteur = nom de l'auteur -->
<!ELEMENT bauteur (#PCDATA | bprenom | bnom)*>
<!ELEMENT bprenom (#PCDATA)>
<!ELEMENT bnom (#PCDATA)>
<!-- btitre = titre de l'article -->
<!ELEMENT btitre ANY>
<!-- bsoustitre = sous-titre de l'article -->
<!ELEMENT bsoustitre (#PCDATA)>
<!-- brevue = titre de la revue -->
<!ELEMENT brevue (#PCDATA)>
<!-- bconference = titre de la conf�rence -->
<!ELEMENT bconference (#PCDATA)>
<!-- bserie = num�ro de la serie ou titre -->
<!ELEMENT bserie (#PCDATA)>
<!-- btome = num�ro du tome ou titre -->
<!ELEMENT btome (#PCDATA)>
<!-- bfasc = num�ro du fascicule ou titre -->
<!ELEMENT bfasc (#PCDATA)>
<!--  bnumed = num�ro d'�dition -->
<!ELEMENT bnumed (#PCDATA)>
<!-- bediteur = nom de l'�diteur -->
<!ELEMENT bediteur (#PCDATA)>
<!-- blieued = lieu d'�dition -->
<!ELEMENT blieued (#PCDATA)>
<!-- bannee = ann�e d'�dition -->
<!ELEMENT bannee (#PCDATA)>
<!-- bpagedeb et bpagefin = num�ro de page -->
<!ELEMENT bpagedeb (#PCDATA)>
<!ELEMENT bpagefin (#PCDATA)>
<!-- bcoll = num�ro ou titre de la collection -->
<!ELEMENT bcoll ANY>
<!-- buri = num�ro identifiant de l'article cit� -->
<!ELEMENT buri (#PCDATA)>

<!ELEMENT b ANY>
<!ELEMENT em ANY>
<!ELEMENT sub ANY>
<!ELEMENT sup ANY>
<!ELEMENT span ANY>
<!ATTLIST span
        class CDATA #IMPLIED >
<!ELEMENT a ANY>
<!ATTLIST a
        href CDATA #IMPLIED >
<!ELEMENT xref (#PCDATA)>
<!-- /article -->
