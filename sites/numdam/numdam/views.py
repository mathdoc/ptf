import random

from extra_views import FormSetSuccessMessageMixin
from extra_views import ModelFormSetView

from django.conf import settings
from django.forms import TextInput
from django.http import Http404
from django.shortcuts import redirect
from django.shortcuts import render
from django.urls import reverse
from django.views.decorators.http import require_http_methods
from django.views.generic import RedirectView

from ptf import model_helpers
from ptf.models import Article
from ptf.models import Collection
from ptf.models import Stats

from .forms import FormSetHelper


class CollectionLiveManagement(FormSetSuccessMessageMixin, ModelFormSetView):
    model = Collection
    fields = [
        "pid",
        "alive",
    ]
    factory_kwargs = {
        "extra": 0,
        "widgets": {
            "pid": TextInput(attrs={"readonly": "true"}),
        },
    }
    template_name = "collection_formset.html"
    success_message = "Statuts mis à jour avec succès"

    def get_queryset(self):
        # First, we need to update all child collections alive field to false
        Collection.objects.filter(parent__isnull=False).update(alive=False)
        # Then, we can continue the normal process of the view
        return super().get_queryset().filter(parent__isnull=True)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["helper"] = FormSetHelper()
        return context


def home(request):
    # new = models.SerialHistory.objects.select_related('serial').all()

    page_count = 0
    stat = Stats.objects.first()
    if stat:
        page_count = stat.value

    article_count = Article.objects.count()
    journal_count = model_helpers.get_journals().count()
    acta_count = model_helpers.get_actas().count()
    book_count = model_helpers.get_books().count()
    book_collection_count = model_helpers.get_collection_of_books(with_lectures=True).count()
    thesis_count = model_helpers.get_theses().count()

    carousel = []
    # latest_containers  = model_helpers.get_random_containers()
    latest_pids = settings.CAROUSEL_NEW_PIDS
    random.shuffle(latest_pids)
    carousel_count = 0
    all_pids = []

    # for container in latest_containers:
    for pid in latest_pids:
        resource = model_helpers.get_resource(pid)
        if resource:
            obj = resource.cast()
            collection = obj.get_collection()
            icon = collection.small_icon() if collection.pid != "CJPS" else obj.small_icon()

            item = {
                "title": obj.get_collection().title_tex,
                "icon": icon,
                "new": True,
                "volume": obj.get_volume(),
                "number": obj.get_number(),
                "pid": pid,
            }
            carousel.append(item)
            carousel_count += 1
            all_pids.append(pid)

            if carousel_count == 6:
                break

    pids = settings.CAROUSEL_PIDS
    random.shuffle(pids)

    for pid in pids:
        if pid not in all_pids:
            resource = model_helpers.get_resource(pid)
            if resource:
                obj = resource.cast()

                item = {
                    "title": obj.title_tex,
                    "icon": obj.small_icon(),
                    "new": False,
                    "volume": "",
                    "number": "",
                    "pid": pid,
                }

                carousel.append(item)

                carousel_count += 1

                if carousel_count == 12:
                    break

    random.shuffle(carousel)
    context = {
        "page_count": page_count,
        "article_count": article_count,
        "journal_count": journal_count,
        "acta_count": acta_count,
        "book_count": book_count,
        "book_collection_count": book_collection_count,
        "thesis_count": thesis_count,
        "carousel": carousel,
    }

    return render(request, "home.html", context)


# prise en compte d'une redirection pour des liens fournis sur GALLICA BNF
# -> lien fourni par le REPOX oai gallica
@require_http_methods(["GET"])
def gallicaBnf(request):
    """
    redirect to the collection page
    @param request:
    @return:
    """
    colid = request.GET.get("j")
    journal = model_helpers.get_collection(colid)

    if journal is None:
        raise Http404
    return redirect(reverse("journal-issues", kwargs={"jid": colid}))


@require_http_methods(["GET"])
def complete_works(request, api=False):
    context = {}

    return render(request, "complete-works.html", context)


class RedirectOldUrlView(RedirectView):
    pattern_name = "item_id"

    def get_redirect_url(self, *args, **kwargs):
        if not (kwargs.get("pid", None) or self.request.GET.get("id", None)):
            raise Http404

        the_pid = kwargs.get("pid", None)

        if not the_pid:
            kwargs["pid"] = self.request.GET.get("id")
            kwargs["pid"] = kwargs["pid"].rstrip(".djvu").rstrip(".pdf")

        if "extension" in kwargs:
            kwargs.pop("extension")

        return super().get_redirect_url(*args, **kwargs)


def app_logout(request):
    pass
