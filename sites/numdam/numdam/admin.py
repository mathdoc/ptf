from django.contrib import admin

from ptf.models import Article
from ptf.models import Collection
from ptf.models import Container
from ptf.models import Contrib
from ptf.models import ContribGroup
from ptf.models import ExtLink
from ptf.models import Provider
from ptf.models import PtfSite


class ExtLinkInline(admin.StackedInline):
    model = ExtLink


class SerialAdmin(admin.ModelAdmin):
    inlines = [ExtLinkInline]
    ordering = ["title_sort"]


@admin.register(Collection)
class CollectionAdmin(admin.ModelAdmin):
    inlines = [ExtLinkInline]
    # change_list_template = "back-end/django-admin/collections_changelist.html"


admin.site.register(Container)


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    search_fields = ("pid",)
    # change_list_template = "back-end/django-admin/remove_all_articles.html"


admin.site.register(Provider)

admin.site.register(ExtLink)

admin.site.register(ContribGroup)

admin.site.register(Contrib)

admin.site.register(PtfSite)
