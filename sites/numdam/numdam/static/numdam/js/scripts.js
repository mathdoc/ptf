$(document).ready(function () {
    $(".letter").on("click", function () {
        var selection = $(this).html();

        $(".letter").removeClass("active");
        $(this).addClass("active");

        $(".letter-div").hide();
        $(".letter-" + selection).show();
    });

    $(".owl-carousel").owlCarousel({
        autoPlay: 10000,
        items: 4,
    });
});
