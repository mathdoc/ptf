function fillGraphNode(node) {
    let label = "";
    label += '<div class="graph-text';
    if (node.active) {
        label += " active";
    }
    if (node.numdam_url) {
        label +=
            '"><div class="list-group list-group-emphasis"><a href="' +
            node.numdam_url +
            '" class="list-group-item list-group-item-info';
    }

    label +=
        '"><strong><p class="node-title" data-toggle="tooltip" data-placement="bottom" title="' +
        node.name +
        '">' +
        node.name +
        "</p></strong>";

    if (node.first_year || node.last_year) {
        label += '<p><div class="issn">';
        if (node.e_issn) {
            label += "e-issn:" + node.e_issn;
        }
        if (node.p_issn) {
            if (node.e_issn) {
                label += " - ";
            }
            label += "issn:" + node.p_issn;
        }
        label += "</div></p>";
    }
    label += "<p>";
    if (node.first_year || node.last_year) {
        label += '<span class="label label-info label-years">';
        if (node.first_year) {
            label += node.first_year;
        }
        label += " - ";
        if (node.last_year) {
            if (node.last_year == 9999) {
                label += "...";
            } else {
                label += node.last_year;
            }
        }
        label += "</span>";
    }
    if (node.numdam_url) {
        label +=
            '<img class="pull-right" src="/static/numdam/img/numdam_favicon.ico"></p></a></div>';
    } else {
        label += "</p>";
    }
    label += "</div>";

    var dict = {
        labelType: "html",
        label: label,
        class: "comp",
        rx: 5,
        ry: 5,
    };

    if (node.active) {
        dict["class"] = "active";
    }
    return dict;
}

function updateGraphData(data, g) {
    var nodes = data.nodes;
    var links = data.links;
    var canvas = document.createElement("canvas");
    var ctx = canvas.getContext("2d");

    // Add nodes
    nodes.forEach(function (node) {
        dict = fillGraphNode(node);
        g.setNode(node.id, dict);
    });

    // Add links
    links.forEach(function (link) {
        g.setEdge(link.source, link.target, {
            arrowhead: "vee",
            class: "normal",
            lineCurve: d3.curveBasis,
        });
    });
}

function renderGraph(g) {
    var render = new dagreD3.render();
    var svg = d3.select("svg");
    var svgGroup = svg.append("g");
    render(d3.select("svg g"), g);

    // Adjust SVG height and width to content
    var svgParent = $("#journal-graph > svg");
    var main = svgParent.find("g > g");
    var h = main.get(0).getBoundingClientRect().height;
    var w = main.get(0).getBoundingClientRect().width;
    var newHeight = h + 40;
    var newWidth = w + 100;
    newHeight = newHeight < 80 ? 80 : newHeight;
    newWidth = newWidth < 200 ? 200 : newWidth;
    svgParent.height(newHeight);
    svgParent.width(newWidth);

    // Delete class label in g (it messes up with bootstrap css)
    $("g.label").removeClass("label").addClass("node-label");
}

function createGraph(data) {
    // Preparation of DagreD3 data structures
    var g = new dagreD3.graphlib.Graph()
        .setGraph({
            nodesep: 30,
            ranksep: 30,
            rankdir: "BT",
            marginx: 10,
            marginy: 10,
        })
        .setDefaultEdgeLabel(function () {
            return {};
        });
    updateGraphData(data, g);
    renderGraph(g);
}

$("#history-modal").on("shown.bs.modal", function (e) {
    // If Graph has allready been created, we do not need to redraw it
    if ($("#svg-canvas").children().length == 1) {
        createGraph(family_graph, $("#journal-graph > svg"));
    }
});

// Ici, un clic sur la div de l'historique ferme le modal
// Dans l'idéal, il faudrait restreindre ce comportement au ancres vers les sections.
// Mais cela ne fonctionne pas car ces ancres sont incrustées dans des balises <svg>
$(".modal-body").on("mouseup", function () {
    $("#history-modal").modal("hide");
});

// Quick and Dirty Hack :
// Because hidding the modal takes time, we need to wait 1ms before replacing location
// to selected anchor.
// If not, the window will ascend as much as the modal height
$("#history-modal").on("hidden.bs.modal", function (e) {
    if (window.location.href.includes("#")) {
        setTimeout(() => {
            location.replace(window.location.href);
        }, 1);
    }
});

$(document).ready(function () {
    var falseHistoryPids = ["BSMF", "MSMF", "MRR", "OGEO"];
    var pid = $("#journal-graph").attr("data-pid");
    if (!falseHistoryPids.includes(pid)) {
        var issn = $("#journal-graph").attr("data-issn");
        var e_issn = $("#journal-graph").attr("data-e-issn");
        if (issn || e_issn) {
            if (!issn) {
                var url = "https://cfp.mathdoc.fr/api/periodiques/issn/" + e_issn + "/history/";
            } else {
                var url = "https://cfp.mathdoc.fr/api/periodiques/issn/" + issn + "/history/";
            }
            $.get(url).done(function (result) {
                family_graph = result.family_graph;
                if (family_graph.nodes.length > 1) {
                    $("#history-button").removeClass("hidden");
                    $.each(family_graph.nodes, function (key, node) {
                        if (node.p_issn) {
                            var issn = node.p_issn;
                        } else {
                            var issn = node.e_issn;
                        }
                        var url = "/api/collection/issn/" + issn + "/";
                        $.get(url).done(function (result) {
                            node.numdam_url = result.url;
                        });
                    });
                }
            });
        }
    }
});
