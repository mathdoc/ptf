import os
import socket
import sys

from configurations import Configuration


class Base(Configuration):
    # Django settings for numdam project.
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    PTF_DIR = os.path.dirname(os.path.dirname(BASE_DIR))
    APPS_DIR = "%s/apps" % PTF_DIR

    sys.path.append(APPS_DIR)

    from ptf.site_register import SITE_REGISTER

    SITE_NAME = "numdam"
    SITE_ID = SITE_REGISTER[SITE_NAME]["site_id"]
    SITE_DOMAIN = SITE_REGISTER[SITE_NAME]["site_domain"]
    COLLECTION_PID = SITE_REGISTER[SITE_NAME]["collection_pid"]

    ADMINS = (("labbeo", "olivier.labbe@univ-grenoble-alpes.fr"),)

    # MANAGERS = ADMINS

    # Local time zone for this installation. Choices can be found here:
    # http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
    # although not all choices may be available on all operating systems.
    # On Unix systems, a value of None will cause Django to use the same
    # timezone as the operating system.
    # If running in a Windows environment this must be set to the same as your
    # system time zone.
    TIME_ZONE = "Europe/Paris"

    # Language code for this installation. All choices can be found here:
    # http://www.i18nguy.com/unicode/language-identifiers.html
    LANGUAGE_CODE = "fr"
    LANGUAGES = (("fr", "French"), ("en", "English"))
    # If you set this to False, Django will make some optimizations so as not
    # to load the internationalization machinery.
    USE_I18N = True

    # If you set this to False, Django will not format dates, numbers and
    # calendars according to the current locale
    USE_L10N = True

    HOST_FQDN = socket.getfqdn()
    if HOST_FQDN == "numdam-prod.u-ga.fr":
        HOST_FQDN = "www.numdam.org"

    # Absolute filesystem path to the directory that will hold user-uploaded files.
    # Example: "/home/media/media.lawrence.com/media/"
    MEDIA_ROOT = "%s/media/" % APPS_DIR

    # URL that handles the media served from MEDIA_ROOT. Make sure to use a
    # trailing slash.
    # Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
    MEDIA_URL = "/media/"

    # Absolute path to the directory static files should be collected to.
    # Don't put anything in this directory yourself; store your static files
    # in apps' "static/" subdirectories and in STATICFILES_DIRS.
    # Example: "/home/media/media.lawrence.com/static/"
    # STATIC_ROOT = '%s/static/' % BASE_DIR
    STATIC_ROOT = "%s/static/" % BASE_DIR

    # URL prefix for static files.
    # Example: "http://media.lawrence.com/static/"
    STATIC_URL = "/static/"

    # URL prefix for admin static files -- CSS, JavaScript and images.
    # Make sure to use a trailing slash.
    # Examples: "http://foo.com/static/admin/", "/static/admin/".
    ADMIN_MEDIA_PREFIX = "/static/admin/"

    # STATICFILES_DIRS += (
    #     '%s/static/' % INSTANCE_ROOT,
    #     '%s/ptf/static/'
    # )

    STATICFILES_DIRS = (
        "%s/numdam/static/" % BASE_DIR,
        "%s/ptf/static" % APPS_DIR,
        "%s/mersenne_cms/static" % APPS_DIR,
    )

    LOCALE_PATHS = ("%s/locale/" % BASE_DIR,)

    ROOT_URLCONF = "numdam.urls"

    TEMPLATES = [
        {
            "BACKEND": "django.template.backends.django.DjangoTemplates",
            "APP_DIRS": True,
            "DIRS": [
                "%s/templates" % BASE_DIR,
                "%s/ptf/templates" % APPS_DIR,
                "%s/oai/templates" % APPS_DIR,
            ],
            "OPTIONS": {
                "context_processors": [
                    "django.template.context_processors.debug",
                    "django.template.context_processors.request",
                    "django.template.context_processors.i18n",
                    "django.contrib.auth.context_processors.auth",
                    "django.contrib.messages.context_processors.messages",
                    "ptf.context_processors.ptf",
                ],
            },
        },
    ]

    INSTALLED_APPS = (
        "django.contrib.auth",
        "django.contrib.contenttypes",
        "django.contrib.sessions",
        "django.contrib.messages",
        "django.contrib.staticfiles",
        "django.contrib.flatpages",
        "django_extensions",
        "crispy_forms",
        "ptf",
        "kbart",
        "oai",
        "django.contrib.sites",
        "numdam",
        "django.contrib.admin",
        "crawl",
    )

    USE_NATURAL_ID = True
    TEMPLATEDB = "ptf.display.templatedb"

    ALLOWED_HOSTS = ["*"]

    OAI_REPOSITORY = "numdamlibs.oai.repository"

    MIDDLEWARE = [
        "django.contrib.sessions.middleware.SessionMiddleware",
        # 'ptf.middleware.ForceDefaultLanguageMiddleware',
        "django.middleware.locale.LocaleMiddleware",
        "django.middleware.common.CommonMiddleware",
        "django.middleware.csrf.CsrfViewMiddleware",
        "django.contrib.auth.middleware.AuthenticationMiddleware",
        "django.contrib.messages.middleware.MessageMiddleware",
        "django.contrib.flatpages.middleware.FlatpageFallbackMiddleware",
    ]

    USE_TZ = True

    ARTICLE_BASE_URL = "/article/"
    ISSUE_BASE_URL = "/issue/"
    ICON_BASE_URL = "/icon/"

    DEFAULT_COVER = '<img src="' + ICON_BASE_URL + '/couv_ampa.jpg"/>'
    DOI_BASE_URL = "https://doi.org/"

    RESOURCES_ROOT = "/numdam_data"

    # repository sous la forme URL_oai, 'NomClassduRepository'
    REPOSITORIES = {"http://%s/oai/" % HOST_FQDN: "NumdamRepository"}

    CAROUSEL_PIDS = [
        "AMPA",
        "AIF",
        "ASENS",
        "ASNSP",
        "BSMF",
        "CM",
        "CRMATH",
        "M2AN",
        "JSFS",
        "PMIHES",
        "SB",
        "SMJ",
    ]
    CAROUSEL_NEW_PIDS = ["XUPS_2011___", "RFM_1996__1_"]

    SHOW_DJVU = True

    # utilisation d'un settings_local.py
    # http://stackoverflow.com/questions/1626326/how-to-manage-local-vs-production-settings-in-django

    # import * is not allowed in a class, use __import__ and add all variables manually
    # from ptf.settings import *
    module = __import__("ptf.settings")
    ptf_settings = module.settings
    d = dir(ptf_settings)
    for key in d:
        if key[0] != "_":
            value = getattr(ptf_settings, key)
            locals()[key] = value

    LOCK_FILE = "/var/www/numdam/shared/lock.txt"

    try:
        # from numdam.settings_local import *
        module = __import__("numdam.settings_local")
        ptf_settings = module.settings_local
        d = dir(ptf_settings)
        for key in d:
            if key[0] != "_":
                value = getattr(ptf_settings, key)
                locals()[key] = value

    except ImportError:
        raise Exception("You must defined a settings_local.py")

    try:
        if LOCAL_TEMPLATES_DEBUG_OPTION:
            TEMPLATES[0]["OPTIONS"]["debug"] = LOCAL_TEMPLATES_DEBUG_OPTION
        if LOCAL_INSTALLED_APPS:
            INSTALLED_APPS += LOCAL_INSTALLED_APPS

        for key, value in LOCAL_REPOSITORIES.items():
            REPOSITORIES[key] = value
    except NameError:
        pass

    MEDIA_URL = "/media/"
    ICON_URL = "/icon/"
    try:
        if RESOURCES_ROOT:
            ICON_ROOT = RESOURCES_ROOT
    except NameError:
        pass

    # COMPATIBILITE NUMDAM

    HOST_FQDN = socket.getfqdn()
    # without / at the end
    BASE_URL = "http://%s" % HOST_FQDN

    USE_META_COLLECTIONS = True

    # see https://django-crispy-forms.readthedocs.io/en/latest/install.html
    CRISPY_TEMPLATE_PACK = "bootstrap3"


class Test(Base):
    DEBUG = True
    DATABASES = {"default": {"ENGINE": "django.db.backends.sqlite3", "NAME": "db_name.sqlite3"}}
    RESOURCES_ROOT = "tests/data"
    SOLR_URL = "http://127.0.0.1:8983/solr/core0-test"
    MAX_RESULT_SIZE = 7
    REPOSITORIES = {"http://127.0.0.1/oai/": "NumdamRepository"}
    SECRET_KEY = "dsfqsdlkfjlkqjsdhokqdfhgkdqfhgildfhgidfhgiqdfhghdfghsfiughel"
    TEMP_FOLDER = "/tmp"
    MERSENNE_TMP_FOLDER = "/tmp"
    MERSENNE_LOG_FILE = MERSENNE_TMP_FOLDER + "/transformations.log"
    LOG_DIR = "/tmp"
