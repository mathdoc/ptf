"""
WSGI config for numdam project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/howto/deployment/wsgi/
"""

import logging
import os
import sys

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "numdam.settings")
os.environ.setdefault("DJANGO_CONFIGURATION", "Base")

from configurations.wsgi import get_wsgi_application

# maniere 'brutale' d envoyer les logs vers /var/log/apache/error.log en particulier pour les 500 :
# http://stackoverflow.com/questions/8007176/500-error-without-anything-in-the-apache-logs
logging.basicConfig(stream=sys.stderr)

application = get_wsgi_application()
