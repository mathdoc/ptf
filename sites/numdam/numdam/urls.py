from django.conf import settings
from django.conf.urls.static import static
from django.urls import include
from django.urls import path
from django.urls import re_path
from django.views.generic import TemplateView

from numdam.views import CollectionLiveManagement
from numdam.views import RedirectOldUrlView
from numdam.views import home

urlpatterns = [
    path('', home, name="home"),
    path('', include('oai.urls')),
    path('', include('ptf.urls')),
    path('about/', TemplateView.as_view(template_name="about.html"), name='about'),
    path('dml/', TemplateView.as_view(template_name="dml.html"), name='dml'),
    path('credits/', TemplateView.as_view(template_name="credits.html"), name='credits'),

    path('help/', TemplateView.as_view(template_name="help.html"), name='help'),
    path('formats/', TemplateView.as_view(template_name="formats.html"), name='formats'),
    path('fulltext/', TemplateView.as_view(template_name="fulltext.html"), name='fulltext'),
    path('reuse/', TemplateView.as_view(template_name="reuse.html"), name='reuse'),
    path('links/', TemplateView.as_view(template_name="links.html"), name='links'),
    path('metadata/', TemplateView.as_view(template_name="metadata.html"), name='metadata'),
    path('references/', TemplateView.as_view(template_name="help-references.html"), name='help_references'),
    path('click_read/', TemplateView.as_view(template_name="click-read.html"), name='click_read'),

    path('legal/', TemplateView.as_view(template_name="legal.html"), name='legal'),
    re_path(r'^legal.php/$', TemplateView.as_view(template_name="conditions.html")),
    path('conditions/', TemplateView.as_view(template_name="conditions.html"), name='conditions'),
    path('wall/', TemplateView.as_view(template_name="wall.html"), name='wall'),
    path('copyright/', TemplateView.as_view(template_name="copyright.html"), name='copyright'),

    path('fields/', TemplateView.as_view(template_name="fields.html"), name='fields'),
    path('abel/', TemplateView.as_view(template_name="abel.html"), name='abel'),
    path('wolf/', TemplateView.as_view(template_name="wolf.html"), name='wolf'),
    path('gold/', TemplateView.as_view(template_name="gold.html"), name='gold'),

    # re_path(r'^complete-works', views.complete_works, name='complete-works'), futur : OCLC

    path('contact/', TemplateView.as_view(template_name="contact.html"), name='contact'),
    re_path(r'^ARCHIVE.*\/(?P<pid>[-_0-9a-zA-Z]*)\.*(?P<extension>pdf|djvu)*$',
        RedirectOldUrlView.as_view(), name='redirect_old_url_archive'),
    # Compatibility with link in http://sites.mathdoc.fr/cgi-bin/spitem?id=1003
    re_path(r'^numdam-bin.*/item\/(?P<pid>[-_0-9a-zA-Z]+)\.(?P<extension>pdf|djvu)$',
        RedirectOldUrlView.as_view(), name='redirect_old_url_numdambin'),
    re_path(r'^numdam-bin.*/item\/(?P<pid>[-_0-9a-zA-Z]*)$',
        RedirectOldUrlView.as_view(), name='redirect_old_url_numdambin_no_extension'),
    path('kbart/', include('kbart.urls')),
    path('collection/live/management/', CollectionLiveManagement.as_view(), name='collection-live-management'),
]

# # prise en compte d'une redirection pour des liens fournis sur GALLICA BNF -> lien fourni par le REPOX oai gallica
# OBSO avec la nouvelle version d'OAI prise en compte par Gallica : 15/6/2017
# urlpatterns += [
#     re_path(r'^numdam-bin/feuilleter$', gallicaBnf, name='gallica_bnf'),
# ]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.ICON_URL, document_root=settings.ICON_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
