# db params and secret_key are set in environment variables
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",  # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        "NAME": "numdam",  # Or path to database file if using sqlite3.
        "USER": "numdam_user",  # Not used with sqlite3.
        "PASSWORD": "numdam_password",  # Not used with sqlite3.
        "HOST": "localhost",  # Set to empty string for localhost. Not used with sqlite3.
        "PORT": "",  # Set to empty string for default. Not used with sqlite3.
    }
}

DEBUG = True

LOG_DIR = "/var/log/numdam/"

# Make this unique, and don't share it with anybody.
SECRET_KEY = "change_me"


SOLR_URL = "http://127.0.0.1:8983/solr/numdam"
INTERNAL_IPS = ["127.0.0.1"]
EUDML_AUTHORIZED_IP = ["127.0.0.1", "213.135.60.110", "193.219.28.34", "152.77.212.60"]
RESOURCES_ROOT = "/numdam_data"

SENDFILE_BACKEND = "django_sendfile.backends.development"
SENDFILE_ROOT = RESOURCES_ROOT
