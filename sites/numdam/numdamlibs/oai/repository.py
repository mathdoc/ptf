from django.conf import settings

from oai.repository import OAIRepository
from ptf.models import SiteMembership


class NumdamRepository(OAIRepository):
    def Identify(self):
        return {
            "name": "numdam",
            "base_url": "http://www.numdam.org/oai",
            "protocol_version": "2.0",
            "admin_email": "webmaster@numdam.org",
            "earliest_datestamp": SiteMembership.objects.filter(site__id=settings.SITE_ID)
            .order_by("deployed")
            .first()
            .deployed.strftime("%Y-%m-%d"),
            "deleted_record": "no",
            "granularity": "YYYY-MM-DD",
            "repositoryIdentifier": "numdam.org",
            "delimiter": ":",
            "sample_identifier": "oai:numdam.org:AIF_1971__21_3_1_0",
        }


def get_repository(base_url):
    # thismodule = sys.modules[__name__]
    # repo = settings.REPOSITORIES[base_url]
    # repo_class = getattr(thismodule, repo)
    # return repo_class(base_url)
    return NumdamRepository(base_url)
