from cr_app.views import CRHomeView as AbstractCRHomeView

from ptf.site_register import SITE_REGISTER


class CRGeosHomeView(AbstractCRHomeView):
    _sites = [SITE_REGISTER["crgeos"]["site_id"]]

    def _get_class(self):
        return CRGeosHomeView
