import random

from django.conf import settings

from mersenne_cms.models import MERSENNE_ID_HOMEPAGE
from mersenne_cms.models import News
from mersenne_cms.models import Page
from mersenne_cms.views import HomeView


class AFSTHomeView(HomeView):
    template_name = getattr(settings, "CMS_HOME_TEMPLATE", "mersenne_cms/page_detail.html")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page"] = Page.objects.get(mersenne_id=MERSENNE_ID_HOMEPAGE)

        if News.objects.exists():
            all_news = list(News.objects.all())
            random.shuffle(all_news)
            context["news"] = all_news[0]

        return context
