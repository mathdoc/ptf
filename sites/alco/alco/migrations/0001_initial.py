# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations
from django.contrib.auth.hashers import make_password


def apply_migration(apps, schema_editor):

    Group = apps.get_model('auth', 'Group')
    group_admin = Group.objects.get(name='cms-admin')
    group_writer = Group.objects.get(name='cms-writer')

    User = apps.get_model('auth', 'User')
    admin_user = User(
        username='admin-alco',
        email='admin@alco.com',
        password=make_password('admin'),
        is_superuser=False,
        is_staff=True
    )
    admin_user.save()

    admin_user.groups.add(group_admin)
    admin_user.groups.add(group_writer)
    admin_user.save()

def revert_migration(apps, schema_editor):
    User = apps.get_model('auth', 'User')
    User.objects.filter(username="admin-alco").delete()

class Migration(migrations.Migration):
    dependencies = [
        ('auth', '0001_initial'),
        ('ptf', '0010_auto_20170822_1553'),
        ('mersenne_cms', '0007_load_initial_data')
    ]

    operations = [
        migrations.RunPython(apply_migration, revert_migration),
    ]
