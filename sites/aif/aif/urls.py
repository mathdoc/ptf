"""mercene_aif URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  re_path(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  re_path(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import include
    2. Add a URL to urlpatterns:  re_path(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.urls import include
from django.urls import path
from django.urls import re_path

from mersenne_cms.admin import mersenne_admin
from mersenne_cms.views import NextVolumeView
from ptf.urls import ptf_i18n_patterns

urlpatterns = [
    path('', include('mersenne_cms.urls')),
    path('', include('ptf.urls')),
    path('', include('oai.urls')),
    path('coming-issues/', NextVolumeView.as_view(), name='coming-issues'),
]

urlpatterns += ptf_i18n_patterns

if settings.ALLOW_ADMIN:
    urlpatterns += [
        path('upload/', include('upload.urls')),
        re_path(r'^admin/', mersenne_admin.urls),
    ]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
