"""jedp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  re_path(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  re_path(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import include
    2. Add a URL to urlpatterns:  re_path(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.urls import include
from django.urls import path
from django.urls import re_path

from mersenne_cms import urls as cms_urls
from mersenne_cms.admin import mersenne_admin
from oai import urls as oai_urls
from ptf import urls as ptf_urls
from ptf.views import LatestArticlesFeed
from upload import urls as upload_urls

urlpatterns = [
    path('', include(ptf_urls)),
    path('', include(cms_urls)),
    path('', include(oai_urls)),
]

urlpatterns += i18n_patterns(
    re_path(r'^latest/feed/(?P<name>[\w]+)/$', LatestArticlesFeed(), name='latest-articles-feed'),
)

if settings.ALLOW_ADMIN:
    urlpatterns += [
        path('upload/', include(upload_urls)),
        re_path(r'^admin/', mersenne_admin.urls),
    ]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
