=========
PTF TOOLS
=========

rabbitmq
~~~~~~~~
::

    sudo apt-get install rabbitmq-server

Configuration de rabbitmq (https://docs.celeryproject.org/en/stable/getting-started/brokers/rabbitmq.html)::

    sudo rabbitmqctl add_user guest guest_pwd
    sudo rabbitmqctl add_vhost ptf_host
    sudo rabbitmqctl set_permissions -p ptf_host guest ".*" ".*" ".*"

celery
~~~~~~

Celery peut être lancé à chaque fois de la manière suivante (https://docs.celeryproject.org/en/stable/django/first-steps-with-django.html#starting-the-worker-process)::

    celery -A ptf_tools worker -l info

Il est cependant mieux de lancer les worker celery en tant que démons (https://docs.celeryproject.org/en/stable/userguide/daemonizing.html#usage-systemd)
Pour ce faire, créer les fichers suivants :

/etc/systemd/system/celery.service::

    [Unit]
    Description=Celery Service
    After=network.target rabbitmq-server.service
    Requires=rabbitmq-server.service

    [Service]
    Type=forking
    User=<user>
    Group=<user>
    EnvironmentFile=/etc/conf.d/celery
    WorkingDirectory=/home/<user>/Projects/ptf/sites/ptf_tools
    ExecStart=/bin/sh -c '${CELERY_BIN} multi start ${CELERYD_NODES} \
      -A ${CELERY_APP} --pidfile=${CELERYD_PID_FILE} \
      --logfile=${CELERYD_LOG_FILE} --loglevel=${CELERYD_LOG_LEVEL} ${CELERYD_OPTS}'
    ExecStop=/bin/sh -c '${CELERY_BIN} multi stopwait ${CELERYD_NODES} \
      --pidfile=${CELERYD_PID_FILE}'
    ExecReload=/bin/sh -c '${CELERY_BIN} multi restart ${CELERYD_NODES} \
      -A ${CELERY_APP} --pidfile=${CELERYD_PID_FILE} \
      --logfile=${CELERYD_LOG_FILE} --loglevel=${CELERYD_LOG_LEVEL} ${CELERYD_OPTS}'

    [Install]
    WantedBy=multi-user.target

/etc/conf.d/celery::

    # Name of nodes to start
    # here we have a single node
    CELERYD_NODES="w1"
    # or we could have three nodes:
    #CELERYD_NODES="w1 w2 w3"

    # Absolute or relative path to the 'celery' command:
    CELERY_BIN="/home/<user>/Projects/ptf/sites/ptf_tools/venv/bin/celery"
    #CELERY_BIN="/virtualenvs/def/bin/celery"

    # App instance to use
    # comment out this line if you don't use an app
    CELERY_APP="ptf_tools"
    # or fully qualified:
    #CELERY_APP="proj.tasks:app"

    # How to call manage.py
    CELERYD_MULTI="multi"

    # Extra command-line arguments to the worker
    CELERYD_OPTS="--time-limit=300 --concurrency=8"

    # - %n will be replaced with the first part of the nodename.
    # - %I will be replaced with the current child process index
    #   and is important when using the prefork pool to avoid race conditions.
    CELERYD_PID_FILE="/var/run/celery/%n.pid"
    CELERYD_LOG_FILE="/var/log/celery/%n%I.log"
    CELERYD_LOG_LEVEL="INFO"

/etc/tmpfiles.d/celery.conf::

    d /var/run/celery 0755 <user> <user> -
    d /var/log/celery 0755 <user> <user> -

Créer les dossiers suivants avec les bons droits::

    sudo mkdir -p /var/run/celery/
    sudo mkdir -p /var/log/celery/
    sudo chown -R <user>:<user> /var/run/celery
    sudo chown -R <user>:<user> /var/log/celery

Lancer le démon celery::

    sudo systemctl daemon-reload
    sudo systemctl start celery.service
    sudo systemctl enable celery.service

