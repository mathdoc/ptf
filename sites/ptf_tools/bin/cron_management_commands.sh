#!/bin/bash
# Execute a list of django management commands.
# To be added to ptf-tools deployer's crontab. Example every hour:
# VENV=/var/www/ptf_tools/current/venv
# @hourly bash {path_to_ptf}/sites/ptf_tools/bin/cron_management_commands.sh >> /tmp/cron_log.log 2>&1
set -eo pipefail

cd `dirname $0`
cd ..
DIR_PATH=`pwd`
cd ../..
bash shell_scripts/execute_management_command.sh $DIR_PATH clearsessions
bash shell_scripts/execute_management_command.sh $DIR_PATH handle_new_comments
