import getopt
import os
import sys

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ptf_tools.settings")

sys.path.append(".")

from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()

from ptf.models import Article
from sites.ptf_tools.ptf_tools import doi


def usage():
    print("Usage: ")
    print("check_dois.py -c COLID")
    print("pour vérifier les DOIs de tous les articles d'une collection")
    print('(pour repérer les Erreur -> | grep Erreur  ou grep - "Mauvaise URL")')
    print("or")
    print("check_dois.py -r COLID")
    print("pour ré enregistrer tous les DOIs de tous les articles publiés d'une collection")
    sys.exit(1)


try:
    opt, args = getopt.getopt(sys.argv[1:], "c:r:")
except getopt.GetoptError:
    usage()
if args or not opt:
    usage()
recordDoi = False
check = False
colid = None
for o, v in opt:
    if o == "-r":
        recordDoi = True
        colid = v
    elif o == "-c":
        check = True
        colid = v
    else:
        usage()
qs = Article.objects.exclude(date_online_first__isnull=True, date_published__isnull=True).filter(
    my_container__my_collection__pid=colid
)
total = qs.count()
i = 0
for article in qs:
    if article.doi:
        i = i + 1
        if check:
            doibatch = doi.get_or_create_doibatch(article)
            print(
                f"check {i} sur {total}"
                + " - "
                + article.doi
                + " - "
                + doibatch.status
                + " - "
                + doibatch.log
            )
        elif recordDoi:
            print(f"record {i} sur {total}" + " - " + article.doi)
            doi.recordDOI(article)
