#!/bin/bash
# recompile article with date up to date on mathdoc-tex
# cmd = "ssh %s@mathdoc-tex 'bash -s' -- < %s/bin/create_frontpage.sh %s %s %s" % (
#                    user, ptf_tools_bin, article_path, article_name, colid)
#
#article_path=$1
source ~cedrics/setup.sh
export PATH="/home/cedrics/texlive/2022/bin/x86_64-linux:$PATH"
cd $1
mv $2.pdf $2.pdf_SAV

if [  "$3" == "PCJ" ]; then
    dopax.sh $4
fi

latexmk -pdf $2.tex
#result=$?
#if [ $result == 0 ]
#then
#	echo "\ItIsPublished" >> $2.ptf
#else
#	exit $result
#fi
