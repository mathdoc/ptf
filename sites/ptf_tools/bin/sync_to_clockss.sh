#!/bin/bash
# synchronise le répertoire mathdoc_archive XML + IMG + PDF/DJVU (sans les src) avec CLOCKSS
# a mettre dans crontab de deployer
# deployer@ptf-tools:~$ crontab -l
# 35 18 * * * /var/www/ptf_tools/current/sites/ptf_tools/bin/sync_to_clockss.sh
rm /tmp/error_sync_clockss.log
source /var/www/ptf_tools/current/venv/bin/activate
cd  /var/www/ptf_tools/current/sites/ptf_tools
STATUS="OK"
CONTAINER=""
python -W ignore manage.py listforclockss > /tmp/listeContainer.lst
if [ $? -ne 0 ];
then
STATUS="WARNING"
CONTAINER="container avec article ayant un DOI et d'autre non"
fi
#recupere les login / passwd depuis settings_local.py (pas de passwd dans git)
CLOCKSS_USER=`python -W ignore manage.py shell -c 'from django.conf import settings ;  print(settings.CLOCKSS_USER)'`
CLOCKSS_PASSWORD=`python -W ignore manage.py shell -c 'from django.conf import settings ;  print(settings.CLOCKSS_PASSWORD)'`
CLOCKSS_ERROR_MAIL=`python -W ignore manage.py shell -c 'from django.conf import settings ;  print(settings.CLOCKSS_ERROR_MAIL)'`


for i in  ` cat /tmp/listeContainer.lst `
  do
#utilisation de lftp
RESULT=`/usr/bin/lftp -e "set net:timeout 5s;set net:max-retries 1;set net:reconnect-interval-base 1;set net:reconnect-interval-multiplier 1 ;mirror -R --exclude-glob *.json --exclude=src --max-errors=3 --parallel=20  -e -n /mathdoc_archive/$i $i  ; bye " -u $CLOCKSS_USER,"$CLOCKSS_PASSWORD" ftp.clockss.org 2>>/tmp/error_sync_clockss.log `
   if [ $? -ne 0 ];
     then
STATUS="ERROR"
     fi

  done
if [ $STATUS != "OK" ];
then
RESULT=`cat /tmp/error_sync_clockss.log`
mail -s "[PTF-TOOLS] synchro CLOCKSS en erreur" $CLOCKSS_ERROR_MAIL <<EOF
$RESULT
EOF
fi

#on fait le niveau collection
STATUS="OK"
rm /tmp/error_sync_clockss.log
cat /tmp/listeContainer.lst | cut -d '/' -f 1 | sort | uniq > /tmp/listeCollection.lst
for i in  ` cat /tmp/listeCollection.lst `
  do
#utilisation de lftp avec -r pour non récursif
RESULT=`/usr/bin/lftp -e "set net:timeout 5s;set net:max-retries 1;set net:reconnect-interval-base 1;set net:reconnect-interval-multiplier 1 ;mirror -R -r --exclude=src --exclude-glob *.json --max-errors=3 --parallel=20  -e -n /mathdoc_archive/$i $i  ; bye " -u $CLOCKSS_USER,"$CLOCKSS_PASSWORD" ftp.clockss.org 2>>/tmp/error_sync_clockss.log `
   if [ $? -ne 0 ];
     then
STATUS="ERROR"
     fi

  done
if [ $STATUS != "OK" ];
then
RESULT=`cat /tmp/error_sync_clockss.log`
mail -s "[PTF-TOOLS] synchro CLOCKSS en erreur" $CLOCKSS_ERROR_MAIL <<EOF
$RESULT
EOF
fi



INCLUDE=`cat /tmp/listeContainer.lst`
MSG="<pre>"$RESULT"\nListe des répertoires à inclure :\n"$INCLUDE"<pre>"
jq -n --arg status $STATUS --arg message "$MSG" '{"type": "clockss", "pid": "ALL", "col": "ALL", "status": $status, "data": {"ids_count": 0, "message": $message}}' > /tmp/synchistory.json

python -W ignore manage.py loghistory --file /tmp/synchistory.json
#rm /tmp/synchistory.json
