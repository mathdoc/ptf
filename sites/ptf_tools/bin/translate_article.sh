#!/bin/bash
# recompile article with date up to date on mathdoc-tex
# cmd = "ssh %s@mathdoc-tex 'bash -s' -- < %s/bin/create_frontpage.sh %s %s %s" % (
#                    user, ptf_tools_bin, article_path, article_name, colid)
#
#article_path=$1
source ~cedrics/setup.sh

article_path=$1
xml_file=$2
html_file=$3
lang=$4

cd $article_path
/home/bernaupa/traduction-pdf/import-traduction $xml_file $html_file $lang
/home/bernaupa/traduction-pdf/pdf-traduction $xml_file $lang
