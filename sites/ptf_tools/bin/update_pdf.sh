#!/bin/bash

cp $2 ~/temp/in_pdf
pdftk ~/temp/in_pdf output ~/temp/out.pdf
cp ~/temp/out.pdf $2
