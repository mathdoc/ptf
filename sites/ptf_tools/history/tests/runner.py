"""
To run the test, execute in sites/ptf_tools/history
python manage.py test history --failfast --settings="history.tests.settings"
"""

from django.test.runner import DiscoverRunner


class NoDbTestRunner(DiscoverRunner):
    """A test runner to test without database creation"""

    def setup_databases(self, **kwargs):
        """Override the database creation defined in parent class"""

    def teardown_databases(self, old_config, **kwargs):
        """Override the database teardown defined in parent class"""
