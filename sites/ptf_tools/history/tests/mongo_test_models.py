"""
To run the test, execute in sites/ptf_tools/history
python manage.py test history --failfast --settings="history.tests.settings"
"""

from django.test import TestCase

from history.models import get_db
from history.models import get_history_error_warning_counts
from history.models import get_history_events
from history.models import test_create


class HistoryTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        print("setUpTestData: Run once to set up non-modified data for all class methods.")
        test_create("testdb")
        grouped_events = get_history_events({}, "testdb")

        print()
        for group in grouped_events:
            for event in group["events"]:
                print(
                    "%30s" % event["pid"],
                    "%10s" % event["type"],
                    "%7s" % event["status"],
                    "%5s" % str(event["obsolete"]) if "obsolete" in event else "------",
                    event["created_on"],
                    sep="   ",
                )
        print()

    def setUp(self):
        print("setUp: Run once for every test method to setup clean data.")

    def tearDown(self):
        print("tearDown: Clean up run after every test method.\n")

        db = get_db("testdb")

        if "events" in db.collection_names():
            collection = db.events
            collection.drop()

    def test_main(self):
        print("Method: test_main.")

        db = get_db("testdb")
        events = db.events

        # 1 document should have been merged
        size = events.count_documents({})
        self.assertEqual(size, 9)

        # 2 groups
        grouped_events = get_history_events({}, "testdb")
        self.assertEqual(len(grouped_events), 2)

        # 1 obsolete event
        obsolete_events = list(events.find({"obsolete": True}))
        self.assertEqual(len(obsolete_events), 1)

        merged_events = list(
            events.find({"pid": "AIF_2009__59_7_2593_0"}).sort([("$natural", -1)]).limit(1)
        )
        event = merged_events[0]
        self.assertEqual(event["message"], "[7] zbl:zbl8 checked<br/>[2] zbl:zbl4 false_positive")

        warning_count, error_count = get_history_error_warning_counts("testdb")
        self.assertEqual(warning_count, 0)
        self.assertEqual(error_count, 1)
