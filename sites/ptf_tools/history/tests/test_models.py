"""
To run the test, execute in sites/ptf_tools/history
python manage.py test history --failfast
python manage.py test history --failfast --settings="history.tests.settings" for Mongo
"""

import pytest

from django.test import TestCase

from history.models import get_history_events
from history.models import get_history_last_event_by
from history.models import test_create

pytestmark = pytest.mark.django_db


@pytest.mark.django_db
class HistoryTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        print("setUpTestData: Run once to set up non-modified data for all class methods.")
        test_create()

    def setUp(self):
        print("setUp: Run once for every test method to setup clean data.")

    def tearDown(self):
        print("tearDown: Clean up run after every test method.\n")

    def test_main(self):
        print("Method: test_main.")

        grouped_events = get_history_events({})

        print()
        # for month, count, error_count, warning_count, events in grouped_events:
        for group in grouped_events:
            month = group["month"]
            error_count = group["error_count"]
            warning_count = group["warning_count"]
            events_in_month = group["events_in_month"]
            print(month, error_count, warning_count, sep="    ")
            for event in events_in_month:
                print(
                    "%30s" % event.pid,
                    "%10s" % event.type,
                    "%7s" % event.status,
                    event.created_on,
                    sep="   ",
                )
            print()

        # Test get_history_last_event_by
        last_event = get_history_last_event_by("matching", "AIF")
        self.assertEqual(last_event.pid, "AIF_2015__65_6_2331_0")

        last_event = get_history_last_event_by("edit")
        self.assertEqual(last_event.pid, "JEP_2014__1_7_352_0")

        last_event = get_history_last_event_by("matching", "AIF_2015__65_6")
        self.assertEqual(last_event.pid, "AIF_2015__65_6_2331_0")

        # Test filters
        filtered_events = get_history_events({"status": "warning", "month": "all"})
        self.assertEqual(len(filtered_events), 2)

        filtered_events = get_history_events({"status": "error", "month": "all"})
        self.assertEqual(len(filtered_events), 1)

        filtered_events = get_history_events({"col": "AIF", "type": "edit", "month": "all"})
        self.assertEqual(len(filtered_events), 2)
        self.assertEqual(len(filtered_events[1]["events_in_month"]), 4)

        # test regroup All issues of a collection
        filtered_events = get_history_events({"col": "ALCO", "type": "deploy"})
        self.assertEqual(len(filtered_events), 1)
        self.assertEqual(len(filtered_events[0]["events_in_month"]), 1)
        event = filtered_events[0]["events_in_month"][0]
        issues = event.data["issues"]
        self.assertEqual(len(issues), 4)
