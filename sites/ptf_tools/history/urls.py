from django.urls import path

from history.views import HistoryClearView
from history.views import HistoryEventDeleteView
from history.views import HistoryView

urlpatterns = [
    path('history/', HistoryView.as_view(), name='history'),
    path('history/clear/', HistoryClearView.as_view(), name='history-clear'),
    path('history/<int:pk>/delete/', HistoryEventDeleteView.as_view(), name='history-delete'),
]