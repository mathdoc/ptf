import traceback

from requests.exceptions import Timeout

from django.conf import settings
from django.urls import reverse_lazy
from django.utils import timezone
from django.views.generic import TemplateView
from django.views.generic.base import ContextMixin
from django.views.generic.edit import DeleteView

from history.models import HistoryEvent
from history.models import get_history_error_warning_counts
from history.models import get_history_events
from history.models import insert_history_event
from matching import matching
from ptf import views as ptf_views
from ptf.exceptions import ServerUnderMaintenance


def manage_exceptions(operation, pid, colid, status, exception, target=""):
    if type(exception).__name__ == "ServerUnderMaintenance":
        message = " - ".join([str(exception), "Please try again later"])
    else:
        message = " - ".join([str(exception), traceback.format_exc()])

    print("exception for " + colid + " " + pid)
    message = "<br>".join(message.split("\n"))

    insert_history_event(
        {
            "type": operation,
            "pid": pid,
            "col": colid,
            "status": status,
            "data": {
                "ids_count": 0,
                "message": message,
                "target": target,
            },
        }
    )


def matching_decorator(func, is_article):
    def inner(*args, **kwargs):
        seq = None
        if is_article:
            article = args[0]
        else:
            article = args[0].resource.cast()
            seq = args[0].sequence
        pid = article.pid
        colid = article.get_top_collection().pid

        what = args[1]
        list_ = what.split("-")
        id_type = what if len(list_) == 0 else list_[0]

        try:
            id_value = func(*args, **kwargs)
            if id_value:
                id_dict = {"type": id_type, "id": id_value}
                if seq:
                    id_dict["seq"] = seq
                ids = [id_dict]

                insert_history_event(
                    {
                        "type": "matching",
                        "pid": pid,
                        "col": colid,
                        "status": "OK",
                        "data": {"message": "", "ids_count": 1, "ids": ids},
                    }
                )
            else:
                insert_history_event(
                    {
                        "type": "matching",
                        "pid": pid,
                        "col": colid,
                        "status": "OK",
                        "data": {"message": "", "ids_count": 0, "ids": []},
                    }
                )

        except Timeout as exception:
            """
            Exception caused by the requests module: store it as a warning
            """
            manage_exceptions("matching", pid, colid, "WARNING", exception)
            raise exception

        except ServerUnderMaintenance as exception:
            manage_exceptions("deploy", pid, colid, "ERROR", exception)
            raise exception

        except Exception as exception:
            manage_exceptions("matching", pid, colid, "ERROR", exception)
            raise exception

        return id_value

    return inner


# decorate matching.match_bibitem and match_article
matching.match_bibitem = matching_decorator(matching.match_bibitem, False)
matching.match_article = matching_decorator(matching.match_article, True)


def execute_and_record_func(
    type, pid, colid, func, message="", record_error_only=False, *func_args, **func_kwargs
):
    status = 200
    func_result = None
    try:
        func_result = func(*func_args, **func_kwargs)

        if not record_error_only:
            insert_history_event(
                {
                    "type": type,
                    "pid": pid,
                    "col": colid,
                    "status": "OK",
                    "data": {"message": message},
                }
            )
    except Timeout as exception:
        """
        Exception caused by the requests module: store it as a warning
        """
        manage_exceptions(type, pid, colid, "WARNING", exception, target=message)
        raise exception
    except Exception as exception:
        manage_exceptions(type, pid, colid, "ERROR", exception, target=message)
        raise exception
    return func_result, status, message


def edit_decorator(func):
    def inner(cls_inst, action, *args, **kwargs):
        resource_obj = cls_inst.resource.cast()
        pid = resource_obj.pid
        colid = resource_obj.get_top_collection().pid

        message = ""
        if hasattr(cls_inst, "obj"):
            # Edit 1 item (ExtId or BibItemId)
            obj = cls_inst.obj
            if hasattr(cls_inst, "parent"):
                parent = cls_inst.parent
                if parent:
                    message += "[" + str(parent.sequence) + "] "
            list_ = obj.id_type.split("-")
            id_type = obj.id_type if len(list_) == 0 else list_[0]
            message += id_type + ":" + obj.id_value + " " + action
        else:
            message += "All " + action

        args = (cls_inst, action) + args

        execute_and_record_func("edit", pid, colid, func, message, False, *args, **kwargs)

    return inner


ptf_views.UpdateExtIdView.update_obj = edit_decorator(ptf_views.UpdateExtIdView.update_obj)
ptf_views.UpdateMatchingView.update_obj = edit_decorator(ptf_views.UpdateMatchingView.update_obj)


class HistoryContextMixin(ContextMixin):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        error_count, warning_count = get_history_error_warning_counts()
        context["warning_count"] = warning_count
        context["error_count"] = error_count

        # if isinstance(last_clockss_event, datetime):
        #     now = timezone.now()
        #     td = now - last_clockss_event['created_on']
        #     context['last_clockss_event'] = td.days
        return context


class HistoryView(TemplateView, HistoryContextMixin):
    template_name = "history.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["collections"] = settings.MERSENNE_COLLECTIONS

        all_filters = ["type", "col", "status", "month"]
        filters = {}

        for filter in all_filters:
            value = self.request.GET.get(filter, None)
            if value:
                filters[filter] = value

            params = [
                f"{key}={value}"
                for key, value in self.request.GET.items()
                if key not in [filter, "page", "search"]
            ]
            # Insert empty string to make sure '&' is added before the first param
            params.insert(0, "")
            params_str = "&".join(params)
            context[filter + "_link"] = "?search=" + params_str

        # MongoDB can create groups, SQL does not
        # We use the Django template 'regroup' builtin function to regroup the events in the template
        grouped_events = get_history_events(filters)
        context["grouped_events"] = grouped_events
        context["now"] = timezone.now()

        # events = get_history_events(filters)
        # context['events'] = events

        return context


class HistoryClearView(DeleteView):
    model = HistoryEvent
    template_name_suffix = "_confirm_clear"
    success_url = reverse_lazy("history")

    def get_object(self):
        return self.get_queryset().get_stale_events()


class HistoryEventDeleteView(DeleteView):
    model = HistoryEvent
    success_url = reverse_lazy("history")
