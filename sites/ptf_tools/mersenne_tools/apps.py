from django.apps import AppConfig


class MersenneToolsConfig(AppConfig):
    name = "mersenne_tools"
