from django.db import models

from ptf.models import Resource


class DOIBatch(models.Model):
    STATUS = (("Erreur", "Erreur"), ("Enregistré", "Enregistré"), ("En cours", "En cours"))

    resource = models.OneToOneField(Resource, on_delete=models.CASCADE)
    xml = models.TextField()
    status = models.CharField(max_length=32, choices=STATUS)
    log = models.TextField()


class DOAJBatch(models.Model):
    ERROR = "error"
    REGISTERED = "registered"
    UNREGISTERED = "unregistered"
    IRRELEVANT = "irrelevant"

    STATUS = (
        (ERROR, "Erreur"),
        (REGISTERED, "Enregistré"),
        (UNREGISTERED, "Non enregistré"),
        (IRRELEVANT, "Non applicable"),
    )

    resource = models.OneToOneField(Resource, on_delete=models.CASCADE)
    xml = models.TextField()
    status = models.CharField(max_length=32, choices=STATUS)
    log = models.TextField()
