# Trammel (or PTF-Tools) quickstart guide

#### Création des bases de données ptftools

```bash
sudo su - postgres
# On crée une base de données pour ptf-tools...
createuser -P ptftools_user
createdb ptftools -O ptftools_user

# On quitte le shell postgres
<ctrl-D>
```

#### Création des collections Solr (une pour ptf-tools et une pour les sites des revues)

```bash
sudo su - solr

# On crée une collection solr pour ptf-tools...
/opt/solr/bin/solr create -c ptftools
mv data/ptftools/conf/solrconfig.xml data/ptftools/conf/solrconfig.xml.backup
# <ptf-path> dans les commandes suivantes correspond au chemin d'accès vers le répertoire du projet ptf
ln -s /<ptf-path>/apps/ptf/solr/solrconfig.xml data/ptftools/conf/
ln -s /<ptf-path>/apps/ptf/solr/schema.xml data/ptftools/conf/


# Si on souhaite travailler sur le site numdam, il est nécessaire de lui créer une collection solr aussi
# Il suffit de le créer de la même manière que les deux autres en utilisant le nom "numdam"

# On quitte le shell solr
<ctrl-D>

# Et on relance le serveur pour qu'il prenne en compte les nouveaux coeurs
sudo systemctl restart solr.service
```

## Mise en place du serveur de dev de ptf-tools

Le projet ptf-tools se trouve dans le dossier sites/ptf_tools

```bash
cd sites/ptf_tools
```

#### Copie de la conf

```bash
cp ptf_tools/settings_local.sample.py ptf_tools/settings_local.py
```

Vérifier ensuite dans le fichier résultant que les constantes correspondent à l'environnement de développement

#### Installation des dépendances Python

```bash
python3 -m venv venv
source ./venv/bin/activate
pip install -r requirements.txt --upgrade
```

#### Migration de la base de données

```bash
python manage.py migrate
```

#### Création d'un superuser

```bash
python manage.py createsuperuser
```

et suivre les questions posées.

#### Importer une collection

Import de la collection depuis /mathdoc_archive
/mathdoc_archive contient les articles déjà publiés

Modifier /mathdoc_archive/JEP/JEP.xml et changer les 2 liens "https://jep.centre-mersenne.org" en "http:127.0.0.1:8008"

```bash
python manage.py import -pid JEP -folder /mathdoc_archive
```

Copie des fichiers sources des articles, dont ceux en cours d'édition

```bash
sudo mkdir -p /cedram_dev/production_tex/CEDRAM/
sudo chown -R <user>:<user> /cedram_dev
rsync -avzhe ssh ptf-tools:/cedram_dev/production_tex/CEDRAM/JEP /cedram_dev/production_tex/CEDRAM/
```

Les articles de /cedram_dev peuvent être importés depuis l'interface web de Trammel.

#### Lancement du serveur de dev

```bash
python -Wa manage.py runserver 8002
```

