# Comments Moderation application

This Django application contains all the logic related to the moderation aspect of the comments dashboard. \
See also the `comments_views.core` [documentation](/apps/comments_views/core/doc/comments_views_core.md) for information related to the comment list and the comment details views.

## I. Requirements

This application requires the following applications to work:

  - `comments_views.core`
  - `comments_api`
  - `invitations` (django-invitations)

## II. Rights & User roles

We have defined 3 user _roles_ for the moderation of comments.
An user's role is purely inferred from its groups and the user database object. \
The first 2 roles, comment _admin_ and comment _staff_, are collection based, _ie._ an user could be _admin_ for a given collection and _staff_ for another one. \
However, the current implementation assumes that an user cannot be both a admin moderator and a _staff_ or _base_ moderator.

### II.1. Collection _admin_ moderator

The collection _admin_ moderator role is automatically given to the collection's main users, _ie._ users in a group having rights over the corresponding collection (see [`CollectionGroup`](../../ptf_tools/models.py) model.).

These users can:
  - moderate all comments submitted on a article in a collection they manage.
  - designate existing users or invite new users as collection _staff_ moderator for one (or more) collection(s) they manage.
  - designate existing users or invite new users as basic comment moderator for one (or more) specific comment they can moderate.

### II.2. Collection _staff_ moderator

The collection _staff_ moderator role can only be given by collection _admin_ moderators. The corresponding rights are inferred using the [`CommentModerator`](../models.py) model.

These users can:
  - moderate any comment in a collection they have rights over.
  - designate existing users or invite new users as basic comment moderators for one (or more) specific comment they can moderate.


### II.3. Basic comment moderator

The basic comment moderator role can be given by both collection _admin_ and collection _staff_ moderators. Being a comment moderator is inferred using the [`CommentModerator`](../models.py) model. \
These users can only moderate a comment they have been assigned to by a moderator with higher rights. \
The comments they can moderate are directly returned by the comment server. This data is not stored in **ptf_tools** (Trammel).


## III. E-mails & Invitations

### III.1. E-mails
E-mails are sent on the following occasions:

  - When a comment is moderated. An e-mail is sent to both the article's author and the comment's author if the comment was validated. Otherwise an e-mail is sent to the comment's author only.

  - When a moderator is assigned to a comment. An e-mail is sent to the assigned moderator.

  - When a new comment is submitted. An e-mail is sent to the _staff_ moderators having rights over the collection of the comment's article. This is handled by a [cron](../../bin/handle_new_comments.sh) to avoid e-mail flooding.

  - When a new moderator is invited to the application. See __III.2. Invitation__.


### III.2. Invitations

This app has been the opportunity to integrate an invitation system to **ptf_tools** (Trammel) using the [django-invitations](https://github.com/jazzband/django-invitations) module. \
This is a simple implementation of an invitation system that can be plugged to **django-allauth**. One can create and send an invitation e-mail to any e-mail address which is not already used by another user nor already invited.

_Base_ and _staff_ moderators are given the right to invite users to the application as moderators. We store extra data in the invitation object when that happens (see [`Invitation`](../../ptf_tools/models.py) model). This data is used when the user signs up via the [`update_user_from_invite`](../../ptf_tools/signals.py) signal.
Basically the function:
  - updates the first and last names of the user from the invite.
  - flags the user as a comment moderator.
  - updates the user's collections, if any rights were given.
  - updates the user's comments, if he was assigned to any.
