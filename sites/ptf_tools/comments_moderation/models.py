from django.contrib.auth.models import User
from django.db import models

from ptf.models import Collection


class CommentModerator(models.Model):
    """
    Adds moderator data to the base Django User model.
    """

    user = models.OneToOneField(
        User,
        unique=True,
        on_delete=models.CASCADE,
        related_name="comment_moderator",
        primary_key=True,
    )
    is_moderator = models.BooleanField(default=False)
    collections = models.ManyToManyField(Collection, blank=True)

    def get_collections(self) -> str:
        return ", ".join([col.pid for col in self.collections.all()])

    def __str__(self) -> str:
        return self.user.username
