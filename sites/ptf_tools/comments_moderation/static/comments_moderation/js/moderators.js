function closeCustomDatalist(event) {
    event.stopPropagation()
    let datalist = document.querySelector(".custom-datalist.active")
    // Don't close datalist if its input is the target click
    let input = document.getElementById(datalist.getAttribute("aria-labelledby"))
    if (event.target == input ) {
        return
    }

    if (datalist) {
        datalist.classList.remove("active")
        datalist.style.display = "none"
    }
    document.removeEventListener("click", closeCustomDatalist)
}


function getCustomDatalistInput(datalist) {
    return document.getElementById(datalist.getAttribute("aria-labelledby"))
}


function selectCustomDatalistOption(option, datalist) {
    if (!option || !datalist) {
        return
    }

    let input = getCustomDatalistInput(datalist)
    input.value = option.innerText.trim()
    for (let optionBis of datalist.options) {
        optionBis.classList.remove("active")
    }
    option.classList.add("active")
    // Update form fields
    let moderatorForm = datalist.parentElement.closest(".moderator-modal-panel-content").querySelector("form.moderator-form")
    let idInput = moderatorForm.querySelector("input[type=\"hidden\"][name=\"moderator_id\"]")
    idInput.value = option.value
    let collections = option.dataset.collections?.split(",")
    // Update the collections the selected user has access to
    for (let collectionInput of moderatorForm.querySelectorAll("input[name=\"collections\"]")) {
        let inputValue = collectionInput.getAttribute("value")
        if (inputValue && collections?.includes(inputValue)) {
            collectionInput.checked = true
        }
        else {
            collectionInput.checked = false
        }
    }
}


/**
 * Sets the disabled attribute to true for submit buttons of class button-deactivable.
 */
function disableSubmitButtons() {
    Array.from(document.querySelectorAll("button[type='submit'].button-deactivable")).forEach((button) => {
        button.setAttribute("disabled", true)
    })
}


document.addEventListener("DOMContentLoaded", () => {
    // Moderation button: Validate & Reject a comment.
    Array.from(document.getElementsByClassName("comment-section-action-moderation")).forEach((btn) => {
        btn.addEventListener("click", (event) => {
            // Update the moderation form status and submit it
            event.stopPropagation()
            let formId = btn.dataset.formId
            // If the button does not mention an explicit formId, just submit the encapsulating form without
            // further processing.
            if (!formId){
                let form = btn.closest("form")
                form.submit()
                return
            }
            let form = document.getElementById(formId)
            let status = btn.dataset.status
            let statusField = form?.querySelector("input[id=\"id_status\"]")
            if (!status || !statusField) {
                console.log("Error: Moderator form badly configured.")
                return
            }
            statusField.setAttribute("value", status)
            disableSubmitButtons()
            form.submit()
        })
    })

    // Modal related code
    var addModeratorModal = document.getElementById("comment-add-moderator-modal")
    addModeratorModal.addEventListener("onopen", (event) => {
        let btn = event.detail?.button
        if (btn) {
            let moderatorId = btn.dataset.selectModeratorId
            let datalist = addModeratorModal.querySelector(`#${btn.dataset.datalistId}`)
            let searchInput = addModeratorModal.querySelector("#moderator-search")
            if (moderatorId && datalist) {
                let option = Array.from(datalist.options).find(opt => opt.value == moderatorId)
                selectCustomDatalistOption(option, datalist)
                searchInput.setAttribute("disabled", true)

            }
            else if (btn.dataset.modalPanelReset && btn.dataset.modalPanelReset === "true") {
                searchInput.value = ""
                addModeratorModal.querySelectorAll("#moderator-list option").forEach(el => el.classList.remove("active"))
                addModeratorModal.querySelectorAll(".moderator-form input").forEach((input) => {
                    if (input.name == "csrfmiddlewaretoken" || input.disabled || input.readonly) {
                        return
                    }
                    switch (input.type) {
                        case "checkbox":
                            input.checked = false
                            break
                        default:
                            input.value = ''
                    }
                })
            }
        }
    })
    addModeratorModal.addEventListener("onclose", () => {
        let searchInput = addModeratorModal.querySelector("#moderator-search")
        searchInput.removeAttribute("disabled")
    })

    // Custom datalist display
    Array.from(document.getElementsByClassName("custom-datalist")).forEach((datalist) => {
        let input = getCustomDatalistInput(datalist)
        if (input) {
            // Display options on focus
            input.addEventListener("focus", () => {
                datalist.style.display = "block"
                datalist.classList.add("active")
                setTimeout(() => document.addEventListener("click", closeCustomDatalist), 200)
            })
            // Logic for option selection
            for (let option of datalist.options) {
                option.addEventListener("click", () => {
                    selectCustomDatalistOption(option, datalist)
                })
            }
            // Input search
            input.addEventListener("input", () => {
                let searchValue = input.value.trim().toLowerCase()
                for (let option of datalist.options) {
                    let optionValue = option.innerText
                    if (optionValue.toLowerCase().indexOf(searchValue) > -1) {
                        option.style.display = "block"
                    }
                    else {
                        option.style.display = "none"
                    }
                }
            })
        }
    })

    // Disable all submit buttons on form submission
    Array.from(document.getElementsByClassName("form-deactivate-on-submit")).forEach((form) => {
        form.addEventListener("submit", () => {
            disableSubmitButtons()
        })
    })
})
