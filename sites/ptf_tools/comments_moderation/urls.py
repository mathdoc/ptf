from django.urls import path
from django.urls import re_path

from .views import AddBaseModeratorView
from .views import CommentAdminModeratorView
from .views import CommentChangeStatusView
from .views import CommentDashboardDetailsView
from .views import CommentDashboardListView
from .views import InviteBaseModeratorView
from .views import InviteStaffModeratorView
from .views import RemoveBaseModeratorView

urlpatterns = [
    # Comments dashboard URLs
    path("comments/", CommentDashboardListView.as_view(), name="comment_list"),
    path(
        "comments/<int:pk>/",
        CommentDashboardDetailsView.as_view(),
        name="comment_details"
    ),
    re_path(
        r"^comments/(?P<pk>[0-9]+)/(?P<edit>edit)/$",
        CommentDashboardDetailsView.as_view(),
        name="comment_edit"
    ),
    path(
        "comments/<int:pk>/status-change/light/",
        CommentChangeStatusView.as_view(),
        name="comment_status_change_light"
    ),
    path(
        "comments/<int:pk>/status-change/",
        CommentChangeStatusView.as_view(),
        name="comment_status_change"
    ),

    # Moderation URLs
    path(
        "comments/moderators/",
        CommentAdminModeratorView.as_view(),
        name="comment_moderators"
    ),
    path(
        "comments/moderators/add/",
        AddBaseModeratorView.as_view(),
        name="comment_moderator_add"
    ),
    path(
        "comments/moderators/remove/",
        RemoveBaseModeratorView.as_view(),
        name="comment_moderator_remove"
    ),
    path(
        "comments/moderators/invite/staff/",
        InviteStaffModeratorView.as_view(),
        name="comment_moderator_invite_staff"
    ),
    path(
        "comments/moderators/invite/base/",
        InviteBaseModeratorView.as_view(),
        name="comment_moderator_invite_base"
    )
]
