from django.contrib.auth.models import AbstractUser

from comments_api.constants import PARAM_ADMIN
from comments_api.constants import PARAM_BOOLEAN_VALUE
from comments_api.constants import PARAM_COLLECTION
from comments_api.constants import PARAM_MODERATOR
from comments_views.core.rights import AbstractUserRights
from ptf_tools.templatetags.tools_helpers import get_authorized_collections


class ModeratorUserRights(AbstractUserRights):
    COMMENT_POST_URL = ""
    # Cache of the user's authorized collections.
    _admin_collections = None
    _staff_collections = None

    def get_user_admin_collections(self) -> list[str]:
        if self._admin_collections is None:
            self._admin_collections = get_authorized_collections(self.user)
        return self._admin_collections

    def get_user_staff_collections(self) -> list[str]:
        if self._staff_collections is None:
            self._staff_collections = []
            if hasattr(self.user, "comment_moderator"):
                self._staff_collections = list(
                    self.user.comment_moderator.collections.all().values_list("pid", flat=True)
                )
        return self._staff_collections

    def comment_rights_query_params(self) -> dict:
        query_params = {}
        if isinstance(self.user, AbstractUser):
            query_params[PARAM_MODERATOR] = self.user.pk

            # NOT USED
            if self.user.is_superuser:
                query_params[PARAM_ADMIN] = PARAM_BOOLEAN_VALUE

            # Add other query parameters according to the current user rights
            collections = self.get_user_admin_collections() + self.get_user_staff_collections()
            if collections:
                query_params[PARAM_COLLECTION] = ",".join(sorted(set(collections)))

        return query_params

    def comment_can_delete(self, comment: dict) -> bool:
        """
        Moderators should not have the right to delete a comment.
        """
        return False

    def comment_can_edit(self, comment: dict) -> bool:
        """
        Moderators do not have the right to edit a comment.
        Only the comment's author can.
        """
        return False

    def comment_can_moderate(self, comment: dict) -> bool:
        """
        The user can moderate either if:
            - he/she has rights over the comment's collection (admin or staff moderator).
            - he/she has been selected as a moderator of the comment
        """
        return (
            self.user.is_superuser
            or comment["site_name"].upper() in self.get_user_admin_collections()
            or comment["site_name"].upper() in self.get_user_staff_collections()
            or self.user.pk in comment["moderators"]
        )

    def comment_can_manage_moderators(self, comment) -> bool:
        """
        The user can manage the moderators if the comment is in the user's collections.
        """
        return (
            comment["site_name"].upper() in self.get_user_admin_collections()
            or comment["site_name"].upper() in self.get_user_staff_collections()
            or self.user.is_superuser
        )

    def is_admin_moderator(self) -> bool:
        """An user is an admin moderator if he/she has 1+ collectiongroup."""
        return len(self.get_user_admin_collections()) > 0

    def is_staff_moderator(self) -> bool:
        """An user is a staff moderator if he/she has 1+ commentmoderator collection."""
        return len(self.get_user_staff_collections()) > 0
