from django.http import HttpRequest
from django.utils import translation

from comments_views.core.mixins import AbstractCommentRightsMixin

from .rights import ModeratorUserRights


class ModeratorCommentRightsMixin(AbstractCommentRightsMixin):
    @property
    def rights_class(self) -> type[ModeratorUserRights]:
        return ModeratorUserRights


class ForceLanguageMixin:
    """
    Forces the language to English.

    Trammel (ptf_tools) has internationalization enabled and we need to
    force the comments_moderation app's language to English.
    """

    def dispatch(self, request: HttpRequest, *args, **kwargs):
        translation.activate("en")
        return super().dispatch(request, *args, **kwargs)  # type:ignore
