from typing import Any

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.http import Http404
from django.http import HttpRequest
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import TemplateView
from django.views.generic import View

from comments_api.constants import PARAM_ACTION_CREATE
from comments_api.constants import PARAM_ACTION_DELETE
from comments_api.constants import STATUS_SUBMITTED
from comments_views.core.views import CommentChangeStatusView as BaseCommentChangeStatusView
from comments_views.core.views import (
    CommentDashboardDetailsView as BaseCommentDashboardDetailsView,
)
from comments_views.core.views import CommentDashboardListView as BaseCommentDashboardListView
from ptf_tools.models import Invitation
from ptf_tools.models import InvitationExtraData
from ptf_tools.models import InviteCollectionData
from ptf_tools.models import InviteCommentData

from .forms import AddBaseModeratorForm
from .forms import InviteBaseModeratorForm
from .forms import InviteStaffModeratorForm
from .forms import RemoveBaseModeratorForm
from .forms import StaffModeratorForm
from .mixins import ForceLanguageMixin
from .mixins import ModeratorCommentRightsMixin
from .utils import email_moderator_assigned
from .utils import get_all_moderators
from .utils import get_comment_and_can_manage_moderators
from .utils import get_comment_pending_invitations
from .utils import get_pending_invitations
from .utils import is_comment_moderator
from .utils import update_moderation_right
from .utils import update_moderator_collections


# Maybe create a custom "LoginRequiredMixin" where we additionally check that the user
# has some moderator rights. Otherwise restrict access.
class CommentDashboardListView(
    LoginRequiredMixin,
    ModeratorCommentRightsMixin,
    ForceLanguageMixin,
    BaseCommentDashboardListView,
):
    pass


class CommentDashboardDetailsView(
    LoginRequiredMixin,
    ModeratorCommentRightsMixin,
    ForceLanguageMixin,
    BaseCommentDashboardDetailsView,
):
    """Overloads the default view with additional moderator data."""

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        if "comment" not in context:
            return context

        comment = context["comment"]
        rights = self.get_rights(self.request.user)
        # Add the moderator data if required
        if rights.comment_can_manage_moderators(comment) and comment["status"] == STATUS_SUBMITTED:
            # Add moderator form
            context["form_moderators"] = True

            all_moderators, _ = get_all_moderators(rights, self.request)
            context["all_moderators"] = all_moderators

            # Populate modal forms
            initial_data = {"comment_id": int(kwargs["pk"])}
            context["moderator_add_form"] = AddBaseModeratorForm(initial=initial_data)
            context["moderator_add_form_url"] = reverse("comment_moderator_add")

            context["moderator_invite_form"] = InviteBaseModeratorForm(initial=initial_data)
            context["moderator_invite_form_url"] = reverse("comment_moderator_invite_base")

            # Remove moderator form
            moderators = comment.get("moderators_processed")
            if moderators:
                moderators_choice = tuple([(col, col) for col in moderators.keys()])
                context["moderator_remove_form"] = RemoveBaseModeratorForm(
                    moderators=moderators_choice, initial=initial_data
                )

            # Get the pending invites linked to the comment.
            context["invitations"] = get_comment_pending_invitations(comment["id"])

        return context


class CommentChangeStatusView(
    LoginRequiredMixin, ModeratorCommentRightsMixin, BaseCommentChangeStatusView
):
    pass


class CommentAdminModeratorView(
    LoginRequiredMixin, ModeratorCommentRightsMixin, ForceLanguageMixin, TemplateView
):
    """
    View for managing the comment moderators.
    Only available to "admin" and "staff" moderators.
    """

    template_name = "blocks/comment_moderators.html"

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        """
        Moderator management view.
        Get all moderators related to the current user:
            -   all "staff" moderators with one collection in common
                with the current user
            -   all "base" moderators with one comment related to the current
                user's collections.
        """
        rights = self.get_rights(self.request.user)
        if not rights.is_admin_moderator() and not rights.is_staff_moderator():
            raise Http404

        context = super().get_context_data(**kwargs)

        context["moderation_rights"] = rights
        user_collections = rights.get_user_admin_collections()

        context["user_collections"] = (
            user_collections if user_collections else rights.get_user_staff_collections()
        )

        all_moderators, moderation_rights = get_all_moderators(rights, self.request)

        # Post-process all moderators to categorize them as "staff" moderators
        # and "base" moderators
        # 1) "Staff" moderators
        all_moderators_dict = {}
        staff_moderators = []
        for mod in all_moderators:
            all_moderators_dict[mod.pk] = mod
            common_col = [
                col.pid
                for col in mod.comment_moderator.collections.all()  # type:ignore
                if col.pid in user_collections
            ]
            if common_col:
                staff_moderators.append(mod)

        context["all_moderators"] = all_moderators
        if rights.is_admin_moderator():
            context["staff_moderators"] = staff_moderators

        # 2) "Base" moderators
        if moderation_rights:
            base_moderators = {}
            staff_moderator_ids = [mod.pk for mod in staff_moderators]
            # Group comments per moderator
            # mod_right struct: {"moderator_id": x, "comment_id": x, "comment__status": x}
            for mod_right in moderation_rights:
                try:
                    moderator = int(mod_right["moderator_id"])
                except ValueError:
                    continue
                # Add an entry in base_moderators only if the given moderator
                # is not a staff moderators
                if moderator in all_moderators_dict and (
                    not staff_moderator_ids or moderator not in staff_moderator_ids
                ):
                    if moderator not in base_moderators:
                        moderator_to_cp = all_moderators_dict[moderator]
                        base_moderators[moderator] = {
                            "first_name": moderator_to_cp.first_name,
                            "last_name": moderator_to_cp.last_name,
                            "email": moderator_to_cp.email,
                            "pk": moderator_to_cp.pk,
                        }
                        base_moderators[moderator]["comments_processed"] = []
                        base_moderators[moderator]["comments_pending"] = []
                    comment_status = mod_right["comment__status"]
                    comment_id = mod_right["comment_id"]
                    if comment_status == STATUS_SUBMITTED:
                        base_moderators[moderator]["comments_pending"].append(comment_id)
                    else:
                        base_moderators[moderator]["comments_processed"].append(comment_id)

            if base_moderators:
                context["base_moderators"] = base_moderators.values()

        # Populate modal forms
        user_collections_choice = tuple([(col, col) for col in user_collections])
        context["moderator_add_form"] = StaffModeratorForm(user_collections_choice)
        context["moderator_add_form_url"] = reverse("comment_moderators")

        context["moderator_invite_form"] = InviteStaffModeratorForm(user_collections_choice)
        context["moderator_invite_form_url"] = reverse("comment_moderator_invite_staff")

        # Get all pending moderator invitations related to the current user
        context["invitations"] = get_pending_invitations(rights)

        return context

    def post(self, request: HttpRequest, *args, **kwargs) -> HttpResponseRedirect:
        """
        Updates an existing moderator's collections.
        The updated collections are limited to the collections the current user
        has access to.
        """
        rights = self.get_rights(request.user)
        if not rights.is_admin_moderator():
            raise Http404

        response = HttpResponseRedirect(reverse("comment_moderators"))
        user_collections = rights.get_user_admin_collections()
        user_collections_choice = tuple([(col, col) for col in user_collections])
        form = StaffModeratorForm(user_collections_choice, request.POST)

        if not form.is_valid():
            messages.error(request, "Something went wrong. Please try again.")
            return response

        cleaned_data = form.cleaned_data
        moderator_id = cleaned_data["moderator_id"]
        try:
            moderator = User.objects.filter(comment_moderator__is_moderator=True).get(
                pk=moderator_id
            )
        except User.DoesNotExist:
            messages.error(request, "Error: The chosen moderator does not exist.")
            return response

        selected_collections = cleaned_data["collections"]
        update_moderator_collections(rights, moderator, selected_collections, user_collections)

        if selected_collections:
            message = (
                f"The collections {', '.join(selected_collections)} have been"
                " added to the moderator "
                f"{moderator.first_name} {moderator.last_name}."
            )
        else:
            message = (
                f"The moderator {moderator.first_name} {moderator.last_name}"
                " has been removed from your staff moderators."
            )

        messages.success(request, message)
        return response


class AddBaseModeratorView(LoginRequiredMixin, ModeratorCommentRightsMixin, View):
    """
    View for adding an existing moderator to a specific comment.
    Limited to "admin" and "staff" moderators and to non-processed comments.
    """

    def post(self, request: HttpRequest, *args, **kwargs) -> HttpResponseRedirect:
        rights = self.get_rights(request.user)
        if not rights.is_admin_moderator() and not rights.is_staff_moderator():
            raise Http404

        response = HttpResponseRedirect(reverse("comment_list"))
        form = AddBaseModeratorForm(request.POST)
        if not form.is_valid():
            messages.error(request, "Error: Something went wrong. Please try again.")
            return response

        comment_id = form.cleaned_data["comment_id"]

        # Get the comment data
        error, comment = get_comment_and_can_manage_moderators(request, rights, comment_id)
        if error:
            return response

        moderator_id = form.cleaned_data["moderator_id"]
        try:
            moderator = User.objects.filter(comment_moderator__is_moderator=True).get(
                pk=moderator_id
            )
        except User.DoesNotExist:
            messages.error(request, "Error: The chosen moderator does not exist.")
            return response

        error, _ = update_moderation_right(
            comment_id, moderator.pk, rights, PARAM_ACTION_CREATE, request
        )

        response = HttpResponseRedirect(reverse("comment_details", kwargs={"pk": comment_id}))
        if not error:
            email_moderator_assigned(request, moderator, comment)

        return response


class RemoveBaseModeratorView(LoginRequiredMixin, ModeratorCommentRightsMixin, View):
    """
    View for removing 1+ existing moderator(s) from a specific comment.
    Limited to "admin" and "staff" moderators and to non-processed comments.
    Very similar to AddBaseModeratorView.
    """

    def post(self, request: HttpRequest, *args, **kwargs) -> HttpResponseRedirect:
        rights = self.get_rights(request.user)
        if not rights.is_admin_moderator() and not rights.is_staff_moderator():
            raise Http404

        response = HttpResponseRedirect(reverse("comment_list"))

        try:
            comment_id = int(request.POST.get("comment_id", "notParsable"))
        except ValueError:
            messages.error(request, "Error: Something went wrong. Please try again.")
            return response

        # Get the comment data
        error, comment = get_comment_and_can_manage_moderators(request, rights, comment_id)
        if error:
            return response

        existing_moderators = comment.get("moderators_processed")
        if not existing_moderators:
            messages.error(request, "Error: Something went wrong. Please try again.")
            return response

        moderators_choice = tuple([(mod, mod) for mod in existing_moderators.keys()])
        form = RemoveBaseModeratorForm(request.POST, moderators=moderators_choice)

        if not form.is_valid():
            messages.error(request, "Error: Something went wrong. Please try again.")
            return response

        moderators_form = form.cleaned_data["moderators"]
        moderators = User.objects.filter(
            comment_moderator__is_moderator=True, pk__in=moderators_form
        ).values("first_name", "last_name")

        if len(moderators_form) != len(moderators):
            messages.error(request, "Error: One of the chosed moderators does not exist.")
            return response

        error, _ = update_moderation_right(
            comment_id, ",".join(moderators_form), rights, PARAM_ACTION_DELETE, request
        )

        if not error:
            moderators_str = "<br>".join(
                [f"{mod['first_name']} {mod['last_name']}" for mod in moderators]
            )
            messages.success(
                request,
                "The following moderators have been removed from the comment:"
                f"<br>{moderators_str}",
            )

        response = HttpResponseRedirect(reverse("comment_details", kwargs={"pk": comment_id}))

        return response


class InviteStaffModeratorView(LoginRequiredMixin, ModeratorCommentRightsMixin, View):
    """
    View for inviting a new "staff" moderator.
    Different behaviors depending on the provided e-mail:
        -   already used by an user -> If the user is a moderator, adds the collection
            to his/her available collections. Else error message.
        -   already linked to an existing valid invitation -> update the invitation
            extra data.
        -   else -> create a fresh invitation + send e-mail

    Limited to "admin" moderators.
    """

    def post(self, request: HttpRequest, *args, **kwargs) -> HttpResponseRedirect:
        rights = self.get_rights(request.user)
        if not rights.is_admin_moderator():
            raise Http404

        response = HttpResponseRedirect(reverse("comment_moderators"))

        user_collections = rights.get_user_admin_collections()
        user_collections_choice = tuple([(col, col) for col in user_collections])
        form = InviteStaffModeratorForm(user_collections_choice, request.POST)
        if not form.is_valid():
            messages.error(request, "Error: Something went wrong. Please try again.")
            return response

        cleaned_data = form.cleaned_data
        selected_collections = cleaned_data["collections"]
        email = cleaned_data["email"]
        # Check if there's already an user with the provided email
        try:
            moderator = User.objects.get(email__iexact=email)
            if not is_comment_moderator(moderator):
                messages.error(
                    request,
                    "Error: An non-moderator user with the provided"
                    f"email {email} already exists. Please contact us "
                    "for more information.",
                )
                return response

            update_moderator_collections(
                rights, moderator, selected_collections, selected_collections
            )
            messages.success(
                request,
                f"The collections {', '.join(selected_collections)} "
                f"have been added to the moderator {email}",
            )
            return response

        except User.DoesNotExist:
            pass

        inviter_name = f"{request.user.first_name} {request.user.last_name}"  # type:ignore
        cleaned_data[
            "invite_message_txt"
        ] = f"You have been invited to join Trammel by {inviter_name} to moderate comments."
        invite = Invitation.get_invite(email, request, cleaned_data)

        # Add selected collection(s) to the extra_data
        extra_data = InvitationExtraData(**invite.extra_data)
        extra_data.moderator.collections.append(
            InviteCollectionData(pid=selected_collections, user_id=request.user.pk)
        )
        invite.extra_data = extra_data.serialize()
        invite.save()
        messages.success(request, f"An invite has been sent to {email}")

        return response


class InviteBaseModeratorView(LoginRequiredMixin, ModeratorCommentRightsMixin, View):
    """
    View for inviting a new "base" moderator.
    Logic is similar to InviteStaffModeratorView.

    Limited to "admin" and "staff" moderators.
    """

    def post(self, request: HttpRequest, *args, **kwargs) -> HttpResponseRedirect:
        rights = self.get_rights(request.user)
        if not rights.is_admin_moderator() and not rights.is_staff_moderator():
            raise Http404
        redirect_url = request.headers.get("referer")
        redirect_url = redirect_url if redirect_url else reverse("comment_list")
        response = HttpResponseRedirect(redirect_url)

        form = InviteBaseModeratorForm(request.POST)
        if not form.is_valid():
            messages.error(request, "Error: Something went wrong. Please try again.")
            return response

        cleaned_data = form.cleaned_data
        comment_id = cleaned_data["comment_id"]

        # Get the comment data
        error, comment = get_comment_and_can_manage_moderators(request, rights, comment_id)

        if error:
            return response

        email = cleaned_data.pop("email")

        # Check if there's already an user or an invite with the provided email
        try:
            moderator = User.objects.get(email__iexact=email)
            if not is_comment_moderator(moderator):
                messages.error(
                    request,
                    "Error: An non-moderator user with the provided"
                    f"email {email} already exists. Please contact us "
                    "for more information.",
                )
                return response

            error, _ = update_moderation_right(
                comment_id, moderator.pk, rights, PARAM_ACTION_CREATE, request
            )

            if error:
                return response

            email_moderator_assigned(request, moderator, comment)
            return response

        except User.DoesNotExist:
            pass

        inviter_name = f"{request.user.first_name} {request.user.last_name}"  # type:ignore
        cleaned_data[
            "invite_message_txt"
        ] = f"You have been invited to join Trammel by {inviter_name} to moderate comments."
        invite = Invitation.get_invite(email, request, cleaned_data)

        # Add comment to the invitation extra_data
        extra_data = InvitationExtraData(**invite.extra_data)
        extra_data.moderator.comments.append(
            InviteCommentData(
                id=comment_id,
                user_id=request.user.pk,
                pid=comment["site_name"].upper(),
                doi=comment["doi"],
            )
        )
        invite.extra_data = extra_data.serialize()
        invite.save()
        messages.success(request, f"An invite has been sent to {email}")

        return response
