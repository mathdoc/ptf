from itertools import chain

from django.contrib import messages
from django.contrib.auth.models import User
from django.db.models import Q
from django.http import HttpRequest
from django.urls import reverse

from comments_api.constants import PARAM_ACTION
from comments_api.constants import PARAM_ACTION_CREATE
from comments_api.constants import PARAM_COMMENT
from comments_api.constants import PARAM_MODERATOR
from comments_api.constants import STATUS_SUBMITTED
from comments_views.core.rights import AbstractUserRights
from comments_views.core.utils import comments_credentials
from comments_views.core.utils import comments_server_url
from comments_views.core.utils import format_comment
from comments_views.core.utils import get_comment
from comments_views.core.utils import get_user_dict
from comments_views.core.utils import make_api_request
from ptf.model_helpers import get_article_by_doi
from ptf.models import Collection
from ptf.utils import send_email_from_template
from ptf_tools.models import Invitation
from ptf_tools.models import InvitationExtraData

from .rights import ModeratorUserRights


def is_comment_moderator(user: User) -> bool:
    """Whether the user is a comment moderator."""
    return hasattr(user, "comment_moderator") and user.comment_moderator.is_moderator


def update_moderation_right(
    comment_id: int,
    moderators: int | str,
    rights: AbstractUserRights,
    action: str = PARAM_ACTION_CREATE,
    request_for_message: HttpRequest | None = None,
) -> tuple[bool, dict]:
    """
    Makes a POST request to the comment server to create a new moderation entry or
    delete an existing one.
    """
    post_data = {PARAM_COMMENT: comment_id, PARAM_MODERATOR: moderators}
    query_params = rights.comment_rights_query_params()
    query_params[PARAM_ACTION] = action
    return make_api_request(
        "POST",
        comments_server_url(query_params, "moderators"),
        request_for_message=request_for_message,
        auth=comments_credentials(),
        json=post_data,
        timeout=4,
    )


def get_all_moderators(rights: AbstractUserRights, request: HttpRequest):
    """
    Gets all the existing moderators available to the given rights.

    Returns:
        - all_moderators:       The queryset of all available moderators.
        - moderation_rights:    The moderation rights from the comment server.
    """
    query_params = rights.comment_rights_query_params()

    error, moderation_rights = make_api_request(
        "GET",
        comments_server_url(query_params, "moderators"),
        request_for_message=request,
        auth=comments_credentials(),
    )

    base_moderators = []
    if error:
        moderation_rights = []
    else:
        # mod_right struct: {"moderator_id": x, "comment_id": x, "comment__status": x}
        base_moderators = [m["moderator_id"] for m in moderation_rights]

    user_collections = rights.get_user_admin_collections() + rights.get_user_staff_collections()

    # Remove duplicates
    user_collections = set(user_collections)
    base_moderators = set(base_moderators)

    moderator_filter = Q(comment_moderator__is_moderator=True) & (
        Q(comment_moderator__collections__pid__in=user_collections) | Q(pk__in=base_moderators)
    )

    all_moderators = (
        User.objects.prefetch_related("comment_moderator", "comment_moderator__collections")
        .filter(moderator_filter)
        .distinct()
        .order_by("first_name", "pk")
    )

    return all_moderators, moderation_rights


def update_moderator_collections(
    rights: AbstractUserRights,
    moderator: User,
    collections_to_add: list[str] = [],
    collections_to_delete: list[str] = [],
):
    """
    Updates a moderator's attached collections.
    The modified collections are limited to the rights's admin collections.

    Params:
        - rights:                   the rights of the user updating the moderator's
                                    collections
        - moderator:                the moderator
        - collections_to_add:       the PIDs of the collections to add
        - collections_to_delete:    the PIDs of the collections to delete

    """
    if not is_comment_moderator(moderator) or not rights.is_admin_moderator():
        return

    user_collections = rights.get_user_admin_collections()
    collections = Collection.objects.all().values("pk", "pid")
    collections_to_delete = [
        c["pk"]
        for c in collections
        if (c["pid"] in collections_to_delete and c["pid"] in user_collections)
    ]
    if collections_to_delete:
        moderator.comment_moderator.collections.remove(*collections_to_delete)

    collections_to_add = [
        c["pk"]
        for c in collections
        if (c["pid"] in collections_to_add and c["pid"] in user_collections)
    ]
    if collections_to_add:
        moderator.comment_moderator.collections.add(*collections_to_add)


def merge_dict(existing: dict, update: dict):
    """
    Recursively merges 2 dicts by creating potential missing keys.
    When a key is common between the 2 dicts:
        -   If the value is a dict: recursive call to merge_dict
        -   If the value is a list: the 2 lists are concatenated
            TODO: what if list of dict/list ?
        -   If the value is something else: the value of the update overwrites the
            existing one.
    """
    for k, update_v in update.items():
        existing_v = existing.get(k)

        if isinstance(update_v, dict):
            if not existing_v:
                existing_v = {}
            elif existing_v and not isinstance(existing_v, dict):
                raise TypeError("Cannot merge dict and non-dict objects.")
            existing[k] = merge_dict(existing_v, update_v) if existing_v else update_v

        elif isinstance(update_v, list):
            if not existing_v:
                existing_v = []
            elif existing_v and not isinstance(existing_v, list):
                raise TypeError("Cannot merge list and non-list objects.")
            existing[k] = existing_v + update_v

        else:
            if isinstance(existing_v, dict):
                raise TypeError("Cannot merge dict and non-dict objects.")
            if isinstance(existing_v, list):
                raise TypeError("Cannot merge list and non-list objects.")
            existing[k] = update_v

    return existing


def get_comment_and_can_manage_moderators(
    request: HttpRequest, rights: AbstractUserRights, comment_id: int
) -> tuple[bool, dict]:
    """
    GET the requested comment and check the given rights allow moderator management.
    Adds a message to the request if an error occurs of if the rights are insufficient.
    """
    error, comment = get_comment(
        rights.comment_rights_query_params(), comment_id, request_for_message=request
    )
    if error:
        return True, {}

    users = get_user_dict()

    format_comment(comment, rights, users)
    if not rights.comment_can_manage_moderators(comment) or comment["status"] != STATUS_SUBMITTED:
        messages.error(
            request,
            "Error: You don't have enough rights to manage the moderators of "
            "the given comment.",
        )
        return True, {}

    return False, comment


def email_moderator_assigned(request: HttpRequest, moderator: User, comment: dict):
    """
    Send the "Comment moderation request" mail to the provided moderator.
    Add messages to the provided request.
    """
    messages.success(
        request,
        f"The moderator {moderator.first_name} {moderator.last_name} "
        "has been assigned to the comment.",
    )
    # Send an e-mail to the moderator assigned to the comment -
    # TODO: Maybe this should be done few times a day by a cron
    # to avoid too many e-mails?
    article = get_article_by_doi(comment["doi"])
    if not article:
        messages.warning(
            request,
            "The comment's article was not found on Trammel. "
            + "No e-mail was sent to the moderator.",
        )
        return

    article_title = article.title_tex if article else ""
    context_data = {
        "full_name": f"{moderator.first_name} {moderator.last_name}",
        "inviter_name": f"{request.user.first_name} {request.user.last_name}",
        "article_url": comment.get("base_url"),
        "article_title": article_title,
        "comment_dashboard_url": request.build_absolute_uri(reverse("comment_list")),
        "email_signature": "The editorial team",
    }
    subject = f"[{comment['site_name'].upper()}] Comment moderation request"
    send_email_from_template(
        "mail/comment_moderator_assigned.html",
        context_data,
        subject,
        to=[moderator.email],
        from_collection=comment["site_name"],
    )


def get_comments_for_home(user: User) -> tuple[bool, dict]:
    """
    Query the comments server to get the summary of the comments data per collection.

    Returns:
        -   `error`     Whether the HTTP query was successful
        -   `data`      The processed comments summary:
                        `
                        {
                            "COLID": submitted_comments_nb
                        }
                        `

    There is no entry for a collection without any comments.
    """
    rights = ModeratorUserRights(user)
    query_params = rights.comment_rights_query_params()

    error, comments_summary = make_api_request(
        "GET", comments_server_url(query_params, "comments-summary"), auth=comments_credentials()
    )
    if error:
        return True, {}

    data = {}
    for col_id, col_data in comments_summary.items():
        data[col_id] = col_data.get(STATUS_SUBMITTED, 0)

    return False, data


def get_pending_invitations(rights: AbstractUserRights) -> list:
    """
    Returns the list of pending invitations to comment moderators available to the
    given user rights.
    """
    pending_invites = Invitation.objects.filter(accepted=False, extra_data__has_key="moderator")
    invites = []
    for invite in pending_invites:
        collections_to_add = []
        extra_data = InvitationExtraData(**invite.extra_data)

        # Get all collections matching the user_collections
        if rights.user.is_superuser:
            collections_to_add = chain.from_iterable(
                c.pid for c in extra_data.moderator.collections
            )
        elif rights.is_admin_moderator():
            collections = chain.from_iterable(c.pid for c in extra_data.moderator.collections)
            collections_to_add = [
                c for c in collections if c in rights.get_user_admin_collections()
            ]

        # Get all comments whose collection matches the user_collections
        comments_to_add = []
        user_collections = (
            rights.get_user_admin_collections() or rights.get_user_staff_collections()
        )
        for comment in extra_data.moderator.comments:
            if rights.user.is_superuser:
                comments_to_add.append(comment.id)
            elif comment.pid in user_collections:
                comments_to_add.append(comment.id)

        if collections_to_add or comments_to_add:
            invites.append(
                {
                    "name": f"{invite.first_name} {invite.last_name}",
                    "email": invite.email,
                    "sent": invite.sent,
                    "date_expired": invite.date_expired(),
                    "key_expired": invite.key_expired(),
                    "collections": collections_to_add,
                    "comments": comments_to_add,
                }
            )

    return invites


def get_comment_pending_invitations(comment_id: int) -> list:
    """
    Returns the invitations linked to the given comment ID.
    """
    # Not working on CI with SQLite DB backend.
    # return Invitation.objects.filter(
    #     accepted=False,
    #     extra_data__moderator__comments__contains=[{"id": comment_id}]
    # )
    base_invites = Invitation.objects.filter(
        accepted=False, extra_data__moderator__has_key="comments"
    )
    invites = []
    for invite in base_invites:
        extra_data = InvitationExtraData(**invite.extra_data)
        if any(c.id == comment_id for c in extra_data.moderator.comments):
            invites.append(invite)

    return invites
