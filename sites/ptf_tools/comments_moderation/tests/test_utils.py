import responses
from responses.matchers import json_params_matcher
from responses.matchers import query_param_matcher

from django.contrib.auth.models import User
from django.contrib.messages import get_messages
from django.test import RequestFactory
from django.test import TestCase
from django.test import override_settings

from comments_views.core.tests.utils import get_comments_list
from comments_views.core.utils import format_comment
from comments_views.core.utils import get_user_dict
from ptf.tests.mixins import MessageDependentTestMixin
from ptf_tools.models import Invitation
from ptf_tools.models import InvitationExtraData

from ..models import CommentModerator
from ..rights import ModeratorUserRights
from ..utils import get_all_moderators
from ..utils import get_comment_pending_invitations
from ..utils import get_comments_for_home
from ..utils import get_pending_invitations
from ..utils import is_comment_moderator
from ..utils import merge_dict
from ..utils import update_moderation_right
from ..utils import update_moderator_collections
from .mixins import ModeratorTestMixin

COMMENTS_BASE_URL = "http://comments.test"


@override_settings(
    COMMENTS_VIEWS_API_BASE_URL=COMMENTS_BASE_URL,
    COMMENTS_VIEWS_API_CREDENTIALS=("login", "password"),
)
class UtilsTestCase(ModeratorTestMixin, TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        print("\n**App: 'comments_moderation' - Testing `utils.py`**")
        super().setUpClass()

    def setUp(self) -> None:
        self.setUpModeratorData()
        super().setUp()

    def tearDown(self) -> None:
        self.tearDownModeratorData()
        super().tearDown()

    def test_comment_moderator(self):
        user = User.objects.create_user("user_1", "user_1@test.test", "***")
        self.assertFalse(is_comment_moderator(user))

        comment_moderator = CommentModerator.objects.create(user=user)
        self.assertFalse(is_comment_moderator(user))
        comment_moderator.is_moderator = True

        self.assertTrue(is_comment_moderator(user))

    @responses.activate
    def test_update_moderation_right(self):
        query_params_matcher = {
            "moderator_id": f"{self.admin_moderator.pk}",
            "collection_id": f"{self.collection_1.pid},{self.collection_2.pid}",
            "action": "create",
        }
        json_msg = {"message": "OK"}
        response_params = {
            "method": "POST",
            "url": f"{COMMENTS_BASE_URL}/api/moderators/",
            "status": 201,
            "json": json_msg,
            "match": [
                query_param_matcher(query_params_matcher),
                json_params_matcher({"comment_id": 1, "moderator_id": 1}),
            ],
        }
        responses.add(**response_params)

        rights = ModeratorUserRights(self.admin_moderator)

        error, data = update_moderation_right(1, 1, rights)
        self.assertFalse(error)
        self.assertEqual(data, json_msg)

        query_params_matcher["action"] = "delete"
        response_params["match"][0] = query_param_matcher(query_params_matcher)
        responses.add(**response_params)

        error, data = update_moderation_right(1, 1, rights, "delete")
        self.assertFalse(error)
        self.assertEqual(data, json_msg)

    def test_update_moderator_collections(self):
        rights = ModeratorUserRights(self.admin_moderator)

        moderator = self.base_moderator_1
        collections_qs = moderator.comment_moderator.collections

        self.assertEqual(len(collections_qs.all()), 0)

        # Test no update
        update_moderator_collections(rights, moderator)
        self.assertEqual(len(collections_qs.all()), 0)

        # Test adding a non-allowed collection
        update_moderator_collections(rights, moderator, [self.collection_3.pid])
        self.assertEqual(len(collections_qs.all()), 0)

        # Test adding an allowed collection
        update_moderator_collections(rights, moderator, [self.collection_1.pid])
        collections = collections_qs.all()
        self.assertEqual(len(collections), 1)
        self.assertEqual(collections[0].pid, self.collection_1.pid)

        # Test deleting collection
        update_moderator_collections(rights, moderator, [], [self.collection_1.pid])
        self.assertEqual(len(collections_qs.all()), 0)

        # Test adding multiple collections
        update_moderator_collections(
            rights,
            moderator,
            [self.collection_1.pid, self.collection_2.pid, self.collection_3.pid],
        )
        collections = collections_qs.all()
        self.assertEqual(len(collections), 2)
        self.assertEqual(collections[0].pid, self.collection_1.pid)
        self.assertEqual(collections[1].pid, self.collection_2.pid)

        # Test deleting and adding multiple collections
        update_moderator_collections(
            rights,
            moderator,
            [self.collection_2.pid],
            [self.collection_1.pid, self.collection_2.pid, self.collection_3.pid],
        )
        collections = collections_qs.all()
        self.assertEqual(len(collections), 1)
        self.assertEqual(collections[0].pid, self.collection_2.pid)

        # Test updating collections as a staff moderator (not allowed)
        update_moderator_collections(rights, moderator, [], [self.collection_2.pid])
        self.assertEqual(len(collections_qs.all()), 0)

        rights = ModeratorUserRights(self.staff_moderator_1)
        update_moderator_collections(
            rights,
            moderator,
            [self.collection_1.pid, self.collection_2.pid, self.collection_3.pid],
        )
        self.assertEqual(len(collections_qs.all()), 0)

    def test_merge_dict(self):
        existing = {
            "k_1": {"k_1_1": "ev_1_1", "k_1_2": "ev_1_2"},
            "k_2": ["ev_2.1", "ev_2.2"],
            "k_3": "ev_3",
            "k_4": "ev_4",
        }
        update = {
            "k_1": {"k_1_1": "uv_1_1", "k_1_3": ["uv_1_3.1", "uv_1_3.2"]},
            "k_2": ["ev_2.1", "uv_2.1"],
            "k_4": "uv_4",
            "k_5": {"k_5_1": "uv_5_1"},
        }

        expected_result = {
            "k_1": {"k_1_1": "uv_1_1", "k_1_2": "ev_1_2", "k_1_3": ["uv_1_3.1", "uv_1_3.2"]},
            "k_2": ["ev_2.1", "ev_2.2", "ev_2.1", "uv_2.1"],
            "k_3": "ev_3",
            "k_4": "uv_4",
            "k_5": {"k_5_1": "uv_5_1"},
        }
        self.assertEqual(merge_dict(existing, update), expected_result)

        existing = {"k_1": "e_1"}
        update = {"k_1": ["u_1.1"]}
        self.assertRaises(TypeError, merge_dict, existing, update)

        existing = {"k_1": ["e_1.1"]}
        update = {"k_1": "u_1"}
        self.assertRaises(TypeError, merge_dict, existing, update)

        existing = {"k_1": {"k_1_1": "e_1_1"}}
        update = {"k_1": ["u_1.1"]}
        self.assertRaises(TypeError, merge_dict, existing, update)

        existing = {"k_1": {"k_1_1": "e_1_1"}}
        update = {"k_1": "u_1"}
        self.assertRaises(TypeError, merge_dict, existing, update)

        existing = {"k_1": "e_1"}
        update = {"k_1": {"k_1_1": "u_1_1"}}
        self.assertRaises(TypeError, merge_dict, existing, update)

        existing = {"k_1": "e_1"}
        update = {"k_1": "u_1"}
        self.assertEqual(merge_dict(existing, update), {"k_1": "u_1"})

    def test_format_comment(self):
        comment = get_comments_list()[0]
        comment["site_name"] = self.collection_3.pid
        comment["moderation_data"]["moderator"] = self.staff_moderator_1.pk
        comment["moderators"] = [self.staff_moderator_1.pk, self.base_moderator_1.pk]

        rights = ModeratorUserRights(self.admin_moderator)
        users = get_user_dict()

        format_comment(comment, rights, users)
        # No rights over the comment
        self.assertNotIn("moderated_by", comment)
        self.assertNotIn("moderators_processed", comment)

        # Rights over the comment
        comment = get_comments_list()[0]
        comment["site_name"] = self.collection_1.pid
        comment["moderation_data"]["moderator"] = self.staff_moderator_1.pk
        comment["moderators"] = [self.staff_moderator_1.pk, self.base_moderator_1.pk]

        format_comment(comment, rights, users)
        staff_pk = self.staff_moderator_1.pk
        base_pk = self.base_moderator_1.pk
        self.assertEqual(comment["moderation_data"]["moderated_by"], users[staff_pk])
        self.assertEqual(
            comment["moderators_processed"], {staff_pk: users[staff_pk], base_pk: users[base_pk]}
        )

    @responses.activate
    def test_get_comments_for_home(self):
        # Test OK when there's an RequestException
        error, data = get_comments_for_home(self.admin_moderator)
        self.assertTrue(error)

        # Test normal flow
        query_params = {
            "moderator_id": f"{self.admin_moderator.pk}",
            "collection_id": f"{self.collection_1.pid},{self.collection_2.pid}",
        }
        response_params = {
            "method": "GET",
            "url": f"{COMMENTS_BASE_URL}/api/comments-summary/",
            "status": 200,
            "json": {
                self.collection_1.pid.lower(): {"submitted": 1, "validated": 2},
                self.collection_2.pid.lower(): {"validated": 3},
            },
            "match": [query_param_matcher(query_params)],
        }
        responses.add(**response_params)

        error, data = get_comments_for_home(self.admin_moderator)
        self.assertFalse(error)
        expected_data = {self.collection_1.pid.lower(): 1, self.collection_2.pid.lower(): 0}
        self.assertDictEqual(data, expected_data)


class InvitationTestCase(ModeratorTestMixin, TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        print("\n**App: 'comments_moderation' - Testing invitations stuff in `utils.py`**")
        super().setUpClass()

    def setUp(self):
        super().setUpModeratorData()
        request = RequestFactory().get("/")
        request.user = self.admin_moderator
        extra_data = {
            "moderator": {
                "comments": [
                    {
                        "id": 1,
                        "user_id": self.admin_moderator.pk,
                        "pid": self.collection_1.pid,
                        "doi": "My DOI",
                    },
                    {"id": 2, "user_id": 0, "pid": "UNKNOWN PID", "doi": "My other DOI"},
                ],
                "collections": [
                    {"pid": [self.collection_1.pid], "user_id": self.admin_moderator.pk}
                ],
            }
        }
        invite_data = {"first_name": "Test", "last_name": "1"}
        invite = Invitation.get_invite("test_email@test.test", request, invite_data)
        invite.extra_data = InvitationExtraData(**extra_data).serialize()
        invite.save()

        extra_data = {
            "moderator": {
                "comments": [{"id": 2, "user_id": 0, "pid": "UNKNOWN PID", "doi": "My other DOI"}],
                "collections": [{"pid": ["UNKWOWN PID"], "user_id": self.staff_moderator_1.pk}],
            }
        }
        invite_data = {"first_name": "Test", "last_name": "2"}
        invite_2 = Invitation.get_invite("test_email_2@test.test", request, invite_data)
        invite_2.extra_data = InvitationExtraData(**extra_data).serialize()
        invite_2.save()

        invite_data = {"first_name": "Test", "last_name": "3"}
        invite_3 = Invitation.get_invite("test_email_3@test.test", request, invite_data)
        invite_3.save()

        invite_data = {"first_name": "Test", "last_name": "Accepted"}
        invite_4 = Invitation.get_invite("test_email_accepted@test.test", request, invite_data)
        invite_4.accepted = True
        invite_4.save()

    def tearDown(self):
        super().tearDownModeratorData()
        Invitation.objects.all().delete()

    def test_get_pending_invitations(self):
        """
        Test the filtering of pending invitations according to the user collections
        """
        rights = ModeratorUserRights(self.admin_moderator)
        invites = get_pending_invitations(rights)
        self.assertEqual(len(invites), 1)
        invite = invites[0]
        self.assertEqual(invite["email"], "test_email@test.test")
        self.assertListEqual(invite["collections"], [self.collection_1.pid])
        self.assertListEqual(invite["comments"], [1])

        rights = ModeratorUserRights(self.staff_moderator_1)
        invites = get_pending_invitations(rights)
        self.assertEqual(len(invites), 1)
        invite = invites[0]
        self.assertEqual(invite["email"], "test_email@test.test")
        self.assertListEqual(invite["collections"], [])
        self.assertListEqual(invite["comments"], [1])

        rights = ModeratorUserRights(self.staff_moderator_2)
        invites = get_pending_invitations(rights)
        self.assertEqual(len(invites), 0)

    def test_get_comment_pending_invitations(self):
        """
        Test the filtering of pending invitations for a single.
        """
        invites = get_comment_pending_invitations(1)
        self.assertEqual(len(invites), 1)
        self.assertEqual(invites[0].email, "test_email@test.test")

        invites = get_comment_pending_invitations(2)
        self.assertEqual(len(invites), 2)
        self.assertEqual(invites[0].email, "test_email@test.test")
        self.assertEqual(invites[1].email, "test_email_2@test.test")

        invites = get_comment_pending_invitations(0)
        self.assertEqual(len(invites), 0)


@override_settings(
    COMMENTS_VIEWS_API_BASE_URL=COMMENTS_BASE_URL,
    COMMENTS_VIEWS_API_CREDENTIALS=("login", "password"),
)
class UtilsMessageTestCase(MessageDependentTestMixin, ModeratorTestMixin, TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        print("\n**App: 'comments_moderation' - Testing `utils.py` with requests**")
        super().setUpClass()

    def setUp(self) -> None:
        self.setUpMessageData()
        self.setUpModeratorData()
        super().setUp()

    def tearDown(self) -> None:
        self.tearDownModeratorData()
        super().tearDown()

    @responses.activate
    def test_get_all_moderators(self):
        query_params_matcher = {
            "moderator_id": f"{self.admin_moderator.pk}",
            "collection_id": f"{self.collection_1.pid},{self.collection_2.pid}",
        }
        json_data = [
            {
                "moderator_id": self.base_moderator_1.pk,
                "comment_id": 1,
                "comment__status": "validated",
            },
            {
                "moderator_id": self.base_moderator_1.pk,
                "comment_id": 2,
                "comment__status": "submitted",
            },
        ]
        response_params = {
            "method": "GET",
            "url": f"{COMMENTS_BASE_URL}/api/moderators/",
            "status": 200,
            "json": json_data,
            "match": [query_param_matcher(query_params_matcher)],
        }
        responses.add(**response_params)

        rights = ModeratorUserRights(self.admin_moderator)
        request = self.request_factory.get("/")
        self.prepare_request(request)

        all_moderators, moderation_rights = get_all_moderators(rights, request)

        self.assertEqual(len(all_moderators), 3)
        self.assertEqual(all_moderators[0].pk, self.staff_moderator_1.pk)
        self.assertEqual(all_moderators[1].pk, self.staff_moderator_2.pk)
        self.assertEqual(all_moderators[2].pk, self.base_moderator_1.pk)
        self.assertEqual(len(moderation_rights), 2)

        response_params["json"] = []
        responses.add(**response_params)

        all_moderators, moderation_rights = get_all_moderators(rights, request)

        self.assertEqual(len(all_moderators), 2)
        self.assertEqual(all_moderators[0].pk, self.staff_moderator_1.pk)
        self.assertEqual(all_moderators[1].pk, self.staff_moderator_2.pk)
        self.assertEqual(len(moderation_rights), 0)

        # Comment server bad request
        response_params["status"] = 400
        msg = "400 - Bad Request"
        response_params["json"] = {"error_message": msg}
        responses.add(**response_params)

        all_moderators, moderation_rights = get_all_moderators(rights, request)

        self.assertEqual(len(all_moderators), 2)
        self.assertEqual(all_moderators[0].pk, self.staff_moderator_1.pk)
        self.assertEqual(all_moderators[1].pk, self.staff_moderator_2.pk)
        self.assertEqual(len(moderation_rights), 0)

        messages = list(get_messages(request))
        self.assertEqual(len(messages), 1)
        self.assertIn(messages[0].level_tag, ["error", "danger"])

    def test_get_comment_and_can_manage_moderators(self):
        """Already tested via the tests on the views."""

    def test_email_moderator_assigned(self):
        """Already tested via the tests on the views."""
