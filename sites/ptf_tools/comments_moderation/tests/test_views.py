from copy import deepcopy

import responses
from responses.matchers import json_params_matcher
from responses.matchers import query_param_matcher

from django.contrib.auth.models import User
from django.contrib.messages import get_messages
from django.core import mail
from django.test import override_settings

from comments_views.core.tests.utils import get_comments_list
from ptf.factories import ArticleWithSiteFactory
from ptf.factories import ContributionFactory
from ptf.tests.base_test_case import ViewTestCase as BaseViewTestCase
from ptf_tools.models import Invitation
from ptf_tools.models import InvitationExtraData

from .mixins import ModeratorTestMixin

COMMENTS_BASE_URL = "http://comments.test"


@override_settings(
    COMMENTS_VIEWS_API_BASE_URL=COMMENTS_BASE_URL,
    COMMENTS_VIEWS_API_CREDENTIALS=("login", "password"),
)
class ViewTestCase(ModeratorTestMixin, BaseViewTestCase):
    @classmethod
    def setUpClass(cls) -> None:
        print("\n**App: 'comments_moderation' - Testing `views.py`**")
        super().setUpClass()

    def setUp(self) -> None:
        self.setUpModeratorData()
        super().setUp()

    def tearDown(self) -> None:
        self.tearDownModeratorData()
        super().tearDown()

    @responses.activate
    def test_comment_dashboard_list(self):
        test_url = "/comments/"

        # Mock GET request to comments API
        query_params = {
            "dashboard": "true",
            "moderator_id": f"{self.admin_moderator.pk}",
            "collection_id": f"{self.collection_1.pid},{self.collection_2.pid}",
        }
        responses.add(
            method="GET",
            url=f"{COMMENTS_BASE_URL}/api/comments/",
            status=200,
            json=get_comments_list(),
            match=[query_param_matcher(query_params)],
        )

        # Anonymous user should get redirected
        response = self.client.get(test_url)
        self.assertRedirects(
            response, f"/accounts/login/?next={test_url}", fetch_redirect_response=False
        )

        # Authenticated admin moderator
        self.client.force_login(self.admin_moderator)
        response = self.client.get(test_url)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context["display_moderators"])
        self.assertEqual(len(response.context["submitted_comments"]), 1)
        self.assertEqual(len(response.context["processed_comments"]), 2)

    @responses.activate
    def test_comment_dashboard_details(self):
        test_url = "/comments/3/"

        # Mock GET request to comments API
        query_params = {
            "dashboard": "true",
            "moderator_id": f"{self.admin_moderator.pk}",
            "collection_id": f"{self.collection_1.pid},{self.collection_2.pid}",
        }
        comment = get_comments_list()[0]
        comment["site_name"] = self.collection_1.pid
        response_params = {
            "method": "GET",
            "url": f"{COMMENTS_BASE_URL}/api/comments/3/",
            "status": 200,
            "json": comment,
            "match": [query_param_matcher(query_params)],
        }
        responses.add(**response_params)

        # Anonymous user should get redirected
        response = self.client.get(test_url)
        self.assertRedirects(
            response, f"/accounts/login/?next={test_url}", fetch_redirect_response=False
        )

        # Authenticated admin moderator - Processed commment - Can moderate again
        self.client.force_login(self.admin_moderator)

        response = self.client.get(test_url)

        self.assertEqual(response.status_code, 200)
        context = response.context
        self.assertTrue(context["display_moderators"])
        self.assertNotIn("edit", context)
        self.assertNotIn("show_edit", context)
        self.assertNotIn("comment_form", context)
        self.assertNotIn("delete_form", context)
        self.assertIn("moderate_form", context)
        self.assertTrue(
            all([f.field.disabled is False for f in context["moderate_form"].visible_fields()])
        )

        # Comment is processed so the following variables should not be set
        self.assertNotIn("form_moderators", context)
        self.assertNotIn("all_moderators", context)
        self.assertNotIn("moderator_add_form", context)
        self.assertNotIn("moderator_invite_form", context)
        self.assertNotIn("moderator_remove_form", context)

        # Authenticated admin moderator - Submitted comment
        response_params["json"]["status"] = "submitted"
        response_params["json"]["moderators"] = [-1]
        responses.add(**response_params)

        moderator_json = [
            {
                "moderator_id": self.base_moderator_1.pk,
                "comment_id": 1,
                "comment__status": "validated",
            },
            {
                "moderator_id": self.base_moderator_1.pk,
                "comment_id": 2,
                "comment__status": "submitted",
            },
        ]
        query_params_bis = {**query_params}
        query_params_bis.pop("dashboard")
        responses.add(
            method="GET",
            url=f"{COMMENTS_BASE_URL}/api/moderators/",
            status=200,
            json=moderator_json,
            match=[query_param_matcher(query_params_bis)],
        )

        response = self.client.get(test_url)

        self.assertEqual(response.status_code, 200)
        context = response.context
        self.assertTrue(context["display_moderators"])
        self.assertNotIn("edit", context)
        self.assertNotIn("show_edit", context)
        self.assertNotIn("comment_form", context)
        self.assertNotIn("delete_form", context)
        self.assertIn("moderate_form", context)
        self.assertTrue(
            all([f.field.disabled is False for f in context["moderate_form"].visible_fields()])
        )

        # Comment is not processed so the following variables should be set
        self.assertIn("form_moderators", context)
        self.assertIn("all_moderators", context)
        self.assertIn("moderator_add_form", context)
        self.assertIn("moderator_invite_form", context)
        # Except this one
        self.assertNotIn("moderator_remove_form", context)

        # Check the remove form is present
        response_params["json"]["moderators"] = [self.base_moderator_1.pk]
        responses.add(**response_params)

        response = self.client.get(test_url)

        self.assertIn("moderator_remove_form", response.context)

    @responses.activate
    def test_post_comment_status(self):
        test_url = "/comments/3/status-change/light/"

        mail.outbox = []
        article = ArticleWithSiteFactory(title_tex="My article")
        contrib_1 = ContributionFactory(
            resource=article, email="author_1@test.test", corresponding=True
        )
        contrib_2 = ContributionFactory(
            resource=article, email="author_2@test.test", corresponding=True
        )
        # Mock GET request to comments API
        query_params = {
            "dashboard": "true",
            "moderator_id": f"{self.admin_moderator.pk}",
            "collection_id": f"{self.collection_1.pid},{self.collection_2.pid}",
        }
        comment_initial = get_comments_list()[2]
        comment_initial["site_name"] = self.collection_1.pid
        comment_initial["doi"] = article.doi
        comment_initial["status"] = "submitted"
        comment_initial["validation_sent_mail"] = False
        response_params = {
            "method": "GET",
            "url": f"{COMMENTS_BASE_URL}/api/comments/3/",
            "status": 200,
            "json": comment_initial,
            "match": [query_param_matcher(query_params)],
        }
        responses.add(**response_params)

        # Mock PUT request to comments API
        comment_resp = deepcopy(comment_initial)
        comment_resp["status"] = "validated"
        response_params_put = {**response_params}
        response_params_put["method"] = "PUT"
        response_params_put["json"] = comment_resp
        responses.add(**response_params_put)

        post_data = {"status": "validated"}

        # Anonymous user should get redirected
        response = self.client.post(test_url, post_data)
        self.assertRedirects(
            response, f"/accounts/login/?next={test_url}", fetch_redirect_response=False
        )

        # Authenticated admin moderator - First validation of comment => send e-mails
        self.client.force_login(self.admin_moderator)

        response = self.client.post(test_url, post_data)
        self.assertRedirects(response, "/comments/", fetch_redirect_response=False)
        self.assertUniqueSuccessMessage(response)
        self.assertEqual(len(mail.outbox), 3)
        self.assertEqual(mail.outbox[0].to, [comment_initial["author_email"]])
        self.assertEqual(mail.outbox[1].to, ["author_1@test.test"])
        self.assertEqual(mail.outbox[2].to, ["author_2@test.test"])

        # Reject already moderated comment => e-mail only to comment author
        mail.outbox = []
        comment_initial["status"] = "validated"
        response_params["json"] = comment_initial
        responses.add(**response_params)

        comment_resp["status"] = "rejected"
        response_params_put["json"] = comment_resp
        responses.add(**response_params_put)

        post_data = {"status": "rejected"}
        response = self.client.post(test_url, post_data)
        self.assertRedirects(response, "/comments/", fetch_redirect_response=False)
        self.assertUniqueSuccessMessage(response)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].to, [comment_initial["author_email"]])

        # Re-reject currently rejected comment => no e-mail
        mail.outbox = []
        comment_initial["status"] = "rejected"
        response_params["json"] = comment_initial
        responses.add(**response_params)

        comment_resp["status"] = "rejected"
        response_params_put["json"] = comment_resp
        responses.add(**response_params_put)

        post_data = {"status": "rejected"}
        response = self.client.post(test_url, post_data)
        self.assertUniqueSuccessMessage(response)
        self.assertRedirects(response, "/comments/", fetch_redirect_response=False)
        self.assertEqual(len(mail.outbox), 0)

        # Re-validate already validated comment (currently rejected)
        # => e-mail only to comment author
        mail.outbox = []
        comment_initial["status"] = "rejected"
        comment_initial["validation_email_sent"] = True
        response_params["json"] = comment_initial
        responses.add(**response_params)

        comment_resp["status"] = "validated"
        response_params_put["json"] = comment_resp
        responses.add(**response_params_put)

        post_data = {"status": "validated"}
        response = self.client.post(test_url, post_data)
        self.assertUniqueSuccessMessage(response)
        self.assertRedirects(response, "/comments/", fetch_redirect_response=False)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].to, [comment_initial["author_email"]])

        # Re-validate currently validated comment => no e-mail
        mail.outbox = []
        comment_initial["status"] = "rejected"
        comment_initial["validation_email_sent"] = True
        response_params["json"] = comment_initial
        responses.add(**response_params)

        comment_resp["status"] = "rejected"
        response_params_put["json"] = comment_resp
        responses.add(**response_params_put)

        post_data = {"status": "rejected"}
        response = self.client.post(test_url, post_data)
        self.assertUniqueSuccessMessage(response)
        self.assertRedirects(response, "/comments/", fetch_redirect_response=False)
        self.assertEqual(len(mail.outbox), 0)

    @responses.activate
    def test_post_comment_status_detailed(self):
        """
        Same test as the previous one, this time using "detailed" endpoint, not the
        "light" one. More data should be sent in the PUT request.
        """
        test_url = "/comments/3/status-change/"
        article = ArticleWithSiteFactory(title_tex="My article")

        # Mock GET request to comments API
        query_params = {
            "dashboard": "true",
            "moderator_id": f"{self.admin_moderator.pk}",
            "collection_id": f"{self.collection_1.pid},{self.collection_2.pid}",
        }
        comment_initial = get_comments_list()[2]
        comment_initial["site_name"] = self.collection_1.pid
        comment_initial["doi"] = article.doi
        comment_initial["status"] = "submitted"
        response_params = {
            "method": "GET",
            "url": f"{COMMENTS_BASE_URL}/api/comments/3/",
            "status": 200,
            "json": comment_initial,
            "match": [query_param_matcher(query_params)],
        }
        responses.add(**response_params)

        # Mock PUT request to comments API
        comment_resp = deepcopy(comment_initial)
        comment_resp["status"] = "validated"
        response_params_put = {**response_params}
        response_params_put["method"] = "PUT"
        response_params_put["json"] = comment_resp
        response_params_put["match"] = [
            query_param_matcher(query_params),
            json_params_matcher(
                {
                    "status": "validated",
                    "article_author_comment": True,
                    "editorial_team_comment": False,
                    "hide_author_name": False,
                }
            ),
        ]
        responses.add(**response_params_put)

        post_data = {
            "status": "validated",
            "article_author_comment": True,
            "editorial_team_comment": False,
            "hide_author_name": False,
        }
        self.client.force_login(self.admin_moderator)
        response = self.client.post(test_url, post_data)
        self.assertRedirects(response, "/comments/", fetch_redirect_response=False)
        self.assertUniqueSuccessMessage(response)

        response_params_put["match"] = [
            query_param_matcher(query_params),
            json_params_matcher(
                {
                    "status": "validated",
                    "article_author_comment": False,
                    "editorial_team_comment": True,
                    "hide_author_name": True,
                }
            ),
        ]
        responses.add(**response_params_put)

        post_data = {
            "status": "validated",
            "article_author_comment": False,
            "editorial_team_comment": True,
            "hide_author_name": True,
        }
        response = self.client.post(test_url, post_data)
        self.assertRedirects(response, "/comments/", fetch_redirect_response=False)
        self.assertUniqueSuccessMessage(response)

        # Check handling of incorrect form data
        post_data["editorial_team_comment"] = False
        response = self.client.post(test_url, post_data)
        self.assertRedirects(response, "/comments/", fetch_redirect_response=False)
        self.assertUniqueErrorMessage(response)

        # Finally check that the light endpoint doesn't PUT data other than "status"
        # with the json matcher
        test_url = "/comments/3/status-change/light/"
        response_params_put["match"] = [
            query_param_matcher(query_params),
            json_params_matcher(
                {
                    "status": "validated",
                }
            ),
        ]
        responses.add(**response_params_put)

        post_data = {
            "status": "validated",
            "article_author_comment": False,
            "editorial_team_comment": True,
        }
        response = self.client.post(test_url, post_data)
        self.assertRedirects(response, "/comments/", fetch_redirect_response=False)
        self.assertUniqueSuccessMessage(response)

    @override_settings(COMMENTS_VIEWS_EMAIL_AUTHOR=False)
    @responses.activate
    def test_setting_email_author(self):
        """
        Test setting EMAIL_AUTHOR. No e-mail should be sent to the article's author
        if `EMAIL_AUTHOR = False`.
        """
        test_url = "/comments/3/status-change/"

        mail.outbox = []
        article = ArticleWithSiteFactory(title_tex="My article")
        contrib_1 = ContributionFactory(
            resource=article, email="author_1@test.test", corresponding=True
        )
        contrib_2 = ContributionFactory(
            resource=article, email="author_2@test.test", corresponding=True
        )

        # Mock GET request to comments API
        query_params = {
            "dashboard": "true",
            "moderator_id": f"{self.admin_moderator.pk}",
            "collection_id": f"{self.collection_1.pid},{self.collection_2.pid}",
        }
        comment_initial = get_comments_list()[2]
        comment_initial["site_name"] = self.collection_1.pid
        comment_initial["doi"] = article.doi
        comment_initial["status"] = "submitted"
        comment_initial["validation_sent_mail"] = False
        response_params = {
            "method": "GET",
            "url": f"{COMMENTS_BASE_URL}/api/comments/3/",
            "status": 200,
            "json": comment_initial,
            "match": [query_param_matcher(query_params)],
        }
        responses.add(**response_params)

        # Mock PUT request to comments API
        comment_resp = deepcopy(comment_initial)
        comment_resp["status"] = "validated"
        response_params_put = {**response_params}
        response_params_put["method"] = "PUT"
        response_params_put["json"] = comment_resp
        responses.add(**response_params_put)

        post_data = {"status": "validated"}

        self.client.force_login(self.admin_moderator)
        response = self.client.post(test_url, post_data)

        # No e-mail sent to the authors with EMAIL_AUTHOR = False
        self.assertRedirects(response, "/comments/", fetch_redirect_response=False)
        self.assertUniqueSuccessMessage(response)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].to, [comment_initial["author_email"]])

    @responses.activate
    def test_comment_admin_moderator(self):
        test_url = "/comments/moderators/"

        # Mock GET request to comments API
        query_params = {
            "moderator_id": f"{self.admin_moderator.pk}",
            "collection_id": f"{self.collection_1.pid},{self.collection_2.pid}",
        }
        moderator_json = [
            {
                "moderator_id": self.base_moderator_1.pk,
                "comment_id": 1,
                "comment__status": "validated",
            },
            {
                "moderator_id": self.base_moderator_1.pk,
                "comment_id": 2,
                "comment__status": "submitted",
            },
            {
                "moderator_id": self.base_moderator_1.pk,
                "comment_id": 3,
                "comment__status": "submitted",
            },
        ]
        response_params = {
            "method": "GET",
            "url": f"{COMMENTS_BASE_URL}/api/moderators/",
            "status": 200,
            "json": moderator_json,
            "match": [query_param_matcher(query_params)],
        }
        responses.add(**response_params)

        # Anonymous user should get redirected
        response = self.client.get(test_url)
        self.assertRedirects(
            response, f"/accounts/login/?next={test_url}", fetch_redirect_response=False
        )

        # Authenticated admin moderator
        self.client.force_login(self.admin_moderator)

        response = self.client.get(test_url)
        self.assertEqual(response.status_code, 200)

        context = response.context
        self.assertEqual(
            context["user_collections"], [self.collection_1.pid, self.collection_2.pid]
        )

        # all_moderators = 2 staff moderators + the base moderator in moderator_json
        self.assertEqual(len(context["all_moderators"]), 3)
        self.assertEqual(len(context["staff_moderators"]), 2)
        self.assertEqual(len(context["base_moderators"]), 1)
        base_mod = list(context["base_moderators"])[0]
        self.assertEqual(base_mod["pk"], self.base_moderator_1.pk)
        self.assertEqual(len(base_mod["comments_processed"]), 1)
        self.assertEqual(len(base_mod["comments_pending"]), 2)

        self.assertIn("moderator_add_form", context)
        self.assertIn("moderator_invite_form", context)

        # Authenticated staff moderator
        query_params["moderator_id"] = f"{self.staff_moderator_1.pk}"
        query_params["collection_id"] = self.collection_1.pid
        response_params["match"] = [query_param_matcher(query_params)]
        responses.add(**response_params)

        self.client.force_login(self.staff_moderator_1)

        response = self.client.get(test_url)

        context = response.context
        self.assertEqual(len(context["all_moderators"]), 2)
        self.assertNotIn("staff_moderators", context)
        base_mod = list(context["base_moderators"])[0]
        self.assertEqual(base_mod["pk"], self.base_moderator_1.pk)
        self.assertEqual(len(base_mod["comments_processed"]), 1)
        self.assertEqual(len(base_mod["comments_pending"]), 2)

    def test_post_comment_admin_moderator(self):
        post_data = {
            "moderator_id": self.staff_moderator_1.pk,
            "collections": [self.collection_1.pid, self.collection_2.pid],
        }

        test_url = "/comments/moderators/"

        # Anonymous user - Redirected
        response = self.client.post(test_url, post_data)
        self.assertRedirects(
            response, f"/accounts/login/?next={test_url}", fetch_redirect_response=False
        )

        # Staff moderator - Unauthorized
        self.client.force_login(self.staff_moderator_1)
        response = self.client.post(test_url, post_data)
        self.assertEqual(response.status_code, 404)

        # Admin moderator
        self.client.force_login(self.admin_moderator)

        collections_qs = self.staff_moderator_1.comment_moderator.collections
        self.assertEqual(len(collections_qs.all()), 1)

        response = self.client.post(test_url, post_data)

        self.assertRedirects(response, test_url, fetch_redirect_response=False)
        self.assertUniqueSuccessMessage(response)

        collections = collections_qs.all()
        self.assertEqual(len(collections), 2)
        self.assertIn(collections[0].pid, self.collection_1.pid)
        self.assertIn(collections[1].pid, self.collection_2.pid)

        # Delete all collections
        del post_data["collections"]

        response = self.client.post(test_url, post_data)

        self.assertRedirects(response, test_url, fetch_redirect_response=False)
        self.assertUniqueSuccessMessage(response)

        collections = collections_qs.all()
        self.assertEqual(len(collections), 0)

        # Try updating non-allowed collections
        post_data["collections"] = self.collection_3.pid

        response = self.client.post(test_url, post_data)

        self.assertRedirects(response, test_url, fetch_redirect_response=False)
        self.assertUniqueErrorMessage(response)

        collections = collections_qs.all()
        self.assertEqual(len(collections), 0)

        # Try updating non-existing moderator
        post_data["collections"] = self.collection_1.pid
        post_data["moderator_id"] = self.admin_moderator.pk

        response = self.client.post(test_url, post_data)

        self.assertRedirects(response, test_url, fetch_redirect_response=False)
        self.assertUniqueErrorMessage(response)

    @responses.activate
    def test_add_base_moderator(self):
        article = ArticleWithSiteFactory()
        test_url = "/comments/moderators/add/"

        # Mock GET request to comments API
        comment_query_params = {
            "moderator_id": f"{self.admin_moderator.pk}",
            "collection_id": f"{self.collection_1.pid},{self.collection_2.pid}",
        }
        comment = get_comments_list()[0]
        comment["doi"] = article.doi
        comment["status"] = "validated"
        comment["moderators"] = []
        comment["site_name"] = self.collection_1.pid.lower()
        comment_id = comment["id"]
        comment_response_params = {
            "method": "GET",
            "url": f"{COMMENTS_BASE_URL}/api/comments/{comment_id}/",
            "status": 200,
            "json": comment,
            "match": [query_param_matcher(comment_query_params)],
        }
        responses.add(**comment_response_params)

        post_data = {"comment_id": comment_id, "moderator_id": self.base_moderator_1.pk}
        ## Anonymous user - Redirected
        response = self.client.post(test_url, post_data)
        self.assertRedirects(
            response, f"/accounts/login/?next={test_url}", fetch_redirect_response=False
        )

        ## Non-admin & non-staff moderator user
        self.client.force_login(self.base_moderator_1)
        response = self.client.post(test_url, post_data)
        self.assertEqual(response.status_code, 404)

        ## Admin moderator
        # Already processed comment
        self.client.force_login(self.admin_moderator)

        response = self.client.post(test_url, post_data)
        self.assertUniqueErrorMessage(response)

        # No rights over the comment's collection
        comment["status"] = "submitted"
        comment["site_name"] = self.collection_3.pid.lower()
        responses.add(**comment_response_params)

        response = self.client.post(test_url, post_data)
        self.assertUniqueErrorMessage(response)

        # Non-existing moderator
        comment["status"] = "submitted"
        comment["site_name"] = self.collection_1.pid.lower()
        responses.add(**comment_response_params)

        post_data["moderator_id"] = -1
        response = self.client.post(test_url, post_data)
        self.assertUniqueErrorMessage(response)

        # Comment server error
        comment_response_params["status"] = 400
        responses.add(**comment_response_params)

        response = self.client.post(test_url, post_data)
        self.assertUniqueErrorMessage(response)

        # Normal flow
        comment["site_name"] = self.collection_1.pid.lower()
        comment_response_params["status"] = 200
        responses.add(**comment_response_params)

        # Mock POST request to comments API
        mod_query_params = {
            "moderator_id": f"{self.admin_moderator.pk}",
            "collection_id": f"{self.collection_1.pid},{self.collection_2.pid}",
            "action": "create",
        }
        mod_post_data = {"comment_id": comment_id, "moderator_id": self.base_moderator_1.pk}
        mod_response_params = {
            "method": "POST",
            "url": f"{COMMENTS_BASE_URL}/api/moderators/",
            "status": 201,
            "json": {},
            "match": [query_param_matcher(mod_query_params), json_params_matcher(mod_post_data)],
        }
        responses.add(**mod_response_params)
        mail.outbox = []

        post_data["moderator_id"] = self.base_moderator_1.pk
        response = self.client.post(test_url, post_data)

        self.assertRedirects(response, f"/comments/{comment_id}/", fetch_redirect_response=False)
        self.assertUniqueSuccessMessage(response)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].to, [self.base_moderator_1.email])

        mail.outbox = []

        ## Staff moderator
        # Normal flow
        comment_query_params = {
            "moderator_id": f"{self.staff_moderator_1.pk}",
            "collection_id": self.collection_1.pid,
        }
        comment_response_params["match"] = [query_param_matcher(comment_query_params)]
        responses.add(**comment_response_params)

        mod_query_params = {
            "moderator_id": f"{self.staff_moderator_1.pk}",
            "collection_id": self.collection_1.pid,
            "action": "create",
        }
        mod_response_params["match"] = [query_param_matcher(mod_query_params)]
        responses.add(**mod_response_params)

        self.client.force_login(self.staff_moderator_1)
        response = self.client.post(test_url, post_data)

        self.assertRedirects(response, f"/comments/{comment_id}/", fetch_redirect_response=False)
        self.assertUniqueSuccessMessage(response)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].to, [self.base_moderator_1.email])

        mail.outbox = []

        # Article is not in Trammel
        comment["doi"] = "unknown_doi"
        responses.add(**comment_response_params)

        response = self.client.post(test_url, post_data)

        self.assertRedirects(response, f"/comments/{comment_id}/", fetch_redirect_response=False)
        messages = list(get_messages(response.wsgi_request))
        self.assertEqual(len(messages), 2)
        self.assertEqual(messages[0].level_tag, "success")
        self.assertEqual(messages[1].level_tag, "warning")
        self.assertEqual(len(mail.outbox), 0)
        self.clear_messages()

        # Comment server error
        mod_response_params["status"] = 400
        responses.add(**mod_response_params)

        response = self.client.post(test_url, post_data)

        self.assertRedirects(response, f"/comments/{comment_id}/", fetch_redirect_response=False)
        self.assertUniqueErrorMessage(response)

    @responses.activate
    def test_remove_base_moderator(self):
        test_url = "/comments/moderators/remove/"

        # Mock GET request to comments API
        comment_query_params = {
            "moderator_id": f"{self.admin_moderator.pk}",
            "collection_id": f"{self.collection_1.pid},{self.collection_2.pid}",
        }
        comment = get_comments_list()[0]
        comment["status"] = "submitted"
        comment["site_name"] = self.collection_1.pid.lower()
        comment["moderators"] = [self.base_moderator_1.pk]
        comment_id = comment["id"]
        comment_response_params = {
            "method": "GET",
            "url": f"{COMMENTS_BASE_URL}/api/comments/{comment_id}/",
            "status": 200,
            "json": comment,
            "match": [query_param_matcher(comment_query_params)],
        }

        post_data = {"moderators": self.base_moderator_1.pk}

        ## Anonymous user
        response = self.client.post(test_url, post_data)
        self.assertRedirects(
            response, f"/accounts/login/?next={test_url}", fetch_redirect_response=False
        )

        ## Regular user
        self.client.force_login(self.base_moderator_1)
        response = self.client.post(test_url, post_data)
        self.assertEqual(response.status_code, 404)

        ## Admin moderator
        # Invalid POST data
        self.client.force_login(self.admin_moderator)
        response = self.client.post(test_url, post_data)

        self.assertRedirects(response, "/comments/", fetch_redirect_response=False)
        self.assertUniqueErrorMessage(response)

        # Moderator not in existing moderators
        comment["moderators"] = []
        responses.add(**comment_response_params)

        post_data["comment_id"] = comment_id

        response = self.client.post(test_url, post_data)

        self.assertRedirects(response, "/comments/", fetch_redirect_response=False)
        self.assertUniqueErrorMessage(response)

        # Comment server error
        comment["moderators"] = [self.base_moderator_1.pk]
        comment_response_params["status"] = 400
        responses.add(**comment_response_params)

        response = self.client.post(test_url, post_data)

        self.assertRedirects(response, "/comments/", fetch_redirect_response=False)
        self.assertUniqueErrorMessage(response)

        # Normal flow
        comment_response_params["status"] = 200
        responses.add(**comment_response_params)

        # Mock POST request to comments API
        mod_query_params = {
            "moderator_id": f"{self.admin_moderator.pk}",
            "collection_id": f"{self.collection_1.pid},{self.collection_2.pid}",
            "action": "delete",
        }
        mod_json_data = {"comment_id": comment_id, "moderator_id": str(self.base_moderator_1.pk)}
        mod_response_params = {
            "method": "POST",
            "url": f"{COMMENTS_BASE_URL}/api/moderators/",
            "status": 201,
            "json": {},
            "match": [query_param_matcher(mod_query_params), json_params_matcher(mod_json_data)],
        }
        responses.add(**mod_response_params)

        response = self.client.post(test_url, post_data)

        self.assertRedirects(response, f"/comments/{comment_id}/", fetch_redirect_response=False)
        self.assertUniqueSuccessMessage(response)

        # Multiple moderator deletions
        comment["moderators"] = [self.base_moderator_1.pk, self.base_moderator_2.pk]
        responses.add(**comment_response_params)

        mod_json_data["moderator_id"] = f"{self.base_moderator_1.pk},{self.base_moderator_2.pk}"
        responses.add(**mod_response_params)

        post_data["moderators"] = comment["moderators"]
        response = self.client.post(test_url, post_data)

        self.assertRedirects(response, f"/comments/{comment_id}/", fetch_redirect_response=False)
        self.assertUniqueSuccessMessage(response)

    def test_invite_staff_moderator(self):
        test_url = "/comments/moderators/invite/staff/"

        post_data = {
            "collections": self.collection_1.pid,
            "first_name": "Staff",
            "last_name": "Moderator 3",
            "email": "staff_moderator_3@test.test",
        }

        ## Anonymous user
        response = self.client.post(test_url, post_data)
        self.assertRedirects(
            response, f"/accounts/login/?next={test_url}", fetch_redirect_response=False
        )

        ## Base moderator
        self.client.force_login(self.base_moderator_1)
        response = self.client.post(test_url, post_data)
        self.assertEqual(response.status_code, 404)

        ## Staff moderator
        self.client.force_login(self.staff_moderator_1)
        response = self.client.post(test_url, post_data)
        self.assertEqual(response.status_code, 404)

        ## Admin moderator
        self.client.force_login(self.admin_moderator)
        mail.outbox = []
        response = self.client.post(test_url, post_data)

        self.assertRedirects(response, "/comments/moderators/", fetch_redirect_response=False)
        self.assertUniqueSuccessMessage(response)
        invite = Invitation.objects.get(email=post_data["email"])
        extra_data = {
            "moderator": {
                "collections": [
                    {"pid": [self.collection_1.pid], "user_id": self.admin_moderator.pk}
                ]
            }
        }
        self.assertDictEqual(invite.extra_data, InvitationExtraData(**extra_data).serialize())

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].to, [post_data["email"]])

        # Test sending invite to already invited user
        post_data["collections"] = self.collection_2.pid
        mail.outbox = []
        response = self.client.post(test_url, post_data)

        self.assertUniqueSuccessMessage(response)
        invite = Invitation.objects.get(email=post_data["email"])

        extra_data = {
            "moderator": {
                "collections": [
                    {"pid": [self.collection_1.pid], "user_id": self.admin_moderator.pk},
                    {"pid": [self.collection_2.pid], "user_id": self.admin_moderator.pk},
                ]
            }
        }
        self.assertDictEqual(invite.extra_data, InvitationExtraData(**extra_data).serialize())
        # No e-mail was sent
        self.assertEqual(len(mail.outbox), 0)

        # Test sending invite to existing moderator user
        mail.outbox = []
        collections_qs = self.base_moderator_1.comment_moderator.collections
        self.assertEqual(len(collections_qs.all()), 0)
        post_data["email"] = self.base_moderator_1.email

        response = self.client.post(test_url, post_data)

        self.assertUniqueSuccessMessage(response)
        self.assertEqual(len(collections_qs.all()), 1)
        self.assertRaises(
            Invitation.DoesNotExist, Invitation.objects.get, email=post_data["email"]
        )
        # No e-mail was sent
        self.assertEqual(len(mail.outbox), 0)

        # Test sending invite to existing non-moderator User
        user = User.objects.create_user("user_test", "user_test@test.test", "***")

        post_data["email"] = "user_test@test.test"

        response = self.client.post(test_url, post_data)

        self.assertUniqueErrorMessage(response)
        self.assertRaises(
            Invitation.DoesNotExist, Invitation.objects.get, email=post_data["email"]
        )
        # No e-mail was sent
        self.assertEqual(len(mail.outbox), 0)

    @responses.activate
    def test_invite_base_moderator(self):
        test_url = "/comments/moderators/invite/base/"

        # Mock GET request to comments API
        comment_query_params = {
            "moderator_id": f"{self.staff_moderator_1.pk}",
            "collection_id": self.collection_1.pid,
        }
        comment = get_comments_list()[0]
        comment["status"] = "submitted"
        comment["site_name"] = self.collection_1.pid.lower()
        comment["moderators"] = [self.base_moderator_1.pk]
        comment_id = comment["id"]
        comment_response_params = {
            "method": "GET",
            "url": f"{COMMENTS_BASE_URL}/api/comments/{comment_id}/",
            "status": 200,
            "json": comment,
            "match": [query_param_matcher(comment_query_params)],
        }

        responses.add(**comment_response_params)
        post_data = {
            "comment_id": comment_id,
            "first_name": "Base",
            "last_name": "Moderator 3",
            "email": "base_moderator_3@test.test",
        }

        ## Anonymous user
        response = self.client.post(test_url, post_data)
        self.assertRedirects(
            response, f"/accounts/login/?next={test_url}", fetch_redirect_response=False
        )

        ## Regular moderator
        self.client.force_login(self.base_moderator_1)
        response = self.client.post(test_url, post_data)
        self.assertEqual(response.status_code, 404)

        ## Staff moderator
        self.client.force_login(self.staff_moderator_1)
        response = self.client.post(test_url, post_data)

        self.assertRedirects(response, "/comments/", fetch_redirect_response=False)
        self.assertUniqueSuccessMessage(response)

        invite = Invitation.objects.get(email=post_data["email"])
        extra_data = {
            "moderator": {
                "comments": [
                    {
                        "id": comment_id,
                        "doi": comment["doi"],
                        "pid": comment["site_name"].upper(),
                        "user_id": self.staff_moderator_1.pk,
                    }
                ],
            }
        }
        self.assertDictEqual(invite.extra_data, InvitationExtraData(**extra_data).serialize())

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].to, [post_data["email"]])

        mail.outbox = []

        # Test sending an invite to already invited user
        comment_id_bis = 10
        comment["id"] = comment_id_bis
        comment_response_params["url"] = f"{COMMENTS_BASE_URL}/api/comments/{comment_id_bis}/"
        responses.add(**comment_response_params)

        post_data["comment_id"] = comment_id_bis
        response = self.client.post(test_url, post_data)

        self.assertRedirects(response, "/comments/", fetch_redirect_response=False)
        self.assertUniqueSuccessMessage(response)

        invite = Invitation.objects.get(email=post_data["email"])
        extra_data = {
            "moderator": {
                "comments": [
                    {
                        "id": comment_id,
                        "doi": comment["doi"],
                        "pid": comment["site_name"].upper(),
                        "user_id": self.staff_moderator_1.pk,
                    },
                    {
                        "id": comment_id_bis,
                        "doi": comment["doi"],
                        "pid": comment["site_name"].upper(),
                        "user_id": self.staff_moderator_1.pk,
                    },
                ]
            }
        }
        self.assertDictEqual(invite.extra_data, InvitationExtraData(**extra_data).serialize())
        # No e-mail was sent
        self.assertEqual(len(mail.outbox), 0)

        # Test sending an invite to existing moderator user
        article = ArticleWithSiteFactory()
        comment["doi"] = article.doi
        responses.add(**comment_response_params)

        # Mock POST request to comments API
        mod_query_params = {
            "moderator_id": f"{self.staff_moderator_1.pk}",
            "collection_id": self.collection_1.pid,
            "action": "create",
        }
        mod_json_data = {"comment_id": comment_id_bis, "moderator_id": self.base_moderator_1.pk}
        mod_response_params = {
            "method": "POST",
            "url": f"{COMMENTS_BASE_URL}/api/moderators/",
            "status": 201,
            "json": {},
            "match": [query_param_matcher(mod_query_params), json_params_matcher(mod_json_data)],
        }
        responses.add(**mod_response_params)

        post_data["email"] = self.base_moderator_1.email

        response = self.client.post(test_url, post_data)

        self.assertRedirects(response, "/comments/", fetch_redirect_response=False)
        self.assertUniqueSuccessMessage(response)

        self.assertRaises(
            Invitation.DoesNotExist, Invitation.objects.get, email=post_data["email"]
        )
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].to, [self.base_moderator_1.email])

        mail.outbox = []

        # Test sending an invite to existing non-moderator user
        user = User.objects.create_user("user_test", "user_test@test.test", "***")

        post_data["email"] = "user_test@test.test"

        response = self.client.post(test_url, post_data)

        self.assertUniqueErrorMessage(response)
        self.assertRaises(
            Invitation.DoesNotExist, Invitation.objects.get, email=post_data["email"]
        )
        # No e-mail was sent
        self.assertEqual(len(mail.outbox), 0)

        ## Admin moderator - Make sure it works for admin
        self.client.force_login(self.admin_moderator)
        post_data["email"] = "another_test@test.test"
        post_data["comment_id"] = comment_id
        comment["id"] = comment_id

        comment_query_params = {
            "moderator_id": f"{self.admin_moderator.pk}",
            "collection_id": f"{self.collection_1.pid},{self.collection_2.pid}",
        }
        comment_response_params["match"] = [query_param_matcher(comment_query_params)]
        comment_response_params["url"] = f"{COMMENTS_BASE_URL}/api/comments/{comment_id}/"
        responses.add(**comment_response_params)

        mail.outbox = []

        response = self.client.post(test_url, post_data)
        self.assertUniqueSuccessMessage(response)

        invite = Invitation.objects.get(email=post_data["email"])
        extra_data = {
            "moderator": {
                "comments": [
                    {
                        "id": comment_id,
                        "doi": comment["doi"],
                        "pid": comment["site_name"].upper(),
                        "user_id": self.admin_moderator.pk,
                    }
                ]
            }
        }
        self.assertDictEqual(invite.extra_data, InvitationExtraData(**extra_data).serialize())

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].to, [post_data["email"]])

        mail.outbox = []
