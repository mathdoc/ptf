import responses
from responses.matchers import json_params_matcher

from django.core import mail
from django.test import TestCase
from django.test import override_settings

from ..management.commands.handle_new_comments import handle_new_comments
from .mixins import ModeratorTestMixin

COMMENTS_BASE_URL = "http://comments.test"


@override_settings(
    COMMENTS_VIEWS_API_BASE_URL=COMMENTS_BASE_URL,
    COMMENTS_VIEWS_API_CREDENTIALS=("login", "password"),
)
class CommandTestCase(ModeratorTestMixin, TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        print("\n**App: 'comments_moderation' - Testing management commands.**")
        super().setUpClass()

    def setUp(self) -> None:
        self.setUpModeratorData()
        mail.outbox = []
        super().setUp()

    def tearDown(self) -> None:
        self.tearDownModeratorData()
        super().tearDown()

    @responses.activate
    def test_handle_new_comments_01(self):
        """Should send a mail to staff_moderator_1 & staff_moderator_2."""
        new_comments = [
            {"pid": self.collection_1.pid.lower(), "comments": [1, 2, 3]},
            {"pid": self.collection_2.pid.lower(), "comments": [4, 5]},
        ]

        responses.add(
            "GET", url=f"{COMMENTS_BASE_URL}/api/comments-new/", status=200, json=new_comments
        )

        post_data = {"comment_id": [1, 2, 3, 4, 5]}
        responses.add(
            "POST",
            url=f"{COMMENTS_BASE_URL}/api/comments-new/",
            status=200,
            match=[json_params_matcher(post_data)],
            json={},
        )

        sent_mails = handle_new_comments()

        self.assertEqual(len(sent_mails), 2)
        self.assertIn(self.staff_moderator_1.email, sent_mails)
        self.assertIn(self.staff_moderator_2.email, sent_mails)

        self.assertEqual(len(mail.outbox), 2)
        # We don't know in which order the mails were sent
        self.assertIn(
            mail.outbox[0].to, [[self.staff_moderator_1.email], [self.staff_moderator_2.email]]
        )
        self.assertIn(
            mail.outbox[1].to, [[self.staff_moderator_1.email], [self.staff_moderator_2.email]]
        )
        self.assertNotEqual(mail.outbox[0].to, mail.outbox[1].to)

    @responses.activate
    def test_handle_new_comments_02(self):
        """Should send a mail to staff_moderator_2 only."""
        new_comments = [{"pid": self.collection_2.pid.lower(), "comments": [4, 5]}]

        responses.add(
            "GET", url=f"{COMMENTS_BASE_URL}/api/comments-new/", status=200, json=new_comments
        )

        post_data = {"comment_id": [4, 5]}
        responses.add(
            "POST",
            url=f"{COMMENTS_BASE_URL}/api/comments-new/",
            status=200,
            match=[json_params_matcher(post_data)],
            json={},
        )

        sent_mails = handle_new_comments()

        self.assertEqual(len(sent_mails), 1)
        self.assertIn(self.staff_moderator_2.email, sent_mails)

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].to, [self.staff_moderator_2.email])

    @responses.activate
    def test_handle_new_comments_03(self):
        """Should send a mail to staff_moderator_1 only."""
        new_comments = [
            {"pid": self.collection_1.pid.lower(), "comments": [1, 2, 3]},
            {"pid": self.collection_2.pid.lower(), "comments": [4, 5]},
        ]

        responses.add(
            "GET", url=f"{COMMENTS_BASE_URL}/api/comments-new/", status=200, json=new_comments
        )

        post_data = {"comment_id": [1, 2, 3]}
        responses.add(
            "POST",
            url=f"{COMMENTS_BASE_URL}/api/comments-new/",
            status=200,
            match=[json_params_matcher(post_data)],
            json={},
        )

        sent_mails = handle_new_comments([self.collection_1.pid.lower()])

        self.assertEqual(len(sent_mails), 1)
        self.assertIn(self.staff_moderator_1.email, sent_mails)

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].to, [self.staff_moderator_1.email])
