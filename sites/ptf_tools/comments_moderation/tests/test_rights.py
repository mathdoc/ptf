from django.test import TestCase

from ..rights import ModeratorUserRights
from .mixins import ModeratorTestMixin


class RightsTestCase(ModeratorTestMixin, TestCase):
    @classmethod
    def setUpClass(cls):
        print("\n**App: 'comments_moderation' - Testing `rights.py`**")
        super().setUpClass()

    def setUp(self) -> None:
        self.setUpModeratorData()
        super().setUp()

    def tearDown(self) -> None:
        self.tearDownModeratorData()
        super().tearDown()

    def get_comment(self) -> dict:
        return {
            "id": 1,
            "doi": "10.5802/crbiol.107",
            "sanitized_html": "<p>My wonderful comment</p>",
            "status": "submitted",
            "date_submitted": "2023-04-01T00:00:00Z",
            "date_last_modified": "2023-04-01T00:00:00Z",
            "article_author_comment": False,
            "editorial_team_comment": False,
            "author_id": 1,
            "author_email": "user_oidc_1@test.test",
            "author_name": "User OIDC 1",
            "author_provider": "ORCID",
            "author_provider_uid": "0000-0000-0000-0001",
            "site": 2,
            "site_name": self.collection_1.pid.lower(),
            "parent": None,
            "parent_author_name": None,
            "base_url": "http://server.test",
            "moderation_data": {
                "moderator": self.base_moderator_1.pk,
                "comment": 1,
                "date_created": "2023-04-01T12:00:00Z",
                "date_moderated": "2023-04-01T12:00:00Z",
            },
            "moderators": [self.base_moderator_1.pk, self.staff_moderator_1.pk],
        }

    def test_admin_moderator_rights(self):
        rights = ModeratorUserRights(self.admin_moderator)

        self.assertTrue(rights.is_admin_moderator())
        self.assertFalse(rights.is_staff_moderator())

        admin_collections = rights.get_user_admin_collections()
        self.assertEqual(len(admin_collections), 2)
        self.assertIn(self.collection_1.pid, admin_collections)
        self.assertIn(self.collection_2.pid, admin_collections)

        staff_collections = rights.get_user_staff_collections()
        self.assertEqual(staff_collections, [])

        query_params = rights.comment_rights_query_params()
        expected = {
            "moderator_id": self.admin_moderator.pk,
            "collection_id": f"{self.collection_1.pid},{self.collection_2.pid}",
        }
        self.assertEqual(query_params, expected)

        comment = self.get_comment()
        self.assertFalse(rights.comment_can_delete(comment))
        self.assertFalse(rights.comment_can_edit(comment))

        self.assertTrue(rights.comment_can_moderate(comment))
        self.assertTrue(rights.comment_can_manage_moderators(comment))

        comment["site_name"] = self.collection_3.pid.lower()
        self.assertFalse(rights.comment_can_moderate(comment))
        self.assertFalse(rights.comment_can_manage_moderators(comment))

    def test_staff_moderator_rights(self):
        rights = ModeratorUserRights(self.staff_moderator_1)

        self.assertFalse(rights.is_admin_moderator())
        self.assertTrue(rights.is_staff_moderator())

        admin_collections = rights.get_user_admin_collections()
        self.assertEqual(admin_collections, [])

        staff_collections = rights.get_user_staff_collections()
        self.assertEqual(len(staff_collections), 1)
        self.assertIn(self.collection_1.pid, staff_collections)

        query_params = rights.comment_rights_query_params()
        expected = {
            "moderator_id": self.staff_moderator_1.pk,
            "collection_id": self.collection_1.pid,
        }
        self.assertEqual(query_params, expected)

        comment = self.get_comment()
        self.assertFalse(rights.comment_can_delete(comment))
        self.assertFalse(rights.comment_can_edit(comment))

        self.assertTrue(rights.comment_can_moderate(comment))
        self.assertTrue(rights.comment_can_manage_moderators(comment))

        comment["site_name"] = "pid_2"
        self.assertTrue(rights.comment_can_moderate(comment))
        self.assertFalse(rights.comment_can_manage_moderators(comment))

    def test_base_moderator_rights(self):
        rights = ModeratorUserRights(self.base_moderator_1)

        self.assertFalse(rights.is_admin_moderator())
        self.assertFalse(rights.is_staff_moderator())

        admin_collections = rights.get_user_admin_collections()
        self.assertEqual(admin_collections, [])

        staff_collections = rights.get_user_staff_collections()
        self.assertEqual(staff_collections, [])

        query_params = rights.comment_rights_query_params()
        expected = {"moderator_id": self.base_moderator_1.pk}
        self.assertEqual(query_params, expected)

        comment = self.get_comment()
        self.assertFalse(rights.comment_can_delete(comment))
        self.assertFalse(rights.comment_can_edit(comment))

        self.assertTrue(rights.comment_can_moderate(comment))
        self.assertFalse(rights.comment_can_manage_moderators(comment))

        rights = ModeratorUserRights(self.base_moderator_2)
        self.assertFalse(rights.comment_can_moderate(comment))
        self.assertFalse(rights.comment_can_manage_moderators(comment))
