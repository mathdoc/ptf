from django.contrib.auth.models import Group
from django.contrib.auth.models import User

from ptf.factories import CollectionFactory
from ptf.models import Collection
from ptf_tools.models import CollectionGroup

from ..models import CommentModerator


class ModeratorTestMixin:
    def setUpModeratorData(self) -> None:
        """
        Insert collections, group, admin moderator, staff moderators
        and base moderators in database.
        """
        # Collections
        # We use actual collections because the code relies on the entry
        # `email_from` in site_register.py
        self.collection_1 = CollectionFactory(pid="CRBIOL", title_tex="Collection 1")
        self.collection_2 = CollectionFactory(pid="CRMATH", title_tex="Collection 2")
        self.collection_3 = CollectionFactory(pid="CRCHIM", title_tex="Collection 3")

        # Group with 2 collections
        group = Group.objects.create(name="Collections 1 & 2")
        col_group = CollectionGroup.objects.create(group=group)
        col_group.collections.add(self.collection_1, self.collection_2)

        # Admin moderator
        self.admin_moderator = User.objects.create_user(
            "admin_moderator", "admin@test.test", "***"
        )
        self.admin_moderator.groups.add(group)

        # Staff moderators
        self.staff_moderator_1 = User.objects.create_user(
            "staff_moderator_1", "staff_1@test.test", "***"
        )
        staff_cm_1 = CommentModerator.objects.create(
            user=self.staff_moderator_1, is_moderator=True
        )
        staff_cm_1.collections.add(self.collection_1)

        self.staff_moderator_2 = User.objects.create_user(
            "staff_moderator_2", "staff_2@test.test", "***"
        )
        staff_cm_2 = CommentModerator.objects.create(
            user=self.staff_moderator_2, is_moderator=True
        )
        staff_cm_2.collections.add(self.collection_2)

        # Base moderators
        self.base_moderator_1 = User.objects.create_user(
            "base_moderator_1", "base_1@test.test", "***"
        )
        CommentModerator.objects.create(user=self.base_moderator_1, is_moderator=True)

        self.base_moderator_2 = User.objects.create_user(
            "base_moderator_2", "base_2@test.test", "***"
        )
        CommentModerator.objects.create(user=self.base_moderator_2, is_moderator=True)

    def tearDownModeratorData(self) -> None:
        """Empty the tables filled in `setUpModeratorData`."""
        User.objects.all().delete()
        Group.objects.all().delete()
        Collection.objects.all().delete()
