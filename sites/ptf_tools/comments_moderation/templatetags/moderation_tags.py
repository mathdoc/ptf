from django import template
from django.conf import settings
from django.contrib.auth.models import AnonymousUser
from django.contrib.auth.models import User

from comments_moderation.rights import ModeratorUserRights
from ptf_tools.templatetags.tools_helpers import get_authorized_collections

from ..utils import is_comment_moderator

register = template.Library()


@register.filter
def show_comments_nav(user: User | AnonymousUser) -> bool:
    return (
        not getattr(settings, "COMMENTS_DISABLED", False)
        and isinstance(user, User)
        and (is_comment_moderator(user) or get_authorized_collections(user) or user.is_superuser)
    )


@register.filter
def can_manage_moderators(user: User) -> bool:
    rights = ModeratorUserRights(user)
    return (
        len(rights.get_user_admin_collections()) > 0
        or len(rights.get_user_staff_collections()) > 0
    )
