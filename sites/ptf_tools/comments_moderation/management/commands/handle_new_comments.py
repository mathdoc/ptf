import requests

from django.conf import settings
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from django.core.management.base import CommandParser
from django.urls import reverse
from django.utils.html import strip_tags

from comments_api.constants import PARAM_COLLECTION
from comments_api.constants import PARAM_COMMENT
from comments_views.core.utils import comments_credentials
from comments_views.core.utils import comments_server_url
from ptf.utils import send_email_from_template


class Command(BaseCommand):
    help = """Get all the newly posted comments, send mails and update them."""

    def add_arguments(self, parser: CommandParser) -> None:
        parser.add_argument(
            "-p",
            "--pid",
            dest="pid",
            action="store",
            nargs="*",
            default=[],
            type=str,
            help="Collections' PID if you want to restrict to specific collections. You can specify multiple PIDs with a space between each.",
            required=False,
        )

    def handle(self, *args, **options):
        self.stdout.write("Querying new comments...\n")

        sent_mails = handle_new_comments(options["pid"])
        for to, message in sent_mails.items():
            self.stdout.write(f"""Mail sent to '{to}': {message}""")

        if sent_mails:
            self.stdout.write(f"{len(sent_mails)} e-mails sent!")
        else:
            self.stdout.write("No e-mail sent!")


def handle_new_comments(collections: list[str] = []) -> dict[str, str]:
    collections = [c.upper() for c in collections]
    query_params = {}
    if collections:
        query_params[PARAM_COLLECTION] = ",".join(collections)

    response = requests.get(
        comments_server_url(query_params, url_prefix="comments-new"), auth=comments_credentials()
    )

    response.raise_for_status()

    # [{pid: PID, comments: [comment_id, comment_id, ...]}]
    response_data = response.json()

    # Filter the data on the provided collections
    # NB: This should already done by the comment server.
    data = {}
    for d in response_data:
        if not collections or d["pid"].upper() in collections:
            data[d["pid"]] = d["comments"]

    # Get the recipients of newly posted comments
    moderators = User.objects.filter(
        comment_moderator__is_moderator=True, comment_moderator__collections__pid__isnull=False
    )
    if collections:
        moderators = moderators.filter(comment_moderator__collections__pid__in=collections)

    moderators = moderators.prefetch_related("comment_moderator__collections").distinct()

    # Get the current site URL from the settings
    base_url = getattr(settings, "SITE_REAL_BASE_URL", None)
    if base_url is None:
        base_url = "https://" + getattr(settings, "SITE_DOMAIN", "centre-mersenne.org")
    if base_url[-1] == "/":
        base_url = base_url[:-1]
    comment_dashboard_url = f"{base_url}{reverse('comment_list')}"

    sent_mails = {}

    # Iterate over all the staff moderators and check if they have any new comment
    comments_processed = set()
    for moderator in moderators:
        new_comments = []
        collections = [c.pid for c in moderator.comment_moderator.collections.all()]
        for col in collections:
            if col.lower() in data:
                col_comments = data[col.lower()]
                new_comments.append(
                    f"<li><strong>{col}:</strong> {len(col_comments)} comment(s), </li>"
                )
                comments_processed.update(col_comments)

        # Send mail if any new comments have been submitted for this moderator
        if new_comments:
            context_data = {
                "full_name": f"{moderator.first_name} {moderator.last_name}",
                "comments_data": f"""<ul>{"<br>".join(new_comments)}</ul>""",
                "collections": ", ".join(collections),
                "email_signature": "The editorial team",
                "comment_dashboard_url": comment_dashboard_url,
            }
            send_email_from_template(
                "mail/comment_new.html",
                context_data,
                "[TRAMMEL] New commment(s)",
                to=[moderator.email],
            )
            comments_print = " ".join([strip_tags(c) for c in new_comments])
            sent_mails[moderator.email] = comments_print

    # POST all the processed comments to update their "is_new" column
    post_data = {PARAM_COMMENT: list(comments_processed)}
    response = requests.post(
        comments_server_url(query_params, url_prefix="comments-new"),
        auth=comments_credentials(),
        json=post_data,
    )
    response.raise_for_status()

    return sent_mails
