from django import forms

from ptf_tools.forms import InviteUserForm

from .models import CommentModerator


class MultipleCollectionForm(forms.Form):
    # To be initialized by each instance of the form
    # Not required when the user unselect all
    collections = forms.MultipleChoiceField(required=False, widget=forms.CheckboxSelectMultiple)

    def __init__(self, collection_choices: tuple, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["collections"].choices = collection_choices


class StaffModeratorForm(MultipleCollectionForm, forms.Form):
    """
    Generic form for adding a staff comment moderator.
    """

    moderator_id = forms.IntegerField(required=True, widget=forms.HiddenInput)


class CommentIdForm(forms.Form):
    comment_id = forms.IntegerField(required=True, widget=forms.HiddenInput)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["comment_id"].widget.attrs["readonly"] = True


class AddBaseModeratorForm(CommentIdForm):
    """
    Generic form for adding a basic comment moderator.
    """

    moderator_id = forms.IntegerField(required=True, widget=forms.HiddenInput)


class RemoveBaseModeratorForm(forms.Form):
    required_css_class = "required"

    moderators = forms.MultipleChoiceField(
        label="moderators", widget=forms.CheckboxSelectMultiple, required=True
    )
    comment_id = forms.IntegerField(required=True, widget=forms.HiddenInput)

    def __init__(self, *args, **kwargs):
        moderators = kwargs.pop("moderators")
        super().__init__(*args, **kwargs)
        self.fields["comment_id"].widget.attrs["readonly"] = True
        self.fields["moderators"].choices = moderators


class UserCommentModeratorForm(forms.ModelForm):
    class Meta:
        model = CommentModerator
        fields = ["user", "is_moderator", "collections"]

    def clean(self) -> dict:
        cleaned_data = super().clean()
        if cleaned_data["collections"] and not cleaned_data["is_moderator"]:
            self.add_error(
                "is_moderator",
                "Error: the user must be flagged as a moderator to have" "moderating collections.",
            )
        return cleaned_data


class InviteBaseModeratorForm(CommentIdForm, InviteUserForm):
    pass


class InviteStaffModeratorForm(MultipleCollectionForm, InviteUserForm):
    collections = forms.MultipleChoiceField(required=True, widget=forms.CheckboxSelectMultiple)
