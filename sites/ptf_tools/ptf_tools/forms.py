import glob
import os
from operator import itemgetter

from allauth.account.forms import SignupForm as BaseSignupForm
from ckeditor_uploader.fields import RichTextUploadingFormField
from crispy_forms.helper import FormHelper

from django import forms
from django.conf import settings
from django.contrib.auth import get_user_model

from invitations.forms import CleanEmailMixin
from mersenne_cms.models import News
from mersenne_cms.models import Page
from ptf.model_helpers import get_collection_id
from ptf.model_helpers import is_site_en_only
from ptf.model_helpers import is_site_fr_only
from ptf.models import BibItemId
from ptf.models import Collection
from ptf.models import ExtId
from ptf.models import ExtLink
from ptf.models import GraphicalAbstract
from ptf.models import RelatedArticles
from ptf.models import ResourceId

from .models import Invitation
from .models import InvitationExtraData

TYPE_CHOICES = (
    ("doi", "doi"),
    ("mr-item-id", "mr"),
    ("zbl-item-id", "zbl"),
    ("numdam-id", "numdam"),
    ("pmid", "pubmed"),
)

RESOURCE_ID_CHOICES = (
    ("issn", "p-issn"),
    ("e-issn", "e-issn"),
)

REL_CHOICES = (
    ("small_icon", "small_icon"),
    ("icon", "icon"),
    ("test_website", "test_website"),
    ("website", "website"),
)

IMPORT_CHOICES = (
    ("1", "Préserver des métadonnées existantes dans ptf-tools (equal-contrib, coi_statement)"),
    ("2", "Remplacer tout par le fichier XML"),
)


class PtfFormHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.label_class = "col-xs-4 col-sm-2"
        self.field_class = "col-xs-8 col-sm-6"
        self.form_tag = False


class PtfModalFormHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.label_class = "col-xs-3"
        self.field_class = "col-xs-8"
        self.form_tag = False


class PtfLargeModalFormHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.label_class = "col-xs-6"
        self.field_class = "col-xs-6"
        self.form_tag = False


class FormSetHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.form_tag = False
        self.template = "bootstrap3/whole_uni_formset.html"


class BibItemIdForm(forms.ModelForm):
    class Meta:
        model = BibItemId
        fields = ["bibitem", "id_type", "id_value"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["id_type"].widget = forms.Select(choices=TYPE_CHOICES)
        self.fields["bibitem"].widget = forms.HiddenInput()


class ExtIdForm(forms.ModelForm):
    class Meta:
        model = ExtId
        fields = ["resource", "id_type", "id_value"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["id_type"].widget = forms.Select(choices=TYPE_CHOICES)
        self.fields["resource"].widget = forms.HiddenInput()


class ExtLinkForm(forms.ModelForm):
    class Meta:
        model = ExtLink
        fields = ["rel", "location"]
        widgets = {
            "rel": forms.Select(choices=REL_CHOICES),
        }


class ResourceIdForm(forms.ModelForm):
    class Meta:
        model = ResourceId
        fields = ["id_type", "id_value"]
        widgets = {
            "id_type": forms.Select(choices=RESOURCE_ID_CHOICES),
        }


class CollectionForm(forms.ModelForm):
    class Meta:
        model = Collection
        fields = [
            "pid",
            "provider",
            "coltype",
            "title_tex",
            "abbrev",
            "doi",
            "wall",
            "alive",
            "sites",
        ]
        widgets = {
            "title_tex": forms.TextInput(),
        }

    def __init__(self, *args, **kwargs):
        # Add extra fields before the base class __init__
        self.base_fields["description_en"] = RichTextUploadingFormField(
            required=False, label="Description (EN)"
        )
        self.base_fields["description_fr"] = RichTextUploadingFormField(
            required=False, label="Description (FR)"
        )

        super().__init__(*args, **kwargs)

        # self.instance is now set, specify initial values
        qs = self.instance.abstract_set.filter(tag="description")
        for abstract in qs:
            if abstract.lang == "fr":
                self.initial["description_fr"] = abstract.value_html
            elif abstract.lang == "en":
                self.initial["description_en"] = abstract.value_html


class ContainerForm(forms.Form):
    pid = forms.CharField(required=True, initial="")
    title = forms.CharField(required=False, initial="")
    trans_title = forms.CharField(required=False, initial="")
    publisher = forms.CharField(required=True, initial="")
    year = forms.CharField(required=False, initial="")
    volume = forms.CharField(required=False, initial="")
    number = forms.CharField(required=False, initial="")
    icon = forms.FileField(required=False)

    def __init__(self, container, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.container = container

        if "data" in kwargs:
            # form_invalid: preserve input values
            self.fields["pid"].initial = kwargs["data"]["pid"]
            self.fields["publisher"].initial = kwargs["data"]["publisher"]
            self.fields["year"].initial = kwargs["data"]["year"]
            self.fields["volume"].initial = kwargs["data"]["volume"]
            self.fields["number"].initial = kwargs["data"]["number"]
            self.fields["title"].initial = kwargs["data"]["title"]
            self.fields["trans_title"].initial = kwargs["data"]["trans_title"]
        elif container:
            self.fields["pid"].initial = container.pid
            self.fields["title"].initial = container.title_tex
            self.fields["trans_title"].initial = container.trans_title_tex
            if container.my_publisher:
                self.fields["publisher"].initial = container.my_publisher.pub_name
            self.fields["year"].initial = container.year
            self.fields["volume"].initial = container.volume
            self.fields["number"].initial = container.number

            for extlink in container.extlink_set.all():
                if extlink.rel == "icon":
                    self.fields["icon"].initial = os.path.basename(extlink.location)

    def clean(self):
        cleaned_data = super().clean()
        return cleaned_data


class ArticleForm(forms.Form):
    pid = forms.CharField(required=True, initial="")
    title = forms.CharField(required=False, initial="")
    fpage = forms.CharField(required=False, initial="")
    lpage = forms.CharField(required=False, initial="")
    page_count = forms.CharField(required=False, initial="")
    page_range = forms.CharField(required=False, initial="")
    icon = forms.FileField(required=False)
    pdf = forms.FileField(required=False)
    coi_statement = forms.CharField(required=False, initial="")
    show_body = forms.BooleanField(required=False, initial=True)
    do_not_publish = forms.BooleanField(required=False, initial=True)

    def __init__(self, article, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.article = article

        if "data" in kwargs:
            data = kwargs["data"]
            # form_invalid: preserve input values
            self.fields["pid"].initial = data["pid"]
            if "title" in data:
                self.fields["title"].initial = data["title"]
            if "fpage" in data:
                self.fields["fpage"].initial = data["fpage"]
            if "lpage" in data:
                self.fields["lpage"].initial = data["lpage"]
            if "page_range" in data:
                self.fields["page_range"].initial = data["page_range"]
            if "page_count" in data:
                self.fields["page_count"].initial = data["page_count"]
            if "coi_statement" in data:
                self.fields["coi_statement"].initial = data["coi_statement"]
            if "show_body" in data:
                self.fields["show_body"].initial = data["show_body"]
            if "do_not_publish" in data:
                self.fields["do_not_publish"].initial = data["do_not_publish"]
        elif article:
            # self.fields['pid'].initial = article.pid
            self.fields["title"].initial = article.title_tex
            self.fields["fpage"].initial = article.fpage
            self.fields["lpage"].initial = article.lpage
            self.fields["page_range"].initial = article.page_range
            self.fields["coi_statement"].initial = (
                article.coi_statement if article.coi_statement else ""
            )
            self.fields["show_body"].initial = article.show_body
            self.fields["do_not_publish"].initial = article.do_not_publish

            for count in article.resourcecount_set.all():
                if count.name == "page-count":
                    self.fields["page_count"].initial = count.value

            for extlink in article.extlink_set.all():
                if extlink.rel == "icon":
                    self.fields["icon"].initial = os.path.basename(extlink.location)

            qs = article.datastream_set.filter(rel="full-text", mimetype="application/pdf")
            if qs.exists():
                datastream = qs.first()
                self.fields["pdf"].initial = datastream.location

    def clean(self):
        cleaned_data = super().clean()
        return cleaned_data


def cast_volume(element):
    # Permet le classement des volumes dans le cas où :
    # - un numero de volume est de la forme "11-12" (cf crchim)
    # - un volume est de la forme "S5" (cf smai)
    if not element:
        return "", ""
    try:
        casted = int(element.split("-")[0])
        extra = ""
    except ValueError as _:
        casted = int(element.split("-")[0][1:])
        extra = element
    return extra, casted


def unpack_pid(filename):
    # retourne un tableau pour chaque filename de la forme :
    # [filename, collection, year, vseries, volume_extra, volume, issue_extra, issue]
    # Permet un tri efficace par la suite
    collection, year, vseries, volume, issue = filename.split("/")[-1].split(".")[0].split("_")
    extra_volume, casted_volume = cast_volume(volume)
    extra_issue, casted_issue = cast_volume(issue)
    return (
        filename,
        collection,
        year,
        vseries,
        extra_volume,
        casted_volume,
        extra_issue,
        casted_issue,
    )


def get_volume_choices(colid, to_appear=False):
    if settings.IMPORT_CEDRICS_DIRECTLY:
        collection_folder = os.path.join(settings.CEDRAM_TEX_FOLDER, colid)

        if to_appear:
            issue_folders = [
                volume for volume in os.listdir(collection_folder) if f"{colid}_0" in volume
            ]

        else:
            issue_folders = [
                d
                for d in os.listdir(collection_folder)
                if os.path.isdir(os.path.join(collection_folder, d))
            ]
            issue_folders = sorted(issue_folders, reverse=True)

        files = [
            (os.path.join(collection_folder, d, d + "-cdrxml.xml"), d)
            for d in issue_folders
            if os.path.isfile(os.path.join(collection_folder, d, d + "-cdrxml.xml"))
        ]
    else:
        if to_appear:
            volumes_path = os.path.join(
                settings.CEDRAM_XML_FOLDER, colid, "metadata", f"{colid}_0*.xml"
            )
        else:
            volumes_path = os.path.join(settings.CEDRAM_XML_FOLDER, colid, "metadata", "*.xml")

        files = [unpack_pid(filename) for filename in glob.glob(volumes_path)]
        sort = sorted(files, key=itemgetter(1, 2, 3, 4, 5, 6, 7), reverse=True)
        files = [(item[0], item[0].split("/")[-1]) for item in sort]
    return files


def get_article_choices(colid, issue_name):
    issue_folder = os.path.join(settings.CEDRAM_TEX_FOLDER, colid, issue_name)
    article_choices = [
        (d, os.path.basename(d))
        for d in os.listdir(issue_folder)
        if (
            os.path.isdir(os.path.join(issue_folder, d))
            and os.path.isfile(os.path.join(issue_folder, d, d + "-cdrxml.xml"))
        )
    ]
    article_choices = sorted(article_choices, reverse=True)

    return article_choices


class ImportArticleForm(forms.Form):
    issue = forms.ChoiceField(
        label="Numéro",
    )
    article = forms.ChoiceField(
        label="Article",
    )

    def __init__(self, *args, **kwargs):
        # we need to pop this extra colid kwarg if not, the call to super.__init__ won't work
        colid = kwargs.pop("colid")
        super().__init__(*args, **kwargs)
        volumes = get_volume_choices(colid)
        self.fields["issue"].choices = volumes
        articles = []
        if volumes:
            articles = get_article_choices(colid, volumes[0][1])
        self.fields["article"].choices = articles


class ImportContainerForm(forms.Form):
    filename = forms.ChoiceField(
        label="Numéro",
    )
    remove_email = forms.BooleanField(
        label="Supprimer les mails des contribs issus de CEDRAM ?",
        initial=True,
        required=False,
    )
    remove_date_prod = forms.BooleanField(
        label="Supprimer les dates de mise en prod issues de CEDRAM ?",
        initial=True,
        required=False,
    )

    def __init__(self, *args, **kwargs):
        # we need to pop this extra colid kwarg if not, the call to super.__init__ won't work
        colid = kwargs.pop("colid")
        to_appear = kwargs.pop("to_appear")
        super().__init__(*args, **kwargs)
        self.fields["filename"].choices = get_volume_choices(colid, to_appear)


class DiffContainerForm(forms.Form):
    import_choice = forms.ChoiceField(
        choices=IMPORT_CHOICES, label="Que faire des différences ?", widget=forms.RadioSelect()
    )

    def __init__(self, *args, **kwargs):
        # we need to pop this extra full_path kwarg if not, the call to super.__init__ won't work
        kwargs.pop("colid")
        # filename = kwargs.pop('filename')
        # to_appear = kwargs.pop('to_appear')
        super().__init__(*args, **kwargs)

        self.fields["import_choice"].initial = IMPORT_CHOICES[0][0]


class RegisterPubmedForm(forms.Form):
    CHOICES = [
        ("off", "Yes"),
        ("on", "No, update the article in PubMed"),
    ]
    update_article = forms.ChoiceField(
        label="Are you registering the article for the first time ?",
        widget=forms.RadioSelect,
        choices=CHOICES,
        required=False,
        initial="on",
    )


class CreateFrontpageForm(forms.Form):
    create_frontpage = forms.BooleanField(
        label="Update des frontpages des articles avec date de mise en ligne ?",
        initial=False,
        required=False,
    )


class RelatedForm(forms.ModelForm):
    doi_list = forms.CharField(
        required=False,
        widget=forms.Textarea(attrs={"rows": "10", "placeholder": "doi_1\ndoi_2\ndoi_3\n"}),
    )

    exclusion_list = forms.CharField(
        required=False,
        widget=forms.Textarea(attrs={"rows": "10"}),
    )

    class Meta:
        model = RelatedArticles
        fields = ["doi_list", "exclusion_list", "automatic_list"]


class GraphicalAbstractForm(forms.ModelForm):
    """Form for the Graphical Abstract model"""

    class Meta:
        model = GraphicalAbstract
        fields = ("graphical_abstract", "illustration")


class PageForm(forms.ModelForm):
    class Meta:
        model = Page
        fields = [
            "menu_title_en",
            "menu_title_fr",
            "parent_page",
            "content_en",
            "content_fr",
            "state",
            "slug_en",
            "slug_fr",
            "menu_order",
            "position",
            "mersenne_id",
            "site_id",
        ]

    def __init__(self, *args, **kwargs):
        site_id = kwargs.pop("site_id")
        user = kwargs.pop("user")
        super().__init__(*args, **kwargs)

        self.fields["site_id"].initial = site_id

        if not user.is_staff:
            for field_name in ["mersenne_id", "site_id"]:
                field = self.fields[field_name]
                # Hide the field is not enough, otherwise BaseForm._clean_fields will not get the value
                field.disabled = True
                field.widget = field.hidden_widget()

        colid = get_collection_id(int(site_id))

        # By default, CKEditor stores files in 1 folder
        # We want to store the files in a @colid folder
        for field_name in ["content_en", "content_fr"]:
            field = self.fields[field_name]
            widget = field.widget
            widget.config["filebrowserUploadUrl"] = "/ckeditor/upload/" + colid
            widget.config["filebrowserBrowseUrl"] = "/ckeditor/browse/" + colid

        pages = Page.objects.filter(site_id=site_id, parent_page=None)
        if self.instance:
            pages = pages.exclude(id=self.instance.id)

        choices = [(p.id, p.menu_title_en) for p in pages if p.menu_title_en]
        self.fields["parent_page"].choices = sorted(
            choices + [(None, "---------")], key=lambda x: x[1]
        )

        self.fields["menu_title_en"].widget.attrs.update({"class": "menu_title"})
        self.fields["menu_title_fr"].widget.attrs.update({"class": "menu_title"})

        if is_site_en_only(site_id):
            self.fields.pop("content_fr")
            self.fields.pop("menu_title_fr")
            self.fields.pop("slug_fr")
        elif is_site_fr_only(site_id):
            self.fields.pop("content_en")
            self.fields.pop("menu_title_en")
            self.fields.pop("slug_en")

    def save_model(self, request, obj, form, change):
        obj.site_id = form.cleaned_data["site_id"]
        super().save_model(request, obj, form, change)


class NewsForm(forms.ModelForm):
    class Meta:
        model = News
        fields = [
            "title_en",
            "title_fr",
            "content_en",
            "content_fr",
            "site_id",
        ]

    def __init__(self, *args, **kwargs):
        site_id = kwargs.pop("site_id")
        user = kwargs.pop("user")
        super().__init__(*args, **kwargs)

        self.fields["site_id"].initial = site_id

        if not user.is_staff:
            for field_name in ["site_id"]:
                field = self.fields[field_name]
                # Hide the field is not enough, otherwise BaseForm._clean_fields will not get the value
                field.disabled = True
                field.widget = field.hidden_widget()

        colid = get_collection_id(int(site_id))

        # By default, CKEditor stores files in 1 folder
        # We want to store the files in a @colid folder
        for field_name in ["content_en", "content_fr"]:
            field = self.fields[field_name]
            widget = field.widget
            widget.config["filebrowserUploadUrl"] = "/ckeditor/upload/" + colid
            widget.config["filebrowserBrowseUrl"] = "/ckeditor/browse/" + colid

        if is_site_en_only(site_id):
            self.fields.pop("content_fr")
            self.fields.pop("title_fr")
        elif is_site_fr_only(site_id):
            self.fields.pop("content_en")
            self.fields.pop("title_en")

    def save_model(self, request, obj, form, change):
        obj.site_id = form.cleaned_data["site_id"]
        super().save_model(request, obj, form, change)


class InviteUserForm(forms.Form):
    """Base form to invite user."""

    required_css_class = "required"

    first_name = forms.CharField(label="First name", max_length=150, required=True)
    last_name = forms.CharField(label="Last name", max_length=150, required=True)
    email = forms.EmailField(label="E-mail address", required=True)


class InvitationAdminChangeForm(forms.ModelForm):
    class Meta:
        model = Invitation
        fields = "__all__"

    def clean_extra_data(self):
        """
        Enforce the JSON structure with the InvitationExtraData dataclass interface.
        """
        try:
            InvitationExtraData(**self.cleaned_data["extra_data"])
        except Exception as e:
            raise forms.ValidationError(e)

        return self.cleaned_data["extra_data"]


class InvitationAdminAddForm(InvitationAdminChangeForm, CleanEmailMixin):
    class Meta:
        fields = ("email", "first_name", "last_name", "extra_data")

    def save(self, *args, **kwargs):
        """
        Populate the invitation data, save in DB and send the invitation e-mail.
        """
        cleaned_data = self.clean()
        email = cleaned_data["email"]
        params = {"email": email}
        if cleaned_data.get("inviter"):
            params["inviter"] = cleaned_data["inviter"]
        else:
            user = getattr(self, "user", None)
            if isinstance(user, get_user_model()):
                params["inviter"] = user
        instance = Invitation.create(**params)
        instance.first_name = cleaned_data["first_name"]
        instance.last_name = cleaned_data["last_name"]
        instance.extra_data = cleaned_data.get("extra_data", {})
        instance.save()
        full_name = f"{instance.first_name} {instance.last_name}"
        instance.send_invitation(self.request, **{"full_name": full_name})
        super().save(*args, **kwargs)
        return instance


class SignupForm(BaseSignupForm):
    email = forms.EmailField(widget=forms.HiddenInput())
