import os

SECRET_KEY = "KeyToChange1234567890&"

DEBUG = True

SOLR_URL = "http://127.0.0.1:8983/solr/ptftools/"

# ARTICLE_BASE_URL = BASE_URL + 'article/'
# ISSUE_BASE_URL = BASE_URL + 'issue/'
# ICON_BASE_URL = BASE_URL + 'icon/'
RESOURCES_ROOT = "/mersenne_test_data/"

MATHDOC_ARCHIVE_FOLDER = "/mathdoc_archive"

MERSENNE_TMP_FOLDER = "/tmp/mersenne/"
MERSENNE_LOG_FILE = os.path.join(MERSENNE_TMP_FOLDER, "transformations.log")
MERSENNE_TEST_DATA_FOLDER = "/mersenne_test_data"
MERSENNE_PROD_DATA_FOLDER = "/mersenne_prod_data"

NUMDAM_DATA_ROOT = "/numdam_data"
NUMDAM_URL = "http://127.0.0.1:8003"

PTF_XSL_FOLDER = "../../../ptf-xsl"

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": "ptftools",  # Or path to database file if using sqlite3.
        "USER": "ptftools_user",  # Not used with sqlite3.
        "PASSWORD": "ptftools_password",  # Not used with sqlite3.
        "HOST": "localhost",  # Set to empty string for localhost. Not used with sqlite3.
        "PORT": "",  # Set to empty string for default. Not used with sqlite3.
    }
}


# Uncomment this in development
SENDFILE_BACKEND = "sendfile.backends.development"

LOG_DIR = "/tmp/"


def get_virtualenv_dir(base_dir, ptf_dir):
    return os.path.join(base_dir, "venv")


DOAJ_TOKEN_MERSENNE = ""
DOAJ_TOKEN_PCJ = ""
