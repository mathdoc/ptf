from django.contrib import admin
from django.contrib.auth.admin import GroupAdmin as BaseGroupAdmin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import Group
from django.contrib.auth.models import User
from django.db.models import QuerySet
from django.http import HttpRequest

from comments_moderation.forms import UserCommentModeratorForm
from comments_moderation.models import CommentModerator
from mersenne_cms.admin import PageAdmin
from mersenne_cms.admin import Site
from mersenne_cms.models import Page
from ptf.admin import ptf_admin
from ptf_tools.models import CollectionGroup

from .forms import InvitationAdminAddForm
from .forms import InvitationAdminChangeForm
from .models import Invitation


class GroupInline(admin.StackedInline):
    model = CollectionGroup
    can_delete = False
    verbose_name_plural = "collection groups"


class GroupAdmin(BaseGroupAdmin):
    inlines = (GroupInline,)
    list_display = ("name", "get_collections")

    def get_queryset(self, request: HttpRequest) -> QuerySet:
        return (
            super()
            .get_queryset(request)
            .prefetch_related("collectiongroup", "collectiongroup__collections")
        )

    @admin.display(description="Collections")
    def get_collections(self, obj) -> str:
        return obj.collectiongroup.get_collections()


# Re-register GroupAdmin
ptf_admin.unregister(Group)
ptf_admin.register(Group, GroupAdmin)
ptf_admin.register(Page, PageAdmin)
ptf_admin.register(Site)


class ModeratorUserInline(admin.StackedInline):
    model = CommentModerator
    can_delete = False
    fields = ("is_moderator", "collections")
    form = UserCommentModeratorForm
    add_form = UserCommentModeratorForm


class UserAdmin(BaseUserAdmin):
    inlines = (ModeratorUserInline,)
    list_display = (
        "username",
        "email",
        "first_name",
        "last_name",
        "is_staff",
        "is_superuser",
        "is_moderator",
    )

    def get_queryset(self, request: HttpRequest) -> QuerySet:
        return super().get_queryset(request).prefetch_related("comment_moderator")

    @admin.display(
        description="Moderator",
        ordering="comment_moderator__is_moderator",
    )
    @admin.display(boolean=True)
    def is_moderator(self, obj) -> bool:
        return hasattr(obj, "comment_moderator") and obj.comment_moderator.is_moderator


ptf_admin.unregister(User)
ptf_admin.register(User, UserAdmin)


class InvitationAdmin(admin.ModelAdmin):
    list_display = ("email", "first_name", "last_name", "sent", "accepted", "key")
    raw_id_fields = ("inviter",)

    readonly_fields = ["accepted", "key", "sent", "inviter", "created"]

    def get_form(self, request, obj=None, **kwargs):
        if obj:
            kwargs["form"] = InvitationAdminChangeForm
        else:
            kwargs["form"] = InvitationAdminAddForm
            kwargs["form"].user = request.user
            kwargs["form"].request = request
        return super().get_form(request, obj, **kwargs)

    def get_readonly_fields(self, request: HttpRequest, obj):
        """
        Disable editing the email field once the invite has already been sent.
        """
        readonly_fields = list(self.readonly_fields)
        if obj:
            readonly_fields += ["email"]
        return readonly_fields


ptf_admin.register(Invitation, InvitationAdmin)
