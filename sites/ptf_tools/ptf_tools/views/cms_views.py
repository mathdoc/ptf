import base64
import json
import os
import re
import shutil
from datetime import datetime

import requests
from ckeditor_uploader.views import ImageUploadView
from ckeditor_uploader.views import browse
from munch import Munch
from requests import Timeout

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import UserPassesTestMixin
from django.core.exceptions import PermissionDenied
from django.forms.models import model_to_dict
from django.http import Http404
from django.http import HttpResponse
from django.http import HttpResponseBadRequest
from django.http import HttpResponseRedirect
from django.http import HttpResponseServerError
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.urls import resolve
from django.urls import reverse
from django.utils import timezone
from django.utils.safestring import mark_safe
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import CreateView
from django.views.generic import TemplateView
from django.views.generic import UpdateView
from django.views.generic import View

from mersenne_cms.models import MERSENNE_ID_VIRTUAL_ISSUES
from mersenne_cms.models import News
from mersenne_cms.models import Page
from mersenne_cms.models import get_news_content
from mersenne_cms.models import get_pages_content
from mersenne_cms.models import import_news
from mersenne_cms.models import import_pages
from ptf import model_data_converter
from ptf import model_helpers
from ptf.cmds import solr_cmds
from ptf.cmds import xml_cmds
from ptf.cmds.ptf_cmds import base_ptf_cmds
from ptf.cmds.xml import xml_utils
from ptf.cmds.xml.jats.builder.issue import get_issue_title_xml

# from ptf.display import resolver
from ptf.exceptions import ServerUnderMaintenance

# from ptf.model_data import ArticleData
from ptf.model_data import IssueData
from ptf.model_data import create_contributor
from ptf.model_data import create_publisherdata

# from ptf.models import ExtLink
# from ptf.models import ResourceInSpecialIssue
# from ptf.models import Contribution
# from ptf.models import Collection
# from ptf.models import Abstract
from ptf.models import Article
from ptf.models import Collection
from ptf.models import Container
from ptf.models import ContribAddress
from ptf.models import GraphicalAbstract
from ptf.models import RelatedArticles
from ptf.models import get_names
from ptf.site_register import SITE_REGISTER
from ptf_tools.forms import GraphicalAbstractForm
from ptf_tools.forms import NewsForm
from ptf_tools.forms import PageForm
from ptf_tools.forms import RelatedForm
from ptf_tools.utils import is_authorized_editor

from .base_views import check_lock


def get_media_base_root(colid):
    """
    Base folder where media files are stored in Trammel
    """
    if colid in ["CRMECA", "CRBIOL", "CRGEOS", "CRCHIM", "CRMATH", "CRPHYS"]:
        colid = "CR"

    return os.path.join(settings.RESOURCES_ROOT, "media", colid)


def get_media_base_root_in_test(colid):
    """
    Base folder where media files are stored in the test website
    Use the same folder as the Trammel media folder so that no copy is necessary when deploy in test
    """
    return get_media_base_root(colid)


def get_media_base_root_in_prod(colid):
    """
    Base folder where media files are stored in the prod website
    """
    if colid in ["CRMECA", "CRBIOL", "CRGEOS", "CRCHIM", "CRMATH", "CRPHYS"]:
        colid = "CR"

    return os.path.join(settings.MERSENNE_PROD_DATA_FOLDER, "media", colid)


def get_media_base_url(colid):
    path = os.path.join(settings.MEDIA_URL, colid)

    if colid in ["CRMECA", "CRBIOL", "CRGEOS", "CRCHIM", "CRMATH", "CRPHYS"]:
        prefixes = {
            "CRMECA": "mecanique",
            "CRBIOL": "biologies",
            "CRGEOS": "geoscience",
            "CRCHIM": "chimie",
            "CRMATH": "mathematique",
            "CRPHYS": "physique",
        }
        path = f"/{prefixes[colid]}{settings.MEDIA_URL}/CR"

    return path


def change_ckeditor_storage(colid):
    """
    By default, CKEditor stores all the files under 1 folder (MEDIA_ROOT)
    We want to store the files under a subfolder of @colid
    To do that we have to
    - change the URL calling this view to pass the site_id (info used by the Pages to filter the objects)
    - modify the storage location
    """

    from ckeditor_uploader import utils
    from ckeditor_uploader import views

    from django.core.files.storage import FileSystemStorage

    storage = FileSystemStorage(
        location=get_media_base_root(colid), base_url=get_media_base_url(colid)
    )

    utils.storage = storage
    views.storage = storage


class EditorRequiredMixin(UserPassesTestMixin):
    def test_func(self):
        return is_authorized_editor(self.request.user, self.kwargs.get("colid"))


class CollectionImageUploadView(EditorRequiredMixin, ImageUploadView):
    """
    By default, CKEditor stores all the files under 1 folder (MEDIA_ROOT)
    We want to store the files under a subfolder of @colid
    To do that we have to
    - change the URL calling this view to pass the site_id (info used by the Pages to filter the objects)
    - modify the storage location
    """

    def dispatch(self, request, *args, **kwargs):
        colid = kwargs["colid"]

        change_ckeditor_storage(colid)

        return super().dispatch(request, **kwargs)


class CollectionBrowseView(EditorRequiredMixin, View):
    def dispatch(self, request, **kwargs):
        colid = kwargs["colid"]

        change_ckeditor_storage(colid)

        return browse(request)


file_upload_in_collection = csrf_exempt(CollectionImageUploadView.as_view())
file_browse_in_collection = csrf_exempt(CollectionBrowseView.as_view())


def deploy_cms(site, collection):
    colid = collection.pid
    base_url = getattr(collection, site)()

    if base_url is None:
        return JsonResponse({"message": "OK"})

    if site == "website":
        from_base_path = get_media_base_root_in_test(colid)
        to_base_path = get_media_base_root_in_prod(colid)

        for sub_path in ["uploads", "images"]:
            from_path = os.path.join(from_base_path, sub_path)
            to_path = os.path.join(to_base_path, sub_path)
            if os.path.exists(from_path):
                try:
                    shutil.copytree(from_path, to_path, dirs_exist_ok=True)
                except OSError as exception:
                    return HttpResponseServerError(f"Error during copy: {exception}")

    site_id = model_helpers.get_site_id(colid)
    if model_helpers.get_site_default_language(site_id):
        from modeltranslation import fields
        from modeltranslation import manager

        old_ftor = manager.get_language
        manager.get_language = monkey_get_language_en
        fields.get_language = monkey_get_language_en

        pages = get_pages_content(colid)
        news = get_news_content(colid)

        manager.get_language = old_ftor
        fields.get_language = old_ftor
    else:
        pages = get_pages_content(colid)
        news = get_news_content(colid)

    data = json.dumps({"pages": json.loads(pages), "news": json.loads(news)})
    url = getattr(collection, site)() + "/import_cms/"

    try:
        response = requests.put(url, data=data, verify=False)

        if response.status_code == 503:
            e = ServerUnderMaintenance(
                "The journal test website is under maintenance. Please try again later."
            )
            return HttpResponseServerError(e, status=503)

    except Timeout as exception:
        return HttpResponse(exception, status=408)
    except Exception as exception:
        return HttpResponseServerError(exception)

    return JsonResponse({"message": "OK"})


class HandleCMSMixin(EditorRequiredMixin):
    """
    Mixin for classes that need to send request to (test) website to import/export CMS content (pages, news)
    """

    # def dispatch(self, request, *args, **kwargs):
    #     self.colid = self.kwargs["colid"]
    #     return super().dispatch(request, *args, **kwargs)

    def init_data(self, kwargs):
        self.collection = None

        self.colid = kwargs.get("colid", None)
        if self.colid:
            self.collection = model_helpers.get_collection(self.colid)
            if not self.collection:
                raise Http404(f"{self.colid} does not exist")

        test_server_url = self.collection.test_website()
        if not test_server_url:
            raise Http404("The collection has no test site")

        prod_server_url = self.collection.website()
        if not prod_server_url:
            raise Http404("The collection has no prod site")


class GetCMSFromSiteAPIView(HandleCMSMixin, View):
    """
    Get the CMS content from the (test) website and save it on disk.
    It can be used if needed to restore the Trammel content with RestoreCMSAPIView below
    """

    def get(self, request, *args, **kwargs):
        self.init_data(self.kwargs)

        site = kwargs.get("site", "test_website")

        try:
            url = getattr(self.collection, site)() + "/export_cms/"
            response = requests.get(url, verify=False)

            # Just to need to save the json on disk
            # Media files are already saved in MEDIA_ROOT which is equal to
            # /mersenne_test_data/@colid/media
            folder = get_media_base_root(self.colid)
            os.makedirs(folder, exist_ok=True)
            filename = os.path.join(folder, f"pages_{self.colid}.json")
            with open(filename, mode="w", encoding="utf-8") as file:
                file.write(response.content.decode(encoding="utf-8"))

        except Timeout as exception:
            return HttpResponse(exception, status=408)
        except Exception as exception:
            return HttpResponseServerError(exception)

        return JsonResponse({"message": "OK", "status": 200})


def monkey_get_language_en():
    return "en"


class RestoreCMSAPIView(HandleCMSMixin, View):
    """
    Restore the Trammel CMS content (of a colid) from disk
    """

    def get(self, request, *args, **kwargs):
        self.init_data(self.kwargs)

        folder = get_media_base_root(self.colid)
        filename = os.path.join(folder, f"pages_{self.colid}.json")
        with open(filename, encoding="utf-8") as f:
            json_data = json.load(f)

        pages = json_data.get("pages")

        site_id = model_helpers.get_site_id(self.colid)
        if model_helpers.get_site_default_language(site_id):
            from modeltranslation import fields
            from modeltranslation import manager

            old_ftor = manager.get_language
            manager.get_language = monkey_get_language_en
            fields.get_language = monkey_get_language_en

            import_pages(pages, self.colid)

            manager.get_language = old_ftor
            fields.get_language = old_ftor
        else:
            import_pages(pages, self.colid)

        if "news" in json_data:
            news = json_data.get("news")
            import_news(news, self.colid)

        return JsonResponse({"message": "OK", "status": 200})


class DeployCMSAPIView(HandleCMSMixin, View):
    def get(self, request, *args, **kwargs):
        self.init_data(self.kwargs)

        if check_lock():
            msg = "Trammel is under maintenance. Please try again later."
            messages.error(self.request, msg)
            return JsonResponse({"messages": msg, "status": 503})

        site = kwargs.get("site", "test_website")

        response = deploy_cms(site, self.collection)

        if response.status_code == 503:
            messages.error(
                self.request, "The journal website is under maintenance. Please try again later."
            )

        return response


def get_server_urls(collection, site="test_website"):
    urls = [""]
    if hasattr(settings, "MERSENNE_DEV_URL"):
        # set RESOURCES_ROOT and apache config accordingly (for instance with "/mersenne_dev_data")
        url = getattr(collection, "test_website")().split(".fr")
        urls = [settings.MERSENNE_DEV_URL + url[1] if len(url) == 2 else ""]
    elif site == "both":
        urls = [getattr(collection, "test_website")(), getattr(collection, "website")()]
    elif hasattr(collection, site) and getattr(collection, site)():
        urls = [getattr(collection, site)()]
    return urls


class SuggestDeployView(EditorRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        doi = kwargs.get("doi", "")
        site = kwargs.get("site", "test_website")
        article = get_object_or_404(Article, doi=doi)

        obj, created = RelatedArticles.objects.get_or_create(resource=article)
        form = RelatedForm(request.POST or None, instance=obj)
        if form.is_valid():
            data = form.cleaned_data
            obj.date_modified = timezone.now()
            form.save()
            collection = article.my_container.my_collection
            urls = get_server_urls(collection, site=site)
            response = requests.models.Response()
            for url in urls:
                url = url + reverse("api-update-suggest", kwargs={"doi": doi})
                try:
                    response = requests.post(url, data=data, timeout=15)
                except requests.exceptions.RequestException as e:
                    response.status_code = 503
                    response.reason = e.args[0]
                    break
            return HttpResponse(status=response.status_code, reason=response.reason)
        else:
            return HttpResponseBadRequest()


def suggest_debug(results, article, message):
    crop_results = 5
    if results:
        dois = []
        results["docs"] = results["docs"][:crop_results]
        numFound = f'({len(results["docs"])} sur {results["numFound"]} documents)'
        head = f"Résultats de la recherche automatique {numFound} :\n\n"
        for item in results["docs"]:
            doi = item.get("doi")
            if doi:
                explain = results["explain"][item["id"]]
                terms = re.findall(r"([0-9.]+?) = weight\((.+?:.+?) in", explain)
                terms.sort(key=lambda t: t[0], reverse=True)
                details = (" + ").join(f"{round(float(s), 1)}:{t}" for s, t in terms)
                score = f'Score : {round(float(item["score"]), 1)} (= {details})\n'
                url = ""
                suggest = Article.objects.filter(doi=doi).first()
                if suggest and suggest.my_container:
                    collection = suggest.my_container.my_collection
                    base_url = collection.website() or ""
                    url = base_url + "/articles/" + doi
                dois.append((doi, url, score))

        tail = f'\n\nScore minimum retenu : {results["params"]["min_score"]}\n\n\n'
        tail += "Termes principaux utilisés pour la requête "
        tail = [tail + "(champ:terme recherché | pertinence du terme) :\n"]
        if results["params"]["mlt.fl"] == "all":
            tail.append(" * all = body + abstract + title + authors + keywords\n")
        terms = results["interestingTerms"]
        terms = [" | ".join((x[0], str(x[1]))) for x in zip(terms[::2], terms[1::2])]
        tail.extend(reversed(terms))
        tail.append("\n\nParamètres de la requête :\n")
        tail.extend([f"{k}: {v} " for k, v in results["params"].items()])
        return [(head, dois, "\n".join(tail))]
    else:
        msg = f'Erreur {message["status"]} {message["err"]} at {message["url"]}'
        return [(msg, [], "")]


class SuggestUpdateView(EditorRequiredMixin, TemplateView):
    template_name = "editorial_tools/suggested.html"

    def get_context_data(self, **kwargs):
        doi = kwargs.get("doi", "")
        article = get_object_or_404(Article, doi=doi)

        obj, created = RelatedArticles.objects.get_or_create(resource=article)
        collection = article.my_container.my_collection
        base_url = collection.website() or ""
        response = requests.models.Response()
        try:
            response = requests.get(base_url + "/mlt/" + doi, timeout=10.0)
        except requests.exceptions.RequestException as e:
            response.status_code = 503
            response.reason = e.args[0]
        msg = {
            "url": response.url,
            "status": response.status_code,
            "err": response.reason,
        }
        results = None
        if response.status_code == 200:
            results = solr_cmds.auto_suggest_doi(obj, article, response.json())
        context = super().get_context_data(**kwargs)
        context["debug"] = suggest_debug(results, article, msg)
        context["form"] = RelatedForm(instance=obj)
        context["author"] = "; ".join(get_names(article, "author"))
        context["citation_base"] = article.get_citation_base().strip(", .")
        context["article"] = article
        context["date_modified"] = obj.date_modified
        context["url"] = base_url + "/articles/" + doi
        return context


class EditorialToolsVolumeItemsView(EditorRequiredMixin, TemplateView):
    template_name = "editorial_tools/volume-items.html"

    def get_context_data(self, **kwargs):
        vid = kwargs.get("vid")
        site_name = settings.SITE_NAME if hasattr(settings, "SITE_NAME") else ""
        is_cr = len(site_name) == 6 and site_name[0:2] == "cr"
        issues_articles, collection = model_helpers.get_issues_in_volume(vid, is_cr)
        context = super().get_context_data(**kwargs)
        context["issues_articles"] = issues_articles
        context["collection"] = collection
        return context


class EditorialToolsArticleView(EditorRequiredMixin, TemplateView):
    template_name = "editorial_tools/find-article.html"

    def get_context_data(self, **kwargs):
        colid = kwargs.get("colid")
        doi = kwargs.get("doi")
        article = get_object_or_404(Article, doi=doi, my_container__my_collection__pid=colid)

        context = super().get_context_data(**kwargs)
        context["article"] = article
        context["citation_base"] = article.get_citation_base().strip(", .")
        return context


class GraphicalAbstractUpdateView(EditorRequiredMixin, TemplateView):
    template_name = "editorial_tools/graphical-abstract.html"

    def get_context_data(self, **kwargs):
        doi = kwargs.get("doi", "")
        article = get_object_or_404(Article, doi=doi)

        obj, created = GraphicalAbstract.objects.get_or_create(resource=article)
        context = super().get_context_data(**kwargs)
        context["author"] = "; ".join(get_names(article, "author"))
        context["citation_base"] = article.get_citation_base().strip(", .")
        context["article"] = article
        context["date_modified"] = obj.date_modified
        context["form"] = GraphicalAbstractForm(instance=obj)
        context["graphical_abstract"] = obj.graphical_abstract
        context["illustration"] = obj.illustration
        return context


class GraphicalAbstractDeployView(EditorRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        doi = kwargs.get("doi", "")
        site = kwargs.get("site", "both")
        article = get_object_or_404(Article, doi=doi)

        obj, created = GraphicalAbstract.objects.get_or_create(resource=article)
        form = GraphicalAbstractForm(request.POST, request.FILES or None, instance=obj)
        if form.is_valid():
            obj.date_modified = timezone.now()
            data = {"date_modified": obj.date_modified}
            form.save()
            files = {}
            if obj.graphical_abstract and os.path.exists(obj.graphical_abstract.path):
                with open(obj.graphical_abstract.path, "rb") as fp:
                    files.update({"graphical_abstract": (obj.graphical_abstract.name, fp.read())})
            if obj.illustration and os.path.exists(obj.illustration.path):
                with open(obj.illustration.path, "rb") as fp:
                    files.update({"illustration": (obj.illustration.name, fp.read())})
            collection = article.my_container.my_collection
            urls = get_server_urls(collection, site=site)
            response = requests.models.Response()
            for url in urls:
                url = url + reverse("api-graphical-abstract", kwargs={"doi": doi})
                try:
                    if not obj.graphical_abstract and not obj.illustration:
                        response = requests.delete(url, data=data, files=files, timeout=15)
                    else:
                        response = requests.post(url, data=data, files=files, timeout=15)
                except requests.exceptions.RequestException as e:
                    response.status_code = 503
                    response.reason = e.args[0]
                    break
            return HttpResponse(status=response.status_code, reason=response.reason)
        else:
            return HttpResponseBadRequest()


def parse_content(content):
    table = re.search(r'(.*?)(<table id="summary".+?</table>)(.*)', content, re.DOTALL)
    if not table:
        return {"head": content, "tail": "", "articles": []}

    articles = []
    rows = re.findall(r"<tr>.+?</tr>", table.group(2), re.DOTALL)
    for row in rows:
        citation = re.search(r'<div href=".*?">(.*?)</div>', row, re.DOTALL)
        href = re.search(r'href="(.+?)\/?">', row)
        doi = re.search(r"(10[.].+)", href.group(1)) if href else ""
        src = re.search(r'<img.+?src="(.+?)"', row)
        item = {}
        item["citation"] = citation.group(1) if citation else ""
        item["doi"] = doi.group(1) if doi else href.group(1) if href else ""
        item["src"] = src.group(1) if src else ""
        item["imageName"] = item["src"].split("/")[-1] if item["src"] else ""
        if item["doi"] or item["src"]:
            articles.append(item)
    return {"head": table.group(1), "tail": table.group(3), "articles": articles}


class VirtualIssueParseView(EditorRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        pid = kwargs.get("pid", "")
        page = get_object_or_404(Page, id=pid)

        data = {"pid": pid}
        data["colid"] = kwargs.get("colid", "")
        journal = model_helpers.get_collection(data["colid"])
        data["journal_title"] = journal.title_tex.replace(".", "")
        site_id = model_helpers.get_site_id(data["colid"])
        data["page"] = model_to_dict(page)
        pages = Page.objects.filter(site_id=site_id).exclude(id=pid)
        data["parents"] = [model_to_dict(p, fields=["id", "menu_title"]) for p in pages]

        content_fr = parse_content(page.content_fr)
        data["head_fr"] = content_fr["head"]
        data["tail_fr"] = content_fr["tail"]

        content_en = parse_content(page.content_en)
        data["articles"] = content_en["articles"]
        data["head_en"] = content_en["head"]
        data["tail_en"] = content_en["tail"]
        return JsonResponse(data)


class VirtualIssueUpdateView(EditorRequiredMixin, TemplateView):
    template_name = "editorial_tools/virtual-issue.html"

    def get(self, request, *args, **kwargs):
        pid = kwargs.get("pid", "")
        get_object_or_404(Page, id=pid)
        return super().get(request, *args, **kwargs)


class VirtualIssueCreateView(EditorRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        colid = kwargs.get("colid", "")
        site_id = model_helpers.get_site_id(colid)
        parent, _ = Page.objects.get_or_create(
            mersenne_id=MERSENNE_ID_VIRTUAL_ISSUES,
            parent_page=None,
            site_id=site_id,
        )
        page = Page.objects.create(
            menu_title_en="New virtual issue",
            menu_title_fr="Nouvelle collection transverse",
            parent_page=parent,
            site_id=site_id,
            state="draft",
        )
        kwargs = {"colid": colid, "pid": page.id}
        return HttpResponseRedirect(reverse("virtual_issue_update", kwargs=kwargs))


class SpecialIssuesIndex(EditorRequiredMixin, TemplateView):
    template_name = "editorial_tools/special-issues-index.html"

    def get_context_data(self, **kwargs):
        colid = kwargs.get("colid", "")

        context = super().get_context_data(**kwargs)
        context["colid"] = colid
        collection = Collection.objects.get(pid=colid)
        context["special_issues"] = Container.objects.filter(ctype="issue_special").filter(
            my_collection=collection
        )

        context["journal"] = model_helpers.get_collection(colid, sites=False)
        return context


class SpecialIssueEditView(EditorRequiredMixin, TemplateView):
    template_name = "editorial_tools/special-issue-edit.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class VirtualIssuesIndex(EditorRequiredMixin, TemplateView):
    template_name = "editorial_tools/virtual-issues-index.html"

    def get_context_data(self, **kwargs):
        colid = kwargs.get("colid", "")
        site_id = model_helpers.get_site_id(colid)
        vi = get_object_or_404(Page, mersenne_id=MERSENNE_ID_VIRTUAL_ISSUES)
        pages = Page.objects.filter(site_id=site_id, parent_page=vi)
        context = super().get_context_data(**kwargs)
        context["journal"] = model_helpers.get_collection(colid)
        context["pages"] = pages
        return context


def get_citation_fr(doi, citation_en):
    citation_fr = citation_en
    article = Article.objects.filter(doi=doi).first()
    if article and article.trans_title_html:
        trans_title = article.trans_title_html
        try:
            citation_fr = re.sub(
                r'(<a href="https:\/\/doi\.org.*">)([^<]+)',
                rf"\1{trans_title}",
                citation_en,
            )
        except re.error:
            pass
    return citation_fr


def summary_build(articles, colid):
    summary_fr = ""
    summary_en = ""
    head = '<table id="summary"><tbody>'
    tail = "</tbody></table>"
    style = "max-width:180px;max-height:200px"
    colid_lo = colid.lower()
    site_domain = SITE_REGISTER[colid_lo]["site_domain"].split("/")
    site_domain = "/" + site_domain[-1] if len(site_domain) == 2 else ""

    for article in articles:
        image_src = article.get("src", "")
        image_name = article.get("imageName", "")
        doi = article.get("doi", "")
        citation_en = article.get("citation", "")
        if doi or citation_en:
            row_fr = f'<div href="{doi}">{get_citation_fr(doi, citation_en)}</div>'
            row_en = f'<div href="{doi}">{citation_en}</div>'
            if image_src:
                date = datetime.now().strftime("%Y/%m/%d/")
                base_url = get_media_base_url(colid)
                suffix = os.path.join(base_url, "uploads", date)
                image_url = os.path.join(site_domain, suffix, image_name)
                image_header = "^data:image/.+;base64,"
                if re.match(image_header, image_src):
                    image_src = re.sub(image_header, "", image_src)
                    base64_data = base64.b64decode(image_src)
                    base_root = get_media_base_root(colid)
                    path = os.path.join(base_root, "uploads", date)
                    os.makedirs(path, exist_ok=True)
                    with open(path + image_name, "wb") as fp:
                        fp.write(base64_data)
                    im = f'<img src="{image_url}" style="{style}" />'
                # TODO mettre la vrai valeur pour le SITE_DOMAIN
                elif settings.SITE_DOMAIN == "http://127.0.0.1:8002":
                    im = f'<img src="{image_src}" style="{style}" />'
                else:
                    im = f'<img src="{site_domain}{image_src}" style="{style}" />'
                summary_fr += f"<tr><td>{im}</td><td>{row_fr}</td></tr>"
                summary_en += f"<tr><td>{im}</td><td>{row_en}</td></tr>"
    summary_fr = head + summary_fr + tail
    summary_en = head + summary_en + tail
    return {"summary_fr": summary_fr, "summary_en": summary_en}


# @method_decorator([csrf_exempt], name="dispatch")
class VirtualIssueDeployView(HandleCMSMixin, View):
    """
    called by the Virtual.vue VueJS component, when the virtual issue is saved
    We get data in JSON and we need to update the corresponding Page.
    The Page is then immediately posted to the test_website.
    The "Apply the changes to the production website" button is then used to update the (prod) website
       => See DeployCMSAPIView
    """

    def post(self, request, *args, **kwargs):
        self.init_data(self.kwargs)

        pid = kwargs.get("pid")
        colid = self.colid
        data = json.loads(request.body)
        summary = summary_build(data["articles"], colid)
        page = get_object_or_404(Page, id=pid)
        page.slug = page.slug_fr = page.slug_en = None
        page.menu_title_fr = data["title_fr"]
        page.menu_title_en = data["title_en"]
        page.content_fr = data["head_fr"] + summary["summary_fr"] + data["tail_fr"]
        page.content_en = data["head_en"] + summary["summary_en"] + data["tail_en"]
        page.state = data["page"]["state"]
        page.menu_order = data["page"]["menu_order"]
        page.parent_page = Page.objects.filter(id=data["page"]["parent_page"]).first()
        page.save()

        response = deploy_cms("test_website", self.collection)
        if response.status_code == 503:
            messages.error(
                self.request, "The journal website is under maintenance. Please try again later."
            )

        return response  # HttpResponse(status=response.status_code, reason=response.reason)


class SpecialIssueEditAPIView(HandleCMSMixin, TemplateView):
    template_name = "editorial_tools/special-issue-edit.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    def set_contrib_addresses(self, contrib, contribution):
        for address in contrib:
            contrib_address = ContribAddress(contribution=contribution, address=address)
            contrib_address.save()

    def delete(self, pk, colid):
        special_issue = Container.objects.get(id=pk)
        cmd = base_ptf_cmds.addContainerPtfCmd()
        cmd.set_object_to_be_deleted(special_issue)
        cmd.undo()

    def get(self, request, *args, **kwargs):
        pk = kwargs.get("pk", "")

        data = {"pk": pk}
        data["colid"] = kwargs.get("colid", "")

        name = resolve(request.path_info).url_name
        if name == "special_issue_delete":
            self.delete(pk, data["colid"])
            return redirect("special_issues_index", data["colid"])

        journal = model_helpers.get_collection(data["colid"], sites=False)
        data["journal_title"] = journal.title_tex.replace(".", "")

        if pk != "create":
            container = get_object_or_404(Container, id=pk)
            # TODO: pass the lang and trans_lang as well
            # In VueJS (Special.vu)e, titleFr = title_html
            data["title"] = container.trans_title_html
            data["trans_title"] = container.title_html
            data["year"] = container.year
            data["volume"] = container.volume
            data["articles"] = [
                {"doi": article.resource_doi, "citation": article.citation}
                for article in container.resources_in_special_issue.all()
            ]

            contribs = model_data_converter.db_to_contributors(container.contributions)
            data["contribs"] = contribs
        else:
            data["title"] = ""
            data["trans_title"] = ""
            data["year"] = ""
            data["volume"] = ""
            data["articles"] = []
            data["contribs"] = []

        # abstract_set = container.abstract_set.all()

        # try:
        #     data["head_fr"] = abstract_set.get(tag="head_fr").value_html
        # except:
        #     data["head_fr"] = ""

        # try:
        #     data["head_en"] = abstract_set.get(tag="head_en").value_html
        # except:
        #     data["head_en"] = ""

        # try:
        #     data["tail_fr"] = abstract_set.get(tag="tail_fr").value_html
        # except:
        #     data["tail_fr"] = ""
        # try:
        #     data["tail_en"] = abstract_set.get(tag="tail_en").value_html
        # except:
        #     data["tail_en"] = ""

        # try:
        #     ExtLink.objects.get(resource=container, rel="icon")

        #     icon_dirname = (
        #         settings.MERSENNE_TEST_DATA_FOLDER
        #         + "/media/"
        #         + data["colid"]
        #         + "/"
        #         + container.pid
        #     )
        #     # TODO mettre le settings.media_root
        #     icon_html_src = "/media" + "/" + data["colid"] + "/" + container.pid
        #     if os.path.isdir(icon_dirname):
        #         data["icon_name"] = os.listdir(icon_dirname)[0]

        #         for image in os.listdir(icon_dirname):
        #             data["icon_path"] = f"{icon_html_src}/{image}"
        # except ExtLink.DoesNotExist:
        #     pass

        # try:
        #     data["articles"] = parse_content(
        #         container.abstract_set.all().get(tag="summary_en").value_html
        #     )["articles"]
        # except:
        #     data["articles"] = ""

        return JsonResponse(data)

    def post(self, request, *args, **kwargs):
        # le but est de faire un IssueDAta
        pk = kwargs.get("pk", "")
        colid = kwargs.get("colid", "")
        journal = collection = model_helpers.get_collection(colid, sites=False)
        special_issue = IssueData()
        year = request.POST["year"]
        # TODO 1: the values should be the tex values, not the html ones
        # TODO 2: In VueJS, titleFr = title
        trans_title_html = request.POST["title"]
        title_html = request.POST["trans_title"]
        volume = collection.content.filter(year=year).first().volume
        if pk != "create":
            # TODO: do not use the pk, but the pid in the URLs
            container = get_object_or_404(Container, id=pk)
            lang = container.lang
            trans_lang = container.trans_lang
            xpub = create_publisherdata()
            xpub.name = container.my_publisher.pid
            special_issue.provider = container.provider
            special_issue.number = container.number
            resources_in_base = [
                resource_in_special_issue
                for resource_in_special_issue in container.resources_in_special_issue.all()
            ]

        else:
            lang = "en"
            trans_lang = "fr"
            xpub = create_publisherdata()
            xpub.name = collection.content.filter(year=year).first().my_publisher.pid
            special_issue.provider = collection.provider
            special_issue.number = (
                len(collection.content.filter(year=year).exclude(title_html="")) + 1
            )
            resources_in_base = []

        title_xml = get_issue_title_xml(title_html, lang, trans_title_html, trans_lang)

        special_issue_pid = f"{colid}_{year}__{volume}_S{special_issue.number}"

        existing_issue = model_helpers.get_resource(special_issue_pid)
        if pk == "create" and existing_issue is not None:
            raise ValueError(f"The special issue with the pid {special_issue_pid} already exists")

        special_issue.lang = lang
        special_issue.trans_lang = trans_lang
        special_issue.year = year
        special_issue.volume = volume
        special_issue.title_html = title_html
        special_issue.trans_title_html = trans_title_html
        special_issue.title_xml = title_xml
        special_issue.journal = journal
        special_issue.ctype = "issue_special"
        special_issue.publisher = xpub
        special_issue.pid = special_issue_pid
        special_issue.last_modified_iso_8601_date_str = datetime.now().strftime(
            "%Y-%m-%d %H:%M:%S"
        )

        articles = []
        contribs = []
        index = 0

        # contribs_in_base = container.contributions.all()

        if "nb_articles" in request.POST.keys():
            while index < int(request.POST["nb_articles"]):
                article = json.loads(request.POST[f"article[{index}]"])
                article["citation"] = xml_utils.replace_html_entities(article["citation"])
                articles.append(article)

                index += 1
            for resource in resources_in_base:
                resource.delete()

        # we delete resources that are no longer part of the special issue
        else:
            for resource in resources_in_base:
                resource.delete()
        special_issue.articles = [Munch(article) for article in articles]
        index = 0
        if "nb_contrib" in request.POST.keys():
            while index < int(request.POST["nb_contrib"]):
                contrib = json.loads(request.POST[f"contrib[{index}]"])
                contributor = create_contributor()
                contributor["first_name"] = contrib["first_name"]
                contributor["last_name"] = contrib["last_name"]
                contributor["orcid"] = contrib["orcid"]
                contributor["role"] = "editor"

                contrib_xml = xml_utils.get_contrib_xml(contrib)
                contributor["contrib_xml"] = contrib_xml
                contribs.append(Munch(contributor))
                index += 1
        special_issue.contributors = contribs

        #         try:
        #             contrib_in_special_issue = Contribution.objects.get(id=contrib["author_id"])
        #             contrib_in_special_issue.first_name = first_name
        #             contrib_in_special_issue.last_name = last_name
        #             contrib_in_special_issue.corresponding = corresponding
        #             contrib_in_special_issue.email = email
        #             contrib_in_special_issue.orcid = orcid
        #             contrib_in_special_issue.equal_contrib = equal_contrib
        #             contrib_in_special_issue.deceased_before_publication = deceased
        #             contrib_in_special_issue.seq = index
        #             contrib_in_special_issue.contrib_xml = contrib_xml
        #             contrib_in_special_issue.contribaddress_set.all().delete()
        #             contrib_in_special_issue.save()
        #             self.set_contrib_addresses(contrib["addresses"], contrib_in_special_issue)

        #             contribs_in_base = contribs_in_base.exclude(id=contrib_in_special_issue.id)
        #         except Contribution.DoesNotExist:
        #             contrib_in_special_issue = Contribution(
        #                 first_name=first_name,
        #                 last_name=last_name,
        #                 corresponding=corresponding,
        #                 email=email,
        #                 orcid=orcid,
        #                 role=role,
        #                 equal_contrib=equal_contrib,
        #                 deceased_before_publication=deceased,
        #                 seq=index,
        #                 resource=container,
        #                 contrib_xml=contrib_xml,
        #             )
        #             contrib_in_special_issue.save()
        #             self.set_contrib_addresses(contrib["addresses"], contrib_in_special_issue)

        #             contribs_in_base = contribs_in_base.exclude(id=contrib_in_special_issue.id)
        #         index += 1

        #     for contribution in contribs_in_base:
        #         contribution.delete()
        # else:
        #     for contrib in contribs_in_base:
        #         contrib.delete()
        # container.year = request.POST["year"]
        # container.title_html = request.POST["title"]
        # container.trans_title_html = request.POST["trans_title"]
        # container.title_xml = jats_parser.get_issue_title_xml(
        #     container.title_html, container.lang, container.trans_title_html, container.trans_lang
        # )
        # container.volume = request.POST["volume"]
        colid_lo = colid.lower()
        site_domain = SITE_REGISTER[colid_lo]["site_domain"].split("/")
        site_domain = "/" + site_domain[-1] if len(site_domain) == 2 else ""

        # if "IssuesIllustration" in self.request.FILES:
        #     icon_name = os.path.basename(self.request.FILES["IssuesIllustration"].name)
        #     file_extension = icon_name.split(".")[1]
        #     media = container.pid
        #     icon_file_path = resolver.get_disk_location(
        #         f"{settings.MEDIA_ROOT}",
        #         f"{collection.pid}",
        #         file_extension,
        #         media,
        #         None,
        #         True,
        #     )
        #     path = os.path.dirname(icon_file_path)
        #     if os.path.isdir(path):
        #         shutil.rmtree(path)
        #         os.mkdir(path)
        #     with open(icon_file_path, "wb+") as destination:
        #         for chunk in self.request.FILES["IssuesIllustration"].chunks():
        #             destination.write(chunk)
        #     icon = media + "." + file_extension
        #     location = location = f"{collection.pid}/{container.pid}/{icon}"
        #     try:
        #         extlink = ExtLink.objects.get(resource=container, rel="icon")
        #         extlink.location = location
        #     except ExtLink.DoesNotExist:
        #         extlink = ExtLink(
        #             resource=container,
        #             rel="icon",
        #             location=location,
        #         )
        #     extlink.save()
        # elif "icon_present" in request.POST:
        #     pass

        # else:
        #     try:
        #         location = settings.MEDIA_ROOT + collection.pid + "/" + container.pid
        #         if os.path.isdir(location):
        #             for icon in os.listdir(location):
        #                 os.remove(location + "/" + icon)
        #         extlink = ExtLink.objects.get(resource=container, rel="icon")
        #         extlink.delete()
        #     except ExtLink.DoesNotExist:
        #         pass

        # Part of the code that handle forwords and lastwords
        # head_fr_html = xml_utils.replace_html_entities(request.POST["head_fr"])
        # head_en_html = xml_utils.replace_html_entities(request.POST["head_en"])
        # tail_fr_html = xml_utils.replace_html_entities(request.POST["tail_fr"])
        # tail_en_html = xml_utils.replace_html_entities(request.POST["tail_en"])
        # try:
        #     head_fr = container.abstract_set.all().get(tag="head_fr")
        #     head_fr.value_html = head_fr_html
        #     head_fr.value_xml = (
        #         "<abstract xml:lang='fr'>"
        #         + head_fr.value_html.replace("<p>", "<p xml:space='preserve'>")
        #         + "</abstract>"
        #     )
        # except Abstract.DoesNotExist:
        #     head_fr = Abstract(
        #         resource=container, lang="fr", tag="head_fr", value_html=head_fr_html, seq=1
        #     )
        #     head_fr.value_xml = (
        #         "<abstract xml:lang='fr'>"
        #         + head_fr.value_html.replace("<p>", "<p xml:space='preserve'>")
        #         + "</abstract>"
        #     )
        # try:
        #     head_en = container.abstract_set.all().get(tag="head_en")
        #     head_en.value_html = head_en_html
        #     head_en.value_xml = (
        #         "<trans-abstract xml:lang='en'>"
        #         + head_en.value_html.replace("<p>", "<p xml:space='preserve'>")
        #         + "</abstract>"
        #     )
        # except Abstract.DoesNotExist:
        #     head_en = Abstract(
        #         resource=container, lang="en", tag="head_en", value_html=head_en_html, seq=2
        #     )
        #     head_en.value_xml = (
        #         "<trans-abstract xml:lang='en'>"
        #         + head_en.value_html.replace("<p>", "<p xml:space='preserve'>")
        #         + "</abstract>"
        #     )
        # try:
        #     tail_fr = container.abstract_set.all().get(tag="tail_fr")
        #     tail_fr.value_html = tail_fr_html
        #     tail_fr.value_xml = (
        #         "<abstract xml:lang='fr' abstract-type='tail_fr'>"
        #         + tail_fr.value_html.replace("<p>", "<p xml:space='preserve'>")
        #         + "</abstract>"
        #     )
        # except Abstract.DoesNotExist:
        #     tail_fr = Abstract(
        #         resource=container, lang="fr", tag="tail_fr", value_html=tail_fr_html, seq=5
        #     )
        #     tail_fr.value_xml = (
        #         "<abstract xml:lang='fr' abstract-type='tail_fr'>"
        #         + tail_fr.value_html.replace("<p>", "<p xml:space='preserve'>")
        #         + "</abstract>"
        #     )
        # try:
        #     tail_en = container.abstract_set.all().get(tag="tail_en")
        #     tail_en.value_html = tail_en_html
        #     tail_en.value_xml = (
        #         "<abstract xml:lang='en' abstract-type='tail_en'>"
        #         + tail_en.value_html.replace("<p>", "<p xml:space='preserve'>")
        #         + "</abstract>"
        #     )
        # except Abstract.DoesNotExist:
        #     tail_en = Abstract(
        #         resource=container, lang="en", tag="tail_en", value_html=tail_en_html, seq=6
        #     )
        #     tail_en.value_xml = (
        #         "<abstract xml:lang='en' abstract-type='tail_en'>"
        #         + tail_en.value_html.replace("<p>", "<p xml:space='preserve'>")
        #         + "</abstract>"
        #     )

        # we build the summary with articles provided
        # print(request.POST)
        # summary = summary_build(articles, colid)
        # try:
        #     container_summary_fr = container.abstract_set.all().get(tag="summary_fr")
        #     container_summary_fr.value_html = summary["summary_fr"]
        # except Abstract.DoesNotExist:
        #     container_summary_fr = Abstract(
        #         resource=container,
        #         lang="fr",
        #         tag="summary_fr",
        #         value_html=summary["summary_fr"],
        #         seq=3,
        #     )
        # try:
        #     container_summary_en = container.abstract_set.all().get(tag="summary_en")
        #     container_summary_en.value_html = summary["summary_en"]
        # except Abstract.DoesNotExist:
        #     container_summary_en = Abstract(
        #         resource=container,
        #         lang="en",
        #         tag="summary_en",
        #         value_html=summary["summary_en"],
        #         seq=4,
        #     )

        # head_fr.save()
        # head_en.save()
        # container_summary_fr.save()
        # container_summary_en.save()
        # tail_fr.save()
        # tail_en.save()

        # container.save()
        # special_issue = model_data_converter.db_to_issue_data(special_issue)
        special_issue = Munch(special_issue.__dict__)
        params = {"xissue": special_issue, "use_body": False}
        cmd = xml_cmds.addOrUpdateIssueXmlCmd(params)
        new_container = cmd.do()
        pk = new_container.pk
        return redirect("special_issue_edit_api", colid, pk)


class PageIndexView(EditorRequiredMixin, TemplateView):
    template_name = "mersenne_cms/page_index.html"

    def get_context_data(self, **kwargs):
        colid = kwargs.get("colid", "")
        site_id = model_helpers.get_site_id(colid)
        vi = Page.objects.filter(site_id=site_id, mersenne_id=MERSENNE_ID_VIRTUAL_ISSUES).first()
        if vi:
            pages = Page.objects.filter(site_id=site_id).exclude(parent_page=vi)
        else:
            pages = Page.objects.filter(site_id=site_id)
        context = super().get_context_data(**kwargs)
        context["colid"] = colid
        context["journal"] = model_helpers.get_collection(colid)
        context["pages"] = pages
        context["news"] = News.objects.filter(site_id=site_id)
        context["fields_lang"] = "fr" if model_helpers.is_site_fr_only(site_id) else "en"
        return context


class PageBaseView(HandleCMSMixin, View):
    template_name = "mersenne_cms/page_form.html"
    model = Page
    form_class = PageForm

    def dispatch(self, request, *args, **kwargs):
        self.colid = self.kwargs["colid"]
        self.collection = model_helpers.get_collection(self.colid, sites=False)
        self.site_id = model_helpers.get_site_id(self.colid)

        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse("page_index", kwargs={"colid": self.colid})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["journal"] = self.collection
        return context

    def update_test_website(self):
        response = deploy_cms("test_website", self.collection)
        if response.status_code < 300:
            messages.success(self.request, "The test website has been updated")
        else:
            text = "ERROR: Unable to update the test website<br/>"

            if response.status_code == 503:
                text += "The test website is under maintenance. Please try again later.<br/>"
            else:
                text += f"Please contact the centre Mersenne<br/><br/>Status code: {response.status_code}<br/>"
                if hasattr(response, "content") and response.content:
                    text += f"{response.content.decode()}<br/>"
                if hasattr(response, "reason") and response.reason:
                    text += f"Reason: {response.reason}<br/>"
                if hasattr(response, "text") and response.text:
                    text += f"Details: {response.text}<br/>"
            messages.error(self.request, mark_safe(text))

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["site_id"] = self.site_id
        kwargs["user"] = self.request.user
        return kwargs

    def form_valid(self, form):
        form.save()

        self.update_test_website()

        return HttpResponseRedirect(self.get_success_url())


#  @method_decorator([csrf_exempt], name="dispatch")
class PageDeleteView(PageBaseView):
    def post(self, request, *args, **kwargs):
        colid = kwargs.get("colid", "")
        pk = kwargs.get("pk")
        page = get_object_or_404(Page, id=pk)
        if page.mersenne_id:
            raise PermissionDenied

        page.delete()

        self.update_test_website()

        if page.parent_page and page.parent_page.mersenne_id == MERSENNE_ID_VIRTUAL_ISSUES:
            return HttpResponseRedirect(reverse("virtual_issues_index", kwargs={"colid": colid}))
        else:
            return HttpResponseRedirect(reverse("page_index", kwargs={"colid": colid}))


class PageCreateView(PageBaseView, CreateView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Add a menu page"
        return context


class PageUpdateView(PageBaseView, UpdateView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Edit a menu page"
        return context


class NewsBaseView(PageBaseView):
    template_name = "mersenne_cms/news_form.html"
    model = News
    form_class = NewsForm


class NewsDeleteView(NewsBaseView):
    def post(self, request, *args, **kwargs):
        pk = kwargs.get("pk")
        news = get_object_or_404(News, id=pk)

        news.delete()

        self.update_test_website()

        return HttpResponseRedirect(self.get_success_url())


class NewsCreateView(NewsBaseView, CreateView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Add a News"
        return context


class NewsUpdateView(NewsBaseView, UpdateView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Edit a News"
        return context


# def page_create_view(request, colid):
#     context = {}
#     if not is_authorized_editor(request.user, colid):
#         raise PermissionDenied
#     collection = model_helpers.get_collection(colid)
#     page = Page(site_id=model_helpers.get_site_id(colid))
#     form = PageForm(request.POST or None, instance=page)
#     if form.is_valid():
#         form.save()
#         response = deploy_cms("test_website", collection)
#         if response.status_code < 300:
#             messages.success(request, "Page created successfully.")
#         else:
#             text = f"ERROR: page creation failed<br/>Status code: {response.status_code}<br/>"
#             if hasattr(response, "reason") and response.reason:
#                 text += f"Reason: {response.reason}<br/>"
#             if hasattr(response, "text") and response.text:
#                 text += f"Details: {response.text}<br/>"
#             messages.error(request, mark_safe(text))
#         kwargs = {"colid": colid, "pid": form.instance.id}
#         return HttpResponseRedirect(reverse("page_update", kwargs=kwargs))
#
#     context["form"] = form
#     context["title"] = "Add a menu page"
#     context["journal"] = collection
#     return render(request, "mersenne_cms/page_form.html", context)


# def page_update_view(request, colid, pid):
#     context = {}
#     if not is_authorized_editor(request.user, colid):
#         raise PermissionDenied
#
#     collection = model_helpers.get_collection(colid)
#     page = get_object_or_404(Page, id=pid)
#     form = PageForm(request.POST or None, instance=page)
#     if form.is_valid():
#         form.save()
#         response = deploy_cms("test_website", collection)
#         if response.status_code < 300:
#             messages.success(request, "Page updated successfully.")
#         else:
#             text = f"ERROR: page update failed<br/>Status code: {response.status_code}<br/>"
#             if hasattr(response, "reason") and response.reason:
#                 text += f"Reason: {response.reason}<br/>"
#             if hasattr(response, "text") and response.text:
#                 text += f"Details: {response.text}<br/>"
#             messages.error(request, mark_safe(text))
#         kwargs = {"colid": colid, "pid": form.instance.id}
#         return HttpResponseRedirect(reverse("page_update", kwargs=kwargs))
#
#     context["form"] = form
#     context["pid"] = pid
#     context["title"] = "Edit a menu page"
#     context["journal"] = collection
#     return render(request, "mersenne_cms/page_form.html", context)
