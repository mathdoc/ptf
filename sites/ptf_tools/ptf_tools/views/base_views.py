import io
import json
import os
import re
from datetime import datetime
from itertools import groupby

import jsonpickle
import requests
from braces.views import CsrfExemptMixin
from braces.views import LoginRequiredMixin
from braces.views import StaffuserRequiredMixin
from celery import Celery
from celery import current_app
from django_celery_results.models import TaskResult
from extra_views import CreateWithInlinesView
from extra_views import InlineFormSetFactory
from extra_views import NamedFormsetsMixin
from extra_views import UpdateWithInlinesView
from requests import Timeout

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import UserPassesTestMixin
from django.db.models import Q
from django.http import Http404
from django.http import HttpRequest
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import HttpResponseServerError
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.shortcuts import render
from django.urls import resolve
from django.urls import reverse
from django.urls import reverse_lazy
from django.utils import timezone
from django.views.decorators.http import require_http_methods
from django.views.generic import ListView
from django.views.generic import TemplateView
from django.views.generic import View
from django.views.generic.base import RedirectView
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.edit import CreateView
from django.views.generic.edit import DeleteView
from django.views.generic.edit import FormView
from django.views.generic.edit import UpdateView

from comments_moderation.utils import get_comments_for_home
from comments_moderation.utils import is_comment_moderator
from history import models as history_models
from history import views as history_views
from ptf import model_data_converter
from ptf import model_helpers
from ptf import tex
from ptf import utils
from ptf.cmds import ptf_cmds
from ptf.cmds import xml_cmds
from ptf.cmds.base_cmds import make_int
from ptf.cmds.xml.jats.builder.issue import get_issue_title_xml
from ptf.cmds.xml.xml_utils import replace_html_entities
from ptf.display import resolver
from ptf.exceptions import DOIException
from ptf.exceptions import PDFException
from ptf.exceptions import ServerUnderMaintenance
from ptf.model_data import create_issuedata
from ptf.model_data import create_publisherdata
from ptf.models import Abstract
from ptf.models import Article
from ptf.models import BibItem
from ptf.models import BibItemId
from ptf.models import Collection
from ptf.models import Container
from ptf.models import ExtId
from ptf.models import ExtLink
from ptf.models import Resource
from ptf.models import ResourceId
from ptf.views import ArticleEditAPIView
from ptf_tools.doaj import doaj_pid_register
from ptf_tools.doi import get_or_create_doibatch
from ptf_tools.doi import recordDOI
from ptf_tools.forms import BibItemIdForm
from ptf_tools.forms import CollectionForm
from ptf_tools.forms import ContainerForm
from ptf_tools.forms import DiffContainerForm
from ptf_tools.forms import ExtIdForm
from ptf_tools.forms import ExtLinkForm
from ptf_tools.forms import FormSetHelper
from ptf_tools.forms import ImportArticleForm
from ptf_tools.forms import ImportContainerForm
from ptf_tools.forms import PtfFormHelper
from ptf_tools.forms import PtfLargeModalFormHelper
from ptf_tools.forms import PtfModalFormHelper
from ptf_tools.forms import RegisterPubmedForm
from ptf_tools.forms import ResourceIdForm
from ptf_tools.forms import get_article_choices
from ptf_tools.models import ResourceInNumdam
from ptf_tools.tasks import archive_numdam_collection
from ptf_tools.tasks import archive_numdam_issue
from ptf_tools.tasks import archive_trammel_collection
from ptf_tools.tasks import archive_trammel_resource
from ptf_tools.templatetags.tools_helpers import get_authorized_collections
from ptf_tools.utils import is_authorized_editor
from pubmed.views import recordPubmed


def view_404(request: HttpRequest):
    """
    Dummy view raising HTTP 404 exception.
    """
    raise Http404


def check_collection(collection, server_url, server_type):
    """
    Check if a collection exists on a serveur (test/prod)
    and upload the collection (XML, image) if necessary
    """

    url = server_url + reverse("collection_status", kwargs={"colid": collection.pid})
    response = requests.get(url, verify=False)
    # First, upload the collection XML
    xml = ptf_cmds.exportPtfCmd({"pid": collection.pid}).do()
    body = xml.encode("utf8")

    url = server_url + reverse("upload-serials")
    if response.status_code == 200:
        # PUT http verb is used for update
        response = requests.put(url, data=body, verify=False)
    else:
        # POST http verb is used for creation
        response = requests.post(url, data=body, verify=False)

    # Second, copy the collection images
    # There is no need to copy files for the test server
    # Files were already copied in /mersenne_test_data during the ptf_tools import
    # We only need to copy files from /mersenne_test_data to
    # /mersenne_prod_data during an upload to prod
    if server_type == "website":
        resolver.copy_binary_files(
            collection, settings.MERSENNE_TEST_DATA_FOLDER, settings.MERSENNE_PROD_DATA_FOLDER
        )
    elif server_type == "numdam":
        from_folder = settings.MERSENNE_PROD_DATA_FOLDER
        if collection.pid in settings.NUMDAM_COLLECTIONS:
            from_folder = settings.MERSENNE_TEST_DATA_FOLDER

        resolver.copy_binary_files(collection, from_folder, settings.NUMDAM_DATA_ROOT)


def check_lock():
    return hasattr(settings, "LOCK_FILE") and os.path.isfile(settings.LOCK_FILE)


def load_cedrics_article_choices(request):
    colid = request.GET.get("colid")
    issue = request.GET.get("issue")
    article_choices = get_article_choices(colid, issue)
    return render(
        request, "cedrics_article_dropdown_list_options.html", {"article_choices": article_choices}
    )


class ImportCedricsArticleFormView(FormView):
    template_name = "import_article.html"
    form_class = ImportArticleForm

    def dispatch(self, request, *args, **kwargs):
        self.colid = self.kwargs["colid"]
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        if self.colid:
            return reverse("collection-detail", kwargs={"pid": self.colid})
        return "/"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["colid"] = self.colid
        context["helper"] = PtfModalFormHelper
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["colid"] = self.colid
        return kwargs

    def form_valid(self, form):
        self.issue = form.cleaned_data["issue"]
        self.article = form.cleaned_data["article"]
        return super().form_valid(form)

    def import_cedrics_article(self, *args, **kwargs):
        cmd = xml_cmds.addorUpdateCedricsArticleXmlCmd(
            {"container_pid": self.issue_pid, "article_folder_name": self.article_pid}
        )
        cmd.do()

    def post(self, request, *args, **kwargs):
        self.colid = self.kwargs.get("colid", None)
        issue = request.POST["issue"]
        self.article_pid = request.POST["article"]
        self.issue_pid = os.path.basename(os.path.dirname(issue))

        import_args = [self]
        import_kwargs = {}

        try:
            _, status, message = history_views.execute_and_record_func(
                "import",
                f"{self.issue_pid} / {self.article_pid}",
                self.colid,
                self.import_cedrics_article,
                "",
                False,
                *import_args,
                **import_kwargs,
            )

            messages.success(
                self.request, f"L'article {self.article_pid} a été importé avec succès"
            )

        except Exception as exception:
            messages.error(
                self.request,
                f"Echec de l'import de l'article {self.article_pid} : {str(exception)}",
            )

        return redirect(self.get_success_url())


class ImportCedricsIssueView(FormView):
    template_name = "import_container.html"
    form_class = ImportContainerForm

    def dispatch(self, request, *args, **kwargs):
        self.colid = self.kwargs["colid"]
        self.to_appear = self.request.GET.get("to_appear", False)
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        if self.filename:
            return reverse(
                "diff_cedrics_issue", kwargs={"colid": self.colid, "filename": self.filename}
            )
        return "/"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["colid"] = self.colid
        context["helper"] = PtfModalFormHelper
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["colid"] = self.colid
        kwargs["to_appear"] = self.to_appear
        return kwargs

    def form_valid(self, form):
        self.filename = form.cleaned_data["filename"].split("/")[-1]
        return super().form_valid(form)


class DiffCedricsIssueView(FormView):
    template_name = "diff_container_form.html"
    form_class = DiffContainerForm
    diffs = None
    xissue = None
    xissue_encoded = None

    def get_success_url(self):
        return reverse("collection-detail", kwargs={"pid": self.colid})

    def dispatch(self, request, *args, **kwargs):
        self.colid = self.kwargs["colid"]
        #        self.filename = self.kwargs['filename']
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        self.filename = request.GET["filename"]
        self.remove_mail = request.GET["remove_email"]
        self.remove_date_prod = request.GET["remove_date_prod"]

        try:
            result, status, message = history_views.execute_and_record_func(
                "import",
                os.path.basename(self.filename),
                self.colid,
                self.diff_cedrics_issue,
                "",
                True,
            )
        except Exception as exception:
            pid = self.filename.split("/")[-1]
            messages.error(self.request, f"Echec de l'import du volume {pid} : {exception}")
            return HttpResponseRedirect(self.get_success_url())

        no_conflict = result[0]
        self.diffs = result[1]
        self.xissue = result[2]

        if no_conflict:
            # Proceed with the import
            self.form_valid(self.get_form())
            return redirect(self.get_success_url())
        else:
            # Display the diff template
            self.xissue_encoded = jsonpickle.encode(self.xissue)

            return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.filename = request.POST["filename"]
        data = request.POST["xissue_encoded"]
        self.xissue = jsonpickle.decode(data)

        return super().post(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["colid"] = self.colid
        context["diff"] = self.diffs
        context["filename"] = self.filename
        context["xissue_encoded"] = self.xissue_encoded
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["colid"] = self.colid
        return kwargs

    def diff_cedrics_issue(self, *args, **kwargs):
        params = {
            "colid": self.colid,
            "input_file": self.filename,
            "remove_email": self.remove_mail,
            "remove_date_prod": self.remove_date_prod,
            "diff_only": True,
        }

        if settings.IMPORT_CEDRICS_DIRECTLY:
            params["is_seminar"] = self.colid in settings.MERSENNE_SEMINARS
            params["force_dois"] = self.colid not in settings.NUMDAM_COLLECTIONS
            cmd = xml_cmds.importCedricsIssueDirectlyXmlCmd(params)
        else:
            cmd = xml_cmds.importCedricsIssueXmlCmd(params)

        result = cmd.do()
        if len(cmd.warnings) > 0 and self.request.user.is_superuser:
            messages.warning(
                self.request, message="Balises non parsées lors de l'import : %s" % cmd.warnings
            )

        return result

    def import_cedrics_issue(self, *args, **kwargs):
        # modify xissue with data_issue if params to override
        if "import_choice" in kwargs and kwargs["import_choice"] == "1":
            issue = model_helpers.get_container(self.xissue.pid)
            if issue:
                data_issue = model_data_converter.db_to_issue_data(issue)
                for xarticle in self.xissue.articles:
                    filter_articles = [
                        article for article in data_issue.articles if article.doi == xarticle.doi
                    ]
                    if len(filter_articles) > 0:
                        db_article = filter_articles[0]
                        xarticle.coi_statement = db_article.coi_statement
                        xarticle.kwds = db_article.kwds
                        xarticle.contrib_groups = db_article.contrib_groups

        params = {
            "colid": self.colid,
            "xissue": self.xissue,
            "input_file": self.filename,
        }

        if settings.IMPORT_CEDRICS_DIRECTLY:
            params["is_seminar"] = self.colid in settings.MERSENNE_SEMINARS
            params["add_body_html"] = self.colid not in settings.NUMDAM_COLLECTIONS
            cmd = xml_cmds.importCedricsIssueDirectlyXmlCmd(params)
        else:
            cmd = xml_cmds.importCedricsIssueXmlCmd(params)

        cmd.do()

    def form_valid(self, form):
        if "import_choice" in self.kwargs and self.kwargs["import_choice"] == "1":
            import_kwargs = {"import_choice": form.cleaned_data["import_choice"]}
        else:
            import_kwargs = {}
        import_args = [self]

        try:
            _, status, message = history_views.execute_and_record_func(
                "import",
                self.xissue.pid,
                self.kwargs["colid"],
                self.import_cedrics_issue,
                "",
                False,
                *import_args,
                **import_kwargs,
            )
        except Exception as exception:
            messages.error(
                self.request, f"Echec de l'import du volume {self.xissue.pid} : " + str(exception)
            )
            return super().form_invalid(form)

        messages.success(self.request, f"Le volume {self.xissue.pid} a été importé avec succès")
        return super().form_valid(form)


class BibtexAPIView(View):
    def get(self, request, *args, **kwargs):
        pid = self.kwargs.get("pid", None)
        all_bibtex = ""
        if pid:
            article = model_helpers.get_article(pid)
            if article:
                for bibitem in article.bibitem_set.all():
                    bibtex_array = bibitem.get_bibtex()
                    last = len(bibtex_array)
                    i = 1
                    for bibtex in bibtex_array:
                        if i > 1 and i < last:
                            all_bibtex += "    "
                        all_bibtex += bibtex + "\n"
                        i += 1

        data = {"bibtex": all_bibtex}
        return JsonResponse(data)


class MatchingAPIView(View):
    def get(self, request, *args, **kwargs):
        pid = self.kwargs.get("pid", None)

        url = settings.MATCHING_URL
        headers = {"Content-Type": "application/xml"}

        body = ptf_cmds.exportPtfCmd({"pid": pid, "with_body": False}).do()

        if settings.DEBUG:
            print("Issue exported to /tmp/issue.xml")
            f = open("/tmp/issue.xml", "w")
            f.write(body.encode("utf8"))
            f.close()

        r = requests.post(url, data=body.encode("utf8"), headers=headers)
        body = r.text.encode("utf8")
        data = {"status": r.status_code, "message": body[:1000]}

        if settings.DEBUG:
            print("Matching received, new issue exported to /tmp/issue1.xml")
            f = open("/tmp/issue1.xml", "w")
            text = body
            f.write(text)
            f.close()

        resource = model_helpers.get_resource(pid)
        obj = resource.cast()
        colid = obj.get_collection().pid

        full_text_folder = settings.CEDRAM_XML_FOLDER + colid + "/plaintext/"

        cmd = xml_cmds.addOrUpdateIssueXmlCmd(
            {"body": body, "assign_doi": True, "full_text_folder": full_text_folder}
        )
        cmd.do()

        print("Matching finished")
        return JsonResponse(data)


class ImportAllAPIView(View):
    def internal_do(self, *args, **kwargs):
        pid = self.kwargs.get("pid", None)

        root_folder = os.path.join(settings.MATHDOC_ARCHIVE_FOLDER, pid)
        if not os.path.isdir(root_folder):
            raise ValueError(root_folder + " does not exist")

        resource = model_helpers.get_resource(pid)
        if not resource:
            file = os.path.join(root_folder, pid + ".xml")
            body = utils.get_file_content_in_utf8(file)
            journals = xml_cmds.addCollectionsXmlCmd(
                {
                    "body": body,
                    "from_folder": settings.MATHDOC_ARCHIVE_FOLDER,
                    "to_folder": settings.MERSENNE_TEST_DATA_FOLDER,
                }
            ).do()
            if not journals:
                raise ValueError(file + " does not contain a collection")
            resource = journals[0]
            # resolver.copy_binary_files(
            #     resource,
            #     settings.MATHDOC_ARCHIVE_FOLDER,
            #     settings.MERSENNE_TEST_DATA_FOLDER)

        obj = resource.cast()

        if obj.classname != "Collection":
            raise ValueError(pid + " does not contain a collection")

        cmd = xml_cmds.collectEntireCollectionXmlCmd(
            {"pid": pid, "folder": settings.MATHDOC_ARCHIVE_FOLDER}
        )
        pids = cmd.do()

        return pids

    def get(self, request, *args, **kwargs):
        pid = self.kwargs.get("pid", None)

        try:
            pids, status, message = history_views.execute_and_record_func(
                "import", pid, pid, self.internal_do
            )
        except Timeout as exception:
            return HttpResponse(exception, status=408)
        except Exception as exception:
            return HttpResponseServerError(exception)

        data = {"message": message, "ids": pids, "status": status}
        return JsonResponse(data)


class DeployAllAPIView(View):
    def internal_do(self, *args, **kwargs):
        pid = self.kwargs.get("pid", None)
        site = self.kwargs.get("site", None)

        pids = []

        collection = model_helpers.get_collection(pid)
        if not collection:
            raise RuntimeError(pid + " does not exist")

        if site == "numdam":
            server_url = settings.NUMDAM_PRE_URL
        elif site != "ptf_tools":
            server_url = getattr(collection, site)()
            if not server_url:
                raise RuntimeError("The collection has no " + site)

        if site != "ptf_tools":
            # check if the collection exists on the server
            # if not, check_collection will upload the collection (XML,
            # image...)
            check_collection(collection, server_url, site)

        for issue in collection.content.all():
            if site != "website" or (site == "website" and issue.are_all_articles_published()):
                pids.append(issue.pid)

        return pids

    def get(self, request, *args, **kwargs):
        pid = self.kwargs.get("pid", None)
        site = self.kwargs.get("site", None)

        try:
            pids, status, message = history_views.execute_and_record_func(
                "deploy", pid, pid, self.internal_do, site
            )
        except Timeout as exception:
            return HttpResponse(exception, status=408)
        except Exception as exception:
            return HttpResponseServerError(exception)

        data = {"message": message, "ids": pids, "status": status}
        return JsonResponse(data)


class AddIssuePDFView(View):
    def __init(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.pid = None
        self.issue = None
        self.collection = None
        self.site = "test_website"

    def post_to_site(self, url):
        response = requests.post(url, verify=False)
        status = response.status_code
        if not (199 < status < 205):
            messages.error(self.request, response.text)
            if status == 503:
                raise ServerUnderMaintenance(response.text)
            else:
                raise RuntimeError(response.text)

    def internal_do(self, *args, **kwargs):
        """
        Called by history_views.execute_and_record_func to do the actual job.
        """

        issue_pid = self.issue.pid
        colid = self.collection.pid

        if self.site == "website":
            # Copy the PDF from the test to the production folder
            resolver.copy_binary_files(
                self.issue, settings.MERSENNE_TEST_DATA_FOLDER, settings.MERSENNE_PROD_DATA_FOLDER
            )
        else:
            # Copy the PDF from the cedram to the test folder
            from_folder = resolver.get_cedram_issue_tex_folder(colid, issue_pid)
            from_path = os.path.join(from_folder, issue_pid + ".pdf")
            if not os.path.isfile(from_path):
                raise Http404(f"{from_path} does not exist")

            to_path = resolver.get_disk_location(
                settings.MERSENNE_TEST_DATA_FOLDER, colid, "pdf", issue_pid
            )
            resolver.copy_file(from_path, to_path)

        url = reverse("issue_pdf_upload", kwargs={"pid": self.issue.pid})

        if self.site == "test_website":
            # Post to ptf-tools: it will add a Datastream to the issue
            absolute_url = self.request.build_absolute_uri(url)
            self.post_to_site(absolute_url)

        server_url = getattr(self.collection, self.site)()
        absolute_url = server_url + url
        # Post to the test or production  website
        self.post_to_site(absolute_url)

    def get(self, request, *args, **kwargs):
        """
        Send an issue PDF to the test or production website
        :param request: pid (mandatory), site (optional) "test_website" (default) or 'website'
        :param args:
        :param kwargs:
        :return:
        """
        if check_lock():
            m = "Trammel is under maintenance. Please try again later."
            messages.error(self.request, m)
            return JsonResponse({"message": m, "status": 503})

        self.pid = self.kwargs.get("pid", None)
        self.site = self.kwargs.get("site", "test_website")

        self.issue = model_helpers.get_container(self.pid)
        if not self.issue:
            raise Http404(f"{self.pid} does not exist")
        self.collection = self.issue.get_top_collection()

        try:
            pids, status, message = history_views.execute_and_record_func(
                "deploy",
                self.pid,
                self.collection.pid,
                self.internal_do,
                f"add issue PDF to {self.site}",
            )

        except Timeout as exception:
            return HttpResponse(exception, status=408)
        except Exception as exception:
            return HttpResponseServerError(exception)

        data = {"message": message, "status": status}
        return JsonResponse(data)


class ArchiveAllAPIView(View):
    """
    - archive le xml de la collection ainsi que les binaires liés
    - renvoie une liste de pid des issues de la collection qui seront ensuite archivés par appel JS
     @return array of issues pid
    """

    def internal_do(self, *args, **kwargs):
        collection = kwargs["collection"]
        pids = []
        colid = collection.pid

        logfile = os.path.join(settings.LOG_DIR, "archive.log")
        if os.path.isfile(logfile):
            os.remove(logfile)

        ptf_cmds.exportPtfCmd(
            {
                "pid": colid,
                "export_folder": settings.MATHDOC_ARCHIVE_FOLDER,
                "with_binary_files": True,
                "for_archive": True,
                "binary_files_folder": settings.MERSENNE_PROD_DATA_FOLDER,
            }
        ).do()

        cedramcls = os.path.join(settings.CEDRAM_TEX_FOLDER, "cedram.cls")
        if os.path.isfile(cedramcls):
            dest_folder = os.path.join(settings.MATHDOC_ARCHIVE_FOLDER, collection.pid, "src/tex")
            resolver.create_folder(dest_folder)
            resolver.copy_file(cedramcls, dest_folder)

        for issue in collection.content.all():
            qs = issue.article_set.filter(
                date_online_first__isnull=True, date_published__isnull=True
            )
            if qs.count() == 0:
                pids.append(issue.pid)

        return pids

    def get(self, request, *args, **kwargs):
        pid = self.kwargs.get("pid", None)

        collection = model_helpers.get_collection(pid)
        if not collection:
            return HttpResponse(f"{pid} does not exist", status=400)

        dict_ = {"collection": collection}
        args_ = [self]

        try:
            pids, status, message = history_views.execute_and_record_func(
                "archive", pid, pid, self.internal_do, "", False, *args_, **dict_
            )
        except Timeout as exception:
            return HttpResponse(exception, status=408)
        except Exception as exception:
            return HttpResponseServerError(exception)

        data = {"message": message, "ids": pids, "status": status}
        return JsonResponse(data)


class CreateAllDjvuAPIView(View):
    def internal_do(self, *args, **kwargs):
        issue = kwargs["issue"]
        pids = [issue.pid]

        for article in issue.article_set.all():
            pids.append(article.pid)

        return pids

    def get(self, request, *args, **kwargs):
        pid = self.kwargs.get("pid", None)
        issue = model_helpers.get_container(pid)
        if not issue:
            raise Http404(f"{pid} does not exist")

        try:
            dict_ = {"issue": issue}
            args_ = [self]

            pids, status, message = history_views.execute_and_record_func(
                "numdam",
                pid,
                issue.get_collection().pid,
                self.internal_do,
                "",
                False,
                *args_,
                **dict_,
            )
        except Exception as exception:
            return HttpResponseServerError(exception)

        data = {"message": message, "ids": pids, "status": status}
        return JsonResponse(data)


class ImportJatsContainerAPIView(View):
    def internal_do(self, *args, **kwargs):
        pid = self.kwargs.get("pid", None)
        colid = self.kwargs.get("colid", None)

        if pid and colid:
            body = resolver.get_archive_body(settings.MATHDOC_ARCHIVE_FOLDER, colid, pid)

            cmd = xml_cmds.addOrUpdateContainerXmlCmd(
                {
                    "body": body,
                    "from_folder": settings.MATHDOC_ARCHIVE_FOLDER,
                    "to_folder": settings.MERSENNE_TEST_DATA_FOLDER,
                    "backup_folder": settings.MATHDOC_ARCHIVE_FOLDER,
                }
            )
            container = cmd.do()
            if len(cmd.warnings) > 0:
                messages.warning(
                    self.request,
                    message="Balises non parsées lors de l'import : %s" % cmd.warnings,
                )

            if not container:
                raise RuntimeError("Error: the container " + pid + " was not imported")

        # resolver.copy_binary_files(
        #         container,
        #         settings.MATHDOC_ARCHIVE_FOLDER,
        #         settings.MERSENNE_TEST_DATA_FOLDER)
        #
        #     for article in container.article_set.all():
        #         resolver.copy_binary_files(
        #             article,
        #             settings.MATHDOC_ARCHIVE_FOLDER,
        #             settings.MERSENNE_TEST_DATA_FOLDER)
        else:
            raise RuntimeError("colid or pid are not defined")

    def get(self, request, *args, **kwargs):
        pid = self.kwargs.get("pid", None)
        colid = self.kwargs.get("colid", None)

        try:
            _, status, message = history_views.execute_and_record_func(
                "import", pid, colid, self.internal_do
            )
        except Timeout as exception:
            return HttpResponse(exception, status=408)
        except Exception as exception:
            return HttpResponseServerError(exception)

        data = {"message": message, "status": status}
        return JsonResponse(data)


class DeployCollectionAPIView(View):
    # Update collection.xml on a site (with its images)

    def internal_do(self, *args, **kwargs):
        colid = self.kwargs.get("colid", None)
        site = self.kwargs.get("site", None)

        collection = model_helpers.get_collection(colid)
        if not collection:
            raise RuntimeError(f"{colid} does not exist")

        if site == "numdam":
            server_url = settings.NUMDAM_PRE_URL
        else:
            server_url = getattr(collection, site)()
            if not server_url:
                raise RuntimeError(f"The collection has no {site}")

        # check_collection creates or updates the collection (XML, image...)
        check_collection(collection, server_url, site)

    def get(self, request, *args, **kwargs):
        colid = self.kwargs.get("colid", None)
        site = self.kwargs.get("site", None)

        try:
            _, status, message = history_views.execute_and_record_func(
                "deploy", colid, colid, self.internal_do, site
            )
        except Timeout as exception:
            return HttpResponse(exception, status=408)
        except Exception as exception:
            return HttpResponseServerError(exception)

        data = {"message": message, "status": status}
        return JsonResponse(data)


class DeployJatsResourceAPIView(View):
    # A RENOMMER aussi DeleteJatsContainerAPIView (mais fonctionne tel quel)

    def internal_do(self, *args, **kwargs):
        pid = self.kwargs.get("pid", None)
        colid = self.kwargs.get("colid", None)
        site = self.kwargs.get("site", None)

        if site == "ptf_tools":
            raise RuntimeError("Do not choose to deploy on PTF Tools")

        resource = model_helpers.get_resource(pid)
        if not resource:
            raise RuntimeError(f"{pid} does not exist")

        obj = resource.cast()
        article = None
        if obj.classname == "Article":
            article = obj
            container = article.my_container
            articles_to_deploy = [article]
        else:
            container = obj
            articles_to_deploy = container.article_set.exclude(do_not_publish=True)

        if site == "website" and article is not None and article.do_not_publish:
            raise RuntimeError(f"{pid} is marked as Do not publish")
        if site == "numdam" and article is not None:
            raise RuntimeError("You can only deploy issues to Numdam")

        collection = container.get_top_collection()
        colid = collection.pid
        djvu_exception = None

        if site == "numdam":
            server_url = settings.NUMDAM_PRE_URL
            ResourceInNumdam.objects.get_or_create(pid=container.pid)

            # 06/12/2022: DjVu are no longer added with Mersenne articles
            # Add Djvu (before exporting the XML)
            if False and int(container.year) < 2020:
                for art in container.article_set.all():
                    try:
                        cmd = ptf_cmds.addDjvuPtfCmd()
                        cmd.set_resource(art)
                        cmd.do()
                    except Exception as e:
                        # Djvu are optional.
                        # Allow the deployment, but record the exception in the history
                        djvu_exception = e
        else:
            server_url = getattr(collection, site)()
            if not server_url:
                raise RuntimeError(f"The collection has no {site}")

        # check if the collection exists on the server
        # if not, check_collection will upload the collection (XML,
        # image...)
        if article is None:
            check_collection(collection, server_url, site)

        with open(os.path.join(settings.LOG_DIR, "cmds.log"), "w", encoding="utf-8") as file_:
            # Create/update deployed date and published date on all container articles
            if site == "website":
                file_.write(
                    "Create/Update deployed_date and date_published on all articles for {}\n".format(
                        pid
                    )
                )

                # create date_published on articles without date_published (ou date_online_first pour le volume 0)
                cmd = ptf_cmds.publishResourcePtfCmd()
                cmd.set_resource(resource)
                updated_articles = cmd.do()

                tex.create_frontpage(colid, container, updated_articles, test=False)

                mersenneSite = model_helpers.get_site_mersenne(colid)
                # create or update deployed_date on container and articles
                model_helpers.update_deployed_date(obj, mersenneSite, None, file_)

                for art in articles_to_deploy:
                    if art.doi and (art.date_published or art.date_online_first):
                        if art.my_container.year is None:
                            art.my_container.year = datetime.now().strftime("%Y")
                            # BUG ? update the container but no save() ?

                    file_.write(
                        "Publication date of {} : Online First: {}, Published: {}\n".format(
                            art.pid, art.date_online_first, art.date_published
                        )
                    )

                if article is None:
                    resolver.copy_binary_files(
                        container,
                        settings.MERSENNE_TEST_DATA_FOLDER,
                        settings.MERSENNE_PROD_DATA_FOLDER,
                    )

                for art in articles_to_deploy:
                    resolver.copy_binary_files(
                        art,
                        settings.MERSENNE_TEST_DATA_FOLDER,
                        settings.MERSENNE_PROD_DATA_FOLDER,
                    )

            elif site == "test_website":
                # create date_pre_published on articles without date_pre_published
                cmd = ptf_cmds.publishResourcePtfCmd({"pre_publish": True})
                cmd.set_resource(resource)
                updated_articles = cmd.do()

                tex.create_frontpage(colid, container, updated_articles)

            export_to_website = site == "website"

            if article is None:
                with_djvu = site == "numdam"
                # if obj.ctype == 'issue_special':
                #
                xml = ptf_cmds.exportPtfCmd(
                    {
                        "pid": pid,
                        "with_djvu": with_djvu,
                        "export_to_website": export_to_website,
                    }
                ).do()
                body = xml.encode("utf8")

                if container.ctype == "issue" or container.ctype == "issue_special":
                    url = server_url + reverse("issue_upload")
                else:
                    url = server_url + reverse("book_upload")

                # verify=False: ignore TLS certificate
                response = requests.post(url, data=body, verify=False)
            else:
                xml = ptf_cmds.exportPtfCmd(
                    {
                        "pid": pid,
                        "with_djvu": False,
                        "article_standalone": True,
                        "collection_pid": collection.pid,
                        "export_to_website": export_to_website,
                        "export_folder": settings.LOG_DIR,
                    }
                ).do()
                # Unlike containers that send their XML as the body of the POST request,
                # articles send their XML as a file, because PCJ editor sends multiple files (XML, PDF, img)
                xml_file = io.StringIO(xml)
                files = {"xml": xml_file}

                url = server_url + reverse(
                    "article_in_issue_upload", kwargs={"pid": container.pid}
                )
                # verify=False: ignore TLS certificate
                header = {}
                response = requests.post(url, headers=header, files=files, verify=False)

            status = response.status_code

            if 199 < status < 205:
                # There is no need to copy files for the test server
                # Files were already copied in /mersenne_test_data during the ptf_tools import
                # We only need to copy files from /mersenne_test_data to
                # /mersenne_prod_data during an upload to prod
                if site == "website":
                    for art in articles_to_deploy:
                        # record DOI automatically when deploying in prod

                        if art.doi and art.allow_crossref():
                            recordDOI(art)

                        if colid == "CRBIOL":
                            recordPubmed(
                                art, force_update=False, updated_articles=updated_articles
                            )

                    if colid == "PCJ":
                        self.update_pcj_editor(updated_articles)

                    # Archive the container or the article
                    if article is None:
                        archive_trammel_resource.delay(
                            colid=colid,
                            pid=pid,
                            mathdoc_archive=settings.MATHDOC_ARCHIVE_FOLDER,
                            binary_files_folder=settings.MERSENNE_PROD_DATA_FOLDER,
                        )
                    else:
                        archive_trammel_resource.delay(
                            colid=colid,
                            pid=pid,
                            mathdoc_archive=settings.MATHDOC_ARCHIVE_FOLDER,
                            binary_files_folder=settings.MERSENNE_PROD_DATA_FOLDER,
                            article_doi=article.doi,
                        )
                    # cmd = ptf_cmds.archiveIssuePtfCmd({
                    #     "pid": pid,
                    #     "export_folder": settings.MATHDOC_ARCHIVE_FOLDER,
                    #     "binary_files_folder": settings.MERSENNE_PROD_DATA_FOLDER})
                    # cmd.set_article(article)  # set_article allows archiving only the article
                    # cmd.do()

                elif site == "numdam":
                    from_folder = settings.MERSENNE_PROD_DATA_FOLDER
                    if colid in settings.NUMDAM_COLLECTIONS:
                        from_folder = settings.MERSENNE_TEST_DATA_FOLDER

                    resolver.copy_binary_files(container, from_folder, settings.NUMDAM_DATA_ROOT)
                    for article in container.article_set.all():
                        resolver.copy_binary_files(article, from_folder, settings.NUMDAM_DATA_ROOT)

            elif status == 503:
                raise ServerUnderMaintenance(response.text)
            else:
                raise RuntimeError(response.text)

        if djvu_exception:
            raise djvu_exception

    def get(self, request, *args, **kwargs):
        pid = self.kwargs.get("pid", None)
        colid = self.kwargs.get("colid", None)
        site = self.kwargs.get("site", None)

        try:
            _, status, message = history_views.execute_and_record_func(
                "deploy", pid, colid, self.internal_do, site
            )
        except Timeout as exception:
            return HttpResponse(exception, status=408)
        except Exception as exception:
            return HttpResponseServerError(exception)

        data = {"message": message, "status": status}
        return JsonResponse(data)

    def update_pcj_editor(self, updated_articles):
        for article in updated_articles:
            data = {
                "date_published": article.date_published.strftime("%Y-%m-%d"),
                "article_number": article.article_number,
            }
            url = "http://pcj-editor.u-ga.fr/submit/api-article-publish/" + article.doi + "/"
            requests.post(url, json=data, verify=False)


class DeployTranslatedArticleAPIView(CsrfExemptMixin, View):
    article = None

    def internal_do(self, *args, **kwargs):
        lang = self.kwargs.get("lang", None)

        translation = None
        for trans_article in self.article.translations.all():
            if trans_article.lang == lang:
                translation = trans_article

        if translation is None:
            raise RuntimeError(f"{self.article.doi} does not exist in {lang}")

        collection = self.article.get_top_collection()
        colid = collection.pid
        container = self.article.my_container

        if translation.date_published is None:
            # Add date posted
            cmd = ptf_cmds.publishResourcePtfCmd()
            cmd.set_resource(translation)
            updated_articles = cmd.do()

            # Recompile PDF to add the date posted
            try:
                tex.create_frontpage(colid, container, updated_articles, test=False, lang=lang)
            except Exception:
                raise PDFException(
                    "Unable to compile the article PDF. Please contact the centre Mersenne"
                )

        # Unlike regular articles, binary files of translations need to be copied before uploading the XML.
        # The full text in HTML is read by the JATS parser, so the HTML file needs to be present on disk
        resolver.copy_binary_files(
            self.article, settings.MERSENNE_TEST_DATA_FOLDER, settings.MERSENNE_PROD_DATA_FOLDER
        )

        # Deploy in prod
        xml = ptf_cmds.exportPtfCmd(
            {
                "pid": self.article.pid,
                "with_djvu": False,
                "article_standalone": True,
                "collection_pid": colid,
                "export_to_website": True,
                "export_folder": settings.LOG_DIR,
            }
        ).do()
        xml_file = io.StringIO(xml)
        files = {"xml": xml_file}

        server_url = getattr(collection, "website")()
        if not server_url:
            raise RuntimeError("The collection has no website")
        url = server_url + reverse("article_in_issue_upload", kwargs={"pid": container.pid})
        header = {}

        try:
            response = requests.post(
                url, headers=header, files=files, verify=False
            )  # verify: ignore TLS certificate
            status = response.status_code
        except requests.exceptions.ConnectionError:
            raise ServerUnderMaintenance(
                "The journal is under maintenance. Please try again later."
            )

        # Register translation in Crossref
        if 199 < status < 205:
            if self.article.allow_crossref():
                try:
                    recordDOI(translation)
                except Exception:
                    raise DOIException(
                        "Error while recording the DOI. Please contact the centre Mersenne"
                    )

    def get(self, request, *args, **kwargs):
        doi = kwargs.get("doi", None)
        self.article = model_helpers.get_article_by_doi(doi)
        if self.article is None:
            raise Http404(f"{doi} does not exist")

        try:
            _, status, message = history_views.execute_and_record_func(
                "deploy",
                self.article.pid,
                self.article.get_top_collection().pid,
                self.internal_do,
                "website",
            )
        except Timeout as exception:
            return HttpResponse(exception, status=408)
        except Exception as exception:
            return HttpResponseServerError(exception)

        data = {"message": message, "status": status}
        return JsonResponse(data)


class DeleteJatsIssueAPIView(View):
    # TODO ? rename in DeleteJatsContainerAPIView mais fonctionne tel quel pour book*
    def get(self, request, *args, **kwargs):
        pid = self.kwargs.get("pid", None)
        colid = self.kwargs.get("colid", None)
        site = self.kwargs.get("site", None)
        message = "Le volume a bien été supprimé"
        status = 200

        issue = model_helpers.get_container(pid)
        if not issue:
            raise Http404(f"{pid} does not exist")
        try:
            mersenneSite = model_helpers.get_site_mersenne(colid)

            if site == "ptf_tools":
                if issue.is_deployed(mersenneSite):
                    issue.undeploy(mersenneSite)
                    for article in issue.article_set.all():
                        article.undeploy(mersenneSite)

                p = model_helpers.get_provider("mathdoc-id")

                cmd = ptf_cmds.addContainerPtfCmd(
                    {
                        "pid": issue.pid,
                        "ctype": "issue",
                        "to_folder": settings.MERSENNE_TEST_DATA_FOLDER,
                    }
                )
                cmd.set_provider(p)
                cmd.add_collection(issue.get_collection())
                cmd.set_object_to_be_deleted(issue)
                cmd.undo()

            else:
                if site == "numdam":
                    server_url = settings.NUMDAM_PRE_URL
                else:
                    collection = issue.get_collection()
                    server_url = getattr(collection, site)()

                if not server_url:
                    message = "The collection has no " + site
                    status = 500
                else:
                    url = server_url + reverse("issue_delete", kwargs={"pid": pid})
                    response = requests.delete(url, verify=False)
                    status = response.status_code

                    if status == 404:
                        message = "Le serveur retourne un code 404. Vérifier que le volume soit bien sur le serveur"
                    elif status > 204:
                        body = response.text.encode("utf8")
                        message = body[:1000]
                    else:
                        status = 200
                        # unpublish issue in collection site (site_register.json)
                        if site == "website":
                            if issue.is_deployed(mersenneSite):
                                issue.undeploy(mersenneSite)
                                for article in issue.article_set.all():
                                    article.undeploy(mersenneSite)
                                    # delete article binary files
                                    folder = article.get_relative_folder()
                                    resolver.delete_object_folder(
                                        folder,
                                        to_folder=settings.MERSENNE_PROD_DATA_FORLDER,
                                    )
                                # delete issue binary files
                                folder = issue.get_relative_folder()
                                resolver.delete_object_folder(
                                    folder, to_folder=settings.MERSENNE_PROD_DATA_FORLDER
                                )

        except Timeout as exception:
            return HttpResponse(exception, status=408)
        except Exception as exception:
            return HttpResponseServerError(exception)

        data = {"message": message, "status": status}
        return JsonResponse(data)


class ArchiveIssueAPIView(View):
    def get(self, request, *args, **kwargs):
        try:
            pid = kwargs["pid"]
            colid = kwargs["colid"]
        except IndexError:
            raise Http404

        try:
            cmd = ptf_cmds.archiveIssuePtfCmd(
                {
                    "pid": pid,
                    "export_folder": settings.MATHDOC_ARCHIVE_FOLDER,
                    "binary_files_folder": settings.MERSENNE_PROD_DATA_FOLDER,
                }
            )
            result_, status, message = history_views.execute_and_record_func(
                "archive", pid, colid, cmd.do
            )
        except Exception as exception:
            return HttpResponseServerError(exception)

        data = {"message": message, "status": 200}
        return JsonResponse(data)


class CreateDjvuAPIView(View):
    def internal_do(self, *args, **kwargs):
        pid = self.kwargs.get("pid", None)

        resource = model_helpers.get_resource(pid)
        cmd = ptf_cmds.addDjvuPtfCmd()
        cmd.set_resource(resource)
        cmd.do()

    def get(self, request, *args, **kwargs):
        pid = self.kwargs.get("pid", None)
        colid = pid.split("_")[0]

        try:
            _, status, message = history_views.execute_and_record_func(
                "numdam", pid, colid, self.internal_do
            )
        except Exception as exception:
            return HttpResponseServerError(exception)

        data = {"message": message, "status": status}
        return JsonResponse(data)


class PTFToolsHomeView(LoginRequiredMixin, View):
    """
    Home Page.
    -   Admin & staff -> Render blank home.html
    -   User with unique authorized collection -> Redirect to collection details page
    -   User with multiple authorized collections -> Render home.html with data
    -   Comment moderator -> Comments dashboard
    -   Others -> 404 response
    """

    def get(self, request, *args, **kwargs) -> HttpResponse:
        # Staff or user with authorized collections
        if request.user.is_staff or request.user.is_superuser:
            return render(request, "home.html")

        colids = get_authorized_collections(request.user)
        is_mod = is_comment_moderator(request.user)

        # The user has no rights
        if not (colids or is_mod):
            raise Http404("No collections associated with your account.")
        # Comment moderator only
        elif not colids:
            return HttpResponseRedirect(reverse("comment_list"))

        # User with unique collection -> Redirect to collection detail page
        if len(colids) == 1 or getattr(settings, "COMMENTS_DISABLED", False):
            return HttpResponseRedirect(reverse("collection-detail", kwargs={"pid": colids[0]}))

        # User with multiple authorized collections - Special home
        context = {}
        context["overview"] = True

        all_collections = Collection.objects.filter(pid__in=colids).values("pid", "title_html")
        all_collections = {c["pid"]: c for c in all_collections}

        # Comments summary
        try:
            error, comments_data = get_comments_for_home(request.user)
        except AttributeError:
            error, comments_data = True, {}

        context["comment_server_ok"] = False

        if not error:
            context["comment_server_ok"] = True
        if comments_data:
            for col_id, comment_nb in comments_data.items():
                if col_id.upper() in all_collections:
                    all_collections[col_id.upper()]["pending_comments"] = comment_nb

        # TODO: Translations summary
        context["translation_server_ok"] = False

        # Sort the collections according to the number of pending comments
        context["collections"] = sorted(
            all_collections.values(), key=lambda col: col.get("pending_comments", -1), reverse=True
        )

        return render(request, "home.html", context)


class BaseMersenneDashboardView(TemplateView, history_views.HistoryContextMixin):
    columns = 5

    def get_common_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        now = timezone.now()
        curyear = now.year
        years = range(curyear - self.columns + 1, curyear + 1)

        context["collections"] = settings.MERSENNE_COLLECTIONS
        context["containers_to_be_published"] = []
        context["last_col_events"] = []

        event = history_models.get_history_last_event_by("clockss", "ALL")
        clockss_gap = history_models.get_gap(now, event)

        context["years"] = years
        context["clockss_gap"] = clockss_gap

        return context

    def calculate_articles_and_pages(self, pid, years):
        data_by_year = []
        total_articles = [0] * len(years)
        total_pages = [0] * len(years)

        for year in years:
            articles = self.get_articles_for_year(pid, year)
            articles_count = articles.count()
            page_count = sum(article.get_article_page_count() for article in articles)

            data_by_year.append({"year": year, "articles": articles_count, "pages": page_count})
            total_articles[year - years[0]] += articles_count
            total_pages[year - years[0]] += page_count

        return data_by_year, total_articles, total_pages

    def get_articles_for_year(self, pid, year):
        return Article.objects.filter(
            Q(my_container__my_collection__pid=pid)
            & (
                Q(date_published__year=year, date_online_first__isnull=True)
                | Q(date_online_first__year=year)
            )
        ).prefetch_related("resourcecount_set")


class PublishedArticlesDashboardView(BaseMersenneDashboardView):
    template_name = "dashboard/published_articles.html"

    def get_context_data(self, **kwargs):
        context = self.get_common_context_data(**kwargs)
        years = context["years"]

        published_articles = []
        total_published_articles = [
            {"year": year, "total_articles": 0, "total_pages": 0} for year in years
        ]

        for pid in settings.MERSENNE_COLLECTIONS:
            if pid != "MERSENNE":
                articles_data, total_articles, total_pages = self.calculate_articles_and_pages(
                    pid, years
                )
                published_articles.append({"pid": pid, "years": articles_data})

                for i, year in enumerate(years):
                    total_published_articles[i]["total_articles"] += total_articles[i]
                    total_published_articles[i]["total_pages"] += total_pages[i]

        context["published_articles"] = published_articles
        context["total_published_articles"] = total_published_articles

        return context


class CreatedVolumesDashboardView(BaseMersenneDashboardView):
    template_name = "dashboard/created_volumes.html"

    def get_context_data(self, **kwargs):
        context = self.get_common_context_data(**kwargs)
        years = context["years"]

        created_volumes = []
        total_created_volumes = [
            {"year": year, "total_articles": 0, "total_pages": 0} for year in years
        ]

        for pid in settings.MERSENNE_COLLECTIONS:
            if pid != "MERSENNE":
                volumes_data, total_articles, total_pages = self.calculate_volumes_and_pages(
                    pid, years
                )
                created_volumes.append({"pid": pid, "years": volumes_data})

                for i, year in enumerate(years):
                    total_created_volumes[i]["total_articles"] += total_articles[i]
                    total_created_volumes[i]["total_pages"] += total_pages[i]

        context["created_volumes"] = created_volumes
        context["total_created_volumes"] = total_created_volumes

        return context

    def calculate_volumes_and_pages(self, pid, years):
        data_by_year = []
        total_articles = [0] * len(years)
        total_pages = [0] * len(years)

        for year in years:
            issues = Container.objects.filter(my_collection__pid=pid, year=year)
            articles_count = 0
            page_count = 0

            for issue in issues:
                articles = issue.article_set.filter(
                    Q(date_published__isnull=False) | Q(date_online_first__isnull=False)
                ).prefetch_related("resourcecount_set")

                articles_count += articles.count()
                page_count += sum(article.get_article_page_count() for article in articles)

            data_by_year.append({"year": year, "articles": articles_count, "pages": page_count})
            total_articles[year - years[0]] += articles_count
            total_pages[year - years[0]] += page_count

        return data_by_year, total_articles, total_pages


class BaseCollectionView(TemplateView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        aid = context.get("aid")
        year = context.get("year")

        if aid and year:
            context["collection"] = self.get_collection(aid, year)

        return context

    def get_collection(self, aid, year):
        """Method to be overridden by subclasses to fetch the appropriate collection"""
        raise NotImplementedError("Subclasses must implement get_collection method")


class ArticleListView(BaseCollectionView):
    template_name = "collection-list.html"

    def get_collection(self, aid, year):
        return Article.objects.filter(
            Q(my_container__my_collection__pid=aid)
            & (
                Q(date_published__year=year, date_online_first__isnull=True)
                | Q(date_online_first__year=year)
            )
        ).prefetch_related("resourcecount_set")


class VolumeListView(BaseCollectionView):
    template_name = "collection-list.html"

    def get_collection(self, aid, year):
        return Article.objects.filter(
            Q(my_container__my_collection__pid=aid, my_container__year=year)
            & (Q(date_published__isnull=False) | Q(date_online_first__isnull=False))
        ).prefetch_related("resourcecount_set")


class DOAJResourceRegisterView(View):
    def get(self, request, *args, **kwargs):
        pid = kwargs.get("pid", None)
        resource = model_helpers.get_resource(pid)
        if resource is None:
            raise Http404

        try:
            data = {}
            doaj_meta, response = doaj_pid_register(pid)
            if response is None:
                return HttpResponse(status=204)
            elif doaj_meta and 200 <= response.status_code <= 299:
                data.update(doaj_meta)
            else:
                return HttpResponse(status=response.status_code, reason=response.text)
        except Timeout as exception:
            return HttpResponse(exception, status=408)
        except Exception as exception:
            return HttpResponseServerError(exception)
        return JsonResponse(data)


class CROSSREFResourceRegisterView(View):
    def get(self, request, *args, **kwargs):
        pid = kwargs.get("pid", None)
        # option force for registering doi of articles without date_published (ex; TSG from Numdam)
        force = kwargs.get("force", None)
        if not request.user.is_superuser:
            force = None

        resource = model_helpers.get_resource(pid)
        if resource is None:
            raise Http404

        resource = resource.cast()
        meth = getattr(self, "recordDOI" + resource.classname)
        try:
            data = meth(resource, force)
        except Timeout as exception:
            return HttpResponse(exception, status=408)
        except Exception as exception:
            return HttpResponseServerError(exception)
        return JsonResponse(data)

    def recordDOIArticle(self, article, force=None):
        result = {"status": 404}
        if (
            article.doi
            and not article.do_not_publish
            and (article.date_published or article.date_online_first or force == "force")
        ):
            if article.my_container.year is None:  # or article.my_container.year == '0':
                article.my_container.year = datetime.now().strftime("%Y")
            result = recordDOI(article)
        return result

    def recordDOICollection(self, collection, force=None):
        return recordDOI(collection)

    def recordDOIContainer(self, container, force=None):
        data = {"status": 200, "message": "tout va bien"}

        if container.ctype == "issue":
            if container.doi:
                result = recordDOI(container)
                if result["status"] != 200:
                    return result
            if force == "force":
                articles = container.article_set.exclude(
                    doi__isnull=True, do_not_publish=True, date_online_first__isnull=True
                )
            else:
                articles = container.article_set.exclude(
                    doi__isnull=True,
                    do_not_publish=True,
                    date_published__isnull=True,
                    date_online_first__isnull=True,
                )

            for article in articles:
                result = self.recordDOIArticle(article, force)
                if result["status"] != 200:
                    data = result
        else:
            return recordDOI(container)
        return data


class CROSSREFResourceCheckStatusView(View):
    def get(self, request, *args, **kwargs):
        pid = kwargs.get("pid", None)
        resource = model_helpers.get_resource(pid)
        if resource is None:
            raise Http404
        resource = resource.cast()
        meth = getattr(self, "checkDOI" + resource.classname)
        try:
            meth(resource)
        except Timeout as exception:
            return HttpResponse(exception, status=408)
        except Exception as exception:
            return HttpResponseServerError(exception)

        data = {"status": 200, "message": "tout va bien"}
        return JsonResponse(data)

    def checkDOIArticle(self, article):
        if article.my_container.year is None or article.my_container.year == "0":
            article.my_container.year = datetime.now().strftime("%Y")
        get_or_create_doibatch(article)

    def checkDOICollection(self, collection):
        get_or_create_doibatch(collection)

    def checkDOIContainer(self, container):
        if container.doi is not None:
            get_or_create_doibatch(container)
        for article in container.article_set.all():
            self.checkDOIArticle(article)


class RegisterPubmedFormView(FormView):
    template_name = "record_pubmed_dialog.html"
    form_class = RegisterPubmedForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["pid"] = self.kwargs["pid"]
        context["helper"] = PtfLargeModalFormHelper
        return context


class RegisterPubmedView(View):
    def get(self, request, *args, **kwargs):
        pid = kwargs.get("pid", None)
        update_article = self.request.GET.get("update_article", "on") == "on"

        article = model_helpers.get_article(pid)
        if article is None:
            raise Http404
        try:
            recordPubmed(article, update_article)
        except Exception as exception:
            messages.error("Unable to register the article in PubMed")
            return HttpResponseServerError(exception)

        return HttpResponseRedirect(
            reverse("issue-items", kwargs={"pid": article.my_container.pid})
        )


class PTFToolsContainerView(TemplateView):
    template_name = ""

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        container = model_helpers.get_container(self.kwargs.get("pid"))
        if container is None:
            raise Http404
        citing_articles = container.citations()
        source = self.request.GET.get("source", None)
        if container.ctype.startswith("book"):
            book_parts = (
                container.article_set.filter(sites__id=settings.SITE_ID).all().order_by("seq")
            )
            references = False
            if container.ctype == "book-monograph":
                # on regarde si il y a au moins une bibliographie
                for art in container.article_set.all():
                    if art.bibitem_set.count() > 0:
                        references = True
            context.update(
                {
                    "book": container,
                    "book_parts": list(book_parts),
                    "source": source,
                    "citing_articles": citing_articles,
                    "references": references,
                    "test_website": container.get_top_collection()
                    .extlink_set.get(rel="test_website")
                    .location,
                    "prod_website": container.get_top_collection()
                    .extlink_set.get(rel="website")
                    .location,
                }
            )
            self.template_name = "book-toc.html"
        else:
            articles = container.article_set.all().order_by("seq")
            for article in articles:
                try:
                    last_match = (
                        history_models.HistoryEvent.objects.filter(
                            pid=article.pid,
                            type="matching",
                        )
                        .only("created_on")
                        .latest("created_on")
                    )
                except history_models.HistoryEvent.DoesNotExist as _:
                    article.last_match = None
                else:
                    article.last_match = last_match.created_on

            # article1 = articles.first()
            # date = article1.deployed_date()
            # TODO next_issue, previous_issue

            # check DOI est maintenant une commande à part
            # # specific PTFTools : on regarde pour chaque article l'état de l'enregistrement DOI
            # articlesWithStatus = []
            # for article in articles:
            #     get_or_create_doibatch(article)
            #     articlesWithStatus.append(article)

            test_location = prod_location = ""
            qs = container.get_top_collection().extlink_set.filter(rel="test_website")
            if qs:
                test_location = qs.first().location
            qs = container.get_top_collection().extlink_set.filter(rel="website")
            if qs:
                prod_location = qs.first().location
            context.update(
                {
                    "issue": container,
                    "articles": articles,
                    "source": source,
                    "citing_articles": citing_articles,
                    "test_website": test_location,
                    "prod_website": prod_location,
                }
            )
            self.template_name = "issue-items.html"

        context["allow_crossref"] = container.allow_crossref()
        context["coltype"] = container.my_collection.coltype
        return context


class ExtLinkInline(InlineFormSetFactory):
    model = ExtLink
    form_class = ExtLinkForm
    factory_kwargs = {"extra": 0}


class ResourceIdInline(InlineFormSetFactory):
    model = ResourceId
    form_class = ResourceIdForm
    factory_kwargs = {"extra": 0}


class IssueDetailAPIView(View):
    def get(self, request, *args, **kwargs):
        issue = get_object_or_404(Container, pid=kwargs["pid"])
        deployed_date = issue.deployed_date()
        result = {
            "deployed_date": timezone.localtime(deployed_date).strftime("%Y-%m-%d %H:%M")
            if deployed_date
            else None,
            "last_modified": timezone.localtime(issue.last_modified).strftime("%Y-%m-%d %H:%M"),
            "all_doi_are_registered": issue.all_doi_are_registered(),
            "registered_in_doaj": issue.registered_in_doaj(),
            "doi": issue.my_collection.doi,
            "has_articles_excluded_from_publication": issue.has_articles_excluded_from_publication(),
        }
        try:
            latest = history_models.HistoryEvent.objects.get_last_unsolved_error(
                pid=issue.pid, strict=False
            )
        except history_models.HistoryEvent.DoesNotExist as _:
            pass
        else:
            result["latest"] = latest.data["message"]
            result["latest_target"] = latest.data.get("target", "")
            result["latest_date"] = timezone.localtime(latest.created_on).strftime(
                "%Y-%m-%d %H:%M"
            )

            result["latest_type"] = latest.type.capitalize()
        for event_type in ["matching", "edit", "deploy", "archive", "import"]:
            try:
                result[event_type] = timezone.localtime(
                    history_models.HistoryEvent.objects.filter(
                        type=event_type,
                        status="OK",
                        pid__startswith=issue.pid,
                    )
                    .latest("created_on")
                    .created_on
                ).strftime("%Y-%m-%d %H:%M")
            except history_models.HistoryEvent.DoesNotExist as _:
                result[event_type] = ""
        return JsonResponse(result)


class CollectionFormView(LoginRequiredMixin, StaffuserRequiredMixin, NamedFormsetsMixin, View):
    model = Collection
    form_class = CollectionForm
    inlines = [ResourceIdInline, ExtLinkInline]
    inlines_names = ["resource_ids_form", "ext_links_form"]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["helper"] = PtfFormHelper
        context["formset_helper"] = FormSetHelper
        return context

    def add_description(self, collection, description, lang, seq):
        if description:
            la = Abstract(
                resource=collection,
                tag="description",
                lang=lang,
                seq=seq,
                value_xml=f'<description xml:lang="{lang}">{replace_html_entities(description)}</description>',
                value_html=description,
                value_tex=description,
            )
            la.save()

    def form_valid(self, form):
        if form.instance.abbrev:
            form.instance.title_xml = f"<title-group><title>{form.instance.title_tex}</title><abbrev-title>{form.instance.abbrev}</abbrev-title></title-group>"
        else:
            form.instance.title_xml = (
                f"<title-group><title>{form.instance.title_tex}</title></title-group>"
            )

        form.instance.title_html = form.instance.title_tex
        form.instance.title_sort = form.instance.title_tex
        result = super().form_valid(form)

        collection = self.object
        collection.abstract_set.all().delete()

        seq = 1
        description = form.cleaned_data["description_en"]
        if description:
            self.add_description(collection, description, "en", seq)
            seq += 1
        description = form.cleaned_data["description_fr"]
        if description:
            self.add_description(collection, description, "fr", seq)

        return result

    def get_success_url(self):
        messages.success(self.request, "La Collection a été modifiée avec succès")
        return reverse("collection-detail", kwargs={"pid": self.object.pid})


class CollectionCreate(CollectionFormView, CreateWithInlinesView):
    """
    Warning : Not yet finished
    Automatic site membership creation is still missing
    """


class CollectionUpdate(CollectionFormView, UpdateWithInlinesView):
    slug_field = "pid"
    slug_url_kwarg = "pid"


def suggest_load_journal_dois(colid):
    articles = (
        Article.objects.filter(my_container__my_collection__pid=colid)
        .filter(doi__isnull=False)
        .filter(Q(date_published__isnull=False) | Q(date_online_first__isnull=False))
        .values_list("doi", flat=True)
    )

    try:
        articles = sorted(
            articles,
            key=lambda d: (
                re.search(r"([a-zA-Z]+).\d+$", d).group(1),
                int(re.search(r".(\d+)$", d).group(1)),
            ),
        )
    except:
        pass
    return [f'<option value="{doi}">' for doi in articles]


def get_context_with_volumes(journal):
    result = model_helpers.get_volumes_in_collection(journal)
    volume_count = result["volume_count"]
    collections = []
    for ancestor in journal.ancestors.all():
        item = model_helpers.get_volumes_in_collection(ancestor)
        volume_count = max(0, volume_count)
        item.update({"journal": ancestor})
        collections.append(item)

    # add the parent collection to its children list and sort it by date
    result.update({"journal": journal})
    collections.append(result)

    collections = [c for c in collections if c["sorted_issues"]]
    collections.sort(
        key=lambda ancestor: ancestor["sorted_issues"][0]["volumes"][0]["lyear"],
        reverse=True,
    )

    context = {
        "journal": journal,
        "sorted_issues": result["sorted_issues"],
        "volume_count": volume_count,
        "max_width": result["max_width"],
        "collections": collections,
        "choices": "\n".join(suggest_load_journal_dois(journal.pid)),
    }
    return context


class CollectionDetail(
    UserPassesTestMixin, SingleObjectMixin, ListView, history_views.HistoryContextMixin
):
    model = Collection
    slug_field = "pid"
    slug_url_kwarg = "pid"
    template_name = "ptf/collection_detail.html"

    def test_func(self):
        return is_authorized_editor(self.request.user, self.kwargs.get("pid"))

    def get(self, request, *args, **kwargs):
        self.object = self.get_object(queryset=Collection.objects.all())
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["object_list"] = context["object_list"].filter(ctype="issue")
        context.update(get_context_with_volumes(self.object))

        if self.object.pid in settings.ISSUE_TO_APPEAR_PIDS:
            context["issue_to_appear_pid"] = settings.ISSUE_TO_APPEAR_PIDS[self.object.pid]
            context["issue_to_appear"] = Container.objects.filter(
                pid=context["issue_to_appear_pid"]
            ).exists()
        try:
            latest_error = history_models.HistoryEvent.objects.get_last_unsolved_error(
                self.object.pid,
                strict=True,
            )
        except history_models.HistoryEvent.DoesNotExist as _:
            pass
        else:
            message = latest_error.data["message"]
            i = message.find(" - ")
            latest_exception = message[:i]
            latest_error_message = message[i + 3 :]
            context["latest_exception"] = latest_exception
            context["latest_exception_date"] = latest_error.created_on
            context["latest_exception_type"] = latest_error.type
            context["latest_error_message"] = latest_error_message
        return context

    def get_queryset(self):
        query = self.object.content.all()

        for ancestor in self.object.ancestors.all():
            query |= ancestor.content.all()

        return query.order_by("-year", "-vseries", "-volume", "-volume_int", "-number_int")


class ContainerEditView(FormView):
    template_name = "container_form.html"
    form_class = ContainerForm

    def get_success_url(self):
        if self.kwargs["name"] == "special_issue_create":
            return reverse("special_issues_index", kwargs={"colid": self.kwargs["colid"]})
        if self.kwargs["pid"]:
            return reverse("issue-items", kwargs={"pid": self.kwargs["pid"]})
        return reverse("mersenne_dashboard/published_articles")

    def set_success_message(self):  # pylint: disable=no-self-use
        messages.success(self.request, "Le fascicule a été modifié")

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if "pid" not in self.kwargs:
            self.kwargs["pid"] = None
        if "colid" not in self.kwargs:
            self.kwargs["colid"] = None
            if "data" in kwargs and "colid" in kwargs["data"]:
                # colid is passed as a hidden param in the form.
                # It is used when you submit a new container
                self.kwargs["colid"] = kwargs["data"]["colid"]

        self.kwargs["container"] = kwargs["container"] = model_helpers.get_container(
            self.kwargs["pid"]
        )
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["pid"] = self.kwargs["pid"]
        context["colid"] = self.kwargs["colid"]
        context["container"] = self.kwargs["container"]

        context["edit_container"] = context["pid"] is not None
        context["name"] = resolve(self.request.path_info).url_name

        return context

    def form_valid(self, form):
        new_pid = form.cleaned_data.get("pid")
        new_title = form.cleaned_data.get("title")
        new_trans_title = form.cleaned_data.get("trans_title")
        new_publisher = form.cleaned_data.get("publisher")
        new_year = form.cleaned_data.get("year")
        new_volume = form.cleaned_data.get("volume")
        new_number = form.cleaned_data.get("number")

        collection = None
        issue = self.kwargs["container"]
        if issue is not None:
            collection = issue.my_collection
        elif self.kwargs["colid"] is not None:
            if "CR" in self.kwargs["colid"]:
                collection = model_helpers.get_collection(self.kwargs["colid"], sites=False)
            else:
                collection = model_helpers.get_collection(self.kwargs["colid"])

        if collection is None:
            raise ValueError("Collection for " + new_pid + " does not exist")

        # Icon
        new_icon_location = ""
        if "icon" in self.request.FILES:
            filename = os.path.basename(self.request.FILES["icon"].name)
            file_extension = filename.split(".")[1]

            icon_filename = resolver.get_disk_location(
                settings.MERSENNE_TEST_DATA_FOLDER,
                collection.pid,
                file_extension,
                new_pid,
                None,
                True,
            )

            with open(icon_filename, "wb+") as destination:
                for chunk in self.request.FILES["icon"].chunks():
                    destination.write(chunk)

            folder = resolver.get_relative_folder(collection.pid, new_pid)
            new_icon_location = os.path.join(folder, new_pid + "." + file_extension)
        name = resolve(self.request.path_info).url_name
        if name == "special_issue_create":
            self.kwargs["name"] = name
        if self.kwargs["container"]:
            # Edit Issue
            issue = self.kwargs["container"]
            if issue is None:
                raise ValueError(self.kwargs["pid"] + " does not exist")

            if new_trans_title and (not issue.trans_lang or issue.trans_lang == "und"):
                issue.trans_lang = "fr" if issue.lang == "en" else "en"

            issue.pid = new_pid
            issue.title_tex = issue.title_html = new_title
            issue.trans_title_tex = issue.trans_title_html = new_trans_title
            issue.title_xml = get_issue_title_xml(
                new_title, issue.lang, new_trans_title, issue.trans_lang
            )
            issue.year = new_year
            issue.volume = new_volume
            issue.volume_int = make_int(new_volume)
            issue.number = new_number
            issue.number_int = make_int(new_number)
            issue.save()
        else:
            xissue = create_issuedata()

            if name == "special_issue_create":
                xissue.ctype = "issue_special"
                if not new_year:
                    new_year = datetime.now().year
            else:
                xissue.ctype = "issue"
            xissue.pid = new_pid
            # TODO: add lang + trans_lang
            xissue.title_tex = new_title
            xissue.title_html = new_title
            xissue.trans_title_tex = new_trans_title
            xissue.title_xml = get_issue_title_xml(new_title)

            #     new_title, new_trans_title, issue.trans_lang
            # )
            xissue.year = new_year
            xissue.volume = new_volume
            xissue.number = new_number
            xissue.last_modified_iso_8601_date_str = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

            cmd = ptf_cmds.addContainerPtfCmd({"xobj": xissue})
            cmd.add_collection(collection)
            cmd.set_provider(model_helpers.get_provider_by_name("mathdoc"))
            issue = cmd.do()

        self.kwargs["pid"] = new_pid

        # Add objects related to the article: contribs, datastream, counts...
        params = {
            "icon_location": new_icon_location,
        }
        cmd = ptf_cmds.updateContainerPtfCmd(params)
        cmd.set_resource(issue)
        cmd.do()

        publisher = model_helpers.get_publisher(new_publisher)
        if not publisher:
            xpub = create_publisherdata()
            xpub.name = new_publisher
            publisher = ptf_cmds.addPublisherPtfCmd({"xobj": xpub}).do()
        issue.my_publisher = publisher
        issue.save()

        self.set_success_message()

        return super().form_valid(form)


# class ArticleEditView(FormView):
#     template_name = 'article_form.html'
#     form_class = ArticleForm
#
#     def get_success_url(self):
#         if self.kwargs['pid']:
#             return reverse('article', kwargs={'aid': self.kwargs['pid']})
#         return reverse('mersenne_dashboard/published_articles')
#
#     def set_success_message(self):  # pylint: disable=no-self-use
#         messages.success(self.request, "L'article a été modifié")
#
#     def get_form_kwargs(self):
#         kwargs = super(ArticleEditView, self).get_form_kwargs()
#
#         if 'pid' not in self.kwargs or self.kwargs['pid'] == 'None':
#             # Article creation: pid is None
#             self.kwargs['pid'] = None
#         if 'issue_id' not in self.kwargs:
#             # Article edit: issue_id is not passed
#             self.kwargs['issue_id'] = None
#             if 'data' in kwargs and 'issue_id' in kwargs['data']:
#                 # colid is passed as a hidden param in the form.
#                 # It is used when you submit a new container
#                 self.kwargs['issue_id'] = kwargs['data']['issue_id']
#
#         self.kwargs['article'] = kwargs['article'] = model_helpers.get_article(self.kwargs['pid'])
#         return kwargs
#
#     def get_context_data(self, **kwargs):
#         context = super(ArticleEditView, self).get_context_data(**kwargs)
#
#         context['pid'] = self.kwargs['pid']
#         context['issue_id'] = self.kwargs['issue_id']
#         context['article'] = self.kwargs['article']
#
#         context['edit_article'] = context['pid'] is not None
#
#         article = context['article']
#         if article:
#             context['author_contributions'] = article.get_author_contributions()
#             context['kwds_fr'] = None
#             context['kwds_en'] = None
#             kwd_gps = article.get_non_msc_kwds()
#             for kwd_gp in kwd_gps:
#                 if kwd_gp.lang == 'fr' or (kwd_gp.lang == 'und' and article.lang == 'fr'):
#                     if kwd_gp.value_xml:
#                         kwd_ = types.SimpleNamespace()
#                         kwd_.value = kwd_gp.value_tex
#                         context['kwd_unstructured_fr'] = kwd_
#                     context['kwds_fr'] = kwd_gp.kwd_set.all()
#                 elif kwd_gp.lang == 'en' or (kwd_gp.lang == 'und' and article.lang == 'en'):
#                     if kwd_gp.value_xml:
#                         kwd_ = types.SimpleNamespace()
#                         kwd_.value = kwd_gp.value_tex
#                         context['kwd_unstructured_en'] = kwd_
#                     context['kwds_en'] = kwd_gp.kwd_set.all()
#
#         # Article creation: init pid
#         if context['issue_id'] and context['pid'] is None:
#             issue = model_helpers.get_container(context['issue_id'])
#             context['pid'] = issue.pid + '_A' + str(issue.article_set.count() + 1) + '_0'
#
#         return context
#
#     def form_valid(self, form):
#
#         new_pid = form.cleaned_data.get('pid')
#         new_title = form.cleaned_data.get('title')
#         new_fpage = form.cleaned_data.get('fpage')
#         new_lpage = form.cleaned_data.get('lpage')
#         new_page_range = form.cleaned_data.get('page_range')
#         new_page_count = form.cleaned_data.get('page_count')
#         new_coi_statement = form.cleaned_data.get('coi_statement')
#         new_show_body = form.cleaned_data.get('show_body')
#         new_do_not_publish = form.cleaned_data.get('do_not_publish')
#
#         # TODO support MathML
#         # 27/10/2020: title_xml embeds the trans_title_group in JATS.
#         #             We need to pass trans_title to get_title_xml
#         #             Meanwhile, ignore new_title_xml
#         new_title_xml = jats_parser.get_title_xml(new_title)
#         new_title_html = new_title
#
#         authors_count = int(self.request.POST.get('authors_count', "0"))
#         i = 1
#         new_authors = []
#         old_author_contributions = []
#         if self.kwargs['article']:
#             old_author_contributions = self.kwargs['article'].get_author_contributions()
#
#         while authors_count > 0:
#             prefix = self.request.POST.get('contrib-p-' + str(i), None)
#
#             if prefix is not None:
#                 addresses = []
#                 if len(old_author_contributions) >= i:
#                     old_author_contribution = old_author_contributions[i - 1]
#                     addresses = [contrib_address.address for contrib_address in
#                                  old_author_contribution.get_addresses()]
#
#                 first_name = self.request.POST.get('contrib-f-' + str(i), None)
#                 last_name = self.request.POST.get('contrib-l-' + str(i), None)
#                 suffix = self.request.POST.get('contrib-s-' + str(i), None)
#                 orcid = self.request.POST.get('contrib-o-' + str(i), None)
#                 deceased = self.request.POST.get('contrib-d-' + str(i), None)
#                 deceased_before_publication = deceased == 'on'
#                 equal_contrib = self.request.POST.get('contrib-e-' + str(i), None)
#                 equal_contrib = equal_contrib == 'on'
#                 corresponding = self.request.POST.get('corresponding-' + str(i), None)
#                 corresponding = corresponding == 'on'
#                 email = self.request.POST.get('email-' + str(i), None)
#
#                 params = jats_parser.get_name_params(first_name, last_name, prefix, suffix, orcid)
#                 params['deceased_before_publication'] = deceased_before_publication
#                 params['equal_contrib'] = equal_contrib
#                 params['corresponding'] = corresponding
#                 params['addresses'] = addresses
#                 params['email'] = email
#
#                 params['contrib_xml'] = xml_utils.get_contrib_xml(params)
#
#                 new_authors.append(params)
#
#                 authors_count -= 1
#             i += 1
#
#         kwds_fr_count = int(self.request.POST.get('kwds_fr_count', "0"))
#         i = 1
#         new_kwds_fr = []
#         while kwds_fr_count > 0:
#             value = self.request.POST.get('kwd-fr-' + str(i), None)
#             new_kwds_fr.append(value)
#             kwds_fr_count -= 1
#             i += 1
#         new_kwd_uns_fr = self.request.POST.get('kwd-uns-fr-0', None)
#
#         kwds_en_count = int(self.request.POST.get('kwds_en_count', "0"))
#         i = 1
#         new_kwds_en = []
#         while kwds_en_count > 0:
#             value = self.request.POST.get('kwd-en-' + str(i), None)
#             new_kwds_en.append(value)
#             kwds_en_count -= 1
#             i += 1
#         new_kwd_uns_en = self.request.POST.get('kwd-uns-en-0', None)
#
#         if self.kwargs['article']:
#             # Edit article
#             container = self.kwargs['article'].my_container
#         else:
#             # New article
#             container = model_helpers.get_container(self.kwargs['issue_id'])
#
#         if container is None:
#             raise ValueError(self.kwargs['issue_id'] + " does not exist")
#
#         collection = container.my_collection
#
#         # Copy PDF file & extract full text
#         body = ''
#         pdf_filename = resolver.get_disk_location(settings.MERSENNE_TEST_DATA_FOLDER,
#                                                   collection.pid,
#                                                   "pdf",
#                                                   container.pid,
#                                                   new_pid,
#                                                   True)
#         if 'pdf' in self.request.FILES:
#             with open(pdf_filename, 'wb+') as destination:
#                 for chunk in self.request.FILES['pdf'].chunks():
#                     destination.write(chunk)
#
#             # Extract full text from the PDF
#             body = utils.pdf_to_text(pdf_filename)
#
#         # Icon
#         new_icon_location = ''
#         if 'icon' in self.request.FILES:
#             filename = os.path.basename(self.request.FILES['icon'].name)
#             file_extension = filename.split('.')[1]
#
#             icon_filename = resolver.get_disk_location(settings.MERSENNE_TEST_DATA_FOLDER,
#                                                        collection.pid,
#                                                        file_extension,
#                                                        container.pid,
#                                                        new_pid,
#                                                        True)
#
#             with open(icon_filename, 'wb+') as destination:
#                 for chunk in self.request.FILES['icon'].chunks():
#                     destination.write(chunk)
#
#             folder = resolver.get_relative_folder(collection.pid, container.pid, new_pid)
#             new_icon_location = os.path.join(folder, new_pid + '.' + file_extension)
#
#         if self.kwargs['article']:
#             # Edit article
#             article = self.kwargs['article']
#             article.fpage = new_fpage
#             article.lpage = new_lpage
#             article.page_range = new_page_range
#             article.coi_statement = new_coi_statement
#             article.show_body = new_show_body
#             article.do_not_publish = new_do_not_publish
#             article.save()
#
#         else:
#             # New article
#             params = {
#                 'pid': new_pid,
#                 'title_xml': new_title_xml,
#                 'title_html': new_title_html,
#                 'title_tex': new_title,
#                 'fpage': new_fpage,
#                 'lpage': new_lpage,
#                 'page_range': new_page_range,
#                 'seq': container.article_set.count() + 1,
#                 'body': body,
#                 'coi_statement': new_coi_statement,
#                 'show_body': new_show_body,
#                 'do_not_publish': new_do_not_publish
#             }
#
#             xarticle = create_articledata()
#             xarticle.pid = new_pid
#             xarticle.title_xml = new_title_xml
#             xarticle.title_html = new_title_html
#             xarticle.title_tex = new_title
#             xarticle.fpage = new_fpage
#             xarticle.lpage = new_lpage
#             xarticle.page_range = new_page_range
#             xarticle.seq = container.article_set.count() + 1
#             xarticle.body = body
#             xarticle.coi_statement = new_coi_statement
#             params['xobj'] = xarticle
#
#             cmd = ptf_cmds.addArticlePtfCmd(params)
#             cmd.set_container(container)
#             cmd.add_collection(container.my_collection)
#             article = cmd.do()
#
#             self.kwargs['pid'] = new_pid
#
#         # Add objects related to the article: contribs, datastream, counts...
#         params = {
#             # 'title_xml': new_title_xml,
#             # 'title_html': new_title_html,
#             # 'title_tex': new_title,
#             'authors': new_authors,
#             'page_count': new_page_count,
#             'icon_location': new_icon_location,
#             'body': body,
#             'use_kwds': True,
#             'kwds_fr': new_kwds_fr,
#             'kwds_en': new_kwds_en,
#             'kwd_uns_fr': new_kwd_uns_fr,
#             'kwd_uns_en': new_kwd_uns_en
#         }
#         cmd = ptf_cmds.updateArticlePtfCmd(params)
#         cmd.set_article(article)
#         cmd.do()
#
#         self.set_success_message()
#
#         return super(ArticleEditView, self).form_valid(form)


@require_http_methods(["POST"])
def do_not_publish_article(request, *args, **kwargs):
    next = request.headers.get("referer")

    pid = kwargs.get("pid", "")

    article = model_helpers.get_article(pid)
    if article:
        article.do_not_publish = not article.do_not_publish
        article.save()
    else:
        raise Http404

    return HttpResponseRedirect(next)


@require_http_methods(["POST"])
def show_article_body(request, *args, **kwargs):
    next = request.headers.get("referer")

    pid = kwargs.get("pid", "")

    article = model_helpers.get_article(pid)
    if article:
        article.show_body = not article.show_body
        article.save()
    else:
        raise Http404

    return HttpResponseRedirect(next)


class ArticleEditWithVueAPIView(CsrfExemptMixin, ArticleEditAPIView):
    """
    API to get/post article metadata
    The class is derived from ArticleEditAPIView (see ptf.views)
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields_to_update = [
            "lang",
            "title_xml",
            "title_tex",
            "title_html",
            "trans_lang",
            "trans_title_html",
            "trans_title_tex",
            "trans_title_xml",
            "atype",
            "contributors",
            "abstracts",
            "subjs",
            "kwds",
            "ext_links",
        ]

    def convert_data_for_editor(self, data_article):
        super().convert_data_for_editor(data_article)
        data_article.is_staff = self.request.user.is_staff

    def save_data(self, data_article):
        # On sauvegarde les données additionnelles (extid, deployed_date,...) dans un json
        # The icons are not preserved since we can add/edit/delete them in VueJs
        params = {
            "pid": data_article.pid,
            "export_folder": settings.MERSENNE_TMP_FOLDER,
            "export_all": True,
            "with_binary_files": False,
        }
        ptf_cmds.exportExtraDataPtfCmd(params).do()

    def restore_data(self, article):
        ptf_cmds.importExtraDataPtfCmd(
            {
                "pid": article.pid,
                "import_folder": settings.MERSENNE_TMP_FOLDER,
            }
        ).do()


class ArticleEditWithVueView(LoginRequiredMixin, TemplateView):
    template_name = "article_form.html"

    def get_success_url(self):
        if self.kwargs["doi"]:
            return reverse("article", kwargs={"aid": self.kwargs["doi"]})
        return reverse("mersenne_dashboard/published_articles")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if "doi" in self.kwargs:
            context["article"] = model_helpers.get_article_by_doi(self.kwargs["doi"])
            context["pid"] = context["article"].pid

        return context


class ArticleDeleteView(View):
    def get(self, request, *args, **kwargs):
        pid = self.kwargs.get("pid", None)
        article = get_object_or_404(Article, pid=pid)

        try:
            mersenneSite = model_helpers.get_site_mersenne(article.get_collection().pid)
            article.undeploy(mersenneSite)

            cmd = ptf_cmds.addArticlePtfCmd(
                {"pid": article.pid, "to_folder": settings.MERSENNE_TEST_DATA_FOLDER}
            )
            cmd.set_container(article.my_container)
            cmd.set_object_to_be_deleted(article)
            cmd.undo()
        except Exception as exception:
            return HttpResponseServerError(exception)

        data = {"message": "L'article a bien été supprimé de ptf-tools", "status": 200}
        return JsonResponse(data)


def get_messages_in_queue():
    app = Celery("ptf-tools")
    # tasks = list(current_app.tasks)
    tasks = list(sorted(name for name in current_app.tasks if name.startswith("celery")))
    print(tasks)
    # i = app.control.inspect()

    with app.connection_or_acquire() as conn:
        remaining = conn.default_channel.queue_declare(queue="celery", passive=True).message_count
        return remaining


class FailedTasksListView(ListView):
    model = TaskResult
    queryset = TaskResult.objects.filter(
        status="FAILURE",
        task_name="ptf_tools.tasks.archive_numdam_issue",
    )


class FailedTasksDeleteView(DeleteView):
    model = TaskResult
    success_url = reverse_lazy("tasks-failed")


class FailedTasksRetryView(SingleObjectMixin, RedirectView):
    model = TaskResult

    @staticmethod
    def retry_task(task):
        colid, pid = (arg.strip("'") for arg in task.task_args.strip("()").split(", "))
        archive_numdam_issue.delay(colid, pid)
        task.delete()

    def get_redirect_url(self, *args, **kwargs):
        self.retry_task(self.get_object())
        return reverse("tasks-failed")


class NumdamView(TemplateView, history_views.HistoryContextMixin):
    template_name = "numdam.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["objs"] = ResourceInNumdam.objects.all()

        pre_issues = []
        prod_issues = []
        url = f"{settings.NUMDAM_PRE_URL}/api-all-issues/"
        try:
            response = requests.get(url)
            if response.status_code == 200:
                data = response.json()
                if "issues" in data:
                    pre_issues = data["issues"]
        except Exception:
            pass

        url = f"{settings.NUMDAM_URL}/api-all-issues/"
        response = requests.get(url)
        if response.status_code == 200:
            data = response.json()
            if "issues" in data:
                prod_issues = data["issues"]

        new = sorted(list(set(pre_issues).difference(prod_issues)))
        removed = sorted(list(set(prod_issues).difference(pre_issues)))
        grouped = [
            {"colid": k, "issues": list(g)} for k, g in groupby(new, lambda x: x.split("_")[0])
        ]
        grouped_removed = [
            {"colid": k, "issues": list(g)} for k, g in groupby(removed, lambda x: x.split("_")[0])
        ]
        context["added_issues"] = grouped
        context["removed_issues"] = grouped_removed

        context["numdam_collections"] = settings.NUMDAM_COLLECTIONS
        return context


class TasksProgressView(View):
    def get(self, *args, **kwargs):
        task_name = self.kwargs.get("task", "archive_numdam_issue")
        successes = TaskResult.objects.filter(
            task_name=f"ptf_tools.tasks.{task_name}", status="SUCCESS"
        ).count()
        fails = TaskResult.objects.filter(
            task_name=f"ptf_tools.tasks.{task_name}", status="FAILURE"
        ).count()
        last_task = (
            TaskResult.objects.filter(
                task_name=f"ptf_tools.tasks.{task_name}",
                status="SUCCESS",
            )
            .order_by("-date_done")
            .first()
        )
        if last_task:
            last_task = " : ".join([last_task.date_done.strftime("%Y-%m-%d"), last_task.task_args])
        remaining = get_messages_in_queue()
        all = successes + remaining
        progress = int(successes * 100 / all) if all else 0
        error_rate = int(fails * 100 / all) if all else 0
        status = "consuming_queue" if (successes or fails) and not progress == 100 else "polling"
        data = {
            "status": status,
            "progress": progress,
            "total": all,
            "remaining": remaining,
            "successes": successes,
            "fails": fails,
            "error_rate": error_rate,
            "last_task": last_task,
        }
        return JsonResponse(data)


class TasksView(TemplateView):
    template_name = "tasks.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["tasks"] = TaskResult.objects.all()
        return context


class NumdamArchiveView(RedirectView):
    @staticmethod
    def reset_task_results():
        TaskResult.objects.all().delete()

    def get_redirect_url(self, *args, **kwargs):
        self.colid = kwargs["colid"]

        if self.colid != "ALL" and self.colid in settings.MERSENNE_COLLECTIONS:
            return Http404

        # we make sure archiving is not already running
        if not get_messages_in_queue():
            self.reset_task_results()
            response = requests.get(f"{settings.NUMDAM_URL}/api-all-collections/")
            if response.status_code == 200:
                data = sorted(response.json()["collections"])

                if self.colid != "ALL" and self.colid not in data:
                    return Http404

                colids = [self.colid] if self.colid != "ALL" else data

                with open(
                    os.path.join(settings.LOG_DIR, "archive.log"), "w", encoding="utf-8"
                ) as file_:
                    file_.write("Archive " + " ".join([colid for colid in colids]) + "\n")

                for colid in colids:
                    if colid not in settings.MERSENNE_COLLECTIONS:
                        archive_numdam_collection.delay(colid)
        return reverse("numdam")


class DeployAllNumdamAPIView(View):
    def internal_do(self, *args, **kwargs):
        pids = []

        for obj in ResourceInNumdam.objects.all():
            pids.append(obj.pid)

        return pids

    def get(self, request, *args, **kwargs):
        try:
            pids, status, message = history_views.execute_and_record_func(
                "deploy", "numdam", "numdam", self.internal_do, "numdam"
            )
        except Exception as exception:
            return HttpResponseServerError(exception)

        data = {"message": message, "ids": pids, "status": status}
        return JsonResponse(data)


class NumdamDeleteAPIView(View):
    def get(self, request, *args, **kwargs):
        pid = self.kwargs.get("pid", None)

        try:
            obj = ResourceInNumdam.objects.get(pid=pid)
            obj.delete()
        except Exception as exception:
            return HttpResponseServerError(exception)

        data = {"message": "Le volume a bien été supprimé de la liste pour Numdam", "status": 200}
        return JsonResponse(data)


class ExtIdApiDetail(View):
    def get(self, request, *args, **kwargs):
        extid = get_object_or_404(
            ExtId,
            resource__pid=kwargs["pid"],
            id_type=kwargs["what"],
        )
        return JsonResponse(
            {
                "pk": extid.pk,
                "href": extid.get_href(),
                "fetch": reverse(
                    "api-fetch-id",
                    args=(
                        extid.resource.pk,
                        extid.id_value,
                        extid.id_type,
                        "extid",
                    ),
                ),
                "check": reverse("update-extid", args=(extid.pk, "toggle-checked")),
                "uncheck": reverse("update-extid", args=(extid.pk, "toggle-false-positive")),
                "update": reverse("extid-update", kwargs={"pk": extid.pk}),
                "delete": reverse("update-extid", args=(extid.pk, "delete")),
                "is_valid": extid.checked,
            }
        )


class ExtIdFormTemplate(TemplateView):
    template_name = "common/externalid_form.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["sequence"] = kwargs["sequence"]
        return context


class BibItemIdFormView(LoginRequiredMixin, StaffuserRequiredMixin, View):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["helper"] = PtfFormHelper
        return context

    def get_success_url(self):
        self.post_process()
        return self.object.bibitem.resource.get_absolute_url()

    def post_process(self):
        cmd = xml_cmds.updateBibitemCitationXmlCmd()
        cmd.set_bibitem(self.object.bibitem)
        cmd.do()
        model_helpers.post_resource_updated(self.object.bibitem.resource)


class BibItemIdCreate(BibItemIdFormView, CreateView):
    model = BibItemId
    form_class = BibItemIdForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["bibitem"] = BibItem.objects.get(pk=self.kwargs["bibitem_pk"])
        return context

    def get_initial(self):
        initial = super().get_initial()
        initial["bibitem"] = BibItem.objects.get(pk=self.kwargs["bibitem_pk"])
        return initial

    def form_valid(self, form):
        form.instance.checked = False
        return super().form_valid(form)


class BibItemIdUpdate(BibItemIdFormView, UpdateView):
    model = BibItemId
    form_class = BibItemIdForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["bibitem"] = self.object.bibitem
        return context


class ExtIdFormView(LoginRequiredMixin, StaffuserRequiredMixin, View):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["helper"] = PtfFormHelper
        return context

    def get_success_url(self):
        self.post_process()
        return self.object.resource.get_absolute_url()

    def post_process(self):
        model_helpers.post_resource_updated(self.object.resource)


class ExtIdCreate(ExtIdFormView, CreateView):
    model = ExtId
    form_class = ExtIdForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["resource"] = Resource.objects.get(pk=self.kwargs["resource_pk"])
        return context

    def get_initial(self):
        initial = super().get_initial()
        initial["resource"] = Resource.objects.get(pk=self.kwargs["resource_pk"])
        return initial

    def form_valid(self, form):
        form.instance.checked = False
        return super().form_valid(form)


class ExtIdUpdate(ExtIdFormView, UpdateView):
    model = ExtId
    form_class = ExtIdForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["resource"] = self.object.resource
        return context


class BibItemIdApiDetail(View):
    def get(self, request, *args, **kwargs):
        bibitemid = get_object_or_404(
            BibItemId,
            bibitem__resource__pid=kwargs["pid"],
            bibitem__sequence=kwargs["seq"],
            id_type=kwargs["what"],
        )
        return JsonResponse(
            {
                "pk": bibitemid.pk,
                "href": bibitemid.get_href(),
                "fetch": reverse(
                    "api-fetch-id",
                    args=(
                        bibitemid.bibitem.pk,
                        bibitemid.id_value,
                        bibitemid.id_type,
                        "bibitemid",
                    ),
                ),
                "check": reverse("update-bibitemid", args=(bibitemid.pk, "toggle-checked")),
                "uncheck": reverse(
                    "update-bibitemid", args=(bibitemid.pk, "toggle-false-positive")
                ),
                "update": reverse("bibitemid-update", kwargs={"pk": bibitemid.pk}),
                "delete": reverse("update-bibitemid", args=(bibitemid.pk, "delete")),
                "is_valid": bibitemid.checked,
            }
        )


class UpdateTexmfZipAPIView(View):
    def get(self, request, *args, **kwargs):
        def copy_zip_files(src_folder, dest_folder):
            os.makedirs(dest_folder, exist_ok=True)

            zip_files = [
                os.path.join(src_folder, f)
                for f in os.listdir(src_folder)
                if os.path.isfile(os.path.join(src_folder, f)) and f.endswith(".zip")
            ]
            for zip_file in zip_files:
                resolver.copy_file(zip_file, dest_folder)

            # Exceptions: specific zip/gz files
            zip_file = os.path.join(src_folder, "texmf-bsmf.zip")
            resolver.copy_file(zip_file, dest_folder)

            zip_file = os.path.join(src_folder, "texmf-cg.zip")
            resolver.copy_file(zip_file, dest_folder)

            gz_file = os.path.join(src_folder, "texmf-mersenne.tar.gz")
            resolver.copy_file(gz_file, dest_folder)

        src_folder = settings.CEDRAM_DISTRIB_FOLDER

        dest_folder = os.path.join(
            settings.MERSENNE_TEST_DATA_FOLDER, "MERSENNE", "media", "texmf"
        )

        try:
            copy_zip_files(src_folder, dest_folder)
        except Exception as exception:
            return HttpResponseServerError(exception)

        try:
            dest_folder = os.path.join(
                settings.MERSENNE_PROD_DATA_FOLDER, "MERSENNE", "media", "texmf"
            )
            copy_zip_files(src_folder, dest_folder)
        except Exception as exception:
            return HttpResponseServerError(exception)

        data = {"message": "Les texmf*.zip ont bien été mis à jour", "status": 200}
        return JsonResponse(data)


class TestView(TemplateView):
    template_name = "mersenne.html"

    def get_context_data(self, **kwargs):
        super().get_context_data(**kwargs)
        issue = model_helpers.get_container(pid="CRPHYS_0__0_0", prefetch=True)
        model_data_converter.db_to_issue_data(issue)


class TrammelArchiveView(RedirectView):
    @staticmethod
    def reset_task_results():
        TaskResult.objects.all().delete()

    def get_redirect_url(self, *args, **kwargs):
        self.colid = kwargs["colid"]
        self.mathdoc_archive = settings.MATHDOC_ARCHIVE_FOLDER
        self.binary_files_folder = settings.MERSENNE_PROD_DATA_FOLDER
        # Make sure archiving is not already running
        if not get_messages_in_queue():
            self.reset_task_results()
            if "progress/" in self.colid:
                self.colid = self.colid.replace("progress/", "")
            if "/progress" in self.colid:
                self.colid = self.colid.replace("/progress", "")

            if self.colid != "ALL" and self.colid not in settings.MERSENNE_COLLECTIONS:
                return Http404

            colids = [self.colid] if self.colid != "ALL" else settings.MERSENNE_COLLECTIONS

            with open(
                os.path.join(settings.LOG_DIR, "archive.log"), "w", encoding="utf-8"
            ) as file_:
                file_.write("Archive " + " ".join([colid for colid in colids]) + "\n")

            for colid in colids:
                archive_trammel_collection.delay(
                    colid, self.mathdoc_archive, self.binary_files_folder
                )

        if self.colid == "ALL":
            return reverse("home")
        else:
            return reverse("collection-detail", kwargs={"pid": self.colid})


class TrammelTasksProgressView(View):
    def get(self, request, *args, **kwargs):
        """
        Return a JSON object with the progress of the archiving task Le code permet de récupérer l'état d'avancement
        de la tache celery (archive_trammel_resource) en SSE (Server-Sent Events)
        """
        task_name = self.kwargs.get("task", "archive_numdam_issue")

        def get_event_data():
            # Tasks are typically in the CREATED then SUCCESS or FAILURE state

            # Some messages (in case of many call to <task>.delay) have not been converted to TaskResult yet
            remaining_messages = get_messages_in_queue()

            all_tasks = TaskResult.objects.filter(task_name=f"ptf_tools.tasks.{task_name}")
            successed_tasks = all_tasks.filter(status="SUCCESS").order_by("-date_done")
            failed_tasks = all_tasks.filter(status="FAILURE")

            all_tasks_count = all_tasks.count()
            success_count = successed_tasks.count()
            fail_count = failed_tasks.count()

            all_count = all_tasks_count + remaining_messages
            remaining_count = all_count - success_count - fail_count

            success_rate = int(success_count * 100 / all_count) if all_count else 0
            error_rate = int(fail_count * 100 / all_count) if all_count else 0
            status = "consuming_queue" if remaining_count != 0 else "polling"

            last_task = successed_tasks.first()
            last_task = (
                " : ".join([last_task.date_done.strftime("%Y-%m-%d"), last_task.task_args])
                if last_task
                else ""
            )

            # SSE event format
            event_data = {
                "status": status,
                "success_rate": success_rate,
                "error_rate": error_rate,
                "all_count": all_count,
                "remaining_count": remaining_count,
                "success_count": success_count,
                "fail_count": fail_count,
                "last_task": last_task,
            }

            return event_data

        def stream_response(data):
            # Send initial response headers
            yield f"data: {json.dumps(data)}\n\n"

        data = get_event_data()
        format = request.GET.get("format", "stream")
        if format == "json":
            response = JsonResponse(data)
        else:
            response = HttpResponse(stream_response(data), content_type="text/event-stream")
        return response


class TrammelFailedTasksListView(ListView):
    model = TaskResult
    queryset = TaskResult.objects.filter(
        status="FAILURE",
        task_name="ptf_tools.tasks.archive_trammel_resource",
    )
