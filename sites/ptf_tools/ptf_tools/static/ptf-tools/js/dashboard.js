$(document).ready(function () {
    // toggle small or large menu
    $("#menu_toggle").on("click", function () {
        if ($("body").hasClass("nav-md")) {
            $("#sidebar-menu").find("li.active ul").hide();
            $("#sidebar-menu").find("li.active").addClass("active-sm").removeClass("active");
        } else {
            $("#sidebar-menu").find("li.active-sm ul").show();
            $("#sidebar-menu").find("li.active-sm").addClass("active").removeClass("active-sm");
        }

        $("body").toggleClass("nav-md nav-sm");
    });
});
