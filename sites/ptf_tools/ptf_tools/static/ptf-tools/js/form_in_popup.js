$(document).ready(function () {
    var formAjaxSubmit = function (form, modal) {
        $(form).submit(function (e) {
            e.preventDefault();
            $.ajax({
                type: $(this).attr("method"),
                url: $(this).attr("action"),
                data: $(this).serialize(),
                success: function (xhr, ajaxOptions, thrownError) {
                    if ($(xhr).find(".has-error").length > 0) {
                        $(modal).find(".modal-body").html(xhr);
                        formAjaxSubmit(form, modal);
                    } else {
                        $(modal).modal("toggle");
                        location.reload();
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    // handle response errors here
                    notify_user(xhr, this.url);
                    $(modal).modal("toggle");
                },
            });
        });
    };

    var deleteKwdField = function (div_to_remove, lang) {
        var the_parent = div_to_remove.parent();
        var uns = div_to_remove.attr("data-uns");

        div_to_remove.remove();
        the_parent.find(".first-input-of-kwd").last().focus();

        if (uns != "1") {
            var the_id = "#kwds_" + lang + "_count";
            var count = parseInt($(the_id).val(), 10) - 1;
            console.log(count);
            $(the_id).val(count);
        }
    };

    var addKwdFields = function (lang, kwds) {
        var the_class = ".kwd-group-" + lang;
        // Because of the prototype, i is already equal to len(kwds) + 1
        var i = $(the_class).length;

        for (j = 0; j < kwds.length; j++) {
            var kwd = kwds[j];

            // A prototype was added by the Django template
            // Clone it to create the new line
            var the_id = "#template-kwd-" + lang + "-form";
            html_text = $(the_id).html();
            html_text = html_text.replace(/\#\#i\#\#/g, i);
            html_text = html_text.replace(/\#\#value\#\#/g, kwd);

            the_id = "#article-kwds-" + lang;
            var the_parent = $(the_id);
            the_parent.append(html_text);

            i += 1;
        }

        the_parent.find(".first-input-of-kwd").last().focus();

        the_id = "#kwds_" + lang + "_count";
        var count = parseInt($(the_id).val(), 10) + kwds.length;
        console.log(count);
        $(the_id).val(count);
    };

    $(".form-modal-button").click(function () {
        var url = $(this).attr("data-url");
        var title = $(this).attr("modal-title");
        var id_select = $(this).attr("data-select-id");
        // For some browsers, `attr` is undefined; for others, `attr` is false. Check for both.
        if (typeof id_select !== typeof undefined && id_select !== false) {
            // Element has this attribute
            var site = $("#" + id_select).val();
            url = url.replace("toreplacesite", site);
        }
        $("#form-modal-body").load(url, function () {
            $("#form-modal").modal("toggle");
            $("#form-modal h4").text(title);
            formAjaxSubmit("#form-modal-body form", "#form-modal");
        });
    });

    $(".form-modal-button-no-ajax-submit").click(function () {
        var url = $(this).attr("data-url");
        var title = $(this).attr("modal-title");
        var id_select = $(this).attr("data-select-id");
        // For some browsers, `attr` is undefined; for others, `attr` is false. Check for both.
        if (typeof id_select !== typeof undefined && id_select !== false) {
            // Element has this attribute
            var site = $("#" + id_select).val();
            url = url.replace("toreplacesite", site);
        }
        $("#form-modal-body").load(url, function () {
            $("#form-modal").modal("toggle");
            $("#form-modal h4").text(title);
        });
    });

    $(document).on("input", "#id_fpage,#id_lpage,#id_page_range,#id_page_count", function (event) {
        var fpage = $("#id_fpage").val();
        var lpage = $("#id_lpage").val();
        var page_range = $("#id_page_range").val();
        var page_count = $("#id_page_count").val();

        $("#id_fpage").prop("disabled", page_range || page_count);
        $("#id_lpage").prop("disabled", page_range || page_count);
        $("#id_page_range").prop("disabled", fpage || lpage || page_count);
        $("#id_page_count").prop("disabled", fpage || lpage || page_range);
    });

    $(document).on("input", "#id_year,#id_number", function (event) {
        var data_auto = $("#id_pid").attr("data-auto");

        if (data_auto == "on") {
            var colid = $("#id_pid").attr("data-colid");
            var year = $("#id_year").val();
            var number = $("#id_number").val();
            var serie = "";
            var volume = "";
            var new_pid = colid + "_" + year + "_" + serie + "_" + volume + "_" + number;
            $("#id_pid").val(new_pid);
        }
    });

    $(document).on("input", "#id_pid", function (event) {
        var pid = $("#id_pid").val();
        if (pid == "") {
            $("#id_pid").attr("data-auto", "on");
        } else {
            $("#id_pid").attr("data-auto", "off");
        }
    });

    // Add a new contrib line
    $(document).on("click", "#add-contrib-field", function () {
        // Because of the prototype, i is already equal to len(authors) + 1
        var i = $(".contrib-group").length;

        // A prototype was added by the Django template
        // Clone it to create the new line
        html_text = $("#template-contrib-form").html();
        html_text = html_text.replace(/\#\#i\#\#/g, i);

        var the_parent = $("#article-authors");
        the_parent.append(html_text);
        the_parent.find(".first-input-of-contrib").last().focus();

        var count = parseInt($("#authors_count").val(), 10) + 1;
        $("#authors_count").val(count);
    });

    $("#article-authors").on("click", ".remove-contrib-field", function () {
        var div_to_remove = $(this).parent();
        var the_parent = div_to_remove.parent();

        div_to_remove.remove();
        the_parent.find(".first-input-of-contrib").last().focus();

        var count = parseInt($("#authors_count").val(), 10) - 1;
        $("#authors_count").val(count);
    });

    // Add a new contrib line
    $(document).on("click", ".add-kwd-field", function () {
        var lang = $(this).parent().attr("data-lang");

        addKwdFields(lang, [""]);
    });

    $(document).on("click", ".remove-kwd-field", function () {
        var lang = $(this).attr("data-lang");
        var div_to_remove = $(this).parent();

        deleteKwdField(div_to_remove, lang);
    });

    $(document).on("click", ".split-kwd-field", function () {
        var lang = $(this).attr("data-lang");
        var input = $(this).parent().find("input");
        var text = input.val();
        var kwds = text.split(", ");

        $(this).parent().parent().find(".add-kwd-field").prop("disabled", false);

        var div_to_remove = $(this).parent();
        deleteKwdField(div_to_remove, lang);

        addKwdFields(lang, kwds);
    });
});
