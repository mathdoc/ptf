function notify_message(params) {
  new PNotify({
    title: params.title ? params.title : "Échec",
    text: params.message ? params.message : "",
    type: params.type ? params.type : "error",
    styling: "bootstrap3",
    delay: params.delay ? params.delay : "3000",
  });
}

function chgActionForm(select) {
  document.getElementById("suggestForm").action = select.value;
}

function chgSubmitForm(form_id) {
  if (form_id) {
    form_id.addEventListener("submit", (event) => {
      var url = form_id.action;
      var spinner = form_id.querySelector("#status-spinner");
      if (spinner) { spinner.style.display = "inline-block"; }
      console.log(url);
      event.preventDefault();
      fetch(event.target.action, {
        method: "POST",
        body: new FormData(event.target),
      }).then((response) => {
          console.log(response);
          var site = document.getElementById("site");
          if (site) {
            response.deploy = site.selectedOptions[0].text.toLowerCase();
          }
          notify_user(response, url);
          if (!response.ok) {
            throw new Error(`HTTP error! Status: ${response.status}`);
          }
          return response;
        }).catch(function (error) {
          console.log(error);
        }).then(function () {
          if (spinner) { spinner.style.display = "none"; }
          setTimeout(function () {
            window.location.reload(true);
          }, 2000);
        });
    });
  }
}

function spinnerLoader(event) {
  var spinner = event.target.querySelector("#status-spinner");
  if (spinner) { spinner.style.display = "inline-block"; }
}

$(document).ready(function () {
  $(document).on("keypress", "#journal-dois", function (event) {
    var search_field = document.getElementById("journal-dois");
    if (event.keyCode == 13) {
      event.preventDefault();
      let location = window.location.href;
      let colid = new URL(location).pathname.split("collection/")[1];
      if (search_field.value.length != 0) {
        window.location.href = "/editor_article/" + colid + search_field.value + "/";
      }
    }
  });

  var forms = ["suggestForm", "gaForm", "pageForm"];
  for (const form of forms) {
    chgSubmitForm(document.getElementById(form));
  }

  for (const image_name of ["graphical_abstract", "illustration"]) {
    const imgInput = document.querySelector("#id_" + image_name);
    if (imgInput) {
      imgInput.addEventListener("change", (event) => {
        const imgObject = event.target.files[0];
        const displayImg = document.querySelector("#" + image_name);
        displayImg.src = URL.createObjectURL(imgObject);
      });
    }
  }

  let check = document.getElementById("id_automatic_list");
  if (check) {
    let doisList = document.getElementById("id_doi_list");
    doisList.disabled = check.checked;
    check.addEventListener("click", () => (doisList.disabled = check.checked));
  }

  $(".page-table").DataTable({
    pageLength: 20,
  });

});
