$(document).ready(function () {
    $("#resourceid-add").click(function () {
        add_form($(this), "resourceid", 2);
    });
    $("#extlink-add").click(function () {
        add_form($(this), "extlink", 4);
    });
});

function add_form(where, model, max) {
    if ($("#id_" + model + "_set-TOTAL_FORMS").attr("value") < max) {
        var count = $("#id_" + model + "_set-TOTAL_FORMS").attr("value");

        // Ajoute un ligne au formset
        var form_template = where.prev().find(".empty-form").html();
        var compiled_template = form_template.replace(/__prefix__/g, count);
        where.prev().append(compiled_template);

        // Incrémente le nombre total de formulaires
        $("#id_" + model + "_set-TOTAL_FORMS").attr("value", ++count);
    }
    // pas plus de 'max' forms dans le formset
    if ($("#id_" + model + "_set-TOTAL_FORMS").attr("value") > max - 1) {
        $("#" + model + "-add").fadeOut();
    }
}
