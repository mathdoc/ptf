function displayProgressBar(success_rate, error_rate) {
    // Show progress bar
    $("#archiving-progress").removeClass("hidden");
    $("#results").removeClass("hidden");

    // Handle buttons
    $("#celery-archiving").addClass("disabled");  // Prevent other clicks on the Archive button
    $("#archive-errors-btn").addClass("hidden");  // Hide error button while archiving is in progress

    // Update progress bar
    $(".progress-bar-success")
        .css("width", success_rate + "%")
        .text(success_rate + "%");
    $(".progress-bar-warning")
        .css("width", error_rate + "%")
        .text(error_rate + "%");
}

function updateProgressBar(result) {
    let report = "Archive status<br>Errors : " + result.fail_count;
    report += "<br>Successes : " + result.success_count;
    report += "<br>Total : " + result.all_count;

    if (result.status == "consuming_queue") {
        report += "<br>Remaining tasks : " + result.remaining_count;
        if (result.last_task != '') {
            report += "<br>Last Task : " + result.last_task;
        }
    }
    $("#results").html(report);

    if (result.status == "consuming_queue") {
        // Archiving in progress
        displayProgressBar(result.success_rate, result.error_rate);
    } else {
        $("#archiving-progress").addClass("hidden");
        if (result.fail_count != 0) {
            $("#archive-errors-btn").removeClass("hidden");
        }
        $("#celery-archiving").removeClass("disabled");
        disconnectArchivingSSE();
    }
}

var eventSource;

function connectArchivingSSE() {
    let url_progress = $("#archiving-progress").attr("data-url-progress");
    eventSource = new EventSource(url_progress);
    eventSource.addEventListener('message', handleArchivingSSEMessage);
    eventSource.addEventListener('error', handleArchivingSSEConnectionError);
    console.log("Connection opened.");
}

function disconnectArchivingSSE() {
    if (eventSource) {
    eventSource.close();
    eventSource = null;
    }
    console.log("Connection closed.");
}

function handleArchivingSSEConnectionError(event) {
  if (eventSource.readyState === EventSource.CLOSED) {
    // La connexion a été fermée, relancez la connexion SSE
    connectArchivingSSE();
  }
}

function handleArchivingSSEMessage(event) {
    console.log("Connection message received.");
    var result = JSON.parse(event.data);
    updateProgressBar(result);
}

function checkProgress() {
    // If you reload the web page, we need to check if tasks are still being processed
    let url_progress = $("#archiving-progress").attr("data-url-progress") + "?format=json";

    $.ajax({
        url: url_progress,
        success: function (result) {
            if (result.status == 'consuming_queue') {
                updateProgressBar(result);
                connectArchivingSSE();
            }
        }
    });
}

$(document).ready(function () {
    $("#celery-archiving").click(function () {
        displayProgressBar(0, 0);
        connectArchivingSSE();
    });

    checkProgress();
});
