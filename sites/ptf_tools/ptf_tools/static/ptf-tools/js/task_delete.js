$("#deleteTaskModal").on("show.bs.modal", function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var url = button.data("url"); // Extract info from data-* attributes
    var args = button.data("taskargs");
    var modal = $(this);
    modal.find("#delete-form").attr("action", url);
    modal.find("#task-args").text(args);
});
