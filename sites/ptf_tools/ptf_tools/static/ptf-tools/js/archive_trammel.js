function displayProgressBar(success_rate, error_rate) {
    // Show progress bar
    $("#archiving-success_rate").removeClass("hidden");
    $("#results").removeClass("hidden");

    // Handle buttons
    $("#celery-archiving").addClass("disabled");  // Prevent other clicks on the Archive button
    $("#archive-errors-btn").addClass("hidden");  // Hide error button while archiving is in progress

    // Update progress bar
    $(".progress-bar-success")
        .css("width", success_rate + "%")
        .text(success_rate + "%");
    $(".progress-bar-warning")
        .css("width", error_rate + "%")
        .text(error_rate + "%");
}

var eventSource;

function connectArchivingSSE() {
    let url_progress = $("#archiving-progress").attr("data-url-progress");
    eventSource = new EventSource(url_progress);
    eventSource.addEventListener('message', handleArchivingSSEMessage);
    eventSource.addEventListener('error', handleArchivingSSEConnectionError);
    console.log("Connection opened.");
}

function disconnectArchivingSSE() {
    if (eventSource) {
    eventSource.close();
    eventSource = null;
    }
    console.log("Connection closed.");
}

function handleArchivingSSEConnectionError(event) {
  if (eventSource.readyState === EventSource.CLOSED) {
    // La connexion a été fermée, relancez la connexion SSE
    connectArchivingSSE();
  }
}

function handleArchivingSSEMessage(event) {
    console.log("Connection message received.");
    var result = JSON.parse(event.data);

    if (result.status == "consuming_queue") {
        // Archiving in progress
        displayProgressBar(result.success_rate, result.error_rate);

        let report = "Remaining tasks : " + result.remaining_count;
        report += "<br>Errors : " + result.fail_count;
        report += "<br>Successes : " + result.success_count;
        report += "<br>Total : " + result.all_count;
        report = "<br>Last Task : " + result.last_task;
        $("#results").html(report);

    } else {
        $("#archiving-success_rate").addClass("hidden");
        if (result.fail_count != 0) {
            $("#archive-errors-btn").removeClass("hidden");
        }
        $("#celery-archiving").removeClass("disabled");
        disconnectArchivingSSE();
    }
}

$(document).ready(function () {
    $("#celery-archiving").click(function () {
        displayProgressBar(0, 0);
    });
    connectArchivingSSE();
});
