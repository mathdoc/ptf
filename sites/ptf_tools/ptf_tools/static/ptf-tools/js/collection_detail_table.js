function update_issue_row(line) {
    let row, pid, url, colid, exception, traceback, target;
    [row, pid] = line.attr("id").split("!");
    colid = pid.split("_")[0];
    url = "/api/issue/" + pid + "/";
    $.get(url).done(function (result) {
        // Last events
        $("time#lastevent-import-" + row)
            .html(result["import"])
            .attr("datetime", result["import"]);
        $("time#lastevent-matching-" + row)
            .html(result["matching"])
            .attr("datetime", result["matching"]);
        $("time#lastevent-archive-" + row)
            .html(result["archive"])
            .attr("datetime", result["archive"]);
        $("time#lastevent-deploy-" + row)
            .html(result["deployed_date"])
            .attr("datetime", result["deployed_date"]);
        // Metada deploy and archive statuses
        if (result["deployed_date"] != null) {
            $("a#archive-" + row).toggleClass("hidden");
            if (result["deployed_date"] < result["last_modified"]) {
                $("span#metadata-" + row).html("Le site de production n'est pas à jour");
            }
        } else {
            $("span#metadata-" + row).html("Le site de production n'est pas à jour");
        }
        // DOI Status
        if (result["all_doi_are_registered"]) {
            $("span#doi-" + row).html("Tous les DOIs sont enregistrés");
            $("span#doi-" + row).addClass("text-success");
        } else {
            if (result["doi"]) {
                $("span#doi-" + row).html(
                    "<a href='/issues/" +
                        pid +
                        "' class='text-warning'>Au moins un DOI n'est pas enregistré</a>"
                );
            } else {
                $("span#doi-" + row).html(colid + " n'a pas de DOI");
                $("span#doi-" + row).addClass("text-danger");
            }
        }
        if (result["has_articles_excluded_from_publication"]) {
            $("span#exclude-" + row).show();
        }
        // Last event in Error if available
        if (result["latest"]) {
            [exception, traceback] = result["latest"].split(" - ");
            target = result["latest_target"];
            let button_title =
                '<i class="glyphicon glyphicon-fire"></i>  Error : ' + result["latest_type"];
            if (target != "") {
                if (target == "test_website") {
                    target = "Test";
                } else {
                    target = "Prod";
                }
                button_title = button_title + " " + target;
            }
            $("button#latest-" + row).toggleClass("hidden");
            $("button#latest-" + row).attr("data-content", traceback);
            $("button#latest-" + row).attr(
                "data-original-title",
                exception.slice(0, 140) + "<br>" + result["latest_date"]
            );
            $("button#latest-" + row).html(button_title);
        }
        // on marque la ligne comme traitée
        // ainsi, la prochaine fois que la table est redessinée, cette ligne n'apparaitra pas
        // dans la liste des lignes à mettre à jour
        line.removeClass("issue-row");
        line.addClass("issue-row-processed");
    });
}

$("#collection_detail").on("draw.dt", function () {
    // à chaque fois que la table est "dessinée", on met à jour les infos
    // présentes dans les nouvelles lignes qui apparaissent dans le tableau
    $(".issue-row").each(function () {
        update_issue_row($(this));
    });
    // et on ré-initialize le plugin popover de bootstrap
    // pour que les erreurs puissent être affichées
    $(function () {
        $('[data-toggle="popover"]').popover();
    });
});

// Initialize datatables
$(document).ready(function () {
    $("#collection_detail").DataTable({ ordering: false });
});

// Change transfer and delete button targets when selecting (test, prod, numdam, ptf-tools)
$(document).on("click", ".select_target", function (event) {
    row = $(this).attr("data-row");
    value = $(this).attr("value");
    $("#deploy-one-site-sel-" + row).attr("value", value);
    var old_value = $("#deploy-one-site-sel-" + row).html();
    $("#deploy-one-site-sel-" + row).html(
        old_value.replace(/[ a-zA-Z]* <span/g, $(this).html() + " <span")
    );
});

// Disable buttons when a volume is being transfered/deleted
$(document).on("click", ".handle-one-button", function (event) {
    data_url = $(this).attr("data-url");
    if (data_url.includes("deploy_jats_issue") || data_url.includes("delete_jats_issue")) {
        toolbar = $(this).parentsUntil($("div.related-actions"));
        toolbar.find("a.handle-one-button").addClass("disabled");
        toolbar.find("button.dropdown-toggle").attr("disabled", "disabled");
    }
});
