function prepareAllIssueProgress() {
    $(".progress-bar").css("width", "0%").attr("aria-valuenow", 0).text("0%");

    $("#handle-all-progress").show();
    $(".handle-all-button").prop("disabled", true);
    $("#cancel-all-button").show();

    var cb = $("#cancel-all-checkbox");
    cb.prop("checked", false);
    cb.val(cb.prop("checked"));

    // Disable check/uncheck/delete all-ids buttons
    $("#check-all-ids").addClass("disabled");
    $("#uncheck-all-ids").addClass("disabled");
    $("#delete-all-ids").addClass("disabled");
}

function resetAllIssueProgress() {
    $("#handle-all-progress").hide();
    $(".handle-all-button").prop("disabled", false);
    $("#cancel-all-button").hide();

    var cb = $("#cancel-all-checkbox");
    cb.prop("checked", false);
    cb.val(cb.prop("checked"));

    // Enable check/uncheck/delete all-ids buttons
    $("#check-all-ids").removeClass("disabled");
    $("#uncheck-all-ids").removeClass("disabled");
    $("#delete-all-ids").removeClass("disabled");

    // Show all Trashes
    $("a.externalid-trash").removeClass("hidden");
    $("a.externalid-update").removeClass("hidden");
}

function notify_user(result, url, delay) {
    text = url + " : <br><strong>" + result.status + "</strong> " + result.responseText;
    if (result.status == 200 || result.status == 201) {
        title = " Succès";
        type = "success";
        text = "Opération terminée";
    } else if (result.status == 408) {
        title = " Warning";
        type = "notice";
    } else {
        title = " Échec";
        type = "error";
        text = "Opération non effectuée\n";
        text += result.status ? result.status + " Error: " : ""
        text += result.statusText;
    }

    if (result.deploy) {
        text += " sur le site de " + result.deploy + ".";
        if (result.status != 200) {
            text += "\nErreur " + result.status;
            text += " : " + result.statusText;
        }
    }

    new PNotify({
        title: title,
        text: text,
        type: type,
        styling: "bootstrap3",
        delay: delay ? delay : "3000",
    });
}

function handleOneIssue(one_url, ids, count, reload, success_callback, end_url) {
    if (ids.length == 0 && typeof end_url != "undefined") {
        one_url = end_url;
        ids.push("");
        // set end_url to undefined
        end_url = void 0;
    }

    if (ids.length > 0) {
        id = ids.shift();
        //console.log(id);

        var n = one_url.indexOf("toreplacepid");
        if (one_url.slice(1, 10) == "match-one") {
            url = one_url.slice(0, n) + id + "/";
        } else {
            url = one_url.replace("toreplacepid", id);
        }
        $.get(url)
            .done(function (result) {
                var count_ = ids.length;
                if (end_url) {
                    count_ += 1;
                }
                var value = Math.round((100 * (count - count_)) / count);
                $(".progress-bar")
                    .css("width", value + "%")
                    .attr("aria-valuenow", value)
                    .text(value + "%");

                if (typeof success_callback != "undefined" && success_callback != null) {
                    success_callback(url, result);
                }

                if ($("#cancel-all-checkbox").is(":checked")) {
                    new PNotify({
                        title: " Echec",
                        text: "Opération annulée",
                        type: "notice",
                        styling: "bootstrap3",
                    });

                    console.log("Opération annulée");
                    resetAllIssueProgress();

                    if (reload) {
                        window.location.reload();
                    }
                } else {
                    handleOneIssue(one_url, ids, count, reload, success_callback, end_url);
                }
            })
            .fail(function (result) {
                notify_user(result, url);
                handleOneIssue(one_url, ids, count, reload, success_callback, end_url);
            });
    } else {
        new PNotify({
            title: " Succès",
            text: "Opération terminée",
            type: "success",
            styling: "bootstrap3",
        });
        resetAllIssueProgress();

        if (reload) {
            window.location.reload(true);
        }
    }
}

function handleAllButton(obj, success_callback) {
    prepareAllIssueProgress();

    var url = $(obj).attr("data-all-url");
    var one_url = $(obj).attr("data-one-url");
    var end_url = $(obj).attr("data-end-url");
    var site_select_id = $(obj).attr("data-site-select-id");
    var reload = $(obj).attr("data-reload");

    if (site_select_id) {
        var site = $("#" + site_select_id).val();
        one_url = one_url.replace("toreplacesite", site);
    }
    url = url.replace("toreplacesite", site);
    $.get(url)
        .done(function (result) {
            ids = result.ids;
            var count = ids.length;
            if (end_url) {
                count += 1;
            }
            handleOneIssue(one_url, ids, count, reload, success_callback, end_url);
        })
        .fail(function (result) {
            resetAllIssueProgress();
            notify_user(result, url);
        });
}

function fetch_id(link) {
    // show spinner while waiting fetching result
    let url = link.attr("data-url");

    if (url != '') {
        let spinner = link.next();
        spinner.removeClass("hidden");

        $.get(url)
            .fail(function (result) {
                notify_user(result, url);
            })
            .always(function (result) {
                show_fetched_id(url, result);
            });
    }
}

function show_fetched_id(one_url, result) {
    if (result.status != 200) {
        data = "<span class='text-danger'>Fetch failed !</span>";
    } else {
        data = result.result;
    }
    var dom_url = $("[data-url='" + one_url + "']");
    var dom = dom_url.parent().parent().next().children(".externalid-result");
    dom.html(data);
    dom.removeClass("hidden");

    // hide spinner now that query is done
    dom_url.next().addClass("hidden");
}

function show_matching_result(url, matching_result) {
    platform = {
        "mr-item-id": "MR",
        "zbl-item-id": "Zbl",
        "numdam-id": "Numdam",
        "jfm-item-id": "Jfm",
        "doi": "DOI",
        "pmid": "PMID",
    };
    let pid, sequence, what, nothing, value;
    [pid, sequence, what, nothing, value] = matching_result.message.split(" ");
    if (value != "") {
        if (sequence == 0) {
            url = "/api/extid/" + pid + "/" + what + "/";
        } else {
            url = "/api/bibitemid/" + pid + "/" + what + "/" + sequence + "/";
        }
        $.get(url)
            .done(function (result) {
                // get hidden form-template
                template = $(".form-template-" + sequence + ".hidden").first();
                // get a new form-template for later matchings and insert it after the current one
                $.get("/extid/form/" + sequence + "/").done(function (result) {
                    template.after(result);
                    template.next().find(".externalid").bootstrapToggle();
                });
                // fill-in form-template
                template.find("span.externalid-link").first().attr("data-url", result.fetch);
                template.find("a.externalid-link-blank").first().attr("href", result.href);
                template
                    .find("span.externalid-link")
                    .first()
                    .append(platform[what] + " " + value);
                template.find("input.externalid-checked").first().attr("data-url", result.check);
                template.find("input.externalid-other").first().attr("data-url", result.uncheck);
                template.find("a.externalid-trash").first().attr("href", result.delete);
                template.find("a.externalid-update").first().attr("href", result.update);
                // fetch id
                fetch_id(template.find("span.externalid-link").first());
                // hide trash button (to avoid reload of the page)
                template.find("a.externalid-trash").first().addClass("hidden");
                template.find("a.externalid-update").first().addClass("hidden");

                // PubMed matching is checked by default
                if (result.is_valid) {
                    template.find("span.externalid-link").first().addClass("externalid-valid");
                    template.find("input.externalid-checked").first().prop("checked", true);
                }

                // reveal form-template
                template.removeClass("hidden");
            })
            .fail(function (result) {
                console.log(url + " : " + result.status + result.statusText);
            });
    }
}


$(document).ready(function () {
    var clipboard = new Clipboard(".copy-button");
    
    $(document).on("click", ".handle-all-button", function (event) {
        handleAllButton(this);
    });

    $(document).on("click", ".open-matching-dialog", function (event) {
        var pid = $(this).data("pid");
        var data_url = $("#matching-form").attr("data-all-url");
        data_url = data_url.replace("toreplaceallpid", pid);
        $("#matching-form").attr("data-all-url", data_url);
    });

    $(document).on("click", "#matching-check-all-platforms", function (event) {
        $(this).parent().find("input[type='checkbox']").prop("checked", true);
    });

    $(document).on("click", "#matching-submit-form", function (event) {
        $("#MatchingModal .close").click();
        /*$("#MatchingModal").hide();*/

        // get form data
        var form = $("#matching-form");
        var formData = form.serializeArray();
        force = formData.shift()["value"];
        let what = "";
        formData.forEach(function (item, index) {
            what += item["value"];
            if (index != formData.length - 1) {
                what += "_";
            }
        });

        // modify url
        let [i, match, pid, l, m, n] = $("#matching-form").attr("data-all-url").split("/");
        data_url = [i, match, pid, what, force, n].join("/");
        $("#matching-form").attr("data-all-url", data_url);

        // destroy the display of some externalids depending on force and what values
        if (force != 0) {
            existing_ids = $("div[class*='form-template-']:not(.hidden)");
            // pick only ids within the content of what
            filtered_ids = $();
            for (let id_type of what.split("_")) {
                filtered_ids = filtered_ids.add(
                    existing_ids.has("span[data-url*='" + id_type + "']")
                );
            }
        }
        if (force == 1) {
            // we destroy only unchecked ids
            filtered_ids
                .has("span.externalid-link:not(.externalid-valid):not(.false-positive)")
                .remove();
        }
        if (force == 2) {
            // we destroy all ids
            filtered_ids.remove();
        }

        // begin matching
        handleAllButton(form, show_matching_result);
    });

    $(document).on("change", ".externalid-checked", function (event) {
        let input = $(this);
        let url = input.attr("data-url");
        $.get(url).done(function (result) {
            let link_label = input.parentsUntil(".row").prev().children().first();
            link_label.toggleClass("externalid-valid");
        });
    });

    $(document).on("change", ".externalid-other", function (event) {
        let input = $(this);
        let url = input.attr("data-url");
        $.get(url).done(function (result) {
            let link_label = input.parents(".externalid-checkbox").prev().children().first();
            let check_input = input
                .parents(".externalid-checkbox")
                .find(".externalid-checked")
                .first();
            let check_toggler = check_input.parent().parent();
            link_label.toggleClass("false-positive");
            // hide/show the other toggler
            check_toggler.toggleClass("hidden");
            // if the other toggler is checked, change its state
            // calls : .on("change", ".externalid-checked",
            if (check_input.prop("checked")) {
                check_input.bootstrapToggle("toggle");
            }
        });
    });

    $(document).on("click", ".handle-one-button", function (event) {
        var button = $(this);
        var url = $(this).attr("data-url");
        var id_select = $(this).attr("data-select-id");
        var site = $("#" + id_select).val();
        console.log("cou" , id_select)
        url = url.replace("toreplacesite", site);

        var id_indicator = $(this).attr("data-matching-indicator");
        $("#" + id_indicator).removeClass("hidden");
        var reload = $(this).attr("data-reload");

        var spinner = document.getElementById("status-spinner");
        if (spinner) { spinner.style.display = "inline-block"; }

        $.get(url)
            .done(function (result) {
                if (!reload) {
                    notify_user(result, url);
                }
            })
            .fail(function (result) {
                if (!reload) {
                    notify_user(result, url);
                }
            })
            .always(function (result) {
                if (url.includes("deploy_jats_issue") || url.includes("delete_jats_issue")) {
                    toolbar = button.parentsUntil($("div.related-actions"));
                    toolbar.find("a.handle-one-button").removeClass("disabled");
                    toolbar.find("button.dropdown-toggle").removeAttr("disabled");
                }
                $("#" + id_indicator).addClass("hidden");
                if (spinner) { spinner.style.display = "none"; }
                if (reload) {
                    window.location.reload();
                }
            });
    });

    $(document).on("click", "#cancel-all-button", function (event) {
        var cb = $("#cancel-all-checkbox");
        cb.prop("checked", true);
        cb.val(cb.prop("checked"));
    });

    $(document).on("click", ".externalid-link", function (event) {
        fetch_id($(this));
    });

    $(document).on("click", "#importcedrics", function (event) {
        $(this).button("loading");
        $("#close-importcedrics-modal").toggleClass("hidden");
    });

    $(document).on("click", "#diffcedrics", function (event) {
        $(this).button("loading");
        $("#close-diffcedrics-modal").toggleClass("hidden");
    });

    $(document).on("click", "#fetch-all-external-link-button", function (event) {
        $(".externalid-link").each(function () {
            fetch_id($(this));
        });
    });

    $(document).on("click", ".handle-doaj-button", function (event) {
        var url = $(this).attr("data-url");
        var id_indicator = $(this).attr("data-matching-indicator");
        $("#" + id_indicator).removeClass("hidden");

        fetch(url)
            .then((response) => notify_user(response, url))
            .catch((error) => console.log(error))
            .finally(() => $("#" + id_indicator).addClass("hidden"));
    });

});

$(function () {
    $(".collapse.plus")
        .on("shown.bs.collapse", function () {
            $(this)
                .parent()
                .find(".glyphicon-plus")
                .removeClass("glyphicon-plus")
                .addClass("glyphicon-minus");
        })
        .on("hidden.bs.collapse", function () {
            $(this)
                .parent()
                .find(".glyphicon-minus")
                .removeClass("glyphicon-minus")
                .addClass("glyphicon-plus");
        });

    /**if ($("#table_issues").length !== 0) {
        var url = $("#table_issues").attr("data-url");
        var TABLE = $("#table_issues").DataTable({
            pageLength: 50,
            lengthMenu: [50, 100],
            bSort: false,
            language: {
                search: "Rechercher",
                lengthMenu: "Afficher   _MENU_    par page",
                infoFiltered: "(filtré de _MAX_ entrées)",
                info: " _START_ à _END_ de _TOTAL_ entries",
                infoEmpty: "Pas de résultats",
                zeroRecords: "Pas de résultats",
                paginate: {
                    next: "Suivant",
                    previous: "Précédent",
                },
            },
            deferLoading: 10,
            ajax: {
                url: url,
            },
            columns: [
                {
                    data: "col1",
                    width: "250px",
                    render: function (data, type, row, meta) {
                        return data;
                    },
                },
                {
                    data: "col2",
                    width: "400px",
                    render: function (data, type, row, meta) {
                        return data;
                    },
                },
                {
                    data: "col3",
                    render: function (data, type, row, meta) {
                        return data;
                    },
                },
                {
                    data: "col4",
                    render: function (data, type, row, meta) {
                        return data;
                    },
                },
                {
                    data: "col5",
                    render: function (data, type, row, meta) {
                        return data;
                    },
                },
            ],
        });

        TABLE.on("draw", function () {
            $("#issues-loading-indicator").hide();
        });
    }
    **/
    if ($("#articles_count").length !== 0) {
        var ARTICLES_TABLE = $("#articles_count").DataTable({
            ordering: false,
            paging: false,
            searching: false,
            columnDefs: [
                {
                    width: "10%",
                    targets: 0,
                },
            ],
            dom: "Bt",
            buttons: ["csvHtml5"],
        });
    }

    if ($("#articles_count2").length !== 0) {
        var ARTICLES_TABLE2 = $("#articles_count2").DataTable({
            ordering: false,
            paging: false,
            searching: false,
            columnDefs: [
                {
                    width: "10%",
                    targets: 0,
                },
            ],
            dom: "Bt",
            buttons: ["csvHtml5"],
        });
    }
});
