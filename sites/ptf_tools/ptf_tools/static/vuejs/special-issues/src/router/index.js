import { createRouter, createWebHistory } from 'vue-router'
import Special from '../views/Special.vue'

const router = createRouter({
    routes: [
        {
            path: '/',
            name: 'home',
            component: Special
        }
    ]
})

export default router