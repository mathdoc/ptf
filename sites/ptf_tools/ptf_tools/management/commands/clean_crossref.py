from django.core.management.base import BaseCommand

from ptf import model_helpers
from ptf.models import Container
from ptf_tools.doi import removeOldDataInCrossref


class Command(BaseCommand):
    help = "Deploy a container to a (test) website, only the metadata"

    def add_arguments(self, parser):
        parser.add_argument(
            "-pid",
            action="store",
            dest="pid",
            type=str,
            default=None,
            required=True,
            help="Resource pid",
        )
        parser.add_argument(
            "-start_pid",
            action="store",
            dest="start_pid",
            type=str,
            default=None,
            required=False,
            help="Resource pid",
        )

    def clean_crossref(self, container):
        for article in container.article_set.all():
            removeOldDataInCrossref(article, testing=False)

    def handle(self, *args, **kwargs):
        pid = kwargs["pid"]
        start_pid = kwargs["start_pid"]

        if "_" in pid:
            container = model_helpers.get_container(pid)
            if not container:
                raise RuntimeError(f"{pid} does not exist")

            self.clean_crossref(container)
        else:
            year = 2002
            start = start_pid is None
            while year < 2020:
                containers = Container.objects.filter(pid__startswith=f"{pid}_{year}").order_by(
                    "pid"
                )
                for container in containers:
                    if not start and container.pid == start_pid:
                        start = True

                    if start:
                        print(container)
                        self.clean_crossref(container)
                year += 1
