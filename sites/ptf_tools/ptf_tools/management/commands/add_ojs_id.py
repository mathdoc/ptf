from django.core.management.base import BaseCommand

from ptf import model_helpers
from ptf.cmds import ptf_cmds
from ptf.display import resolver
from ptf.models import *


class Command(BaseCommand):
    help = "Recompile the front page of an article, even if it is already published"

    def add_arguments(self, parser):
        parser.add_argument(
            "-pid",
            action="store",
            dest="pid",
            type=str,
            default=None,
            required=True,
            help="Resource pid",
        )

    def add_ojs_id(self, article, ojs_id):
        if not article.get_ojs_id():
            print(article.pid, article.doi, ojs_id)
            cmd = ptf_cmds.updateResourceIdPtfCmd({"id_type": "ojs-id", "id_value": ojs_id})
            cmd.set_resource(article)
            cmd.do()

    def handle(self, *args, **kwargs):
        colid = kwargs["pid"]

        collection = model_helpers.get_collection(colid)

        for issue in collection.content.all().order_by("pid"):
            tex_src_folder = resolver.get_cedram_issue_tex_folder(colid, issue.pid)
            tex_folders, _ = resolver.get_cedram_tex_folders(colid, issue.pid)

            if tex_folders:
                i = 0
                for article in issue.article_set.all():
                    ojs_id = tex_folders[i]
                    self.add_ojs_id(article, ojs_id)
                    i += 1
