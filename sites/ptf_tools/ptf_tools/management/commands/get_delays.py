from django.core.management.base import BaseCommand

from ptf import model_helpers
from ptf.display import resolver
from ptf.models import os


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        stats = {
            "CRMATH": {},
            "CRMECA": {},
            "CRPHYS": {},
            "CRGEOS": {},
            "CRCHIM": {},
            "CRBIOL": {},
        }

        for colid in ["CRMATH", "CRCHIM", "CRMECA", "CRPHYS", "CRBIOL", "CRGEOS"]:
            collection = model_helpers.get_collection(colid)
            months = {}
            for issue in collection.content.all().order_by("pid"):
                tex_src_folder = resolver.get_cedram_issue_tex_folder(colid, issue.pid)
                tex_folders, _ = resolver.get_cedram_tex_folders(colid, issue.pid)
                i = -1

                for article in issue.article_set.all():
                    i = 0

                    if article.date_published is not None or article.date_online_first is not None:
                        date_received = article.date_received
                        date_accepted = article.date_accepted

                        if date_accepted is None:
                            tex_file = os.path.join(
                                tex_src_folder, tex_folders[i], tex_folders[i] + ".tex"
                            )
                            with open(tex_file, encoding="utf-8") as f:
                                for line in f:
                                    if line.find("datereceived") > 0 and date_received is None:
                                        date_ = line.split("{")[1].split("}")[0]
                                        date_received = model_helpers.parse_date_str(date_)
                                    if line.find("dateaccepted") > 0 and date_accepted is None:
                                        date_ = line.split("{")[1].split("}")[0]
                                        date_accepted = model_helpers.parse_date_str(date_)

                        month = (
                            article.date_published.month
                            if article.date_published is not None
                            else article.date_online_first.month
                        )

                        acceptation = None
                        if date_received is not None and date_accepted:
                            acceptation = date_accepted - date_received

                        publication = None
                        if date_accepted is not None and article.date_published is not None:
                            publication = article.date_published - date_accepted
                        if date_accepted is not None and article.date_online_first is not None:
                            publication = article.date_online_first - date_accepted

                        if acceptation is not None or publication is not None:
                            if month not in months:
                                months[month] = {}

                        if acceptation is not None and acceptation.days > 0:
                            if "acceptation" not in months[month]:
                                months[month]["acceptation"] = [acceptation.days]
                            else:
                                months[month]["acceptation"].append(acceptation.days)
                            if "acceptation" not in stats[colid]:
                                stats[colid]["acceptation"] = [acceptation.days]
                            else:
                                stats[colid]["acceptation"].append(acceptation.days)

                        if publication is not None and publication.days > 0:
                            if "publication" not in months[month]:
                                months[month]["publication"] = [publication.days]
                            else:
                                months[month]["publication"].append(publication.days)
                            if "publication" not in stats[colid]:
                                stats[colid]["publication"] = [publication.days]
                            else:
                                stats[colid]["publication"].append(publication.days)

            stats[colid]["months"] = months

        for colid in stats:
            stat = stats[colid]
            avg_acceptation = avg_publication = 0
            if "acceptation" in stat and len(stat["acceptation"]) > 0:
                avg_acceptation = sum(stat["acceptation"]) / len(stat["acceptation"])
            if "publication" in stat and len(stat["publication"]) > 0:
                avg_publication = sum(stat["publication"]) / len(stat["publication"])

            months = []
            for month in range(1, 13):
                avg_month_acceptation = avg_month_publication = 0
                if month in stat["months"]:
                    if "acceptation" in stat["months"][month]:
                        avg_month_acceptation = sum(stat["months"][month]["acceptation"]) / len(
                            stat["months"][month]["acceptation"]
                        )
                    if "publication" in stat["months"][month]:
                        avg_month_publication = sum(stat["months"][month]["publication"]) / len(
                            stat["months"][month]["publication"]
                        )
                months.append((int(avg_month_acceptation), int(avg_month_publication)))

            print(colid, int(avg_acceptation), int(avg_publication), months)
