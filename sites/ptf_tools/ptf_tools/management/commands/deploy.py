import requests

from django.conf import settings
from django.core.management.base import BaseCommand
from django.urls import reverse

from ptf import model_helpers
from ptf.cmds import ptf_cmds
from ptf.display import resolver
from ptf.models import Container
from ptf_tools.models import ResourceInNumdam


class Command(BaseCommand):
    help = "Deploy a container to a (test) website, only the metadata"

    def add_arguments(self, parser):
        parser.add_argument(
            "-pid",
            action="store",
            dest="pid",
            type=str,
            default=None,
            required=True,
            help="Resource pid",
        )
        parser.add_argument(
            "-site",
            action="store",
            dest="site",
            type=str,
            default=None,
            required=True,
            help="website, test_website or numdam",
        )
        parser.add_argument(
            "-start_pid",
            action="store",
            dest="start_pid",
            type=str,
            default=None,
            required=False,
            help="Resource pid",
        )

    def deploy(self, pid, site):
        container = model_helpers.get_container(pid)
        if not container:
            raise RuntimeError(f"{pid} does not exist")

        collection = container.get_collection()

        if site == "numdam":
            server_url = settings.NUMDAM_PRE_URL
            ResourceInNumdam.objects.get_or_create(pid=container.pid)

            # Add Djvu (before exporting the XML)
            if int(container.year) < 2020:
                for article in container.article_set.all():
                    try:
                        cmd = ptf_cmds.addDjvuPtfCmd()
                        cmd.set_resource(article)
                        cmd.do()
                    except Exception as e:
                        print(f"Djvu error for {article.pid}, {e}")
        else:
            server_url = getattr(collection, site)()
            if not server_url:
                raise RuntimeError(f"The collection has no {site}")

        if site == "website":
            mersenne_site = model_helpers.get_site_mersenne(collection.pid)
            # create or update deployed_date on container and articles
            model_helpers.update_deployed_date(container, mersenne_site)
            container = model_helpers.get_container(pid)

        export_to_website = site == "website"
        with_djvu = site == "numdam"
        xml = ptf_cmds.exportPtfCmd(
            {"pid": pid, "with_djvu": with_djvu, "export_to_website": export_to_website}
        ).do()
        body = xml.encode("utf8")

        # if container = issue
        if container.ctype == "issue":
            url = server_url + reverse("issue_upload")
        else:
            url = server_url + reverse("book_upload")

        response = requests.post(url, data=body, verify=False)
        status = response.status_code

        if status < 200 or status > 204:
            print("Error, status code", status)
        elif site == "numdam":
            from_folder = settings.MERSENNE_PROD_DATA_FOLDER
            if collection.pid in settings.NUMDAM_COLLECTIONS:
                from_folder = settings.MERSENNE_TEST_DATA_FOLDER

            resolver.copy_binary_files(container, from_folder, settings.NUMDAM_DATA_ROOT)
            for article in container.article_set.all():
                resolver.copy_binary_files(article, from_folder, settings.NUMDAM_DATA_ROOT)

    def handle(self, *args, **kwargs):
        pid = kwargs["pid"]
        site = kwargs["site"]
        start_pid = kwargs["start_pid"]

        if "_" in pid:
            self.deploy(pid, site)
        elif pid not in ["CRMATH", "CRMECA", "CRPHYS", "CRGEOS", "CRCHIM", "CRBIOL"]:
            start = start_pid is None
            containers = Container.objects.filter(pid__startswith=f"{pid}_").order_by("pid")
            for container in containers:
                if not start and container.pid == start_pid:
                    start = True

                if start:
                    print(container)
                    self.deploy(container.pid, site)
        else:
            year = 2002
            start = start_pid is None
            while year < 2020:
                containers = Container.objects.filter(pid__startswith=f"{pid}_{year}").order_by(
                    "pid"
                )
                for container in containers:
                    if not start and container.pid == start_pid:
                        start = True

                    if start:
                        print(container)
                        self.deploy(container.pid, site)
                year += 1
