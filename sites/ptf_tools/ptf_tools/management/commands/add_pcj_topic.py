import csv

import requests

from django.core.management.base import BaseCommand

from ptf.display.resolver import PCJ_MANDATORY_TOPICS
from ptf.models import Article
from ptf.models import Subj


class Command(BaseCommand):
    help = "Recompile the front page of an article, even if it is already published"

    def add_arguments(self, parser):
        parser.add_argument(
            "-csv",
            action="store",
            dest="csv",
            type=str,
            default=None,
            required=False,
            help="csv file with topics",
        )

    def read_csv(self, filename):
        with open(filename) as csvfile:
            csv_reader = csv.reader(csvfile, delimiter=";")

            for row in csv_reader:
                doi = row[0]
                topics = row[1].split(",")

                qs = Article.objects.filter(doi=doi)
                if qs.exists():
                    article = qs[0]

                    article.subj_set.filter(type="topic").delete()

                    for topic in topics:
                        self.add_topic(article, topic)

                    print(
                        doi,
                        ",".join(
                            [subj.value for subj in article.subj_set.all() if subj.type == "topic"]
                        ),
                    )

    def add_topic(self, article, topic):
        topics = [
            subj.value
            for subj in article.subj_set.all()
            if subj.type == "topic" and subj.value == topic
        ]

        if len(topics) == 0:
            s = Subj(
                resource=article,
                type="topic",
                lang="en",
                value=topic,
                seq=article.subj_set.count() + 1,
            )
            s.save()

    def add_mandatory_topics(self):
        for article in Article.objects.filter(my_container__my_collection__pid="PCJ").order_by(
            "doi"
        ):
            pci_section = article.get_pci_section()

            if pci_section == "":
                # Previous bug where the PCI section is not set ?
                # Get th section from Trammel
                url = "https://trammel.centre-mersenne.org/api-article-dump/" + article.doi
                response = requests.get(url)
                if response.status_code == 200:
                    data = response.json()
                    pci_section = data.get("pci_section", "")
                    print(article.doi, pci_section)

            mandatory_topic = (
                PCJ_MANDATORY_TOPICS[pci_section] if pci_section in PCJ_MANDATORY_TOPICS else None
            )

            if mandatory_topic is not None:
                self.add_topic(article, mandatory_topic)

    def handle(self, *args, **kwargs):
        filename = kwargs["csv"]

        if filename is not None:
            self.read_csv(filename)
        else:
            self.add_mandatory_topics()
