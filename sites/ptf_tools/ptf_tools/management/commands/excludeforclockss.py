from django.conf import settings
from django.core.management.base import BaseCommand

from ptf import model_helpers
from ptf.display.resolver import get_relative_folder
from ptf.models import Collection
from ptf.models import Container

# que veut on synchroniser ?
# - les articles et livres avec un DOI
# - les collections ayant des items avec un DOI

# lftp (utilisé dans ptf-tools/bin/sync_to_clockss.sh) copie récursivement le répertoire mathdoc_archive
# on peut lui passer en paramètre une liste de path à exclure
# LIST EXCLUDE FOR CLOCKSS construite ici


class Command(BaseCommand):
    help = "Export a list of folder to be excluded from CLOCKSS"

    def handle(self, *args, **options):
        # on cherche les containers dont au moins un des articles à un DOI null
        containers_with_articles_without_doi = Container.objects.filter(
            article__doi__isnull=True
        ).distinct()
        containers_with_articles_with_doi = Container.objects.filter(
            article__doi__isnull=False
        ).distinct()
        result = containers_with_articles_without_doi.intersection(
            containers_with_articles_with_doi
        )

        # self.stdout.write('---- repertoire a supprimer ----')

        paths_to_be_deleted = {}

        for container in containers_with_articles_without_doi:
            col = container.my_collection
            paths_to_be_deleted[container.pid] = get_relative_folder(col.pid, container.pid)

        for cid_to_be_deleted in settings.ISSUE_TO_APPEAR_PIDS.values():
            container = model_helpers.get_container(cid_to_be_deleted)
            if container is not None:
                paths_to_be_deleted[cid_to_be_deleted] = get_relative_folder(
                    container.my_collection.pid, container.pid
                )

        paths_to_be_deleted = sorted(paths_to_be_deleted.values())
        # self.stdout.write('---- repertoire a supprimer ----')
        for path in paths_to_be_deleted:
            self.stdout.write(f"{path}")

        # il ne faut pas synchroniser les collections qui n'ont aucun article ou container avec DOI

        col_with_article_without_doi = Collection.objects.filter(
            content__article__doi__isnull=True
        ).distinct()
        col_with_article_with_doi = Collection.objects.filter(
            content__article__doi__isnull=False
        ).distinct()
        col_result = col_with_article_without_doi.difference(col_with_article_with_doi)

        for col in col_result:
            self.stdout.write(f"{col.pid}")

        if result.count() > 0:
            str = ""
            for container in result:
                str = f"{str} {container.pid}"
            raise Exception(
                "Il y a des containers qui ont à la fois des articles avec DOI et sans : %s" % str
            )
