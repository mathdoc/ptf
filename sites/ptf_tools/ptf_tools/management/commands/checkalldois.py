import datetime

from django.core.management.base import BaseCommand

from ptf.models import Collection
from ptf_tools.doi import get_doibatch
from ptf_tools.doi import get_or_create_doibatch

# relance la vérification des DOI sur toute une collection


class Command(BaseCommand):
    help = "check DOI of all articles of a collection"

    def add_arguments(self, parser):
        parser.add_argument(
            "-pid",
            action="store",
            dest="pid",
            type=str,
            default=None,
            required=True,
            help="Collection pid",
        )

    def handle(self, *args, **kwargs):
        colid = kwargs["pid"]
        col = Collection.objects.get(pid=colid)
        print("Opération longue - selon le nbre d article")
        for con in col.content.all():
            for art in con.article_set.all():
                if art.doi is None:
                    print("Article sans DOI %s" % art.pid)
                else:
                    doib = get_doibatch(art)
                    # on supprime le doibatch pour relancer le check
                    if doib:
                        doib.delete()
                        art.doibatch = None
                    if art.my_container.year is None or art.my_container.year == "0":
                        art.my_container.year = datetime.datetime.now().strftime("%Y")
                    doib = get_or_create_doibatch(art)
                    if not doib:
                        print("DOI non enregistré : %s " % art.pid)
                    elif doib.status != "Enregistré":
                        print(f"Pb status : {doib.status} : {art.pid} ")
