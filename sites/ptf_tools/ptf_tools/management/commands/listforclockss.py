from django.core.management.base import BaseCommand

from ptf.display.resolver import get_relative_folder
from ptf.models import Container

# que veut on synchroniser ?
# - les articles et livres avec un DOI
# - les collections ayant des items avec un DOI

# lftp (utilisé dans ptf-tools/bin/sync_to_clockss.sh) copie récursivement le répertoire mathdoc_archive
# on peut lui passer en paramètre une liste de path à exclure
# LIST EXCLUDE FOR CLOCKSS construite ici


class Command(BaseCommand):
    help = "Export a list of article folder to be sync with CLOCKSS"
    # PB ! les XML sont niveau container

    def handle(self, *args, **options):
        # il me faut la liste des articles ayant un DOI et étant publié

        # published_articles_with_doi = Article.objects.filter(doi__isnull=False).filter(date_published__isnull=False)
        #
        # # self.stdout.write('---- article a sync (path) ----')
        # for article in published_articles_with_doi:
        #     #path = get_relative_folder(article.my_container.my_collection.pid, article.my_container.pid, article.pid)
        #     path = get_relative_folder(article.my_container.my_collection.pid, article.my_container.pid)
        #     self.stdout.write('{}'.format(path))
        #
        # self.stdout.write('-----------------')

        # on fait niveau container (on ne fait pas niveau collection car sinon c'est sync récursif
        # -> le niveau collection est traité dans le bash)

        # containers_with_published_articles_with_doi = Container.objects.filter(article__doi__isnull=False).\
        #    filter(article__date_published__isnull=False).distinct()  -> pas bon car un viel article n'a pas de date_published

        containers = [
            con
            for con in Container.objects.filter(article__doi__isnull=False).distinct()
            if con.deployed_date() is not None
        ]

        for container in containers:
            path = get_relative_folder(container.my_collection.pid, container.pid)
            self.stdout.write(f"{path}")

        # on sauvegarde par défaut même si dans ces containers, il y a des articles sans DOI ou non publié (1er niveau)
        # on averti juste si le cas est présent

        containers_with_articles_without_doi = Container.objects.filter(
            article__doi__isnull=True
        ).distinct()
        containers_with_articles_with_doi = Container.objects.filter(
            article__doi__isnull=False
        ).distinct()
        result = containers_with_articles_without_doi.intersection(
            containers_with_articles_with_doi
        )
        # result contient les containers pas forcément publiés contenant des articles sans doi et avec doi
        # on restreint à ceux publiés
        published_containers_with_articles_without_doi = [
            con for con in result if con.deployed_date() is not None
        ]

        if len(published_containers_with_articles_without_doi) > 0:
            str = ""
            for container in published_containers_with_articles_without_doi:
                str = f"{str} {container.pid}"
            raise Exception(
                "Il y a des containers publiés qui ont à la fois des articles avec DOI et sans : %s"
                % str
            )
