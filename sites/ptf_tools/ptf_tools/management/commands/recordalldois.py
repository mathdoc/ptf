import datetime

from django.core.management.base import BaseCommand

from ptf.models import Container
from ptf_tools.doi import recordDOI

# relance la vérification des DOI sur toute une collection


class Command(BaseCommand):
    help = "record DOI of all articles of a collection"

    def add_arguments(self, parser):
        parser.add_argument(
            "-pid",
            action="store",
            dest="pid",
            type=str,
            default=None,
            required=True,
            help="Collection pid",
        )

    def record_containers(self, qs):
        for container in qs:
            print(container)
            for article in container.article_set.all():
                if article.doi is None:
                    print(f"Article with no DOI: {article.pid}")
                else:
                    result = {"status": 404}
                    if article.doi and not article.do_not_publish:
                        if (
                            article.my_container.year is None
                        ):  # or article.my_container.year == '0':
                            article.my_container.year = datetime.datetime.now().strftime("%Y")
                        result = recordDOI(article)
                    if result["status"] > 400:
                        print(f"Error while recording DOI : {article.pid}")

    def handle(self, *args, **kwargs):
        pid = kwargs["pid"]

        if pid not in ["CRMATH", "CRMECA", "CRPHYS", "CRGEOS", "CRCHIM", "CRBIOL"]:
            qs = Container.objects.filter(pid__startswith=f"{pid}_").order_by("pid")
            self.record_containers(qs)
        else:
            year = 2002
            while year < 2020:
                qs = Container.objects.filter(pid__startswith=f"{pid}_{year}").order_by("pid")
                self.record_containers(qs)
                year += 1
