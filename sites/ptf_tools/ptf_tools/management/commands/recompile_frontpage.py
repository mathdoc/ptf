import subprocess

from django.core.management.base import BaseCommand

from ptf import model_helpers
from ptf import tex
from ptf.models import Article


class Command(BaseCommand):
    help = "Recompile the front page of an article, even if it is already published"

    def add_arguments(self, parser):
        parser.add_argument(
            "-pid",
            action="store",
            dest="pid",
            type=str,
            default=None,
            required=True,
            help="Resource pid",
        )
        parser.add_argument(
            "-start_id",
            action="store",
            dest="start_id",
            type=str,
            default=None,
            help="Articles are sorted before update. start_id is the article pid to start the update",
        )
        parser.add_argument(
            "-skip_compilation",
            action="store_true",
            dest="skip_compilation",
            default=False,
            help="No TeX compilation, only reassemble the PDFs",
        )

    def recompile(self, article, container, skip_compilation):
        # Recompile the PDF (new front page with old content (its front page is removed))
        # The new PDF is directly copied to MERSENNE_TEST_DATA
        try:
            tex.create_frontpage(
                container.my_collection.pid,
                container,
                [article],
                test=False,
                replace_frontpage_only=True,
                skip_compilation=skip_compilation,
            )
        except subprocess.CalledProcessError:
            file1 = open("/home/www-data/failed.txt", "a")  # append mode
            file1.write(f"{article.pid} {article.get_ojs_id()}\n")
            file1.close()

    def handle(self, *args, **kwargs):
        pid = kwargs["pid"]
        start_id = kwargs["start_id"]
        skip_compilation = kwargs["skip_compilation"]

        article = model_helpers.get_article(pid)
        if article:
            print(article.pid)
            self.recompile(article, article.my_container, skip_compilation)
        else:
            if pid in [
                "CML",
                "AIF",
                "CRMATH",
                "CRMECA",
                "CRPHYS",
                "CRGEOS",
                "CRBIOL",
                "CRCHIM",
                "AFST",
                "AMBP",
                "JTNB",
                "MRR",
                "MSIA",
                "OJMO",
                "PMB",
                "ROIA",
                "SMAI-JCM",
                "JEP",
            ]:
                if pid in ["CML", "MSIA", "OJMO"]:
                    year = 2020
                    end_year = 2022
                elif pid == "AIF":
                    year = 2015
                    end_year = 2022
                elif pid == "AFST":
                    year = 2019
                    end_year = 2023
                elif pid in ["AMBP", "JTNB"]:
                    year = 2017
                    end_year = 2022
                elif pid == "MRR":
                    year = 2020
                    end_year = 2023
                elif pid == "PMB":
                    year = 2019
                    end_year = 2022
                elif pid == "SMAI-JCM":
                    year = 2017
                    end_year = 2023
                elif pid in ["CRMATH", "CRMECA", "CRPHYS", "CRGEOS", "CRBIOL", "CRCHIM", "ROIA"]:
                    year = 2020
                    end_year = 2023
                elif pid == "JEP":
                    year = 2022
                    end_year = 2023

            start = start_id is None
            while year < end_year:
                articles = Article.objects.filter(
                    my_container__pid__startswith=f"{pid}_{year}"
                ).order_by("pid")

                for article in articles:
                    skip = False
                    if (
                        pid == "CRMATH"
                        and year == 2022
                        and not (0 < int(article.my_container.pid[-1]) < 5)
                    ):
                        skip = True
                    if (
                        pid == "CRMECA"
                        and year == 2022
                        and article.my_container.pid[-2:] not in ["G1", "G2"]
                    ):
                        skip = True
                    if (
                        pid == "CRPHYS"
                        and year == 2022
                        and article.my_container.pid[-2:] not in ["G1", "G2"]
                    ):
                        skip = True
                    if (
                        pid == "CRGEOS"
                        and year == 2022
                        and article.my_container.pid[-2:] not in ["G1", "G2"]
                    ):
                        skip = True
                    if pid == "CRBIOL" and year == 2022 and article.my_container.pid[-1] != "1":
                        skip = True
                    if (
                        pid == "CRCHIM"
                        and year == 2022
                        and article.my_container.pid[-2:] not in ["G1", "G2"]
                    ):
                        skip = True
                    if (
                        pid == "AFST"
                        and year == 2022
                        and not (0 < int(article.my_container.pid[-1]) < 4)
                    ):
                        skip = True
                    if pid == "AIF" and year == 2021 and article.my_container.pid[-1] == "6":
                        skip = True
                    if pid == "ROIA" and year == 2022 and article.my_container.pid[-1] == "6":
                        skip = True

                    if start and not skip:
                        print(article.pid)
                        self.recompile(article, article.my_container, skip_compilation)

                    if article.get_ojs_id() == start_id:
                        start = True

                year += 1
