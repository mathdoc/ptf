import json
import re

import requests

from django.conf import settings
from django.db.models import Q
from django.http import Http404

from mersenne_tools.models import DOAJBatch
from ptf import model_helpers
from ptf.cmds.xml.xml_utils import remove_html
from ptf.models import Container


def has_date_online_first(document):
    return hasattr(document, "date_online_first") and document.date_online_first


def has_date_published(document):
    return hasattr(document, "date_published") and document.date_published


def has_publication_date(document):
    return has_date_online_first(document) or has_date_published(document)


def is_published(document):
    if not hasattr(document, "do_not_publish"):
        return True
    return not document.do_not_publish


def get_names(resource, role):
    names = []
    for contribution in resource.contributions.all():
        if contribution.role == role:
            person = {"name": str(contribution)}
            addresses = contribution.contribaddress_set.all()
            if addresses:
                person["affiliation"] = "; ".join([c.address for c in addresses if c.address])
            if contribution.orcid:
                orcid = contribution.orcid.strip()
                orcid = orcid.encode("ascii", "ignore").decode("utf-8")
                if re.match(r"^\d{4}-\d{4}-\d{4}-\d{3}(\d|X)$", orcid):
                    person["orcid_id"] = "https://orcid.org/" + orcid
            names.append(person)
    return names


def get_token(colid):
    token = None
    if colid == "PCJ":
        token = settings.DOAJ_TOKEN_PCJ
    elif colid == "OJMO":
        token = settings.DOAJ_TOKEN_OJMO
    elif colid.startswith("CR") and len(colid) > 2:
        token = settings.DOAJ_TOKEN_CR
    return token


def doaj_pid_register(pid):
    resource = model_helpers.get_resource(pid)
    if not resource:
        raise Http404

    container = None
    if resource.classname == "Container":
        container = resource.container

    if not container:
        raise Http404

    collection = container.get_collection()
    if not collection:
        raise Http404

    results = []
    data, response = None, None
    token = get_token(collection.pid)
    if token:
        for article in resource.container.article_set.all():
            if is_published(article) and has_publication_date(article):
                data = doaj_resource_register(article)
                if data:
                    results.append(data)

    if results:
        url = f"https://doaj.org/api/bulk/articles?api_key={token}"
        response = requests.post(url, json=results)
        container_batch = DOAJBatch.objects.get_or_create(resource=resource)[0]
        if response.status_code == 201:
            container_batch.status = DOAJBatch.REGISTERED
            results = response.json()
            data = {
                "doaj_status": response.status_code,
                "doaj_message": [r["status"] for r in results],
                "doaj_id": [r["id"] for r in results],
                "doaj_location": [r["location"] for r in results],
            }
            for article in resource.container.article_set.all():
                if is_published(article) and has_publication_date(article):
                    article_batch = DOAJBatch.objects.get_or_create(resource=article)[0]
                    article_batch.status = DOAJBatch.REGISTERED
                    article_batch.save()
        else:
            container_batch.status = DOAJBatch.ERROR
            if response.text:
                container_batch.log = response.text
        container_batch.save()
    return data, response


def doaj_resource_register(resource):
    container = None
    if resource.classname == "Article":
        document = resource.article
        container = document.my_container
        fpage = document.fpage
        lpage = document.lpage
    elif resource.classname == "Container":
        document = resource.container
        container = document
        fpage = lpage = ""

    if not container:
        return None

    doi = resource.doi
    collection = container.get_collection()
    if not doi or not collection:
        return None

    if collection.pid.startswith("CR") and not doi.startswith("10.5802/cr"):
        return None

    month = year = ""
    if container.year != "0":
        year = container.year.split("-")[-1]

    if has_date_online_first(document):
        month = document.date_online_first.strftime("%B")
        year = document.date_online_first.strftime("%Y")
    elif has_date_published(document):
        month = document.date_published.strftime("%B")
        year = document.date_published.strftime("%Y")

    volume = number = ""
    if not container.to_appear():
        is_cr = container.is_cr()
        if container.volume:
            volume = container.volume
        if container.number and not (is_cr and container.number[0] == "G"):
            number = container.number

    eissn = collection.e_issn
    pissn = ""  # collection.issn
    colid = collection.pid.lower()
    domain = settings.SITE_REGISTER[colid]["site_domain"]
    if colid == "pcj":
        domain = "peercommunityjournal.org"

    url = f"https://{domain}/articles/{doi}/"
    lang = resource.lang if resource.lang and resource.lang != "und" else ""
    authors = get_names(resource, "author")
    publisher = container.my_publisher
    pub_name = publisher.pub_name if publisher and publisher.pub_name else ""

    data = {"admin": {}, "bibjson": {"journal": {}}}
    data["admin"]["publisher_record_id"] = doi
    data["bibjson"]["title"] = remove_html(document.title_tex)
    data["bibjson"]["month"] = month
    data["bibjson"]["year"] = year

    keywords = [
        kwd.value for kwd in document.kwd_set.all() if kwd.type != "msc" and kwd.lang == lang
    ]

    abstract = (
        document.abstract_set.all().filter(Q(lang="en") | Q(lang="und")).order_by("lang").first()
    )
    data["bibjson"]["abstract"] = remove_html(abstract.value_tex) if abstract else ""
    data["bibjson"]["author"] = authors
    data["bibjson"]["keywords"] = keywords
    data["bibjson"]["link"] = [{"url": url, "type": "fulltext", "content_type": "HTML"}]

    data["bibjson"]["identifier"] = [{"type": "doi", "id": doi}]
    if eissn:
        data["bibjson"]["identifier"].append({"type": "eissn", "id": eissn})
    if pissn:
        data["bibjson"]["identifier"].append({"type": "pissn", "id": pissn})
    if not eissn and colid == "pcj":
        data["bibjson"]["identifier"].append({"type": "eissn", "id": "2804-3871"})

    data["bibjson"]["journal"]["country"] = "FR"
    data["bibjson"]["journal"]["title"] = collection.title_tex
    data["bibjson"]["journal"]["start_page"] = fpage
    data["bibjson"]["journal"]["end_page"] = lpage
    data["bibjson"]["journal"]["language"] = [lang]
    data["bibjson"]["journal"]["number"] = number
    data["bibjson"]["journal"]["volume"] = volume
    data["bibjson"]["journal"]["publisher"] = pub_name
    return data


def doaj_delete_article(doi):
    colid = ""
    resource = model_helpers.get_resource_by_doi(doi)
    if resource:
        colid = resource.article.my_container.get_collection().pid

    token = get_token(colid)
    url = f"https://doaj.org/api/search/articles/{doi}"
    response = requests.get(url)
    if response.status_code == 200:
        results = response.json().get("results")
        if results:
            article_id = results[0].get("id", "")
            url = f"https://doaj.org/api/articles/{article_id}?api_key={token}"
            response = requests.delete(url)
            if response.status_code == 204:
                return doi + " deleted"
        else:
            return doi + " not found or article already deleted"
    return doi + " deletion failed"


def doaj_delete_articles_in_collection(colid, check_published=True):
    for container in Container.objects.filter(pid__startswith=colid):
        print(container)
        for article in container.article_set.all():
            try:
                if check_published:
                    if is_published(article) and has_publication_date(article):
                        doaj_delete_article(article.doi)
                else:
                    doaj_delete_article(article.doi)
            except Exception as ex:
                print(ex)


def doaj_retrieve_applications():
    application_ids = [
        "798d4f21a22d43579cea322bed8a560e",
        "7a0889a89de64979a3d5e26aace31db7",
        "0e30bf1ac2514bcda8d1cc0855237cd4",
        "4bcd45d13d23475bb246cbce9eaed9ee",
        "d85467c6c5914759886aa29481cce4b4",
        "11b60f2f3dd64ec087510dff3d82e0ab",
        "71951ece12524e45abac7628de6a8d22",
    ]

    for app_id in application_ids:
        response = requests.get("https://doaj.org/api/search/journals/" + app_id)
        if response.status_code == 200:
            results = response.json().get("results")
            if results:
                filename = results[0]["bibjson"]["title"] + ".json"
                with open(filename, "w") as fio:
                    json.dump(results, fio)
