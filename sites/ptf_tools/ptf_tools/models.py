import datetime
from dataclasses import asdict
from dataclasses import dataclass
from dataclasses import field

from django.db import models
from django.http import HttpRequest

from invitations.app_settings import app_settings as invitations_app_settings
from invitations.models import Invitation as BaseInvitation
from ptf.models import Collection


class ResourceInNumdam(models.Model):
    pid = models.CharField(max_length=64, db_index=True)


class CollectionGroup(models.Model):
    """
    Overwrites original Django Group.
    """

    def __str__(self):
        return self.group.name

    group = models.OneToOneField("auth.Group", unique=True, on_delete=models.CASCADE)
    collections = models.ManyToManyField(Collection)
    email_alias = models.EmailField(max_length=70, blank=True, default="")

    def get_collections(self) -> str:
        return ", ".join([col.pid for col in self.collections.all()])


class Invitation(BaseInvitation):
    """
    Invitation model. Additionally data can be stored in `extra_data`, to be used
    when an user signs up following the invitation link.
    Cf. signals.py
    """

    first_name = models.CharField("First name", max_length=150, null=False, blank=False)
    last_name = models.CharField("Last name", max_length=150, null=False, blank=False)
    extra_data = models.JSONField(
        default=dict,
        blank=True,
        help_text="JSON field used to dynamically update the created user object when the invitation is accepted.",
    )

    @classmethod
    def get_invite(cls, email: str, request: HttpRequest, invite_data: dict) -> "Invitation":
        """
        Gets the existing valid invitation or creates a new one and send it.
        If there's an existing invitation but it's expired, we delete it and
        send a new one.

        `invite_data` must contain `first_name` and `last_name` entries. It is passed
        as the context of the invite mail renderer.
        """
        try:
            invite = cls.objects.get(email__iexact=email)
            # Delete the invite if it's expired and create a fresh one
            if invite.key_expired():
                invite.delete()
                raise cls.DoesNotExist
        except cls.DoesNotExist:
            first_name = invite_data["first_name"]
            last_name = invite_data["last_name"]

            invite = cls.create(
                email, inviter=request.user, first_name=first_name, last_name=last_name
            )

            mail_template_context = {**invite_data}
            mail_template_context["full_name"] = f"{first_name} {last_name}"
            invite.send_invitation(request, **mail_template_context)

        return invite

    def date_expired(self) -> datetime.datetime:
        return self.sent + datetime.timedelta(
            days=invitations_app_settings.INVITATION_EXPIRY,
        )


@dataclass
class InviteCommentData:
    id: int
    user_id: int
    pid: str
    doi: str


@dataclass
class InviteCollectionData:
    pid: list[str]
    user_id: int


@dataclass
class InviteModeratorData:
    """
    Interface for storing the moderator data in an invitation.
    """

    comments: list[InviteCommentData] = field(default_factory=list)
    collections: list[InviteCollectionData] = field(default_factory=list)

    def __post_init__(self):
        try:
            comments = self.comments
            if not isinstance(comments, list):
                raise ValueError("'comments' must be a list")
            self.comments = [
                InviteCommentData(**c) if not isinstance(c, InviteCommentData) else c
                for c in comments
            ]
        except Exception as e:
            raise ValueError(f"Error while parsing provided InviteCommentData. {str(e)}")

        try:
            collections = self.collections
            if not isinstance(collections, list):
                raise ValueError("'collections' must be a list")
            self.collections = [
                InviteCollectionData(**c) if not isinstance(c, InviteCollectionData) else c
                for c in collections
            ]
        except Exception as e:
            raise ValueError(f"Error while parsing provided InviteCollectionData. {str(e)}")


@dataclass
class InvitationExtraData:
    """
    Interface representing an invitation's extra data.
    """

    moderator: InviteModeratorData = field(default_factory=InviteModeratorData)
    user_groups: list[int] = field(default_factory=list)

    def __post_init__(self):
        """
        Dataclasses do not provide an effective fromdict method to deserialize
        a dataclass (JSON to python dataclass object).

        This enables to effectively deserialize a JSON into a InvitationExtraData object,
        by replacing the nested dict by their actual dataclass representation.
        Beware this might not work well with typing (?)
        """
        moderator = self.moderator
        if moderator and not isinstance(moderator, InviteModeratorData):
            try:
                self.moderator = InviteModeratorData(**moderator)
            except Exception as e:
                raise ValueError(f"Error while parsing provided InviteModeratorData. {str(e)}")

    def serialize(self) -> dict:
        return asdict(self)
