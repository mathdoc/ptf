import os
import subprocess

from celery import shared_task

from django.conf import settings

from history.views import insert_history_event
from history.views import manage_exceptions
from ptf.cmds.ptf_cmds import archiveCollectionPtfCmd
from ptf.cmds.ptf_cmds import archiveIssuePtfCmd
from ptf.cmds.ptf_cmds import archiveNumdamResourcePtfCmd
from ptf.cmds.ptf_cmds import get_numdam_issues_list
from ptf.models import Article
from ptf.models import Collection
from ptf.models import Container


@shared_task
def archive_numdam_collection(colid):
    """
    Archive the files related to a collection (top level only, does not archive files of the issues)
    => col.xml and the collection images
    """

    if colid in settings.MERSENNE_COLLECTIONS:
        return

    try:
        # First, check NFS mount. A Bad mount will result in a timeout with os.path.isfile or isdir
        # The code will hang and the Celery tasks will be killed at some point
        # It's better to check and raise an exception
        subprocess.check_call(["test", "-d", settings.NUMDAM_ISSUE_SRC_FOLDER], timeout=0.5)
        subprocess.check_call(["test", "-d", settings.NUMDAM_ARTICLE_SRC_FOLDER], timeout=0.5)
        subprocess.check_call(
            ["test", "-d", os.path.join(settings.NUMDAM_DATA_ROOT, colid)], timeout=0.5
        )

        archiveNumdamResourcePtfCmd({"colid": colid}).do()

        pids = sorted(get_numdam_issues_list(colid))
        for pid in pids:
            print("1 task (issue)")
            archive_numdam_issue.delay(colid, pid)

    # except requests.exceptions.RequestException as exception:
    #     manage_exceptions("archive", colid, colid, "WARNING", exception)
    #     raise
    except Exception as exception:
        manage_exceptions("archive", colid, colid, "ERROR", exception)
        raise

    insert_history_event(
        {"type": "archive", "pid": colid, "col": colid, "status": "OK", "data": {"message": ""}}
    )


@shared_task
def archive_numdam_issue(colid, pid):
    """
    Archive the files of an issue. Get the list of files from numdam.org
    """
    try:
        archiveNumdamResourcePtfCmd({"colid": colid, "pid": pid}).do()
    # except requests.exceptions.RequestException as exception:
    #     manage_exceptions("archive", pid, colid, "WARNING", exception)
    #     raise
    except Exception as exception:
        manage_exceptions("archive", pid, colid, "ERROR", exception)
        raise


@shared_task
def archive_trammel_collection(colid, mathdoc_archive, binary_files_folder):
    try:
        # First, check NFS mount. A Bad mount will result in a timeout with os.path.isfile or isdir
        # The code will hang and the Celery tasks will be killed at some point
        # It's better to check and raise an exception
        subprocess.check_call(["test", "-d", settings.NUMDAM_ISSUE_SRC_FOLDER], timeout=0.5)
        subprocess.check_call(["test", "-d", settings.NUMDAM_ARTICLE_SRC_FOLDER], timeout=0.5)
        subprocess.check_call(["test", "-d", settings.CEDRAM_TEX_FOLDER], timeout=0.5)

        collection = Collection.objects.get(pid=colid)
        issues = collection.content.all()

        archiveCollectionPtfCmd({"colid": colid, "issues": issues}).do()
        for issue in issues:
            archive_trammel_resource.delay(colid, issue.pid, mathdoc_archive, binary_files_folder)

    except Exception as exception:
        manage_exceptions("archive", colid, colid, "ERROR", exception)
        raise

    insert_history_event(
        {"type": "archive", "pid": colid, "col": colid, "status": "OK", "data": {"message": ""}}
    )


@shared_task
def archive_trammel_resource(
    colid=None, pid=None, mathdoc_archive=None, binary_files_folder=None, article_doi=None
):
    """
    Archive the files of an issue or an article stored in the ptf-tools database (Trammel)
    """
    try:
        if article_doi is not None:
            article = Article.objects.get(doi=article_doi)
            cmd = archiveIssuePtfCmd(
                {
                    "pid": pid,
                    "export_folder": mathdoc_archive,
                    "binary_files_folder": binary_files_folder,
                    "article": article,
                }
            )
        else:
            issue = Container.objects.get(pid=pid)
            cmd = archiveIssuePtfCmd(
                {
                    "pid": issue.pid,
                    "export_folder": mathdoc_archive,
                    "binary_files_folder": binary_files_folder,
                }
            )
        cmd.do()
    except Exception as exception:
        manage_exceptions("archive", pid, colid, "ERROR", exception)
        raise

    insert_history_event(
        {"type": "archive", "pid": pid, "col": colid, "status": "OK", "data": {"message": ""}}
    )
