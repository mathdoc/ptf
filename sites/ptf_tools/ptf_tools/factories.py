import factory.django

from mersenne_cms.models import Page


class PageFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Page

    id = factory.Sequence(lambda n: n)
    menu_title = "title"
    menu_title_en = "title_en"
    menu_title_fr = "title_fr"
    slug = "slug"
    slug_fr = "slug_fr"
    slug_en = "slug_en"
    parent_page = None
    content = "content"
    content_en = "content_en"
    content_fr = "content_fr"
    site_id = 2
