from ptf_tools.templatetags.tools_helpers import get_authorized_collections


def is_authorized_editor(user, colid):
    colids = get_authorized_collections(user)
    is_authenticated = user.is_active and user.is_authenticated
    return is_authenticated and (colid in colids or user.is_staff)
