from urllib.parse import urlencode

import responses
from responses.matchers import json_params_matcher
from responses.matchers import query_param_matcher

from django.contrib.auth.models import Group
from django.contrib.auth.models import User
from django.core import mail
from django.test import RequestFactory
from django.test import TestCase
from django.test import override_settings

from comments_moderation.tests.mixins import ModeratorTestMixin

from ..models import Invitation
from ..models import InvitationExtraData
from ..signals import update_user_from_invite

COMMENTS_BASE_URL = "http://comments.test"


@override_settings(
    COMMENTS_VIEWS_API_BASE_URL=COMMENTS_BASE_URL,
    COMMENTS_VIEWS_API_CREDENTIALS=("login", "password"),
)
class CommentModeratorSignalTestCase(ModeratorTestMixin, TestCase):
    def setUp(self) -> None:
        mail.outbox = []
        self.setUpModeratorData()
        super().setUp()

    def tearDown(self) -> None:
        self.tearDownModeratorData()
        Invitation.objects.all().delete()
        super().tearDown()

    def test_update_user_collectyon_from_invite(self):
        request = RequestFactory().get("/")
        request.user = self.admin_moderator
        email = "user_test@test.test"
        invite = Invitation.get_invite(email, request, {"first_name": "User", "last_name": "Test"})
        extra_data = {
            "moderator": {
                "collections": [
                    {"pid": [self.collection_1.pid], "user_id": self.admin_moderator.pk},
                    {"pid": [self.collection_3.pid], "user_id": self.admin_moderator.pk},
                ]
            }
        }
        invite.extra_data = InvitationExtraData(**extra_data).serialize()
        invite.save()

        user = User.objects.create_user(email, email, "***")
        # Clean invitation mail
        mail.outbox = []

        update_user_from_invite(None, request, user)

        user = User.objects.get(email=email)
        self.assertEqual(user.first_name, "User")
        self.assertEqual(user.last_name, "Test")

        self.assertTrue(user.comment_moderator.is_moderator)

        collections = user.comment_moderator.collections.all()
        self.assertEqual(len(collections), 2)
        self.assertEqual(collections[0].pid, self.collection_1.pid)
        self.assertEqual(collections[1].pid, self.collection_3.pid)

        # Check that there's only 1 mail in the outbox with recipient admin_moderator
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].to, [self.admin_moderator.email])

    @responses.activate
    def test_update_user_moderation_rights_from_invite(self):
        request = RequestFactory().get("/")
        request.user = self.admin_moderator
        email = "user_test@test.test"
        invite = Invitation.get_invite(email, request, {"first_name": "User", "last_name": "Test"})

        extra_data = {
            "moderator": {
                "comments": [
                    {
                        "id": 1,
                        "user_id": self.admin_moderator.pk,
                        "doi": "my_doi",
                        "pid": self.collection_1.pid,
                    },
                    {
                        "id": 2,
                        "user_id": self.staff_moderator_1.pk,
                        "doi": "my_doi",
                        "pid": self.collection_1.pid,
                    },
                ]
            }
        }
        invite.extra_data = InvitationExtraData(**extra_data).serialize()
        invite.save()

        user = User.objects.create_user(email, email, "***")

        # Comment server down - User can still log in
        update_user_from_invite(None, request, user)

        user = User.objects.get(email=email)
        self.assertEqual(user.first_name, "User")
        self.assertEqual(user.last_name, "Test")

        self.assertTrue(user.comment_moderator.is_moderator)

        user.delete()
        invite.delete()

        # Comment server up
        user = User.objects.create_user(email, email, "***")

        query_params = {
            "moderator_id": f"{self.admin_moderator.pk}",
            "collection_id": f"{self.collection_1.pid},{self.collection_2.pid}",
            "action": "create",
        }
        mod_query_params = query_param_matcher(query_params)
        mod_json_data = json_params_matcher({"comment_id": 1, "moderator_id": user.pk})
        mod_response_params = {
            "method": "POST",
            "url": f"{COMMENTS_BASE_URL}/api/moderators/",
            "status": 201,
            "json": {},
            "match": [mod_query_params, mod_json_data],
        }
        response = responses.add(**mod_response_params)

        query_params_bis = {
            "moderator_id": f"{self.staff_moderator_1.pk}",
            "collection_id": self.collection_1.pid,
            "action": "create",
        }
        mod_query_params_bis = query_param_matcher(query_params_bis)
        mod_json_data_bis = json_params_matcher({"comment_id": 2, "moderator_id": user.pk})
        mod_response_params_bis = {
            "method": "POST",
            "url": f"{COMMENTS_BASE_URL}/api/moderators/",
            "status": 201,
            "json": {},
            "match": [mod_query_params_bis, mod_json_data_bis],
        }
        response_bis = responses.add(**mod_response_params_bis)

        invite = Invitation.get_invite(email, request, {"first_name": "User", "last_name": "Test"})

        invite.extra_data = InvitationExtraData(**extra_data).serialize()
        invite.save()

        mail.outbox = []
        update_user_from_invite(None, request, user)

        user = User.objects.get(email=email)
        self.assertEqual(user.first_name, "User")
        self.assertEqual(user.last_name, "Test")

        self.assertTrue(user.comment_moderator.is_moderator)

        # Make sure the POST requests were performed
        # Not working with wonderful python 3.6 used at Mathdoc :)
        # self.assertEqual(response.call_count, 1)
        # self.assertEqual(response_bis.call_count, 1)
        response_url = f"{mod_response_params['url']}?{urlencode(query_params)}"
        # They were 2 calls to the URL - One before the mock and one after
        responses.assert_call_count(response_url, 2)

        response_url_bis = f"{mod_response_params_bis['url']}?{urlencode(query_params_bis)}"
        responses.assert_call_count(response_url_bis, 2)

        # Check that e-mails were sent to the users that invited the moderator.
        self.assertEqual(len(mail.outbox), 2)
        recipients = [email.to for email in mail.outbox]
        self.assertListEqual(
            recipients, [[self.admin_moderator.email], [self.staff_moderator_1.email]]
        )


class UserSignalTestCase(TestCase):
    def tearDown(self) -> None:
        User.objects.all().delete()
        Group.objects.all().delete()
        super().tearDown()

    def test_user_group_signup(self):
        """
        Test the user group handling in invitation extra data.
        """
        inviter = User.objects.create_user("inviter@test.test", "inviter@test.test", "***")
        group_1 = Group.objects.create(name="Group 1")
        group_2 = Group.objects.create(name="Group 2")

        request = RequestFactory().get("/")
        request.user = inviter
        email = "user_test@test.test"
        invite = Invitation.get_invite(email, request, {"first_name": "User", "last_name": "Test"})

        invite.extra_data = InvitationExtraData(user_groups=[group_1.pk, group_2.pk]).serialize()
        invite.save()

        user = User.objects.create_user(email, email, "***")

        update_user_from_invite(None, request, user)

        user = User.objects.get(email=email)

        user_groups = user.groups.all()

        self.assertEqual(len(user_groups), 2)
        self.assertEqual(user_groups[0].pk, group_1.pk)
        self.assertEqual(user_groups[1].pk, group_2.pk)
