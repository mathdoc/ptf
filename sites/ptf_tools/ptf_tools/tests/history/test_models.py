import datetime

from django.conf import settings
from django.test import TestCase
from django.test.client import RequestFactory

from history.models import HistoryEvent
from history.models import delete_history_event
from history.models import get_history_error_warning_counts
from history.models import get_history_events
from history.models import get_history_last_event_by
from history.models import insert_history_event
from ptf.cmds import solr_cmds


class RequestTestCase(TestCase):
    def get_solr(self):
        return solr_cmds.solrFactory.get_solr()

    def setUp(self):
        settings.DEBUG = True
        self.factory = RequestFactory()

        solr_cmds.solrFactory.solr_url = settings.SOLR_URL

        self.get_solr().delete(q="*:*")

    def solrSearch(self, searchStr):
        return self.get_solr().search(searchStr)

    def tearDown(self):
        self.get_solr().delete(q="*:*")


class ModelTestCase(RequestTestCase):
    def test_01_insert_history_event(self):
        print("** test 00_insert_history_event")
        HistoryEvent.objects.all().delete()
        insert_history_event(
            {
                "type": "edit",
                "pid": "JEP_2014__1_7_352_0",
                "col": "JEP",
                "status": "OK",
                "data": {"message": "Zbl:zbl8 checked"},
            }
        )
        self.assertEqual(HistoryEvent.objects.all().count(), 1)

    def test_02_get_history_event(self):
        print("** test 01_get_history_event")
        HistoryEvent.objects.all().delete()
        insert_history_event(
            {
                "type": "edit",
                "pid": "JEP_2014__1_7_352_0",
                "col": "JEP",
                "status": "OK",
                "data": {"message": "Zbl:zbl8 checked"},
            }
        )
        self.assertEqual(HistoryEvent.objects.all().count(), 1)
        history_events = get_history_events({})
        for event in history_events:
            for event_in_month in event["events_in_month"]:
                self.assertEqual(event_in_month.pid, "JEP_2014__1_7_352_0")

    def test_03_get_history_error_warning_counts(self):
        print("** test 03_get_history_error_warning_counts")
        HistoryEvent.objects.all().delete()
        insert_history_event(
            {
                "type": "matching",
                "pid": "AIF_2007__57_7",
                "col": "AIF",
                "status": "ERROR",
                "created_on": datetime.datetime(2019, 1, 4, tzinfo=datetime.UTC),
                "data": {
                    "message": "Internal Error",
                    "ids_count": 3,
                    "articles": [
                        {
                            "pid": "AIF_2007__57_7_2143_0",
                            "ids": [{"type": "zbl", "id": "zbl3", "seq": 5}],
                        },
                        {
                            "pid": "AIF_2007__57_7_2143_0",
                            "ids": [
                                {"type": "zbl", "id": "zbl4", "seq": 8},
                                {"type": "mr", "id": "mr2", "seq": 12},
                            ],
                        },
                    ],
                },
            }
        )
        insert_history_event(
            {
                "type": "matching",
                "pid": "AIF_2007__57_8",
                "col": "AIF",
                "status": "WARNING",
                "created_on": datetime.datetime(2019, 1, 4, tzinfo=datetime.UTC),
                "data": {
                    "message": "Internal Error",
                    "ids_count": 3,
                    "articles": [
                        {
                            "pid": "AIF_2007__57_7_2143_0",
                            "ids": [{"type": "zbl", "id": "zbl3", "seq": 5}],
                        },
                        {
                            "pid": "AIF_2007__57_7_2143_0",
                            "ids": [
                                {"type": "zbl", "id": "zbl4", "seq": 8},
                                {"type": "mr", "id": "mr2", "seq": 12},
                            ],
                        },
                    ],
                },
            }
        )
        nb_history_errors_warnings = get_history_error_warning_counts()
        self.assertEqual(nb_history_errors_warnings, (1, 1))

    def test_04_get_history_last_event_by(self):
        print("** test 04_get_history_last_event_by")
        HistoryEvent.objects.all().delete()
        insert_history_event(
            {
                "type": "matching",
                "pid": "AIF_2007__57_7",
                "col": "AIF",
                "status": "OK",
                "created_on": datetime.datetime(2019, 1, 4, tzinfo=datetime.UTC),
                "data": {
                    "message": "Internal Error",
                    "ids_count": 3,
                    "articles": [
                        {
                            "pid": "AIF_2007__57_7_2143_0",
                            "ids": [{"type": "zbl", "id": "zbl3", "seq": 5}],
                        },
                        {
                            "pid": "AIF_2007__57_7_2143_0",
                            "ids": [
                                {"type": "zbl", "id": "zbl4", "seq": 8},
                                {"type": "mr", "id": "mr2", "seq": 12},
                            ],
                        },
                    ],
                },
            }
        )
        insert_history_event(
            {
                "type": "matching",
                "pid": "AIF_2007__57_8",
                "col": "AIF",
                "status": "OK",
                "created_on": datetime.datetime(2018, 1, 4, tzinfo=datetime.UTC),
                "data": {
                    "message": "Internal Error",
                    "ids_count": 3,
                    "articles": [
                        {
                            "pid": "AIF_2007__57_7_2143_0",
                            "ids": [{"type": "zbl", "id": "zbl3", "seq": 5}],
                        },
                        {
                            "pid": "AIF_2007__57_7_2143_0",
                            "ids": [
                                {"type": "zbl", "id": "zbl4", "seq": 8},
                                {"type": "mr", "id": "mr2", "seq": 12},
                            ],
                        },
                    ],
                },
            }
        )
        last_event = get_history_last_event_by("matching", "AIF_2007__57_7")
        self.assertEqual(last_event.col, "AIF")

    def test_05_delete_history_event(self):
        print("** test 05_delete_history_event")
        HistoryEvent.objects.all().delete()
        insert_history_event(
            {
                "type": "edit",
                "pid": "JEP_2014__1_7_352_0",
                "col": "JEP",
                "status": "OK",
                "data": {"message": "Zbl:zbl8 checked"},
            }
        )
        self.assertEqual(HistoryEvent.objects.all().count(), 1)
        history_event = get_history_events({})
        for event in history_event:
            for event_in_month in event["events_in_month"]:
                delete_history_event(event_in_month.pk)
        self.assertEqual(HistoryEvent.objects.all().count(), 0)
