import os
import shutil
from datetime import datetime
from tempfile import TemporaryDirectory

import dateutil.parser
from lxml import etree

from django.conf import settings
from django.test import TestCase
from django.test import override_settings
from django.test.client import RequestFactory
from django.urls import reverse

from mersenne_tools.models import DOIBatch
from ptf import model_helpers
from ptf import tex
from ptf import utils
from ptf.cmds import ptf_cmds
from ptf.cmds import solr_cmds
from ptf.cmds import xml_cmds
from ptf.display import resolver
from ptf.models import Abstract
from ptf.models import Article
from ptf.models import BibItem
from ptf.models import Collection
from ptf.models import Container
from ptf.models import Contrib
from ptf.models import ContribGroup
from ptf.models import Kwd
from ptf.models import Provider
from ptf_tools import doi
from upload.utils import compute_article_for_pcj


class ToolsTestCase(TestCase):
    data_folder = os.path.dirname(os.path.abspath(__file__)) + "/data"
    LOG_DIR_TEMP = TemporaryDirectory()

    def get_solr(self):
        return solr_cmds.solrFactory.get_solr()

    def setUp(self):
        solr_cmds.solrFactory.solr_url = settings.SOLR_URL
        self.get_solr().delete(q="*:*")
        self.factory = RequestFactory()

    def tearDown(self):
        self.get_solr().delete(q="*:*")

    def solrSearch(self, searchStr):
        return self.get_solr().search(searchStr)

    def check_empty_database(self):
        self.assertEqual(Collection.objects.count(), 0)
        self.assertEqual(Container.objects.count(), 0)
        self.assertEqual(Article.objects.count(), 0)
        self.assertEqual(BibItem.objects.count(), 0)
        self.assertEqual(Abstract.objects.count(), 0)
        self.assertEqual(ContribGroup.objects.count(), 0)
        self.assertEqual(Contrib.objects.count(), 0)
        self.assertEqual(Kwd.objects.count(), 0)
        # self.assertEqual(Publisher.objects.count(), 0)

    @override_settings(MERSENNE_TEST_DATA_FOLDER="/tmp")
    @override_settings(MERSENNE_TMP_FOLDER="/tmp")
    def import_from_archive(self, archive_folder=None, erase=True):
        settings.DEBUG = True
        self.check_empty_database()

        if not archive_folder:
            archive_folder = self.data_folder + "/archive"

        with self.settings(MATHDOC_ARCHIVE_FOLDER=archive_folder):
            # data_folder = os.path.dirname((os.path.abspath(__file__))) + '/data'

            # else:
            #     settings.MATHDOC_ARCHIVE_FOLDER = data_folder + '/archive'
            print("  --- import_from_archive folder: " + settings.MATHDOC_ARCHIVE_FOLDER)

            # INFO : lorsque l'on importe sur ptf-tools les binaires sont copiés ds mersenne_test
            # ce qui peut provoquer des incohérences si le site de test n'est pas mis à jour
            # (mais bon généralement si on importe sur ptf-tools c'est pour mettre sur le site de test)

            # attention si dans /tmp/CRPHYS il y avait déjà qqch ?  on supprime son contenu
            if os.path.exists(settings.MERSENNE_TEST_DATA_FOLDER + "/CRPHYS"):
                shutil.rmtree(settings.MERSENNE_TEST_DATA_FOLDER + "/CRPHYS")

            params = {
                "pid": "CRPHYS",
                "from_folder": settings.MATHDOC_ARCHIVE_FOLDER,
                "to_folder": settings.MERSENNE_TEST_DATA_FOLDER,
                "backup_folder": settings.MATHDOC_ARCHIVE_FOLDER,
                "with_cedrics": False,
            }

            cmd = xml_cmds.importEntireCollectionXmlCmd(params)
            cmd.do()

            self.assertEqual(Container.objects.count(), 2)
            self.assertEqual(Article.objects.count(), 3)

            site = model_helpers.get_site_mersenne("CRPHYS")
            article_org = model_helpers.get_article("CRPHYS_0__0_0_A1_0")
            container0 = model_helpers.get_container("CRPHYS_0__0_0")
            article21_1_107 = model_helpers.get_article("CRPHYS_2020__21_1_107_0")
            # on pourrait vérifier si indexation solr / en theorie 0_0_0 n'est pas indexé

            # test dateS
            self.assertEqual(
                container0.deployed_date(site),
                dateutil.parser.parse("2020-11-03 13:38:28.208668+00:00"),
            )
            self.assertEqual(
                article_org.date_online_first, model_helpers.parse_date_str("2020-11-03")
            )
            self.assertEqual(
                article21_1_107.date_published, model_helpers.parse_date_str("2020-09-17")
            )

            # test bibitem faux positif
            bibitem = model_helpers.get_bibitem_by_seq(article21_1_107, 26)
            bibitemid = bibitem.bibitemid_set.first()
            self.assertTrue(bibitemid.false_positive)

            # pdfs ?
            pdf_path = (
                settings.MERSENNE_TEST_DATA_FOLDER
                + "/"
                + article21_1_107.get_binary_disk_location("self", "application/pdf", "")
            )
            self.assertTrue(os.path.isfile(pdf_path))
            # test img ajoutée via ptf-tools
            icon_path = (
                settings.MERSENNE_TEST_DATA_FOLDER
                + "/"
                + article21_1_107.get_relative_folder()
                + "/CRPHYS_2020__21_1_107_0.jpg"
            )
            self.assertTrue(os.path.isfile(icon_path))

            icon_path = (
                settings.MERSENNE_TEST_DATA_FOLDER
                + "/"
                + article_org.get_relative_folder()
                + "/CRPHYS_0__0_0_A1_0.jpg"
            )
            self.assertTrue(os.path.isfile(icon_path))

            # TODO attach ? il n'y en pas pour le moment dans les data de test

            # 0_0_0_A2 a des figures / vérifier qu'elles sont copiés au bon endroit
            # bizarre que les images soient copiées ds src/tex/figures alors RelatedObject.get_href donne plutôt .../Article_id/attach/
            # refactoring prévu de RelatedObject
            articleA2 = model_helpers.get_article("CRPHYS_0__0_0_A2_0")
            related_objs = articleA2.relatedobject_set.filter(rel="html-image")
            self.assertTrue(related_objs.count(), 5)
            img_dir = os.path.join(
                settings.MERSENNE_TEST_DATA_FOLDER,
                resolver.get_relative_folder("CRPHYS", container0.pid, articleA2.pid),
                "src/tex/figures",
            )
            for related_obj in related_objs:
                img_path = img_dir + "/" + os.path.basename(related_obj.location)
                self.assertTrue(os.path.isfile(img_path))

            # tex
            self_binary_files = articleA2.get_binary_files_href()["self"]
            self.assertEqual(len(self_binary_files), 2)
            tex_path = (
                settings.MERSENNE_TEST_DATA_FOLDER
                + "/"
                + articleA2.get_binary_disk_location("self", "application/x-tex", "")
            )
            self.assertTrue(os.path.isfile(tex_path))

            # positionnement des DOIBatch sur le 0nline First pour test
            doibatch = DOIBatch(resource=article_org, status="Enregistré", log="-- import --")
            doibatch.save()
            doibatch = DOIBatch(resource=articleA2, status="Enregistré", log="-- import --")
            doibatch.save()

            if erase:
                self._erase(to_folder=settings.MERSENNE_TEST_DATA_FOLDER)

    @override_settings(MERSENNE_TEST_DATA_FOLDER="/tmp")
    @override_settings(MERSENNE_PROD_DATA_FOLDER="/tmp")
    def _erase(self, to_folder=None):
        print("  --- erase all")
        col = model_helpers.get_collection("CRPHYS")
        provider = Provider.objects.get(name="mathdoc")
        if col:
            issues = col.content.all()
            for issue in issues:
                params = {"pid": issue.pid, "ctype": issue.ctype}
                params["to_folder"] = to_folder
                cmd = ptf_cmds.addContainerPtfCmd(params)
                cmd.set_provider(provider)
                cmd.set_object_to_be_deleted(issue)
                cmd.undo()
            if to_folder:
                # TODO : tester suppression fichier
                # cmd.undo : pb on peut supprimer si on est sur ptf-tools mais pas si c'est sur test non ?
                self.assertFalse(
                    os.path.isdir(settings.MERSENNE_TEST_DATA_FOLDER + "/CRPHYS/CRPHYS_0__0_0")
                )

            cmd1 = ptf_cmds.addCollectionPtfCmd({"pid": col.pid})
            cmd1.set_provider(provider)
            cmd1.set_object_to_be_deleted(col)
            cmd1.undo()

            # cmd2 = ptf_cmds.addProviderPtfCmd(
            #     {"name": provider.name, "pid_type": provider.pid_type})
            # cmd2.undo()

            # vérification
            self.check_empty_database()
            self.assertEqual(len(self.solrSearch("*")), 0)

            # supprime /tmp/CRPHYS
            shutil.rmtree(settings.MERSENNE_TEST_DATA_FOLDER + "/CRPHYS")
            self.assertFalse(os.path.isdir(settings.MERSENNE_TEST_DATA_FOLDER + "/CRPHYS"))

    def __test_01_import_from_archive(self):
        print("** test_01_import_from_archive")
        self.import_from_archive(erase=True)

    @override_settings(MATHDOC_ARCHIVE_FOLDER=data_folder + "/archive")
    @override_settings(MERSENNE_TEST_DATA_FOLDER="/tmp")
    @override_settings(RESOURCES_ROOT="/tmp")
    @override_settings(CEDRAM_XML_FOLDER=data_folder + "/cedram/exploitation")
    @override_settings(CEDRAM_TEX_FOLDER=data_folder + "/cedram/production_tex")
    def test_02_move_article_from_online_first_to_final_issue(self):
        print("test_02_move_article_from_online_first_to_final_issue")
        self.import_from_archive(erase=False)

        # data_folder = os.path.dirname(os.path.abspath(__file__)) + '/data'
        # settings.MATHDOC_ARCHIVE_FOLDER = data_folder + '/archive'
        # settings.MERSENNE_TEST_DATA_FOLDER = '/tmp'
        settings.MERSENNE_TMP_FOLDER = "/tmp/CRPHYS/tmp"

        #
        # 0_0_0 contenant un article doi.A avec matching et onlinefirstdate ET un article doi.B
        # et 0_0_0 a une deployed_date(Site)
        # 2 - import de 2020_2 contenant l'article doi.A uniquement
        # on vérifie que :
        # - doi.A a bien conservé ses dates / matching
        # - 0_0_0 ne contient que doi.B
        # - 2020_2 n'a pas de deployed_date
        # - pas de DOIBatch associé aux nouveaux articles du nouveau container
        article_org = model_helpers.get_article("CRPHYS_0__0_0_A1_0")

        # settings.CEDRAM_XML_FOLDER = data_folder + '/cedram/exploitation'
        #
        # settings.CEDRAM_TEX_FOLDER = data_folder + '/cedram/production_tex'
        print("** test_02_import_from_final_issue_with_article_in_online_first")
        # test DOIBatch
        doib = doi.get_doibatch(article_org)
        self.assertNotEqual(doib, None)
        cmd1 = xml_cmds.importCedricsIssueDirectlyXmlCmd(
            {
                "colid": "CRPHYS",
                "input_file": settings.CEDRAM_TEX_FOLDER
                + "/CRPHYS/CRPHYS_2020__21_2/CRPHYS_2020__21_2-cdrxml.xml",
                "remove_email": False,
                "remove_date_prod": True,
                "diff_only": False,
            }
        )
        cmd1.do()

        new_issue = model_helpers.get_container("CRPHYS_2020__21_2")
        firstOnline = model_helpers.get_container("CRPHYS_0__0_0")
        self.assertEqual(firstOnline.article_set.count(), 1)
        self.assertEqual(new_issue.article_set.count(), 1)  # nouvel article
        # verifie les dates deployed_date et mise en prod

        site = model_helpers.get_site_mersenne("CRPHYS")
        self.assertIsNone(new_issue.deployed_date(site))

        new_article = new_issue.article_set.get()
        self.assertEqual(new_article.date_online_first, article_org.date_online_first)
        self.assertIsNone(new_article.date_published)

        # test DOIBatch
        doib = doi.get_doibatch(new_article)
        self.assertEqual(doib, None)
        doib = doi.get_doibatch(firstOnline.article_set.first())
        self.assertNotEqual(doib, None)
        self.assertTrue(len(cmd1.get_warnings()) == 0)

        # on vérifie que le HTML de RVT est bien repris par le fait que les images ont bien été copiées :
        # les balises référencant des images ds le HTML sont parsés et les images deviennent des RelatedObject
        # qui eux même sont copiés par copy_binary_file

        related_objs2 = new_article.relatedobject_set.filter(rel="html-image")
        self.assertTrue(related_objs2.count(), 5)
        img_dir = os.path.join(
            settings.MERSENNE_TEST_DATA_FOLDER,
            resolver.get_relative_folder("CRPHYS", new_issue.pid, new_article.pid),
            "src/tex/figures",
        )
        for related_obj in related_objs2:
            img_path = img_dir + "/" + os.path.basename(related_obj.location)
            self.assertTrue(os.path.isfile(img_path))

        # on vérif que l'ancien rép. des binaires de l'article passé ds le volume final n'existe plus
        self.assertFalse(
            os.path.isdir(
                settings.MERSENNE_TEST_DATA_FOLDER + "/CRPHYS/CRPHYS_0__0_0/CRPHYS_0__0_0_A1_0"
            )
        )

        # sur CRPHYS_0__0_0_A1, une icon a été ajoutée via ptf-tools et donc exportée dans archive en tant que ext-link
        # on vérifie qu'elle est récupérée dans le nouvel article
        img_path = os.path.join(
            settings.MERSENNE_TEST_DATA_FOLDER,
            resolver.get_relative_folder("CRPHYS", new_issue.pid, new_article.pid),
            "%s" % new_article.pid + ".jpg",
        )
        self.assertTrue(os.path.isfile(img_path))

        # on vérifie que l'article importé n'a pas de DOIBatch associé et que celui resté en OnlineFirst l'a gardé

        cmd2 = xml_cmds.importCedricsIssueDirectlyXmlCmd(
            {
                "colid": "CRPHYS",
                "input_file": settings.CEDRAM_TEX_FOLDER
                + "/CRPHYS/CRPHYS_2020__21_2/CRPHYS_2020__21_2-cdrxml.xml",
                "remove_email": False,  # permet de tester les warnings en même temps : <email> doit bugger
                "remove_date_prod": True,
                "diff_only": False,
            }
        )
        cmd2.do()

        # on vérifie que l'icon est toujours présente après update.
        self.assertTrue(os.path.isfile(img_path))

        self._erase()

    @override_settings(MATHDOC_ARCHIVE_FOLDER=data_folder + "/archive")
    @override_settings(MERSENNE_TEST_DATA_FOLDER="/tmp")
    @override_settings(MERSENNE_PROD_DATA_FOLDER="/tmp")
    @override_settings(MATHDOC_ARCHIVE_FOLDER="/tmp/CRPHYS_archive")
    @override_settings(
        NUMDAM_ISSUE_SRC_FOLDER=os.path.join(
            data_folder, "numdam_dev/numerisation/donnees_validees"
        )
    )
    @override_settings(
        NUMDAM_ARTICLE_SRC_FOLDER=os.path.join(data_folder, "numdam_dev/raffinement")
    )
    @override_settings(NUMDAM_DATA_ROOT=os.path.join(data_folder, "numdam_data"))
    @override_settings(CEDRAM_XML_FOLDER=data_folder + "/cedram/exploitation")
    @override_settings(CEDRAM_TEX_FOLDER=data_folder + "/cedram/production_tex")
    def test_03_archive_collection(self):
        print(
            "** test_03_archive_collection : test en même temps la méthode appelée lors de mise en prod : ptf_cmds.archiveIssuePtfCmd"
        )

        # on rejout l'import depuis archive
        self.import_from_archive(erase=False)
        # # archivage de la collection en entier
        # settings.MERSENNE_PROD_DATA_FOLDER = settings.MERSENNE_TEST_DATA_FOLDER
        # #destination de l'archive
        # settings.MATHDOC_ARCHIVE_FOLDER = settings.MERSENNE_TEST_DATA_FOLDER + '/CRPHYS_archive'

        if os.path.exists(settings.MATHDOC_ARCHIVE_FOLDER):
            raise Exception(
                "Attention, on se base sur un "
                + settings.MATHDOC_ARCHIVE_FOLDER
                + " vide et non existant pour ces tests"
            )

        os.makedirs(settings.MATHDOC_ARCHIVE_FOLDER)
        cmd2 = ptf_cmds.exportPtfCmd(
            {
                "pid": "CRPHYS",
                "export_folder": settings.MATHDOC_ARCHIVE_FOLDER,
                "with_binary_files": True,
                "for_archive": True,
                "binary_files_folder": settings.MERSENNE_PROD_DATA_FOLDER,
            }
        )
        cmd2.do()
        collection = model_helpers.get_collection("CRPHYS")
        pids_to_archive = []

        # # on en profite pour tester l'archivage des fichiers originaux de NUMDAM : tif, djvu ...
        # # archivage fait dans ptf.display.resolver.copy_numdam_src_files
        # data_folder = os.path.dirname(os.path.abspath(__file__)) + '/data'
        # settings.NUMDAM_ISSUE_SRC_FOLDER = os.path.join(data_folder, 'numdam_dev/numerisation/donnees_validees')
        # settings.NUMDAM_ARTICLE_SRC_FOLDER = os.path.join(data_folder, 'numdam_dev/raffinement')
        # settings.NUMDAM_DATA_ROOT = os.path.join(data_folder, 'numdam_data')

        for issue in collection.content.all():
            if issue.deployed_date() is not None:
                pids_to_archive.append(issue.pid)

        for pid in pids_to_archive:
            cmdX = ptf_cmds.archiveIssuePtfCmd(
                {
                    "pid": pid,
                    "export_folder": settings.MATHDOC_ARCHIVE_FOLDER,
                    "binary_files_folder": settings.MERSENNE_PROD_DATA_FOLDER,
                    "skip_pdfa": True,
                }
            )
            cmdX.do()

        # test présence du djvu/tif/pdf suite archivage Numdam
        numdam_files_archive_path = os.path.join(
            settings.MATHDOC_ARCHIVE_FOLDER, "CRPHYS/CRPHYS_2020__21_1/src/digitisation"
        )
        tif_file = os.path.join(numdam_files_archive_path, "page0001.tif")
        self.assertTrue(os.path.isfile(tif_file))
        issue_xml = os.path.join(numdam_files_archive_path, "CRPHYS_2020__21_1.xml")
        self.assertTrue(os.path.isfile(issue_xml))

        numdam_files_archive_article_path = os.path.join(
            settings.MATHDOC_ARCHIVE_FOLDER,
            "CRPHYS/CRPHYS_2020__21_1/CRPHYS_2020__21_1_107_0/src/digitisation",
        )

        full_text = os.path.join(numdam_files_archive_article_path, "CRPHYS_2020__21_1_107_0.xml")
        self.assertTrue(os.path.isfile(full_text))

        # djvu_file = os.path.join(
        #     settings.MATHDOC_ARCHIVE_FOLDER,
        #     "CRPHYS/CRPHYS_2020__21_1/CRPHYS_2020__21_1_107_0/CRPHYS_2020__21_1_107_0.djvu",
        # )
        # If the DjVu is only in /numdam_data but not specified in the XML, it is ignored
        # self.assertTrue(os.path.isfile(djvu_file))

        # on efface la base/solr pour test ré import
        self._erase()
        self.import_from_archive(archive_folder=settings.MATHDOC_ARCHIVE_FOLDER, erase=True)

        shutil.rmtree(settings.MERSENNE_TEST_DATA_FOLDER + "/CRPHYS_archive")
        self.assertFalse(os.path.isdir(settings.MERSENNE_TEST_DATA_FOLDER + "/CRPHYS_archive"))

    @override_settings(MERSENNE_TEST_DATA_FOLDER="/tmp")
    @override_settings(MERSENNE_PROD_DATA_FOLDER="/tmp/prod")
    @override_settings(CEDRAM_XML_FOLDER=data_folder + "/cedram/exploitation")
    def test_04_copy_binaries_when_deploy(self):
        print("** test_04_copy_binaries_when_deploy")

        if os.path.exists(settings.MERSENNE_PROD_DATA_FOLDER):
            raise Exception(
                "Attention, on se base sur un "
                + settings.MERSENNE_PROD_DATA_FOLDER
                + " vide et non existant pour ces tests"
            )

        # on rejout l'import depuis archive
        self.import_from_archive(erase=False)

        # data_folder = os.path.dirname(os.path.abspath(__file__)) + '/data'
        # settings.CEDRAM_XML_FOLDER = data_folder + '/cedram/exploitation'

        # test simple : on regarde combien il y a de fichiers/rép avant ds settings.MERSENNE_TEST_DATA_FOLDER
        # et on compare après la copie avec settings.MERSENNE_PROD_DATA_FOLDER

        container = model_helpers.get_container("CRPHYS_0__0_0")

        nbre_dirs_test = 0
        nbre_files_test = 0
        for root, dirs, files in os.walk(
            settings.MERSENNE_TEST_DATA_FOLDER + "CRPHYS/CRPHYS_0__0_0"
        ):
            nbre_dirs_test += len(dirs)
            nbre_files_test += len(files)

        os.makedirs(settings.MERSENNE_PROD_DATA_FOLDER)
        print("  --- test copy")
        # on reproduit ce qui se fait lors de la mise en prod if site == 'website':
        resolver.copy_binary_files(
            container, settings.MERSENNE_TEST_DATA_FOLDER, settings.MERSENNE_PROD_DATA_FOLDER
        )

        for article in container.article_set.all():
            resolver.copy_binary_files(
                article, settings.MERSENNE_TEST_DATA_FOLDER, settings.MERSENNE_PROD_DATA_FOLDER
            )
            # resolver.copy_binary_files(
            #         article,
            #         settings.CEDRAM_XML_FOLDER,
            #         settings.MERSENNE_PROD_DATA_FOLDER)

        nbre_dirs_prod = 0
        nbre_files_prod = 0
        for root, dirs, files in os.walk(
            settings.MERSENNE_PROD_DATA_FOLDER + "CRPHYS/CRPHYS_0__0_0"
        ):
            nbre_dirs_prod += len(dirs)
            nbre_files_prod += len(files)

        self.assertEqual(nbre_dirs_prod, nbre_dirs_test)
        self.assertEqual(nbre_files_prod, nbre_files_test)

        self._erase()
        shutil.rmtree(settings.MERSENNE_PROD_DATA_FOLDER)

    def test_05_archive_when_deploy(self):
        # ce qui est utilisé lors du déploiement
        # ptf_cmds.archiveIssuePtfCmd est testé lors du test archive global
        # TODO : tester la vue ?
        pass

    def test_06_delete_binary_files_with_http_request(self):
        self.import_from_archive(erase=False)

        response = self.client.get(
            reverse(
                "api_delete_jats_issue",
                kwargs={"colid": "CRPHYS", "pid": "CRPHYS_0__0_0", "site": "ptf_tools"},
            )
        )
        self.assertEqual(response.status_code, 302)
        # self.assertFalse(os.path.isdir(settings.MERSENNE_TEST_DATA_FOLDER + '/CRPHYS/CRPHYS_0__0_0') )

        self._erase()

    @override_settings(CROSSREF_BATCHURL="http://localhost/fake_doi")
    def test_07_check_xml_for_CROSSREF_doi(self):
        print(" ** test 07 export CROSSREF pour DOI avec validation de l'xml")
        self.import_from_archive(erase=False)

        article = Article.objects.first()

        doi.recordDOI(article, testing=True)
        doib = article.doibatch
        xml = str(doib.xml)

        tree = etree.XML(xml.encode("utf-8"))

        module_dir = os.path.join(os.path.dirname(__file__), "schemas")
        file_path = os.path.join(module_dir, "crossref4.4.2.xsd")
        xmlschema_doc = etree.parse(file_path)
        xmlschema = etree.XMLSchema(xmlschema_doc)
        xmlschema.assertValid(tree)
        is_valid = xmlschema.validate(tree)
        self.assertTrue(is_valid)

        self._erase()

    @override_settings(MERSENNE_PROD_DATA_FOLDER="/tmp/prod")
    @override_settings(MATHDOC_ARCHIVE_FOLDER=data_folder + "/archive")
    @override_settings(MERSENNE_TEST_DATA_FOLDER="/tmp")
    @override_settings(CEDRAM_XML_FOLDER=data_folder + "/cedram/exploitation")
    @override_settings(CEDRAM_TEX_FOLDER=data_folder + "/cedram/production_tex")
    @override_settings(MERSENNE_TMP_FOLDER="/tmp")
    @override_settings(MERSENNE_CREATE_FRONTPAGE=False)
    @override_settings(LOG_DIR=LOG_DIR_TEMP.name)
    def test_08_create_frontpage(self):
        print("test_08_create_frontpage")
        # self.import_from_archive(erase=False)

        # il faut importer un dossier cedrics qui ne soit pas CR* pour tester create frontpage
        # potentiellement avec date online first ?
        f = open(settings.CEDRAM_TEX_FOLDER + "/JEP/collection-jep.xml")
        body = f.read()
        f.close()
        cmd = xml_cmds.addCollectionsXmlCmd({"body": body})
        cmd.do()

        cmd1 = xml_cmds.importCedricsIssueDirectlyXmlCmd(
            {
                "colid": "JEP",
                "input_file": settings.CEDRAM_TEX_FOLDER
                + "/JEP/JEP_2021__8_/JEP_2021__8_-cdrxml.xml",
                "remove_email": False,
                "remove_date_prod": True,
                "diff_only": False,
            }
        )
        cmd1.do()

        container = model_helpers.get_container("JEP_2021__8_")

        cmd = ptf_cmds.publishResourcePtfCmd({"pre_publish": False})
        cmd.set_resource(container)
        container.article_set.all()
        updated_articles = cmd.do()
        # copie des fichiers nécessaires dans /tmp/log/tmp
        os.system("mkdir -p " + settings.LOG_DIR + "/tmp")
        os.system("cp " + self.data_folder + "/pdf_test/* " + settings.LOG_DIR + "/tmp/")

        # test utils.execute_cmd()
        with self.settings(MERSENNE_CREATE_FRONTPAGE=True):
            # os.system('touch '+ settings.LOG_DIR + '/cmd.logs')
            cmd_str = "touch " + settings.MERSENNE_TMP_FOLDER + "/toto"
            utils.execute_cmd(cmd_str)
        self.assertTrue(os.path.isfile(settings.MERSENNE_TMP_FOLDER + "/toto"))
        # add an online_first date
        art = updated_articles[0]
        art.date_online_first = datetime.now().astimezone()
        art.save()

        # test only logic, not real ssh cmd
        # on met en prod un doc et du coup creation de la frontpage avec date de mise en ligne
        tex.create_frontpage(
            container.my_collection.pid,
            container,
            updated_articles,
            test=False,
            replace_frontpage_only=True,
            skip_compilation=False,
        )

        # pas très propre, mais pour vérifier que des tex avec date_published ont été créés
        now = datetime.now().strftime("%Y-%m-%d")

        cmd_str = f'grep "\\datepublished{{{now}}}" ' + settings.LOG_DIR + "/tmp/*"
        import subprocess

        result = subprocess.check_output(cmd_str, shell=True)
        line_count = result.decode("utf-8").count("\n")
        self.assertEqual(line_count, 4)
        # test du pdf généré
        pdf_to_test = settings.LOG_DIR + "/tmp/dagnolo-kashiwara_FP.pdf"
        subprocess.check_output("qpdf --check " + pdf_to_test, shell=True)

        # test compile_tex
        container.my_collection.pid = "PCJ"
        container.my_collection.save()
        for updated_article in updated_articles:
            updated_article.my_container.my_collection.pid = "PCJ"

        tex.create_frontpage(
            container.my_collection.pid,
            container,
            updated_articles,
            test=False,
            replace_frontpage_only=True,
            skip_compilation=False,
        )

        cmd_str = (
            'grep "\\PCIincludepdf{article_JEP_2021__8__27_0.pdf}" ' + settings.LOG_DIR + "/tmp/*"
        )
        result = subprocess.check_output(cmd_str, shell=True)
        line_count = result.decode("utf-8").count("\n")
        self.assertEqual(line_count, 1)

        # test si l'ajout de métadonnées a fonctionné : avant pas d'ISSN dans les metadata
        cmd_str = (
            "exiftool  "
            + settings.LOG_DIR
            + '/tmp/dagnolo-kashiwara_FP.pdf_merged | grep "ISSN.*2429-7100" '
        )
        result = subprocess.check_output(cmd_str, shell=True)
        line_count = result.decode("utf-8").count("\n")
        self.assertEqual(line_count, 1)

        container.my_collection.pid = "JEP"
        container.my_collection.save()

        col = model_helpers.get_collection("JEP")
        provider = Provider.objects.get(name="mathdoc")
        issues = col.content.all()
        for issue in issues:
            params = {"pid": issue.pid, "ctype": issue.ctype}
            params["to_folder"] = None
            cmd = ptf_cmds.addContainerPtfCmd(params)
            cmd.set_provider(provider)
            cmd.set_object_to_be_deleted(issue)
            cmd.undo()

        cmd1 = ptf_cmds.addCollectionPtfCmd({"pid": col.pid})
        cmd1.set_provider(provider)
        cmd1.set_object_to_be_deleted(col)
        cmd1.undo()

        # os.remove(os.path.join(settings.MERSENNE_TMP_FOLDER, "fulltext.txt"))
        # os.remove(os.path.join(settings.MERSENNE_TMP_FOLDER, "fulltext.txt"))
        shutil.rmtree(settings.MERSENNE_TEST_DATA_FOLDER + "/JEP")
        self.assertFalse(os.path.isdir(settings.MERSENNE_TEST_DATA_FOLDER + "/CRPHYS"))

        self.check_empty_database()

    @override_settings(MERSENNE_PROD_DATA_FOLDER="/tmp/prod")
    @override_settings(MATHDOC_ARCHIVE_FOLDER=data_folder + "/archive")
    @override_settings(MERSENNE_TEST_DATA_FOLDER="/tmp")
    @override_settings(CEDRAM_XML_FOLDER=data_folder + "/cedram/exploitation")
    @override_settings(CEDRAM_TEX_FOLDER=data_folder + "/cedram/production_tex")
    @override_settings(MERSENNE_TMP_FOLDER="/tmp")
    @override_settings(MERSENNE_CREATE_FRONTPAGE=False)
    @override_settings(LOG_DIR=LOG_DIR_TEMP.name)
    def test_09_create_frontpage_main_workflow(self):
        """
        test_08_create_frontpage tests the recompilation of an article (to replace the front page)
        test_09_create_frontpage_main_workflow tests the regular compilation of an article, during publication
        """

        print("test_09_create_frontpage_main_workflow")
        # self.import_from_archive(erase=False)

        # il faut importer un dossier cedrics qui ne soit pas CR* pour tester create frontpage
        # potentiellement avec date online first ?
        f = open(settings.CEDRAM_TEX_FOLDER + "/JEP/collection-jep.xml")
        body = f.read()
        f.close()
        cmd = xml_cmds.addCollectionsXmlCmd({"body": body})
        cmd.do()

        cmd1 = xml_cmds.importCedricsIssueDirectlyXmlCmd(
            {
                "colid": "JEP",
                "input_file": settings.CEDRAM_TEX_FOLDER
                + "/JEP/JEP_2021__8_/JEP_2021__8_-cdrxml.xml",
                "remove_email": False,
                "remove_date_prod": True,
                "diff_only": False,
            }
        )
        cmd1.do()

        container = model_helpers.get_container("JEP_2021__8_")

        cmd = ptf_cmds.publishResourcePtfCmd({"pre_publish": False})
        cmd.set_resource(container)
        container.article_set.all()
        updated_articles = cmd.do()

        # add an online_first date
        art = updated_articles[0]
        art.date_online_first = datetime.now().astimezone()
        art.save()

        # test only logic, not real ssh cmd
        # on met en prod un doc et du coup creation de la frontpage avec date de mise en ligne
        tex.create_frontpage(container.my_collection.pid, container, updated_articles, test=False)

        # pdf_path = (
        #     settings.MERSENNE_TEST_DATA_FOLDER
        #     + "/"
        #     + art.get_binary_disk_location("self", "application/pdf", "")
        # )

        # # test si l'ajout de métadonnées a fonctionné : avant pas d'ISSN dans les metadata
        # cmd_str = f'exiftool  {pdf_path} | grep "ISSN.*2429-7100" '
        # import subprocess
        #
        # result = subprocess.check_output(cmd_str, shell=True)
        # line_count = result.decode("utf-8").count("\n")
        # self.assertEqual(line_count, 1)

        container.my_collection.pid = "JEP"
        container.my_collection.save()

        col = model_helpers.get_collection("JEP")
        provider = Provider.objects.get(name="mathdoc")
        issues = col.content.all()
        for issue in issues:
            params = {"pid": issue.pid, "ctype": issue.ctype}
            params["to_folder"] = None
            cmd = ptf_cmds.addContainerPtfCmd(params)
            cmd.set_provider(provider)
            cmd.set_object_to_be_deleted(issue)
            cmd.undo()

        cmd1 = ptf_cmds.addCollectionPtfCmd({"pid": col.pid})
        cmd1.set_provider(provider)
        cmd1.set_object_to_be_deleted(col)
        cmd1.undo()

        # os.remove(os.path.join(settings.MERSENNE_TMP_FOLDER, "fulltext.txt"))
        # os.remove(os.path.join(settings.MERSENNE_TMP_FOLDER, "fulltext.txt"))
        shutil.rmtree(settings.MERSENNE_TEST_DATA_FOLDER + "/JEP")

        self.check_empty_database()

    @override_settings(MERSENNE_TMP_FOLDER="/tmp")
    @override_settings(MERSENNE_TEST_DATA_FOLDER="/tmp")
    @override_settings(CEDRAM_TEX_FOLDER="/tmp")
    def test_10_compute_article_for_pcj(self):
        print("test_10_compute_article_for_pcj")
        from_folder = os.path.join(self.data_folder, "cedram/production_tex/PCJ")

        with open(f"{from_folder}/collection-pcj.xml") as collection_xml:
            collection_body = collection_xml.read()
        cmd0 = xml_cmds.addCollectionsXmlCmd({"body": collection_body})
        collections = cmd0.do()
        self.assertEqual(len(collections), 1)

        collection = collections[0]
        self.assertEqual(collection.pid, "PCJ")

        with open(f"{from_folder}/issue-pcj.xml") as issue_xml:
            issue_body = issue_xml.read()
        cmd1 = xml_cmds.addIssueXmlCmd({"body": issue_body})
        issue = cmd1.do()
        self.assertEqual(Container.objects.count(), 1)
        self.assertEqual(issue.pid, "PCJ_2024__4_")

        with open(f"{from_folder}/article.xml") as article_xml:
            body = article_xml.read()

        html_file_name = from_folder + "/10_24072_pcjournal_408.html"
        self.assertEqual(os.path.isfile(html_file_name), True)

        self.assertEqual(os.path.isdir(from_folder), True)
        self.assertIsInstance(collection, Collection)
        self.assertEqual(collection.pid, "PCJ")

        article, body = compute_article_for_pcj(
            body, collection, from_folder, issue, html_file_name=html_file_name
        )
        self.assertNotEqual(article.body_html, "")
        self.assertEqual(article.do_not_publish, True)

        pass

    # def test_00_check_schema(self):
    #     f = open('/mathdoc_archive/CRGEOS/CRGEOS_2020__352_4-5/CRGEOS_2020__352_4-5_309_0/CRGEOS_2020__352_4-5_309_0.xml')
    #     # f = open(
    #     #     '/mathdoc_archive/CRGEOS/CRGEOS_2020__352_4-5/CRGEOS_2020__352_4-5.xml')
    #     body = f.read()
    #     f.close()
    #
    #     tree = etree.XML(body.encode('utf-8'))
    #
    #     module_dir = os.path.join(os.path.dirname(__file__), 'schemas')
    #     file_path = os.path.join(module_dir, 'JATS-journalpublishing1-3d2-mathml3.xsd')
    #     xmlschema_doc = etree.parse(file_path)
    #     xmlschema = etree.XMLSchema(xmlschema_doc)
    #     xmlschema.assertValid(tree)
    #     is_valid = xmlschema.validate(tree)
    #     self.assertTrue(is_valid)
