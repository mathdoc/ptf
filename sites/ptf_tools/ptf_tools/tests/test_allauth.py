from ptf_tools.allauth_adapter import DisableSignUp


def test_allauth_adapter(admin_client):
    assert DisableSignUp(admin_client).is_open_for_signup(admin_client) is False
