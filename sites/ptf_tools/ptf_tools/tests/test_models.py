import datetime

from django.contrib.auth.models import User
from django.core import mail
from django.test import RequestFactory
from django.test import TestCase

from ..models import Invitation


class InviteTestCase(TestCase):
    def tearDown(self) -> None:
        Invitation.objects.all().delete()
        super().tearDown()

    def test_create_invitation(self):
        mail.outbox = []
        inviter = User.objects.create_user("inviter@test.test", "inviter@test.test", "*")
        request = RequestFactory().get("/")
        request.user = inviter

        email = "test_user@test.test"

        data = {"first_name": "Test", "last_name": "User"}

        # Try inviting without first_name entry
        self.assertRaises(KeyError, Invitation.get_invite, email, request, {"first_name": "Test"})
        self.assertRaises(KeyError, Invitation.get_invite, email, request, {"last_name": "User"})

        # Correct invite
        invite = Invitation.get_invite(email, request, data)

        self.assertEqual(invite.first_name, "Test")
        self.assertEqual(invite.last_name, "User")
        self.assertEqual(invite.extra_data, {})

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].to, [email])

        # Try inviting same user again - Should get the existing valid invitation
        mail.outbox = []
        self.assertEqual(len(Invitation.objects.all()), 1)

        invite_bis = Invitation.get_invite(email, request, data)

        self.assertEqual(len(Invitation.objects.all()), 1)
        self.assertEqual(len(mail.outbox), 0)

        # Try inviting same user again - Existing invitation being expired.
        mail.outbox = []
        with self.settings(INVITATIONS_INVITATION_EXPIRY=20):
            # Make the invitation expired
            invite_bis.sent -= datetime.timedelta(days=21)
            invite_bis.save()

            self.assertTrue(invite_bis.key_expired())
            self.assertEqual(len(Invitation.objects.all()), 1)

            invite_new = Invitation.get_invite(email, request, data)

            self.assertEqual(len(Invitation.objects.all()), 1)
            self.assertEqual(len(mail.outbox), 1)
            self.assertEqual(mail.outbox[0].to, [email])
