from django.contrib.auth.models import Group
from django.contrib.auth.models import User
from django.forms.models import model_to_dict
from django.test import Client
from django.test import TestCase
from django.urls import reverse

from ptf.factories import ArticleWithSiteFactory
from ptf.factories import CollectionFactory
from ptf.factories import ContainerWithSiteFactory
from ptf.model_helpers import get_site_id
from ptf_tools.factories import PageFactory
from ptf_tools.models import CollectionGroup


class MenuPagesTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.page1 = PageFactory()
        cls.page1.site_id = get_site_id("CRGEOS")
        cls.data = model_to_dict(cls.page1)
        cls.data.pop("parent_page")
        cls.col1 = CollectionFactory(pid="CR")
        cls.col2 = CollectionFactory(pid="CRGEOS")
        cls.col3 = CollectionFactory(pid="PCJ")

        cls.c1 = ContainerWithSiteFactory(my_collection=cls.col2)
        cls.article1 = ArticleWithSiteFactory(my_container=cls.c1)

        cls.client = Client()

        cls.group1 = Group(name="CR_Group")
        cls.group1.save()
        cls.group2 = Group(name="PCJ_Group")
        cls.group2.save()

        cls.collectiongroup1 = CollectionGroup(group=cls.group1)
        cls.collectiongroup1.save()
        cls.collectiongroup1.collections.set([cls.col1, cls.col2])

        cls.collectiongroup2 = CollectionGroup(group=cls.group2)
        cls.collectiongroup2.save()
        cls.collectiongroup2.collections.set([cls.col3])

        cls.user_cr = User.objects.create_user(username="user_cr", password="pass_1")
        cls.user_cr.groups.add(cls.group1)
        cls.user_cr.save()

        cls.user_pcj = User.objects.create_user(username="user_pcj", password="pass_2")
        cls.user_pcj.groups.add(cls.group2)
        cls.user_pcj.save()

    def test_01_index_menu_page_get(self):
        self.client.login(username="user_cr", password="pass_1")
        url = reverse("page_index", kwargs={"colid": self.col2.pid})
        response = self.client.get(url)
        assert response.status_code == 200

    def test_02_create_menu_page_get(self):
        self.client.login(username="user_cr", password="pass_1")
        url = reverse("page_create", kwargs={"colid": self.col2.pid})
        response = self.client.get(url)
        assert response.status_code == 200

    def test_03_create_menu_page_post(self):
        self.client.login(username="user_cr", password="pass_1")
        url = reverse("page_create", kwargs={"colid": self.col2.pid})
        response = self.client.post(url, data=self.data)
        assert response.status_code == 302
        assert response.url == "/page/" + self.col2.pid + "/"

    def test_04_create_menu_page_post_unauthenticated(self):
        url = reverse("page_create", kwargs={"colid": self.col2.pid})
        response = self.client.post(url, data=self.data)
        assert response.status_code == 302
        assert response.url == "/accounts/login/?next=/page/" + self.col2.pid + "/add/"

    def test_05_create_menu_page_post_403(self):
        self.client.login(username="user_pcj", password="pass_2")
        url = reverse("page_create", kwargs={"colid": self.col2.pid})
        response = self.client.post(url, data=self.data)
        assert response.status_code == 403

    def test_06_edit_menu_page_get(self):
        self.client.login(username="user_cr", password="pass_1")
        url = reverse("page_update", kwargs={"colid": self.col2.pid, "pk": self.page1.id})
        response = self.client.get(url)
        assert response.status_code == 200

    def test_07_edit_menu_page_post(self):
        self.client.login(username="user_cr", password="pass_1")
        url = reverse("page_update", kwargs={"colid": self.col2.pid, "pk": self.page1.id})
        response = self.client.post(url, data=self.data)
        assert response.status_code == 302

    def test_08_edit_menu_page_post_403(self):
        self.client.login(username="user_pcj", password="pass_2")
        url = reverse("page_update", kwargs={"colid": self.col2.pid, "pk": self.page1.id})
        response = self.client.post(url, data=self.data)
        assert response.status_code == 403

    def test_09_suggest_update_get(self):
        self.client.login(username="user_cr", password="pass_1")
        kwargs = {"colid": self.col2.pid, "doi": self.article1.doi}
        url = reverse("suggest_update", kwargs=kwargs)
        response = self.client.get(url)
        assert response.status_code == 200

    def test_10_suggest_deploy_post(self):
        self.client.login(username="user_cr", password="pass_1")
        kwargs = {"colid": self.col2.pid, "doi": self.article1.doi, "site": "local"}
        url = reverse("suggest_deploy", kwargs=kwargs)
        response = self.client.post(url, data={"dummy_data": "__"})
        assert response.status_code == 503

    def test_11_graphical_abstract_update_get(self):
        self.client.login(username="user_cr", password="pass_1")
        kwargs = {"colid": self.col2.pid, "doi": self.article1.doi}
        url = reverse("graphical_abstract_update", kwargs=kwargs)
        response = self.client.get(url)
        assert response.status_code == 200

    def test_12_graphical_abstract_deploy_post(self):
        self.client.login(username="user_cr", password="pass_1")
        kwargs = {"colid": self.col2.pid, "doi": self.article1.doi, "site": "local"}
        url = reverse("graphical_abstract_deploy", kwargs=kwargs)
        response = self.client.post(url, data={"dummy_data": "__"})
        assert response.status_code == 503

    def test_13_editor_article_get(self):
        self.client.login(username="user_cr", password="pass_1")
        kwargs = {"colid": self.col2.pid, "doi": self.article1.doi}
        url = reverse("editor_article", kwargs=kwargs)
        response = self.client.get(url)
        assert response.status_code == 200

    def test_14_editor_volume_items_get(self):
        self.client.login(username="user_cr", password="pass_1")
        kwargs = {"colid": self.col2.pid, "vid": self.c1.pid}
        url = reverse("editor_volume_items", kwargs=kwargs)
        response = self.client.get(url)
        assert response.status_code == 200
