\def\bysame{\leavevmode ---------\thinspace}
\makeatletter\if@francais\providecommand{\og}{<<~}\providecommand{\fg}{~>>}
\else\gdef\og{``}\gdef\fg{''}\fi\makeatother
\def\cdrandname{\&}
\providecommand\cdrnumero{no.~}
\providecommand{\cdredsname}{eds.}
\providecommand{\cdredname}{ed.}
\providecommand{\cdrchapname}{chap.}
\providecommand{\cdrmastersthesisname}{Memoir}
\providecommand{\cdrphdthesisname}{PhD Thesis}
\begin{thebibliography}{10}

\bibitem{gal1638}
G.~Galilei, \emph{Discours et d\'{e}monstrations math\'{e}matiques concernant
  deux sciences nouvelles}, Presses Universitaires de France, 1995, French
  translation by M. Clavelin.

\bibitem{newt1686}
I.~Newton, \emph{Principes math\'{e}matiques de la philosophie naturelle},
  Librairie scientifique et technique Albert Blanchard, 1966, French
  translation by la Marquise du Chastellet.

\bibitem{eotvos1922}
L.~E\"{o}tv\"{o}s, D.~Pek\'{a}r, E.~Fekete, {\og {Beitr\"{a}ge zum Gesetz der
  Proportionalit\"{a}t von Tr\"{a}gheit and Gravit\"{a}t}\fg}, \emph{Ann.
  Phys.} \textbf{68} (1922), p.~11, English translation in Annales
  Universitatis Scientiarium Budapestiensis de Rolando E\"{o}tv\"{o}s Nominate,
  Sectio Geologica, 7, 111, 1963.

\bibitem{schlam2008}
S.~Schlamminger, K.-Y. Choi, T.~A. Wagner, J.~H. Gundlach, E.~G. Adelberger,
  {\og {Test of the equivalence principle using a rotating torsion
  balance}\fg}, \emph{Phys. Rev. Lett.} \textbf{100} (2008), \cdrnumero 4,
  article ID 041101.

\bibitem{chabe2020}
J.~Chab\'{e}, C.~Courde, J.-M. Torre, S.~Bouquillon, A.~Bourgoin, M.~Aimar,
  D.~Alban\`{e}se, B.~Chauvineau, H.~Mariey, G.~Martinot-Lagarde, N.~Maurice,
  D.-H. Phung, E.~Samain, H.~Viot, {\og {Recent progress in lunar laser ranging
  at grasse laser ranging station}\fg}, \emph{Earth Space Sci.} \textbf{7}
  (2020), \cdrnumero 3,  article ID e2019EA000785.

\bibitem{nord1968}
K.~Nordtvedt, {\og {Testing relativity with laser ranging to the moon}\fg},
  \emph{Phys. Rev.} \textbf{170} (1968), p.~1186-1187.

\bibitem{visnu2018}
V.~Viswanathan, A.~Fienga, O.~Minazzoli, L.~Bernus, J.~Laskar, M.~Gastineau,
  {\og {The new lunar ephemeris INPOP17a and its application to fundamental
  physics}\fg}, \emph{Mon. Not. R. Astron. Soc.} \textbf{476} (2018),
  \cdrnumero 2, p.~1877-1888.

\bibitem{einstein07}
A.~Einstein, {\og {\"{U}ber das Relativit\"{a}tsprinzip und die aus demselben
  gezogene Folgerunge}\fg}, \emph{Jahrb. Radioakt Elektronik} \textbf{4}
  (1907), p.~411, English translation in The Collected Papers of Albert
  Einstein Volume 2, A. Beck and P. Havas (Princeton University Press, 1989)
  doc.47.

\bibitem{einstein16}
A.~Einstein, {\og {Die Grundlage der allgemeinen Relativit\"{a}tstheorie}\fg},
  \emph{Ann. Phys.} \textbf{354} (1916), p.~769-822.

\bibitem{abbott2016}
B.~P. Abbott {\normalfont \emph{et~al.}}, {\og {Observation of gravitational
  waves from a binary black hole merger}\fg}, \emph{Phys. Rev. Lett.}
  \textbf{116} (2016),  article ID 061102.

\bibitem{damour2012}
T.~Damour, {\og {Theoretical aspects of the equivalence principle}\fg},
  \emph{Class. Quantum Gravity} \textbf{29} (2012), \cdrnumero 18,  article ID
  184001.

\bibitem{chapman01}
P.~K. Chapman, A.~J. Hanson, {\og {An E\"{o}tv\"{o}s experiment in earth
  orbit}\fg}, in \emph{Proc. Conf. on Experimental Tests of Gravitation
  Theories} (R.~W. Davies, \cdredname), vol. JPL TM 33-499, California
  Institute of Technology, Passadena, 1970, p.~228.

\bibitem{everitt2003}
C.~Everitt, T.~Damour, K.~Nordtvedt, R.~Reinhard, {\og {Historical perspective
  on testing the equivalence principle}\fg}, \emph{Adv. Space Res.} \textbf{32}
  (2003), \cdrnumero 7, p.~1297-1300.

\bibitem{toub2001}
P.~Touboul, M.~Rodrigues, G.~M\'{e}tris, B.~Tatry, {\og {MICROSCOPE, testing
  the equivalence principle in space}\fg}, \emph{C. R. Acad. Sci., Paris IV}
  \textbf{2} (2001), \cdrnumero 9, p.~1271-1286.

\bibitem{toub2012}
P.~Touboul, G.~M\'{e}tris, V.~Lebat, A.~Robert, {\og {The MICROSCOPE
  experiment, ready for the in-orbit test of the equivalence principle}\fg},
  \emph{Class. Quantum Gravity} \textbf{29} (2012), \cdrnumero 18,  article ID
  184010.

\bibitem{hardy2013}
E.~Hardy, A.~Levy, M.~Rodrigues, P.~Touboul, G.~M\'{e}tris, {\og {Validation of
  the in-flight calibration procedures for the MICROSCOPE space mission}\fg},
  \emph{Adv. Space Res.} \textbf{52} (2013), \cdrnumero 9, p.~1634-1646.

\bibitem{Touboul_2019}
P.~Touboul, G.~M\'{e}tris, M.~Rodrigues, Y.~Andr\'{e}, Q.~Baghi, J.~Berg\'{e},
  D.~Boulanger, S.~Bremer, R.~Chhun, B.~Christophe, V.~Cipolla, T.~Damour,
  P.~Danto, H.~Dittus, P.~Fayet, B.~Foulon, P.-Y. Guidotti, E.~Hardy, P.-A.
  Huynh, C.~L\"{a}mmerzahl, V.~Lebat, F.~Liorzou, M.~List, I.~Panet, S.~Pires,
  B.~Pouilloux, P.~Prieur, S.~Reynaud, B.~Rievers, A.~Robert, H.~Selig,
  L.~Serron, T.~Sumner, P.~Visser, {\og {Space test of the equivalence
  principle: first results of the MICROSCOPE mission}\fg}, \emph{Class. Quantum
  Gravity} \textbf{36} (2019), \cdrnumero 22,  article ID 225006.

\bibitem{prieur05}
C.~Fallet, M.~Le~Du, C.~Pittet, P.~Prieur, A.~Torres, {\og {First in orbit
  results from DEMETER}\fg}, in \emph{28th Annual AAS Control and Guidance
  Conference}, Univelt, San Diego, CA, 2005.

\bibitem{ledu02}
M.~Le~Du, J.~Maureau, P.~Prieur, {\og {Myriade : an adaptative concept}\fg}, in
  \emph{5th ESA International Conference on Spacecraft Guidance, Navigation and
  Control Systems in Frascati}, ESA Publications Division, Oordwijk, 2002.

\bibitem{prieur17}
P.~Prieur, T.~Lienart, M.~Rodrigues, P.~Touboul, T.~Denver, J.~L. Jorgensen,
  A.~M. Bang, G.~Metris, {\og {MICROSCOPE mission: on-orbit assessment of the
  drag-free and attitude control system}\fg}, in \emph{Paper presented at 31st
  International Symposium on Space Technology and Science (ISTS 2017),
  Matsuyama-Ehime, Japan}, 2017.

\bibitem{touboul04}
P.~Touboul, B.~Foulon, M.~Rodrigues, J.~P. Marque, {\og {In orbit nano-g
  measurements, lessons for future space missions}\fg}, \emph{Aerosp. Sci.
  Technol.} \textbf{8} (2004), \cdrnumero 5, p.~431-441.

\bibitem{flury08}
J.~Flury, S.~Bettadpur, B.~D. Tapley, {\og {Precise accelerometry onboard the
  grace gravity field satellite mission}\fg}, \emph{Adv. Space Res.}
  \textbf{42} (2008), \cdrnumero 8, p.~1414-1423.

\bibitem{marque10}
J.-P. Marque, B.~Christophe, B.~Foulon, {\og {Accelerometers of the GOCE
  mission: return of experience from one year of in-orbit}\fg}, in \emph{ESA
  Living Planet Symposium}, ESA Special Publication, vol. 686, 2010, p.~57.

\bibitem{rummel11}
R.~Rummel, W.~Yi, C.~Stummer, {\og {GOCE gravitational gradiometry}\fg},
  \emph{J. Geod.} \textbf{85} (2011), p.~777-790.

\bibitem{willemenot00b}
E.~Willemenot, P.~Touboul, {\og {On-ground investigation of space
  accelerometers noise with an electrostatic torsion pendulum}\fg}, \emph{Rev.
  Sci. Instrum.} \textbf{71} (2000), \cdrnumero 1, p.~302-309.

\bibitem{Toub2017}
P.~Touboul, G.~M\'{e}tris, M.~Rodrigues, Y.~Andr\'{e}, Q.~Baghi, J.~Berg\'{e},
  D.~Boulanger, S.~Bremer, P.~Carle, R.~Chhun, B.~Christophe, V.~Cipolla,
  T.~Damour, P.~Danto, H.~Dittus, P.~Fayet, B.~Foulon, C.~Gageant, P.-Y.
  Guidotti, D.~Hagedorn, E.~Hardy, P.-a. Huynh, H.~Inchauspe, P.~Kayser,
  S.~Lala, C.~L\"{a}mmerzahl, V.~Lebat, P.~Leseur, F.~Liorzou, M.~List,
  F.~L\"{o}ffler, I.~Panet, B.~Pouilloux, P.~Prieur, A.~Rebray, S.~Reynaud,
  B.~Rievers, A.~Robert, H.~Selig, L.~Serron, T.~Sumner, N.~Tanguy, P.~Visser,
  {\og {MICROSCOPE mission: first results of a space test of the equivalence
  principle}\fg}, \emph{Phys. Rev. Lett.} \textbf{119} (2017), \cdrnumero 23,
  p.~231101-1-231101-7.

\end{thebibliography}
