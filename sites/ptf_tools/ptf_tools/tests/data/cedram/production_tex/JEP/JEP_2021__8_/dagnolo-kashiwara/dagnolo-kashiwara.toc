\select@language {english}
\select@language {english}
\select@language {french}
\select@language {english}
\contentsline {section}{\tocsection {}{1}{Introduction}}{28}{section.2}
\contentsline {section}{\tocsection {}{2}{Notations and complements}}{29}{section.3}
\contentsline {subsection}{\tocsubsection {}{2.1}{Bordered spaces}}{29}{subsection.4}
\contentsline {subsection}{\tocsubsection {}{2.2}{Ind-sheaves on good spaces}}{30}{subsection.11}
\contentsline {subsection}{\tocsubsection {}{2.3}{Ind-sheaves on bordered spaces}}{31}{subsection.13}
\contentsline {subsection}{\tocsubsection {}{2.4}{Enhanced ind-sheaves}}{33}{subsection.25}
\contentsline {subsection}{\tocsubsection {}{2.5}{Stable objects}}{34}{subsection.30}
\contentsline {subsection}{\tocsubsection {}{2.6}{Constructible objects}}{35}{subsection.34}
\contentsline {section}{\tocsection {}{3}{Sheafification}}{35}{section.35}
\contentsline {subsection}{\tocsubsection {}{3.1}{Associated ind-sheaf}}{35}{subsection.36}
\contentsline {subsection}{\tocsubsection {}{3.2}{Associated sheaf}}{38}{subsection.46}
\contentsline {subsection}{\tocsubsection {}{3.3}{(Weak-)\tmspace +\thinmuskip {.1667em}constructibility}}{40}{subsection.65}
\contentsline {section}{\tocsection {}{4}{Stalk formula}}{42}{section.72}
\contentsline {subsection}{\tocsubsection {}{4.1}{Restriction and stalk formula}}{42}{subsection.73}
\contentsline {section}{\tocsection {}{5}{Specialization and microlocalization}}{44}{section.81}
\contentsline {subsection}{\tocsubsection {}{5.1}{Real oriented blow-up transforms}}{45}{subsection.82}
\contentsline {subsection}{\tocsubsection {}{5.2}{Sheafification on vector bundles}}{46}{subsection.84}
\contentsline {subsection}{\tocsubsection {}{5.3}{Specialization and microlocalization}}{47}{subsection.86}
\contentsline {section}{\tocsection {Appendix}{A}{Complements on enhanced ind-sheaves}}{50}{appendix.91}
\contentsline {section}{\tocsection {Appendix}{B}{Complements on weak constructibility}}{51}{appendix.98}
\contentsline {subsection}{\tocsubsection {}{B.1}{Lojasiewicz's inequalities}}{52}{subsection.99}
\contentsline {subsection}{\tocsubsection {}{B.2}{Barycentric decomposition}}{53}{subsection.106}
\contentsline {subsection}{\tocsubsection {}{B.3}{Proof of Theorem\nonbreakingspace \ref {th:subanalytic ext}}}{54}{subsection.120}
\contentsline {section}{\tocsection {Appendix}{}{References}}{55}{section*.123}
