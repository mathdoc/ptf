% reading source alvarez-igusa.bib
%
\citation{AGZV12}{cite:AGV85}{bid12}{year}{book}
\bauthors{\bpers[V. I.]{V. I.}{}{Arnold}{}\bpers[S. M.]{S. M.}{}{Gusein-Zade}{}\bpers[A. N.]{A. N.}{}{Varchenko}{}}
\cititem{btitle}{Singularities of differentiable maps. {V}olume~1}
\cititem{bseries}{Modern Birkh\"{a}user Classics}
\cititem{bpublisher}{Birkh\"{a}user/Springer}
\cititem{baddress}{New York}
\cititem{byear}{2012}
\endcitation
%
\citation{AK18}{cite:AK18}{bid66}{year}{article}
\bauthors{\bpers[Mohammed]{M.}{}{Abouzaid}{}\bpers[Thomas]{T.}{}{Kragh}{}}
\cititem{btitle}{Simple homotopy equivalence of nearby {L}agrangians}
\cititem{bjournal}{Acta Math.}
\cititem{bnumber}{2}
\cititem{bvolume}{220}
\cititem{byear}{2018}
\cititem{bpages}{207--237}
\cititem{bdoi}{10.4310/ACTA.2018.v220.n2.a1}
\endcitation
%
\citation{Bar64}{cite:B64}{bid55}{year}{phdthesis}
\bauthors{\bpers[D.]{D.}{}{Barden}{}}
\cititem{btitle}{On the structure and classification of differential manifolds}
\cititem{btype}{Ph. D. Thesis}
\cititem{bschool}{Cambridge University}
\cititem{byear}{1964}
\endcitation
%
\citation{BFG+18}{cite:5W}{bid10}{year}{misc}
\bauthors{\bpers[Karin]{K.}{}{Baur}{}\bpers[Eleonore]{E.}{}{Faber}{}\bpers[Sira]{S.}{}{Gratz}{}\bpers[Khrystyna]{K.}{}{Serhiyenko}{}\bpers[Gordana]{G.}{}{Todorov}{}}
\cititem{btitle}{Friezes satisfying higher {$\mathrm{SL}_k$}-determinants}
\cititem{byear}{2018}
\cititem{eprint}{1810.10562}
\cititem{bnote}{to appear in \textit{Algebra \& Number Theory}}
\endcitation
%
\citation{BL95}{cite:BL95}{bid50}{year}{article}
\bauthors{\bpers[Jean-Michel]{J.-M.}{}{Bismut}{}\bpers[John]{J.}{}{Lott}{}}
\cititem{btitle}{Flat vector bundles, direct images and higher real analytic torsion}
\cititem{bjournal}{J.~Amer. Math. Soc.}
\cititem{bnumber}{2}
\cititem{bvolume}{8}
\cititem{byear}{1995}
\cititem{bpages}{291--363}
\cititem{bdoi}{10.2307/2152820}
\endcitation
%
\citation{Cer70}{cite:C70}{bid58}{year}{article}
\bauthors{\bpers[Jean]{J.}{}{Cerf}{}}
\cititem{btitle}{La stratification naturelle des espaces de fonctions diff\'{e}rentiables r\'{e}elles et le th\'{e}or\`eme de la pseudo-isotopie}
\cititem{bjournal}{Publ. Math. Inst. Hautes {\'E}tudes Sci.}
\cititem{bvolume}{39}
\cititem{byear}{1970}
\cititem{bpages}{5--173}
\csname @href\endcsname{http://www.numdam.org/item?id=PMIHES_1970__39__5_0}{\url{http://www.numdam.org/item?id=PMIHES_1970__39__5_0}}
\endcitation
%
\citation{Cha19}{cite:C17}{bid71}{year}{article}
\bauthors{\bpers[François]{F.}{}{Charette}{}}
\cititem{btitle}{Quantum {R}eidemeister torsion, open {G}romov-{W}itten invariants and a spectral sequence of {Oh}}
\cititem{bjournal}{Internat. Math. Res. Notices}
\cititem{bnumber}{8}
\cititem{byear}{2019}
\cititem{bpages}{2483--2518}
\cititem{bdoi}{10.1093/imrn/rnx195}
\endcitation
%
\citation{Cha84}{cite:C84}{bid14}{year}{article}
\bauthors{\bpers[Marc]{M.}{}{Chaperon}{}}
\cititem{btitle}{Une id\'{e}e du type `g\'{e}od\'{e}siques bris\'{e}es' pour les syst\`emes hamiltoniens}
\cititem{bjournal}{C.~R. Acad. Sci. Paris S{\'e}r. I Math.}
\cititem{bnumber}{13}
\cititem{bvolume}{298}
\cititem{byear}{1984}
\cititem{bpages}{293--296}
\endcitation
%
\citation{Che02}{cite:C02}{bid1}{year}{article}
\bauthors{\bpers[Yuri V.]{Y. V.}{}{Chekanov}{}}
\cititem{btitle}{Differential algebra of {L}egendrian links}
\cititem{bjournal}{Invent. Math.}
\cititem{bnumber}{3}
\cititem{bvolume}{150}
\cititem{byear}{2002}
\cititem{bpages}{441--483}
\cititem{bdoi}{10.1007/s002220200212}
\endcitation
%
\citation{Che96}{cite:C96}{bid17}{year}{article}
\bauthors{\bpers[Yuri V.]{Y. V.}{}{Chekanov}{}}
\cititem{btitle}{Critical points of quasi-functions and generating families of {L}egendrian manifolds}
\cititem{bjournal}{Funct. Anal. Appl.}
\cititem{bnumber}{2}
\cititem{bvolume}{30}
\cititem{byear}{1996}
\cititem{bpages}{118--128}
\endcitation
%
\citation{CM18}{cite:CM18}{bid9}{year}{article}
\bauthors{\bpers[Roger]{R.}{}{Casals}{}\bpers[Emmy]{E.}{}{Murphy}{}}
\cititem{btitle}{Differential algebra of cubic planar graphs}
\cititem{bjournal}{Adv. Math.}
\cititem{bvolume}{338}
\cititem{byear}{2018}
\cititem{bpages}{401--446}
\cititem{bdoi}{10.1016/j.aim.2018.09.002}
\endcitation
%
\citation{de40}{cite:dR39}{bid39}{year}{article}
\bauthors{\bpers[Georges]{G.}{}{de Rham}{}}
\cititem{btitle}{Sur les complexes avec automorphismes}
\cititem{bjournal}{Comment. Math. Helv.}
\cititem{bvolume}{12}
\cititem{byear}{1940}
\cititem{bpages}{191--211}
\cititem{bdoi}{10.1007/BF01620647}
\endcitation
%
\citation{Dim11}{cite:DR11}{bid30}{year}{article}
\bauthors{\bpers[Georgios]{G.}{}{Dimitroglou Rizell}{}}
\cititem{btitle}{Knotted {L}egendrian surfaces with few {R}eeb chords}
\cititem{bjournal}{Algebraic Geom. Topol.}
\cititem{bnumber}{5}
\cititem{bvolume}{11}
\cititem{byear}{2011}
\cititem{bpages}{2903--2936}
\cititem{bdoi}{10.2140/agt.2011.11.2903}
\endcitation
%
\citation{DWW03}{cite:DWW03}{bid52}{year}{article}
\bauthors{\bpers[W.]{W.}{}{Dwyer}{}\bpers[M.]{M.}{}{Weiss}{}\bpers[B.]{B.}{}{Williams}{}}
\cititem{btitle}{A parametrized index theorem for the algebraic {$K$}-theory {E}uler class}
\cititem{bjournal}{Acta Math.}
\cititem{bnumber}{1}
\cititem{bvolume}{190}
\cititem{byear}{2003}
\cititem{bpages}{1--104}
\cititem{bdoi}{10.1007/BF02393236}
\endcitation
%
\citation{EES07}{cite:EES07}{bid3}{year}{article}
\bauthors{\bpers[Tobias]{T.}{}{Ekholm}{}\bpers[John]{J.}{}{Etnyre}{}\bpers[Michael G.]{M. G.}{}{Sullivan}{}}
\cititem{btitle}{Legendrian contact homology in {$P\times\mathbb R$}}
\cititem{bjournal}{Trans. Amer. Math. Soc.}
\cititem{bnumber}{7}
\cititem{bvolume}{359}
\cititem{byear}{2007}
\cititem{bpages}{3301--3335}
\cititem{bdoi}{10.1090/S0002-9947-07-04337-1}
\endcitation
%
\citation{EG98}{cite:EG98}{bid11}{year}{incollection}
\bauthors{\bpers[Yakov M.]{Y. M.}{}{Eliashberg}{}\bpers[Misha]{M.}{}{Gromov}{}}
\cititem{btitle}{Lagrangian intersection theory: finite-dimensional approach}
\cititem{bbooktitle}{Geometry of differential equations}
\cititem{bseries}{Amer. Math. Soc. Transl. Ser.~2}
\cititem{bvolume}{186}
\cititem{bpublisher}{American Mathematical Society}
\cititem{baddress}{Providence, RI}
\cititem{byear}{1998}
\cititem{bpages}{27--118}
\cititem{bdoi}{10.1090/trans2/186/02}
\endcitation
%
\citation{Eli98}{cite:E98}{bid2}{year}{incollection}
\bauthors{\bpers[Yakov M.]{Y. M.}{}{Eliashberg}{}}
\cititem{btitle}{Invariants in contact topology}
\cititem{bbooktitle}{Proceedings of the {I}{C}{M}, {V}ol.~{II} ({B}erlin, 1998)}
\cititem{bseries}{Doc. Math.}
\cititem{bpublisher}{Deutsche Mathematiker-Vereinigung}
\cititem{baddress}{Berlin}
\cititem{byear}{1998}
\cititem{bpages}{327--338}
\cititem{bnote}{Extra Vol.~II}
\endcitation
%
\citation{EM12}{cite:EM12}{bid32}{year}{incollection}
\bauthors{\bpers[Yakov M.]{Y. M.}{}{Eliashberg}{}\bpers[N. M.]{N. M.}{}{Mishachev}{}}
\cititem{btitle}{The space of framed functions is contractible}
\cititem{bbooktitle}{Essays in mathematics and its applications}
\cititem{bpublisher}{Springer}
\cititem{baddress}{Heidelberg}
\cititem{byear}{2012}
\cititem{bpages}{81--109}
\cititem{bdoi}{10.1007/978-3-642-28821-0_5}
\endcitation
%
\citation{FI04}{cite:FI04}{bid23}{year}{article}
\bauthors{\bpers[Dmitry]{D.}{}{Fuchs}{}\bpers[Tigran]{T.}{}{Ishkhanov}{}}
\cititem{btitle}{Invariants of {L}egendrian knots and decompositions of front diagrams}
\cititem{bjournal}{Moscow Math.~J.}
\cititem{bnumber}{3}
\cititem{bvolume}{4}
\cititem{byear}{2004}
\cititem{bpages}{707--717, 783}
\cititem{bdoi}{10.17323/1609-4514-2004-4-3-707-717}
\endcitation
%
\citation{FR11}{cite:FR11}{bid20}{year}{article}
\bauthors{\bpers[Dmitry]{D.}{}{Fuchs}{}\bpers[Dan]{D.}{}{Rutherford}{}}
\cititem{btitle}{Generating families and {L}egendrian contact homology in the standard contact space}
\cititem{bjournal}{J.~Topology}
\cititem{bnumber}{1}
\cititem{bvolume}{4}
\cititem{byear}{2011}
\cititem{bpages}{190--226}
\cititem{bdoi}{10.1112/jtopol/jtq033}
\endcitation
%
\citation{Fra35}{cite:F35}{bid38}{year}{article}
\bauthors{\bpers[Wolfgang]{W.}{}{Franz}{}}
\cititem{btitle}{\"{U}ber die {T}orsion einer \"{U}berdeckung}
\cititem{bjournal}{J.~reine angew. Math.}
\cititem{bvolume}{173}
\cititem{byear}{1935}
\cititem{bpages}{245--254}
\cititem{bdoi}{10.1515/crll.1935.173.245}
\endcitation
%
\citation{Fuk97}{cite:F95}{bid64}{year}{incollection}
\bauthors{\bpers[Kenji]{K.}{}{Fukaya}{}}
\cititem{btitle}{The symplectic {$s$}-cobordism conjecture: a summary}
\cititem{bbooktitle}{Geometry and physics ({A}arhus, 1995)}
\cititem{bseries}{Lecture Notes in Pure and Appl. Math.}
\cititem{bvolume}{184}
\cititem{bpublisher}{Dekker}
\cititem{baddress}{New York}
\cititem{byear}{1997}
\cititem{bpages}{209--219}
\endcitation
%
\citation{GKS12}{cite:GKS12}{bid5}{year}{article}
\bauthors{\bpers[St{\'e}phane]{S.}{}{Guillermou}{}\bpers[Masaki]{M.}{}{Kashiwara}{}\bpers[Pierre]{P.}{}{Schapira}{}}
\cititem{btitle}{Sheaf quantization of {H}amiltonian isotopies and applications to nondisplaceability problems}
\cititem{bjournal}{Duke Math.~J.}
\cititem{bnumber}{2}
\cititem{bvolume}{161}
\cititem{byear}{2012}
\cititem{bpages}{201--245}
\cititem{bdoi}{10.1215/00127094-1507367}
\endcitation
%
\citation{Hen11}{cite:H04}{bid25}{year}{article}
\bauthors{\bpers[Michael B.]{M. B.}{}{Henry}{}}
\cititem{btitle}{Connections between {F}loer-type invariants and {M}orse-type invariants of {L}egendrian knots}
\cititem{bjournal}{Pacific J.~Math.}
\cititem{bnumber}{1}
\cititem{bvolume}{249}
\cititem{byear}{2011}
\cititem{bpages}{77--133}
\cititem{bdoi}{10.2140/pjm.2011.249.77}
\endcitation
%
\citation{HI19}{cite:HI19}{bid7}{year}{misc}
\bauthors{\bpers[Eric J.]{E. J.}{}{Hanson}{}\bpers[Kiyoshi]{K.}{}{Igusa}{}}
\cititem{btitle}{A counterexample to the $\phi$-dimension conjecture}
\cititem{byear}{2019}
\cititem{eprint}{1911.00614}
\endcitation
%
\citation{HL99}{cite:HL99}{bid68}{year}{article}
\bauthors{\bpers[Michael]{M.}{}{Hutchings}{}\bpers[Yi-Jen]{Y.-J.}{}{Lee}{}}
\cititem{btitle}{Circle-valued {M}orse theory and {R}eidemeister torsion}
\cititem{bjournal}{Geom. Topol.}
\cititem{bvolume}{3}
\cititem{byear}{1999}
\cititem{bpages}{369--396}
\cititem{bdoi}{10.2140/gt.1999.3.369}
\endcitation
%
\citation{HR15}{cite:HR15}{bid27}{year}{article}
\bauthors{\bpers[Michael B.]{M. B.}{}{Henry}{}\bpers[Dan]{D.}{}{Rutherford}{}}
\cititem{btitle}{Equivalence classes of augmentations and {M}orse complex sequences of {L}egendrian knots}
\cititem{bjournal}{Algebraic Geom. Topol.}
\cititem{bnumber}{6}
\cititem{bvolume}{15}
\cititem{byear}{2015}
\cititem{bpages}{3323--3353}
\cititem{bdoi}{10.2140/agt.2015.15.3323}
\endcitation
%
\citation{HW73}{cite:HW73}{bid36}{year}{book}
\bauthors{\bpers[Allen]{A.}{}{Hatcher}{}\bpers[John]{J.}{}{Wagoner}{}}
\cititem{btitle}{Pseudo-isotopies of compact manifolds}
\cititem{bseries}{Ast{\'e}risque}
\cititem{bvolume}{6}
\cititem{bpublisher}{Soci{\'e}t{\'e} Math{\'e}matique de France}
\cititem{baddress}{Paris}
\cititem{byear}{1973}
\endcitation
%
\citation{Igu}{cite:K}{bid53}{year}{misc}
\bauthors{\bpers[Kiyoshi]{K.}{}{Igusa}{}}
\cititem{btitle}{The generalized {G}rassmann invariant}
\cititem{bnote}{preprint}
\endcitation
%
\citation{Igu02}{cite:I02}{bid48}{year}{book}
\bauthors{\bpers[Kiyoshi]{K.}{}{Igusa}{}}
\cititem{btitle}{Higher {F}ranz-{R}eidemeister torsion}
\cititem{bseries}{AMS/IP Studies in Advanced Math.}
\cititem{bvolume}{31}
\cititem{bpublisher}{American Mathematical Society}
\cititem{baddress}{Providence, RI}
\cititem{byear}{2002}
\cititem{bdoi}{10.1016/s0550-3213(02)00739-3}
\endcitation
%
\citation{Igu04}{cite:I04}{bid33}{year}{article}
\bauthors{\bpers[Kiyoshi]{K.}{}{Igusa}{}}
\cititem{btitle}{Combinatorial {M}iller-{M}orita-{M}umford classes and {W}itten cycles}
\cititem{bjournal}{Algebraic Geom. Topol.}
\cititem{bvolume}{4}
\cititem{byear}{2004}
\cititem{bpages}{473--520}
\cititem{bdoi}{10.2140/agt.2004.4.473}
\endcitation
%
\citation{Igu05}{cite:I05}{bid49}{year}{book}
\bauthors{\bpers[Kiyoshi]{K.}{}{Igusa}{}}
\cititem{btitle}{Higher complex torsion and the framing principle}
\cititem{bseries}{Mem. Amer. Math. Soc.}
\cititem{bvolume}{177, no.\,835}
\cititem{bpublisher}{American Mathematical Society}
\cititem{baddress}{Providence, RI}
\cititem{byear}{2005}
\cititem{bdoi}{10.1090/memo/0835}
\csname @href\endcsname{https://doi.org/10.1090/memo/0835}{\url{https://doi.org/10.1090/memo/0835}}
\endcitation
%
\citation{Igu79}{cite:I79}{bid59}{year}{phdthesis}
\bauthors{\bpers[Kiyoshi]{K.}{}{Igusa}{}}
\cititem{btitle}{The {$\mathrm{Wh}_3(\pi)$} obstruction for pseudoisotopy}
\cititem{btype}{Ph. D. Thesis}
\cititem{bschool}{Princeton University}
\cititem{byear}{1979}
\csname @href\endcsname{http://gateway.proquest.com/openurl?url_ver=Z39.88-2004\&rft_val_fmt=info:ofi/fmt:kev:mtx:dissertation\&res_dat=xri:pqdiss\&rft_dat=xri:pqdiss:7913362}{\url{http://gateway.proquest.com/openurl?url_ver=Z39.88-2004\&rft_val_fmt=info:ofi/fmt:kev:mtx:dissertation\&res_dat=xri:pqdiss\&rft_dat=xri:pqdiss:7913362}}
\endcitation
%
\citation{Igu84}{cite:I82}{bid60}{year}{incollection}
\bauthors{\bpers[Kiyoshi]{K.}{}{Igusa}{}}
\cititem{btitle}{What happens to {H}atcher and {W}agoner's formulas for {$\pi \sb{0}C(M)$} when the first {P}ostnikov invariant of {$M$} is nontrivial?}
\cititem{bbooktitle}{Algebraic {$K$}-theory, number theory, geometry and analysis ({B}ielefeld, 1982)}
\cititem{bseries}{Lect. Notes in Math.}
\cititem{bvolume}{1046}
\cititem{bpublisher}{Springer}
\cititem{baddress}{Berlin}
\cititem{byear}{1984}
\cititem{bpages}{104--172}
\cititem{bdoi}{10.1007/BFb0072020}
\endcitation
%
\citation{Igu87}{cite:I87}{bid31}{year}{article}
\bauthors{\bpers[Kiyoshi]{K.}{}{Igusa}{}}
\cititem{btitle}{The space of framed functions}
\cititem{bjournal}{Trans. Amer. Math. Soc.}
\cititem{bnumber}{2}
\cititem{bvolume}{301}
\cititem{byear}{1987}
\cititem{bpages}{431--477}
\cititem{bdoi}{10.2307/2000654}
\endcitation
%
\citation{Igu88}{cite:I88}{bid61}{year}{article}
\bauthors{\bpers[Kiyoshi]{K.}{}{Igusa}{}}
\cititem{btitle}{The stability theorem for smooth pseudoisotopies}
\cititem{bjournal}{$K$-Theory}
\cititem{bnumber}{1\nobreakdash-2}
\cititem{bvolume}{2}
\cititem{byear}{1988}
\cititem{bpages}{1-355}
\cititem{bdoi}{10.1007/BF00533643}
\endcitation
%
\citation{Igu93}{cite:IK93a}{bid47}{year}{article}
\bauthors{\bpers[Kiyoshi]{K.}{}{Igusa}{}}
\cititem{btitle}{The {B}orel regulator map on pictures.~{I}. {A} dilogarithm formula}
\cititem{bjournal}{$K$-Theory}
\cititem{bnumber}{3}
\cititem{bvolume}{7}
\cititem{byear}{1993}
\cititem{bpages}{201--224}
\cititem{bdoi}{10.1007/BF00961064}
\endcitation
%
\citation{IK93}{cite:IK93}{bid0}{year}{article}
\bauthors{\bpers[Kiyoshi]{K.}{}{Igusa}{}\bpers[John]{J.}{}{Klein}{}}
\cititem{btitle}{The {B}orel regulator map on pictures.~{II}. {A}n example from {M}orse theory}
\cititem{bjournal}{$K$-Theory}
\cititem{bnumber}{3}
\cititem{bvolume}{7}
\cititem{byear}{1993}
\cititem{bpages}{225--267}
\cititem{bdoi}{10.1007/BF00961065}
\endcitation
%
\citation{Jek89}{cite:J89}{bid35}{year}{article}
\bauthors{\bpers[Solomon M.]{S. M.}{}{Jekel}{}}
\cititem{btitle}{A simplicial formula and bound for the {E}uler class}
\cititem{bjournal}{Israel J.~Math.}
\cititem{bnumber}{1-3}
\cititem{bvolume}{66}
\cititem{byear}{1989}
\cititem{bpages}{247--259}
\cititem{bdoi}{10.1007/BF02765896}
\endcitation
%
\citation{JKS16}{cite:JKS16}{bid6}{year}{article}
\bauthors{\bpers[Bernt Tore]{B. T.}{}{Jensen}{}\bpers[Alastair D.]{A. D.}{}{King}{}\bpers[Xiuping]{X.}{}{Su}{}}
\cititem{btitle}{A categorification of {G}rassmannian cluster algebras}
\cititem{bjournal}{Proc. London Math. Soc.~(3)}
\cititem{bnumber}{2}
\cititem{bvolume}{113}
\cititem{byear}{2016}
\cititem{bpages}{185--212}
\cititem{bdoi}{10.1112/plms/pdw028}
\endcitation
%
\citation{JT06}{cite:T06}{bid19}{year}{article}
\bauthors{\bpers[Jill]{J.}{}{Jordan}{}\bpers[Lisa]{L.}{}{Traynor}{}}
\cititem{btitle}{Generating family invariants for {L}egendrian links of unknots}
\cititem{bjournal}{Algebraic Geom. Topol.}
\cititem{bvolume}{6}
\cititem{byear}{2006}
\cititem{bpages}{895--933}
\cititem{bdoi}{10.2140/agt.2006.6.895}
\endcitation
%
\citation{Kle89}{cite:K89}{bid46}{year}{phdthesis}
\bauthors{\bpers[John Robert]{J. R.}{}{Klein}{}}
\cititem{btitle}{The cell complex construction and higher {R}-torsion for bundles with framed {M}orse functions}
\cititem{btype}{Ph. D. Thesis}
\cititem{bschool}{Brandeis University}
\cititem{byear}{1989}
\csname @href\endcsname{http://gateway.proquest.com/openurl?url_ver=Z39.88-2004\&rft_val_fmt=info:ofi/fmt:kev:mtx:dissertation\&res_dat=xri:pqdiss\&rft_dat=xri:pqdiss:8922192}{\url{http://gateway.proquest.com/openurl?url_ver=Z39.88-2004\&rft_val_fmt=info:ofi/fmt:kev:mtx:dissertation\&res_dat=xri:pqdiss\&rft_dat=xri:pqdiss:8922192}}
\endcitation
%
\citation{Kon92}{cite:K92}{bid34}{year}{article}
\bauthors{\bpers[Maxim]{M.}{}{Kontsevich}{}}
\cititem{btitle}{Intersection theory on the moduli space of curves and the matrix {A}iry function}
\cititem{bjournal}{Comm. Math. Phys.}
\cititem{bnumber}{1}
\cititem{bvolume}{147}
\cititem{byear}{1992}
\cititem{bpages}{1--23}
\csname @href\endcsname{http://projecteuclid.org/euclid.cmp/1104250524}{\url{http://projecteuclid.org/euclid.cmp/1104250524}}
\endcitation
%
\citation{Kra18}{cite:K18}{bid63}{year}{misc}
\bauthors{\bpers[Thomas]{T.}{}{Kragh}{}}
\cititem{btitle}{Generating families for {L}agrangians in {$\mathbb{R}^{2n}$} and the {H}atcher-{W}aldhausen map}
\cititem{byear}{2018}
\cititem{eprint}{1804.02557}
\endcitation
%
\citation{Lau12}{cite:La11}{bid26}{year}{book}
\bauthors{\bpers[François]{F.}{}{Laudenbach}{}}
\cititem{btitle}{Transversalit\'{e}, courants et th\'{e}orie de {M}orse}
\cititem{bpublisher}{\'{E}ditions de l'\'{E}cole polytechnique}
\cititem{baddress}{Palaiseau}
\cititem{byear}{2012}
\endcitation
%
\citation{Lee05a}{cite:L05a}{bid69}{year}{article}
\bauthors{\bpers[Yi-Jen]{Y.-J.}{}{Lee}{}}
\cititem{btitle}{Reidemeister torsion in {F}loer-{N}ovikov theory and counting pseudo-holo\-morphic tori.~{I}}
\cititem{bjournal}{J.~Symplectic Geom.}
\cititem{bnumber}{2}
\cititem{bvolume}{3}
\cititem{byear}{2005}
\cititem{bpages}{221--311}
\csname @href\endcsname{http://projecteuclid.org/euclid.jsg/1144947796}{\url{http://projecteuclid.org/euclid.jsg/1144947796}}
\endcitation
%
\citation{Lee05b}{cite:L05b}{bid70}{year}{article}
\bauthors{\bpers[Yi-Jen]{Y.-J.}{}{Lee}{}}
\cititem{btitle}{Reidemeister torsion in {F}loer-{N}ovikov theory and counting pseudo-holomorphic tori. {II}}
\cititem{bjournal}{J.~Symplectic Geom.}
\cititem{bnumber}{3}
\cititem{bvolume}{3}
\cititem{byear}{2005}
\cititem{bpages}{385--480}
\csname @href\endcsname{http://projecteuclid.org/euclid.jsg/1144954879}{\url{http://projecteuclid.org/euclid.jsg/1144954879}}
\endcitation
%
\citation{LS85}{cite:LS85}{bid15}{year}{article}
\bauthors{\bpers[François]{F.}{}{Laudenbach}{}\bpers[Jean-Claude]{J.-C.}{}{Sikorav}{}}
\cititem{btitle}{Persistance d'intersection avec la section nulle au cours d'une isotopie hamiltonienne dans un fibr\'{e} cotangent}
\cititem{bjournal}{Invent. Math.}
\cititem{bnumber}{2}
\cititem{bvolume}{82}
\cititem{byear}{1985}
\cititem{bpages}{349--357}
\cititem{bdoi}{10.1007/BF01388807}
\endcitation
%
\citation{Maz63}{cite:M63}{bid56}{year}{article}
\bauthors{\bpers[Barry]{B.}{}{Mazur}{}}
\cititem{btitle}{Relative neighborhoods and the theorems of {S}male}
\cititem{bjournal}{Ann. of Math.~(2)}
\cititem{bvolume}{77}
\cititem{byear}{1963}
\cititem{bpages}{232--249}
\cititem{bdoi}{10.2307/1970215}
\endcitation
%
\citation{Mil61}{cite:M61}{bid40}{year}{article}
\bauthors{\bpers[John]{J.}{}{Milnor}{}}
\cititem{btitle}{Two complexes which are homeomorphic but combinatorially distinct}
\cititem{bjournal}{Ann. of Math.~(2)}
\cititem{bvolume}{74}
\cititem{byear}{1961}
\cititem{bpages}{575--590}
\cititem{bdoi}{10.2307/1970299}
\endcitation
%
\citation{Mil66}{cite:M66}{bid41}{year}{article}
\bauthors{\bpers[John]{J.}{}{Milnor}{}}
\cititem{btitle}{Whitehead torsion}
\cititem{bjournal}{Bull. Amer. Math. Soc.}
\cititem{bvolume}{72}
\cititem{byear}{1966}
\cititem{bpages}{358--426}
\cititem{bdoi}{10.1090/S0002-9904-1966-11484-2}
\endcitation
%
\citation{MT96}{cite:MT96}{bid43}{year}{article}
\bauthors{\bpers[Guowu]{G.}{}{Meng}{}\bpers[Clifford Henry]{C. H.}{}{Taubes}{}}
\cititem{btitle}{{$\underline{\mathit{SW}}=$} {M}ilnor torsion}
\cititem{bjournal}{Math. Res. Lett.}
\cititem{bnumber}{5}
\cititem{bvolume}{3}
\cititem{byear}{1996}
\cititem{bpages}{661--674}
\cititem{bdoi}{10.4310/MRL.1996.v3.n5.a8}
\endcitation
%
\citation{Mur19}{cite:E12}{bid13}{year}{misc}
\bauthors{\bpers[Emmy]{E.}{}{Murphy}{}}
\cititem{btitle}{Loose {L}egendrian embeddings in high dimensional contact manifolds}
\cititem{byear}{2019}
\cititem{eprint}{1201.2245v5}
\endcitation
%
\citation{Rei35}{cite:R35}{bid37}{year}{article}
\bauthors{\bpers[Kurt]{K.}{}{Reidemeister}{}}
\cititem{btitle}{Homotopieringe und {L}insenr\"{a}ume}
\cititem{bjournal}{Abh. Math. Sem. Univ. Hamburg}
\cititem{bnumber}{1}
\cititem{bvolume}{11}
\cititem{byear}{1935}
\cititem{bpages}{102--109}
\cititem{bdoi}{10.1007/BF02940717}
\endcitation
%
\citation{RS18}{cite:RS18}{bid28}{year}{article}
\bauthors{\bpers[Dan]{D.}{}{Rutherford}{}\bpers[Michael G.]{M. G.}{}{Sullivan}{}}
\cititem{btitle}{Generating families and augmentations for {L}egendrian surfaces}
\cititem{bjournal}{Algebraic Geom. Topol.}
\cititem{bnumber}{3}
\cititem{bvolume}{18}
\cititem{byear}{2018}
\cititem{bpages}{1675--1731}
\cititem{bdoi}{10.2140/agt.2018.18.1675}
\endcitation
%
\citation{RS71}{cite:RS71}{bid51}{year}{article}
\bauthors{\bpers[D. B.]{D. B.}{}{Ray}{}\bpers[I. M.]{I. M.}{}{Singer}{}}
\cititem{btitle}{{$R$}-torsion and the {L}aplacian on {R}iemannian manifolds}
\cititem{bjournal}{Adv. Math.}
\cititem{bvolume}{7}
\cititem{byear}{1971}
\cititem{bpages}{145--210}
\cititem{bdoi}{10.1016/0001-8708(71)90045-4}
\endcitation
%
\citation{Sab05}{cite:S05}{bid24}{year}{article}
\bauthors{\bpers[Joshua M.]{J. M.}{}{Sabloff}{}}
\cititem{btitle}{Augmentations and rulings of {L}egendrian knots}
\cititem{bjournal}{Internat. Math. Res. Notices}
\cititem{bnumber}{19}
\cititem{byear}{2005}
\cititem{bpages}{1157--1180}
\cititem{bdoi}{10.1155/IMRN.2005.1157}
\endcitation
%
\citation{Sab06}{cite:S06}{bid21}{year}{article}
\bauthors{\bpers[Joshua M.]{J. M.}{}{Sabloff}{}}
\cititem{btitle}{Duality for {L}egendrian contact homology}
\cititem{bjournal}{Geom. Topol.}
\cititem{bvolume}{10}
\cititem{byear}{2006}
\cititem{bpages}{2351--2381}
\cititem{bdoi}{10.2140/gt.2006.10.2351}
\endcitation
%
\citation{Sik86}{cite:S86}{bid16}{year}{article}
\bauthors{\bpers[Jean-Claude]{J.-C.}{}{Sikorav}{}}
\cititem{btitle}{Sur les immersions lagrangiennes dans un fibr\'{e} cotangent admettant une phase g\'{e}n\'{e}ratrice globale}
\cititem{bjournal}{C.~R. Acad. Sci. Paris S{\'e}r. I Math.}
\cititem{bnumber}{3}
\cititem{bvolume}{302}
\cititem{byear}{1986}
\cititem{bpages}{119--122}
\endcitation
%
\citation{Sma61}{cite:S61}{bid54}{year}{article}
\bauthors{\bpers[Stephen]{S.}{}{Smale}{}}
\cititem{btitle}{Generalized {P}oincar\'{e}'s conjecture in dimensions greater than four}
\cititem{bjournal}{Ann. of Math.~(2)}
\cititem{bvolume}{74}
\cititem{byear}{1961}
\cititem{bpages}{391--406}
\cititem{bdoi}{10.2307/1970239}
\endcitation
%
\citation{SS16}{cite:SS16}{bid22}{year}{article}
\bauthors{\bpers[Joshua M.]{J. M.}{}{Sabloff}{}\bpers[Michael G.]{M. G.}{}{Sullivan}{}}
\cititem{btitle}{Families of {L}egendrian submanifolds via generating families}
\cititem{bjournal}{Quantum Topol.}
\cititem{bnumber}{4}
\cititem{bvolume}{7}
\cititem{byear}{2016}
\cititem{bpages}{639--668}
\cititem{bdoi}{10.4171/QT/82}
\endcitation
%
\citation{STWZ19}{cite:STWZ15}{bid8}{year}{article}
\bauthors{\bpers[Vivek]{V.}{}{Shende}{}\bpers[David]{D.}{}{Treumann}{}\bpers[Harold]{H.}{}{Williams}{}\bpers[Eric]{E.}{}{Zaslow}{}}
\cititem{btitle}{Cluster varieties from {L}egendrian knots}
\cititem{bjournal}{Duke Math.~J.}
\cititem{bnumber}{15}
\cititem{bvolume}{168}
\cititem{byear}{2019}
\cititem{bpages}{2801--2871}
\cititem{bdoi}{10.1215/00127094-2019-0027}
\endcitation
%
\citation{STZ17}{cite:STZ17}{bid4}{year}{article}
\bauthors{\bpers[Vivek]{V.}{}{Shende}{}\bpers[David]{D.}{}{Treumann}{}\bpers[Eric]{E.}{}{Zaslow}{}}
\cititem{btitle}{Legendrian knots and constructible sheaves}
\cititem{bjournal}{Invent. Math.}
\cititem{bnumber}{3}
\cititem{bvolume}{207}
\cititem{byear}{2017}
\cititem{bpages}{1031--1133}
\cititem{bdoi}{10.1007/s00222-016-0681-5}
\endcitation
%
\citation{Sul02}{cite:S02}{bid65}{year}{article}
\bauthors{\bpers[Michael G.]{M. G.}{}{Sullivan}{}}
\cititem{btitle}{{$K$}-theoretic invariants for {F}loer homology}
\cititem{bjournal}{Geom. Funct. Anal.}
\cititem{bnumber}{4}
\cititem{bvolume}{12}
\cititem{byear}{2002}
\cititem{bpages}{810--872}
\cititem{bdoi}{10.1007/s00039-002-8267-3}
\endcitation
%
\citation{Su{\'a}17}{cite:S17}{bid67}{year}{article}
\bauthors{\bpers[Lara Simone]{L. S.}{}{Su{\'a}rez}{}}
\cititem{btitle}{Exact {L}agrangian cobordism and pseudo-isotopy}
\cititem{bjournal}{Internat.~J. Math.}
\cititem{bnumber}{8}
\cititem{bvolume}{28}
\cititem{byear}{2017}
\cititem{bpages}{1750059, 35}
\cititem{bdoi}{10.1142/S0129167X17500598}
\endcitation
%
\citation{Tra01}{cite:T01}{bid18}{year}{article}
\bauthors{\bpers[Lisa]{L.}{}{Traynor}{}}
\cititem{btitle}{Generating function polynomials for {L}egendrian links}
\cititem{bjournal}{Geom. Topol.}
\cititem{bvolume}{5}
\cititem{byear}{2001}
\cititem{bpages}{719--760}
\cititem{bdoi}{10.2140/gt.2001.5.719}
\endcitation
%
\citation{Tur86}{cite:T86}{bid42}{year}{article}
\bauthors{\bpers[Vladimir G.]{V. G.}{}{Turaev}{}}
\cititem{btitle}{Reidemeister torsion in knot theory}
\cititem{bjournal}{Uspehi Mat. Nauk}
\cititem{bnumber}{1(247)}
\cititem{bvolume}{41}
\cititem{byear}{1986}
\cititem{bpages}{97--147, 240}
\endcitation
%
\citation{Tur98}{cite:T98}{bid44}{year}{article}
\bauthors{\bpers[Vladimir G.]{V. G.}{}{Turaev}{}}
\cititem{btitle}{A combinatorial formulation for the {S}eiberg-{W}itten invariants of {$3$}-manifolds}
\cititem{bjournal}{Math. Res. Lett.}
\cititem{bnumber}{5}
\cititem{bvolume}{5}
\cititem{byear}{1998}
\cititem{bpages}{583--598}
\cititem{bdoi}{10.4310/MRL.1998.v5.n5.a3}
\endcitation
%
\citation{Vit92}{cite:V92}{bid29}{year}{article}
\bauthors{\bpers[Claude]{C.}{}{Viterbo}{}}
\cititem{btitle}{Symplectic topology as the geometry of generating functions}
\cititem{bjournal}{Math. Ann.}
\cititem{bnumber}{4}
\cititem{bvolume}{292}
\cititem{byear}{1992}
\cititem{bpages}{685--710}
\cititem{bdoi}{10.1007/BF01444643}
\endcitation
%
\citation{Wag78}{cite:W76}{bid45}{year}{inproceedings}
\bauthors{\bpers[J. B.]{J. B.}{}{Wagoner}{}}
\cititem{btitle}{Diffeomorphisms, {$K\sb{2}$}, and analytic torsion}
\cititem{bbooktitle}{Algebraic and geometric topology ({P}roc. {S}ympos. {P}ure {M}ath., {S}tanford {U}niv., {S}tanford, {C}alif., 1976), {P}art~1}
\cititem{bseries}{Proc. Sympos. Pure Math.}
\cititem{bvolume}{XXXII}
\cititem{bpublisher}{American Mathematical Society}
\cititem{bpages}{23--33}
\cititem{baddress}{Providence, RI}
\cititem{byear}{1978}
\endcitation
%
\citation{Wal82}{cite:W82}{bid62}{year}{incollection}
\bauthors{\bpers[Friedhelm]{F.}{}{Waldhausen}{}}
\cititem{btitle}{Algebraic {$K$}-theory of spaces, a manifold approach}
\cititem{bbooktitle}{Current trends in algebraic topology, {P}art~1 ({L}ondon, {O}nt., 1981)}
\cititem{bseries}{CMS Conf. Proc.}
\cititem{bvolume}{2}
\cititem{bpublisher}{American Mathematical Society}
\cititem{baddress}{Providence, RI}
\cititem{byear}{1982}
\cititem{bpages}{141--184}
\endcitation
%
\citation{Whi50}{cite:W50}{bid57}{year}{article}
\bauthors{\bpers[J. H. C.]{J. H. C.}{}{Whitehead}{}}
\cititem{btitle}{Simple homotopy types}
\cititem{bjournal}{Amer.~J. Math.}
\cititem{bvolume}{72}
\cititem{byear}{1950}
\cititem{bpages}{1--57}
\cititem{bdoi}{10.2307/2372133}
\endcitation
