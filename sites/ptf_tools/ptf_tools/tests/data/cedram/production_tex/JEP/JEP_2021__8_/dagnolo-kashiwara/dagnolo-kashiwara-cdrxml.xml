<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE article SYSTEM '/home/cedram/XML/dtd/cedramarticle.dtd'>
<!-- Translated from latex by tralics 2.15.4, date: 2020/12/10-->
<article>
<production><fichier_tex>dagnolo-kashiwara</fichier_tex>
<fichier_bib>dagnolo-kashiwara</fichier_bib>
<fichier_tralics>dagnolo-kashiwara-cdrxml</fichier_tralics>
<date_prod>2020-12-10</date_prod>
<date_prod_PDF>2020-11-23</date_prod_PDF>
<timestamp>2020-12-10 11:13:00 MET</timestamp>
<pdftimestamp>2020-11-23 11:08:43 MET</pdftimestamp>
</production>
<gestion><date_reception>2020-02-16</date_reception>
<date_acceptation>2020-11-04</date_acceptation>
<paru>yes</paru>
<publishTeX>yes</publishTeX>
</gestion>
<idart>JEP_2021__8__27_0</idart>
<doi>10.5802/jep.140</doi>
<pagedeb systnum='arabe'>27</pagedeb>
<pagefin systnum='arabe'>55</pagefin>
<auteur><nomcomplet>Andrea D’Agnolo</nomcomplet>
<prenom>Andrea</prenom>
<nom>D’Agnolo</nom>
<adresse ordre='1'>Dipartimento di Matematica, Università di Padova via Trieste 63, 35121 Padova, Italy</adresse>
<mel><xref url='dagnolo@math.unipd.it'>dagnolo@math.<allowbreak/>unipd.<allowbreak/>it</xref></mel>
</auteur>
<auteur><nomcomplet>Masaki Kashiwara</nomcomplet>
<prenom>Masaki</prenom>
<nom>Kashiwara</nom>
<adresse ordre='2'>Kyoto University Institute for Advanced study, Research Institute for Mathematical Sciences, Kyoto University, Kyoto 606-8502, Japan &amp; Korea Institute for Advanced Study, Seoul 02455, Korea</adresse>
<mel><xref url='masaki@kurims.kyoto-u.ac.jp'>masaki@kurims.<allowbreak/>kyoto-u.<allowbreak/>ac.<allowbreak/>jp</xref></mel>
</auteur>
<nombre_auteurs>2</nombre_auteurs>
<nombre_contributeurs>0</nombre_contributeurs>
<titre xml:lang='fr'>Sur un analogue topologique de la régularisation pour les <formula type='inline'><math xmlns='http://www.w3.org/1998/Math/MathML'><mi>&Dscr;</mi></math></formula>-modules holonomes</titre><TeXtitre xml:lang='fr'>Sur un analogue topologique de la régularisation pour les <texmath textype='inline' type='inline'>\protect \mathscr{D}</texmath>-modules holonomes</TeXtitre>
<titre xml:lang='en'>On a topological counterpart of regularization for holonomic <formula type='inline'><math xmlns='http://www.w3.org/1998/Math/MathML'><mi>&Dscr;</mi></math></formula>-modules</titre><TeXtitre xml:lang='en'>On a topological counterpart of regularization for holonomic <texmath textype='inline' type='inline'>\protect \mathscr{D}</texmath>-modules</TeXtitre>
<langue>en</langue>
<resume xml:lang='fr'><p>Sur une variété complexe lisse, l’inclusion de la catégorie des <formula type='inline'><math xmlns='http://www.w3.org/1998/Math/MathML'><mi>&Dscr;</mi></math></formula>-modules holonomes réguliers dans celle des <formula type='inline'><math xmlns='http://www.w3.org/1998/Math/MathML'><mi>&Dscr;</mi></math></formula>-modules holonomes admet un foncteur quasi-inverse à gauche <formula type='inline'><math xmlns='http://www.w3.org/1998/Math/MathML'><mrow><mi>&Mscr;</mi><mo>&rightarrow;</mo><msub><mi>&Mscr;</mi> <mi mathvariant='normal'>reg</mi> </msub></mrow></math></formula>, appelé régularisation. Rappelons que <formula type='inline'><math xmlns='http://www.w3.org/1998/Math/MathML'><msub><mi>&Mscr;</mi> <mi mathvariant='normal'>reg</mi> </msub></math></formula> est reconstruit à partir du complexe de de Rham de <formula type='inline'><math xmlns='http://www.w3.org/1998/Math/MathML'><mi>&Mscr;</mi></math></formula> par la correspondance de Riemann-Hilbert régulière. De même, sur un espace topologique, l’inclusion des faisceaux dans les ind-faisceaux enrichis admet un foncteur quasi-inverse à gauche, qu’on appelle ici faisceautisation. La régularisation et la faisceautisation sont échangées par la correspondance de Riemann-Hilbert irrégulière. Dans ce travail, nous étudions certaines des propriétés du foncteur de faisceautisation. En particulier, nous fournissons une formule qui calcule la fibre du faisceautisé de la spécialisation et de la microlocalisation enrichies.</p></resume><TeXresume xml:lang='fr'><p>Sur une variété complexe lisse, l’inclusion de la catégorie des <texmath textype='inline' type='inline'>\mathcal{D}</texmath>-modules holonomes réguliers dans celle des <texmath textype='inline' type='inline'>\mathcal{D}</texmath>-modules holonomes admet un foncteur quasi-inverse à gauche <texmath textype='inline' type='inline'>\mathcal{M}\rightarrow \mathcal{M}_\mathrm{reg}</texmath>, appelé régularisation. Rappelons que <texmath textype='inline' type='inline'>\mathcal{M}_\mathrm{reg}</texmath> est reconstruit à partir du complexe de de Rham de <texmath textype='inline' type='inline'>\mathcal{M}</texmath> par la correspondance de Riemann-Hilbert régulière. De même, sur un espace topologique, l’inclusion des faisceaux dans les ind-faisceaux enrichis admet un foncteur quasi-inverse à gauche, qu’on appelle ici faisceautisation. La régularisation et la faisceautisation sont échangées par la correspondance de Riemann-Hilbert irrégulière. Dans ce travail, nous étudions certaines des propriétés du foncteur de faisceautisation. En particulier, nous fournissons une formule qui calcule la fibre du faisceautisé de la spécialisation et de la microlocalisation enrichies.</p></TeXresume>
<resume xml:lang='en'><p>On a complex manifold, the embedding of the category of regular holonomic <formula type='inline'><math xmlns='http://www.w3.org/1998/Math/MathML'><mi>&Dscr;</mi></math></formula>-modules into that of holonomic <formula type='inline'><math xmlns='http://www.w3.org/1998/Math/MathML'><mi>&Dscr;</mi></math></formula>-modules has a left quasi-inverse functor <formula type='inline'><math xmlns='http://www.w3.org/1998/Math/MathML'><mrow><mi>&Mscr;</mi><mo>&rightarrow;</mo><msub><mi>&Mscr;</mi> <mi mathvariant='normal'>reg</mi> </msub></mrow></math></formula>, called regularization. Recall that <formula type='inline'><math xmlns='http://www.w3.org/1998/Math/MathML'><msub><mi>&Mscr;</mi> <mi mathvariant='normal'>reg</mi> </msub></math></formula> is reconstructed from the de Rham complex of <formula type='inline'><math xmlns='http://www.w3.org/1998/Math/MathML'><mi>&Mscr;</mi></math></formula> by the regular Riemann-Hilbert correspondence. Similarly, on a topological space, the embedding of sheaves into enhanced ind-sheaves has a left quasi-inverse functor, called here sheafification. Regularization and sheafification are intertwined by the irregular Riemann-Hilbert correspondence. Here, we study some of the properties of the sheafification functor. In particular, we provide a stalk formula for the sheafification of enhanced specialization and microlocalization.</p></resume><TeXresume xml:lang='en'><p>On a complex manifold, the embedding of the category of regular holonomic <texmath textype='inline' type='inline'>\mathcal{D}</texmath>-modules into that of holonomic <texmath textype='inline' type='inline'>\mathcal{D}</texmath>-modules has a left quasi-inverse functor <texmath textype='inline' type='inline'>\mathcal{M}\rightarrow \mathcal{M}_\mathrm{reg}</texmath>, called regularization. Recall that <texmath textype='inline' type='inline'>\mathcal{M}_\mathrm{reg}</texmath> is reconstructed from the de Rham complex of <texmath textype='inline' type='inline'>\mathcal{M}</texmath> by the regular Riemann-Hilbert correspondence. Similarly, on a topological space, the embedding of sheaves into enhanced ind-sheaves has a left quasi-inverse functor, called here sheafification. Regularization and sheafification are intertwined by the irregular Riemann-Hilbert correspondence. Here, we study some of the properties of the sheafification functor. In particular, we provide a stalk formula for the sheafification of enhanced specialization and microlocalization.</p></TeXresume>
<motcle xml:lang='fr'>Correspondance de Riemann-Hilbert irrégulière, faisceau pervers enrichi, D-module holonome</motcle><TeXmotcle xml:lang='fr'>Correspondance de Riemann-Hilbert irrégulière, faisceau pervers enrichi, D-module holonome</TeXmotcle>
<motcle xml:lang='en'>Irregular Riemann-Hilbert correspondence, enhanced perverse sheaves, holonomic D-modules</motcle><TeXmotcle xml:lang='en'>Irregular Riemann-Hilbert correspondence, enhanced perverse sheaves, holonomic D-modules</TeXmotcle>
<msc>32C38, 14F05</msc>
<biblio><bib_entry user-id='DK16' id='bid1' doctype='article'><reference>1</reference>
<bauteur><nom>D’Agnolo</nom><prenom>Andrea</prenom><initiale>A.</initiale></bauteur><bauteur><nom>Kashiwara</nom><prenom>Masaki</prenom><initiale>M.</initiale></bauteur>
<title>Riemann-Hilbert correspondence for holonomic D-modules</title><TeXtitle>Riemann-Hilbert correspondence for holonomic D-modules</TeXtitle><journal>Publ. Math. Inst. Hautes Études Sci.</journal>
<volume>123</volume>
<year>2016</year>
<pagedeb>69</pagedeb><pagefin>197</pagefin><pages>69–197</pages>
<doi>10.1007/s10240-015-0076-y</doi></bib_entry><bib_entry user-id='DK18' id='bid2' doctype='article'><reference>2</reference>
<bauteur><nom>D’Agnolo</nom><prenom>Andrea</prenom><initiale>A.</initiale></bauteur><bauteur><nom>Kashiwara</nom><prenom>Masaki</prenom><initiale>M.</initiale></bauteur>
<title>A microlocal approach to the enhanced Fourier-Sato transform in dimension one</title><TeXtitle>A microlocal approach to the enhanced Fourier-Sato transform in dimension one</TeXtitle><journal>Adv. Math.</journal>
<volume>339</volume>
<year>2018</year>
<pagedeb>1</pagedeb><pagefin>59</pagefin><pages>1–59</pages>
<doi>10.1016/j.aim.2018.09.022</doi></bib_entry><bib_entry user-id='DK19' id='bid6' doctype='article'><reference>3</reference>
<bauteur><nom>D’Agnolo</nom><prenom>Andrea</prenom><initiale>A.</initiale></bauteur><bauteur><nom>Kashiwara</nom><prenom>Masaki</prenom><initiale>M.</initiale></bauteur>
<title>Enhanced perversities</title><TeXtitle>Enhanced perversities</TeXtitle><journal>J. reine angew. Math.</journal>
<volume>751</volume>
<year>2019</year>
<pagedeb>185</pagedeb><pagefin>241</pagefin><pages>185–241</pages>
<doi>10.1515/crelle-2016-0062</doi></bib_entry><bib_entry user-id='DK19nu' id='bid12' doctype='misc'><reference>4</reference>
<bauteur><nom>D’Agnolo</nom><prenom>Andrea</prenom><initiale>A.</initiale></bauteur><bauteur><nom>Kashiwara</nom><prenom>Masaki</prenom><initiale>M.</initiale></bauteur>
<title>Enhanced specialization and microlocalization</title><TeXtitle>Enhanced specialization and microlocalization</TeXtitle><year>2019</year>
<eprint-id>1908.01276</eprint-id></bib_entry><bib_entry user-id='GS14' id='bid5' doctype='incollection'><reference>5</reference>
<bauteur><nom>Guillermou</nom><prenom>Stéphane</prenom><initiale>S.</initiale></bauteur><bauteur><nom>Schapira</nom><prenom>Pierre</prenom><initiale>P.</initiale></bauteur>
<title>Microlocal theory of sheaves and Tamarkin’s non displaceability theorem</title><TeXtitle>Microlocal theory of sheaves and Tamarkin’s non displaceability theorem</TeXtitle><booktitle>Homological mirror symmetry and tropical geometry</booktitle><TeXbooktitle>Homological mirror symmetry and tropical geometry</TeXbooktitle><series>Lect. Notes Unione Mat. Ital.</series>
<volume>15</volume>
<publisher>Springer, Cham</publisher>
<year>2014</year>
<pagedeb>43</pagedeb><pagefin>85</pagefin><pages>43–85</pages>
<doi>10.1007/978-3-319-06514-4_3</doi></bib_entry><bib_entry user-id='IT20' id='bid11' doctype='article'><reference>6</reference>
<bauteur><nom>Ito</nom><prenom>Yohei</prenom><initiale>Y.</initiale></bauteur><bauteur><nom>Takeuchi</nom><prenom>Kiyoshi</prenom><initiale>K.</initiale></bauteur>
<title>On irregularities of Fourier transforms of regular holonomic <formula type='inline'><math xmlns='http://www.w3.org/1998/Math/MathML'><mi>&Dscr;</mi></math></formula>-modules</title><TeXtitle>On irregularities of Fourier transforms of regular holonomic <texmath textype='inline' type='inline'>\mathcal{D}</texmath>-modules</TeXtitle><journal>Adv. Math.</journal>
<volume>366</volume>
<year>2020</year>
<pages>107093, 62</pages>
<doi>10.1016/j.aim.2020.107093</doi></bib_entry><bib_entry user-id='Kas16' id='bid9' doctype='article'><reference>8</reference>
<bauteur><nom>Kashiwara</nom><prenom>Masaki</prenom><initiale>M.</initiale></bauteur>
<title>Riemann-Hilbert correspondence for irregular holonomic <formula type='inline'><math xmlns='http://www.w3.org/1998/Math/MathML'><mi>&Dscr;</mi></math></formula>-modules</title><TeXtitle>Riemann-Hilbert correspondence for irregular holonomic <texmath textype='inline' type='inline'>\mathcal{D}</texmath>-modules</TeXtitle><journal>Japan. J. Math.</journal>
<number>1</number>
<volume>11</volume>
<year>2016</year>
<pagedeb>113</pagedeb><pagefin>149</pagefin><pages>113–149</pages>
<doi>10.1007/s11537-016-1564-7</doi></bib_entry><bib_entry user-id='Kas84' id='bid0' doctype='article'><reference>7</reference>
<bauteur><nom>Kashiwara</nom><prenom>Masaki</prenom><initiale>M.</initiale></bauteur>
<title>The Riemann-Hilbert problem for holonomic systems</title><TeXtitle>The Riemann-Hilbert problem for holonomic systems</TeXtitle><journal>Publ. RIMS, Kyoto Univ.</journal>
<number>2</number>
<volume>20</volume>
<year>1984</year>
<pagedeb>319</pagedeb><pagefin>365</pagefin><pages>319-365</pages>
<doi>10.2977/prims/1195181610</doi></bib_entry><bib_entry user-id='KS01' id='bid7' doctype='book'><reference>10</reference>
<bauteur><nom>Kashiwara</nom><prenom>Masaki</prenom><initiale>M.</initiale></bauteur><bauteur><nom>Schapira</nom><prenom>Pierre</prenom><initiale>P.</initiale></bauteur>
<title>Ind-sheaves</title><TeXtitle>Ind-sheaves</TeXtitle><series>Astérisque</series>
<volume>271</volume>
<publisher>Société Mathématique de France</publisher>
<address>Paris</address>
<year>2001</year>
</bib_entry><bib_entry user-id='KS03' id='bid10' doctype='incollection'><reference>11</reference>
<bauteur><nom>Kashiwara</nom><prenom>Masaki</prenom><initiale>M.</initiale></bauteur><bauteur><nom>Schapira</nom><prenom>Pierre</prenom><initiale>P.</initiale></bauteur>
<title>Microlocal study of ind-sheaves. I. Micro-support and regularity</title><TeXtitle>Microlocal study of ind-sheaves. I. Micro-support and regularity</TeXtitle><booktitle>Autour de l’analyse microlocale</booktitle><TeXbooktitle>Autour de l’analyse microlocale</TeXbooktitle><series>Astérisque</series>
<volume>284</volume>
<publisher>Société Mathématique de France</publisher>
<address>Paris</address>
<year>2003</year>
<pagedeb>143</pagedeb><pagefin>164</pagefin><pages>143–164</pages>
</bib_entry><bib_entry user-id='KS16' id='bid8' doctype='book'><reference>12</reference>
<bauteur><nom>Kashiwara</nom><prenom>Masaki</prenom><initiale>M.</initiale></bauteur><bauteur><nom>Schapira</nom><prenom>Pierre</prenom><initiale>P.</initiale></bauteur>
<title>Regular and irregular holonomic D-modules</title><TeXtitle>Regular and irregular holonomic D-modules</TeXtitle><series>London Math. Soc. Lect. Note Series</series>
<volume>433</volume>
<publisher>Cambridge University Press</publisher>
<address>Cambridge</address>
<year>2016</year>
<doi>10.1017/CBO9781316675625</doi></bib_entry><bib_entry user-id='KS90' id='bid3' doctype='book'><reference>9</reference>
<bauteur><nom>Kashiwara</nom><prenom>Masaki</prenom><initiale>M.</initiale></bauteur><bauteur><nom>Schapira</nom><prenom>Pierre</prenom><initiale>P.</initiale></bauteur>
<title>Sheaves on manifolds</title><TeXtitle>Sheaves on manifolds</TeXtitle><series>Grundlehren Math. Wiss.</series>
<volume>292</volume>
<publisher>Springer-Verlag</publisher>
<address>Berlin, Heidelberg</address>
<year>1990</year>
</bib_entry><bib_entry user-id='Tam08' id='bid4' doctype='incollection'><reference>13</reference>
<bauteur><nom>Tamarkin</nom><prenom>Dmitry</prenom><initiale>D.</initiale></bauteur>
<title>Microlocal condition for non-displaceability</title><TeXtitle>Microlocal condition for non-displaceability</TeXtitle><booktitle>Algebraic and analytic microlocal analysis</booktitle><TeXbooktitle>Algebraic and analytic microlocal analysis</TeXbooktitle><series>Springer Proc. Math. Stat.</series>
<volume>269</volume>
<publisher>Springer, Cham</publisher>
<year>2018</year>
<pagedeb>99</pagedeb><pagefin>223</pagefin><pages>99–223</pages>
<doi>10.1007/978-3-030-01588-6_3</doi></bib_entry></biblio>
</article>
