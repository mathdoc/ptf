\select@language {english}
\select@language {english}
\select@language {french}
\select@language {english}
\contentsline {section}{\tocsection {}{1}{Introduction}}{58}{section.2}
\contentsline {subsection}{\tocsubsection {}{1.1}{Main results}}{58}{subsection.3}
\contentsline {subsection}{\tocsubsection {}{1.2}{Structure of the article}}{65}{subsection.40}
\contentsline {subsection}{\tocsubsection {}{1.3}{Naturality of Legendrian invariants}}{66}{subsection.41}
\contentsline {subsection}{\tocsubsection {}{1.4}{Cluster algebras}}{67}{subsection.45}
\contentsline {subsubsection}{\tocsubsubsection {}{}{Acknowledgements}}{68}{section*.47}
\contentsline {section}{\tocsection {}{2}{Generating families}}{69}{section.48}
\contentsline {subsection}{\tocsubsection {}{2.1}{Legendrians as Cerf diagrams}}{69}{subsection.49}
\contentsline {subsection}{\tocsubsection {}{2.2}{Existence of generating families}}{71}{subsection.58}
\contentsline {subsection}{\tocsubsection {}{2.3}{Homotopy lifting property}}{72}{subsection.65}
\contentsline {subsection}{\tocsubsection {}{2.4}{Generating family invariants}}{73}{subsection.69}
\contentsline {subsection}{\tocsubsection {}{2.5}{Beyond trivial bundles}}{75}{subsection.75}
\contentsline {section}{\tocsection {}{3}{Turaev torsion}}{78}{section.89}
\contentsline {subsection}{\tocsubsection {}{3.1}{Torsion from the Morse-theoretic viewpoint}}{78}{subsection.90}
\contentsline {subsection}{\tocsubsection {}{3.2}{Euler structures and Turaev torsion}}{81}{subsection.94}
\contentsline {subsection}{\tocsubsection {}{3.3}{Fibre Turaev torsion}}{84}{subsection.112}
\contentsline {subsection}{\tocsubsection {}{3.4}{Torsion of open manifolds}}{86}{subsection.121}
\contentsline {subsection}{\tocsubsection {}{3.5}{Stability of torsion}}{88}{subsection.129}
\contentsline {subsection}{\tocsubsection {}{3.6}{A Legendrian invariant}}{89}{subsection.134}
\contentsline {section}{\tocsection {}{4}{Mesh Legendrians}}{91}{section.145}
\contentsline {subsection}{\tocsubsection {}{4.1}{Generating families from systems of disks}}{91}{subsection.146}
\contentsline {subsection}{\tocsubsection {}{4.2}{Formal triviality}}{96}{subsection.158}
\contentsline {subsection}{\tocsubsection {}{4.3}{$0$-parametric Morse theory}}{98}{subsection.164}
\contentsline {subsection}{\tocsubsection {}{4.4}{$1$-parametric Morse theory}}{99}{subsection.170}
\contentsline {subsection}{\tocsubsection {}{4.5}{$2$-parametric Morse theory}}{101}{subsection.174}
\contentsline {subsection}{\tocsubsection {}{4.6}{Conclusion of the computation}}{106}{subsection.183}
\contentsline {section}{\tocsection {Appendix}{}{Appendix. Overview of torsion}}{108}{section*.189}
\contentsline {subsection}{\tocsubsection {}{A.1}{Reidemeister and Turaev torsion}}{108}{subsection.191}
\contentsline {subsection}{\tocsubsection {}{A.2}{Higher Reidemeister torsion}}{109}{subsection.192}
\contentsline {subsection}{\tocsubsection {}{A.3}{Pictures for $K_3$}}{110}{subsection.193}
\contentsline {subsection}{\tocsubsection {}{A.4}{Pseudo-isotopy theory}}{113}{subsection.196}
\contentsline {subsection}{\tocsubsection {}{A.5}{Floer-theoretic torsions}}{115}{subsection.199}
\contentsline {section}{\tocsection {Appendix}{}{References}}{116}{section*.200}
