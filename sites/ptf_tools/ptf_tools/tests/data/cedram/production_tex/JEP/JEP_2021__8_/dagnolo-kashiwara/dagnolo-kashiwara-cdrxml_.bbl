% reading source dagnolo-kashiwara.bib
%
\citation{DK16}{cite:DK16}{bid1}{year}{article}
\bauthors{\bpers[Andrea]{A.}{}{D'Agnolo}{}\bpers[Masaki]{M.}{}{Kashiwara}{}}
\cititem{btitle}{Riemann-{H}ilbert correspondence for holonomic {D}-modules}
\cititem{bjournal}{Publ. Math. Inst. Hautes {\'E}tudes Sci.}
\cititem{bvolume}{123}
\cititem{byear}{2016}
\cititem{bpages}{69--197}
\cititem{bdoi}{10.1007/s10240-015-0076-y}
\endcitation
%
\citation{DK18}{cite:DK18}{bid2}{year}{article}
\bauthors{\bpers[Andrea]{A.}{}{D'Agnolo}{}\bpers[Masaki]{M.}{}{Kashiwara}{}}
\cititem{btitle}{A microlocal approach to the enhanced {F}ourier-{S}ato transform in dimension one}
\cititem{bjournal}{Adv. Math.}
\cititem{bvolume}{339}
\cititem{byear}{2018}
\cititem{bpages}{1--59}
\cititem{bdoi}{10.1016/j.aim.2018.09.022}
\endcitation
%
\citation{DK19a}{cite:DK19}{bid6}{year}{article}
\bauthors{\bpers[Andrea]{A.}{}{D'Agnolo}{}\bpers[Masaki]{M.}{}{Kashiwara}{}}
\cititem{btitle}{Enhanced perversities}
\cititem{bjournal}{J.~reine angew. Math.}
\cititem{bvolume}{751}
\cititem{byear}{2019}
\cititem{bpages}{185--241}
\cititem{bdoi}{10.1515/crelle-2016-0062}
\endcitation
%
\citation{DK19b}{cite:DK19nu}{bid12}{year}{misc}
\bauthors{\bpers[Andrea]{A.}{}{D'Agnolo}{}\bpers[Masaki]{M.}{}{Kashiwara}{}}
\cititem{btitle}{Enhanced specialization and microlocalization}
\cititem{byear}{2019}
\cititem{eprint}{1908.01276}
\endcitation
%
\citation{GS14}{cite:GS14}{bid5}{year}{incollection}
\bauthors{\bpers[St{\'e}phane]{S.}{}{Guillermou}{}\bpers[Pierre]{P.}{}{Schapira}{}}
\cititem{btitle}{Microlocal theory of sheaves and {T}amarkin's non displaceability theorem}
\cititem{bbooktitle}{Homological mirror symmetry and tropical geometry}
\cititem{bseries}{Lect.\ Notes Unione Mat. Ital.}
\cititem{bvolume}{15}
\cititem{bpublisher}{Springer, Cham}
\cititem{byear}{2014}
\cititem{bpages}{43--85}
\cititem{bdoi}{10.1007/978-3-319-06514-4\_3}
\endcitation
%
\citation{IT20}{cite:IT20}{bid11}{year}{article}
\bauthors{\bpers[Yohei]{Y.}{}{Ito}{}\bpers[Kiyoshi]{K.}{}{Takeuchi}{}}
\cititem{btitle}{On irregularities of {F}ourier transforms of regular holonomic {$\mathcal{D}$}-modules}
\cititem{bjournal}{Adv. Math.}
\cititem{bvolume}{366}
\cititem{byear}{2020}
\cititem{bpages}{107093, 62}
\cititem{bdoi}{10.1016/j.aim.2020.107093}
\endcitation
%
\citation{Kas16}{cite:Kas16}{bid9}{year}{article}
\bauthors{\bpers[Masaki]{M.}{}{Kashiwara}{}}
\cititem{btitle}{Riemann-{H}ilbert correspondence for irregular holonomic {$\mathcal{D}$}-modules}
\cititem{bjournal}{Japan.~J. Math.}
\cititem{bnumber}{1}
\cititem{bvolume}{11}
\cititem{byear}{2016}
\cititem{bpages}{113--149}
\cititem{bdoi}{10.1007/s11537-016-1564-7}
\endcitation
%
\citation{Kas84}{cite:Kas84}{bid0}{year}{article}
\bauthors{\bpers[Masaki]{M.}{}{Kashiwara}{}}
\cititem{btitle}{The {R}iemann-{H}ilbert problem for holonomic systems}
\cititem{bjournal}{Publ. RIMS, Kyoto Univ.}
\cititem{bnumber}{2}
\cititem{bvolume}{20}
\cititem{byear}{1984}
\cititem{bpages}{319-365}
\cititem{bdoi}{10.2977/prims/1195181610}
\endcitation
%
\citation{KS01}{cite:KS01}{bid7}{year}{book}
\bauthors{\bpers[Masaki]{M.}{}{Kashiwara}{}\bpers[Pierre]{P.}{}{Schapira}{}}
\cititem{btitle}{Ind-sheaves}
\cititem{bseries}{Ast{\'e}risque}
\cititem{bvolume}{271}
\cititem{bpublisher}{Soci{\'e}t{\'e} Math{\'e}matique de France}
\cititem{baddress}{Paris}
\cititem{byear}{2001}
\endcitation
%
\citation{KS03}{cite:KS03}{bid10}{year}{incollection}
\bauthors{\bpers[Masaki]{M.}{}{Kashiwara}{}\bpers[Pierre]{P.}{}{Schapira}{}}
\cititem{btitle}{Microlocal study of ind-sheaves.~{I}. {M}icro-support and regularity}
\cititem{bbooktitle}{Autour de l'analyse microlocale}
\cititem{bseries}{Ast{\'e}risque}
\cititem{bvolume}{284}
\cititem{bpublisher}{Soci{\'e}t{\'e} Math{\'e}matique de France}
\cititem{baddress}{Paris}
\cititem{byear}{2003}
\cititem{bpages}{143--164}
\endcitation
%
\citation{KS16}{cite:KS16}{bid8}{year}{book}
\bauthors{\bpers[Masaki]{M.}{}{Kashiwara}{}\bpers[Pierre]{P.}{}{Schapira}{}}
\cititem{btitle}{Regular and irregular holonomic {D}-modules}
\cititem{bseries}{London Math.\ Soc.\ Lect.\ Note Series}
\cititem{bvolume}{433}
\cititem{bpublisher}{Cambridge University Press}
\cititem{baddress}{Cambridge}
\cititem{byear}{2016}
\cititem{bdoi}{10.1017/CBO9781316675625}
\endcitation
%
\citation{KS90}{cite:KS90}{bid3}{year}{book}
\bauthors{\bpers[Masaki]{M.}{}{Kashiwara}{}\bpers[Pierre]{P.}{}{Schapira}{}}
\cititem{btitle}{Sheaves on manifolds}
\cititem{bseries}{Grundlehren Math. Wiss.}
\cititem{bvolume}{292}
\cititem{bpublisher}{Springer-Verlag}
\cititem{baddress}{Berlin, Heidelberg}
\cititem{byear}{1990}
\endcitation
%
\citation{Tam18}{cite:Tam08}{bid4}{year}{incollection}
\bauthors{\bpers[Dmitry]{D.}{}{Tamarkin}{}}
\cititem{btitle}{Microlocal condition for non-displaceability}
\cititem{bbooktitle}{Algebraic and analytic microlocal analysis}
\cititem{bseries}{Springer Proc. Math. Stat.}
\cititem{bvolume}{269}
\cititem{bpublisher}{Springer, Cham}
\cititem{byear}{2018}
\cititem{bpages}{99--223}
\cititem{bdoi}{10.1007/978-3-030-01588-6\_3}
\endcitation
