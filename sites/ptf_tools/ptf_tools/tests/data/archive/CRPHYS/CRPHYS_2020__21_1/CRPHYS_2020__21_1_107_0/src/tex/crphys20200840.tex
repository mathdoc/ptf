\makeatletter
\@ifundefined{HCode}
{
\documentclass[CRPHYS,Thematic,Unicode,screen]{cedram}
\newenvironment{noXML}{}{}
\newenvironment{Table}{\begin{table}}{\end{table}}
\def\thead{\noalign{\relax}\hline}
\def\endthead{\noalign{\relax}\hline}
\def\tabnote#1{\vskip4pt\parbox{.8\linewidth}{#1}}
\def\tsup#1{$^{{#1}}$}
\def\tsub#1{$_{{#1}}$}
\RequirePackage{etoolbox}
\def\jobid{crphys20200840}
\def\xbdot{\boldsymbol{\cdot}}
%\graphicspath{{/tmp/\jobid_figs/web/}}
\graphicspath{{./figures/}}
\newcounter{runlevel}
\def\rme{\mathrm{e}}
\let\MakeYrStrItalic\relax
\def\refinput#1{}
\def\back#1{}
\def\sfrac#1#2{{#1}/{#2}}
\def\0{\phantom{0}}
\csdef{Seqnsplit}{\\}
\DOI{10.5802/crphys.12}
\datereceived{2020-05-25}
\dateaccepted{2020-06-15}
}
{\documentclass[crphys]{article}
\def\xbdot{\cdot}
\def\CDRdoi{10.5802/crphys.12}
\let\newline\break
\def\sfrac#1#2{{#1}/{#2}}
\def\rme{\mathrm{e}}
\makeatletter
}
\makeatother

\usepackage{upgreek}
\begin{DefTralics}
	\let\uppi\pi
	\def\etal{et al.}
\end{DefTralics}

\begin{document}

\begin{noXML}

% \TopicEF{Thematic Issue}{Fascicule th\'ematique}

\title{Rare kaon decays}

\alttitle{Les d\'esint\'egrations rares des kaons}

\author{\firstname{Augusto} \lastname{Ceccucci}\IsCorresp}
\address{EP Department, CERN, 1211 Geneva 23, CH, Switzerland}
\email[A. Ceccucci]{augusto.ceccucci@cern.ch}

\author{\firstname{Cristina} \lastname{Lazzeroni}}
\address{School of Physics and Astronomy, University of Birmingham, UK}
\email[C. Lazzeroni]{cristina.lazzeroni@cern.ch}

\keywords{Kaon, Decay, CP symmetry violation, Rare processes, CKM
tests, New physics searches}

\altkeywords{Kaon, D\'esint\'egration, Violation de la sym\'etrie CP,
Processus rares, Tests CKM, Recherches en Nouvelle Physique}

\begin{abstract} 
The article reviews the status of rare K decay physics. The explored
area covers a broad program, ranging from precise tests of flavour
theory and \textit{CP}-violation to the searches for explicit
violations of the standard model and the study of strong interactions
at low energies.  
\end{abstract}

\begin{altabstract} 
Cet article discute l'\'etat de l'art de la physique des 
d\'esint\'egrations rares des kaons. Tests de pr\'ecision des
transitions de saveurs de quarks et de la brisure de la sym\'etrie
\textit{CP}, recherches explicites de brisure  des lois de conservation
du mod\`ele standard et \'etude des interactions fortes \`a basse
\'energie forment le programme de cette revue. 
\end{altabstract}

\maketitle

\end{noXML}

\newcommand{\Real}{{\mathcal{R}e}}
\newcommand{\Imag}{{\mathcal{I}m}}

\section{Introduction}
The discovery of kaons and the study of their properties are seminal
for the building of the Standard Model (SM). Strangeness, flavour
mixing, \textit{CP}-Violation, lack of flavour changing neutral
currents (FCNC), GIM mechanism~\cite{Glashow:1970gm}, all these pillars
of the SM trace their origin in strange particles. We are still lacking
a theory explaining why fundamental fermions appear in three families
and many outstanding questions such as the baryon asymmetry of the
Universe or the puzzle of dark matter remain unanswered. The interplay
of symmetry violation, such as \textit{CP},  and the mixing of quarks
remains one of the most active areas of experimental research in high
energy physics. To summarise the overarching strategy of flavour
physics, one can say that in the SM: 

\begin{enumerate}[(i)]
\item  The Higgs sector is the source of flavour violation. It is when
the Higgs acquires a vacuum expectation value that the quarks get a
physical mass and the charged-current $W^{\pm}$ interations couple to
the quark interaction eigenstates with couplings given by the
Cabibbo-Kobayashi and Maskawa (CKM) matrix~\cite{Cabibbo,KM};

\item So far all manifestations of \textit{CP} violation and quark
mixing are compatible with the single complex phase of the CKM matrix;

\item  Further experimental investment in the field of flavour physics
is justified by the high energy scales addressed by these studies: in
many cases they exceed the direct reach achievable at colliders. 
\end{enumerate} 

The study of kaons are not only complementary to those performed with
$B$ mesons but also mutually reinforcing: we need to over-constrain the
CKM matrix with precise determinations of its parameters extracted
independently from different quark systems. A discrepancy between the
determinations of the parameters obtained from $K$ and $B$ would signal
physics beyond the SM (BSM). 

What makes rare $K$ decays special is the unique combination of precise
theoretical predictions and the strong suppression of the SM
contributions. These two factors make the detection of BSM contribution
relatively plausible.  The SM contribution to rare kaon decays are tiny
and well predicted. In some cases they are not only absent at tree
level but also suppressed at loop levels. Final states with a neutrino
- antineutrino pair are the least affected by long distance
contributions.  

It should be emphasised that to explore the flavour structure, it is
not enough to check the unitarity of CKM, which is such almost by
construction: one has to study genuine~\cite{Barbieri:2001mw}
electro-weak FCNC processes. Among these, the $K \to \uppi \nu
\bar{\nu}$ ones described in the next section are a particularly
interesting combination of theoretical cleanliness and experimental
challenge. 

\section{$K \to \uppi \nu \bar{\nu}$: gold-plated decays to test the CKM
paradigm beyond the SM} 

The two decay modes $K^+ \to \uppi^+ \nu \bar{\nu}$ and $K^0_L \to
\uppi^0 \nu \bar{\nu}$ can be used to quantitatively check the
description of quark mixing and \textit{CP}-violation  independently
from information extracted from the $B$ system. To see why this is the
case it is convenient to express the formulas for the branching
fractions in terms of contributions from the different loop functions.
Following~\cite{Buras:2015qea} the SM predictions can be written as:  
$$ 
\mathcal{B}(K^+ \to \uppi^+ \nu \bar{\nu})=
\kappa_+(1+\Delta_{\mathrm{EM}})\left[ \left(\frac{\Imag\,
\lambda_t}{\lambda^5} X(x_t)\right)^2  + \left(\frac{\Real\,
\lambda_c}{\lambda}P_c(X)+\frac{\Real\,
\lambda_t}{\lambda^5}X(x_t)\right)^2  \right],
$$
with $\Delta_{\mathrm{EM}}=-0.003$ the electromagnetic radiative
corrections, $x_t= m_t^2/M_W^2$,  $\lambda = |V_{us}|$,
$\lambda_i=V^*_{is}V_{id}$ the relevant combinations of CKM matrix
elements, $X$ and  $P_c(X)$ the loop functions for the top and charm
quark respectively, and 
$$
\kappa_+ = (5.173 \pm 0.025)\times
10^{-11}\left[\frac{\lambda}{0.225}\right]^8 
$$
the parameter encoding the relevant hadronic matrix elements extracted
from a suitable combination of semi-leptonic rates. As the formula
shows, $\mathcal{B}(K^+ \to \uppi^+ \nu \bar{\nu})$ depends on the sum
of the square of the imaginary part of the top loop (\textit{CP}
violating)  and the square of the sum of the charm contribution and the
real part of the top loop. 

The corresponding formulas for $K^0_L$ are: 
$$
\mathcal{B}(K^0_L \to \uppi^0 \nu \bar{\nu})= \kappa_L
\left(\frac{\Imag \lambda_t}{\lambda^5} X(x_t)\right)^2,
$$
and
$$
\kappa_L = (2.231 \pm 0.013)\times
10^{-10}\left[\frac{\lambda}{0.225}\right]^8. 
$$

The $\mathcal{B}(K^0_L \to \uppi^0 \nu \bar{\nu})$ depends only on the
square of the imaginary part of the top loop which is
\textit{CP}-violating. The charm contributions drop out because $K^0_L$
is mostly an odd linear combination of $K^0$ and $\bar{K}^0$. This
makes the theoretical prediction for the $K^0_L$ rate even cleaner than
for the $K^+$. A measurement of the $K^0_L \to \uppi^0 \nu \bar{\nu}$
mode would amount to the measurement of the square of the
\textit{CP}-violating parameter $\eta$ in the Wolfenstein
parametrisation~\cite{Wolfenstein:1983yz} and directly related to the
Jarlskog invariant $J$~\cite{Jarlskog:1985ht}, the unique measure of
the magnitude of \textit{CP}-Violation in the SM. 

Inserting the numerical factors and making the dependence to the
relevant CKM parameters explicit, one obtains: 
\begin{eqnarray*}
&\displaystyle \mathcal{B}(K^+ \to \uppi^+ \nu \bar{\nu})=(8.39 \pm
0.30) \times 10^{-11} \left[ \frac{|V_{cb}|}{40.7 \times
10^{-3}}\right]^{2.8}\left[ \frac{\gamma}{73.2^\circ}\right]^{0.74},
\\
&\displaystyle \mathcal{B}(K^0_L \to \uppi^0 \nu \bar{\nu})=(3.36 \pm
0.05) \times 10^{-11} \left[ \frac{|V_{ub}|}{3.88 \times
10^{-3}}\right]^{2}\left[ \frac{|V_{cb}|}{40.7 \times
10^{-3}}\right]^{2} \left[\frac{\sin \gamma}{\sin
73.2^\circ}\right]^{2}.
\end{eqnarray*}

In the above formulas the explicit numerical errors are the theoretical
ones originating from QCD and electroweak uncertainties. They amount to
3.6\% and 1.5\% for the charged and the neutral mode respectively. 

Taking $|V_{cb}|_{\mathrm{avg}}=(40.7 \pm 1.4 ) \times 10^{-3}$,
$|V_{ub}|_{\mathrm{avg}}=(3.88 \pm 0.29 ) \times 10^{-3}$ and $\gamma =
(73.2^{+6.3}_{-7.0})^\circ$~\cite{pdg}, one finds: 
\begin{eqnarray*}
&\displaystyle \mathcal{B}(K^+ \to \uppi^+ \nu \bar{\nu})=(8.4 \pm 1.0)
\times 10^{-11}, \\
&\displaystyle \mathcal{B}(K^0_L \to \uppi^0 \nu \bar{\nu})=(3.4 \pm
0.6) \times 10^{-11}.
\end{eqnarray*}

The predictions are currently dominated by the parametric uncertainty
that will plausibly be reduced by new measurements of $|V_{ub}|$,
$|V_{cb}|$ and $\gamma$ by LHCb and Belle II.  

With the discovery of the Higgs boson, the particle content of the SM
is complete but we know that the SM itself cannot be the full story.
There are many extensions of the SM where one could expect sizeable
contributions, among which we can mention: Warped extra
dimensions~\cite{Blanke:2008yr}; MSSM
analyses~\cite{Tanimoto:2016yfy,Blazek:2014qda,Isidori:2006qy};
Simplified Z and Z$'$ models~\cite{Buras:2015yca}, Littlest Higgs with
T-Parity~\cite{Blanke:2015wba}, Lepton Flavour Universality Violation
models~\cite{Bordone:2017lsy} and Lepto-quarks~\cite{Fajfer:2018bfj}.
As an example of complementarity between $K \to \uppi \nu \bar{\nu}$ 
and $B$-meson physics one can mention the case of $B_s \to \upmu^+
\upmu^-$: the $B$ decay is sensitive to possible new pseudoscalar (e.g.
charged Higgs) interactions while the $K$ rare decay is sensitive to
possible new vector ones such as a $Z^\prime$. 

The SM theoretical precision is waiting to be matched experimentally.
The experiments are difficult because the three-body final state lacks
a significant signature and the $\nu \bar{\nu}$ pair cannot be
detected. A long series of decay-at-rest searches for $K^+ \to \uppi^+
\nu \bar{\nu}$ have culminated with the final results of the E878/E949
AGS experiments which have found strong evidence for the decay and
quoted as final result~\cite{Artamonov:2008qb}: 
$$
\mathcal{B}(K^+ \to \uppi^+ \nu
\bar{\nu})_{\mathrm{exp}}=(17.3^{+11.5}_{-10.5} ) \times 10^{-11}.
$$

Although the mean value is about twice the SM prediction, the result is
still consistent with the SM prediction because of the large
statistical error. In addition to the purity of the separated kaon
bean, the advantages of the stopped kaon technique include good
kinematic constraint to kill backgrounds from two body kaon decays and
the possibility to enforce good charged particle identification
following the full $K^+ \to \uppi^+ \to \upmu^+ \to \rme^+$ decay chain.
Limitations of the technique are the small acceptance once acceptable
levels of muon and photon rejection levels are achieved and the
presence of the material of the stopping target which leads to
$\uppi^+$ scattering and loss of energy. 

An alternative method to study the decay is to let the kaon decay in
flight, as done by the NA62 experiment at CERN~\cite{na62td}. High
momentum (75~GeV/$c$), positively charged,  secondary beams from the CERN
SPS are well suited to provide a high flux of kaons and good immunity
to backgrounds. A drawback is that the separation at beam level of
kaons from the pion and proton components is practically impossible and
because of this the main difficulty of the experiment is to track each
particle of the beam before it enters the decay region. Only about  6\%
of the beam particles are kaons and of those only about 10\% are
usefully decaying in the fiducial volume. So for each useful $K$ decay
NA62 has to track almost 200 particles. To be able to accumulate a
sufficient number of decay the beam rate is very high
($\simeq$750~MHz). The SPS beam is delivered by a slow extraction to
minimise the instantaneous beam intensity. The beam is not bunched as
in collider experiments because otherwise the kaon decays occuring in
the same bunch would veto each other. Nevertheless,  even employing a
slow extraction and an overall duty cycle of about 20\%, the
instantaneous beam intensity is so high that accidental activity in the
beam tracker leads to the wrong association between a beam track
(assumed to be the kaon) and the decay product. To avoid this problem
and to correctly associate the kaon minimising mistags, NA62 has developed
a novel Si pixel detector tracking
detector~\cite{AglieriRinella:2019eri} dubbed Gigatracker (GTK). The
time resolution achieved with three GTK stations is as good as 65~ps.
First results published by NA62 indicate that the novel in-flight
technique is viable for $K^+ \rightarrow \uppi^+ \nu
\bar{\nu}$~\cite{CortinaGil:2018fkc}.

Based on three candidate events (with an expected background of
$1.65\pm0.31$), collected from the 2016--2017 data sample, the NA62
Collaboration reported~\cite{KAON2019}
$$
\mathcal{B}(K^+ \to \uppi^+ \nu
\bar{\nu})_{\mathrm{exp}}=(4.7^{+7.2}_{-4.7} ) \times 10^{-11}
$$
which, if interpreted as background leads to a 90\% CL upper limit
of~\cite{KAON2019}: 
$$
\mathcal{B}(K^+ \to \uppi^+ \nu \bar{\nu}) < 18.5\times 10^{-11}.
$$ 
The NA62 result based on 2017 data excludes in practice the BNL mean
value reported above. This is the first significant result obtained
with the novel in-flight technique developed by
NA62~\cite{NA62:2017rwk}. NA62 has collected more data in 2018 and
results based on this data sample will significantly improve the
measurement. The overall kaon flux accumulated by NA62  so far
(${\approx}6 \times 10^{12}$ kaons) corresponds to about 50  SM $K^+ \to
\uppi^+ \nu \bar{\nu}$ events for a nominal overall acceptance of 10\%.
The acceptance of the analysis on which the result presented above was
obtained is of the order of $\approx$1\% so lot of effort is underway
to increase it towards the nominal value. To achieve the goal of 100 SM
events NA62 will resume data taking after the CERN Long Shutdown 2
(LS2) in 2021.  So far one extra year has been granted. Data taking may
be further extended depending on the progress of the analysis. The
possibility to further improve the measurement towards a 5\% accuracy
for a SM signal is being studied within NA62. Assuming that the
infrastructure could be upgraded to take a factor of four more proton
intensity, the detector would need to be upgraded in order to: 

\begin{enumerate}[(i)] 
\item{Improve the time resolution of the beam tracker to less than
50~ps/station} 

The experiment is not limited by the number of protons deliverable by
the SPS, but rather by the time resolution required to resolve the
tracks in the beam tracker and to correctly match the kaon one to the
pion reconstructed in the downstream tracker.  As stated above, the
NA62 Gigatracker has a time resolution of 120 ps per station, or 65 ps
for the three stations combined. The NA62 GTK success has inspired
several new R\&D to develop Si pixel detectors with even better timing
in the view of, for example, of the Upgrade 2 of LHCb~\cite{LHCbU2}. A
significant improvement of the time resolution would enable NA62 to
operate efficiently with increased beam intensity.  

\item{Reduce material budget of the trackers} 

While any amount of material in the beam leads to unwanted scattering
and interactions, for this experiment it is also important to minimise
the thickness of the main pion tracker because particle scattered on
detector material can mimic the weak signal signature. The NA62 straw
tracker has a thickness of 1.2\% of a radiation length. It is made of
straws of 9.8~mm diameter. It is operated in the vacuum tank to
minimise scattering. The main source of material is the plastic wall of
the straw which amounts to 36~$\upmu$m. Reducing the radius of the
straw to approx. 5~mm opens the possibility to employ much thinner
walls and reduce the material budget by about a factor of two. In
addition, a shorter drift time will improve the high rate capability of
the detector. 

\item{Faster EM calorimetry}

One of the essential aspects of NA62 is the capability to detect
photons with high efficiency. Increasing considerably the beam
intensity will require faster veto counters in order to avoid large
signal losses due to random veto. 
\end{enumerate} 

While the experimental situation for $K^+ \to \uppi^+ \nu \bar{\nu}$
shows that we have two independent experimental techniques able to
reach SM sensitivities and beyond, the situation for the neutral mode
is more complex. Progress has been hampered by the lack of experimental
signature, as no redundancy is available once the $\uppi^0$ mass is
used as constraint to define the decay vertex. 

On the experimental front for the neutral mode, the KOTO experiment at
JPARC has published the 90\% CL limit~\cite{Ahn:2018mvc}: 
$$
\mathcal{B}(K^0_L \to \uppi^0 \nu \bar{\nu}) < 3\times 10^{-9}. 
$$ 

KOTO builds on the experience of the E391a predecessor experiment which
was performed at KEK. It is based on the technique of letting a very
well collimated ``pencil'' beam enter the decay region surrounded by high
performance photon vetoes. By vetoing extra photons and applying a
transverse momentum cut (150~MeV/$c$) to eliminate residual $\Lambda 
\to n \uppi^0$ decays KOTO plans to reach SM sensitivities by the mid
of the next decade. The first step would be to improve the model
independent 90\% CL Grossman-Nir limit~\cite{Grossman:1997sk}, which
has been recently updated incorporating the latest NA62 result: 
$$
\mathcal{B}(K^0_L \to \uppi^0 \nu \bar{\nu})_{\mathrm{Grossman-Nir}} <
8.14 \times 10^{-10}.
$$

KOTO is starting to consider seriously a new experiment to reach 100 SM
events \mbox{(KOTO Step-2)}~\cite{KOTO2}. The possibility to explore the
neutral decay mode at CERN once NA62 is completed has been explored in
the framework of the European particle physics strategy upgrade. The
experiment under study (KLEVER)~\cite{Ambrosino:2019qvz} is based on
the same technique employed by KOTO but at much higher kaon energies.
Higher kaon energies are expected to simplify the task of rejecting the
photons from $K^0_L \to \uppi^0 \uppi^0$ which is the dominant source
of background from kaon decays. One should notice that with respect to
the charged mode, the  two-pion branching ratio is
\textit{CP}-violating and therefore suppressed by a factor of about
200. Is this fact that makes the approach to study $K^0_L \to \uppi^0
\uppi^0$ at all thinkable.  KLEVER expects to collect 60 SM events
within five years of data taking after the CERN Long Shutdown 3 (from
2026 onward).   KOTO and KLEVER are holding joint meetings to address
issues of mutual interest. 

In conclusion, what make the case to continue the study of these rare
decays compelling, is that sensitivity beyond SM is there for most of
the proposed extensions.  Together with the study of muon rare decays
and searches for electric dipole moments of elementary particles, rare 
kaon decays like $K^+ \to \uppi^+ \nu \bar{\nu}$ and $K^0_L\to \uppi^0
\nu \bar{\nu}$ offer a genuine window of sensitivity to access high
energy scales thanks to the absence of tree level contributions (CKM
Unitarity), to the absence of long distance contributions ($\nu
\bar{\nu}$ pair in the final state), and the hard GIM suppression at
loop level in~SM.  

\section{Other rare kaon decays}

\subsection{$K_S , K_L \rightarrow \upmu^+ \upmu^-$ and $K_{L,S}
\rightarrow l_1^+ l_1^- l_2^+ l_2^- $}

The study of the rare process $K_L \rightarrow \upmu^+ \upmu^-$ has  played
a crucial role in the understanding of the flavour content and
structure of the standard model (SM) of electroweak interactions. The
$K_S , K_L \rightarrow \upmu^+ \upmu^-$ decays are dominated in the SM by
long-distance two-photons contributions (LD), while the
flavour-changing-neutral-current contributions provide information on
short-distance (SD) dynamics of $\Delta S=1$ transitions~\cite{a1,a2}.
The SM predictions for $K_S, K_L \rightarrow \upmu^+ \upmu^-$
are~\cite{a3}:
\begin{eqnarray*}
&\displaystyle \mathcal{B}(K_S \rightarrow \upmu^+ \upmu^- )_{\mathrm{SM}}
= (5.18\pm1.50_{\mathrm{LD}}\pm0.02_{\mathrm{SD}}) \times10^{-12} 
,\\
&\displaystyle \mathcal{B}(K_L \rightarrow \upmu^+ \upmu^- )_{\mathrm{SM}}
= {}^{(6.85\pm 0.80_{\mathrm{LD}} \pm 0.06_{\mathrm{SD}}) \times 10^{-9}
(+) } _{(8.11\pm 1.49_{\mathrm{LD}} \pm 0.13_{\mathrm{SD}}) \times
10^{-9} (-)} 
\end{eqnarray*}
where the SD contribution to the uncertainty includes the uncertainty
on the CKM parameter ($\bar{\eta}$ and $ \bar{\rho}$ respectively); the
sign ambiguity for the $\mathcal{B}(K_L \rightarrow \upmu^+ \upmu^-
)_{\mathrm{SM}}$  depends on the (destructive or constructive)
interference between short- and long-distance contributions which
itself depends on the sign of an unknown low-energy constant~\cite{a4}.
The amplitude for the $K_L \rightarrow \upmu^+ \upmu^-$ mode receives
contributions from a complex long-distance amplitude generated by
the two-photon exchange and a real amplitude induced by Z-penguin and
box diagrams; the long-distance amplitude has a large absorptive
(imaginary) part that  provides the dominant contribution to the total
rate; the uncertainty on the dispersive part of the two-photon
amplitude is at present the dominant individual source of
error~\cite{a2}. The smallness of the total dispersive amplitude is
well established thanks to precise experimental results on both
$\Gamma(K_L \rightarrow \upmu^+ \upmu^-)$~\cite{pdg} and
$\Gamma(K_L\rightarrow\gamma\gamma)$~\cite{a6}. The current
experimental result for $K_L$ is based on over 6200 candidates from the
BNL E871 Collaboration~\cite{a7}:
$$
\mathcal{B}(K_L \rightarrow \upmu^+ \upmu^- )= (6.84\pm0.11)\times 10^{-9}
$$ 
while for $K_S$ the limit from the 70~s~\cite{a8} has been improved by
upper limits at 90\% CL by the LHCb experiment~\cite{a9,a10}:
$$
\mathcal{B}(K_S  \rightarrow \upmu^+ \upmu^- )< 2.1 \times 10^{-10} .
$$
In the Phase-II upgrade during the High Luminosity LHC era, LHCb is
expecting to collect around $300~\mathrm{fb}^{-1}$. If the trigger
efficiency will be near 1, as can be achieved technically with the
Phase-I full software trigger, LHCb will be able to explore branching
fractions below $10^{-11}$ and could exclude values down towards the
vicinity of the SM prediction~\cite{a11}.

The gap between the SM prediction and the current limit on $K_S , K_L 
\rightarrow \upmu^+ \upmu^- $ leaves room for Beyond the Standard Model
(BSM) contributions. The current limit already place constraints on
leptoquark models~\cite{a12,a13}, supersymmetry~\cite{a14,a15} and
extensions of the SM~\cite{a16}. Experiments using neutral kaon beams
can address the \textit{CP} asymmetry, which is also sensitive to BSM
scenarios. Although both $K_S , K_L \rightarrow \upmu^+ \upmu^-$ are almost
\textit{CP}-conserving decays, direct \textit{CP} violation can be
addressed by interference between $K_L$ and $K_S$ in the kaon beam,
with the interference contribution affecting the $K_S$ SM predictions
at the level of 60\%; besides the unknown sign of the $K_L$ two-photon
amplitude can be determined by a measurement of the
interference~\cite{a16}.

The decays into 4 leptons are also of interest since the sign of the
amplitude of $K_L \rightarrow \gamma \gamma$ affecting the
determination of short-distance contributions to $K_L \rightarrow \upmu^+
\upmu^-$ can be measured by studying $K_L$ and $K_S$ decays in four
leptons~\cite{a18}.   The predicted branching ratios in the SM are of
the order~\cite{a19}:
\begin{eqnarray*}
&\displaystyle \mathcal{B}(K_S \rightarrow \rme^+ \rme^- \rme^+ \rme^- ) \sim
10^{-10}  ,\\
&\displaystyle \mathcal{B}(K_S \rightarrow \upmu^+ \upmu^- \rme^+ \rme^- ) \sim
10^{-11}  ,\\
&\displaystyle \mathcal{B}(K_S \rightarrow \upmu^+ \upmu^- \upmu^+ \upmu^- )
\sim 10^{-14}  ,\\
&\displaystyle \mathcal{B}(K_L \rightarrow \rme^+ \rme^- \rme^+ \rme^- ) \sim
10^{-10}  ,\\
&\displaystyle \mathcal{B}(K_L \rightarrow \upmu^+ \upmu^- \rme^+ \rme^- ) \sim
10^{-11} ,\\
&\displaystyle \mathcal{B}(K_L \rightarrow \upmu^+ \upmu^- \upmu^+ \upmu^- )
\sim 10^{-14} .
\end{eqnarray*}

The $K_S$ modes have never been observed and there are no experimental
limits available in literature. The $K_L$ modes limits are from the
KTeV and NA48 experiments~\cite{a20,a21}.

Recently LHCb has shown prospects for the $K_S$ modes~\cite{a22}.
Future high-intensity kaon experiments, with intensities around 5 times
the most recent ones, could address all the modes with 2 or 4
electrons. 

\subsection{$K_S, K_L \rightarrow \uppi^0 l^+ l^-$} 
In the SM, the process $K_S \rightarrow \uppi^0 l^+ l^-$ is
\textit{CP}-conserving and dominated by a single photon exchange, while
the $K_L$ decay is \textit{CP}-violating, apart from a  small
\textit{CP}-conserving contribution (mainly originating from two-photon
process). In Chiral Perturbation Theory expansion beyond leading order,
the $K_S$ decays can be expressed as:
\begin{eqnarray*}
&\displaystyle \mathcal{B}(K_S \rightarrow \uppi^0 \rme^+ \rme^-) = (0.01-
0.76a_S - 0.21b_S + 46.5a^2_S + 12.9a_S b_S + 1.44b^2_S ) \times
10^{-10}  ,\\
&\displaystyle \mathcal{B}(K_S \rightarrow \uppi^0 \upmu^+ \upmu^-) =(0.07
- 4.52a_S - 1.50b_S + 98.7a^2_S + 57.7a_S b_S + 8.95b^2_S )\times
10^{-11} ,
\end{eqnarray*}
where $a_S$ and $b_S$ are free, real chiral-perturbation-theory
parameters of the polynomial expansion of the EM form factor in terms
of the di-lepton invariant mass $q^2$ (with $b_S$ being the coefficient
of the linear term in $q^2$)~\cite{b1}. The parameters $a_S$ and $b_S$
can be determined from data; avoiding assumptions from Vector Meson
Dominance model is possible if a large enough statistics become
available. For the $K_L$ process, \textit{CP}-violating contributions
can originate from  $K^0-\bar{K^0}$ mixing via a decay of the
\textit{CP}-even component of the $K_L$, and direct
\textit{CP}-violating contribution from short distance physics via
loops sensitive to $\mathrm{Im}(\lambda_t )=\mathrm{Im}(V_{td} V^*_{ts}
)$. The indirect and direct \textit{CP}-violating contributions can
interfere and the expression for the total \textit{CP}-violating
branching ratio can be written as~\cite{b1}:
$$
\mathcal{B}(K_L \rightarrow \uppi^0 l^+ l^- )_{CPV} =
\left[C_{\mathrm{MIX}} \pm C_{\mathrm{INT}} \frac{\mathrm{Im}
\lambda_t}{10^{-4}} + C_{\mathrm{DIR}} \left(\frac{\mathrm{Im}
\lambda_t}{10^{-4}}\right)^2 \right] \times 10^{-12}
$$
where $C_{\mathrm{INT}}$ is due to the interference between the direct
($C_{\mathrm{DIR}}$) and indirect ($C_{\mathrm{MIX}}$)
\textit{CP}-violating components. Here $C_{\mathrm{MIX}}$ and
$C_{\mathrm{INT}}$ depend on $\mathcal{B}(K_S \rightarrow \uppi^0 l^+
l^-)$ and $\sqrt{\mathcal{B}(K_S \rightarrow \uppi^0 l^+ l^-)}$
respectively.  The SM predictions for the branching ratios of $K_L
\rightarrow \uppi^0 l^+ l^-$ are:
\begin{eqnarray*}
&\displaystyle \mathcal{B}(K_L \rightarrow \uppi^0 \rme^+
\rme^-)_{\mathrm{SM}} = (3.5 \pm 0.9, 1.6 \pm 0.6) \times 10^{-11} ,
\\
&\displaystyle B(K_L \rightarrow \uppi^0 \upmu^+ \upmu^-)_{\mathrm{SM}} =
(1.4 \pm 0.3, 0.9 \pm 0.2) \times 10^{-11}
\end{eqnarray*}
for constructive (destructive) interference respectively~\cite{b2}.
Better measurements of the $K_S \rightarrow \uppi^0 l^+ l^-$ decay rate
would allow to improve the quoted errors, which are currently dominated
by the uncertainty due to the chiral-perturbation-theory parameters
$a_S$ and $b_S$. A joint study of the Dalitz plot variables and the
components of the $\upmu^+$ polarisation could directly allow the
separation of the indirect, direct \textit{CP}-violating and
\textit{CP}-conserving contributions~\cite{b3}.

The current experimental limits for $K_S$ modes come from
NA48/1~\cite{b4,b5}:
\begin{eqnarray*}
&\displaystyle 
\mathcal{B}(K_S \rightarrow \uppi^0 \upmu^+ \upmu^-) = (2.9^{+1.5}_{-1.2}
\pm 0.2) \times 10^{-9} , \\
&\displaystyle \mathcal{B}(K_S \rightarrow \uppi^0 \rme^+ \rme^- )=
(5.8^{+2.8}_{-2.3} \pm 0.8) \times 10^{-9}  ,
\end{eqnarray*}
together with the determination of two allowed regions for $a_S$ and
$b_S$ from the combination of the two branching ratio measurements
using a maximum likelihood method: $a_S = -1.6^{+2.1}_{-1.8}$ and $b_S
= 10.8^{+5.4}_{-7.7}$, or  $a_S = +1.9^{+1.6}_{-2.4}$ and $b_S =
-11.3^{+8.8}_{-4.5}$. The 90\% CL limits for the $K_L$ modes come from
KTeV~\cite{b6,b7}:
\begin{eqnarray*}
&\displaystyle \mathcal{B}(K_L \rightarrow \uppi^0 \rme^+ \rme^-)<2.8 \times
10^{-10} ,\\
&\displaystyle \mathcal{B}(K_L \rightarrow \uppi^0 \upmu^+ \upmu^-)<3.8
\times 10^{-10} .
\end{eqnarray*}
The $K_L$ channel is sensitive to BSM models. Searches of
extra-dimensions where enhancements of the branching ratio of both
$K_L$ modes by a factor of about 5 are possible without violating any
constraints~\cite{b2}. Specific flavour structures can correlate
effects in $K_S, K_L \rightarrow \uppi^0 l^+ l^-$ with B-physics
anomalies, for example LeptoQuark models with Rank-One Flavour
violation~\cite{b8,b9}. While future experiments using neutral kaon
beams can address all the four channels, the LHCb experiment can
significantly improve the precision on $\mathcal{B}(K_S \rightarrow
\uppi^0 \upmu^+ \upmu^-)$. LHCb can reach $0.25\times10^{-9}$ with 
50~fb$^{-1}$ of integrated luminosity, assuming 100\% trigger
efficiency in LHC Run3; as the precision increases, the use of the
$q^2$ dependence becomes a viable approach to avoid model dependence in
the extraction of $a_S$ and LHCb can reach a precision of 0.10 on $a_S$
and 0.35 on $b_S$ with Phase II-Upgrade~\cite{b10}.

\subsection{Lepton Number/Flavour violation}
Rare kaon decays can be used to search for explicit violation of Lepton
Number or Flavour. Both Lepton Number and Flavour are exact symmetries
in the SM, and kaon decays  $K \rightarrow (n \uppi ) \mu e$
contemplating their violation are null tests of the SM.  However many
extensions of the SM violate Lepton Flavour and Number.  Violations can
occur in BSM models, for example involving leptoquarks or the exchange
of multi-TeV Gauge mediators, that can relate the observed anomalies in
the B sector to observables in the kaon sector~\cite{c1,c2,c3,c4,c5,c6}
with kaon branching ratios expected in the range $10^{-12}$--$10^{-13}$.
Other models advocate Heavy Majorana Neutrinos as a source of Lepton
Number violation~\cite{c7,c8}. 

Limits (at 90\% CL) on the branching fractions for the $K \rightarrow
(n \uppi ) \mu e$ modes were achieved by BNL~\cite{c9},
KTeV~\cite{c10}, E865~\cite{c11,c12}  in 1990--2000, and more recently
by NA48/2~\cite{c13}:
\begin{eqnarray*}
&\displaystyle \mathcal{B} (K_L \rightarrow \rme^{\pm} \upmu^{\mp}) < 4.7
\times 10^{-12} ,\\
&\displaystyle \mathcal{B} (K_L \rightarrow \uppi^0 \rme^{\pm} \upmu^{\mp})<
7.6 \times 10^{-11} ,\\
&\displaystyle \mathcal{B} (K^+ \rightarrow \uppi^+ \rme^{-} \upmu^{+} )<
1.3 \times 10^{-11} ,\\
&\displaystyle \mathcal{B} (K^+ \rightarrow \uppi^+ \rme^{+} \upmu^{-} )<
5.2 \times 10^{-10} ,\\
&\displaystyle \mathcal{B} (K^+ \rightarrow \uppi^- \rme^{+} \upmu^{+} )<
5.0 \times 10^{-10} ,\\
&\displaystyle \mathcal{B} (K^+ \rightarrow \uppi^- \upmu^{+} \upmu^{+}) <
8.6 \times 10^{-11} ,\\
&\displaystyle \mathcal{B} (K^+ \rightarrow \uppi^- \rme^{+} \rme^{+}) < 6.4
\times 10^{-10} .
\end{eqnarray*}

The NA62 experiment has already improved on some of these
limits~\cite{c14}:
\begin{eqnarray*}
&\displaystyle \mathcal{B} (K^+ \rightarrow \uppi^- \upmu^{+} \upmu^{+} )<
4.2 \times 10^{-11} ,\\
&\displaystyle \mathcal{B} (K^+ \rightarrow \uppi^- \rme^{+} \rme^{+}) < 2.2
\times 10^{-10} ,
\end{eqnarray*}
and has prospects to push the limits for the $K^+$ modes to the range
of $10^{-12}$ to $10^{-11}$, considering the data taking foreseen after
2021. These modes can be also pursued at LHCb in the Upgrade phase,
benefiting from the large strange-production cross-section and the improved
efficiency for kaon decays; LHCb may be able to update the existing
limits and probe a sizeable part of the parameter space suggested by
the discrepancies in B physics~\cite{c15}.

\subsection{$K^+ \rightarrow \uppi^+ l^+ l^-$}

The decays $K^+ \rightarrow \uppi^+ l^+ l^-$ ($l=\upmu , \rme$) are
Flavour-Changing-Neutral-Current processes; their short-distance
contribution is described by $Z, \gamma$ penguins and box diagram, with
the amplitude depending on the logarithm-like GIM mechanism. 

The decays $K^+ \rightarrow \uppi^+ l^+ l^-$ are dominated by
long-distance contributions involving one photon exchange ($K^+
\rightarrow \uppi^+ \gamma \rightarrow \uppi^+ l^+ l^-$), and their
branching fraction can be derived within the framework of Chiral
Perturbation Theory in terms of a vector-interaction form factor, which
describes the single-photon exchange and characterises the di-muon
invariant-mass spectrum. The form factor includes a small contribution
from the two-pion-loop intermediate state and is dominated by a term
phenomenologically described as a first-order polynomial $(a_+ + b_+
z)$, where $z=(m_{\mu\mu} / M_K)^2$ and $a_+$ and $b_+$ are free
parameters, used to describe the non-perturbative QCD effects in the
chiral expansion~\cite{d1,d2}. In order to obtain both the parameters
and the corresponding branching fraction, the differential decay rate
spectrum must be reconstructed from  experimental data.

Similarly to $B \rightarrow K l^+ l^-$, this process can be described
by an effective Lagrangian with no-zero Wilson coefficients for the
semi-leptonic operators $Q_{7V}$ and $Q_{7A}$~\cite{d3},  and new
physics processes can be interpreted as deviations from the Standard
Model Wilson coefficients $C_{7V}$, $C_{7A}$. In particular, the Wilson
coefficient $C_{7A}$ can be related to $a_+$, making the form factor
measurement a test of beyond-the-SM effects~\cite{d4}.  Beside, Lepton
Flavour Universality implies the free parameters to be the same for
both the electron and muon channels. Their comparison would provide a
test of Lepton Flavour Universality, with any deviation being a sign of
short-distance new physics dynamics~\cite{d5}.

The current best experimental measurements of the $K^+ \rightarrow
\uppi^+ l^+ l^-$ branching ratios are from the NA48/2
collaboration~\cite{d6,d7}:
\begin{eqnarray*}
&\displaystyle \mathcal{B} (K^+ \rightarrow \uppi^+ \rme^+ \rme^-) =
(3.11 \pm 0.04_{\mathrm{stat}} \pm 0.12_{\mathrm{syst}})\times 10^{-7}
,\\
&\displaystyle \mathcal{B} (K^+ \rightarrow \uppi^+ \upmu^+ \upmu^-) =
(9.62 \pm 0.21_{\mathrm{stat}} \pm 0.13_{\mathrm{syst}})\times 10^{-7}. 
\end{eqnarray*}

Both the NA48/2 and E865~\cite{d8,d9} have extracted the free
parameters $a_+$ and $b_+$ for muon and electron channels, placing
limits on Lepton Flavour Universality violation. However, such test is
at present limited by the uncertainties of the measurements, especially
in the muon channel.

At NA62 both larger and significantly cleaner samples of both channels
are expected to be collected over the lifetime of the experiment
because of vast increases in instantaneous rate, improved tracking and
larger  field-integrals. The LHCb mass resolution is sufficient to
separate the muon decay from the kinematically similar three-pion
decay; the experiment can collect of order $10^4$ decays in the muon
channel per year of upgraded-LHCb  data taking. Similar considerations
apply to the electron channel, where a reduced reconstruction
efficiency is somehow compensated by the larger branching
fraction~\cite{d10}.

\section{HNL and exotics} 

The long lifetime of kaons opens the interesting possibility to
investigate with good sensitivity decays of kaons in exotic final
states including heavy neutral leptons (HNL) or exotics such as $K^+
\to \uppi^+ X$ where X is a long-lived boson.  As by-product of the
$K^+ \to \uppi^+ \nu \bar{\nu}$ analysis, one can search for new stable
neutral bosons in two body decays of the type: $K^+ \to \uppi^+ X$ and
$K^0_L\to \uppi^0 X$.  

A generic possibility of $k$ new sterile neutrino mass states 
can be written as
$$
\nu_\alpha =   \sum^{3+k}_{i=1} U_{\alpha i}\nu_i ,\quad
(\alpha = e, \mu, \tau)  .
$$

On general grounds, the extension of the neutrino sector is motivated
by its relation to the neutrino mass generation mechanism. The
$\nu$MSM~\cite{Asaka:2005pn,Asaka:2005an} is the most economical theory
accounting for neutrino masses and oscillations, baryogenesis, and dark
matter. Three heavy neutral leptons (HNLs) are posited to provide a
Dark Matter candidate ($m_1 \approx 10~\mathrm{keV}/c^2$) while two
more massive neutrinos could exist with $m_{2,3} \approx
1~\mathrm{GeV}/c^2$. 

The production of Heavy Neutral Leptons~\cite{Asaka:2005an,c17,c18} can be
searched in $K^+ \rightarrow l^+ \nu$ as a peak search over a well
know, well modelled background, independently of the HNL decay mode. 
While pion decays allow one to explore the mass region between 60 and
135~MeV/$c^2$~\cite{Aguilar-Arevalo:2017vlf}, kaons decays enable us
to extend a very sensitive search up $\approx$450~MeV/$c^2$. In
particular, both the rare decay $K^+ \rightarrow \rme^+ \nu$ and the
abundant $K^+ \rightarrow \upmu^+ \nu$ have been successfully used to
set limits in the mass range up to about 450~MeV/$c^2$.

Limits at 90\% CL on the square of the mixing angle extend down to
about $10^{-8}$ for $K^+ \to \upmu^+ N$~ \cite{Artamonov:2014urb} and
close to $10^{-9}$ for $K^+ \to \rme^+ N$~\cite{Evgueni}.  NA62
foresees to reach a sensitivity of order $10^{-9}$ and $10^{-8}$ on the
electron coupling and muon coupling respectively, with the existing
full data set. 

This is part of a broader programme covering searches for
feebly-interacting long-lived particles at LHC experiments and possible
future facilities~\cite{pbcreport}, with an interplay between the
exploration of large masses and relatively strong couplings at
colliders, and masses in the MeV--GeV region and low couplings in meson
decays and at future beam-dump facilities.

\section{Outlook}

What makes the case to continue the study of rare kaon decays
compelling, is their sensitivity beyond SM of most of the proposed
extensions, offering a genuine window of sensitivity to access high
energy scales.  The NA62 experiment at CERN will resume data taking
after LS2 to complete its physics programme for $K^+ \rightarrow
\uppi^+ \nu \nu$ and a variety of other rare kaon decays and exotic
searches. The possibility of high-intensity kaon beams, both charged
and neutral, at CERN after LS3 is being explored, with a broad physics
case covering the most interesting kaon decays as well as the golden
channels $K \rightarrow \uppi \nu \nu$. In Japan, the KOTO experiment
plans to reach the SM sensitivity for $K_L \rightarrow \uppi^0 \nu \nu$
by the middle of the next decade, and is considering a new experiment
to reach 100 SM events (KOTO Step-2). The LHCb experiment at CERN has a
programme of rare kaon decays; in the Phase-II upgrade during the High
Luminosity LHC era, the experiment will be able to explore branching
ratios below $10^{-11}$ thanks to a large strange-production
cross-section and an improved efficiency for kaon decays.

\back{}

\bibliographystyle{crunsrt}
\bibliography{crphys20200840}
\refinput{crphys20200840-reference.tex}

\end{document}
