import pytest
import responses

from django.conf import settings

from ptf.factories import AbstractFactory
from ptf.factories import ArticleWithSiteFactory
from ptf.factories import CollectionFactory
from ptf.factories import ContainerFactory
from ptf.factories import ContainerWithSiteFactory
from ptf.factories import ContributionFactory
from ptf.factories import KwdFactory

from ..doaj import doaj_pid_register
from ..doaj import doaj_resource_register


@pytest.fixture
def mocked_responses():
    with responses.RequestsMock() as rsps:
        yield rsps


@pytest.mark.django_db
def test_doaj_pid_register_old_cr():
    settings.DOAJ_TOKEN_CR = "ABCDEF"
    col = CollectionFactory(pid="CRCHIM")
    c1 = ContainerWithSiteFactory(my_collection=col)
    ArticleWithSiteFactory(my_container=c1, doi="10.1016/j.crci.2010.05.014")
    ArticleWithSiteFactory(my_container=c1, doi="10.1016/j.crci.2010.05.015")
    data, response = doaj_pid_register(pid=c1.pid)
    assert data is None
    assert response is None


@pytest.mark.django_db
def test_doaj_pid_register(mocked_responses):
    settings.DOAJ_TOKEN_CR = "ABCDEF"
    mocked_responses.add(
        method="POST",
        url=f"https://doaj.org/api/bulk/articles?api_key={settings.DOAJ_TOKEN_CR}",
        json={},
        status=401,
    )
    col = CollectionFactory(pid="CRCHIM")
    c1 = ContainerWithSiteFactory(my_collection=col)
    ArticleWithSiteFactory(my_container=c1, doi="10.5802/crchim.1")
    ArticleWithSiteFactory(my_container=c1, doi="10.5802/crchim.2")
    data, response = doaj_pid_register(pid=c1.pid)
    assert response.status_code == 401


@pytest.mark.django_db
def test_doaj_pid_resource_article():
    settings.DOAJ_TOKEN_MERSENNE = ""
    col = CollectionFactory(pid="ALCO")
    c1 = ContainerFactory(my_collection=col)
    article = ArticleWithSiteFactory(my_container=c1, title_tex="title", lang="en")
    AbstractFactory(resource=article, lang="en")
    KwdFactory(resource=article, lang="en")
    ContributionFactory(resource=article)
    data = doaj_resource_register(resource=article)
    assert data == {
        "admin": {"publisher_record_id": "10.1111/site.1"},
        "bibjson": {
            "journal": {
                "country": "FR",
                "title": "Collection1",
                "start_page": "",
                "end_page": "",
                "language": ["en"],
                "number": "3",
                "volume": "",
                "publisher": "Publisher1",
            },
            "title": "title",
            "month": "January",
            "year": "2023",
            "abstract": "abstract1",
            "author": [
                {
                    "name": "Smith, John",
                    "orcid_id": "https://orcid.org/9999-9999-9999-999X",
                }
            ],
            "keywords": ["kw1"],
            "link": [
                {
                    "url": "https://alco.centre-mersenne.org/articles/10.1111/site.1/",
                    "type": "fulltext",
                    "content_type": "HTML",
                }
            ],
            "identifier": [{"type": "doi", "id": "10.1111/site.1"}],
        },
    }


@pytest.mark.django_db
def test_doaj_pid_resource_container():
    settings.DOAJ_TOKEN_MERSENNE = ""
    col = CollectionFactory(pid="ALCO")
    c1 = ContainerFactory(my_collection=col, ctype="book-monograph", doi="10.2222")
    data = doaj_resource_register(resource=c1)
    assert data == {
        "admin": {"publisher_record_id": "10.2222"},
        "bibjson": {
            "journal": {
                "country": "FR",
                "title": "Collection1",
                "start_page": "",
                "end_page": "",
                "language": ["fr"],
                "number": "3",
                "volume": "",
                "publisher": "Publisher1",
            },
            "title": "Container1",
            "month": "",
            "year": "2014",
            "abstract": "",
            "author": [],
            "keywords": [],
            "link": [
                {
                    "url": "https://alco.centre-mersenne.org/articles/10.2222/",
                    "type": "fulltext",
                    "content_type": "HTML",
                }
            ],
            "identifier": [{"type": "doi", "id": "10.2222"}],
        },
    }
