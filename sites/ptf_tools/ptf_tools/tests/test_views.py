import responses
from responses.matchers import query_param_matcher

from django.contrib.auth.models import Group
from django.contrib.auth.models import User
from django.test import TestCase
from django.test import override_settings

from comments_moderation.models import CommentModerator
from ptf.factories import CollectionFactory

from ..models import CollectionGroup

COMMENTS_BASE_URL = "http://comments.test"


@override_settings(
    COMMENTS_VIEWS_API_BASE_URL=COMMENTS_BASE_URL,
    COMMENTS_VIEWS_API_CREDENTIALS=("login", "password"),
    COMMENTS_DISABLED=False,
)
class ViewTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        print("\n**App: 'ptf_tools' - Testing `views.py`**")
        super().setUpClass()

    @responses.activate
    def test_home_view_permissions(self):
        """
        Test the correct template selection and redirection according to the user
        status.
        """
        # Anonymous user
        response = self.client.get("/")
        self.assertRedirects(response, "/accounts/login/?next=/", fetch_redirect_response=False)

        # Staff user
        staff_user = User.objects.create_user("staff_user", "staff@test.test", "***")
        staff_user.is_staff = True
        staff_user.save()
        self.client.login(username="staff@test.test", password="***")

        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, "home.html")
        self.assertNotIn("overview", response.context)
        self.client.logout()

        # Comment moderator user
        comment_user = User.objects.create_user("comment_user", "comment@test.test", "***")
        CommentModerator.objects.create(user=comment_user, is_moderator=True)
        self.client.login(username="comment@test.test", password="***")

        response = self.client.get("/")
        self.assertRedirects(response, "/comments/", fetch_redirect_response=False)
        self.client.logout()

        # Random user
        User.objects.create_user("random_user", "random@test.test", "***")
        self.client.login(username="random@test.test", password="***")

        response = self.client.get("/")
        self.assertEqual(response.status_code, 404)
        self.client.logout()

        # User with 1 authorized collection
        collection_1 = CollectionFactory(pid="PID_1", title_tex="Collection 1")
        group = Group.objects.create(name="PID_1")
        col_group = CollectionGroup.objects.create(group=group)
        col_group.collections.add(collection_1)

        group_user = User.objects.create_user("group_user", "group@test.test", "***")
        group_user.groups.add(group)
        self.client.login(username="group@test.test", password="***")

        response = self.client.get("/")
        self.assertRedirects(response, "/collection/PID_1/", fetch_redirect_response=False)
        self.client.logout()

        # User with 2 authorized collections
        collection_2 = CollectionFactory(pid="PID_2", title_tex="Collection 2")
        group = Group.objects.create(name="PID_1&2")
        col_group = CollectionGroup.objects.create(group=group)
        col_group.collections.add(collection_1, collection_2)

        group_user = User.objects.create_user("group_user_2", "group_2@test.test", "***")
        group_user.groups.add(group)
        self.client.login(username="group_2@test.test", password="***")

        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, "home.html")
        self.assertTrue(response.context["overview"])
        self.assertFalse(response.context["comment_server_ok"])
        self.assertFalse(response.context["translation_server_ok"])
        self.client.logout()

        User.objects.all().delete()
        Group.objects.all().delete()

    @responses.activate
    def test_home_view_comments(self):
        """
        Test the correct population of comment data for an user with mutliple
        authorized collections.
        """
        collection_1 = CollectionFactory(pid="PID_1", title_tex="Collection 1")
        collection_2 = CollectionFactory(pid="PID_2", title_tex="Collection 2")
        collection_3 = CollectionFactory(pid="PID_3", title_tex="Collection 3")
        group = Group.objects.create(name="PID_1&2&3")
        col_group = CollectionGroup.objects.create(group=group)
        col_group.collections.add(collection_1, collection_2, collection_3)

        group_user = User.objects.create_user("group_user", "group@test.test", "***")
        group_user.groups.add(group)
        self.client.login(username="group@test.test", password="***")

        # Mock comment request
        query_params = {
            "moderator_id": f"{group_user.pk}",
            "collection_id": f"{collection_1.pid},{collection_2.pid},{collection_3.pid}",
        }
        responses.add(
            method="GET",
            url=f"{COMMENTS_BASE_URL}/api/comments-summary/",
            status=200,
            match=[query_param_matcher(query_params)],
            json={
                collection_1.pid.lower(): {"submitted": 1, "validated": 2},
                collection_2.pid.lower(): {"validated": 4},
            },
        )

        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)
        context = response.context
        self.assertTrue(context["overview"])
        self.assertTrue(response.context["comment_server_ok"])

        collections = context["collections"]
        self.assertEqual(len(collections), 3)
        col = collections[0]
        self.assertEqual(col["pid"], collection_1.pid)
        self.assertEqual(col["pending_comments"], 1)
        col = collections[1]
        self.assertEqual(col["pid"], collection_2.pid)
        self.assertEqual(col["pending_comments"], 0)
        col = collections[2]
        self.assertEqual(col["pid"], collection_3.pid)
        self.assertNotIn("pending_comments", col)

        User.objects.all().delete()
        Group.objects.all().delete()
