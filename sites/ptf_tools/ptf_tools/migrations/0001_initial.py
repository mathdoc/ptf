# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations
from django.contrib.auth.hashers import make_password


def apply_migration(apps, schema_editor):

    User = apps.get_model('auth', 'User')
    admin_user = User(
        username='admin-tools',
        email='admin@tools.com',
        password=make_password('admin'),
        is_superuser=False,
        is_staff=True
    )
    admin_user.save()

def revert_migration(apps, schema_editor):
    User = apps.get_model('auth', 'User')
    User.objects.filter(username="admin-tools").delete()

class Migration(migrations.Migration):
    dependencies = [
        ('auth', '0001_initial'),
        ('ptf', '0010_auto_20170822_1553')
    ]

    operations = [
        migrations.RunPython(apply_migration, revert_migration),
    ]
