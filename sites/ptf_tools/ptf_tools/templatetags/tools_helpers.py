from django import template
from django.conf import settings
from django.contrib.auth.models import AnonymousUser
from django.contrib.auth.models import User
from django.template.defaultfilters import stringfilter

register = template.Library()


@register.filter
@stringfilter
def get_mersenne_collections(value):
    return settings.MERSENNE_COLLECTIONS


@register.filter
def get_authorized_collections(user: User | AnonymousUser) -> list[str]:
    ids = []
    for group in user.groups.all():
        if hasattr(group, "collectiongroup"):
            ids.extend([col.pid for col in group.collectiongroup.collections.all()])
    return sorted(ids)
