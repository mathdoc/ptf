import os
from datetime import datetime

import requests
from lxml import etree

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.template.loader import render_to_string

from mersenne_tools.models import DOIBatch
from ptf.display.resolver import find_id_type


def get_doibatch(resource):
    doibatch = None
    try:
        doibatch = resource.doibatch
    except ObjectDoesNotExist:
        pass

    return doibatch


def get_or_create_doibatch(resource):
    """
    @param models.Resource:
    @return: new or updated doibatch
    """

    update_doi = False
    # check DOI
    url = settings.DOI_BASE_URL + resource.doi
    r = requests.get(url, allow_redirects=False)
    if r.status_code == 302 and resource.get_url_absolute() == r.headers["Location"]:
        status = "Enregistré"
        log = "Vérifié sur CROSSREF"
        update_doi = True
    elif r.status_code == 302 and resource.get_url_absolute() != r.headers["Location"]:
        status = "Erreur"
        log = "Mauvaise URL pour le DOI !!!/à réenregistrer"
        update_doi = True
    doibatch = get_doibatch(resource)
    if update_doi:
        if doibatch:
            doibatch.status = status
            doibatch.log = log
        else:
            doibatch = DOIBatch(resource=resource, status=status, log=log)
        doibatch.save()
        return doibatch

    # si on est dans le cas d'un book-part vu que l'enregistrement se fait niveau container, on ne peut pas interroger le batch
    # lié au book-part, car il a été créé juste pour afficher "En cours" sur le niveau book-part
    if (
        doibatch
        and resource.classname == "Article"
        and resource.my_container.ctype.startswith("book")
    ):
        doibatch.delete()
        doibatch = None

    if doibatch:
        doibatch = checkDOIBatch(doibatch)

    return doibatch


# recordDOI par resource (article)
# problématique liée à l'enregistrement des DOI chez CROSSREF :
# - pour enregistrer un DOI, on utilise le DOI du journal comme référence : CROSSREF prend ça comme une demande d'enregistrement/modification !
# du DOI du journal...
# ce qui se passe lorsque l'on envoie plusieurs requêtes les unes à la suite des autres (Record all DOIs), c'est que l'ordre de traitement est
# différent (aléatoire) de l'ordre d'envoi et on obtient ces erreurs :
# "Record not processed because submitted version: 201810150907372216 is less or equal to previously submitted version {1}"
# ( MAIS le record impliqué ici est celui du journal, celui de l'article ne pose globalement pas de pb)
# car il y a un timestamp dans chaque requête
#
# pour contrer ces erreurs (avant on ne diagnostiquait que le nombre de failure_count et donc il y en avait une) il faut interpréter le xml de retour ::
# <record_diagnostic status="Success">
#       <doi>10.5802/alco.21</doi>
#       <msg>Successfully updated</msg>
# C'est ce qui est retenu (dans checkDOIBatch).
#


def recordDOI(resource, testing=False):
    """
    @param resource:
    @param testing: Boolean set to True when testing
    @return:  data {status: 200 ou 400, 'message': msg}
    """

    doibatch = get_doibatch(resource)
    if doibatch:
        doibatch.delete()

    doibatch = DOIBatch(resource=resource, status="En cours")
    doibatch.save()
    context = {}
    context["doi_batch_id"] = f"{doibatch.pk:04d}"
    # https://data.crossref.org/reports/help/schema_doc/4.4.2/schema_4_4_2.html#timestamp
    timestamp = datetime.now().strftime("%Y%m%d%H%M%S%f")  # len = 20, must be 19
    context["timestamp"] = timestamp[0:19]
    context["mail"] = settings.CROSSREF_MAIL
    template = f"crossref/{resource.classname.lower()}_doi_register.xml"
    crossref_user = None
    crossref_pwd = None

    # hack pour déterminer la date de publication pour une resource
    if resource.classname == "Article":
        # si un article n'a pas de contributeurs, on enregistre un posted-content de type other
        # https://data.crossref.org/reports/help/schema_doc/4.4.2/schema_4_4_2.html#posted_content
        if not resource.get_author_contributions() and resource.classname == "Article":
            template = "crossref/posted-content.xml"

        # on est en présence d'un objet qui a besoin d'une date de publication
        if not resource.date_published and not resource.date_online_first:
            # on extrapole la date du volume
            date = resource.my_container.year
            try:
                date = datetime.strptime(date, "%Y")
                resource.DOIdate = "<year>%s</year>" % resource.my_container.year
            except ValueError:
                # on suppose que la date est du format 2010-2011, on garde la 2eme année du range
                year = resource.my_container.year.split("-")[1]
                resource.DOIdate = "<year>%s</year>" % year
                resource.my_container.year = year
        else:
            # on renseigne la date selon le format voulu par CROSSREF
            if resource.date_published:
                resource.DOIdate = resource.date_published.strftime(
                    "<month>%m</month><day>%d</day><year>%Y</year>"
                )

                # on check aussi la date du container
                date = resource.my_container.year
                try:
                    date = datetime.strptime(date, "%Y")
                except ValueError:
                    # on suppose que la date est du format 2010-2011, on garde la 2eme année du range
                    year = resource.my_container.year.split("-")[1]
                    resource.my_container.year = year
            else:
                # Online First
                # TODO: Is it possible to send 2 dates to Crossref ?
                # You can send multiple <publication_date> but it is for multiple media_type (print vs online)
                resource.DOIdate = resource.date_online_first.strftime(
                    "<month>%m</month><day>%d</day><year>%Y</year>"
                )

                # Le year du container vaut '0'

    elif resource.classname == "Container":
        if resource.ctype.startswith("book"):
            # PS : pas de gestion des chapitres pour les livres, tout est fait dans le template au moment de l'enregistrement du book
            # template en fct du ctype
            if resource.my_collection.issn or resource.my_collection.e_issn:
                template = "crossref/book_series_metadata.xml"
            else:
                template = "crossref/book_set_metadata.xml"
            # else #book tout seul n'appartenant pas à une série
            # template = book_metadata
            context["book_type"] = resource.ctype[5:].replace("-", "_")
            for bookpart in resource.article_set.all():
                doibatch = get_doibatch(bookpart)
                if doibatch:
                    doibatch.delete()
                doibatch = DOIBatch(resource=bookpart, status="En cours")
                doibatch.save()

        elif resource.ctype == "issue":
            # TODO
            template = "issue.xml"
        date = resource.year
        try:
            date = datetime.strptime(date, "%Y")
            resource.DOIdate = "<year>%s</year>" % resource.year
        except ValueError:
            # on suppose que la date est du format 2010-2011, on garde la 2eme année du range
            year = resource.year.split("-")[1]
            resource.DOIdate = "<year>%s</year>" % year

    elif resource.classname == "TranslatedArticle":
        with open(
            os.path.join(settings.LOG_DIR, "record_doi.log"), "a", encoding="utf-8"
        ) as file_:
            file_.write(resource.doi + "\n")

        resource.DOIdate = resource.date_published.strftime(
            "<month>%m</month><day>%d</day><year>%Y</year>"
        )
        context["collection"] = resource.original_article.get_top_collection()

    context["resource"] = resource

    preprint_id = preprint_type = None
    qs = resource.extid_set.filter(id_type="preprint")
    if qs:
        extid = qs.first()
        preprint_id = extid.id_value
        preprint_type = find_id_type(preprint_id)
        # crossref allows "doi" and "arxiv", but not "hal"
        if preprint_type == "hal":
            preprint_type = "other"
    context["preprint_id"] = preprint_id
    context["preprint_type"] = preprint_type

    rdoi = None
    qs = resource.extid_set.filter(id_type="rdoi")
    if qs:
        rdoi = qs.first().id_value
    context["rdoi"] = rdoi

    try:
        xml = render_to_string(template_name=template, context=context)
        doibatch.xml = xml
        doibatch.save()
    except Exception as e:
        if resource.classname == "TranslatedArticle":
            with open(
                os.path.join(settings.LOG_DIR, "record_doi.log"), "a", encoding="utf-8"
            ) as file_:
                file_.write(str(e) + "\n")
        raise e

    files = {"file": (f"{doibatch.pk}.xml", xml)}

    data = {"status": 404}
    if not testing:
        if resource.classname == "TranslatedArticle":
            crossref_user, crossref_pwd = get_user_pwd_crossref(resource.original_article)

            with open(
                os.path.join(settings.LOG_DIR, "record_doi.log"), "a", encoding="utf-8"
            ) as file_:
                file_.write("Call crossref\n")

        elif resource.classname == "Container" and resource.ctype.startswith("book"):
            # pas de doi niveau container, alors pour obtenir les identifiants crossref on part sur le 1er book part
            crossref_user, crossref_pwd = get_user_pwd_crossref(resource.article_set.first())
        else:
            crossref_user, crossref_pwd = get_user_pwd_crossref(resource)

        crossref_batch_url = settings.CROSSREF_BATCHURL_TPL % (crossref_user, crossref_pwd)

        r = requests.post(crossref_batch_url, files=files)
        body = r.text.encode("utf8")
        if r.status_code == 200:
            xml = etree.XML(body)
            title = xml.xpath("//*/title")[0].text
            if title == "SUCCESS":
                data["status"] = r.status_code
        elif r.status_code == 401:
            doibatch.status = "Erreur"
            doibatch.log = "Pb d'authentification"
            doibatch.save()
        else:
            doibatch.status = "Erreur"
            doibatch.save()
        data["message"] = body[:1000].decode("utf-8")

        if resource.classname == "TranslatedArticle":
            with open(
                os.path.join(settings.LOG_DIR, "record_doi.log"), "a", encoding="utf-8"
            ) as file_:
                file_.write(doibatch.status + "\n")
    return data


def get_user_pwd_crossref(resource):
    # get CROSSREF credentials from DOI prefix
    doi = resource.doi
    prefix = doi.split("/")[0]
    md_prefix = prefix.split(".")[1]
    crossref_user_const = "CROSSREF_USER_" + md_prefix
    crossref_pwd_const = "CROSSREF_PWD_" + md_prefix
    try:
        crossref_user = getattr(settings, crossref_user_const)
        crossref_pwd = getattr(settings, crossref_pwd_const)
    except AttributeError:
        crossref_user = settings.CROSSREF_USER_5802
        crossref_pwd = settings.CROSSREF_PWD_5802
    return crossref_user, crossref_pwd


def checkDOIBatch(doibatch):
    """
    check DOI batch status by HTTP request
    @param doibatch: DOIBatch
    @return: DOIBatch with status and log updated
    """

    resource = doibatch.resource
    crossref_user, crossref_pwd = get_user_pwd_crossref(resource)
    url = settings.CROSSREF_BASE_CHECKBATCH_URL_TPL % (crossref_user, crossref_pwd)
    url = url.format(doibatch.pk)
    r = requests.get(url)
    if r.status_code == 200:
        # analyse du xml de retour
        dataXml = r.text.encode("utf8")
        tree = etree.XML(dataXml)
        elem = tree.xpath("/doi_batch_diagnostic")[0]
        batch_status = elem.attrib["status"]
        if batch_status == "completed":
            # le batch a été traité
            doibatch.status = "batch terminé"
            doibatch.log = "Pas de DOI associé dans le batch : voir le xml"
            diags = tree.xpath("//*/record_diagnostic")
            for diag in diags:
                doi = diag.xpath("doi")[0].text
                log = diag.xpath("msg")[0].text
                status = diag.attrib["status"]
                if doi == doibatch.resource.doi:
                    if status == "Success":
                        doibatch.status = "Enregistré"
                    else:
                        doibatch.status = "Erreur"
                else:
                    doibatch.status = "Erreur"
                doibatch.log = log

        elif batch_status == "in_process" or batch_status == "queued":
            doibatch.status = "En cours"
            doibatch.log = "batch en cours de traitement"
        else:  # rafraichit trop tot apres Record DOI
            doibatch.status = "Erreur"
            doibatch.log = (
                "Attention, il se peut qu'il faille rafraichir "
                "un peu plus tard {} ".format(r.text)
            )
    else:
        doibatch.status = "Erreur"
        doibatch.log = r.text
    doibatch.save()
    return doibatch


def removeOldDataInCrossref(article, testing=False):
    """
    The CRAS 2002-2019 articles were registered by Elsevier
    To remove some metadata in Crossref, we need to provide a separate XML with the fields to remove

    @param article:
    @param testing: Boolean set to True when testing
    @return:  data {status: 200 ou 400, 'message': msg}
    """

    doibatch = get_doibatch(article)
    if doibatch:
        doibatch.delete()

    doibatch = DOIBatch(resource=article, status="En cours")
    doibatch.save()

    context = {"resource": article, "doi_batch_id": f"{doibatch.pk:04d}"}

    timestamp = datetime.now().strftime("%Y%m%d%H%M%S%f")  # len = 20, must be 19
    context["timestamp"] = timestamp[0:19]

    context["mail"] = settings.CROSSREF_MAIL
    template = "crossref/article_remove_old_data.xml"

    if article.date_published:
        article.DOIdate = article.date_published.strftime(
            "<month>%m</month><day>%d</day><year>%Y</year>"
        )

    try:
        xml = render_to_string(template_name=template, context=context)

        if testing:
            print(xml)

        doibatch.xml = xml
        doibatch.save()
    except Exception as e:
        raise e

    files = {"file": (f"{doibatch.pk}.xml", xml)}

    data = {"status": 404}
    if not testing:
        crossref_user, crossref_pwd = get_user_pwd_crossref(article)
        crossref_batch_url = settings.CROSSREF_BATCHURL_TPL % (crossref_user, crossref_pwd)

        r = requests.post(crossref_batch_url, files=files)
        body = r.text.encode("utf8")
        if r.status_code == 200:
            xml = etree.XML(body)
            title = xml.xpath("//*/title")[0].text
            if title == "SUCCESS":
                data["status"] = r.status_code
        elif r.status_code == 401:
            doibatch.status = "Erreur"
            doibatch.log = "Pb d'authentification"
            doibatch.save()
        else:
            doibatch.status = "Erreur"
            doibatch.save()
        data["message"] = body[:1000].decode("utf-8")

    return data
