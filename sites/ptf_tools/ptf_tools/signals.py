from itertools import chain

from allauth.account.signals import user_signed_up

from django.contrib.auth.models import Group
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.http import HttpRequest
from django.urls import reverse_lazy

from comments_moderation.models import CommentModerator
from comments_moderation.rights import ModeratorUserRights
from comments_moderation.utils import update_moderation_right
from ptf.models import Collection
from ptf.utils import send_email_from_template

from .models import Invitation
from .models import InvitationExtraData


@receiver(user_signed_up)
def update_user_from_invite(
    sender, request: HttpRequest | None = None, user: User | None = None, **kwargs
):
    """
    Update extra information on the user signing up from the attached invitation.
    The extra information is either basic data (first name, last name)
    or app specific extra information (eg. for comment moderation).
    """

    if not isinstance(user, User):
        return

    try:
        invite: Invitation | None = Invitation.objects.get(email__iexact=user.email)
    # TODO: Log if no invitation attached to the user
    # This should not happened since signup is closed outside of invitation system
    except Invitation.DoesNotExist:
        invite = None

    if invite is None:
        return

    if invite.first_name:
        user.first_name = invite.first_name
    if invite.last_name:
        user.last_name = invite.last_name

    extra_data = InvitationExtraData(**invite.extra_data)

    # Handle comment moderation related data
    if extra_data.moderator:
        # The list of users that "invited" the signing up user as a comment moderator.
        # We will send an e-mail to notify them of the account creation.
        users_to_mail = []
        # Mark the user as a comment moderator
        try:
            comment_moderator = CommentModerator.objects.get(user=user)
        except CommentModerator.DoesNotExist:
            comment_moderator = CommentModerator(user=user)

        comment_moderator.is_moderator = True
        comment_moderator.save()

        # Staff moderators - Fill the selected collections
        if extra_data.moderator.collections:
            pids = chain.from_iterable(c.pid for c in extra_data.moderator.collections)
            collections_id = Collection.objects.filter(pid__in=set(pids)).values_list(
                "pk", flat=True
            )
            comment_moderator.collections.add(*collections_id)

            users_to_mail.extend(c.user_id for c in extra_data.moderator.collections)

        # Base moderatos - Create a moderaton right entry associated to
        # the selected comments
        if extra_data.moderator.comments:
            processed_comments = []
            for comment in extra_data.moderator.comments:
                try:
                    users_to_mail.append(comment.user_id)
                    if comment.id in processed_comments:
                        continue
                    right_creator = User.objects.get(pk=comment.user_id)
                    rights = ModeratorUserRights(right_creator)
                    error, _ = update_moderation_right(comment.id, user.pk, rights)
                    # TODO: Log if the the POST request is unsuccessful ?
                    processed_comments.append(comment.id)
                # Catch any exception and continue the workflow.
                # We don't want this additional processing to result in the sign up
                # failing.
                except Exception:
                    continue
        # Send notification e-mail
        if users_to_mail:
            recipient_list = User.objects.filter(pk__in=set(users_to_mail)).values(
                "first_name", "last_name", "email"
            )
            if recipient_list:
                for r in recipient_list:
                    context_data = {
                        "full_name": f"{r['first_name']} {r['last_name']}",
                        "sign_up_name": f"{user.first_name} {user.last_name}",
                        "sign_up_email": user.email,
                        "comment_moderator_url": request.build_absolute_uri(
                            reverse_lazy("comment_moderators")
                        ),
                        "invitation_date": invite.created,
                        "email_signature": "The editorial team",
                    }
                    send_email_from_template(
                        "mail/comment_moderator_account_created.html",
                        context_data,
                        "[TRAMMEL] Comment moderator sign up",
                        to=[r["email"]],
                    )

    # Add the newly created user to the given user groups.
    if extra_data.user_groups:
        group_pks = []
        for group_pk in extra_data.user_groups:
            try:
                group_pks.append(int(group_pk))
            except ValueError:
                pass
        if group_pks:
            user_groups = Group.objects.filter(pk__in=group_pks)
            if user_groups:
                user.groups.add(*user_groups)

    user.save()
