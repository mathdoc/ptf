import debug_toolbar
from allauth.account import urls as account_urls
from ckeditor_uploader.views import upload

from django.conf import settings
from django.conf.urls.static import static
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.urls import include
from django.urls import path
from django.urls import re_path
from django.views.decorators.cache import never_cache

from comments_moderation import urls as comments_urls
from history import urls as history_urls
from invitations import urls as invitations_urls
from matching import urls as matching_urls
from mersenne_cms import urls as cms_urls
from oai import urls as oai_urls
from ptf import urls
from ptf.admin import ptf_admin
from ptf_tools.views import AddIssuePDFView
from ptf_tools.views import ArchiveAllAPIView
from ptf_tools.views import ArchiveIssueAPIView
from ptf_tools.views import ArticleDeleteView
from ptf_tools.views import ArticleEditWithVueAPIView
from ptf_tools.views import ArticleEditWithVueView
from ptf_tools.views import ArticleListView
from ptf_tools.views import BibItemIdApiDetail
from ptf_tools.views import BibItemIdCreate
from ptf_tools.views import BibItemIdUpdate
from ptf_tools.views import BibtexAPIView
from ptf_tools.views import CollectionCreate
from ptf_tools.views import CollectionDetail
from ptf_tools.views import CollectionUpdate
from ptf_tools.views import ContainerEditView
from ptf_tools.views import CreateAllDjvuAPIView
from ptf_tools.views import CreateDjvuAPIView
from ptf_tools.views import CreatedVolumesDashboardView
from ptf_tools.views import CROSSREFResourceCheckStatusView
from ptf_tools.views import CROSSREFResourceRegisterView
from ptf_tools.views import DeleteJatsIssueAPIView
from ptf_tools.views import DeployAllAPIView
from ptf_tools.views import DeployAllNumdamAPIView
from ptf_tools.views import DeployCMSAPIView
from ptf_tools.views import DeployCollectionAPIView
from ptf_tools.views import DeployJatsResourceAPIView
from ptf_tools.views import DeployTranslatedArticleAPIView
from ptf_tools.views import DiffCedricsIssueView
from ptf_tools.views import DOAJResourceRegisterView
from ptf_tools.views import EditorialToolsArticleView
from ptf_tools.views import EditorialToolsVolumeItemsView
from ptf_tools.views import ExtIdApiDetail
from ptf_tools.views import ExtIdCreate
from ptf_tools.views import ExtIdFormTemplate
from ptf_tools.views import ExtIdUpdate
from ptf_tools.views import FailedTasksDeleteView
from ptf_tools.views import FailedTasksListView
from ptf_tools.views import FailedTasksRetryView
from ptf_tools.views import GetCMSFromSiteAPIView
from ptf_tools.views import GraphicalAbstractDeployView
from ptf_tools.views import GraphicalAbstractUpdateView
from ptf_tools.views import ImportAllAPIView
from ptf_tools.views import ImportCedricsArticleFormView
from ptf_tools.views import ImportCedricsIssueView
from ptf_tools.views import ImportJatsContainerAPIView
from ptf_tools.views import IssueDetailAPIView
from ptf_tools.views import MatchingAPIView
from ptf_tools.views import NewsCreateView
from ptf_tools.views import NewsDeleteView
from ptf_tools.views import NewsUpdateView
from ptf_tools.views import NumdamArchiveView
from ptf_tools.views import NumdamDeleteAPIView
from ptf_tools.views import NumdamView
from ptf_tools.views import PageCreateView
from ptf_tools.views import PageDeleteView
from ptf_tools.views import PageIndexView
from ptf_tools.views import PageUpdateView
from ptf_tools.views import PTFToolsContainerView
from ptf_tools.views import PTFToolsHomeView
from ptf_tools.views import PublishedArticlesDashboardView
from ptf_tools.views import RegisterPubmedFormView
from ptf_tools.views import RegisterPubmedView
from ptf_tools.views import RestoreCMSAPIView
from ptf_tools.views import SpecialIssueEditAPIView
from ptf_tools.views import SpecialIssueEditView
from ptf_tools.views import SpecialIssuesIndex
from ptf_tools.views import SuggestDeployView
from ptf_tools.views import SuggestUpdateView
from ptf_tools.views import TasksProgressView
from ptf_tools.views import TasksView
from ptf_tools.views import TestView
from ptf_tools.views import TrammelArchiveView
from ptf_tools.views import TrammelFailedTasksListView
from ptf_tools.views import TrammelTasksProgressView
from ptf_tools.views import UpdateTexmfZipAPIView
from ptf_tools.views import VirtualIssueCreateView
from ptf_tools.views import VirtualIssueDeployView
from ptf_tools.views import VirtualIssueParseView
from ptf_tools.views import VirtualIssuesIndex
from ptf_tools.views import VirtualIssueUpdateView
from ptf_tools.views import VolumeListView
from ptf_tools.views import do_not_publish_article
from ptf_tools.views import file_browse_in_collection
from ptf_tools.views import file_upload_in_collection
from ptf_tools.views import load_cedrics_article_choices
from ptf_tools.views import show_article_body
from pubmed import urls as pubmed_urls

from .views import view_404

urlpatterns_staff = [
    path("collection/create/", CollectionCreate.as_view(), name="collection-create"),
    path("collection/<path:pid>/update/", CollectionUpdate.as_view(), name="collection-update"),
    path(
        "import_cedrics_issue/<path:colid>/",
        ImportCedricsIssueView.as_view(),
        name="import_cedrics_issue",
    ),
    path(
        "archive_collection/<path:colid>/", TrammelArchiveView.as_view(), name="archive_collection"
    ),
    path(
        "import_cedrics_article/<path:colid>/",
        ImportCedricsArticleFormView.as_view(),
        name="import_cedrics_article",
    ),
    path(
        "diff_cedrics_issue/<path:colid>/",
        DiffCedricsIssueView.as_view(),
        name="diff_cedrics_issue",
    ),
    re_path(
        r"^extid/form/(?P<sequence>[0-9]+)/",
        ExtIdFormTemplate.as_view(),
        name="extid-form-template",
    ),
    path("extid/<int:pk>/update/", ExtIdUpdate.as_view(), name="extid-update"),
    re_path(
        r"^api/bibitemid/(?P<pid>.+)/(?P<what>[a-z-]+)/(?P<seq>[0-9]+)/$",
        BibItemIdApiDetail.as_view(),
        name="api-bibitemid-detail",
    ),
    path("api/issue/<path:pid>/", IssueDetailAPIView.as_view(), name="api-issue-detail"),
    re_path(
        r"^api/extid/(?P<pid>.+)/(?P<what>[a-z-]+)/$",
        ExtIdApiDetail.as_view(),
        name="api-extid-detail",
    ),
    path("api/import_all/<path:pid>/", ImportAllAPIView.as_view(), name="api_import_all"),
    path(
        "api/import_jats_container/<path:colid>/<path:pid>/",
        ImportJatsContainerAPIView.as_view(),
        name="api_import_jats_container",
    ),
    path(
        "api/deploy_all/<path:pid>/<path:site>/", DeployAllAPIView.as_view(), name="api_deploy_all"
    ),
    path(
        "api/deploy_collection/<path:colid>/<path:site>/",
        DeployCollectionAPIView.as_view(),
        name="api_deploy_collection",
    ),
    path("api/archive_all/<path:pid>/", ArchiveAllAPIView.as_view(), name="api_archive_all"),
    path(
        "api/archive_issue/<path:colid>/<path:pid>/",
        ArchiveIssueAPIView.as_view(),
        name="api_archive_issue",
    ),
    path(
        "api/create_all_djvu/<path:pid>/",
        CreateAllDjvuAPIView.as_view(),
        name="api_create_all_djvu",
    ),
    path("api/create_djvu/<path:pid>/", CreateDjvuAPIView.as_view(), name="api_create_djvu"),
    path(
        "api/save_cms/<path:colid>/<path:site>/",
        GetCMSFromSiteAPIView.as_view(),
        name="api_save_cms",
    ),
    path("api/restore_cms/<path:colid>/", RestoreCMSAPIView.as_view(), name="api_restore_cms"),
    path("api/bibtex/<path:pid>/", BibtexAPIView.as_view(), name="api_bibtex"),
    path("api/matching/<path:pid>/", MatchingAPIView.as_view(), name="api_matching"),
    path(
        "api/add_issue_pdf/<path:pid>/<path:site>/",
        AddIssuePDFView.as_view(),
        name="add_issue_pdf",
    ),
    path("doajregister/<path:pid>/", DOAJResourceRegisterView.as_view(), name="doaj_register"),
    path("doiregister/<path:pid>/", CROSSREFResourceRegisterView.as_view(), name="doi_register"),
    path(
        "forcedoiregister/<path:pid>/",
        CROSSREFResourceRegisterView.as_view(),
        {"force": "force"},
        name="force_doi_register",
    ),
    path("doicheck/<path:pid>/", CROSSREFResourceCheckStatusView.as_view(), name="doi_check"),
    path(
        "pubmed-register-form/<path:pid>/",
        RegisterPubmedFormView.as_view(),
        name="pubmed_register_form",
    ),
    path("pubmed-register/<path:pid>/", RegisterPubmedView.as_view(), name="pubmed_register"),
    path("issues/<path:pid>/", PTFToolsContainerView.as_view(), name="issue-items"),
    path("books/<path:pid>/", PTFToolsContainerView.as_view(), name="book-toc"),
    path("container/<path:pid>/edit/", ContainerEditView.as_view(), name="container_edit"),
    path("container/<path:colid>/create/", ContainerEditView.as_view(), name="container_create"),
    path(
        "special-issue/<path:colid>/create/",
        ContainerEditView.as_view(),
        name="special_issue_create",
    ),
    path("article/<int:resource_pk>/extid/create/", ExtIdCreate.as_view(), name="extid-create"),
    # re_path(r'^article/(?P<pid>.+)/edit/$', ArticleEditView.as_view(), name='article_edit'),
    # re_path(r'^article/(?P<issue_id>.+)/create/$', ArticleEditView.as_view(), name='article_create'),
    path("article/<path:pid>/delete/", ArticleDeleteView.as_view(), name="article_delete"),
    re_path(
        r"^api-article-edit/(?P<colid>[A-Z-]+)/(?P<doi>.+)/$",
        ArticleEditWithVueAPIView.as_view(),
        name="api-edit-article",
    ),
    re_path(
        r"^article-edit/(?P<colid>[A-Z-]+)/(?P<doi>.+)/$",
        ArticleEditWithVueView.as_view(),
        name="edit-article",
    ),
    path(
        "article/<path:pid>/do_not_publish/", do_not_publish_article, name="do_not_publish_article"
    ),
    path("article/<path:pid>/show_body/", show_article_body, name="show_article_body"),
    path("numdam/", NumdamView.as_view(), name="numdam"),
    path("numdam/archive/<path:colid>/", NumdamArchiveView.as_view(), name="numdam-archive"),
    path("numdam/archive/progress/", TasksProgressView.as_view(), name="tasks-progress"),
    path(
        "collection/tasks/progress/<path:task>/",
        TrammelTasksProgressView.as_view(),
        name="trammel-tasks-progress",
    ),
    path(
        "collection/tasks/fails/<path:colid>",
        TrammelFailedTasksListView.as_view(),
        name="trammel-tasks-failed",
    ),
    path("numdam/archive/tasks/fails/", FailedTasksListView.as_view(), name="tasks-failed"),
    path(
        "numdam/archive/tasks/<int:pk>/retry/", FailedTasksRetryView.as_view(), name="tasks-retry"
    ),
    path(
        "numdam/archive/tasks/<int:pk>/delete/",
        FailedTasksDeleteView.as_view(),
        name="tasks-delete",
    ),
    path("api/deploy_all_numdam/", DeployAllNumdamAPIView.as_view(), name="api_deploy_all_numdam"),
    path("numdam/<path:pid>/delete", NumdamDeleteAPIView.as_view(), name="api_delete_numdam"),
    path(
        "bibitem/<int:bibitem_pk>/bibitemid/create/",
        BibItemIdCreate.as_view(),
        name="bibitemid-create",
    ),
    path("bibitemid/<int:pk>/update/", BibItemIdUpdate.as_view(), name="bibitemid-update"),
    path(
        "load-cedrics-article-choices/",
        load_cedrics_article_choices,
        name="ajax_load_article_choices",
    ),
    path("test/", TestView.as_view(), name="test"),
    path("__debug__/", include(debug_toolbar.urls)),
    re_path(
        r"^mersenne_dashboard/published_articles",
        PublishedArticlesDashboardView.as_view(),
        name="published_articles",
    ),
    re_path(
        r"^mersenne_dashboard/created_volumes",
        CreatedVolumesDashboardView.as_view(),
        name="created_volumes",
    ),
    path("article-list/<path:aid>/<path:year>", ArticleListView.as_view(), name="article-list"),
    path("volume-list/<path:aid>/<path:year>", VolumeListView.as_view(), name="volume-list"),
    path("invitations/", include(invitations_urls, namespace="invitations")),
    path("tasks/", TasksView.as_view(), name="tasks"),
]

urlpatterns_staff += oai_urls.urlpatterns
urlpatterns_staff += history_urls.urlpatterns
urlpatterns_staff += pubmed_urls.urlpatterns
urlpatterns_staff += urls.urlpatterns_protectable_by_account
urlpatterns_staff += urls.admin_urlpatterns
urlpatterns_staff += cms_urls.urlpatterns_staff

urlpatterns_editors = [
    path("collection/<path:pid>/", CollectionDetail.as_view(), name="collection-detail"),
    path(
        "api/deploy_jats_resource/<path:colid>/<path:pid>/<path:site>/",
        DeployJatsResourceAPIView.as_view(),
        name="api_deploy_jats_resource",
    ),
    path(
        "api/delete_jats_issue/<path:colid>/<path:pid>/<path:site>/",
        DeleteJatsIssueAPIView.as_view(),
        name="api_delete_jats_issue",
    ),
    re_path(
        r"^api/deploy_cms/(?P<colid>[A-Z].+)/(?P<site>.+)$",
        DeployCMSAPIView.as_view(),
        name="api_deploy_cms",
    ),
    re_path(
        r"^editor_volume_items/(?P<colid>[A-Z].+)/(?P<vid>.+)/$",
        EditorialToolsVolumeItemsView.as_view(),
        name="editor_volume_items",
    ),
    re_path(
        r"^editor_article/(?P<colid>[A-Z].+)/(?P<doi>10[.][0-9]{4,}.+)/$",
        EditorialToolsArticleView.as_view(),
        name="editor_article",
    ),
    re_path(
        r"^suggest_update/(?P<colid>[A-Z].+)/(?P<doi>10[.][0-9]{4,}.+)/$",
        SuggestUpdateView.as_view(),
        name="suggest_update",
    ),
    re_path(
        r"^suggest_deploy/(?P<colid>[A-Z].+)/(?P<doi>10[.][0-9]{4,}.+)/(?P<site>.+)/$",
        SuggestDeployView.as_view(),
        name="suggest_deploy",
    ),
    re_path(
        r"^graphical_abstract_update/(?P<colid>[A-Z].+)/(?P<doi>10[.][0-9]{4,}.+)/$",
        GraphicalAbstractUpdateView.as_view(),
        name="graphical_abstract_update",
    ),
    re_path(
        r"^graphical_abstract_deploy/(?P<colid>[A-Z].+)/(?P<doi>10[.][0-9]{4,}.+)/(?P<site>.+)/$",
        GraphicalAbstractDeployView.as_view(),
        name="graphical_abstract_deploy",
    ),
    re_path(
        r"^virtual_issue_create/(?P<colid>[A-Z].+)/$",
        VirtualIssueCreateView.as_view(),
        name="virtual_issue_create",
    ),
    re_path(
        r"^virtual_issue_update/(?P<colid>[A-Z].+)/(?P<pid>.+)/$",
        VirtualIssueUpdateView.as_view(),
        name="virtual_issue_update",
    ),
    re_path(
        r"^virtual_issue_parse/(?P<colid>[A-Z].+)/(?P<pid>.+)/$",
        VirtualIssueParseView.as_view(),
        name="virtual_issue_parse",
    ),
    re_path(
        r"^virtual_issue_deploy/(?P<colid>[A-Z].+)/(?P<pid>.+)/$",
        VirtualIssueDeployView.as_view(),
        name="virtual_issue_deploy",
    ),
    re_path(
        r"^virtual_issues/(?P<colid>[A-Z].+)/$",
        VirtualIssuesIndex.as_view(),
        name="virtual_issues_index",
    ),
    re_path(
        r"^special_issues/(?P<colid>[A-Z].+)/$",
        SpecialIssuesIndex.as_view(),
        name="special_issues_index",
    ),
    re_path(
        r"^special_issue_edit_api/(?P<colid>[A-Z].+)/(?P<pk>.+)/$",
        SpecialIssueEditAPIView.as_view(),
        name="special_issue_edit_api",
    ),
    re_path(
        r"^special_issue_delete/(?P<colid>[A-Z].+)/(?P<pk>.+)/$",
        SpecialIssueEditAPIView.as_view(),
        name="special_issue_delete",
    ),
    re_path("page/(?P<colid>[A-Z].+)/add/$", PageCreateView.as_view(), name="page_create"),
    re_path(
        "page/(?P<colid>[A-Z].+)/(?P<pk>.+)/delete/$",
        PageDeleteView.as_view(),
        name="page_delete",
    ),
    re_path(
        "special_issue_edit/(?P<colid>[A-Z].+)/(?P<pk>.+)/$",
        SpecialIssueEditView.as_view(),
        name="special-issue-edit",
    ),
    re_path("page/(?P<colid>[A-Z].+)/(?P<pk>.+)/$", PageUpdateView.as_view(), name="page_update"),
    re_path("page/(?P<colid>[A-Z].+)/$", PageIndexView.as_view(), name="page_index"),
    re_path("news/(?P<colid>[A-Z].+)/add/$", NewsCreateView.as_view(), name="news_create"),
    re_path(
        "news/(?P<colid>[A-Z].+)/(?P<pk>.+)/delete/$",
        NewsDeleteView.as_view(),
        name="news_delete",
    ),
    re_path("news/(?P<colid>[A-Z].+)/(?P<pk>.+)/$", NewsUpdateView.as_view(), name="news_update"),
    path("", include(matching_urls)),
]
urlpatterns_editors += cms_urls.urlpatterns_editors

urlpatterns = [
    path("", PTFToolsHomeView.as_view(), name="home"),
]

for urlpattern in urlpatterns_staff:
    urlpattern.callback = staff_member_required(urlpattern.callback)
    urlpatterns.append(urlpattern)

for urlpattern in urlpatterns_editors:
    urlpattern.callback = login_required(urlpattern.callback)
    urlpatterns.append(urlpattern)

urlpatterns += urls.urlpatterns_protectable_by_ip

urlpatterns += [
    re_path(r"^admin/", ptf_admin.urls),
    # Block the unused views of allauth
    path("accounts/email/", view_404, name="tools_account_email"),
    path("accounts/confirm-email/", view_404, name="tools_account_email_verification_sent"),
    re_path(r"^accounts/confirm-email/.*", view_404, name="tools_account_confirm_email"),
    path("accounts/", include(account_urls)),
    re_path(
        r"^api/deploy_translated_article/(?P<lang>[a-z]+)/(?P<doi>.+)/$",
        DeployTranslatedArticleAPIView.as_view(),
        name="api_deploy_trans_article",
    ),
    path("update_texmf_zip/", UpdateTexmfZipAPIView.as_view(), name="api_update_texmf_zip"),
    path("", include(comments_urls)),
]

urlpatterns += [
    re_path(
        r"^ckeditor/upload/(?P<colid>[A-Z].+)",
        login_required(file_upload_in_collection),
        name="ckeditor_upload_in_collection",
    ),
    re_path(
        r"^ckeditor/browse/(?P<colid>[A-Z].+)",
        never_cache(login_required(file_browse_in_collection)),
        name="ckeditor_browse_in_collection",
    ),
    re_path(r"^ckeditor/upload/", login_required(upload), name="ckeditor_upload"),
    path("ckeditor/", include("ckeditor_uploader.urls")),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(
    "/mecanique/media/uploads/", document_root="/mersenne_test_data/media/CR/uploads/"
)
urlpatterns += static(f"/mecanique{settings.MEDIA_URL}", document_root=settings.MEDIA_ROOT)
urlpatterns += static(
    "/biologies/media/uploads/", document_root="/mersenne_test_data/media/CR/uploads/"
)
urlpatterns += static(f"/biologies{settings.MEDIA_URL}", document_root=settings.MEDIA_ROOT)
urlpatterns += static(
    "/geoscience/media/uploads/", document_root="/mersenne_test_data/media/CR/uploads/"
)
urlpatterns += static(f"/geoscience{settings.MEDIA_URL}", document_root=settings.MEDIA_ROOT)
urlpatterns += static(
    "/chimie/media/uploads/", document_root="/mersenne_test_data/media/CR/uploads/"
)
urlpatterns += static(
    "/chimie/media/images/", document_root="/mersenne_test_data/media/CR/images/"
)
urlpatterns += static(f"/chimie{settings.MEDIA_URL}", document_root=settings.MEDIA_ROOT)
urlpatterns += static(
    "/mathematique/media/uploads/", document_root="/mersenne_test_data/media/CR/uploads/"
)
urlpatterns += static(f"/mathematique{settings.MEDIA_URL}", document_root=settings.MEDIA_ROOT)
urlpatterns += static(
    "/physique/media/uploads/", document_root="/mersenne_test_data/media/CR/uploads/"
)
urlpatterns += static(f"/physique{settings.MEDIA_URL}", document_root=settings.MEDIA_ROOT)

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
