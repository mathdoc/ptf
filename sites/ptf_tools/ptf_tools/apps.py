from django.apps import AppConfig


class PtfToolsConfig(AppConfig):
    name = "ptf_tools"

    def ready(self) -> None:
        from . import signals
