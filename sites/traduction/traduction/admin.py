from django.contrib import admin

from ptf.models import Article
from ptf.models import Collection
from ptf.models import Container
from ptf.models import TranslatedArticle

from .models import Translation

admin.site.register(Article)
admin.site.register(TranslatedArticle)
admin.site.register(Container)
admin.site.register(Collection)
admin.site.register(Translation)
