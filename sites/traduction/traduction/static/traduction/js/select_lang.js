//var langs;
//
//// get matecat supported languages and display them in the dropdown
//$.ajax({
//        url: '/api-matecat-lang',
//        dataType: "json",
//        success: function (result) {
//            console.log(result);
//            langs = result;
//
//            $.each(result, function (key, lang) {
//
//                $('#id_lang').append($('<option>', {
//                    value: lang.code,
//                    text : lang.name
//                }));
//            });
//
//        },
//        error: function (err) {
//            console.log(err);
//        }
//    });


// display only available languages

var wto;

function checkDoi(delay) {
    clearTimeout(wto);
    wto = setTimeout(function() {
        var doi = $('#id_doi').val().trim();
        if (doi.indexOf('10.5802/') == 0 && doi.indexOf('.', 7) > 9) {
            var suffix = doi.slice(8);
            var journal = suffix.split('.')[0];
            if (journal != "crgeos" && journal != 'crchim' && journal != 'crbiol') {
                $('#doi-error').html("Wrong DOI<br>Only articles from <i>Comptes Rendus Géoscience, Chimie</i> or <i>Biologies</i> can be translated at the moment</i>");
                $('#display_when_known_doi').hide();
            } else {
                var article_number = suffix.split('.')[1];
                if (article_number.length > 0 && article_number != '0' && isNaN(article_number)) {
                    $('#doi-error').html("Wrong DOI<br>'" + article_number + "' is not a number");
                    $('#display_when_known_doi').hide();
                } else if (article_number.length > 0) {
                    $.ajax({
                        url: '/api-article-lang/' + doi,
                        success: function (result) {
                            if (result.lang.length) {
                                $('#doi-error').html("");
                                $('option').prop('disabled', false);
                                $("select").val($("select option:first").val());
                                for (var i = 0; i < result.hide_langs.length; i++) {
                                    $('option[value="' + result.hide_langs[i] + '"]').prop('disabled', true);
                //                    $('option[value="' + result.hide_langs[i] + '"]').hide();
                                }

                                var elt = $('#vo-lang span');
                                elt.html(result.display_lang);
                                $('#display_when_known_doi').show();

                                if ( result.display_trans_langs.length ) {
                                    var elt = $('#other-lang-content');
                                    elt.html(result.display_trans_langs);
                                    $('#vo-lang-label').width("270");
                                    $('#other-lang').show();
                                } else {
                                    $('#vo-lang-label').width("170");
                                    $('#other-lang').hide();
                                }
                            } else {
                                $('#doi-error').html("Article not found");
                                $('#display_when_known_doi').hide();
                            }
                        },
                        error: function (err) {
                            $('#doi-error').html("Article not found");
                            $('#display_when_known_doi').hide();
                        }
                    });
                } else {
                    $('#doi-error').html("");
                    $('#display_when_known_doi').hide();
                }
            }
        } else if (doi.indexOf('10.') == 0 && doi.indexOf('/') > 0 ){
            $('#doi-error').html("Wrong DOI<br>It must start with '10.5802/'");
            $('#display_when_known_doi').hide();
        } else {
            $('#doi-error').html("");
            $('#display_when_known_doi').hide();
        }
    }, delay);
}

$('#id_doi').on('input', function() {
    checkDoi(200);
});

$(document).ready(function(){
    checkDoi(0);
});

//function DisplayLang() {
//    var doi = $('#id_doi').val();
//    console.log('doi : ' + doi);
//    var lang_vo;
//    var lang_trans = [];
//
//    $.ajax({
//        url: '/api-article-lang/' + doi,
//        success: function (result) {
//            console.log(result.lang);
//
//            $('option').show();
//            $('option').prop('disabled', false);
//
//            // hide the source language of the article
//            $.each(langs, function (key, lang) {
//                var code = lang.code.split('-')[0];
//                if (result.lang == code){
//                    $('option[value="' + lang.code + '"]').prop('disabled', true);
//                    $('option[value="' + lang.code + '"]').hide();
//                    lang_vo = lang.name;
//                }
//            });
//
//            // disable the selection of languages in which the article was already translated
//            for (var i in result.trans_lang) {
//                console.log('trans_lang : ' + result.trans_lang[i]);
//
//                $.each(langs, function (key, lang) {
//                    var code = lang.code.split('-')[0];
//                    if (result.trans_lang[i] == code){
//                        $('option[value="' + lang.code + '"]').prop('disabled', true);
//                        lang_trans.push("<strong>" + lang.name + "</strong>");
//                    }
//                });
//            }
//
//            var msg_lang = "Note : This article (" + doi + ") is written in <strong>" + lang_vo + "</strong>";
//
//            if (lang_trans.length > 0) {
//                var lang_str = lang_trans.join(', ');
////                for (let i; i < lang_trans.length; i++) {
////                    if (i==0){
////                        lang_str = lang_trans[0];
////                    }
////                    else if (i == lang_trans.length - 1) {
////                        lang_str = lang_str + " and " + lang_trans[i];
////                    }
////                    else {
////                        lang_str = lang_str + ", " + lang_trans[i];
////                    }
////                }
//
//                msg_trans = " and has been translated in " + lang_str;
//                msg_lang = msg_lang + msg_trans;
//            }
//
//            $("#article_langs").html(msg_lang + '.');
//
//
//            console.log(result);
//
//        },
//        error: function (err) {
//            $('option').show();
//            $('option').prop('disabled', false);
//            console.log(err);
//        }
//    });
//}
