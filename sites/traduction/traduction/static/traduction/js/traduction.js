$(document).ready(function(){

    lightbox(".lightbox", {
        close: true,
        closeText: "x",
        keyboard: true,
        zoom: true,
        zoomText: "<span class='glyphicon glyphicon-zoom-in'><span>",
        docClose: true,
    });

    $('.long-action-with-progress').click(function() {
        if ($('#checkbox_author').is(':checked')) {
            $('#loadingModal').show();
            $('.progress').show();
            $('.start').hide();
            // setInterval(GetProgress, 500);
        }
    });

    $('.long-action').click(function() {
        $('#loadingModal').show();
        $('.long-action').hide();
    });

    $('.submit-form-with-popup').click(function(){
        var data = $(this).attr("data-form");
        $('#is-submit').val("submit");
        console.log("submit", data)
        $('#' + data).submit();
    });

//    $('.nav-link').click(function(){
//        $(this).attr('class', 'nav-link active');
//    })

    $(".trans_text").scroll(function () {
        if ($("#synchScroll").is(":checked")) {
            $(".rev_text").scrollTop($(".trans_text").scrollTop());
        }
    });

    $(".rev_text").scroll(function () {
        if ($("#synchScroll").is(":checked")) {
            $(".trans_text").scrollTop($(".rev_text").scrollTop());
        }
    });

    $(".redirect-button").click(function() {
        var url = $(this).attr("data-url");
        location.href = url;
    });

});


$(window).on("beforeunload", function(e){
      var elt = document.getElementById("id-save");
      if (elt) {
          var saved = elt.classList.contains("btn-needs-save");
          if (saved) {
            // Custom messages won't get displayed anymore by most modern browsers with the "beforeunload" event
            if (confirm('Unsaved Data will be lost. Do you want to Continue ?')) {
                return true;
            }
            e.preventDefault();
            return false;
          }
      }
});

