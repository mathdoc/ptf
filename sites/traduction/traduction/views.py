import io
import os
import re
import time

import requests
from braces.views import CsrfExemptMixin
from braces.views import StaffuserRequiredMixin
from bs4 import BeautifulSoup
from langcodes import Language

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.core.files.temp import NamedTemporaryFile
from django.core.mail import EmailMultiAlternatives
from django.http import Http404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.shortcuts import render
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils import timezone
from django.utils.html import strip_tags
from django.views.generic import CreateView
from django.views.generic import FormView
from django.views.generic import TemplateView
from django.views.generic import UpdateView
from django.views.generic import View

from ptf import model_data_converter
from ptf import model_helpers
from ptf.cmds import ptf_cmds
from ptf.cmds import xml_cmds
from ptf.cmds.xml.ckeditor.ckeditor_parser import CkeditorParser
from ptf.cmds.xml.jats.builder.issue import get_title_xml
from ptf.model_data import Foo
from ptf.model_data import create_articledata
from ptf.model_data import create_contributor
from ptf.model_data import create_issuedata
from ptf.model_data import create_publisherdata
from ptf.models import Article
from ptf.site_register import SITE_REGISTER
from ptf.templatetags.helpers import get_flag_unicode
from ptf.views import ArticleEditAPIView
from ptf.views import detokenize
from ptf.views import get_tokenized

from .forms import TranslationForm
from .models import STATE_ACCEPTED
from .models import STATE_EDIT
from .models import STATE_PUBLISHED
from .models import STATE_SUBMITTED_TO_STAFF
from .models import Translation
from .models import is_state_lower


class UsersOfTranslationAuthMixin:
    """
    Authorize only a translation belonging to a user
    The staff is also authorized
    """

    def dispatch(self, request, *args, **kwargs):
        translation = get_object_or_404(Translation, pk=kwargs["pk"])

        if request.user.is_staff or request.user in translation.users.all():
            return super().dispatch(request, *args, **kwargs)
        else:
            raise PermissionDenied


class TraductionHomeView(TemplateView):
    """
    Home Page: CMS page if user is not logged in
               Redirect to another url based on the user role
    """

    template_name = "traduction/home.html"

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            if request.user.is_staff:
                return redirect(reverse("staff-home"))

            translations = request.user.translation_set.exclude(state=STATE_PUBLISHED).order_by(
                "-pk"
            )
            if translations.count() == 1:
                translation = translations.first()

                if translation.state == STATE_SUBMITTED_TO_STAFF:
                    return redirect(reverse("translation-posted", kwargs={"pk": translation.pk}))

                return redirect(
                    reverse(
                        "edit-article",
                        kwargs={
                            "pk": translation.pk,
                            "colid": translation.article.get_collection().pid,
                            "doi": translation.article.doi,
                        },
                    )
                )
            elif request.user.translation_set.all().exists():
                return redirect(reverse("translation-list"))
            return redirect(reverse("new-translation"))
        else:
            return super().get(request, *args, **kwargs)
            # return redirect(reverse('account_login'))


class StaffView(LoginRequiredMixin, StaffuserRequiredMixin, TemplateView):
    """
    Staff dashboard
    """

    template_name = "traduction/staff.html"
    raise_exception = True
    redirect_unauthenticated_users = True

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["submitted_translations"] = Translation.objects.filter(
            state=STATE_SUBMITTED_TO_STAFF
        )
        context["edit_translations"] = Translation.objects.filter(state=STATE_EDIT)
        context["accepted_translations"] = Translation.objects.filter(state=STATE_ACCEPTED)
        context["published_translations"] = Translation.objects.filter(
            state=STATE_PUBLISHED
        ).order_by("-article__date_published")

        return context


class NewTranslation(LoginRequiredMixin, CreateView, ArticleEditAPIView):
    """
    New translation page
    """

    template_name = "traduction/translation_form.html"
    model = Translation
    form_class = TranslationForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["doi"] = self.request.GET.get("doi")
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["show_breadcrumb"] = True
        context["state_num"] = 1  # For the breadcrumb
        return context

    def get_or_create_issue(self, collection, colid):
        now = timezone.now()
        curyear = now.year
        issue_pid = f"{colid}_{curyear}_1_1_1"
        issue = model_helpers.get_container(issue_pid)

        if issue is None:
            xissue = create_issuedata()
            xissue.pid = issue_pid
            xissue.ctype = "issue"
            xissue.year = str(curyear)
            xissue.last_modified_iso_8601_date_str = timezone.now().isoformat()

            xpublisher = create_publisherdata()
            xpublisher.name = "Académie des sciences"
            xissue.publisher = xpublisher

            cmd = ptf_cmds.addContainerPtfCmd({"pid": issue_pid, "xobj": xissue})

            cmd.add_collection(collection)
            cmd.set_provider(model_helpers.get_provider("mathdoc-id"))
            issue = cmd.do()

        return issue

    def replace_links(self, article):
        """
        Replace relative links in article.body_html with absolute ones to display figures in the text editor
        """
        colid = re.sub("[^a-zA-Z]+", "", article.doi)
        site_domain = "https://" + SITE_REGISTER[colid.lower()]["site_domain"]

        body = article.body_html
        soup = BeautifulSoup(body, "html.parser")
        for a in soup.select("a"):
            for img in a.select("img"):
                if a["href"].startswith("/"):
                    a["href"] = site_domain + a["href"]
                if img["src"].startswith("/"):
                    img["src"] = site_domain + img["src"]

        article.body_html = str(soup)

        return article

    def new_matecat_project(self, article, lang):
        now = timezone.now()
        now_str = now.strftime("%d/%m/%Y - %H:%M:%S")
        with open(
            os.path.join(settings.LOG_DIR, "traductions_new.log"), "a", encoding="utf-8"
        ) as file_:
            file_.write(f"{now_str} : {article.doi} {lang} - NEW\n")

        translation = Translation.objects.get(doi=article.doi, lang=lang)

        matecat_url = "https://www.matecat.com/"
        tokens_info = get_tokenized(article.body_html)

        # Creation of a Matecat project
        with NamedTemporaryFile("a+", encoding="utf-8", suffix=".html") as f:
            f.write(tokens_info["body"])
            f.seek(0)

            try:
                response = requests.post(
                    matecat_url + "api/v1/new",
                    files={"file": f},
                    data={
                        "project_name": article.pid + "-" + lang,
                        "source_lang": settings.TRANSLATION_LANGS[article.lang],
                        "target_lang": settings.TRANSLATION_LANGS[lang],
                    },
                    headers={"X-MATECAT-KEY": settings.MATECAT_API_KEY},
                    timeout=8,
                )
            except Exception:
                return "error", "error"

            if not 199 < response.status_code < 205:
                return "error", "error"

            response = response.json()

        now = timezone.now()
        now_str = now.strftime("%d/%m/%Y - %H:%M:%S")
        with open(
            os.path.join(settings.LOG_DIR, "traductions_new.log"), "a", encoding="utf-8"
        ) as file_:
            file_.write(
                f"{now_str} : {article.doi} {lang} - matecat/api/v1/new - {response['status']} \n"
            )

        project_id = response["id_project"]
        project_pass = response["project_pass"]

        translation.matecat_id = project_id
        translation.matecat_pwd = project_pass
        translation.save()

        # Waiting for Matecat to translate the text by checking the status
        timeout = time.time() + 120

        while True:
            time.sleep(3)
            r_status = requests.get(
                matecat_url + "api/status",
                params={"id_project": project_id, "project_pass": project_pass},
                headers={"X-MATECAT-KEY": settings.MATECAT_API_KEY},
                timeout=5,
            )
            print(matecat_url + "api/status", project_id, project_pass, r_status.status_code)
            if not 199 < r_status.status_code < 205:
                return "error", "error"
            r_status = r_status.json()
            print(r_status["status"])

            now = timezone.now()
            now_str = now.strftime("%d/%m/%Y - %H:%M:%S")
            if r_status["status"] == "DONE":
                with open(
                    os.path.join(settings.LOG_DIR, "traductions_new.log"), "a", encoding="utf-8"
                ) as file_:
                    file_.write(
                        f"{now_str} : {article.doi} {lang} - matecat/api/status - {r_status['status']} \n"
                    )

            if r_status["status"] != "FAIL":
                if "data" in r_status:
                    total_seg = r_status["data"]["summary"]["TOTAL_SEGMENTS"]
                    analyzed_seg = r_status["data"]["summary"]["SEGMENTS_ANALYZED"]
                else:
                    total_seg = r_status["summary"]["total_segments"]
                    analyzed_seg = r_status["summary"]["segments_analyzed"]

                print(str(analyzed_seg) + "/" + str(total_seg))
                print(r_status["status"])

                with open(
                    os.path.join(settings.LOG_DIR, "traductions_new.log"), "a", encoding="utf-8"
                ) as file_:
                    file_.write(
                        f"{now_str} : {article.doi} {lang} - matecat/api/status - {r_status['status']} {analyzed_seg}/{total_seg} \n"
                    )

                if r_status["status"] == "DONE" or (analyzed_seg == total_seg and total_seg != 0):
                    break

                # if matecat still hasn't started the translation : returns error
                if time.time() > timeout and total_seg == 0:
                    return "error", "error"

        # Getting Matecat translated text
        r_project = requests.get(matecat_url + f"api/v2/projects/{project_id}/{project_pass}")

        now = timezone.now()
        now_str = now.strftime("%d/%m/%Y - %H:%M:%S")
        with open(
            os.path.join(settings.LOG_DIR, "traductions_new.log"), "a", encoding="utf-8"
        ) as file_:
            file_.write(
                f"{now_str} : {article.doi} {lang} - matecat/api/v2/projects - {r_project.status_code} \n"
            )

        r_project = r_project.json()
        job = r_project["project"]["jobs"][0]
        job_id = job["id"]
        job_pass = job["password"]

        download_url = requests.get(matecat_url + f"translation/{job_id}/{job_pass}")
        matecat_trans_text = download_url.text

        return matecat_trans_text, tokens_info["tokens"]

    def form_valid(self, form):
        doi = form.cleaned_data["doi"]
        lang = form.cleaned_data["lang"]

        collections = ["CRBIOL", "CRGEOS", "CRCHIM"]
        for colid in collections:
            if colid.lower() in doi:
                break
        collection = model_helpers.get_collection(colid, sites=False)

        issue = self.get_or_create_issue(collection, colid)

        # 1. Export the original article XML from the production website (if needed)
        #    and create an Article in the traduction db
        article = model_helpers.get_article_by_doi(doi=doi)
        if article is None:
            website = collection.website()
            url = os.path.join(website, "api-item-xml", doi)
            response = requests.get(url)
            if response.status_code != 200:
                raise Http404
            xml_body = response.content.decode("utf-8")

            cmd = xml_cmds.addArticleXmlCmd({"body": xml_body, "issue": issue, "standalone": True})
            cmd.set_collection(collection)
            article = cmd.do()

        # 2. Augment the article with a new (empty) translation
        xtranslation_pid = article.pid + "-" + lang
        xtranslation_doi = article.doi + "-" + lang
        trans_article = model_helpers.get_article(pid=xtranslation_pid)
        if trans_article is not None:
            # TODO: display a nice error message (see Django docs on how to write a custom 400.html)
            return HttpResponse(status=400)

        xtranslation = create_articledata()
        xtranslation.pid = xtranslation_pid
        xtranslation.lang = lang
        xtranslation.ids.append(("doi", xtranslation_doi))
        xtranslation.doi = xtranslation_doi
        # xtranslation.title_tex = article.title_tex
        user = create_contributor()
        user["email"] = self.request.user.email
        user["role"] = "translator"
        user["corresponding"] = True
        xtranslation.contributors.append(user)

        article_data = model_data_converter.db_to_article_data(article)
        article_data.translations.append(xtranslation)
        article_data = self.replace_links(article_data)

        # addArticleXmlCmd is going to remove/recreate the article
        # All existing translations will have their article set to None
        # Get the existing_translations and update them after addArticleXmlCmd
        existing_translations = list(Translation.objects.filter(article=article))

        cmd = xml_cmds.addArticleXmlCmd(
            {"xarticle": article_data, "use_body": False, "issue": issue, "standalone": True}
        )
        cmd.set_collection(collection)
        article = cmd.do()

        # addArticleXmlCmd had removed/recreated the article
        # All existing translations now have their article set to None
        # Re set the article
        for translation in existing_translations:
            translation.article = article
            translation.save()

        # 3. Create a Translation
        obj, created = Translation.objects.get_or_create(doi=doi, article=article, lang=lang)
        obj.users.add(self.request.user)
        obj.save()
        obj.check_validity(article_data)

        # 4. Call Matecat API to get a pre-translated version of the text
        matecat_text, tokens = self.new_matecat_project(article, lang)
        # matecat_text = "Texte traduit... ou presque"
        # tokens = {}

        if matecat_text == "error":
            messages.error(
                self.request, "A problem occurred with Matecat. Please try again later."
            )

            translation = obj
            for trans_article in article.translations.all():
                if trans_article.lang == translation.lang:
                    trans_article.delete()
            # article.delete()
            translation.delete()

            return redirect(reverse("new-translation"))

        now = timezone.now()
        now_str = now.strftime("%d/%m/%Y - %H:%M:%S")
        with open(
            os.path.join(settings.LOG_DIR, "traductions_new.log"), "a", encoding="utf-8"
        ) as file_:
            file_.write(f"{now_str} : {article.doi} {lang} - Matecat is done\n")

        trans_text = detokenize(matecat_text, tokens)
        trans_text = re.sub(
            r"\|\|\|UNTRANSLATED_CONTENT_START\|\|\||\|\|\|UNTRANSLATED_CONTENT_END\|\|\||-ERR:REF-NOT-FOUND-",
            "",
            trans_text,
        )

        # strip control characters
        # trans_text = "".join(ch for ch in trans_text if unicodedata.category(ch)[0] != "C")

        #   -ERR:REF-NOT-FOUND-❑ --> usually happens when matecat wrongly segments a sentence with <a> </a> tags
        #       - the <a> tags are not retrieved
        #       - ex. '[Camanni et al. <a>2019</a>]' --> segment 1 = '[Camanni et al.' segment 2 = '2019]'

        for trans_article in article.translations.all():
            if trans_article.lang == xtranslation.lang:
                trans_article.body_html = trans_text
                trans_article.body_tex = trans_text
                trans_article.save()

        now = timezone.now()
        now_str = now.strftime("%d/%m/%Y - %H:%M:%S")
        with open(
            os.path.join(settings.LOG_DIR, "traductions_new.log"), "a", encoding="utf-8"
        ) as file_:
            file_.write(f"{now_str} : {article.doi} {lang} - End of form_valid, redirecting\n")

        return HttpResponseRedirect(
            reverse("edit-article", kwargs={"colid": colid, "doi": doi, "pk": obj.pk})
        )


def aboutView(request):
    return render(request, "traduction/about.html")


def legalView(request):
    return render(request, "traduction/legal.html")


def codeOfConductView(request):
    return render(request, "traduction/code_of_conduct.html")


def contactView(request):
    return render(request, "traduction/contact.html")


def faqView(request):
    return render(request, "traduction/faq.html")


def instructionsView(request):
    return render(request, "traduction/instructions.html")


class TranslationDeleteView(LoginRequiredMixin, UsersOfTranslationAuthMixin, View):
    raise_exception = True
    redirect_unauthenticated_users = True

    def post(self, request, *args, **kwargs):
        translation = Translation.objects.get(pk=kwargs["pk"])
        article = translation.article
        if article is not None:
            for trans_article in article.translations.all():
                if trans_article.lang == translation.lang:
                    trans_article.delete()
        # article.delete()
        translation.delete()

        if request.user.is_authenticated:
            if request.user.is_staff:
                return redirect(reverse("staff-home"))

        return redirect(reverse("translation-list"))


class ArticleEditView(LoginRequiredMixin, UsersOfTranslationAuthMixin, TemplateView):
    """
    Edit Article Metadata page
    The HTML page uses a vuejs widget developed outside this git
    """

    template_name = "article_edit.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        doi = kwargs.get("doi", None)
        translation = Translation.objects.get(pk=self.kwargs["pk"])
        context["colid"] = translation.get_colid()

        context["translation_id"] = translation.pk
        show_breadcrumb = is_state_lower(translation.state, STATE_SUBMITTED_TO_STAFF)
        disable_edit = not (is_state_lower(translation.state, STATE_SUBMITTED_TO_STAFF))
        article = translation.article

        # TODO: Since we got the Translation object just above, we directly have the article
        # => There is no need to call get_article_by_doi, and no need to call Translation.objects.get again below
        # article = model_helpers.get_article_by_doi(doi)
        # show_breadcrumb = False
        # disable_edit = True
        # if article:
        #     translation = Translation.objects.get(article=article)
        #     if translation:
        #         context['translation_id'] = translation.pk
        #         show_breadcrumb = is_state_lower(translation.state, STATE_PUBLISHED)
        #         disable_edit = not (is_state_lower(translation.state, STATE_PUBLISHED))

        context["article"] = article
        context["doi"] = doi
        context["state_num"] = 2  # For the breadcrumb
        context["current_page"] = "edit"
        context["show_breadcrumb"] = show_breadcrumb
        context["disable_edit"] = disable_edit
        context["language_code"] = settings.LANGUAGE_CODE
        return context


class ArticlePreView(ArticleEditView):
    """
    Removed
    Preview page using the ptf common templates
    """

    template_name = "article_preview.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["current_page"] = "preview"
        context["display_errors"] = self.request.user.is_staff
        context["bs_version"] = (
            settings.BOOTSTRAP_VERSION if hasattr(settings, "BOOTSTRAP_VERSION") else 3
        )

        article = context["article"]

        context["show_breadcrumb"] = True
        translation = Translation.objects.get(article=article)
        context["translation_ended"] = (
            not self.request.user.is_staff and is_state_lower(STATE_EDIT, translation.state)
        ) or (
            self.request.user.is_staff
            and is_state_lower(STATE_SUBMITTED_TO_STAFF, translation.state)
        )

        return context


class TranslationAcceptView(LoginRequiredMixin, StaffuserRequiredMixin, FormView):
    """
    Handle the post request of a staff member who accepts an article
    send an email to Mersenne
    """

    template_name = "traduction/staff.html"
    raise_exception = True
    redirect_unauthenticated_users = True

    def get_success_url(self):
        return reverse("staff-home")

    def post(self, request, *args, **kwargs):
        translation = Translation.objects.get(pk=kwargs["pk"])

        article = translation.article

        article.article_number = "XX"
        article.save()

        xml = ptf_cmds.exportPtfCmd(
            {
                "pid": article.pid,
                "with_body": False,
                "with_djvu": False,
                "article_standalone": True,
                "collection_pid": translation.get_colid(),
                "export_folder": "/var/log/mersenne",
            }
        ).do()
        xml_file = io.StringIO(xml)
        files = {"xml": xml_file}

        for trans_article in article.translations.all():
            if trans_article.lang == translation.lang:
                html_file = io.StringIO(trans_article.body_html)
                files["html"] = html_file

        with open(os.path.join(settings.LOG_DIR, "cmds_trad.log"), "w", encoding="utf-8") as file_:
            file_.write("XML and HTML files written\n")

        prefix = settings.PTF_TOOLS_URL
        url = f"{prefix}/upload/translated_article/{translation.lang}/{article.doi}/"
        try:
            response = requests.post(url, files=files, verify=False)
            status = response.status_code
        except requests.exceptions.ConnectionError:
            status = 503
            response = Foo()
            response.text = "Trammel is under maintenance. Please try again later."

        with open(os.path.join(settings.LOG_DIR, "cmds_trad.log"), "a", encoding="utf-8") as file_:
            file_.write(f"Trammel request sent. Status:{status}\n")

        if not (199 < status < 205):
            messages.error(request, response.text)

            with open(
                os.path.join(settings.LOG_DIR, "cmds_trad.log"), "a", encoding="utf-8"
            ) as file_:
                file_.write(f"Error: {response.text}\n")

        else:
            translation.state = STATE_ACCEPTED
            translation.save()

            with open(
                os.path.join(settings.LOG_DIR, "cmds_trad.log"), "a", encoding="utf-8"
            ) as file_:
                file_.write("Sending email\n")

            # send mail to Mersenne (cc users avec rôle Staff)
            self.send_mail_translation_accepted(translation)

        with open(os.path.join(settings.LOG_DIR, "cmds_trad.log"), "a", encoding="utf-8") as file_:
            file_.write("OK. Done\n")

        return redirect(self.get_success_url())

    def send_mail_translation_accepted(self, translation):
        subject = "A translation is accepted by editorial team"
        template = "mail/translation_accepted.html"
        to = [settings.CONTACT_MERSENNE]
        editorial_email = settings.CONTACT_TRADUCTION
        cc = [editorial_email]

        site_domain = SITE_REGISTER[translation.article.get_top_collection().pid.lower()][
            "site_domain"
        ]
        url = f"https://test-{site_domain}/articles/{translation.article.doi}/"

        html_content = render_to_string(
            template, {"translation": translation, "article_url": url}
        )  # render with dynamic value
        text_content = strip_tags(
            html_content
        )  # Strip the html tag. So people can see the pure text at least.
        # create the email, and attach the HTML version as well.
        msg = EmailMultiAlternatives(
            subject,
            text_content,
            editorial_email,
            to,
            cc=cc,
            headers={"Return-path": settings.RETURN_PATH},
        )
        msg.attach_alternative(html_content, "text/html")
        return msg.send(fail_silently=True)


class TranslationSubmitView(LoginRequiredMixin, UsersOfTranslationAuthMixin, UpdateView):
    """
    Last page of the submission "wizard"
    Enable/Disable the submit button based on the submission state
    send confirmation email on submit to author/cc: contact_pcj
    """

    model = Translation
    template_name = "traduction/translation_submit.html"
    fields = ("doi",)
    success_url = "/"

    def is_translated(self, field, lang):
        for obj in field:
            if obj.lang == lang:
                return True
        return False

    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)

        translation = Translation.objects.get(pk=kwargs["pk"])

        if "save_and_submit" in request.POST:
            translation.state = STATE_SUBMITTED_TO_STAFF
            translation.save()

            if request.user.is_staff:
                response = redirect(reverse("home"))
            else:
                # mail to staff and users (get_corresponding_emails)
                self.send_mail_ack_to_translator(translation)
                response = redirect(reverse("translation-posted", kwargs={"pk": kwargs["pk"]}))

        return response

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        translation = Translation.objects.get(pk=self.kwargs["pk"])
        article = Article.objects.get(doi=translation.doi)
        doi = translation.doi
        lang = translation.lang

        context["doi"] = doi
        context["colid"] = translation.get_colid
        context["article"] = article
        context["translation"] = translation
        context["trans_display_lang"] = Language.get(translation.lang).display_name()
        context["state_num"] = 3  # For the breadcrumb
        context["current_page"] = "finalize"
        context["translation_id"] = translation.pk
        context["show_breadcrumb"] = True

        context["translation_ended"] = (
            not self.request.user.is_staff and is_state_lower(STATE_EDIT, translation.state)
        ) or (
            self.request.user.is_staff
            and is_state_lower(STATE_SUBMITTED_TO_STAFF, translation.state)
        )

        empty_fields = []
        abstracts = article.get_abstracts()
        kwds = article.get_kwds_by_type()[1]
        t_kwds = article.get_kwds_by_type()[2]

        # check empty fields
        for trans_article in article.translations.all():
            if trans_article.lang == lang:
                trans_abstracts = trans_article.get_abstracts()
                trans_kwds = trans_article.get_kwds_by_type()[1]
                trans_contribs = trans_article.get_contributions("translator")

                if article.trans_lang != lang and not trans_article.title_tex:
                    empty_fields.append("title")

                if abstracts.exists() and abstracts.first().value_tex:
                    is_abstr_trans = self.is_translated(abstracts, lang)
                    if not is_abstr_trans and (
                        not trans_abstracts or not trans_abstracts.first().value_tex
                    ):
                        empty_fields.append("abstract")

                if kwds:
                    if not t_kwds and not trans_kwds:
                        empty_fields.append("kwds")
                    # elif (not t_kwds and (
                    #     len(trans_kwds) != len(kwds))):  # the user translated the keywords but not all of them
                    #     empty_fields.append('kwds_len')

                if not trans_article.body_html:
                    empty_fields.append("full_text")

                if trans_contribs:
                    for contrib in trans_contribs:
                        if not contrib.first_name or not contrib.last_name or not contrib.email:
                            empty_fields.append("contrib")
                else:
                    empty_fields.append("contrib")

                for field in ["title", "abstract", "kwds", "kwds_len", "full_text"]:
                    if field in empty_fields:
                        empty_fields.append("metadata")
                        break

        context["empty_fields"] = empty_fields

        return context

    def send_mail_ack_to_translator(self, translation):
        # mail sent to the translator(s)
        subject = "Confirmation of a new translation"
        template = "mail/translation_ack.html"
        to = translation.get_corresponding_emails()
        editorial_email = settings.CONTACT_TRADUCTION
        cc = [editorial_email]

        html_content = render_to_string(
            template, {"translation": translation, "editorial_email": editorial_email}
        )  # render with dynamic value
        text_content = strip_tags(
            html_content
        )  # Strip the html tag. So people can see the pure text at least.
        # create the email, and attach the HTML version as well.
        msg = EmailMultiAlternatives(
            subject,
            text_content,
            editorial_email,
            to,
            cc=cc,
            headers={"Return-path": settings.RETURN_PATH},
        )
        msg.attach_alternative(html_content, "text/html")

        return msg.send(fail_silently=False)


class TranslationPostedView(LoginRequiredMixin, UsersOfTranslationAuthMixin, TemplateView):
    """
    Page displayed to the user when the translation has been posted
    """

    template_name = "traduction/translation_posted.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        pk = kwargs.get("pk", None)
        translation = Translation.objects.get(pk=pk)
        context["colid"] = translation.get_colid()
        context["translation"] = translation
        context["translation_id"] = translation.pk

        context["doi"] = translation.article.doi
        context["editorial_email"] = settings.CONTACT_TRADUCTION

        context["state_num"] = 4  # For the breadcrumb
        context["current_page"] = "posted"
        context["disable_edit"] = True
        context["translation_ended"] = True
        context["show_breadcrumb"] = True

        return context


class ArticleInTranslationEditAPIView(CsrfExemptMixin, ArticleEditAPIView):
    """
    API to get/post article metadata
    The class is derived from ArticleEditAPIView (see ptf.views) to keep the submission up to date
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.translations = []
        self.fields_to_update = ["translations"]

    def convert_data_for_editor(self, data_article):
        # The vuejs component works with a data_article.translated_article

        super().convert_data_for_editor(data_article)
        data_article.is_staff = self.request.user.is_staff
        pk = self.kwargs["pk"]
        data_article.next_url = reverse("translation-submit", kwargs={"pk": pk})

        translation = Translation.objects.get(pk=self.kwargs["pk"])
        for trans_data_article in data_article.translations:
            if trans_data_article.lang == translation.lang:
                # trans_data_article.title_tex = trans_data_article.title_tex.replace('\n',  '')
                # self.replace_return(trans_data_article.abstracts[0], 'value_tex')

                # abstract = trans_data_article.abstracts[0]
                # abstract['value_tex'] = abstract['value_tex'].replace('\n', '')
                # strip control characters
                # abstract['value_tex'] = "".join(ch for ch in abstract['value_tex'] if unicodedata.category(ch)[0] != "C")

                # trans_data_article.body_html = trans_data_article.body_html.replace('\n', '')
                # strip control characters
                # trans_data_article.body_html = "".join(ch for ch in trans_data_article.body_html if unicodedata.category(ch)[0] != "C")

                data_article.translated_article = trans_data_article
                data_article.translated_article.is_editable = True
                if (
                    not data_article.is_staff and translation.state == STATE_SUBMITTED_TO_STAFF
                ) or (
                    data_article.is_staff and not is_state_lower(translation.state, STATE_ACCEPTED)
                ):
                    data_article.translated_article.is_editable = False

                for contrib in trans_data_article.contributors:
                    contrib["address_text"] = "\n".join(
                        [address for address in contrib["addresses"]]
                    )

        # The vuejs article pushes the updated translation in its post (see Article.vue, saveForm)
        # Empty the translations so that we can easily get the translated version in convert_data_from_editor
        data_article.translations = []

        data_article.vo_display_lang = (
            get_flag_unicode(data_article.lang)
            + "&nbsp;&nbsp;"
            + Language.get(data_article.lang).display_name()
        )
        data_article.trans_display_lang = (
            get_flag_unicode(translation.lang)
            + "&nbsp;&nbsp;"
            + Language.get(translation.lang).display_name()
        )

    def convert_data_from_editor(self, data_article):
        super().convert_data_from_editor(data_article)

        # convert_data_for_editor sent an empty translations list
        # The vuejs component added the new translation before the post (see Article.vue, saveForm)
        # We need to get the existing other translations from the database (other lang) and append the new translation

        new_translation = data_article.translations[0]

        # test_ft = [str(ord(ch)) for ch in new_translation.body_html if unicodedata.category(ch)[0] == "C"]

        xtitle = CkeditorParser(
            html_value=new_translation.title_tex,
            mml_formulas=data_article.trans_title_formulas,
            ignore_p=True,
        )
        new_translation.title_html = xtitle.value_html
        new_translation.title_tex = xtitle.value_tex
        new_translation.title_xml = get_title_xml(
            xtitle.value_xml, "", new_translation.lang, with_tex_values=False
        )

        for contrib in new_translation.contributors:
            contrib["addresses"] = []
            if "address_text" in contrib:
                contrib["addresses"] = contrib["address_text"].split("\n")

        if len(new_translation.abstracts) > 0:
            abstract = new_translation.abstracts[0]

            # abstract['value_tex'] = abstract['value_tex'].replace('\n', '')

            # strip control characters
            # test = [str(ord(ch)) for ch in abstract['value_tex'] if unicodedata.category(ch)[0] == "C"]

            xabstract = CkeditorParser(
                html_value=abstract["value_tex"], mml_formulas=data_article.trans_abstract_formulas
            )
            abstract["value_html"] = xabstract.value_html
            abstract["value_tex"] = xabstract.value_tex
            abstract[
                "value_xml"
            ] = f'<abstract xml:lang="{new_translation.lang}">{xabstract.value_xml}</abstract>'

        article = model_helpers.get_article_by_doi(data_article.doi)
        old_data_article = model_data_converter.db_to_article_data(article)

        new_translations = [
            translation
            for translation in old_data_article.translations
            if translation.lang != new_translation.lang
        ]
        new_translations.append(new_translation)

        data_article.translations = new_translations

    def save_data(self, data_article):
        # When the vuejs component is saved (post), a new Article is created (and the previous one is discarded)
        # (a new Article with its TranslatedArticle)
        # Save all the translations in self.translations

        article = model_helpers.get_article_by_doi(self.doi)
        self.translations = list(Translation.objects.filter(article=article))
        for translation in self.translations:
            translation.check_validity(data_article)

    def restore_data(self, article):
        # a new Article has been created. Update the translation at the end of the post.
        for translation in self.translations:
            translation.article = article
            translation.save()


class TranslationPublishView(LoginRequiredMixin, StaffuserRequiredMixin, FormView):
    """
    Handle the post request of a staff member who accepts an article
    send an email to Mersenne
    """

    template_name = "traduction/staff.html"
    raise_exception = True
    redirect_unauthenticated_users = True

    def get_success_url(self):
        return reverse("staff-home")

    def post(self, request, *args, **kwargs):
        translation = Translation.objects.get(pk=kwargs["pk"])
        article = translation.article

        # TODO: add API to PTF_TOOLS to publish the translation (doi/lang)
        prefix = settings.PTF_TOOLS_URL
        url = f"{prefix}/api/deploy_translated_article/{translation.lang}/{article.doi}/"

        try:
            response = requests.get(url, verify=False)
            status = response.status_code
        except requests.exceptions.ConnectionError:
            status = 503
            response = Foo()
            response.text = "Trammel is under maintenance. Please try again later."

        if not (199 < status < 205):
            messages.error(request, response.text)
        else:
            trans_article = translation.get_trans_article()
            today = model_helpers.parse_date_str(timezone.now().isoformat())
            trans_article.date_published = today
            trans_article.save()

            translation.state = STATE_PUBLISHED
            translation.save()

            self.send_mail_article_published_to_translator(translation)
            self.send_mail_article_published_to_author(translation)

        return redirect(self.get_success_url())

    def send_mail_article_published_to_translator(self, translation):
        subject = (
            "Your translation has been published in the French Academy of Sciences’ Comptes Rendus"
        )
        template = "mail/translation_published.html"
        to = translation.get_corresponding_emails()
        editorial_email = settings.CONTACT_TRADUCTION
        cc = [editorial_email]

        site_domain = SITE_REGISTER[translation.article.get_top_collection().pid.lower()][
            "site_domain"
        ]
        url = f"https://{site_domain}/articles/{translation.article.doi}/"

        html_content = render_to_string(
            template,
            {
                "translation": translation,
                "trans_display_lang": Language.get(translation.lang).display_name(),
                "trans_display_lang_fr": Language.get(translation.lang).display_name("fr"),
                "article_url": url,
            },
        )  # render with dynamic value
        text_content = strip_tags(
            html_content
        )  # Strip the html tag. So people can see the pure text at least.
        # create the email, and attach the HTML version as well.
        msg = EmailMultiAlternatives(
            subject,
            text_content,
            editorial_email,
            to,
            cc=cc,
            headers={"Return-path": settings.RETURN_PATH},
        )
        msg.attach_alternative(html_content, "text/html")
        return msg.send(fail_silently=True)

    def send_mail_article_published_to_author(self, translation):
        subject = "A translation of your article has been published in the French Academy of Sciences’ Comptes Rendus"
        template = "mail/translation_author.html"
        to = translation.get_authors_emails()
        editorial_email = settings.CONTACT_TRADUCTION
        cc = [editorial_email]

        site_domain = SITE_REGISTER[translation.article.get_top_collection().pid.lower()][
            "site_domain"
        ]
        url = f"https://{site_domain}/articles/{translation.article.doi}/"

        html_content = render_to_string(
            template,
            {
                "translation": translation,
                "trans_display_lang": Language.get(translation.lang).display_name(),
                "trans_display_lang_fr": Language.get(translation.lang).display_name("fr"),
                "article_url": url,
            },
        )  # render with dynamic value
        text_content = strip_tags(
            html_content
        )  # Strip the html tag. So people can see the pure text at least.
        # create the email, and attach the HTML version as well.
        msg = EmailMultiAlternatives(
            subject,
            text_content,
            editorial_email,
            to,
            cc=cc,
            headers={"Return-path": settings.RETURN_PATH},
        )
        msg.attach_alternative(html_content, "text/html")
        return msg.send(fail_silently=True)


class TranslationListView(LoginRequiredMixin, TemplateView):
    """
    User dashboard
    """

    model = Translation
    template_name = "traduction/user_dashboard.html"
    context_object_name = "translations"

    def get(self, request, *args, **kwargs):
        if request.user.translation_set.count() == 0:
            return redirect(reverse("new-translation"))

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user

        translations = user.translation_set.all()
        context["translations"] = translations
        context["colid"] = "CRGEOS"

        return context


class MatecatGetProgressAPIView(View):
    """
    Returns the progression of Matecat text translation
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.doi = None
        self.lang = None

    def get(self, *args, **kwargs):
        doi = kwargs.get("doi", None)
        lang = kwargs.get("lang", None)

        qs = Translation.objects.filter(doi=doi, lang=lang)

        if not qs.exists():
            return JsonResponse({"progress": "loading"})

        translation = qs.first()

        project_id = translation.matecat_id
        project_pass = translation.matecat_pwd

        matecat_url = "https://www.matecat.com/"

        r_status = requests.get(
            matecat_url + "api/status",
            headers={"X-MATECAT-KEY": settings.MATECAT_API_KEY},
            params={"id_project": project_id, "project_pass": project_pass},
        ).json()

        result = {"progress": "loading"}

        if "data" in r_status and "summary" in r_status["data"]:
            total_seg = r_status["data"]["summary"]["TOTAL_SEGMENTS"]
            analyzed_seg = r_status["data"]["summary"]["SEGMENTS_ANALYZED"]

            if total_seg > 0:
                progress = int(analyzed_seg * 100 / total_seg)
                result["progress"] = progress

        return JsonResponse(result)


class GetArticleLangAPI(View):
    def get(self, *args, **kwargs):
        doi = kwargs.get("doi", None)
        lang = display_lang = ""
        hide_langs = []
        display_trans_langs = []

        if doi.find("10.5802/") == 0:
            suffix = doi.split("/")[1]
            if suffix.find(".") > 0:
                key = suffix.split(".")[0]

                if key in settings.SITE_REGISTER:
                    site_domain = SITE_REGISTER[key]["site_domain"]

                    url = "https://" + site_domain + "/api-article-dump/" + doi
                    response = requests.get(url)
                    if response.status_code == 200:
                        data = response.json()
                        lang = data.get("lang", "")
                        display_lang = (
                            get_flag_unicode(lang)
                            + "&nbsp;&nbsp;"
                            + Language.get(lang).display_name()
                        )
                        hide_langs.append(lang)

                    translations = Translation.objects.filter(doi=doi)

                    for translation in translations:
                        hide_langs.append(translation.lang)
                        display_trans_langs.append(
                            get_flag_unicode(translation.lang)
                            + "&nbsp;&nbsp;"
                            + Language.get(translation.lang).display_name()
                        )

        return JsonResponse(
            {
                "lang": lang,
                "display_lang": display_lang,
                "hide_langs": hide_langs,
                "display_trans_langs": "<br>".join(display_trans_langs),
            }
        )


class TranslationRevertView(LoginRequiredMixin, StaffuserRequiredMixin, UpdateView):
    """
    Handle the post request of a staff member who reverts a translation (Accept -> Submitted)
    """

    template_name = "traduction/staff.html"
    raise_exception = True
    redirect_unauthenticated_users = True

    def post(self, request, *args, **kwargs):
        translation = Translation.objects.get(pk=kwargs["pk"])
        translation.state = STATE_SUBMITTED_TO_STAFF
        translation.save()

        return redirect(reverse("staff-home"))


class TranslationReopenView(LoginRequiredMixin, StaffuserRequiredMixin, UpdateView):
    """
    Handle the post request of a staff member who reverts a translation (Accept -> Submitted)
    """

    template_name = "traduction/staff.html"
    raise_exception = True
    redirect_unauthenticated_users = True

    def post(self, request, *args, **kwargs):
        translation = Translation.objects.get(pk=kwargs["pk"])
        translation.state = STATE_EDIT
        translation.save()

        return redirect(reverse("staff-home"))


class MatecatLangAPIView(View):
    """
    Get Matecat supported languages that use latin script with the library langcodes
    - included : some regional languages (Catalan, Welsh, ...), Creole languages
    - excluded : language variations (Belgium French, Canadian French -> French), languages whose code isn't available in the langcodes library (Breton 'br')
    """

    def get(self, request):
        url = "https://www.matecat.com/api/v2/languages"
        r = requests.get(url).json()
        lang_list = {}
        i = 0

        for lang in r:
            lang_short = lang["code"].split("-")[0]
            script = Language.make(lang_short).assume_script().script

            if (script == "Latn" and " " not in lang["name"]) or "Creole" in lang["name"]:
                lang_list[i] = lang
                i += 1

        return JsonResponse(lang_list)


class MatecatReversedTranslation(StaffView):
    """
    Page with the reversed translation of a text (translated lang > original lang)
    """

    template_name = "traduction/reversed_translation.html"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.pk = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        pk = kwargs.get("pk", None)
        translation = Translation.objects.get(pk=pk)
        article = translation.article
        trans_article = translation.get_trans_article()
        lang_src = translation.lang
        lang_trg = article.lang

        matecat_url = "https://www.matecat.com/"
        tokens_info = get_tokenized(trans_article.body_html)

        if lang_src == "error":
            messages.error(
                self.request, "A problem occurred with Matecat. Please try again later."
            )

            return redirect(reverse("staff-home"))

        # Creation of a Matecat project
        with NamedTemporaryFile("a+", encoding="utf-8", suffix=".html") as f:
            f.write(tokens_info["body"])
            f.seek(0)

            response = requests.post(
                matecat_url + "api/v1/new",
                files={"file": f},
                data={
                    "project_name": article.pid,
                    "source_lang": settings.TRANSLATION_LANGS[lang_src],
                    "target_lang": settings.TRANSLATION_LANGS[lang_trg],
                },
                headers={"X-MATECAT-KEY": settings.MATECAT_API_KEY},
                timeout=8,
            )

            response.raise_for_status()
            response = response.json()

        project_id = response["id_project"]
        project_pass = response["project_pass"]

        context["article"] = article
        context["trans_article"] = trans_article
        context["id"] = project_id
        context["pwd"] = project_pass
        context["pk"] = pk

        return context


class MatecatReversedProgressAPI(View):
    """
    Returns the progression of Matecat reversed text translation
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.id = None
        self.pwd = None

    def get(self, *args, **kwargs):
        id = kwargs.get("id", None)
        pwd = kwargs.get("pwd", None)

        matecat_url = "https://www.matecat.com/"

        r_status = requests.get(
            matecat_url + "api/status", params={"id_project": id, "project_pass": pwd}
        ).json()

        total_seg = r_status["data"]["summary"]["TOTAL_SEGMENTS"]
        analyzed_seg = r_status["data"]["summary"]["SEGMENTS_ANALYZED"]

        if total_seg > 0:
            progress = int(analyzed_seg * 100 / total_seg)

            if progress == 100:
                # MatecatReversedTranslationAPI is called by progressbar.js to get the translated text
                pass
                # r_project = requests.get(matecat_url + f"api/v2/projects/{id}/{pwd}").json()
                #
                # job = r_project["project"]["jobs"][0]
                # job_id = job["id"]
                # job_pass = job["password"]

                # download_url = requests.get(matecat_url + f"translation/{job_id}/{job_pass}")
                # matecat_trans_text = download_url.text

            return JsonResponse({"progress": progress})
        else:
            return JsonResponse({"progress": "loading"})


class MatecatReversedTranslationAPI(View):
    """
    Returns 2 text : translated text by contributor + reversed translated text by Matecat
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.id = None
        self.pwd = None
        self.pk = None

    def get(self, *args, **kwargs):
        id = kwargs.get("id", None)
        pwd = kwargs.get("pwd", None)
        pk = kwargs.get("pk", None)

        translation = Translation.objects.get(pk=pk)
        trans_article = translation.get_trans_article()
        tokens = get_tokenized(trans_article.body_html)["tokens"]
        matecat_url = "https://www.matecat.com/"

        r_project = requests.get(matecat_url + f"api/v2/projects/{id}/{pwd}").json()

        job = r_project["project"]["jobs"][0]
        job_id = job["id"]
        job_pass = job["password"]

        download_url = requests.get(matecat_url + f"translation/{job_id}/{job_pass}")
        matecat_text = download_url.text

        reversed_trans = detokenize(matecat_text, tokens)
        reversed_trans = re.sub(
            r"\|\|\|UNTRANSLATED_CONTENT_START\|\|\||\|\|\|UNTRANSLATED_CONTENT_END\|\|\||-ERR:REF-NOT-FOUND-",
            "",
            reversed_trans,
        )

        return JsonResponse({"trans_text": trans_article.body_html, "rev_text": reversed_trans})
