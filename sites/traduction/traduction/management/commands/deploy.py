import io

import requests

from django.conf import settings
from django.core.management.base import BaseCommand

from ptf.cmds import ptf_cmds
from traduction.models import Translation
from traduction.views import STATE_PUBLISHED


class Command(BaseCommand):
    help = "Re-publish a translation to the (test) website, without PDF recompilation"

    def add_arguments(self, parser):
        parser.add_argument(
            "-doi",
            action="store",
            dest="doi",
            type=str,
            default=None,
            required=False,
            help="Translation doi",
        )
        parser.add_argument(
            "-site",
            action="store",
            dest="site",
            type=str,
            default=None,
            required=True,
            help="website, test_website",
        )

    def deploy(self, translation, site):
        article = translation.article

        xml = ptf_cmds.exportPtfCmd(
            {
                "pid": article.pid,
                "with_body": False,
                "with_djvu": False,
                "article_standalone": True,
                "collection_pid": article.get_collection().pid,
                "export_folder": "/var/log/mersenne",
            }
        ).do()
        xml_file = io.StringIO(xml)
        files = {"xml": xml_file}

        date_published_str = None
        for trans_article in article.translations.all():
            if trans_article.lang == translation.lang:
                html_file = io.StringIO(trans_article.body_html)
                files["html"] = html_file
                if trans_article.date_published:
                    date_published_str = trans_article.date_published.strftime("%Y-%m-%d")

        prefix = settings.PTF_TOOLS_URL
        url = f"{prefix}/upload/translated_article/{translation.lang}/{article.doi}/"
        if date_published_str:
            url += f"?date_published={date_published_str}"
        response = requests.post(url, files=files, verify=False)

        if response.status_code < 300 and site == "website":
            url = f"{prefix}/api/deploy_translated_article/{translation.lang}/{article.doi}/"
            response = requests.get(url, verify=False)

        return response.status_code

    def handle(self, *args, **kwargs):
        doi = kwargs["doi"]
        site = kwargs["site"]

        qs = (
            Translation.objects.filter(state=STATE_PUBLISHED)
            if doi is None
            else Translation.objects.filter(article__translations__doi=doi, state=STATE_PUBLISHED)
        )

        for translation in qs:
            status_code = self.deploy(translation, site)
            print(translation.article.doi, status_code)
