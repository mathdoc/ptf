import os

from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.files.storage import FileSystemStorage
from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _

from ptf.models import Article

STATE_EDIT = "edit"
STATE_SUBMITTED_TO_STAFF = "submitted_to_staff"
STATE_ACCEPTED = "accepted"
STATE_PUBLISHED = "published"
SUBMISSION_STATES = (
    (STATE_EDIT, _("Edit")),
    (STATE_SUBMITTED_TO_STAFF, _("Submitted to Staff")),
    (STATE_ACCEPTED, _("Accepted")),
    (STATE_PUBLISHED, _("Published")),
)


def is_state_lower(state1, state2):
    states = [item[0] for item in SUBMISSION_STATES]
    return states.index(state1) < states.index(state2)


def translation_directory_path(translation, filename):
    return os.path.join(str(translation.pk), filename)


class OverwriteStorage(FileSystemStorage):
    def get_available_name(self, name, max_length=None):
        if self.exists(name):
            os.remove(os.path.join(settings.MEDIA_ROOT, name))
        return name


def validate_file_extension(value):
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = [".html"]
    if ext.lower() not in valid_extensions:
        raise ValidationError("Unsupported file extension.")


def is_article_valid(article_data):
    is_valid = True

    if is_valid and len(article_data.title_tex) == 0:
        is_valid = False

    # Disabled on 10/07/2021: for the opening, PCJ wants the references to be optional
    # if is_valid and len([ref for ref in article_data.bibitems if ref.type == 'unknown']) > 0:
    #    is_valid = False

    return is_valid


class Translation(models.Model):
    users = models.ManyToManyField(User)
    article = models.ForeignKey(Article, models.SET_NULL, blank=True, null=True)
    doi = models.CharField(max_length=64, db_index=True)
    lang = models.CharField(max_length=7, default="und")
    html = models.FileField(
        upload_to=translation_directory_path,
        blank=True,
        null=True,
        storage=OverwriteStorage(),
        validators=[validate_file_extension],
        max_length=255,
    )
    state = models.CharField(
        _("State"), default=STATE_EDIT, choices=SUBMISSION_STATES, blank=False, max_length=255
    )
    matecat_id = models.CharField(max_length=64, blank=True)
    matecat_pwd = models.CharField(max_length=64, blank=True)

    def __str__(self):
        users_str = " ".join([user.username for user in self.users.all()])
        return f"Users: {users_str}, DOI: {self.doi}"

    def get_colid(self):
        col = self.article.get_collection()
        return col.pid if col is not None else ""

    def get_trans_article(self):
        for trans_article in self.article.translations.all():
            if trans_article.lang == self.lang:
                return trans_article

    def check_validity(self, data_article):
        if is_state_lower(self.state, STATE_SUBMITTED_TO_STAFF):
            # is_valid = is_article_valid(data_article)
            state = STATE_EDIT
            self.state = state
            self.save()

    def get_corresponding_emails(self):
        emails = []
        if self.article is None:
            return emails

        trans_article = self.get_trans_article()
        author_contributions = trans_article.get_contributions("translator")

        for contribution in author_contributions:
            if contribution.corresponding and contribution.email:
                emails.append(contribution.email)

        first_user = self.users.first()
        if first_user.email and first_user.email not in emails:
            emails.append(first_user.email)

        return emails

    def corresponding_emails_display(self):
        emails = self.get_corresponding_emails()
        display = "\n".join(emails)

        return display

    def get_authors_emails(self):
        emails = []
        author_contributions = self.article.get_contributions("author")

        for contribution in author_contributions:
            if contribution.corresponding and contribution.email:
                emails.append(contribution.email)

        return emails


@receiver(pre_save, sender=User)
def update_username_from_email(sender, instance, **kwargs):
    """
    When a login with the Mathdoc authentification server needs the creation of a Django User,
    we set the username with the email
    """

    user_email = instance.email
    if not instance.username:
        username = user_email[:30]

        # In case we want to allow multiple accounts with the same email address
        n = 1
        while User.objects.exclude(pk=instance.pk).filter(username=username).exists():
            n += 1
            username = user_email[: (29 - len(str(n)))] + "-" + str(n)
        instance.username = username
