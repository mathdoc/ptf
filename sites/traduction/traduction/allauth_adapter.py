from allauth.socialaccount.adapter import DefaultSocialAccountAdapter

from django.contrib.auth.models import User


class CustomSocialAccountAdapter(DefaultSocialAccountAdapter):
    def pre_social_login(self, request, sociallogin):
        """
        We try to connect a new sociallogin to an existing Django User if any
        """

        # If the sociallogin already exists, we simply return
        if sociallogin.is_existing:
            return

        if sociallogin.email_addresses:
            email = sociallogin.email_addresses[0].email

            qs = User.objects.filter(email=email)
            if qs.exists():
                user = qs.first()
                sociallogin.connect(request, user)

    def is_auto_signup_allowed(self, request, sociallogin):
        return True
