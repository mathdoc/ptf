"""traduction URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include
from django.urls import path
from django.urls import re_path

from ptf import urls as ptf_urls
from ptf.urls import ptf_i18n_patterns
from ptf.views import ArticleEditAPIView
from traduction.views import ArticleEditView
from traduction.views import ArticleInTranslationEditAPIView
from traduction.views import ArticlePreView
from traduction.views import GetArticleLangAPI
from traduction.views import MatecatGetProgressAPIView
from traduction.views import MatecatLangAPIView
from traduction.views import MatecatReversedProgressAPI
from traduction.views import MatecatReversedTranslation
from traduction.views import MatecatReversedTranslationAPI
from traduction.views import NewTranslation
from traduction.views import StaffView
from traduction.views import TraductionHomeView
from traduction.views import TranslationAcceptView
from traduction.views import TranslationDeleteView
from traduction.views import TranslationListView
from traduction.views import TranslationPostedView
from traduction.views import TranslationPublishView
from traduction.views import TranslationReopenView
from traduction.views import TranslationRevertView
from traduction.views import TranslationSubmitView
from traduction.views import aboutView
from traduction.views import codeOfConductView
from traduction.views import contactView
from traduction.views import faqView
from traduction.views import instructionsView
from traduction.views import legalView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', TraductionHomeView.as_view(), name='home'),
    path('all-translations/', StaffView.as_view(), name='staff-home'),
    path('translation/', TranslationListView.as_view(), name='translation-list'),
    re_path(r'^translation/new', NewTranslation.as_view(), name='new-translation'),
    path('page/about/', aboutView, name='about'),
    path('page/legal/', legalView, name='legal'),
    path('page/faq/', faqView, name='faq'),
    path('page/contact/', contactView, name='contact'),
    path('page/code-of-conduct/', codeOfConductView, name='code-of-conduct'),
    path('page/instructions/', instructionsView, name='instructions'),
    path('', include(ptf_urls)),
    path('translation/<int:pk>/delete/', TranslationDeleteView.as_view(), name='translation-delete'),
    re_path(r'^article-edit/(?P<colid>[A-Z-]+)/(?P<doi>.+)/(?P<pk>\d+)/$', ArticleEditView.as_view(), name='edit-article'),
    re_path(r'^article-preview/(?P<colid>[A-Z-]+)/(?P<doi>.+)/(?P<pk>\d+)/$', ArticlePreView.as_view(), name='preview-article'),
    path('translation/<int:pk>/accept/', TranslationAcceptView.as_view(), name='translation-accept'),
    path('translation/<int:pk>/publish/', TranslationPublishView.as_view(), name='translation-publish'),
    path('translation/<int:pk>/submit/', TranslationSubmitView.as_view(), name='translation-submit'),
    path('translation/<int:pk>/posted/', TranslationPostedView.as_view(), name='translation-posted'),
    path('translation/<int:pk>/revert/', TranslationRevertView.as_view(), name='translation-revert'),
    path('translation/<int:pk>/reopen/', TranslationReopenView.as_view(), name='translation-reopen'),
    re_path(r'^api-article-edit/(?P<colid>[A-Z-]+)/(?P<doi>.+)/(?P<pk>\d+)/$', ArticleInTranslationEditAPIView.as_view(), name='api-edit-translation'),
    re_path(r'^api-article-edit/(?P<colid>[A-Z-]+)/(?P<doi>.+)/$', ArticleEditAPIView.as_view(), name='api-edit-article'),
    path('api-matecat-progress/<path:doi>/<path:lang>/', MatecatGetProgressAPIView.as_view(), name='api-matecat-progress'),
    path('api-article-lang/<path:doi>/', GetArticleLangAPI.as_view(), name='api-article-lang'),
    path('api-matecat-lang/', MatecatLangAPIView.as_view(), name='api-matecat-lang'),
    re_path(r'^translation/(?P<pk>.+)/reversed/', MatecatReversedTranslation.as_view(), name='reversed-translation'),
    path('api-matecat-progress-reversed/<path:id>/<path:pwd>/', MatecatReversedProgressAPI.as_view(), name='api-matecat-progress-reversed'),
    path('api-matecat-reversed-translation/<path:id>/<path:pwd>/<path:pk>/', MatecatReversedTranslationAPI.as_view(), name='api-matecat-reversed-trans'),
]

urlpatterns += ptf_i18n_patterns

urlpatterns += ptf_urls.admin_urlpatterns
urlpatterns += [
    path('accounts/', include('allauth.urls')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
