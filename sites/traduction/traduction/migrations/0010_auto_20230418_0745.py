# Generated by Django 3.2.16 on 2023-04-18 07:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('traduction', '0009_auto_20230216_1005'),
    ]

    operations = [
        migrations.AlterField(
            model_name='translation',
            name='lang',
            field=models.CharField(default='und', max_length=7),
        ),
        migrations.AlterField(
            model_name='translation',
            name='state',
            field=models.CharField(choices=[('edit', 'Edit'), ('submitted_to_staff', 'Submitted to Staff'), ('accepted', 'Accepted'), ('published', 'Published')], default='edit', max_length=255, verbose_name='State'),
        ),
    ]
