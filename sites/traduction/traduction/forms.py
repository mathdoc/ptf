from langcodes import Language
from unidecode import unidecode

from django import forms
from django.conf import settings

from ptf.cmds.xml.xml_utils import clean_doi
from ptf.display.resolver import find_id_type

from .models import Translation


def get_matecat_lang_choices():
    """
    return a list of language choices to be displayed in form
    Ex: [('da', 'Dansk'), ('de', 'Deutsch'), ('en', 'English'),...]
    """
    lang_objects = [Language.get(l) for l in settings.TRANSLATION_LANGS.keys()]
    lang_codes = [(l.language, l.autonym().capitalize()) for l in lang_objects]
    lang_codes.sort(key=lambda x: unidecode(x[1][0]))

    return lang_codes


class TranslationForm(forms.ModelForm):
    lang = forms.ChoiceField(choices=get_matecat_lang_choices())

    class Meta:
        model = Translation
        fields = ("doi", "lang")

    def __init__(self, *args, **kwargs):
        doi = kwargs.pop("doi")
        super().__init__(*args, **kwargs)

        self.fields["doi"].initial = doi if doi is not None else ""

        for field in self.fields.values():
            field.error_messages = {"required": f"The field {field.label} is required"}

    def clean_doi(self):
        doi = clean_doi(self.cleaned_data["doi"])
        id_type = find_id_type(doi)

        if id_type != "doi":
            raise forms.ValidationError("This field has to be a DOI")

        # Olivier: New translation: we only have a DOI at that point. The article has not been imported yet.

        # article = model_helpers.get_article_by_doi(doi=doi)
        # if not article:
        #     raise forms.ValidationError('No article found')

        # try:
        #     match = resolve(reverse('export_article_html',
        #                             kwargs = {'doi' : doi}))
        # except NoReverseMatch:
        #     raise forms.ValidationError('The DOI is incorrect')

        return doi

    def clean_lang(self):
        lang = self.cleaned_data["lang"]

        return lang
