from django.conf import settings
from django.conf.urls.static import static
from django.urls import include
from django.urls import path
from django.urls import re_path

from ahl import views
from mersenne_cms import urls as cms_urls
from mersenne_cms.admin import mersenne_admin
from oai import urls as oai_urls
from ptf import urls as ptf_urls
from ptf.urls import ptf_i18n_patterns
from upload import urls as upload_urls

urlpatterns = [
    path('articles/', views.ArticlesView.as_view(), name='articles'),
    # re_path(r'^coming-articles/$', NextVolumeView.as_view(), name='coming-articles'),
    path('bibtex/<path:aid>/', views.bibtex, name='bibtex'),
    path('movie/<path:id>/', views.movie, name='movie'),
    path('item/<path:pid>/', views.AHLItemView.as_view(), name='item_id'),
    path('articles/<path:pid>/', views.AHLItemView.as_view(), name='article_id'),
    path('search-form/', views.SearchView.as_view(), name='search-form'),
]

urlpatterns += [path('', include(cms_urls))]
urlpatterns += [path('', include(ptf_urls))]
urlpatterns += [path('', include(oai_urls))]

urlpatterns += ptf_i18n_patterns

if settings.ALLOW_ADMIN:
    urlpatterns += [
        path('upload/', include(upload_urls)),
        re_path(r'^admin/', mersenne_admin.urls),
    ]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

