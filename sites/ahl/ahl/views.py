import json

import requests

from django.http import Http404
from django.http import HttpResponse
from django.views.decorators.http import require_http_methods
from django.views.generic import TemplateView

from ptf import model_helpers
from ptf.views import ItemView


def get_movie_html(id):
    url = (
        "https://annales.lebesgue.fr/index.php/AHL/$$$call$$$/modals/video/video/get-html?movieId="
        + id
    )
    response = requests.get(url)
    body = ""
    if response.status_code == 200:
        body = response.text
    return body


class ArticlesView(TemplateView):
    template_name = "blocks/all-articles.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # collection = model_helpers.get_collection('AHL')
        # issue = collection.content.filter(sites__id=settings.SITE_ID).order_by('-seq').first()
        # articles = issue.article_set.all().order_by('-seq')
        articles = model_helpers.get_articles_by_published_date()[:15]

        for article in articles:
            article.title_html = article.title_html.replace("\u00A0", " ")
        context.update({"articles": articles})
        return context


class AHLItemView(ItemView):
    def get(self, request, *args, **kwargs):
        pid = kwargs["pid"]

        article = model_helpers.get_article(pid, prefetch=True)
        if article:
            related_objects = article.relatedobject_set.filter(rel="video")
            if related_objects.count() > 0:
                video_object = related_objects.first()
                id = video_object.location

                body = get_movie_html(id)
                d_ = json.loads(body)

                movie_html = d_["content"] if "content" in d_ else ""
                movie_html = movie_html.replace("autoplay", "")
                kwargs["movie_html"] = movie_html

        return super().get(request, **kwargs)


class SearchView(TemplateView):
    template_name = "blocks/search.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


@require_http_methods(["GET"])
def bibtex(request, aid, api=False):
    article = model_helpers.get_article(aid)
    if article is None:
        raise Http404

    content = article.get_bibtex(request)

    return HttpResponse(content, content_type="text/plain")


@require_http_methods(["GET"])
def movie(request, id, api=False):
    body = get_movie_html(id)

    return HttpResponse(body, content_type="text/plain")
