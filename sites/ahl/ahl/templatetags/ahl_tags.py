from django import template
from django.utils.translation import gettext_lazy as _

register = template.Library()


@register.simple_tag
def content_menus(max_volume=3):
    menus = list()

    # menus.append({'title': _('Recent articles'),
    #               'url': reverse('coming-issues')})
    #
    # try:
    #     collection = Collection.objects.get(pid=settings.COLLECTION_PID).cast()
    #     volumes = get_volumes_in_collection(
    #         collection)['sorted_issues'][0]['volumes'][:max_volume]
    # except Collection.DoesNotExist:
    #     volumes = list()
    #
    # for volume in volumes:
    #     menus.append(
    #         {
    #             'title': volume['fyear'],
    #             'url': reverse('volume-items', kwargs={'vid': volume['issues'][0].get_vid()})
    #         }
    #     )

    menus.append({"title": _("Older issues"), "url": "#"})
    menus.append({"title": _("Search"), "url": "#"})
    return menus
