from django.conf import settings

from oai import oai_helpers
from oai.repository import OAIRepository
from ptf import model_helpers
from ptf.models import Article


class PCJRepository(OAIRepository):
    def Identify(self):
        return {
            "name": "pcj",
            "base_url": "https://peercommunityjournal.org.org/oai",
            "protocol_version": "2.0",
            "admin_email": "mersenne@listes.mathdoc.fr",
            "earliest_datestamp": Article.objects.filter(sitemembership__site_id=settings.SITE_ID)
            .exclude(classname="TranslatedArticle")
            .order_by("date_published")
            .first()
            .date_published.strftime("%Y-%m-%d"),
            "deleted_record": "no",
            "granularity": "YYYY-MM-DD",
            "repositoryIdentifier": "centre-mersenne.org",
            "delimiter": ":",
            "sample_identifier": "oai:centre-mersenne.org:10_24072_pcjournal_52",
        }

    def has_set(self, setspec):
        col = model_helpers.get_collection(setspec)
        return bool(col)

    def listsets(self):
        collections = oai_helpers.get_collections()
        return [collections]


def get_repository(base_url):
    # thismodule = sys.modules[__name__]
    # repo = settings.REPOSITORIES[base_url]
    # repoClass = getattr(thismodule, repo)
    return PCJRepository(base_url)
