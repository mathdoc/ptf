"""
Django settings for pcj project.

Generated by 'django-admin startproject' using Django 1.10.7.

For more information on this file, see
https://docs.djangoproject.com/en/1.10/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.10/ref/settings/
"""

import os
import socket
import sys

from django.utils.translation import gettext_lazy as _

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PTF_DIR = os.path.dirname(os.path.dirname(BASE_DIR))
APPS_DIR = "%s/apps" % PTF_DIR
BIN_DIR = "%s/bin" % PTF_DIR
sys.path.append(APPS_DIR)
sys.path.append(BIN_DIR)

from ptf.site_register import SITE_REGISTER

SITE_NAME = "pcj"
SITE_ID = SITE_REGISTER[SITE_NAME]["site_id"]
SITE_DOMAIN = SITE_REGISTER[SITE_NAME]["site_domain"]
COLLECTION_PID = SITE_REGISTER[SITE_NAME]["collection_pid"]

HAS_ISSUE_INSIDE_VOLUME = True

# Application definition

INSTALLED_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.sites",
    "django_extensions",
    "modeltranslation.apps.ModeltranslationConfig",
    "pcj",
    "mersenne_cms",
    "ptf",
    "oai",
    "ckeditor",
    "ckeditor_uploader",
    "upload",
    "django.contrib.admin",
]

LANGUAGES = (("en", _("English")),)

# Internationalization
# https://docs.djangoproject.com/en/1.10/topics/i18n/

TIME_ZONE = "UTC"
USE_I18N = True
USE_TZ = True
LANGUAGE_CODE = "en"

ROOT_URLCONF = "pcj.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [
            "%s/pcj/templates" % BASE_DIR,
            "%s/ptf/templates/ptf/bs5" % APPS_DIR,
            "%s/oai/templates" % APPS_DIR,
            # '%s/mersenne_cms/templates' % BASE_DIR,
            BASE_DIR,
            "%s/" % APPS_DIR,
        ],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.template.context_processors.i18n",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "ptf.context_processors.ptf",
            ],
            "libraries": {
                "cms_tags": "mersenne_cms.templatetags.cms_tags",
            },
        },
    },
]


WSGI_APPLICATION = "pcj.wsgi.application"


# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": os.path.join(BASE_DIR, "db.sqlite3"),
    }
}


# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]


MODELTRANSLATION_TRANSLATION_FILES = ("mersenne_cms.translations",)
MODELTRANSLATION_DEFAULT_LANGUAGE = "en"

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/

STATIC_URL = "/static/"


from ptf.settings import *

try:
    from pcj.settings_local import *
except ImportError:
    raise Exception("You must defined a settings_local.py")

ENABLE_STATIC_PAGES = True
STATIC_URL = "/static/"
STATIC_ROOT = BASE_DIR + "/static/"

STATICFILES_DIRS = [os.path.join(BASE_DIR, "pcj/static/")]

MEDIA_URL = "/media/"
ICON_URL = "/icon/"
try:
    if RESOURCES_ROOT:
        MEDIA_ROOT = RESOURCES_ROOT + "/PCJ/media/"
        ICON_ROOT = RESOURCES_ROOT
except NameError:
    pass

# COMPATIBILITE NUMDAM

HOST_FQDN = socket.getfqdn()
# without / at the end
BASE_URL = "http://%s" % HOST_FQDN


OAI_REPOSITORY = "pcj.repository"


DOI_BASE_URL = "https://doi.org/"
ARTICLE_BASE_URL = "/article/"
ISSUE_BASE_URL = "/issue/"
ICON_BASE_URL = "/icon/"

# Mersenne cms settings

COLLECTION_PID = "PCJ"

CKEDITOR_IMAGE_BACKEND = "ckeditor_uploader.backends.PillowBackend"
CKEDITOR_UPLOAD_PATH = "uploads/"
CKEDITOR_ALLOW_NONIMAGE_FILES = True

from mersenne_cms.settings import CKEDITOR_CONFIGS

SHOW_TEX = True

# 'Tome' must be displayed as 'Volume' for PCJ
VOLUME_STRING = True
# PCJ has its own style to display references
REF_PCJ_STYLE = True

# CONTAINER_SEQUENCED = False
SORT_ARTICLES_BY_DATE = True

OAI_BY_DATE_PUBLISHED = True

# Show the html version
SHOW_BODY = False
