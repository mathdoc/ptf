from django import template
from django.template.defaultfilters import stringfilter

from ptf.display import resolver

register = template.Library()


@register.filter
@stringfilter
def is_uga_pci(value):
    resource = value in resolver.PCJ_UGA_SECTION
    return resource


@register.filter
@stringfilter
def get_section_value(value):
    return resolver.PCJ_SECTIONS.get(value, "")
