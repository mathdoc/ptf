$(document).ready(function () {
    $('.section-link').on('click', function() {

        var is_expanded = $(this).next().hasClass('in');

        $('.list-group.collapse').removeClass('in');
        $('.section-link .glyphicon').removeClass('glyphicon-chevron-down');
        $('.section-link .glyphicon').addClass('glyphicon-chevron-right');

        if (is_expanded) {
            $(this).next().addClass('in');
        } else {
            $(this).children('.glyphicon').addClass('glyphicon-chevron-down');
            $(this).children('.glyphicon').removeClass('glyphicon-chevron-right');
        }
    });
});
function highlightReference(elementId, duration, interval) {
    const element = document.getElementById(elementId);
    element.classList.add("highlight")
        setTimeout(() => {
            element.classList.remove("highlight");
        }, duration)
}

document.addEventListener('DOMContentLoaded', () => {
    console.log("ici")
    const tooltips = document.querySelectorAll('.tooltipPCJ')
    console.log(tooltips)
    const articleDisplay = document.querySelector('#abstract-html')
    if (articleDisplay==null) {
        return
    }
    articleDisplayRect = articleDisplay.getBoundingClientRect()

    tooltips.forEach(tooltip => {
        tooltip.removeAttribute('style')
        tooltip.addEventListener('mouseover', function(event)  {
            console.log("event")
            const tooltipText = tooltip.querySelector('.tooltip')
            tooltipText.removeAttribute('style')

            let tooltipRect = tooltipText.getBoundingClientRect()
            if (tooltipRect.right + tooltipText.offsetWidth > articleDisplayRect.right) {
                tooltipText.style.left = 'unset'
                tooltipText.style.right = '10%'
            }

            /* 300 is the distance between top of the window and the bottom of breadcrumb */
            if (tooltipRect.top < 500) {
                tooltipText.style.bottom = 'unset'
                tooltipText.style.top = '100%'
            }

            tooltipText.classList.remove('tooltiptexthidden')
            tooltipText.classList.add('tooltiptextshow')

        })

        tooltip.addEventListener('mouseout', () => {
            const tooltipText = tooltip.querySelector('.tooltip')
            tooltipText.classList.remove("tooltiptextshow")
            tooltipText.classList.add("tooltiptexthidden")
        })

    })
})

const references = document.querySelectorAll(".bibitemcls")
for (let [index, element] of references.entries()) {
    index += 1
    element.innerHTML = element.innerHTML.replace('[' + index + ']', '<a href=#' + index + '>[' + index + ']</a>')
}
