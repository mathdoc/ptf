"""pcj URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  re_path(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  re_path(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import include
    2. Add a URL to urlpatterns:  re_path(r'^blog/', include('blog.urls'))
"""

from django.conf import settings
from django.conf.urls.static import static
from django.urls import include
from django.urls import path
from django.urls import re_path

if settings.DEBUG:
    import debug_toolbar

from mersenne_cms import urls as cms_urls
from mersenne_cms.admin import mersenne_admin
from oai import urls as oai_urls
from ptf import urls as ptf_urls
from ptf.urls import ptf_i18n_patterns
from ptf.views import ItemViewClassFactory
from upload import urls as upload_urls

from .views import ConferenceDetail
from .views import ConferenceList
from .views import HomeView
from .views import PcjArticleView
from .views import SectionDetail
from .views import SectionList
from .views import TopicDetail
from .views import TopicList

urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path("articles/<path:aid>/", PcjArticleView.as_view(), name="article"),
    path('section/', SectionList.as_view(), name='section-list'),
    path('section/<slug>/', SectionDetail.as_view(), name='section-detail'),
    path('topic/', TopicList.as_view(), name='topic-list'),
    path('topic/<path:slug>/', TopicDetail.as_view(), name='topic-detail'),
    path('conference/', ConferenceList.as_view(), name='conference-list'),
    path('conference/<slug>/', ConferenceDetail.as_view(), name='conference-detail'),
    path('', include(ptf_urls)),
    path('', include(cms_urls)),
    path('', include(oai_urls)),
]

urlpatterns += ptf_i18n_patterns

if settings.ALLOW_ADMIN:
    urlpatterns += [
        path('upload/', include(upload_urls)),
        re_path(r'^admin/', mersenne_admin.urls),
    ]

    if settings.DEBUG:
        urlpatterns += [
            path('__debug__/', include(debug_toolbar.urls)),
        ]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.ICON_URL, document_root=settings.ICON_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

ItemViewClassFactory.views["article"] = PcjArticleView
