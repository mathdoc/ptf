import random

from django.conf import settings
from django.http import Http404
from django.views.generic import ListView
from django.views.generic.base import TemplateView

from mersenne_cms.models import Page
from pcj.templatetags.pcj_helpers import get_section_value
from ptf.display.resolver import PCJ_CONFERENCES
from ptf.display.resolver import PCJ_SECTIONS
from ptf.models import Article
from ptf.models import Collection
from ptf.models import Container
from ptf.views.base_views import ArticleView

PCJ_SUPPORTERS = [
    "aeeb.png",
    "agroparistech.png",
    "amu.jpg",
    "aropolis_fondation.jpg",
    "biblio_univ_montreal.jpg",
    "bodleian.png",
    "cbgp.png",
    "cnrs.png",
    "csic.png",
    "ecoevorxiv.png",
    "ecotoq.png",
    "ephe.png",
    "eseb.png",
    "ifremer.png",
    "imperial_college_london.svg",
    "inrae.png",
    "inrap.png",
    "inserm.png",
    "ird.png",
    "isa.jpg",
    "institut_pasteur.png",
    "isem.svg",
    "kndv.png",
    "kuleuven.png",
    "max_planck_dl.png",
    "mesr.jpg",
    "montpellier_supagro.png",
    "mpi-eva.png",
    "ouvrir_la_science.png",
    "palevoprim.png",
    "reseau_urfist.png",
    "royal_danish_library.png",
    "sfe2.png",
    "sib.jpg",
    "sorbonne_universite.png",
    "ucl.png",
    "uclouvain.png",
    "uftmip.png",
    "uga.png",
    "umbc.png",
    "unco.jpg",
    "univ_bath.png",
    "univ_birmingham.png",
    "univ_bristol.png",
    "univ_de_bordeaux.png",
    "univ_guyane.jpg",
    "univ_libre_bruxelle.png",
    "univ_liege.png",
    "univ_lille.png",
    "univ_limoge.png",
    "univ_lorraine.png",
    "univ_lyon_1.png",
    "univ_montpellier.png",
    "univ_oxford.png",
    "univ_paris_nanterre.jpg",
    "univ_paris_saclay.png",
    "univ_poitiers.png",
    "univ_rennes_1.png",
    "univ_sheffield.jpg",
    "univ_strasbourg.png",
    "univ_surrey.jpg",
    "univ_sussex.png",
    "univ_tours.png",
    "univ_uca_auvergne.png",
    "univ_uca_cote_azur.png",
    "univ_york.png",
    "upvd.png",
    "vu_amsterdam.png",
    "uvsq.png",
    "sdu.png",
]


class TagBuilder:
    @staticmethod
    def get_topic_tag(topic):
        topic = "".join(letter for letter in topic if letter.isalnum())
        topic = topic.lower()
        return topic

    @staticmethod
    def get_section_libelle(searched_section):
        for section, value in PCJ_SECTIONS.items():
            if searched_section == section:
                return value


class PcjArticleView(ArticleView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        article = self.obj
        topics = []
        corresponding_author = []
        for contrib in article.contributions.all():
            if contrib.role == "author" and contrib.corresponding:
                corresponding_author.append(contrib)
        if len(corresponding_author) > 0:
            context["corresponding_author"] = corresponding_author
        for subj in article.subj_set.all():
            print("searching ", subj.value, " type", subj.type)
            match subj.type:
                case "topic":
                    url = TagBuilder.get_topic_tag(subj.value)
                    libelle = subj.value
                    print("Topic = url:", url, "value:", libelle)
                    topics.append({"url": url, "libelle": libelle})
                case "pci":
                    section = article.get_pci_section()
                    url = section
                    libelle = TagBuilder.get_section_libelle(section)
                    print("Section = url: ", url, "value:", libelle)
                    context["section"] = {"url": url, "libelle": libelle}
        context["topics"] = topics
        return context


class HomeView(ListView):
    model = Article
    template_name = "pcj/home.html"

    def get_queryset(self):
        queryset = super().get_queryset()
        return (
            queryset.filter(sites__id=settings.SITE_ID)
            .prefetch_for_toc()
            .order_by_published_date()[:4]
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["sections"] = PCJ_SECTIONS
        section_mgr = SectionList()
        context["sections"] = section_mgr.get_categories()

        context["conferences"] = PCJ_CONFERENCES
        context["journal"] = Collection.objects.get(
            pid=settings.COLLECTION_PID, sites__id=settings.SITE_ID
        )
        context["latest_volume"] = (
            Container.objects.filter(sites__id=settings.SITE_ID).latest().pid
        )

        qs = Page.objects.filter(slug_en="supporters")
        if qs.exists():
            supporters = []
            page = qs.first()
            content = page.content_en
            i = content.find('src="/media/PCJ/uploads')
            while i > 0:
                i += 5
                j = content.find('"', i)
                if j > 0:
                    supporters.append(content[i:j])
                i = content.find('src="/media/PCJ/uploads', i)
        else:
            supporters = [f"/static/pcj/img/logo_{supporter}" for supporter in PCJ_SUPPORTERS]

        context["supporters"] = random.sample(supporters, len(supporters))

        return context


class CategoryList(TemplateView):
    template_name = "pcj/category_list.html"

    def __init__(self, category_type, subcategory_type, **kwargs):
        super().__init__(**kwargs)
        self.category_type = category_type
        self.subcategory_type = subcategory_type

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["category_type"] = self.category_type
        context["subcategory_type"] = self.subcategory_type
        context["category_list"] = self.get_categories()
        context["journal"] = Collection.objects.get(
            pid=settings.COLLECTION_PID, sites__id=settings.SITE_ID
        )

        return context

    def get_categories(self):
        return {}


class ConferenceList(CategoryList):
    def __init__(self, **kwargs):
        super().__init__("conference", "", **kwargs)

    def get_categories(self):
        conferences = {}

        for conference in PCJ_CONFERENCES:
            conferences[conference] = {"value": conference, "total": 1}

        return conferences


class CategoryDetail(ListView):
    model = Article
    ordering = ["-date_published", "-seq"]
    paginate_by = 10
    template_name = "pcj/category_detail.html"

    def __init__(
        self, category_type, subcategory_type, subcategory_subj_type, subj_type, **kwargs
    ):
        super().__init__(**kwargs)
        self.category_type = category_type
        self.subcategory_type = subcategory_type
        self.subj_type = subj_type
        self.subcategory_subj_type = subcategory_subj_type

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.filter(sites__id=settings.SITE_ID).filter(
            subj__value=self.kwargs["slug"], subj__type=self.subj_type
        )

        subcategory = self.request.GET.get(self.subcategory_type, "")
        if subcategory:
            qs = qs.filter(subj__value=subcategory, subj__type=self.subcategory_subj_type)

        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        slug = self.kwargs["slug"]

        context["category_type"] = self.category_type
        context["subcategory_type"] = self.subcategory_type
        context["category"] = slug
        context["journal"] = Collection.objects.get(
            pid=settings.COLLECTION_PID, sites__id=settings.SITE_ID
        )
        # path, queryDict = CleanSearchURL.decode(query, path)
        context["query"] = self.request.GET
        context["page_path"] = self.request.path
        params = ""
        for paramName, paramValue in self.request.GET.items():
            if paramName != "page":
                if len(params) == 0:
                    params = "?"
                else:
                    params.join("&")
                params = params + paramName + "=" + paramValue
        context["query"] = params
        context["nbArticle"] = len(self.object_list)
        return context


class SectionList(CategoryList):
    def __init__(self, **kwargs):
        super().__init__("section", "topic", **kwargs)

    def get_categories(self):
        sections = super().get_categories()

        for section, value in PCJ_SECTIONS.items():
            sections[section] = {"total": 0, "value": value, "tag": section}

        qs = Article.objects.filter(sites__id=settings.SITE_ID).prefetch_related("subj_set")
        for article in qs:
            section = article.get_pci_section()
            if not section:
                continue

            sections[section]["total"] += 1
            for subj in article.subj_set.all():
                if subj.type != "topic":
                    continue

                topic = subj.value
                if topic not in sections[section]:
                    sections[section][topic] = {"title": topic, "total": 0}
                sections[section][topic]["total"] += 1

        return sections


class ConferenceDetail(CategoryDetail):
    def __init__(self, **kwargs):
        super().__init__("conference", "", "", "conference", **kwargs)


class SectionDetail(CategoryDetail):
    def __init__(self, **kwargs):
        super().__init__("section", "topic", "topic", "pci", **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if context["category"] not in PCJ_SECTIONS:
            raise Http404
        context["category"] = PCJ_SECTIONS[context["category"]]
        context["subcategory"] = self.request.GET.get("topic", "")
        return context


class TopicList(CategoryList):
    def __init__(self, **kwargs):
        super().__init__("topic", "section", **kwargs)

    def get_topic_tag(self, topic):
        return TagBuilder.get_topic_tag(topic)

    def get_categories(self):
        topics = super().get_categories()

        qs = Article.objects.filter(sites__id=settings.SITE_ID).prefetch_related("subj_set")
        for article in qs:
            section = article.get_pci_section()
            for subj in article.subj_set.all():
                if subj.type != "topic":
                    continue

                topic = subj.value

                if topic not in topics:
                    topics[topic] = {"total": 0, "value": topic, "tag": self.get_topic_tag(topic)}
                topics[topic]["total"] += 1

                if section not in topics[topic]:
                    topics[topic][section] = {"total": 0, "title": get_section_value(section)}
                topics[topic][section]["total"] += 1

        sorted_topics = dict(
            sorted(topics.items(), key=lambda item: item[1]["total"], reverse=True)
        )

        return sorted_topics


class TopicDetail(CategoryDetail):
    def __init__(self, **kwargs):
        super().__init__("topic", "section", "pci", "topic", **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # Dans le cas où notre query n'a rien renvoyé, c'est que le topic n'existe pas
        if len(self.object_list) == 0:
            raise Http404

        context["subcategory"] = get_section_value(self.request.GET.get("section", ""))

        return context
