import os
import sys

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mbk.settings")

sys.path.append(".")

from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()

from ptf import model_helpers
from ptf.cmds import xml_cmds

p = model_helpers.get_provider("mathdoc-id")

collection = model_helpers.get_collection("MBK")
if not collection:
    f = open("/mathdoc_archive/MBK/MBK.xml")
    body = f.read()
    f.close()
    cmd = xml_cmds.addCollectionsXmlCmd({"body": body})
    journals = cmd.do()
    journal = journals[0]

collection = model_helpers.get_collection("MALSM")
if not collection:
    f = open("/mathdoc_archive/MBK/MALSM.xml", encoding="utf-8")
    body = f.read()
    f.close()
    cmd = xml_cmds.addCollectionsXmlCmd({"body": body})
    journals = cmd.do()
    journal = journals[0]

for bid in (
    "MBK_2014__1_",
    "MBK_2014__2_",
    "MBK_2014__3_",
    "MBK_2014__4_",
    "MBK_2014__5_",
    "MBK_2014__6_",
    "MBK_2014__7_",
):
    book = model_helpers.get_container(bid)
    print(book)
    f = open(f"/mathdoc_archive/MBK/{bid}/{bid}.xml", encoding="utf8")
    body = f.read()
    f.close()
    cmd = xml_cmds.addOrUpdateBookXmlCmd({"body": body})
    book = cmd.do()
