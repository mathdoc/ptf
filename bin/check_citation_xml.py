import os
import sys

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ptf_tools.settings")

sys.path.append(".")

from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()

from ptf import models
from ptf.cmds.xml.jats import jats_parser

for article in models.Article.objects.all().order_by("pid"):
    for bibitem in article.bibitem_set.all():
        current_xbibitem = jats_parser.check_bibitem_xml(bibitem)
        current_extids = current_xbibitem.extids

        new_ids = {}
        for bibitemid in bibitem.bibitemid_set.all():
            type = bibitemid.id_type
            if type == "numdam-id":
                type = "mathdoc-id"

            if bibitemid.id_type in ["zbl-item-id", "mr-item-id", "doi", "mathdoc-id"]:
                new_ids[bibitemid.id_type] = {
                    "id_type": bibitemid.id_type,
                    "id_value": bibitemid.id_value,
                    "checked": bibitemid.checked,
                    "false_positive": bibitemid.false_positive,
                }

        xbibitem = jats_parser.update_bibitem_xml(bibitem, new_ids)
        new_extids = xbibitem.extids

        if len(new_extids) != len(current_extids):
            print(article.pid, bibitem.label, current_extids, new_extids)
