#!/bin/bash
for collection in 'TIMB' 'AM'
do
    echo "Deploying $collection"
   python manage.py archive -pid $collection -folder /mathdoc_archive -binary-folder /mathdoc_archive/ -toc-only True
done
