import requests

# from pyvirtualdisplay import Display
# from selenium import NoSuchElementException
# from selenium import Proxy
# from selenium import ProxyType
from selenium import webdriver

# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "alco.settings")
# sys.path.append('.')
#
# from django.core.wsgi import get_wsgi_application
# application = get_wsgi_application()
#
# from ptf import models


def get_article_pids(colid):
    response = requests.get("https://mersenne:Demo@alco-test.centre-mersenne.org/api-articles/")
    response.raise_for_status()

    result = response.json()
    ids = result["articles"]["ids"]
    ids = [id for id in ids if id.find(colid + "_") == 0]
    ids.sort()
    return ids


def get_numdam_pids():
    response = requests.get("http://mersenne:Demo@numdam-pre.u-ga.fr/api-articles/")
    response.raise_for_status()

    result = response.json()
    ids = result["articles"]["ids"]
    ids.sort()
    return ids


def check_one_url(url):
    global driver
    result = True

    passed = False
    while not passed:
        try:
            driver.get(url)

            # elems = driver.find_elements_by_class_name('merror')
            # elems = driver.find_elements_by_class_name('mjx-error')
            elems = driver.find_elements_by_class_name("mjx-n")
            for elem in elems:
                color = elem.value_of_css_property("color")
                if color == "rgb(255, 0, 0)":
                    result = False
            passed = True
        except:
            driver.close()
            driver = webdriver.Firefox()
            driver.implicitly_wait(1.5)

    return result


sites = [
    ("ACIRM", "acirm-test.centre-mersenne.org"),
    ("AFST", "afst-test.centre-mersenne.org"),
    ("AHL", "ahl-test.centre-mersenne.org"),
    ("AIF", "aif-test.centre-mersenne.org"),
    ("ALCO", "alco-test.centre-mersenne.org"),
    ("AMBP", "ambp-test.centre-mersenne.org"),
    ("CCIRM", "ccirm-test.centre-mersenne.org"),
    ("CML", "cml-test.centre-mersenne.org"),
    ("CRBIOL", "test-comptes-rendus.academie-sciences.fr/biologies"),
    ("CRCHIM", "test-comptes-rendus.academie-sciences.fr/chimie"),
    ("CRGEOS", "test-comptes-rendus.academie-sciences.fr/geoscience"),
    ("CRMATH", "test-comptes-rendus.academie-sciences.fr/mathematique"),
    ("CRMECA", "test-comptes-rendus.academie-sciences.fr/mecanique"),
    ("CRPHYS", "test-comptes-rendus.academie-sciences.fr/physique"),
    ("JEDP", "jedp-test.centre-mersenne.org"),
    ("JEP", "jep-test.centre-mersenne.org"),
    ("JTNB", "jtnb-test.centre-mersenne.org"),
    ("MRR", "mrr-test.centre-mersenne.org"),
    ("MSIA", "msia-test.centre-mersenne.org"),
    ("OGEO", "ogeo-test.centre-mersenne.org"),
    ("OJMO", "ojmo-test.centre-mersenne.org"),
    ("PMB", "pmb-test.centre-mersenne.org"),
    ("ROIA", "roia-test.centre-mersenne.org"),
    ("SLSEDP", "slsedp-test.centre-mersenne.org"),
    ("SMAI-JCM", "smai-jcm-test.centre-mersenne.org"),
    ("TSG", "tsg-test.centre-mersenne.org"),
    ("WBLN", "wbln-test.centre-mersenne.org"),
]

driver = webdriver.Firefox()
driver.implicitly_wait(1.5)

# f = open("out.txt", "w")
# f.close()

init = "AFST_1961_4_25__209_0"
start = False

i = 0
pids = get_numdam_pids()

for pid in pids:
    if not start and init is not None and pid == init:
        start = True

    if start:
        i += 1

        # driver.get opens a file. We cannot open too many files.
        if i % 100 == 0:
            print("Closing Firefox")
            driver.close()
            driver = webdriver.Firefox()
            # driver = webdriver.Chrome("/usr/lib/chromium-browser/chromedriver")
            driver.implicitly_wait(1.5)

        url = f"http://mersenne:Demo@numdam-pre.u-ga.fr/item/{pid}/"
        result = check_one_url(url)
        print(pid, result)

        f = open("out.txt", "a")
        f.write(f"{pid},{result}\n")
        f.close()

exit()

i = 0
for site in sites:
    colid = site[0]
    base_url = site[1]

    pids = get_article_pids(colid)

    for pid in pids:
        if not start and init is not None and pid == init:
            start = True

        if start:
            i += 1

            # driver.get opens a file. We cannot open too many files.
            if i % 100 == 0:
                print("Closing Firefox")
                driver.close()
                driver = webdriver.Firefox()
                # driver = webdriver.Chrome("/usr/lib/chromium-browser/chromedriver")
                driver.implicitly_wait(1.5)

            url = f"https://mersenne:Demo@{base_url}/item/{pid}/"
            result = check_one_url(url)
            print(pid, result)

            f = open("out.txt", "a")
            f.write(f"{pid},{result}\n")
            f.close()


# articles = models.Article.objects.filter(my_container__my_collection__pid='ALCO')
# f = open("out.txt", "w")
# f.close()

# i = 0
# for article in articles:
#     i += 1
#
#     # driver.get opens a file. We cannot open too many files.
#     if i % 100 == 0:
#         print("Closing Firefox")
#         driver.close()
#         driver = webdriver.Firefox()
#         # driver = webdriver.Chrome("/usr/lib/chromium-browser/chromedriver")
#         driver.implicitly_wait(1.5)
#
#     f = open("out.txt", "a")
#
#     #article = model_helpers.get_article("RSMUP_2012__128__287_0")
#     try:
#         article_url = "https://mersenne:Demo@alco-test.centre-mersenne.org/item/" + article.pid
#
#         driver.get(article_url)
#
#         has_error = False
#         elems = driver.find_elements_by_class_name('merror')
#         has_error = len(elems) > 0
#
#         elems = driver.find_elements_by_class_name('mjx-error')
#         has_error = len(elems) > 0
#
#         if has_error:
#             print(str(i) + " " + article.pid + " ERROR")
#             f.write(article.pid + "\n")
#             f.flush()
#         else:
#             print(str(i) + " " + article.pid + " OK")
#     except:
#         print(str(i) + " " + article.pid + " CRASH")
#         f.write(article.pid + " CRASH\n")
#         f.flush()
#         pass
#
#     f.close()


# main = driver.find_element_by_id('main')
# main = driver.find_element_by_xpath("//*[@id='main']")
# spans = driver.find_elements_by_xpath("//span")

# for span in spans:
#    class_attribute = span.get_attribute("class")


driver.quit()
# display.stop()
