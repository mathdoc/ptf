import os
import sys

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ptf_tools.settings")

sys.path.append(".")

from django.conf import settings
from django.core.wsgi import get_wsgi_application

from ptf import model_helpers
from ptf.cmds import xml_cmds
from ptf.display import resolver
from ptf.models import Article
from ptf.models import Collection
from ptf.models import Container

application = get_wsgi_application()
collections = settings.MERSENNE_COLLECTIONS
results = {}
all_correct = 0
all_incorrect = 0
for colid in collections:
    col = Collection.objects.get(pid=colid)
    containers = Container.objects.filter(
        my_collection=col, year__in=["2018", "2019", "2020", "2021"]
    )
    correct = 0
    incorrect = 0
    for container in containers:
        folder = resolver.get_cedram_issue_tex_folder(colid, container.pid)
        filename = os.path.join(folder, container.pid + "-cdrxml.xml")

        if os.path.isfile(filename):
            params = {
                "colid": colid,
                "input_file": filename,
                "remove_email": True,
                "remove_date_prod": False,
                "diff_only": True,
            }

            try:
                cmd = xml_cmds.importCedricsIssueDirectlyXmlCmd(params)
                result = cmd.import_cedrics_issue()
                xissue = cmd.xissue
            except Exception:
                continue

            for xarticle in xissue.articles:
                try:
                    article = Article.objects.get(pid=xarticle.pid)
                except Exception:
                    continue

                if article.date_published is not None:
                    date_published = article.date_published.strftime("%Y-%m-%d")

                    try:
                        xdate_published = model_helpers.parse_date_str(
                            xarticle.date_published_iso_8601_date_str
                        ).strftime("%Y-%m-%d")
                    except Exception:
                        xdate_published = ""

                    if xdate_published != date_published:
                        # print(article.pid, date_published, xdate_published)
                        incorrect += 1
                        all_incorrect += 1
                    else:
                        correct += 1
                        all_correct += 1

    if correct + incorrect == 0:
        print(colid)
    else:
        results[colid] = {
            "correct": correct,
            "incorrect": incorrect,
            "%": correct / (correct + incorrect),
        }

print(all_correct / (all_correct + all_incorrect), "%")
print(results)
