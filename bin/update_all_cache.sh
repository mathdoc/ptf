#!/bin/bash
for site in 'acirm' 'afst' 'ahl' 'aif' 'alco' 'ambp' 'ccirm' 'cml' 'crbiol' 'crgeos' 'crmath' 'crchim' 'crmeca' 'crphys' 'jedp' 'jep' 'jtnb' 'mrr' 'msia' 'ogeo' 'ojmo' 'pcj' 'pmb' 'roia' 'smai' 'tsg' 'slsedp' 'wbln'
do
    cd /var/www/$site/current
    source venv/bin/activate
    cd sites/$site
    python manage.py update_cache -pid ${site^^}
done
