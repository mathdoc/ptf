import setup

setup.setup()
import fnmatch
import getopt
import logging
import os
import sys
import time

from old.manage import solrmanager

from ptf.models import Resource


def usage():
    print("index_text -d <directory>")
    print("or")
    print("index_text -f <file>")
    sys.exit(1)


def index_file(solr, path, commit=False):
    idart = os.path.basename(path)[:-4]
    try:
        r = Resource.objects.get(pid=idart)
    except Resource.DoesNotExist:
        return False
    text = (
        open(path).read().decode("utf-8")
    )  # .replace('<', ' ').replace('>', ' ').replace('&', ' ')
    text = text.strip()
    try:
        solr.add_full_text(r.id, text, commit=commit)
    except Exception:
        logging.error(path)
        return False
    return True


try:
    opt, args = getopt.getopt(sys.argv[1:], "d:f:")
except getopt.GetoptError:
    usage()
if args:
    usage()
dp = None
fp = None
for o, v in opt:
    if o == "-d":
        dp = v
    elif o == "-f":
        fp = v
logging.basicConfig(filename="index_text.log", level=logging.INFO)
solr = solrmanager.SolrManager()
count = 0
start = time.strftime("%a, %d %b %Y %H:%M:%S")
if fp is not None:
    success = index_file(solr, fp, commit=True)
    if success:
        count += 1
else:
    names = [x for x in os.listdir(dp) if fnmatch.fnmatch(x, "*.txt")]
    n = len(names)
    for name in names:
        fp = f"{dp}/{name}"
        success = index_file(solr, fp, commit=False)
        if success:
            count += 1
        if count == 10 or count == n:
            # Grrr Java heap space !
            solr.commit()
print(start)
end = time.strftime("%a, %d %b %Y %H:%M:%S")
print("indexed %d files" % count)
print(end)
