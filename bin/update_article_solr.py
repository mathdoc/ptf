import os
import sys

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "crchim.settings")

sys.path.append(".")

from django.conf import settings
from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()

from ptf.cmds import solr_cmds
from ptf.models import Article

colid = settings.COLLECTION_PID
base = settings.SITE_URL_PREFIX

qs = Article.objects.filter(my_container__my_collection__pid=colid)
for article in qs.all():
    binary_files = article.get_binary_files_href()
    data = {}
    if "self" in binary_files:
        files = binary_files["self"]
        for key, value in files.items():
            if key in ["pdf", "djvu", "tex"]:
                data[key] = "/" + base + value

    cmd = solr_cmds.updateResourceSolrCmd(data)
    cmd.set_resource(article)
    cmd.do()

for article in qs.all():
    doc = solr_cmds.solrGetDocumentByPidCmd({"pid": article.pid}).do()
    if doc and "pdf" in doc:
        print(doc["pdf"])
