# In 2019, articles were imported in Mersenne
# Early 2020, articles were re-imported from scratch (to get additional data from Cedrics like history dates)
# But Cedrics XML do not always have doi. They are lost between early 2020 and July 2020
# This script was used to retrieve the doi from crossref

import os
import sys

import requests

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ptf_tools.settings")

sys.path.append(".")

from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()

from lxml import etree

from ptf import models

base_url = "http://api.crossref.org/works/"

cols = {
    "aif": 3326,
    "afst": 1629,
    "ccirm": 28,
    "cml": 59,
    "jedp": 671,
    "jtnb": 1108,
    "pmb": 37,
    "smai-jcm": 64,
    "tsg": 314,
    "wbln": 24,
}

for col, last_id in cols.items():
    for i in range(1, last_id + 1):
        doi = "10.5802/" + col + "." + str(i)
        print(doi, end=" ")

        params = {}
        url = base_url + doi
        response = requests.get(url, params=params, timeout=2)
        response.raise_for_status()

        try:
            result = response.json()
            link = result["message"]["link"][0]["URL"]
            pid = os.path.basename(link)
            if "cedram" not in link:
                pid = pid[:-4]

            a = models.Article.objects.get(pid=pid)
            if a.doi != doi:
                print(pid, a.doi, end=" ")

        except Exception:
            # No link in the JSON, try the XML

            try:
                url += ".xml"
                response = requests.get(url, params=params, timeout=2)
                data = response.text.encode("utf-8")

                tree = etree.XML(data)
                pid = tree.xpath('//*[local-name()="item_number"]')[0].text
                if a.doi != doi:
                    print(pid, a.doi, end=" ")
                    a.doi = doi
                    a.save()

            except Exception:
                print("Exception", end=" ")

        print()
