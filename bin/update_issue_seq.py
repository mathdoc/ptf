import setup

setup.setup()
import sys

from old.manage import manager

from ptf import models

numdam_id = sys.argv[1]
journal = models.Collection.objects.get(provider__pid_type="mathdoc-id", pid=numdam_id)
manager.update_issue_seq(journal)
