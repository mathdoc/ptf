#!/bin/bash
for site in 'cr' 'crbiol' 'crgeos' 'crmath' 'crchim' 'crmeca' 'crphys'
do
    if [ -z "$2" ]
    then
        cap $site:$1 deploy NORESTART=1 FORCE_DEPLOY=1
    else
        cap $site:$1 BRANCH=$2 deploy NORESTART=1 FORCE_DEPLOY=1
    fi
done
cap $site:$1 restart
