import os
import sys


def setup(site="bibnum_site"):
    try:
        PTF_ROOT = os.environ["PTF_ROOT"]
    except KeyError:
        PTF_ROOT = "/home/goutorbe/platitude"
        os.environ["PTF_ROOT"] = PTF_ROOT
    sys.path.append(PTF_ROOT)
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    os.environ["DJANGO_SETTINGS_MODULE"] = f"bibnum.sites.{site}.{site}.settings"
