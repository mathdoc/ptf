#!/bin/bash
if [ $# -ne 1 ];
    then echo 'Usage: boot.sh <data root>'
    exit
fi
dt=$(date '+%d/%m/%Y %H:%M:%S');
echo "$dt"
bash bootstrap.sh $1
bash boot2.sh $1
bash boot3.sh $1
bash boot4.sh $1
bash bootseminars.sh $1
bash bootbooks.sh $1
bash bootproceedings.sh $1
#bash bootcif.sh $1
bash index_text.sh $1
dt=$(date '+%d/%m/%Y %H:%M:%S');
echo "$dt"

