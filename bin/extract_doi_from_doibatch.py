import os
import sys

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ptf_tools.settings")

sys.path.append(".")

from lxml import etree

from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()

from ptf import models

filename = sys.argv[1]
file = open(filename)
data = file.read().encode("utf-8")
# print(data)
file.close()
tree = etree.XML(data)

journal_article = tree.xpath('//*[local-name()="journal_article"]')
for article in journal_article:
    pid = article.xpath('.//*[local-name()="item_number"]')
    doi = article.xpath('.//*[local-name()="doi"]')
    print(f'{{"pid":"{pid[0].text}","doi":"{doi[0].text}"}}')
    art = models.Article.objects.get(pid=pid[0].text)
    art.doi = doi[0].text
    art.save()
