#!/bin/bash

#definition de code couleur

red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

echo "Script permettant de passer numdam-preERREUR en numdam-pre :Attention à utiliser avec précaution"
echo "${red}si numdam-pre a été recréé il sera supprimé !"
echo "êtes vous sûr ? [o/n]${reset}"
read A
if [ "$A" != "o" ]
then
echo "Abandon"
exit
fi

set -x

#detection des VMID
VMID_PRE=`grep "name: numdam-pre$" /etc/pve/qemu-server/* | sed -e 's/^.*\/\([0-9]\{3\}\).conf:.*/\1/g' `
VMID_ERROR=`grep "name: numdam-preERREUR" /etc/pve/qemu-server/* | sed -e 's/^.*\/\([0-9]\{3\}\).conf:.*/\1/g' `

if [ "$VMID_PRE" != "" ]
then

	# machine concernée : numdam-plus/pre
	echo "${green}on demonte numdam_data de numdam-plus ${reset}"
	ssh exist@numdam-plus 'sudo umount /numdam_data/pre'
	if [ $? -gt 0 ]
	then
		echo "${red}**erreur : numdam-pre semble ne pas être monté sur numdam-plus : NON BLOQUANT${reset}"	
	fi

	echo "on arrête numdam-pre avant de le supprimer"
	qm shutdown $VMID_PRE -timeout 10
	echo "on supprime numdam-pre"
	qm destroy $VMID_PRE
fi

echo "on renomme dans la conf numdam-preERREUR en numdam-pre"
sed -i 's/^name: numdam-preERREUR$/name: numdam-pre/' /etc/pve/qemu-server/$VMID_ERROR.conf

echo "on démarre le nouveau numdam-pre et pause de 30s"
qm start $VMID_ERROR
sleep 30
echo "on reactive le service nfs"
ssh deployer@numdam-pre 'sudo systemctl enable nfs-kernel-server'
ssh deployer@numdam-pre 'sudo systemctl start nfs-kernel-server'
echo "on remonte numdam-pre:/numdam_data sur numdam-plus"
ssh exist@numdam-plus 'sudo mount numdam-pre:/numdam_data /numdam_data/pre'
