# Programme qui permet de vérifier si nous avons en local toute les archives de
# mathdoc dans le répertoire /mathdoc_archive/

import argparse
import os
from os.path import isdir
from xml.dom import minidom

from lxml import etree
from progress.bar import Bar
from pyexpat import ExpatError

# Création du paramètre (PID de la collection ex: JEP)

parser = argparse.ArgumentParser(
    description="Cela permet de savoir si tous les fichiers que nous avons besoin pour ptf sont bien présents dans "
    "/mathdoc_archive, le paramètre optionnel est le PID d'une collection"
)
parser.add_argument("-p", "-pid", type=str, metavar="", help="PID de la collection")
args = parser.parse_args()

# Création variables pour les couleurs

OK = "\033[92m"  # GREEN
WARNING = "\033[93m"  # YELLOW
FAIL = "\033[91m"  # RED
RESET = "\033[0m"  # RESET COLOR

path = "/mathdoc_archive/"


def code_check_archive(list_repertoire, lien_mathdoc):
    # Création de la bar de chargement, puis parcours des issues pour récuperer les liens qui mènent aux fichiers
    compteur = 0
    manque_fichiers = []
    print(f"Placement dans {lien_mathdoc}")
    os.chdir(lien_mathdoc)
    print("Recherche des répertoires :")
    bar = Bar("\tResearch", max=len(list_repertoire))
    lien_fichiers = []
    lien_images = []

    if "media" in list_repertoire:
        list_repertoire.remove("media")

    if "toc.xml" in list_repertoire:
        list_repertoire.remove("toc.xml")

    for issue in list_repertoire:
        if issue.endswith(".xml"):
            xml_rep = minidom.parse(issue)
            recup_ext_links = xml_rep.getElementsByTagName("ext-link")
            for recup_ext_link in recup_ext_links:
                recup_image = recup_ext_link.getAttribute("ext-link-type")
                if recup_image == "icon":
                    lien_images.append(recup_ext_link.getAttribute("href"))
                elif recup_image == "small_icon":
                    lien_images.append(recup_ext_link.getAttribute("href"))
        if isdir(lien_mathdoc + "/" + issue):
            if os.access(lien_mathdoc + "/" + issue + "/" + issue + ".xml", os.R_OK):
                os.chdir(lien_mathdoc + "/" + issue)
                try:
                    xml_issue = minidom.parse(issue + ".xml")

                    element_ext_links = xml_issue.getElementsByTagName("ext-link")
                    for element in element_ext_links:
                        element_image = element.getAttribute("ext-link-type")
                        if element_image == "icon":
                            lien_images.append(element.getAttribute("href"))

                    # Add self-uri (PDF...) of the issue
                    issue_metas = xml_issue.getElementsByTagName("issue-meta")
                    for issue_meta in issue_metas:
                        elements_uri = issue_meta.getElementsByTagName("self-uri")
                        for element in elements_uri:
                            lien_fichiers.append(element.getAttribute("xlink:href"))

                    # Add self-uri (PDF...) of the articles.
                    # Some self-uri might be http links to remote PDF: ignore those
                    articles = xml_issue.getElementsByTagName("article")
                    for article in articles:
                        has_remote_pdf = False
                        elements_uri = article.getElementsByTagName("self-uri")
                        for element in elements_uri:
                            href = element.getAttribute("xlink:href")
                            if href.find("http") == 0:
                                has_remote_pdf = True
                            else:
                                lien_fichiers.append(href)

                        # Make sure there is a PDF per article
                        if not has_remote_pdf:
                            elements = article.getElementsByTagName("article-id")
                            for element in elements:
                                pub_id_type = element.getAttribute("pub-id-type")
                                if pub_id_type == "mathdoc-id":
                                    pid = element.firstChild.nodeValue
                                    pid = pid.replace("/", "_").replace(".", "_")
                                    pdf_to_find = os.path.join(
                                        lien_mathdoc, issue, pid, pid + ".pdf"
                                    )
                                    lien_fichiers.append(pdf_to_find)

                except ExpatError:
                    print(
                        FAIL
                        + f"Nous venons d'avoir un problème avec le fichier suivant : {issue}"
                        + RESET
                    )
            os.chdir(lien_mathdoc + "/" + issue)
            list_articles = os.listdir()
            if os.access(lien_mathdoc + "/" + issue + "/src", os.R_OK):
                os.chdir(lien_mathdoc + "/" + issue + "/src")
                compteur_docs = 0
                rep_src = os.listdir()
                # Parcours à la racine (@col/@issue/src)
                if "digitisation" in rep_src:
                    if os.access(lien_mathdoc + "/" + issue + "/src/digitisation", os.R_OK):
                        os.chdir(lien_mathdoc + "/" + issue + "/src/digitisation")
                        list_docs_rep = os.listdir()
                        for list_doc_rep in list_docs_rep:
                            if list_doc_rep.endswith("xml") | list_doc_rep.endswith("tif"):
                                compteur_docs += 1
                        if compteur_docs == 0:
                            print(
                                FAIL
                                + "\rERREUR ! Vous n'avez pas de documents au format .xml ou .tif dans le "
                                "répertoire" + RESET
                            )
                    else:
                        print(FAIL + "\rERREUR ! Nous n'avons pas accès au répertoire" + RESET)
                elif "acquisition" in rep_src:
                    if os.access(lien_mathdoc + "/" + issue + "/src/acquisition/tex", os.R_OK):
                        os.chdir(lien_mathdoc + "/" + issue + "/src/acquisition/tex")
                        list_docs_rep = os.listdir()
                        for list_doc_rep in list_docs_rep:
                            if list_doc_rep.endswith("tex"):
                                compteur_docs += 1
                        if compteur_docs == 0:
                            print(
                                FAIL
                                + "\rERREUR ! Vous n'avez pas de documents au format .tex dans le répertoire"
                                + RESET
                            )
                    else:
                        print(FAIL + "\rERREUR ! Nous n'avons pas accès au répertoire" + RESET)
                        bar.finish()
                        exit()
                elif "tex" in rep_src:
                    if os.access(lien_mathdoc + "/" + issue + "/src/tex", os.R_OK):
                        os.chdir(lien_mathdoc + "/" + issue + "/src/tex")
                        list_docs_rep = os.listdir()
                        for list_doc_rep in list_docs_rep:
                            if list_doc_rep.endswith("tex"):
                                compteur_docs += 1
                        if compteur_docs == 0:
                            print(
                                FAIL
                                + "\rERREUR ! Vous n'avez pas de documents au format .tex dans le répertoire"
                                + RESET
                            )
                    else:
                        print(FAIL + "\rERREUR ! Nous n'avons pas accès au répertoire" + RESET)
            for article in list_articles:
                if os.access(lien_mathdoc + "/" + issue + "/" + article + "/src", os.R_OK):
                    os.chdir(lien_mathdoc + "/" + issue + "/" + article + "/src")
                    compteur_docs = 0
                    rep_src = os.listdir()
                    # Parcours à la racine (@col/@issue/src)
                    if "digitisation" in rep_src:
                        if not os.access(
                            lien_mathdoc + "/" + issue + "/" + article + "/" + article + ".djvu",
                            os.R_OK,
                        ) & os.access(
                            lien_mathdoc + "/" + issue + "/" + article + "/" + article + ".pdf",
                            os.R_OK,
                        ):
                            print(FAIL + "\rERREUR !  Il vous manque le pdf et/ou djvu")
                            bar.finish()
                            exit()
                        os.chdir(lien_mathdoc + "/" + issue + "/" + article + "/src/digitisation")
                        list_docs_rep = os.listdir()
                        for list_doc_rep in list_docs_rep:
                            if list_doc_rep.endswith("xml") | list_doc_rep.endswith("tif"):
                                compteur_docs += 1
                        if compteur_docs == 0:
                            print(
                                FAIL
                                + "\rERREUR ! Vous n'avez pas de documents au format .xml ou .tif dans le "
                                "répertoire" + RESET
                            )
                    elif "acquisition" in rep_src:
                        if (
                            not os.access(
                                lien_mathdoc
                                + "/"
                                + issue
                                + "/"
                                + article
                                + "/"
                                + article
                                + ".djvu",
                                os.R_OK,
                            )
                            & os.access(
                                lien_mathdoc
                                + "/"
                                + issue
                                + "/"
                                + article
                                + "/"
                                + article
                                + ".pdf",
                                os.R_OK,
                            )
                            & os.access(
                                lien_mathdoc
                                + "/"
                                + issue
                                + "/"
                                + article
                                + "/"
                                + article
                                + ".xml",
                                os.R_OK,
                            )
                        ):
                            print(
                                FAIL
                                + "\rERREUR !  Il vous manque le pdf et/ou djvu ou meme le fichier xml"
                                + RESET
                            )
                            bar.finish()
                            exit()
                        os.chdir(lien_mathdoc + "/" + issue + "/" + article + "/src/acquisition")
                        list_docs_rep = os.listdir()
                        for list_doc_rep in list_docs_rep:
                            if list_doc_rep.endswith("zip") | list_doc_rep.endswith("tgz"):
                                compteur_docs += 1
                        if compteur_docs == 0:
                            print(
                                FAIL
                                + "\rERREUR ! Vous n'avez pas de documents au format .zip ou .tgz  dans le "
                                "répertoire" + RESET
                            )
                    elif "tex" in rep_src:
                        if not os.access(
                            lien_mathdoc + "/" + issue + "/" + article + "/" + article + ".pdf",
                            os.R_OK,
                        ):
                            print(FAIL + "\rERREUR !  Il vous manque le pdf" + RESET)
                            bar.finish()
                            exit()
                        os.chdir(lien_mathdoc + "/" + issue + "/" + article + "/src/tex")
                        list_docs_rep = os.listdir()
                        for list_doc_rep in list_docs_rep:
                            if list_doc_rep.endswith("tex") | list_doc_rep.endswith("bib"):
                                compteur_docs += 1
                        if compteur_docs == 0:
                            print(
                                FAIL
                                + "\rERREUR ! Vous n'avez pas de documents au format .tex ou .bib dans le "
                                "répertoire" + RESET
                            )
        bar.next()
    bar.finish()

    # Parcours de liens qui mènent aux fichiers, s'il existe +1 au compteur sinon on ajoute le lien à une liste
    os.chdir(path)
    print("Recherche des fichiers :")
    bar = Bar("\tResearch", max=len(lien_fichiers))
    for fichier in lien_fichiers:
        if os.access(fichier, os.R_OK):
            compteur += 1
        else:
            manque_fichiers.append(fichier)
        bar.next()
    bar.finish()

    if lien_images:
        print("Recherche des images :")
        bar = Bar("\tResearch", max=len(lien_images))
        for image in lien_images:
            if os.access(os.path.join(path, image), os.R_OK):
                compteur += 1
            else:
                manque_fichiers.append(image)
            bar.next()
        bar.finish()

    total_fichier = len(lien_fichiers) + len(lien_images)
    try:
        pourcentage_reussite = int((100 * compteur) / total_fichier)
    except ZeroDivisionError:
        pourcentage_reussite = 0

    if pourcentage_reussite == 100:
        print(
            OK + f"Sur {total_fichier} fichiers nécessaires nous avons réussi à en trouver "
            f"{compteur} soit {pourcentage_reussite}% des fichiers nécessaires." + RESET
        )
    else:
        print(
            OK
            + f"Sur {len(lien_fichiers) + len(lien_images)} fichiers nécessaires nous avons réussi à en trouver "
            f"{compteur} soit "
            + RESET
            + WARNING
            + f"{pourcentage_reussite}% "
            + RESET
            + OK
            + "des fichiers "
            "nécessaires." + RESET
        )

    # Si la liste comporte au minimum un élément, affichage de cette liste
    if manque_fichiers:
        print(FAIL + "Les fichiers qu'il vous manque sont : " + RESET)
        for fichier in manque_fichiers:
            print(FAIL + "\t- " + fichier + RESET)


def check_archive_argument(pid):
    # Si on un argument
    # Vérificaton de l'existance du répertoire contenant les archives (ex: JEP)
    if not os.access(os.path.join(path, pid), os.R_OK):
        print(FAIL + f"Vous ne possédez pas de répertoire {pid} dans /mathdoc_archive" + RESET)
        exit()

    lien_mathdoc = os.path.join(path, pid)
    list_repertoire = os.listdir(lien_mathdoc)
    list_repertoire.sort()
    if "toc.xml" in list_repertoire:
        print("Vérification du nommage des répertoires :")
        print(f"Placement dans {lien_mathdoc}")
        os.chdir(lien_mathdoc)
        f = open("toc.xml")
        body = f.read()
        parser = etree.XMLParser(
            huge_tree=True,
            recover=True,
            remove_blank_text=True,
            remove_comments=True,
            resolve_entities=True,
        )
        tree = etree.fromstring(body.encode("utf-8"), parser=parser)
        list_journals_meta = tree.findall(".//journal-meta")
        list_repertoires = os.listdir()
        for journal_meta in list_journals_meta:
            journal = journal_meta.find("folder").text
            if journal not in list_repertoires:
                print(FAIL + f"\rERREUR ! Le répertoire {journal} n'existe pas" + RESET)
            else:
                print(OK + f"\rLe répertoire {journal} existe" + RESET)

        f.close()
    code_check_archive(list_repertoire, lien_mathdoc)


def check_archive_sans_argument():
    repertoires = os.listdir(path)
    repertoires.sort()

    print(f"Lecture des répertoires dans {path}")

    for repertoire in repertoires:
        os.chdir(os.path.join(path, repertoire))

        lien_mathdoc = os.path.join(path, repertoire)
        list_repertoire = os.listdir(lien_mathdoc)
        list_repertoire.sort()

        code_check_archive(list_repertoire, lien_mathdoc)


if args.p is not None:
    check_archive_argument(args.p.upper())
else:
    check_archive_sans_argument()
