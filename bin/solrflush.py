import setup

setup.setup()
from django.conf import settings

from ptf.solr import pysolr

solr = pysolr.Solr(settings.SOLR_URL)
solr.delete(q="*:*")
solr.commit()
