#!/bin/bash
#definition de code couleur

red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`


echo "Script de creation de numdam-pre"
echo "${red}êtes vous sûr ? [o/n]${reset}"
read A
if [ "$A" != "o" ]
then
echo "Abandon"
exit
fi

# au niveau des droits :
# script à éxécuter sur hyperviseur gérant les vm numdam-pre/prod/old
# - l'accès aux VM se fait via deployer avec une cle publique deposée dans authorized_keys des VM
# - l'accès à numdam-plus se fait via user exist avec une cle publique deposée dans authorized_keys
# - les commandes sont autorisées dans sudoers avec deployer:NOPASSWD la commande

# debbuging
set -x




# machine concernée : numdam-pre sur hyperviseur 
# d'abord recuperer le vmid par le nom
echo "${green} on recrée la vm pre depuis le template${reset}"
qm list
VMID_PRE=`grep "name: numdam-pre" /etc/pve/qemu-server/* | sed -e 's/^.*\/\([0-9]\{3\}\).conf:.*/\1/g' `
VMID_PROD=`grep "name: numdam-prod" /etc/pve/qemu-server/* | sed -e 's/^.*\/\([0-9]\{3\}\).conf:.*/\1/g' `
VMID_OLD=`grep "name: numdam-old" /etc/pve/qemu-server/* | sed -e 's/^.*\/\([0-9]\{3\}\).conf:.*/\1/g' `

echo "id de la vm numdam-prod :"$VMID_PROD
echo "id de la vm numdam-old :"$VMID_OLD
echo "id de la vm numdam-pre :"$VMID_PRE

if [ "$VMID_PRE" != "" ]
then
	echo "*** ATTENTION : VM PRE déjà présente on ne peut pas la recréer"
	exit
fi


        echo "on detecte quelle id est libre pour vm pre" 
	for i in 209 210 211
	do
		if [ "$VMID_PROD" != "$i" ] && [ "$VMID_OLD" != "$i" ]
		then
			VMID_PRE=$i
		fi
	done
	echo "VMID_PRE = "$VMID_PRE


#s'assurer que vm template arreter car sinon pb lors du redemarrage du clone
qm status 208 |grep -q stopped
if [ $? -gt 0 ]
then
	echo "${red}**erreur numdam-template n'est pas arreté${reset}"	
	exit 
fi
qm clone 208 $VMID_PRE -name numdam-pre -full 1
if [ $? -gt 0 ]
then
	echo "${red}**erreur clonage ${reset}"	
	exit 
fi
 
qm start $VMID_PRE
echo "pause le temps que la vm pre se lance"
sleep 60
qm list

#################
STEP=11
echo "${green}changement nom/IP du clone + reboot${reset}"
ssh deployer@numdam-template "sudo cp /etc/network/interfaces.numdam-pre /etc/network/interfaces"
if [ $? -gt 0 ]
then
	echo "${red}**erreur etape : $STEP ${reset}"	
	exit $STEP
fi

ssh deployer@numdam-template "sudo cp /etc/hosts.numdam-pre /etc/hosts"
if [ $? -gt 0 ]
then
	echo "${red}**erreur etape : $STEP ${reset}"	
	exit $STEP
fi
ssh deployer@numdam-template "sudo cp /etc/hostname.numdam-pre /etc/hostname"
if [ $? -gt 0 ]
then
	echo "${red}**erreur etape : $STEP ${reset}"	
	exit $STEP
fi


ssh deployer@numdam-template "sudo cp /etc/mailname.numdam-pre /etc/mailname"
if [ $? -gt 0 ]
then
	echo "${red}**erreur etape : $STEP ${reset}"	
	exit $STEP
fi

qm shutdown $VMID_PRE
qm start $VMID_PRE
qm list
echo "on remonte numdam-pre sur numdam-plus apres une pause de 60s le temps que la VM se lance"
sleep 45

ssh exist@numdam-plus 'sudo mount numdam-pre:/numdam_data /numdam_data/pre'
echo "c'est bon numdam-pre est créé et est prêt à être redéployer : SOURCE : cap pre deploy puis UPLOAD/sync PDF depuis raffinement"

## ancienne methode ....################
#STEP=12
#echo "${green}on recopie numdam_data depuis numdam-old après une pause de 60s le temps que la vm se lance"
#sleep 60
#
#set -x
#ssh exist@numdam-plus 'sudo mount numdam-pre:/numdam_data /numdam_data/pre'
#if [ $? -gt 0 ]
#then
#	echo "${red}**erreur etape : $STEP ${reset}"	
#	exit $STEP
#fi
#
#echo "recopie des fichiers : cela va prendre un certain temps : plus de 100Go via NFS"
#
#set -x
#ssh exist@numdam-plus 'rsync -a /numdam_dev/exploitation2 /numdam_data/pre'
#if [ $? -gt 0 ]
#then
#	echo "${red}**erreur etape : $STEP ${reset}"	
#	exit $STEP
#fi
