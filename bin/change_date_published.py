import os
import sys

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ptf_tools.settings")

sys.path.append(".")

from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()

from ptf import models
from ptf.model_helpers import parse_date_str

YEARS = [2010]
COL = "AIF"
for year in YEARS:
    containers = models.Container.objects.filter(pid__startswith=COL + "_%s" % year)
    for container in containers:
        print(container)
        for article in container.article_set.all():
            print(article)
            article.date_published = parse_date_str("%s-01-01" % year)
            print(article.date_published)
            article.save()
