#!/bin/bash
if [ -z ${VIRTUAL_ENV+x} ]; then echo "il faut executer ce script dans le virtualenv"; exit; fi
if [ ! -e manage.py ]
then
	echo "il faut lancer ce programme dans le projet django où se trouve le manage.py"
	exit
fi
while true; do
    read -p "ce script va effacer ce qui est lié à un site mersenne : fixtures / objects  and solr index ? " yn
    case $yn in
        [Yy]* ) 
echo "
from django.conf import settings
from ptf.models import *
from ptf.model_helpers import *
from ptf.cmds import ptf_cmds

c = get_collection(settings.COLLECTION_PID)
if c:
    for con in c.content.all():
         print(con)
         cmd = ptf_cmds.addContainerPtfCmd({'pid':con.pid,'ctype':con.ctype})
         cmd.set_provider(provider=con.provider)
         cmd.add_collection(con.my_collection)
         cmd.undo()

    c.delete()

    for con in Container.objects.filter(sites__id=settings.SITE_ID).all():
         print(con)
         cmd = ptf_cmds.addContainerPtfCmd({'pid':con.pid,'ctype':con.ctype})
         cmd.set_provider(provider=con.provider)
         cmd.add_collection(con.my_collection)
         cmd.undo()

    Collection.objects.filter(sites__id=settings.SITE_ID).all().delete()

" | python manage.py shell 
python manage.py migrate `basename "$PWD"` zero
break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done
