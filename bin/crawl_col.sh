#!/bin/bash
for col in 'TM'
# 'VSGU' 'CMFD' 'ISU' 'IVP' 'VTGU' 'UZERU' 'MMKZ' 'VMJ' 'MP' 'UZKU' 'KUIPM' 'KUTGS' 'KUKZ' 'KUMEM' 'TAM' 'MMO' 'TSP' 'UZMU' 'TDM' 'INTA' 'INTM' 'INTG' 'INTO' 'INTF' 'INTD' 'INTV' 'TIMM' 'ZNSL' 'MO' 'MAT' 'VVGUM' 'PDMA' 'PDM' 'JSFU' 'VYURM' 'VYURV' 'VYURU' 'JCEM' 'VUU' 'TVIM' 'VKAM' 'SVMO' 'VSGTU' 'CHEB' 'FSSC' 'VTPMK' 'IIMI' 'SVFU' 'VNGU' 'MAIS' 'PA' 'IVM' 'LJM' 'EJMCA' 'EMJ' 'IIGUM' 'VTAMU' 'ITVS' 'CMA' 'MMJ' 'IZKAB' 'MMCM' 'BGUMI' 'CHFMJ' 'VCHGU' 'LFMO' 'ADM' 'THSP' 'SIGMA' 'JMAG' 'BASM' 'DEMR' 'PFMT' 'TIMB' 'DVMG' 'UFA' 'MBB' 'NANO' 'MGTA' 'CGTM' 'FPM' 'VMP' 'VMUMM' 'VSPUI'
do
   python manage.py import -pid ${col} -folder /mathdoc_archive
   python manage.py crawl -pid ${col} -init
   python manage.py crawl -pid ${col} -pdf
   python manage.py crawl -pid ${col} -pdf
   python manage.py export -pid ${col} -folder /mathdoc_archive -for-archive -with-binary-files
   cp -r /mathdoc_archive/${col} /media/labbeo/Transcend/mathdoc_archive/
done
