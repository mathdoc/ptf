#!/bin/bash
#definition de code couleur

red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

curl 2> /dev/null
if [ $? -eq 2 ]
then
echo "test de curl : OK"
else
echo "if faut curl"
exit
fi

echo "Script de mise en prod de Numdam : Attention à utiliser avec précaution"
echo "il faut vérifier si Numdam plus n'est pas en train de déployer"
echo "${green}Raison de mise en prod :(sera envoyee à numdamprod log si reussite mise en prod) utiliser \n pour retour charriot${reset}"
raison=
while [[ $raison = "" ]]; do
   read raison
done

echo "${red}êtes vous sûr ? [o/n]${reset}"
read A
if [ "$A" != "o" ]
then
echo "Abandon"
exit
fi

# au niveau des droits :
# script à éxécuter sur hyperviseur gérant les vm numdam-pre/prod/old
# - l'accès aux VM se fait via deployer avec une cle publique deposée dans authorized_keys des VM
# - l'accès à numdam-plus se fait via user exist avec une cle publique deposée dans authorized_keys
# - les commandes sont autorisées dans sudoers avec deployer:NOPASSWD la commande

# debbuging
#set -x



# STEP 1
# machine concernée : numdam-plus
# tester si numdam-plus est en train de déployer
# TODO



STEP=2
# machine concernée : numdam-plus/pre
echo "${green}on demonte numdam_data de numdam-plus pour éviter les mises à jour des PDFs et on bloque le montage ${reset}"
set -x
ssh exist@numdam-plus 'sudo umount /numdam_data/pre'
if [ $? -gt 0 ]
then
	echo "${red}**erreur etape : $STEP ${reset}"
	exit 2
fi

set -x
ssh deployer@numdam-pre 'sudo systemctl stop nfs-kernel-server'
if [ $? -gt 0 ]
then
	echo "${red}**erreur etape : $STEP ${reset}"
	exit 2
fi

set -x
ssh deployer@numdam-pre 'sudo systemctl disable nfs-kernel-server'
if [ $? -gt 0 ]
then
	echo "${red}**erreur etape : $STEP ${reset}"
	exit 2
fi




STEP=3
# machine concernée : numdam-pre / numdam-prod
echo "${green}on met à jour les dates de publication sur numdam-pre ${reset}"
nomUID=`date +%N`.json
set -x
ssh deployer@numdam-prod "bash /var/www/numdam/current/bin/export_publishing_date.sh /tmp/$nomUID"
if [ $? -gt 0 ]
then
	echo "${red}**erreur etape : $STEP ${reset}"
	exit 3
fi

set -x
scp deployer@numdam-prod:/tmp/$nomUID /tmp
if [ $? -gt 0 ]
then
	echo "${red}**erreur etape : $STEP ${reset}"
	exit 3
fi

set -x
scp /tmp/$nomUID deployer@numdam-pre:/tmp
if [ $? -gt 0 ]
then
	echo "${red}**erreur etape : $STEP ${reset}"
	exit 3
fi

set -x
ssh deployer@numdam-pre "bash  /var/www/numdam/current/bin/import_publishing_date.sh /tmp/$nomUID"
if [ $? -gt 0 ]
then
	echo "${red}**erreur etape : $STEP ${reset}"
	exit 3
fi
rm /tmp/$nomUID

STEP=4
# machine concernée : numdam-old sur hyperviseur : destroy
echo "${green}suppression de numdam-old pour pouvoir renommer numdam-prod en old${reset}"
# d'abord recuperer le vmid par le nom
VMID_OLD=`grep "name: numdam-old" /etc/pve/qemu-server/* | sed -e 's/^.*\/\([0-9]\{3\}\).conf:.*/\1/g' `

if [ "$VMID_OLD" = "" ]
then
	echo "*** ATTENTION : VM OLD non présent mais ce n'est pas une erreur bloquante"
else
	echo "ATTTENTION : arrêt et suppression de numdam-old : vérifier le vmid avant de valider"
	qm list
	echo "on va détruire VMID=$VMID_OLD : êtes vous sûr ? [o/n]"
	read A
	if [ "$A" != "o" ]
	then
		echo "${red}**erreur etape : $STEP ${reset}"
		echo "Abandon de la suppression et du script : penser au rollback"
		exit $STEP
	fi
	set -x
	qm stop $VMID_OLD
	if [ $? -gt 0 ]
	then
		echo "**erreur non bloquante echec arret vm OLD sans doute car non existante**"
		qm list
	fi
	set -x
	qm destroy $VMID_OLD
	if [ $? -gt 0 ]
	then
		echo "**erreur non bloquante suppression vm OLD sans doute car non existante**"
	fi
fi

STEP=5
# machines concernées : prod/pre
echo "${green}recopie des logs d'apache ${reset}"
set -x
ssh deployer@numdam-prod 'sudo tar cf /tmp/apache2_log.tgz /var/log/apache2/ /var/lib/awstats/'
if [ $? -gt 1 ]
then
	echo "${red}**erreur etape : $STEP ${reset}"
	exit $STEP
fi

set -x
scp deployer@numdam-prod:/tmp/apache2_log.tgz /tmp
if [ $? -gt 0 ]
then
	echo "${red}**erreur etape : $STEP ${reset}"
	exit $STEP
fi

set -x
scp /tmp/apache2_log.tgz deployer@numdam-pre:/tmp
if [ $? -gt 0 ]
then
	echo "${red}**erreur etape : $STEP ${reset}"
	exit $STEP
fi

set -x
ssh deployer@numdam-pre 'cd /; sudo tar xvf /tmp/apache2_log.tgz'
if [ $? -gt 0 ]
then
	echo "${red}**erreur etape : $STEP ${reset}"
	exit $STEP
fi

set -x
rm /tmp/apache2_log.tgz
if [ $? -gt 0 ]
then
	echo "${red}**erreur etape : $STEP ${reset}"
fi


STEP=6
echo "${green} changement des IP/noms de prod et pre par copie de fichiers de conf pre configurés${reset}"

echo "===================================================="
echo "${red}ATTENTION ${reset} : à partir de cette étape les IP et noms sont changés : si le script ne va pas jusqu'au bout, il faut tout vérifier car en cas de reboot on peut ne plus avoir de prod"
echo "===================================================="

set -x



ssh deployer@numdam-prod "sudo cp /etc/network/interfaces.numdam-old /etc/network/interfaces"
if [ $? -gt 0 ]
then
	echo "${red}**erreur etape : $STEP ${reset}"
	ssh deployer@numdam-prod "sudo cp /etc/network/interfaces.numdams.org /etc/network/interfaces"

	exit $STEP
fi


ssh deployer@numdam-prod "sudo cp /etc/hosts.numdam-old /etc/hosts"
if [ $? -gt 0 ]
then
	echo "${red}**erreur etape : $STEP ${reset}"
	ssh deployer@numdam-prod "sudo cp /etc/network/interfaces.numdams.org /etc/network/interfaces"
	ssh deployer@numdam-prod "sudo cp /etc/hosts.numdam.org /etc/hosts"
	exit $STEP
fi

ssh deployer@numdam-prod "sudo cp /etc/hostname.numdam-old /etc/hostname"
if [ $? -gt 0 ]
then
	echo "${red}**erreur etape : $STEP ${reset}"
	ssh deployer@numdam-prod "sudo cp /etc/network/interfaces.numdams.org /etc/network/interfaces"
	ssh deployer@numdam-prod "sudo cp /etc/hosts.numdam.org /etc/hosts"
	ssh deployer@numdam-prod "sudo cp /etc/hostname.numdam.org /etc/hostname"
	exit $STEP
fi

ssh deployer@numdam-pre "sudo cp /etc/network/interfaces.numdam.org /etc/network/interfaces"
if [ $? -gt 0 ]
then
	echo "${red}**erreur etape : $STEP ${reset}"
	ssh deployer@numdam-prod "sudo cp /etc/network/interfaces.numdams.org /etc/network/interfaces"
	ssh deployer@numdam-prod "sudo cp /etc/hosts.numdam.org /etc/hosts"
	ssh deployer@numdam-prod "sudo cp /etc/hostname.numdam.org /etc/hostname"
	ssh deployer@numdam-pre "sudo cp /etc/network/interfaces.numdam-pre /etc/network/interfaces"
	exit $STEP
fi

ssh deployer@numdam-pre "sudo cp /etc/hosts.numdam.org /etc/hosts"
if [ $? -gt 0 ]
then
	echo "${red}**erreur etape : $STEP ${reset}"
	ssh deployer@numdam-prod "sudo cp /etc/network/interfaces.numdams.org /etc/network/interfaces"
	ssh deployer@numdam-prod "sudo cp /etc/hosts.numdam.org /etc/hosts"
	ssh deployer@numdam-prod "sudo cp /etc/hostname.numdam.org /etc/hostname"
	ssh deployer@numdam-pre "sudo cp /etc/network/interfaces.numdam-pre /etc/network/interfaces"
	ssh deployer@numdam-pre "sudo cp /etc/hosts.numdam-pre /etc/hosts"
	exit $STEP
fi

ssh deployer@numdam-pre "sudo cp /etc/hostname.numdam.org /etc/hostname"
if [ $? -gt 0 ]
then
	echo "${red}**erreur etape : $STEP ${reset}"
	ssh deployer@numdam-prod "sudo cp /etc/network/interfaces.numdams.org /etc/network/interfaces"
	ssh deployer@numdam-prod "sudo cp /etc/hosts.numdam.org /etc/hosts"
	ssh deployer@numdam-prod "sudo cp /etc/hostname.numdam.org /etc/hostname"
	ssh deployer@numdam-pre "sudo cp /etc/network/interfaces.numdam-pre /etc/network/interfaces"
	ssh deployer@numdam-pre "sudo cp /etc/hosts.numdam-pre /etc/hosts"
	ssh deployer@numdam-pre "sudo cp /etc/hostname.numdam-pre /etc/hostname"
	exit $STEP
fi


ssh deployer@numdam-pre "sudo cp /etc/mailname.numdam.org /etc/mailname"
if [ $? -gt 0 ]
then
	echo "${red}**erreur etape : $STEP ${reset}"
	ssh deployer@numdam-prod "sudo cp /etc/network/interfaces.numdams.org /etc/network/interfaces"
	ssh deployer@numdam-prod "sudo cp /etc/hosts.numdam.org /etc/hosts"
	ssh deployer@numdam-prod "sudo cp /etc/hostname.numdam.org /etc/hostname"
	ssh deployer@numdam-pre "sudo cp /etc/network/interfaces.numdam-pre /etc/network/interfaces"
	ssh deployer@numdam-pre "sudo cp /etc/hosts.numdam-pre /etc/hosts"
	ssh deployer@numdam-pre "sudo cp /etc/hostname.numdam-pre /etc/hostname"
	ssh deployer@numdam-pre "sudo cp /etc/mailname.numdam-pre /etc/mailname"
	exit $STEP
fi

ssh deployer@numdam-pre.u-ga.fr "sudo a2dissite 000-default.numdam-pre"
ssh deployer@numdam-pre.u-ga.fr "sudo a2ensite 000-default.numdam-prod"

ssh deployer@numdam-pre.u-ga.fr "grep -i solr /var/www/numdam/current/sites/numdam/numdam/settings_local.py"
echo "Vérifier que l'url solr est bien 127.0.0.1"

STEP=7
echo "${green}Reboot des VM pour prise en compte effective des nouveaux noms ${reset}"
# on recupere les VMID
qm list
VMID_PROD=`grep "name: numdam-prod" /etc/pve/qemu-server/* | sed -e 's/^.*\/\([0-9]\{3\}\).conf:.*/\1/g' `
VMID_PRE=`grep "name: numdam-pre" /etc/pve/qemu-server/* | sed -e 's/^.*\/\([0-9]\{3\}\).conf:.*/\1/g' `

set -x
qm shutdown $VMID_PRE -timeout 10
if [ $? -gt 0 ]
then
	echo "${red}**erreur etape : $STEP ${reset}"
	exit $STEP
fi

#on renomme les VM dans les fichiers de conf de proxmox
sed -i 's/^name: numdam-pre$/name: numdam-prod/' /etc/pve/qemu-server/$VMID_PRE.conf
set -x
qm shutdown $VMID_PROD -timeout 15
if [ $? -gt 0 ]
then
	echo "${red}**erreur etape : $STEP ${reset} la prod ne s'est pas bien arrêté !!!"
	exit $STEP
fi

echo "${red}MOMENT IMPORTANT : on redémarre pre qui est devenu prod${reset}"
set -x
qm start $VMID_PRE
if [ $? -gt 0 ]
then
	echo "${red}ATTENTION LA PROD N EST PAS PARTIE !!!${reset}"
	exit $STEP
fi
echo "${green}La migration est globalement réussie : VM PROD relancée${reset}"

#  on redémarre old
sed -i 's/^name: numdam-prod$/name: numdam-old/' /etc/pve/qemu-server/$VMID_PROD.conf
set -x
qm start $VMID_PROD
if [ $? -gt 0 ]
then
	echo "${red}old pas parti ${reset}"
	exit $STEP
fi
qm list


##########
STEP=8
echo "${green}on teste le site de prod via curl : on fait une pause de 1min afin que la VM démarre complètement${reset}"


sleep 60
set -x
http_code=`curl -s -o /dev/null -w "%{http_code}" http://www.numdam.org`
if [ $http_code -gt 300 ]
	then
	echo "${red}VERIFIER l'ACcueil !!!${reset}"
	exit $STEP
	fi
http_code=`curl -s -o /dev/null -w "%{http_code}" 'http://www.numdam.org/oai/?verb=Identify'`
if [ $http_code -gt 300 ]
	then
	echo "${red}VERIFIER l'OAI' !!!${reset}"
	exit $STEP
	fi
http_code=`curl -s -o /dev/null -w "%{http_code}" 'http://www.numdam.org/search/test-q'`
if [ $http_code -gt 300 ]
	then
	echo "${red}VERIFIER la recherche !!!${reset}"
	exit $STEP
	fi


##########

STEP=9
#envoi du mail si réussite mise en prod
date=`date`
echo -e "$date\n\n$raison" | mail numdamprod@listes.mathdoc.fr -s "mise en prod du $date"
# les logs sont ils bien gérés ?
# TODO
