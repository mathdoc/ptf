#!/bin/bash

for site in 'acirm' 'afst' 'ahl' 'aif' 'alco' 'ambp' 'ccirm' 'cml' 'jedp' 'jep' 'jtnb' 'mrr' 'msia' 'ogeo' 'ojmo' 'pcj' 'pmb' 'roia' 'smai' 'tsg' 'slsedp' 'wbln'
do
   root_folder=/var/www/${site}
   #root_folder=/home/labbeo/git/ptf
   venv_folder=current/venv
   #venv_folder=sites/env
   release_folder=current
   #release_folder=

   #source ${root_folder}/${venv_folder}/bin/activate
   colid=`echo $site | tr 'a-z' 'A-Z'`
   #cd ${root_folder}/${release_folder}/sites/${site}
   echo $colid
   # echo `pwd`
   python manage.py convert_contribs -pid ${colid} -delete
done
