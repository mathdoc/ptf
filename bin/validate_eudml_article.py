import sys

import requests

if __name__ == "__main__":
    id = sys.argv[1]
    r = requests.get("http://193.54.239.195:8002/export/articles/" + id)
    xml = r.text.encode("utf-8")
    headers = {"Content-type": "application/xml"}
    r = requests.post("http://gajira:8020/validateJatsDocument/article", data=xml, headers=headers)
    print(r.status_code)
    print(r.json())
