#!/bin/bash
FIC=$1
cd /var/www/numdam/current/venv/bin
source activate
python /var/www/numdam/current/sites/numdam/manage.py deploy --export > $FIC
