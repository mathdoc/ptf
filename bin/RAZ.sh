#!/bin/bash
if [ -z ${VIRTUAL_ENV+x} ]; then echo "il faut executer ce script dans le virtualenv"; exit; fi
if [ ! -e manage.py ]
then
	echo "il faut lancer ce programme dans le projet django où se trouve le manage.py"
	exit
fi
while true; do
    read -p "Do you wish to reset your django database and solr instance?" yn
    case $yn in
        [Yy]* ) 
echo "
\t
select 'drop table if exists ' || tablename || ' cascade;'  from pg_tables  where schemaname = 'public';
" | python manage.py dbshell | grep -v "Showing" | grep -v "uples" | python manage.py dbshell
echo "drop table django_migrations;" | python manage.py dbshell
echo "la base de données est en théorie, vide il faut relancer : python manage.py migrate"
echo "on efface solr"
SOLR_URL=`echo "
from django.conf import settings
print(settings.SOLR_URL)
" | python manage.py shell 2>&1 | grep solr | sed -e 's/.*\(http.*\)/\1/'`
SOLR_URL=$SOLR_URL"/update?stream.body=%3Cdelete%3E%3Cquery%3E*:*%3C/query%3E%3C/delete%3E&commit=true"
echo "solr url :"$SOLR_URL
curl "$SOLR_URL" -x ''
break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done
