#!/bin/bash


echo "Script de ROLLBACK :Attention à utiliser avec précaution"
echo " OLD redevient PROD  et PROD devient numdam-preERREUR mais n'est pas redemarrée car a l'ip du numdam-pre potentiellement existant"
echo "${green}Raison de ROLLBACK :(sera enregistree dans fichier log si reussite ROLLBACK)${reset}"
raison=
while [[ $raison = "" ]]; do
   read raison
done

echo "êtes vous sûr ? [o/n]"
read A
if [ "$A" != "o" ]
then
echo "Abandon"
exit
fi

set -x

#STEP 6
echo "changement des IP/noms de prod et pre par copie de fichiers de conf pre configurés"
set -x
ssh deployer@numdam-prod "sudo cp /etc/network/interfaces.numdam-pre /etc/network/interfaces"
set +x
if [ $? -gt 0 ]
then
	echo "**erreur**"	
	exit 61
fi

set -x
ssh deployer@numdam-prod "sudo cp /etc/hosts.numdam-pre /etc/hosts"
set +x
if [ $? -gt 0 ]
then
	echo "**erreur**"	
	exit 62
fi
set -x
ssh deployer@numdam-prod "sudo cp /etc/hostname.numdam-pre /etc/hostname"
set +x
if [ $? -gt 0 ]
then
	echo "**erreur**"	
	exit 63
fi

set -x
ssh deployer@numdam-prod "sudo cp /etc/mailname.numdam-pre /etc/mailname"
set +x
if [ $? -gt 0 ]
then
	echo "**erreur**"	
	exit 63
fi


set -x
ssh deployer@numdam-old "sudo cp /etc/network/interfaces.numdam.org /etc/network/interfaces"
set +x
if [ $? -gt 0 ]
then
	echo "**erreur**"	
	exit 64
fi

set -x
ssh deployer@numdam-old "sudo cp /etc/hosts.numdam.org /etc/hosts"
set +x
if [ $? -gt 0 ]
then
	echo "**erreur**"	
	exit 65
fi
set -x
ssh deployer@numdam-old "sudo cp /etc/hostname.numdam.org /etc/hostname"
set +x
if [ $? -gt 0 ]
then
	echo "**erreur**"	
	exit 66
fi

set -x
ssh deployer@numdam-old "sudo cp /etc/mailname.numdam.org /etc/mailname"
set +x
if [ $? -gt 0 ]
then
	echo "**erreur**"	
	exit 67
fi

echo "on redémarre les VM pour prise en compte des changements de nom/ip"
# on recupere les VMID
VMID_PROD=`grep "name: numdam-prod" /etc/pve/qemu-server/* | sed -e 's/^.*\/\([0-9]\{3\}\).conf:.*/\1/g' `
VMID_OLD=`grep "name: numdam-old" /etc/pve/qemu-server/* | sed -e 's/^.*\/\([0-9]\{3\}\).conf:.*/\1/g' `

set -x
qm shutdown $VMID_OLD -timeout 10
set +x
if [ $? -gt 0 ]
then
	echo "**erreur**"	
	exit 1
fi

echo "on renomme les VM dans les fichiers de conf de proxmox"
sed -i 's/^name: numdam-old$/name: numdam-prod/' /etc/pve/qemu-server/$VMID_OLD.conf
qm shutdown $VMID_PROD -timeout 10
if [ $? -gt 0 ]
then
	echo "**erreur** la prod ne s'est pas bien arrêté !!!"	
	exit 1
fi

sed -i 's/^name: numdam-prod$/name: numdam-preERREUR/' /etc/pve/qemu-server/$VMID_PROD.conf

# MOMENT IMPORTANT : on redémarre old qui est devenu prod
set -x
qm start $VMID_OLD
set +x
if [ $? -gt 0 ]
then
	echo "ATTENTION LA PROD N EST PAS PARTIE !!!"	
	exit 1
fi

qm list
echo "numdam-prod renommé en numdam-preERREUR et ancienne prod en prod"
echo "pour ré utiliser numdam-preERREUR il faut :
- supprimer ? numdam-pre 
- renommer numdam-preERREUR dans le fichier de conf
- relancer numdam-pre
- relancer le service nfs et remonter numdam_data

le script  Numdam-preERREUR_vers_numdam_pre.sh le fait pour vous ;)"
date >> mise_en_prod.log
echo "$raison" >> mise_en_prod.log
