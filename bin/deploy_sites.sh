#!/bin/bash

if [ $# -eq 0 ] ; then
    echo "No stage argument supplied!"
    exit 1
fi


STAGE="$1"


deploy()
{
    for site in 'afst' 'ahl' 'aif' 'alco' 'ambp' 'art' 'cml' 'cr' 'crbiol' 'crgeos' 'crmath' 'crchim' 'crmeca' 'crphys' 'igt' 'jedp' 'jep' 'jtnb' 'mrr' 'msia' 'ogeo' 'ojmo' 'pcj' 'pmb' 'roia' 'smai' 'proceedings' 'mbk'
    do
        cap $site:$STAGE deploy NORESTART=1 FORCE_DEPLOY=1
    done
}

if [ "$STAGE" = "test" ] || [ "$STAGE" = "prod" ]; then
    echo "\n**********************************************************\n"
    echo "Do you really want to deploy on *** $STAGE *** site? (y/n)"
    echo "\n**********************************************************\n"
    read input
    if [ "$input" = "y" ]; then
        deploy
    fi
else
    deploy
fi
