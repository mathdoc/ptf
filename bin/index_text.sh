#!/bin/bash
for y in AMPA AIHP AIHPA AIHPB AIHPC AUG ASCFM ASCFPA ASENS ASNSP BSMF;do
    echo $y
    python index_text.py -d $1/texte/$y
done

for y in  BSMA CAD CTGDC CM DIA COCV M2AN PS JSFS MSH PHSC PMIHES RO ITA RSMUP RHM RSA SAD NAM;do
    echo $y
    python index_text.py -d $1/texte/$y
done

for y in  CSHM GEA STS TAN GAU SSL SG SAF;do
    echo $y
    python index_text.py -d $1/texte/$y
done
for y in  SB SBCD SC SCC SPS SDPP SD SE;do
    echo $y
    python index_text.py -d $1/texte/$y
done
for y in  SHC SJ SJL SLDB SL SPK SAC SMS SLS SENL SSS;do
    echo $y
    python index_text.py -d $1/texte/$y
done
for y in  AIF AFST AMBP JTNB JEDP SEDP TSG;do
    echo $y
    python index_text.py -d $1/texte/$y
done
for y in  MSM MSMF THESE;do
    echo $y
    python index_text.py -d $1/texte/$y
done
